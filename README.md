# Installation
## Install root somewhere.
## Edit env.sh for your system.
## Install ut software under $CBCSYSTEM/sw 

----------------------------
## FC7 firmware installation
```
cd fw/fc7/
```
with svn client,
```
svn co svn+ssh://[USER NAME at CERN]@svn.cern.ch/reps/ph-ese/be/fc7/tags/fc7_5.0.3
```
or download from web from

https://svnweb.cern.ch/cern/wsvn/ph-ese/be/fc7/

#### Modification for backward incompatibility of ipbus

[FC7 firmware top]/fw/src/sys/sys/system_core.vhd:    

at ipbus_ctrl instantiation, change names of these signals
```
ipb: entity work.ipbus_ctrl
pkt => pkt_rx_led,
pkt_oob => pkt_tx_led,
```
----------------------------
## IPBUS installation
```
cd fw/
git clone https://github.com/ipbus/ipbus-firmware.git
cd ipbus-firmware
git checkout v1.2
```
