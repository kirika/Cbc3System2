----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.11.2016 21:28:43
-- Design Name: 
-- Module Name: fast_signal_manager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity fast_command_manager is
		generic (TDC_NBITS : integer := 3);
    port (
    cbc_clk_m8                   : in  std_logic;
    cbc_clk                      : in  std_logic;
    cs_cnfg                     : in  cs_fcm_cnfg_type;
    cs_ctrl                     : in  cs_fcm_ctrl_type;
    cs_stat                     : out cs_fcm_stat_type;
    l1a_veto                     : in  std_logic;
    ext_com                      : in  cbc_fast_com_type; 
    seq_com                      : in  cbc_fast_com_type; 
		l1a_tdc_i                    : in  std_logic_vector(TDC_NBITS-1 downto 0);
    l1a_count_o                  : out std_logic_vector(8 downto 0);
    l1a_tdc                      : out std_logic_vector(TDC_NBITS-1 downto 0);
		com_o                        : out cbc_fast_com_type;
    cbc_fast_com_8bits           : out std_logic_vector(7 downto 0)
);
end fast_command_manager;

architecture Behavioral of fast_command_manager is

    constant header                 : std_logic_vector( 2 downto 0 ) := "110";
    constant trailer                : std_logic := '1';

    type trigger_fsm_type                  is (idle, running);  
    signal trigger_fsm              : trigger_fsm_type;

		signal ipb_com                  : cbc_fast_com_type;
    signal int_com                  : cbc_fast_com_type; 
    signal l1a_veto_40MHz           : std_logic;
		signal com                      : cbc_fast_com_type;
    signal l1a_count                : unsigned(8 downto 0);

    signal ext_sync_l1a             : std_logic;
    signal ext_async_l1a_tdccount   : std_logic_vector(2 downto 0);

    signal start_trigger            : std_logic;
    signal stop_trigger             : std_logic;

		signal reset                    : std_logic;
		signal fcg_reset                : std_logic;
		signal fcg_load_cnfg            : std_logic;
		signal fcg_start                : std_logic;
		signal fcg_stop                 : std_logic;

begin

	-- control signals
  fcm_start_trigger : entity work.dff_sync_edge_detect
    port map( reset => '0',
              clkb  => cbc_clk,
              dina  => cs_ctrl.start_trigger,
              doutb => start_trigger 
    );
  fcm_stop_trigger : entity work.dff_sync_edge_detect
      port map( reset => '0',
                clkb  => cbc_clk,
                dina  => cs_ctrl.stop_trigger,
                doutb => stop_trigger 
      );


    	
  fcm_reset_pulse : entity work.dff_sync_edge_detect
  port map( reset => '0',
            clkb  => cbc_clk,
            dina  => cs_ctrl.reset,
            doutb => reset 
  );
  ipb_fast_reset_pluse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fast_com.fast_reset, 
						doutb => ipb_com.fast_reset 
	);
  ipb_test_pulse_req_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fast_com.tpreq, 
						doutb => ipb_com.tpreq 
	);
  ipb_trigger_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fast_com.trigger, 
						doutb => ipb_com.trigger 
	);
  ipb_orbit_reset_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fast_com.orbit_reset, 
						doutb => ipb_com.orbit_reset 
	);
  fcg_reset_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fcg_reset, 
						doutb => fcg_reset 
	);
  fcg_load_cnfg_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fcg_load_cnfg, 
						doutb => fcg_load_cnfg 
	);
  fcg_start_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fcg_start, 
						doutb => fcg_start 
	);
  fcg_stop_pulse : entity work.dff_sync_edge_detect
	port map( reset => '0', 
						clkb  => cbc_clk, 
						dina  => cs_ctrl.fcg_stop, 
						doutb => fcg_stop 
	);

	-- internal veto to l1a
	process ( cbc_clk, l1a_veto )
	begin
			if  l1a_veto = '1' then
					l1a_veto_40MHz <= '1';
			elsif rising_edge(cbc_clk) then
					l1a_veto_40MHz <= '0';

			end if;
	end process;
    
  -- trigger signals are sent to cbc only when trigger_fsm is running
	cs_stat.trigger_fsm <= '1' when trigger_fsm = running else '0'; 
	process( cbc_clk, reset )
  begin
		if reset = '1' then

			trigger_fsm   <= idle;           

    elsif rising_edge( cbc_clk ) then

			case trigger_fsm is

				when idle =>

					if start_trigger = '1' then
						trigger_fsm <= running;
					end if;

				when running =>

          if stop_trigger = '1' then
						trigger_fsm <= idle;
          end if;

      end case;
		end if;
	end process; 

  -- level 1 counter at BE 
  l1a_count_o <= std_logic_vector(l1a_count);
	process( cbc_clk )
	begin
		if rising_edge(cbc_clk) then
			if com.trigger = '1' then
			 l1a_count <= l1a_count + 1;
			end if;
			if com.orbit_reset = '1' then
				l1a_count <= (others => '0');
			end if;
		end if;
	end process;

	-- all the fast signals are merged here.
	com_o <= com;

	com.fast_reset <=     
	(ext_com.fast_reset     and cs_cnfg.ext_en) 
	or (ipb_com.fast_reset  and cs_cnfg.ipbus_en) 
	or (int_com.fast_reset  and cs_cnfg.internal_en)
	or (seq_com.fast_reset  and cs_cnfg.sequencer_en); 

	com.trigger <=
	( (ext_com.trigger      and cs_cnfg.ext_en) 
		or (ipb_com.trigger   and cs_cnfg.ipbus_en) 
		or (int_com.trigger   and cs_cnfg.internal_en)
		or (seq_com.trigger   and cs_cnfg.sequencer_en) 
	)
	when trigger_fsm = running and l1a_veto_40MHz = '0' else '0';

	com.tpreq <=
	(ext_com.tpreq          and cs_cnfg.ext_en) 
	or (ipb_com.tpreq       and cs_cnfg.ipbus_en) 
	or (int_com.tpreq       and cs_cnfg.internal_en)
	or (seq_com.tpreq       and cs_cnfg.sequencer_en); 

	com.orbit_reset <=
	(ext_com.orbit_reset    and cs_cnfg.ext_en) 
	or (ipb_com.orbit_reset and cs_cnfg.ipbus_en) 
	or (int_com.orbit_reset and cs_cnfg.internal_en)
	or (seq_com.orbit_reset and cs_cnfg.sequencer_en); 
			
	-- fast command byte 
	cbc_fast_com_8bits     <= header & com.fast_reset & com.trigger & com.tpreq & com.orbit_reset & trailer;

	-- external asynchronous trigger
	l1a_tdc <= l1a_tdc_i when trigger_fsm = running else (others => '0');

	-- internal fast command generation
    fast_command_generator_inst : entity work.fast_command_generator
    port map(
        clk                      => cbc_clk,
        reset                    => fcg_reset,
        load_cnfg                => fcg_load_cnfg,            
        start                    => fcg_start,
        stop                     => fcg_stop,
        fast_reset_en_i          => cs_cnfg.fcg_fast_reset_en,
        test_pulse_en_i          => cs_cnfg.fcg_test_pulse_en,
        trigger_en_i             => cs_cnfg.fcg_trigger_en,
        orbit_reset_en_i         => cs_cnfg.fcg_orbit_reset_en,
        Ncycle_i                 => cs_cnfg.fcg_Ncycle,
        cycle_T_i                => cs_cnfg.fcg_cycle_T,
        test_pulse_t_i           => cs_cnfg.fcg_test_pulse_t,
        trigger_t_i              => cs_cnfg.fcg_trigger_t,
        orbit_reset_t_i          => cs_cnfg.fcg_orbit_reset_t,
        fast_reset_o             => int_com.fast_reset,
        trigger_o                => int_com.trigger,
        test_pulse_o             => int_com.tpreq,
        orbit_reset_o            => int_com.orbit_reset,
        fsm_o                    => cs_stat.fcg_fsm
    );    

end Behavioral;
