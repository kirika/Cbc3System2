----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.07.2017 12:46:52
-- Design Name: 
-- Module Name: rdb_controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use  work.cbc_system_package.all;

entity rdb_controller is
Port ( 
	reset_i	         	: in   std_logic;	
	cbc_clk 			: in   std_logic;	
	rd_clk				: in   std_logic;	
	trigger				: in   std_logic;	
    latency             : in   std_logic_vector(7 downto 0);
    write_block_size    : in   std_logic_vector(15 downto 0);
	cbc_par_data        : in   std_logic_vector(6*8-1 downto 0);	
	write_ready         : out  std_logic;
	read_ready_o        : out  std_logic;
    waddr_o             : out  std_logic_vector(15 downto 0);	
	ipb_clk             : in   std_logic;
	ipb_reset           : in   std_logic;
	ipb_mosi_i          : in   ipb_wbus;
	ipb_miso_o          : out  ipb_rbus
);
end rdb_controller;

architecture Behavioral of rdb_controller is

	type rdb_write_fsm_type is (write_process, waiting_read_done);

	signal write_fsm  : rdb_write_fsm_type;
	signal read_done  : boolean;
	signal nrdata     : unsigned(16 downto 0);
	signal read_ready : std_logic;
	signal reset      : std_logic;

	signal rdb_we		    : std_logic;	
	signal rdb_waddr     	: std_logic_vector(15 downto 0);	
	signal rdb_din			: std_logic_vector(63 downto 0);	
	signal rdb_raddr		: std_logic_vector(16 downto 0);	
    signal rdb_dout         : std_logic_vector(31 downto 0);

    signal read_next  : std_logic;
begin

        waddr_o <= rdb_waddr;
        read_ready_o <= read_ready;
        --===========================================--    
        -- raw data frame buffer and the control
        --===========================================--       							
        -- 64bit x 19456 deep write
        -- 32bit x 38912 deep readout
        rdb_inst : entity work.rdb_64b_60k_in_32b_120k_out
        PORT map(
                        clka 	=> cbc_clk,
                        wea(0)  => rdb_we,
                        addra   => rdb_waddr,
                        dina    => rdb_din,
                        clkb 	=> ipb_clk,	
                        addrb   => rdb_raddr,	
                        doutb   => rdb_dout
                );	


	rdb_rst : entity work.dff_sync_edge_detect
	port map( reset => '0',
              clkb  => cbc_clk,
			  dina  => reset_i,
			  doutb => reset );			

	process (cbc_clk, reset)								
		variable write_flag       : boolean;					
		variable wait_frame_count : integer range 0 to 255;			
		variable waddr_int        : integer range 0 to 60000;								
	begin								
		if reset = '1' then												
			waddr_int   := 0;
			rdb_waddr <= (others => '0');							
			read_ready  <= '0';							
			write_ready <= '1';	
			write_flag := false;
			write_fsm <= write_process;	
			wait_frame_count := 0;									
		elsif rising_edge( cbc_clk) then								

		    rdb_we <= '0';
										
			case write_fsm is
											
			when write_process  =>								
											
							
											
				if wait_frame_count /= 0 then							
					if wait_frame_count = to_integer(unsigned(latency)) then						
						write_flag       := true;					
						wait_frame_count := 0;					
					end if;						
					wait_frame_count := wait_frame_count + 1;						
				end if;							
											
				if trigger = '1'  then							
					if latency = x"00" then						
						write_flag := true;					
					else						
						wait_frame_count := 1;					
					end if;						
					write_ready <= '0';						
				end if;							
												
				if write_flag then							
					rdb_we    <= '1';						
					rdb_din   <= x"0000" & cbc_par_data;						
--					rdb_din   <= '0' & std_logic_vector(to_unsigned(waddr_int, 15)) & '0' & std_logic_vector(to_unsigned(waddr_int, 15)) & x"00000000";
					rdb_waddr <= std_logic_vector(to_unsigned(waddr_int, 16));						
					waddr_int := waddr_int + 1;						
				end if;							

				if write_flag and waddr_int = to_integer(unsigned(write_block_size)) then							
					write_flag := false;								
					write_fsm  <= waiting_read_done;										
				end if;	
			
			when waiting_read_done =>								
											
				if read_done then							
					write_ready <= '1';						
					waddr_int   := 0;		
					rdb_waddr <= (others => '0');						
					read_ready  <= '0';
					write_fsm   <= write_process;														
				else
					read_ready <= '1';		
				end if;							
			end case;
		end if;
	end process;
										
	--=============================--								
	process(reset, rd_clk)								
	--=============================--								
	begin								
		if reset='1' then     								
			rdb_raddr  <= (others => '0');								
			nrdata <= (others => '0');	
			read_done <= false;							
		elsif rising_edge(rd_clk) then	
								
			if read_next = '1' then								
				rdb_raddr <= std_logic_vector(nrdata(16 downto 0)+1);								
				nrdata <= nrdata + 1;								
				if nrdata = ( unsigned( write_block_size & '0' ) - 2 ) then							
					read_done  <= true;						
					rdb_raddr  <= (others => '0');						
					nrdata     <= (others => '0');						
				end if;							
			end if;	
			if read_ready = '0' then
				read_done <= false;	
			end if;
		end if;								
	end process;								

    ipb_fifo_read_if_inst : entity work.ipb_fifo_read_if
    port map(
                    clk					=> ipb_clk,
                    reset				=> ipb_reset,
                    ipb_mosi_i	        => ipb_mosi_i,
                    ipb_miso_o          => ipb_miso_o,
                    dout				=> rdb_dout,
                    read_next_o         => read_next
            ); 
	

end Behavioral;
