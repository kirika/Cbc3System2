----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.04.2017 16:08:04
-- Design Name: 
-- Module Name: ipb_fifo_write_if - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
--   ipbus write to a temporary dual port fifo which should interface to faster clock domain.
--   Whenever the fifo is not empty, the module read the fifo with the read clock and interfaces to the main external fifo 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;

entity ipb_fifo_write_if is
generic( addr_width : natural := 1 );
port( ipb_clk                : in  std_logic;
      ext_clk                : in  std_logic;
      reset                  : in  std_logic;
      ipb_mosi_i             : in  ipb_wbus;
      ipb_miso_o             : out ipb_rbus;    
      ext_fifo_wen           : out std_logic;
      ext_fifo_din           : out std_logic_vector(31 downto 0)
--      ext_fifo_count         : out unsigned(11 downto 0);
--      ext_fifo_addr          : out std_logic_vector(11 downto 0)
);
end ipb_fifo_write_if;

architecture Behavioral of ipb_fifo_write_if is

	signal sel					: integer range 0 to 2**addr_width-1;
	signal ack					: std_logic;

    signal fifo_din   : std_logic_vector(31 downto 0);
    signal fifo_wr_en : std_logic;
    signal fifo_rd_en : std_logic;
    signal fifo_dout  : std_logic_vector(31 downto 0);
    signal fifo_empty : std_logic;
    signal uaddr      : unsigned(11 downto 0);
    signal w_count  : unsigned(3 downto 0);       
begin


	--=============================--
--	sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
	--=============================--
	
	--=============================--
	process(reset, ipb_clk)
	--=============================--
	begin
	if reset='1' then
	    ack <= '0';
        fifo_wr_en  <= '0';
        w_count <= (others => '0');        
	elsif rising_edge(ipb_clk) then
	
	    fifo_wr_en <= '0';
		ack <= ipb_mosi_i.ipb_strobe and not ack;
		if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
            fifo_wr_en <= ipb_mosi_i.ipb_strobe and not fifo_wr_en;
            fifo_din   <= ipb_mosi_i.ipb_wdata;	
   
		end if;
           
        if fifo_wr_en = '1' then
            w_count <= w_count + 1;	     
        end if;        	 	
	end if;
	end process;

--    count <= (others => '1');

	ipb_miso_o.ipb_ack <= ack;   
	ipb_miso_o.ipb_err <= '0';     
	-- counter is reset only by ipb_rst_i
    ipb_miso_o.ipb_rdata <= "0001" & std_logic_vector(w_count) & x"000" & std_logic_vector(uaddr);

--    ipbif_to_fscbram.count <= count;

    fifo : ENTITY work.dp_fifo_32b_512d
      PORT map(
        rst => '0',
        wr_clk => ipb_clk,
        rd_clk => ext_clk,
        din    => fifo_din,
        wr_en  => fifo_wr_en,
        rd_en  => fifo_rd_en,
        dout   => fifo_dout,
        full   => open,
        empty  => fifo_empty
      );
      
      process (ext_clk, reset)
      variable timer : integer range 0 to 2;
      begin
	  if reset='1' then
        timer := 0;
        uaddr <= (others => '0');
      elsif rising_edge(ext_clk) then
          fifo_rd_en <= '0';
          ext_fifo_wen <= '0';
          if timer = 2 then
              ext_fifo_din <= fifo_dout;
              ext_fifo_wen <= '1';
--              ext_fifo_addr <= std_logic_vector(uaddr);
              uaddr <= uaddr + 1;          
              timer := 0;
          elsif timer = 0 then
              if fifo_empty = '0'  then
                  fifo_rd_en <= '1';
                  timer := timer + 1;
              end if;
          else
              timer := timer + 1;
          end if;
      end if;
      end process;
      

end Behavioral;
