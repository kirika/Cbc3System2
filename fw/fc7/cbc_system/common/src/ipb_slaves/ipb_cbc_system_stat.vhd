--===========================--
-- ipb_cbc_system_stat
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;
use work.cbc_system_config_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_stat is
    generic( ADDR_WIDTH     : natural := 8 );
    port (
	     clk                   : in  std_logic;
	     reset                 : in  std_logic;
	     ipb_mosi_i            : in  ipb_wbus;
	     ipb_miso_o            : out ipb_rbus;
	     cs_stat               : in  cs_stat_type
	 );
end ipb_cbc_system_stat;

architecture Behavioral of ipb_cbc_system_stat is

    signal regs: array_256x32bit;		
    signal reg_sigs: array_256x32bit;	

    signal sel: integer range 0 to 255;
    signal ack: std_logic;

    attribute keep: boolean;
    attribute keep of sel: signal is true;


    constant SYSTEM_ID_SEL                     : integer := 16#00#;
    constant SYSTEM_VERSION_SEL                : integer := 16#01#;
    constant SYSTEM_TYPE_SEL                   : integer := 16#02#;

    constant GLOBAL_STAT_SEL                   : integer := 16#03#;
    -- NFE          : 0x00000007 
    -- NCBC         : 0x000001f0
    -- N_ACTIVE_CBC : 0x0001f000
    -- fast command manager stat
    constant FCM_STAT_SEL                      : integer := 16#04#;
    -- FCM_FCG_FSM  : 0x00000001
    -- FCM_TRIG_FSM : 0x00000002

    constant CBC_I2C_BM_STAT_SEL_OFST          : integer := 16#05#; -- 16#05# to 16#19#
    constant CBC_I2C_BM_FLAGS_SUBSEL           : integer := 0;
    -- BUS_WAITING       : 0x00000001
    -- BUS_READY         : 0x00000002
    -- COM_FIFO_EMPTY    : 0x00000004
    -- COM_FIFO_FULL     : 0x00000008
    -- REPLY_FIFO_EMPTY  : 0x00000010
    -- REPLY_FIFO_FULL   : 0x00000020
    -- N_ACTIVE_CBC      : 0x1f000000
    constant CBC_I2C_BM_RFIFO_NDATA_SUBSEL     : integer := 1; 
    constant CBC_I2C_BM_RFIFO_NRDATA_SUBSEL    : integer := 2; 
    constant CBC_I2C_BM_STAT_NREGS             : integer := 3;

    constant DS_ACTIVE0_STAT_SEL               : integer := 16#20#; 
    constant DS_ACTIVE1_STAT_SEL               : integer := 16#21#; 
    constant DS_N_ACTIVE_STAT_SEL              : integer := 16#22#;    
    
    constant EB_FLAGS_STAT_SEL                 : integer := 16#25#;
    -- EB_WRITE_FSM                : 0x00000007 
    -- EB_PACKET_SEND_FSM          : 0x00000030
    -- EVENT_DATA_FIFO_EMPTY       : 0x00000040
    -- EVENT_DATA_FIFO_FULL        : 0x00000080
    -- EVENT_DATA_SIZE_FIFO_EMPTY  : 0x00000100
    -- EVENT_DATA_SIZE_FIFO_FULL   : 0x00000200
          
    constant DB_FLAGS_STAT_SEL                 : integer := 16#2a#;
    -- DB_WERR             : 0x00000001 
    -- DB_RERR             : 0x00000002 
    constant DB_NWORD_ALL_STAT_SEL             : integer := 16#2b#;
    constant DB_NWORD_EVENTS_STAT_SEL          : integer := 16#2c#;
    constant DB_NWORD_FREE_STAT_SEL            : integer := 16#2d#;         
    constant DB_WADDR_STAT_SEL                 : integer := 16#2e#;
    constant DB_RADDR_STAT_SEL                 : integer := 16#2f#;  

    -- RAW DATA BUFFER
    constant RDB_STAT_SEL                      : integer := 16#30#;
    -- RDB_READ_READY     : 0x00000001 
    -- RDB_WADDR          : 0xffff0000
    
    -- SEQUENCER 
    constant SEQ_STAT_SEL                      : integer := 16#37#;
    -- SEQ_FSM            : 0x00000007 
    -- SEQ_KEITHLEY_CMP_TIMER : 0x00fffff0



    -- IO
    constant IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST  : integer := 16#3b#; -- 16#3b# to 16#41# 
    -- FSM               : 0x0000000f
    -- idelay_ctrl_fsm   : 0x00000070
    -- mmcm_locked       : 0x00000100
    -- mmcm2_drp_saddr   : 0x00003000
    -- mmcme2_drp_srdy   : 0x00004000
    -- bram_nrdata       : 0x00ff8000
    -- BITSLIP_COUNTER   : 0x07000000
    -- pulse_scan_done   : 0x10000000
    -- 
    constant IO_CBC_DATA_RCV_ISERDES_STAT_SEL_OFST      : integer := 16#42#; -- 16#42# to 16#51# 
    -- DELAY_LOCKED      : 0x00000001
    -- IN_DELAY_TAP_OUT  : 0x0000003e	
    -- SLVS5             : 0x0000ff00
    constant IO_CBC_DATA_RCV_ISERDES_CND_BRAM_STAT_SEL_OFST      : integer := 16#52#;
    -- bram_nrdata       : 0x000001ff 
    
    constant CBC_DP_STAT_SEL_OFST                  : integer := 16#5a#; -- 16#5a# to 16#89#
    constant CBC_DP_FLAGS_SUBSEL                   : integer := 0;
    -- CBC_DP_DATA_FIFO_EMPTY      : 0x00000001
    -- CBC_DP_DATA_FIFO_FULL       : 0x00000002
    -- CBC_DP_DATA_INFO_FIFO_EMPTY : 0x00000004
    -- CBC_DP_DATA_INFO_FIFO_FULL  : 0x00000008 
    -- CBC_DP_TRIG_DATA_FIFO_EMPTY : 0x00000010
    -- CBC_DP_TRIG_DATA_FIFO_FULL  : 0x00000020     
    constant CBC_DP_FRAME_COUNTER_SUBSEL           : integer := 1;
    constant CBC_DP_L1A_COUNTER_SUBSEL             : integer := 2;
    constant CBC_DP_STAT_NREGS                     : integer := 3; 

    --	constant CISC_SEL                                   : integer := 16#80#;
    --	constant CISC_STAT_SUBSEL                           : integer := 0;
    --	constant CISC_CBC_I2C_ERROR_BIT                     : integer := 0;
    --	constant CISC_NWORD_EBRAM_SUBSEL                    : integer := 1;
    --	constant CISC_NWORD_EBRAM_OFFSET                    : integer := 0;
    --	constant CISC_NWORD_EBRAM_WIDTH                     : integer := 16;
    --	constant CISC_DBRAM_NDATA_SUBSEL                    : integer := 2;
    --	constant CISC_DBRAM_NDATA_OFFSET                    : integer := 0;
    --	constant CISC_DBRAM_NDATA_WIDTH                     : integer := 9;   
    --	constant CISC_FSM_SUBSEL                            : integer := 3;
    --	constant CISC_FSM_OFFSET                            : integer := 0;
    --	constant CISC_FSM_WIDTH                             : integer := 2; 

    signal nword_events_1d                    :  std_logic_vector(31 downto 0); 
    signal nword_events_ipb                   :  std_logic_vector(31 downto 0);    
    signal nword_all_1d                       :  std_logic_vector(31 downto 0); 
    signal nword_all_ipb                      :  std_logic_vector(31 downto 0);        
    signal nword_free_1d                      :  std_logic_vector(31 downto 0); 
    signal nword_free_ipb                     :  std_logic_vector(31 downto 0); 
    
begin

	-- counter synchronization
  --=============================--
  process(reset, clk)
  --=============================--
   begin
       if rising_edge(clk) then

	   nword_all_1d    <= cs_stat.db_n_word_all;
	   nword_events_1d <= cs_stat.db_n_word_events;
	   nword_free_1d   <= cs_stat.db_n_word_free;

	   if nword_events_1d = cs_stat.db_n_word_events then
	       nword_events_ipb <= nword_events_1d;
	   end if;        

	   if nword_all_1d = cs_stat.db_n_word_all then
	       nword_all_ipb <= nword_all_1d;
	   end if;  

	   if nword_free_1d = cs_stat.db_n_word_free then
	       nword_free_ipb <= nword_free_1d;
	   end if;            

       end if;        
  end process;
     
  --=============================--
  -- regs mapping
  --=============================--
  reg_sigs(SYSTEM_ID_SEL)                                    <= usr_id_0;                        -- usr_id        = 'C' 'B' 'C' '3'      
  reg_sigs(SYSTEM_VERSION_SEL)                               <= firmware_id;
  reg_sigs(SYSTEM_TYPE_SEL)(2 downto 0)                      <= firmware_type;

  reg_sigs(GLOBAL_STAT_SEL)(FEID_WIDTH-1 downto 0)      <= std_logic_vector(to_unsigned(NFE,FEID_WIDTH));
  reg_sigs(GLOBAL_STAT_SEL)(CBCID_WIDTH-1+4 downto 4)   <= std_logic_vector(to_unsigned(NCBC,CBCID_WIDTH));
  reg_sigs(GLOBAL_STAT_SEL)(CBCID_WIDTH-1+12 downto 12) <= cs_stat.n_active_cbc;  
  

  reg_sigs(FCM_STAT_SEL)(0) <= cs_stat.fcm.fcg_fsm;
  reg_sigs(FCM_STAT_SEL)(1) <= cs_stat.fcm.trigger_fsm; 

  cbc_i2c_bm_stat_gen :
  for i in 0 to MAX_N_FE-1 generate
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(0)    <= cs_stat.cbc_i2c_bm_array(i).bus_waiting;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(1)    <= cs_stat.cbc_i2c_bm_array(i).bus_ready;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(2)    <= cs_stat.cbc_i2c_bm_array(i).com_fifo_empty;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(3)    <= cs_stat.cbc_i2c_bm_array(i).com_fifo_full;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(4)    <= cs_stat.cbc_i2c_bm_array(i).reply_fifo_empty;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(5)               <= cs_stat.cbc_i2c_bm_array(i).reply_fifo_full;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_FLAGS_SUBSEL)(28 downto 24)    <= cs_stat.cbc_i2c_bm_array(i).n_active_cbcs;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_RFIFO_NDATA_SUBSEL) <= cs_stat.cbc_i2c_bm_array(i).reply_fifo_ndata;
      reg_sigs(CBC_I2C_BM_STAT_SEL_OFST + i * CBC_I2C_BM_STAT_NREGS + CBC_I2C_BM_RFIFO_NRDATA_SUBSEL)<= cs_stat.cbc_i2c_bm_array(i).reply_fifo_nrdata;
  end generate;

  reg_sigs(EB_FLAGS_STAT_SEL)(2 downto 0) <= cs_stat.eb.write_fsm;
  reg_sigs(EB_FLAGS_STAT_SEL)(5 downto 4) <= cs_stat.eb.packet_send_fsm;
  reg_sigs(EB_FLAGS_STAT_SEL)(6) <= cs_stat.eb.event_data_fifo_empty;
  reg_sigs(EB_FLAGS_STAT_SEL)(7) <= cs_stat.eb.event_data_fifo_full;
  reg_sigs(EB_FLAGS_STAT_SEL)(8) <= cs_stat.eb.event_data_size_fifo_empty;
  reg_sigs(EB_FLAGS_STAT_SEL)(9) <= cs_stat.eb.event_data_size_fifo_full;
      
  reg_sigs(DB_FLAGS_STAT_SEL)(0)    <= cs_stat.db_werr;
  reg_sigs(DB_FLAGS_STAT_SEL)(1)    <= cs_stat.db_rerr;
  reg_sigs(DB_WADDR_STAT_SEL)       <= cs_stat.db_waddr;
  reg_sigs(DB_RADDR_STAT_SEL)       <= cs_stat.db_raddr;
 
  reg_sigs(RDB_STAT_SEL)(0)            <= cs_stat.rdb_read_ready;
  reg_sigs(RDB_STAT_SEL)(31 downto 16) <= cs_stat.rdb_waddr;
							     			  
  reg_sigs(SEQ_STAT_SEL)(2 downto   0) <= cs_stat.seq_fsm;  
--  reg_sigs(SEQ_STAT_SEL)(15 downto  4) <= cs_stat.seq_dcount;
  reg_sigs(SEQ_STAT_SEL)(23 downto 4) <= cs_stat.seq_keithley_cmp_timer;
 
  reg_sigs(DS_ACTIVE0_STAT_SEL) <= cs_stat.ds_active(31 downto 0);
  reg_sigs(DS_ACTIVE1_STAT_SEL)(MAX_N_DATA_SECTION-33 downto 0) <= cs_stat.ds_active(MAX_N_DATA_SECTION-1 downto 32); 
  reg_sigs(DS_N_ACTIVE_STAT_SEL)(DSID_WIDTH - 1 downto 0 ) <= cs_stat.n_active_ds(DSID_WIDTH - 1 downto 0 );
-- reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST) <= cs_stat.io.cbc_data_rcv.bram_nrdata;
  
  io_cbc_data_rcv_iserdes_cnd_stat_gen :
  for i in 0 to MAX_N_FE-1 generate
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(3 downto 0)   <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).fsm;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(6 downto 4)   <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).idelay_ctrl_fsm;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(8)            <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).mmcm_locked; 
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(13 downto 12) <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).mmcme2_drp_saddr;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(14)           <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).mmcme2_drp_srdy;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(26 downto 24) <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).bitslip_counter;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_STAT_SEL_OFST + i)(28)           <= cs_stat.io.cbc_data_rcv.iserdes_cnd_fe_array(i).pulse_scan_done;
  end generate;

  io_cbc_data_rcv_iserdes_stat_gen :  
  for i in 0 to MAX_N_CBC-1 generate                       
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_STAT_SEL_OFST + i)(0)            <= cs_stat.io.cbc_data_rcv.iserdes_cbc_array(i).delay_locked;
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_STAT_SEL_OFST + i)(5 downto 1)   <= cs_stat.io.cbc_data_rcv.iserdes_cbc_array(i).in_delay_tap_out(4 downto 0);
      reg_sigs(IO_CBC_DATA_RCV_ISERDES_STAT_SEL_OFST + i)(15 downto 8)  <= cs_stat.io.cbc_data_rcv.iserdes_cbc_array(i).slvs5;
  end generate;

  reg_sigs(IO_CBC_DATA_RCV_ISERDES_CND_BRAM_STAT_SEL_OFST)(9 downto 0) <= cs_stat.io.cbc_data_rcv.iserdes_cnd_bram_nrdata;

  cbc_dp_sig_gen :
  for i in 0 to MAX_N_CBC-1 generate
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(0) <= cs_stat.cbc_dp_array(i).data_fifo_empty;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(1) <= cs_stat.cbc_dp_array(i).data_fifo_full;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(2) <= cs_stat.cbc_dp_array(i).data_info_fifo_empty;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(3) <= cs_stat.cbc_dp_array(i).data_info_fifo_full;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(4) <= cs_stat.cbc_dp_array(i).trig_data_fifo_empty;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FLAGS_SUBSEL)(5) <= cs_stat.cbc_dp_array(i).trig_data_fifo_full;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_FRAME_COUNTER_SUBSEL) <= cs_stat.cbc_dp_array(i).cbc_data_frame_counter;
      reg_sigs(CBC_DP_STAT_SEL_OFST + i * CBC_DP_STAT_NREGS + CBC_DP_L1A_COUNTER_SUBSEL) <= cs_stat.cbc_dp_array(i).l1a_counter;
  end generate;

  --=============================--
  sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
  --=============================--
  
  --=============================--
  process(reset, clk)
  --=============================--
  variable index : integer range 0 to 8;
  begin
  if reset='1' then
		ack      <= '0';
		index := 0;
  elsif rising_edge(clk) then
       
    ipb_miso_o.ipb_rdata <= (others => '0');
    -- read 
    if sel = DB_NWORD_EVENTS_STAT_SEL then
        ipb_miso_o.ipb_rdata <=  nword_events_ipb;       
    elsif sel = DB_NWORD_ALL_STAT_SEL then
        ipb_miso_o.ipb_rdata <=  nword_all_ipb;
    elsif sel = DB_NWORD_FREE_STAT_SEL then               
       ipb_miso_o.ipb_rdata  <=   nword_free_ipb;
    else
        ipb_miso_o.ipb_rdata <= regs(sel);
    end if;

    -- ack        
    ack <= ipb_mosi_i.ipb_strobe and not ack;
  
    regs <= reg_sigs;      

  end if;
  end process;
  
  ipb_miso_o.ipb_ack <= ack;
  ipb_miso_o.ipb_err <= '0';
    
end Behavioral;
