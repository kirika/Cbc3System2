--===========================--
-- ipb_cbc_system
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.user_package.all;
use work.ipbus.all;
use work.cbc_system_package.all;
use work.system_package.all;

entity ipb_cbc_system is
port 
(
    ipb_rst_i              : in  std_logic;
    ipb_clk                : in  std_logic;
    ipb_mosi_i             : in  ipb_wbus_array(0 to nbr_usr_slaves-1);
    ipb_miso_o             : out ipb_rbus_array(0 to nbr_usr_slaves-1);
    cs_cnfg                : out cs_cnfg_type;
    cs_ctrl                : out cs_ctrl_type;
    cs_stat                : in  cs_stat_type
);    
end ipb_cbc_system;

architecture Behavioral of ipb_cbc_system is


begin
   
     
	--===========================================--
  ipb_cnfg_cs_inst: entity work.ipb_cbc_system_cnfg
  --===========================================--
  port map
  (
		clk                   => ipb_clk,
		reset                 => ipb_rst_i,
		ipb_mosi_i            => ipb_mosi_i(cs_ipb_cnfg_sel),
		ipb_miso_o            => ipb_miso_o(cs_ipb_cnfg_sel),
		cs_cnfg               => cs_cnfg
	);
  --===========================================--
  ipb_ctrl_cs_inst: entity work.ipb_cbc_system_ctrl
  --===========================================--
  port map
  (
      clk                   => ipb_clk,
      reset                 => ipb_rst_i,
      ipb_mosi_i            => ipb_mosi_i(cs_ipb_ctrl_sel),
      ipb_miso_o            => ipb_miso_o(cs_ipb_ctrl_sel),
      cs_ctrl               => cs_ctrl
  );
  --===========================================--
  ipb_stat_cs_inst: entity work.ipb_cbc_system_stat
  --===========================================--
  port map
  (
      clk                   => ipb_clk,
      reset                 => ipb_rst_i,
      ipb_mosi_i            => ipb_mosi_i(cs_ipb_stat_sel),
      ipb_miso_o            => ipb_miso_o(cs_ipb_stat_sel),
      cs_stat               => cs_stat
  );

 
  

--	ipb_rdb_inst : entity work.ipb_rdb
--	port map(	ipb_clk 							=> ipb_clk,
--                reset								=> ipb_rst_i,
--                ipb_mosi_i							=> ipb_mosi_i(ipb_rdb_sel),
--                ipb_miso_o							=> ipb_miso_o(ipb_rdb_sel),
--                dout                                => rdb_dout,
--                read_next_o                         => rdb_read_next         
--	);	
--
--    ipb_fscbram_fifo_inst : entity work.ipb_fscbram
--    port map( ipb_clk                => ipb_clk,
--              cbc_ctrl_clk           => clk_40MHz_cbc_ctrl,
--              reset                  => ipb_rst_i,
--              ipb_mosi_i             => ipb_mosi_i(ipb_cfscbram_sel),
--              ipb_miso_o             => ipb_miso_o(ipb_cfscbram_sel),
--              ipbif_to_fscbram       => ipbif_to_fscbram
--    );
--    
--    ipb_cisc_dbram_if_inst : entity work.ipb_bram_write_if
--    port map( ipb_clk                => ipb_clk,
--              rd_clk                 => clk_40MHz_cbc_ctrl,
--              reset                  => ipb_rst_i,
--              ipb_mosi_i             => ipb_mosi_i(ipb_cisc_dbram_sel),
--              ipb_miso_o             => ipb_miso_o(ipb_cisc_dbram_sel),
--              bram_wen               => cisc_dbram_wen,
--              bram_din               => cisc_dbram_din
--    );        
--    
--	ipb_cisc_ebram_if_inst : entity work.ipb_bram_read_if
--    port map(	ipb_clk 							=> ipb_clk,
--                reset								=> ipb_rst_i,
--                ipb_mosi_i							=> ipb_mosi_i(ipb_cisc_ebram_sel),
--                ipb_miso_o							=> ipb_miso_o(ipb_cisc_ebram_sel),
--                dout                                => cisc_ebram_dout,
--                read_next_o                         => cisc_ebram_read_next         
--    );	    
    
end Behavioral;
