--===========================--
-- ipb_cbc_system_ctrl
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_ctrl is
generic( ADDR_WIDTH     : natural := 8 );
port (
        clk            : in  std_logic;
        reset          : in  std_logic;
        ipb_mosi_i     : in  ipb_wbus;
        ipb_miso_o     : out ipb_rbus;
        cs_ctrl        : out cs_ctrl_type
        );
end ipb_cbc_system_ctrl;

architecture Behavioral of ipb_cbc_system_ctrl is

    signal regs: array_256x32bit;  
    
    signal sel: integer range 0 to 255;
    signal ack: std_logic;
    
    attribute keep: boolean;
    attribute keep of sel: signal is true;
    
    -- global control
    constant GLOBAL_CTRL_SEL                     : integer := 16#00#;
    constant GLOBAL_RESET_BIT                    : integer := 0;
    constant GLOBAL_DAQ_RESET_BIT                : integer := 1;
    constant GLOBAL_CBC_HARD_RESET_BIT           : integer := 16;
    -- cbc i2c transaction control
    constant CBC_I2C_BM_SEL_OFST                 : integer := 16#02#;--N_MAX_FE from 0x02 to 0x08
    constant CBC_I2C_BM_RESET_BIT                : integer := 0;
    constant CBC_I2C_BM_INIT_BIT                 : integer := 1;
    constant CBC_I2C_BM_RESET_FIFOS_BIT          : integer := 2;
		-- daq control
    constant DAQ_CTRL_SEL                        : integer := 16#10#;
    constant DAQ_DS_RESET_BIT                    : integer := 0;
    constant DAQ_EB_RESET_BIT                    : integer := 1;
    constant DAQ_DB_RESET_BIT                    : integer := 2;
    constant DAQ_DB_TRIG_READALL_BIT             : integer := 3;
    constant DAQ_RDB_RESET_BIT                   : integer := 4;
		-- data section control
		constant DS_CTRL_SEL_OFST                    : integer := 16#11#;-- N_MAX_DATA_SECTION from 0x11 to 0x34 
    constant DS_RESET_BIT                        : integer := 0;
    constant DS_FRAME_COUNTER_RESET_BIT          : integer := 1;
    -- fast command manger control
    constant FCM_CTRL_SEL                        : integer := 16#40#;
    constant FCM_RESET_BIT                       : integer := 0;
    constant FCM_START_TRIGGER_BIT               : integer := 1;
    constant FCM_STOP_TRIGGER_BIT                : integer := 2;
    constant FCM_FAST_RESET_BIT                  : integer := 16;
    constant FCM_TEST_PULSE_REQ_BIT              : integer := 17;
    constant FCM_TRIGGER_BIT                     : integer := 18;
    constant FCM_ORBIT_RESET_BIT                 : integer := 19;
    constant FCM_FCG_RESET_BIT                   : integer := 20;
    constant FCM_FCG_LOAD_CNFG_BIT               : integer := 21;
    constant FCM_FCG_START_BIT                   : integer := 22;
    constant FCM_FCG_STOP_BIT                    : integer := 23;


    -- io control 
    constant CBC_DATA_IO_CTRL_SEL                          : integer := 16#43#;
    constant CBC_DATA_IO_FC_OSERDES_RESET_BIT              : integer := 0;    
    constant CBC_DATA_IO_ISERDES_CND_TUNING_RESET_BIT      : integer := 1;
    constant CBC_DATA_IO_ISERDES_CND_TUNING_TUNE_BIT       : integer := 2;
    constant CBC_DATA_IO_ISERDES_CND_TUNING_SCAN_PULSE_BIT : integer := 3;
	constant CBC_DATA_IO_TIMING_SCAN_BRAM_RESET_BIT        : integer := 4;
	constant CBC_DATA_IO_TIMING_SCAN_BRAM_RD_RESET_BIT     : integer := 5;	

	constant CBC_DATA_IO_ISERDES_CND_CTRL_SEL_OFST         : integer := 16#44#;--N_MAX_FE from 0x44 to 0x4a
    
    -- sequencer control
    constant SEQ_CTRL_SEL                        : integer := 16#50#;
    constant SEQ_RESET_BIT                       : integer := 0;
    constant SEQ_START_BIT                       : integer := 1;
    constant SEQ_RESUME_BIT                      : integer := 2;    
    constant RDB_SEL                             : integer := 16#51#; 
    constant RDB_RESET_BIT                       : integer := 0;
    
    -- cbc i2c seu monitor 
    constant SEU_MON_CTRL_SEL                    : integer := 16#52#;
    constant SEU_MON_RESET_BIT                   : integer := 0;
    constant SEU_MON_START_BIT                   : integer := 1;
    constant SEU_MON_PAUSE_BIT                   : integer := 2;
    constant SEU_MON_RESUME_BIT                  : integer := 3;
    constant SEU_MON_STOP_BIT                    : integer := 4;
    constant SEU_MON_RESET_EBRAM_RADDR_BIT       : integer := 5;

    -- wafer test control signals
    --fuse prog pulse
    constant WT_CTRL_SEL                             : integer := 16#60#;
    constant FPP_BIT                                 : integer := 0;
    
    constant WAIT_FOR_MASK                         : integer := 50;
    
    signal ipb_global_ctrl                            : std_logic_vector(31 downto 0);
    signal ipb_cbc_i2c_bm_ctrl                        : std_logic_vector(MAX_N_FE*32 - 1 downto 0);
    signal ipb_daq_ctrl                               : std_logic_vector(31 downto 0);
	signal ipb_ds_ctrl                                : std_logic_vector(MAX_N_DATA_SECTION * 32 - 1 downto 0);
    signal ipb_fcm_ctrl                               : std_logic_vector(31 downto 0);
    signal ipb_seq_ctrl                               : std_logic_vector(31 downto 0);
    signal ipb_rdb_ctrl                               : std_logic_vector(31 downto 0);
    signal ipb_seu_mon_ctrl                           : std_logic_vector(31 downto 0);
    signal ipb_cbc_data_io_ctrl                       : std_logic_vector(31 downto 0);
    signal ipb_cbc_data_io_iserdes_cnd_ctrl           : std_logic_vector(MAX_N_FE * 32 - 1 downto 0);
    signal ipb_wt_ctrl                                : std_logic_vector(31 downto 0);
begin

    --=============================--
    sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
    --=============================--
 
    --=============================--
    process(reset, clk)
    --=============================--
    variable timer : natural;
    variable wsel  : integer range 0 to 255;
    begin
    if reset='1' then

      regs   <= (others=> (others=>'0'));
      ack    <= '0';

      timer    := 0;
      wsel     := 0;

      ipb_global_ctrl                    <= (others => '0'); 
      ipb_cbc_i2c_bm_ctrl                <= (others => '0'); 
	  ipb_daq_ctrl                       <= (others => '0');
	  ipb_ds_ctrl                        <= (others => '0');
      ipb_fcm_ctrl                       <= (others => '0'); 
      ipb_seq_ctrl                       <= (others => '0'); 
      ipb_rdb_ctrl                       <= (others => '0');
      ipb_seu_mon_ctrl                   <= (others => '0'); 
      ipb_cbc_data_io_ctrl               <= (others => '0'); 
      ipb_cbc_data_io_iserdes_cnd_ctrl   <= (others => '0'); 
      ipb_wt_ctrl                        <= (others => '0'); 
    elsif rising_edge(clk) then
 
      ipb_global_ctrl                    <= (others => '0'); 
      ipb_cbc_i2c_bm_ctrl                <= (others => '0'); 
	  ipb_daq_ctrl                       <= (others => '0');
	  ipb_ds_ctrl                        <= (others => '0');
      ipb_fcm_ctrl                       <= (others => '0'); 
      ipb_seq_ctrl                       <= (others => '0');
      ipb_rdb_ctrl                       <= (others => '0');  
      ipb_seu_mon_ctrl                   <= (others => '0'); 
      ipb_cbc_data_io_ctrl               <= (others => '0'); 
      ipb_cbc_data_io_iserdes_cnd_ctrl   <= (others => '0'); 
      ipb_wt_ctrl                        <= (others => '0'); 
      -- write
      if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
        regs(sel) <= ipb_mosi_i.ipb_wdata;
        timer :=   1;
        wsel  := sel;
      end if;
      
      if timer /= 0 then
        if timer = WAIT_FOR_MASK then
            
            timer := 0;
        
            if wsel = GLOBAL_CTRL_SEL   then
            	ipb_global_ctrl        <= regs(wsel);
            elsif wsel >= CBC_I2C_BM_SEL_OFST and wsel < CBC_I2C_BM_SEL_OFST + MAX_N_FE then
              ipb_cbc_i2c_bm_ctrl((wsel - CBC_I2C_BM_SEL_OFST+1)*32-1 downto (wsel - CBC_I2C_BM_SEL_OFST)*32) <= regs(wsel);
            elsif wsel = DAQ_CTRL_SEL   then
              ipb_daq_ctrl           <= regs(wsel);
            elsif wsel >= DS_CTRL_SEL_OFST and wsel < DS_CTRL_SEL_OFST + MAX_N_DATA_SECTION then
              ipb_ds_ctrl((wsel - DS_CTRL_SEL_OFST+1)*32-1 downto (wsel - DS_CTRL_SEL_OFST)*32) <= regs(wsel);
            elsif wsel = FCM_CTRL_SEL then
              ipb_fcm_ctrl           <= regs(wsel);
            elsif wsel = SEQ_CTRL_SEL then
              ipb_seq_ctrl           <= regs(wsel);
            elsif wsel = RDB_SEL then
              ipb_rdb_ctrl           <= regs(wsel);
            elsif wsel = SEU_MON_CTRL_SEL then
			  ipb_seu_mon_ctrl       <= regs(wsel);	
            elsif wsel = CBC_DATA_IO_CTRL_SEL then
			  ipb_cbc_data_io_ctrl   <= regs(wsel);
            elsif wsel >= CBC_DATA_IO_ISERDES_CND_CTRL_SEL_OFST and wsel < CBC_DATA_IO_ISERDES_CND_CTRL_SEL_OFST + MAX_N_FE then
              ipb_cbc_data_io_iserdes_cnd_ctrl((wsel - CBC_DATA_IO_ISERDES_CND_CTRL_SEL_OFST+1)*32-1 downto (wsel - CBC_DATA_IO_ISERDES_CND_CTRL_SEL_OFST) * 32) <= regs(wsel);
            elsif wsel = WT_CTRL_SEL then
                ipb_wt_ctrl <= regs(wsel);
            end if;
        
            regs(wsel) <= (others => '0');
        
        else
            timer := timer + 1;        
        end if;    
      end if;
    
    -- read 
    ipb_miso_o.ipb_rdata <= regs(sel);
    -- ack
    ack <= ipb_mosi_i.ipb_strobe and not ack;
    
    end if;
    end process;

    ipb_miso_o.ipb_ack <= ack;
    ipb_miso_o.ipb_err <= '0';

    -- hard_reset
	cs_ctrl.io.cbc_hard_reset <= ipb_global_ctrl(GLOBAL_CBC_HARD_RESET_BIT);
  
  --=================--
  -- i2c bus manager signals 
  --=================--               
  cibm_sig_gen :
  for i in 0 to MAX_N_FE-1 generate
    cs_ctrl.cbc_i2c_bm_array(i).reset <= ipb_cbc_i2c_bm_ctrl(i * 32 + CBC_I2C_BM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_CBC_HARD_RESET_BIT);
	cs_ctrl.cbc_i2c_bm_array(i).init  <= ipb_cbc_i2c_bm_ctrl(i * 32 + CBC_I2C_BM_INIT_BIT);
	cs_ctrl.cbc_i2c_bm_array(i).reset_fifos <= ipb_cbc_i2c_bm_ctrl(i * 32 + CBC_I2C_BM_RESET_FIFOS_BIT) or ipb_cbc_i2c_bm_ctrl(i * 32 + CBC_I2C_BM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_CBC_HARD_RESET_BIT);
  end generate;

  --=================--
  -- DAQ control signals
  --=================--
  cs_ctrl.daq_reset <= ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_DAQ_RESET_BIT);
  cdp_sig_gen :
  for i in 0 to MAX_N_CBC-1 generate
		cs_ctrl.cbc_dp_array(i).reset <= ipb_ds_ctrl(i*32+DS_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_DAQ_RESET_BIT) or ipb_daq_ctrl(DAQ_DS_RESET_BIT);
		cs_ctrl.cbc_dp_array(i).frame_counter_reset <= ipb_ds_ctrl(i*32+DS_FRAME_COUNTER_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_DAQ_RESET_BIT);
  end generate;

	cs_ctrl.eb_reset   <= ipb_daq_ctrl(DAQ_EB_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_DAQ_RESET_BIT);
	cs_ctrl.db_reset   <= ipb_daq_ctrl(DAQ_DB_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT) or ipb_global_ctrl(GLOBAL_DAQ_RESET_BIT);
	cs_ctrl.db_readall <= ipb_daq_ctrl(DAQ_DB_TRIG_READALL_BIT);

  --=================--
  -- fast command manager control signals
  --=================--               
	cs_ctrl.fcm.reset                <= ipb_fcm_ctrl(FCM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
    cs_ctrl.fcm.start_trigger        <= ipb_fcm_ctrl(FCM_START_TRIGGER_BIT);
    cs_ctrl.fcm.stop_trigger        <= ipb_fcm_ctrl(FCM_STOP_TRIGGER_BIT);
    cs_ctrl.fcm.fast_com.fast_reset  <= ipb_fcm_ctrl(FCM_FAST_RESET_BIT);
	cs_ctrl.fcm.fast_com.tpreq       <= ipb_fcm_ctrl(FCM_TEST_PULSE_REQ_BIT); 
	cs_ctrl.fcm.fast_com.trigger     <= ipb_fcm_ctrl(FCM_TRIGGER_BIT); 
	cs_ctrl.fcm.fast_com.orbit_reset <= ipb_fcm_ctrl(FCM_ORBIT_RESET_BIT); 
  --=================--
  -- fast command generator control signals
  --=================--               
	cs_ctrl.fcm.fcg_reset     <= ipb_fcm_ctrl(FCM_FCG_RESET_BIT) or ipb_fcm_ctrl(FCM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
	cs_ctrl.fcm.fcg_load_cnfg <= ipb_fcm_ctrl(FCM_FCG_LOAD_CNFG_BIT);
	cs_ctrl.fcm.fcg_start     <= ipb_fcm_ctrl(FCM_FCG_START_BIT);
	cs_ctrl.fcm.fcg_stop      <= ipb_fcm_ctrl(FCM_FCG_STOP_BIT);


    cs_ctrl.seq_reset <= ipb_seq_ctrl(SEQ_RESET_BIT);
    cs_ctrl.seq_start <= ipb_seq_ctrl(SEQ_START_BIT);
    cs_ctrl.seq_resume <= ipb_seq_ctrl(SEQ_RESUME_BIT);
  --=================--
  -- raw data buffer control signals
  --=================--
	cs_ctrl.rdb_reset <= ipb_rdb_ctrl(RDB_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);

  --=================--
  -- cbc i2c seu checker control signals
  --=================--
--	cs_ctrl.cisc_reset            <= ipb_cisc(SEU_MON_RESET_BIT);
--	cs_ctrl.cisc_start            <= ipb_cisc(SEU_MON_START_BIT);		  		  		  		  		  		  		  			  
--	cs_ctrl.cisc_pause            <= ipb_cisc(SEU_MON_PAUSE_BIT);
--	cs_ctrl.cisc_resume           <= ipb_cisc(SEU_MON_RESUME_BIT);
--	cs_ctrl.cisc_stop             <= ipb_cisc(SEU_MON_STOP_BIT);
--	cs_ctrl.cisc_reset_ebram_addr <= ipb_cisc(SEU_MON_RESET_EBRAM_RADDR_BIT);

  --=================--
  -- io serdes & data clock timing tuning control signals
  --=================--    
	oserdes_reset_gen:
  for i in 0 to MAX_N_FE-1 generate       
		cs_ctrl.io.fc_to_cbc_oserdes_reset(i) <= ipb_cbc_data_io_ctrl(CBC_DATA_IO_FC_OSERDES_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
	end generate;

	-- cbc data receiver ctrl 
	cs_ctrl.io.cbc_data_rcv.timing_scan_bram_reset <= ipb_cbc_data_io_ctrl(CBC_DATA_IO_TIMING_SCAN_BRAM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
	cs_ctrl.io.cbc_data_rcv.timing_scan_bram_rd_reset <= ipb_cbc_data_io_ctrl(CBC_DATA_IO_TIMING_SCAN_BRAM_RD_RESET_BIT) or ipb_cbc_data_io_ctrl(CBC_DATA_IO_TIMING_SCAN_BRAM_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
  dctt_ctrl_gen :
  for i in 0 to MAX_N_FE-1 generate       
		cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(i).reset      <= ipb_cbc_data_io_iserdes_cnd_ctrl(i*32+CBC_DATA_IO_ISERDES_CND_TUNING_RESET_BIT) or ipb_cbc_data_io_ctrl(CBC_DATA_IO_ISERDES_CND_TUNING_RESET_BIT) or ipb_global_ctrl(GLOBAL_RESET_BIT);
		cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(i).tune       <= ipb_cbc_data_io_iserdes_cnd_ctrl(i*32+CBC_DATA_IO_ISERDES_CND_TUNING_TUNE_BIT) or ipb_cbc_data_io_ctrl(CBC_DATA_IO_ISERDES_CND_TUNING_TUNE_BIT);
		cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(i).scan_pulse <= ipb_cbc_data_io_iserdes_cnd_ctrl(i*32+CBC_DATA_IO_ISERDES_CND_TUNING_SCAN_PULSE_BIT) or ipb_cbc_data_io_ctrl(CBC_DATA_IO_ISERDES_CND_TUNING_SCAN_PULSE_BIT);
  end generate;    

    cs_ctrl.wt_fpp <= ipb_wt_ctrl(0);

end Behavioral;

