--===========================--
-- ipb_cbc_i2c_regs 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;

use work.cbc_system_config_package.all;
use work.cbc_system_package.all;
entity ipb_cbc_i2c_regs is
generic( addr_width : natural := 4 );
Port (  ipb_clk                      : in  STD_LOGIC;
        cbc_ctrl_clk                 : in  std_logic;
        reset                        : in  std_logic;
        ipb_mosi_i                   : in  ipb_wbus;
        ipb_miso_o                   : out ipb_rbus;
				reply_fifo_reset             : in  std_logic_vector(NFE - 1 downto 0);  
				reply_fifo_read_next         : out std_logic_vector(NFE - 1 downto 0); 
				reply_fifo_empty             : in  std_logic_vector(NFE - 1 downto 0); 
				reply_fifo_dout              : in  std_logic_vector(32 * NFE-1 downto 0); 
				com_fifo_we                  : out std_logic_vector(NFE - 1 downto 0); 
				com_fifo_din                 : out std_logic_vector(32 * NFE-1 downto 0) 
);

end ipb_cbc_i2c_regs;

architecture Behavioral of ipb_cbc_i2c_regs is

    constant COMMAND_FIFO_SEL   : integer := 16#00#;
    constant REPLY_FIFO_SEL     : integer := 16#01#;

    constant I2C_BROADCAST_FEID : std_logic_vector(FEID_WIDTH-1 downto 0)  := (others => '0');
    
    signal fe_id                : std_logic_vector(FEID_WIDTH-1 downto 0);
    signal fe_index             : integer range 0 to 2**FEID_WIDTH;
	
	signal read_next            : std_logic_vector(NFE-1 downto 0);
	signal sel					: integer range 0 to 2**addr_width-1;
	signal ack					: std_logic;
    signal read_ack             : std_logic;
        
    type command_fifo_signals_type is
    record
        din      : std_logic_vector(31 downto 0);
        dout     : std_logic_vector(31 downto 0);
        wr_en    : std_logic;
        rd_en    : std_logic;
        empty    : std_logic;
    end record;
    type command_fifo_signals_set_type is array ( 0 to NFE-1 ) of command_fifo_signals_type;
    signal command_fifo_signals_set : command_fifo_signals_set_type;
    
begin

	--=============================--
	sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
	--=============================--
	
	--=============================--
	process(reset, ipb_clk)
	--=============================--
	begin
	if reset='1' then
	    ack <= '0';
	    for i in 0 to NFE - 1 loop
            command_fifo_signals_set(i).wr_en  <= '0';
        end loop;
        read_ack <= '0';
--        read_next <= (others => '0');
	elsif rising_edge(ipb_clk) then
	    for i in 0 to NFE - 1 loop
            command_fifo_signals_set(i).wr_en  <= '0';
            command_fifo_signals_set(i).din <= (others => '1');
        end loop;
    --        read_next <= (others => '0');
 		ack <= ipb_mosi_i.ipb_strobe and not ack;

		if ipb_mosi_i.ipb_strobe='1' then
            if ipb_mosi_i.ipb_write='1' and sel = COMMAND_FIFO_SEL then
                if fe_id = I2C_BROADCAST_FEID then
                    for i in 0 to NFE - 1 loop
                        command_fifo_signals_set(i).wr_en <= ipb_mosi_i.ipb_strobe and not command_fifo_signals_set(i).wr_en;
                        command_fifo_signals_set(i).din(CBCID_WIDTH+18 downto 19) <= ipb_mosi_i.ipb_wdata(CBCID_WIDTH+23 downto 24);
                        command_fifo_signals_set(i).din(18 downto 17) <= ipb_mosi_i.ipb_wdata(21 downto 20);
                        command_fifo_signals_set(i).din(16)           <= ipb_mosi_i.ipb_wdata(16);
                        command_fifo_signals_set(i).din(15 downto  8) <= ipb_mosi_i.ipb_wdata(15 downto  8);
                        command_fifo_signals_set(i).din( 7 downto  0) <= ipb_mosi_i.ipb_wdata( 7 downto  0);                                        
                    end loop;
                else
                       command_fifo_signals_set(fe_index).wr_en <= ipb_mosi_i.ipb_strobe and not command_fifo_signals_set(fe_index).wr_en;              
                       command_fifo_signals_set(fe_index).din(CBCID_WIDTH+18 downto 19) <= ipb_mosi_i.ipb_wdata(CBCID_WIDTH+23 downto 24);
                       command_fifo_signals_set(fe_index).din(18 downto 17) <= ipb_mosi_i.ipb_wdata(21 downto 20);
                       command_fifo_signals_set(fe_index).din(16)           <= ipb_mosi_i.ipb_wdata(16);
                       command_fifo_signals_set(fe_index).din(15 downto  8) <= ipb_mosi_i.ipb_wdata(15 downto  8);
                       command_fifo_signals_set(fe_index).din( 7 downto  0) <= ipb_mosi_i.ipb_wdata( 7 downto  0);                     		  
                end if;
            else
                read_ack <= ipb_mosi_i.ipb_strobe and not read_ack;
            end if;
                --		  elsif ipb_mosi_i.ipb_write='0' then
                --		      read_next(sel-REPLY_FIFO_SEL) <= ipb_mosi_i.ipb_strobe 
                --		                                       and ( not from_ci2cbmgr_array(sel-REPLY_FIFO_SEL).reply_fifo_empty )
                --		                                       and ( not read_next(sel-REPLY_FIFO_SEL) );	
        end if;-- strobe
    end if;
	end process;

    read_next_gen:
    for i in 0 to NFE - 1 generate
        read_next(i) <= read_ack when ipb_mosi_i.ipb_write='0' and i = sel-REPLY_FIFO_SEL else '0'; 
    end generate;
    
    fe_id <= ipb_mosi_i.ipb_wdata(FEID_WIDTH+28 downto 29);
 	fe_index <= to_integer(unsigned(fe_id)-1);
 	
 	ipb_miso_o.ipb_ack <= ack;   
   
	ipb_miso_o.ipb_err <= '0';     
  ipb_miso_o.ipb_rdata <= reply_fifo_dout(32 * (sel-REPLY_FIFO_SEL+1)-1 downto 32 * (sel-REPLY_FIFO_SEL)) when sel >= REPLY_FIFO_SEL else (others => '0');

    ipbif_to_ci2cbmgr_set_gen :
    for i in 0 to NFE-1 generate

	    reply_fifo_read_next(i)	<= read_next(i);
               
        dp_fifo_inst : ENTITY work.dp_fifo_32b_512d -- temporary fifo to cross the clock domains
          PORT map(
            rst => '0',
            wr_clk => ipb_clk,
            rd_clk => cbc_ctrl_clk,
            din    => command_fifo_signals_set(i).din,
            wr_en  => command_fifo_signals_set(i).wr_en,
            rd_en  => command_fifo_signals_set(i).rd_en,
            dout   => command_fifo_signals_set(i).dout,
            full   => open,
            empty  => command_fifo_signals_set(i).empty
          );
          
        process (reset, cbc_ctrl_clk)
        variable timer : integer range 0 to 2;
        begin
	    if reset='1' then        
            timer := 0;
        elsif rising_edge(cbc_ctrl_clk) then

            command_fifo_signals_set(i).rd_en <= '0';
            com_fifo_we(i) <= '0';

            if timer = 2 then
                com_fifo_din(32 * (i+1) - 1 downto 32 * i) <= command_fifo_signals_set(i).dout;
                com_fifo_we(i) <= '1';
                timer := 0;
            elsif timer = 0 then
                if command_fifo_signals_set(i).empty = '0'  then
                    command_fifo_signals_set(i).rd_en <= '1';
                    timer := timer + 1;
                end if;
            else
                timer := timer + 1;
            end if;
        end if;
        end process;
        
    end generate;


end Behavioral;
