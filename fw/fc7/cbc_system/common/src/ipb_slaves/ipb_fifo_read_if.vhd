----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.07.2017 19:38:10
-- Design Name: 
-- Module Name: rpb_rdb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.system_package.all;
--use work.user_package.all;
--use work.cbc_system_package.all;

entity ipb_fifo_read_if is
	port (	clk 		    : in std_logic;
          reset       : in std_logic;
          ipb_mosi_i  : in  ipb_wbus;
          ipb_miso_o  : out ipb_rbus;
          dout        : in  std_logic_vector(31 downto 0);
          read_next_o : out std_logic         
);
end ipb_fifo_read_if;

architecture lat1clk of ipb_fifo_read_if is

  signal          ack : std_logic;
	signal    read_next : std_logic;

begin

	--=============================--
	process(reset, clk)
	--=============================--
	begin
		if reset='1' then

			ack <= '0';
			read_next <= '0';

		elsif rising_edge(clk) then
	
			ack <= ipb_mosi_i.ipb_strobe and not ack;
			
			if ipb_mosi_i.ipb_write='0' then
				read_next <= ipb_mosi_i.ipb_strobe and not read_next;
			end if;
		end if;

	end process;
	read_next_o          <= read_next; 
	ipb_miso_o.ipb_rdata <= dout;  
	--ipb_miso_o.ipb_rdata <= x"10101010";     
	ipb_miso_o.ipb_ack   <= ack;
	ipb_miso_o.ipb_err   <= '0';

end lat1clk;

