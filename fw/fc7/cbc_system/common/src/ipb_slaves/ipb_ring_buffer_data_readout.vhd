--===========================--
-- ipb_ring_buffer_data_readout
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_ring_buffer_data_readout is
generic( BUFFER_ADDR_WIDTH : integer := 16 );
port (
        clk                   : in  std_logic;
        reset                 : in  std_logic;
        ipb_mosi_i            : in  ipb_wbus;
        ipb_miso_o            : out ipb_rbus;
        next_data             : in  std_logic_vector(31 downto 0);
        next_addr             : in std_logic_vector(BUFFER_ADDR_WIDTH - 1 downto 0);
        load_next             : in std_logic;       
        addr                  : out std_logic_vector(BUFFER_ADDR_WIDTH - 1 downto 0)
        );
end ipb_ring_buffer_data_readout;

architecture Behavioral of ipb_ring_buffer_data_readout is

    signal data : std_logic_vector(31 downto 0);
    signal cntr : unsigned(31 downto 0);
begin

 
    --=============================--
    process(reset, load_next, clk)
    --=============================--

    begin
        if reset='1' then
            addr     <= (others => '0');
            data     <= (others => '1');  

        elsif load_next = '1' then
            addr     <= next_addr;
            data     <= next_data;
        elsif rising_edge(clk) then
            if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='0' then
                addr <= next_addr;
                data <= next_data;

            end if;
        end if;
    end process;

    ipb_miso_o.ipb_rdata <= data;    
    ipb_miso_o.ipb_ack <= ipb_mosi_i.ipb_strobe and (not ipb_mosi_i.ipb_write);
    ipb_miso_o.ipb_err <= '0';

end Behavioral;
