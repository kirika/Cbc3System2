--===========================--
-- ipb_cbc_system_cnfg 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_cnfg is
generic( ADDR_WIDTH     : natural := 8 );
port (
        clk                   : in  std_logic;
        reset                 : in  std_logic;
        ipb_mosi_i            : in  ipb_wbus;
        ipb_miso_o            : out ipb_rbus;
        cs_cnfg               : out cs_cnfg_type
        );
end ipb_cbc_system_cnfg;

architecture Behavioral of ipb_cbc_system_cnfg is

    signal regs: array_256x32bit;  
    
    signal sel: integer range 0 to 255;
    signal ack: std_logic;
    
    attribute keep: boolean;
    attribute keep of sel: signal is true;


	----------------------------------------------------------------------------------------------	    
  -- GLOBAL settings
  constant GLOBAL_CNFG_SEL                               : integer := 16#00#;
  -- BE_ID(BEID_WIDTH=6)  : 0x0000007f
  -- CBC_CLOCK_EXTERNAL   : 0x01000000 
  -- TRIG_MASTER_EXTERNAL : 0x10000000
  -- PROBE_CARD_EN        : 0x00100000
  ----------------------------------------------------------------------------------------------	
  -- test out settings
  constant TEST_OUT_CNFG_SEL                             : integer := 16#01#;
	----------------------------------------------------------------------------------------------	
	-- CBC settings
  constant CBC_CNFG_SEL_OFST                             : integer := 16#02#; --16#02# to 16#11#
  -- 1 word per 1 CBC, CBC1 - CBC16, 
  -- CBC_ACTIVE           : 0x01000000
  -- CBC_ID(CBCID_WIDTH=5): 0x0000001f
  -- CBC_FE_ID            : 0x00000700
  -- CBC_I2CADDRESS       : 0xfe000000 
	----------------------------------------------------------------------------------------------	
	-- reserved for cbc i2c
  constant CBC_I2C_BM_CNFG_SEL                           : integer := 16#12#;
	----------------------------------------------------------------------------------------------	
  -- fast command manager configurations
  constant FCM_CNFG_SEL                                  : integer := 16#13#;        
  -- FCM_FS_EN_EXT          : 0x00000001
  -- FCM_FS_EN_IPBUS        : 0x00000002
  -- FCM_FS_EN_INTERNAL     : 0x00000004
  -- FCM_FS_EN_SEQUENCER    : 0x00000008
  constant FCM_FCG_EN_CNFG_SEL                           : integer := 16#14#;
  -- FCM_FSG_EN_FAST_RESET  : 0x00000001
  -- FCM_FSG_EN_TRIGGER     : 0x00000002
  -- FCM_FSG_EN_TEST_PULSE  : 0x00000004
  -- FCM_FSG_EN_ORBIT_RESET : 0x00000008
  constant FCM_FCG_NCYCLE_CNFG_SEL                       : integer := 16#15#;
  constant FCM_FCG_CYCLE_PERIOD_CNFG_SEL                 : integer := 16#16#;
  constant FCM_FCG_TRIG_TIMING_CNFG_SEL                  : integer := 16#17#;
  constant FCM_FCG_TPREQ_TIMING_CNFG_SEL                 : integer := 16#18#;
  constant FCM_FCG_OR_TIMING_CNFG_SEL                    : integer := 16#19#;
	----------------------------------------------------------------------------------------------	
	-- reserved for sequencer
  constant SEQ_CNFG_SEL                                  : integer := 16#1a#;
	----------------------------------------------------------------------------------------------	
	-- raw data buffer
	constant RDB_CNFG_SEL                                  : integer := 16#1b#;
	-- RDB_CBC_ID             : 0x0000001f 
	-- RDB_LATENCY            : 0x00001fe0 
	-- RDB_WRITE_BLOCK_SIZE   : 0xffff0000 
	----------------------------------------------------------------------------------------------	
  -- data buffer
  constant DB_SAFE_NWORD_FREE_CNFG_SEL                  : integer := 16#1c#;
  constant DEFAULT_SAFE_NWORD_FREE                      : integer := 32 * 14;
	----------------------------------------------------------------------------------------------	
  -- io 
  constant IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST    : integer := 16#1d#; --16#1d# to 16#23#
	-- CBC_ID                : 0x0000001f
	-- IDELAY_OFFSET         : 0x000003e0 
	----------------------------------------------------------------------------------------------	
  -- cbc data processor
  constant CBC_DP_LAT_CNFG_SEL_OFST                     : integer := 16#24#; -- 16#24# to 16#33#
  -- CBC_DP_LAT_L1A        : 0x000001ff
  -- CBC_DP_LAT_TRIG_DATA  : 0x001ff000
	----------------------------------------------------------------------------------------------	
    
  signal cbc_cnfg_array                               : cs_cbc_cnfg_array_type;
  signal fe_cnfg_array                                : cs_fe_cnfg_array_type;


	function set_cbc_cnfg( signal reg   : in std_logic_vector(31 downto 0)) return cs_cbc_cnfg_type is
		variable c : cs_cbc_cnfg_type;
		begin
			c.active      := reg(24); 
			c.id          := reg( 4 downto  0);
			c.fe_id       := reg(10 downto  8);
			c.i2c_address := reg(31 downto 25);
		return c;
		end set_cbc_cnfg;		

	function n_active_cbc( signal a : in cs_cbc_cnfg_array_type ) return unsigned is
        variable result : unsigned( CBCID_WIDTH - 1 downto 0 );
        begin
            result := ( others => '0' );
            for i in a'RANGE loop
                if a(i).active = '1' then
                    result := result + 1;
                end if;
            end loop;
        return result;
    	end n_active_cbc;
    	
	function mk_fe_cnfg_array( signal a : in cs_cbc_cnfg_array_type ) return cs_fe_cnfg_array_type is
        variable fe_cnfg_array : cs_fe_cnfg_array_type;      
        variable curr_index : integer;
	begin
        	for fe_index in fe_cnfg_array'RANGE loop
        	    fe_cnfg_array(fe_index).id := std_logic_vector(to_unsigned(fe_index + 1, FEID_WIDTH));
               	curr_index := 0;
            	for cbc_index in a'RANGE loop
                	if a(cbc_index).fe_id = std_logic_vector(to_unsigned(fe_index+1,FEID_WIDTH)) and a(cbc_index).active = '1' then
                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).id := a(cbc_index).id;
                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).fe_id := a(cbc_index).fe_id;
                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).i2c_address := a(cbc_index).i2c_address;                    		
                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).active := a(cbc_index).active;   
                    		curr_index := curr_index + 1;
                	end if;
                	fe_cnfg_array(fe_index).n_active_cbcs := std_logic_vector(to_unsigned(curr_index,CBCID_WIDTH));
            	end loop;     
        end loop;
	return fe_cnfg_array;
	end mk_fe_cnfg_array;	
	

begin
	--=============================--
	sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
	--=============================--

	--=============================--
	process(reset, clk)
	--=============================--
	variable timer : natural;
	variable wsel  : integer range 0 to 255;
	variable sel_offset : integer range 0 to 15;
	begin

		if reset='1' then
			
			regs   <= (others=> (others=>'0'));
			ack 	 <= '0';

			--================--
			-- basic settings default values
			--================--            
			-- GLOBAL settings
			-- be
			regs(GLOBAL_CNFG_SEL)(BEID_WIDTH-1 downto 0) <= std_logic_vector(to_unsigned(1,BEID_WIDTH)); -- be id = 1
			-- cbc clock external
			regs(GLOBAL_CNFG_SEL)(24)                    <= '0';                                         -- clock internal 
			-- trigger master external
			regs(GLOBAL_CNFG_SEL)(28)                    <= '0';                                         -- trigger master internal 
					-- 0: veto signal is sent to the fast command manager.
					-- 1: veto signal is sent to external port.
            regs(GLOBAL_CNFG_SEL)(20)                    <= '0';  
            
			-- TEST OUT settings
			regs(TEST_OUT_CNFG_SEL) <= x"00020001";

			-- CBC settings
			-- i2c address & active & x"000" & '0' & fe_id & "000" & cbc_id
			-- cbc1
			regs(CBC_CNFG_SEL_OFST+ 0) <= "1000001" & '0' & x"000" & '0' & "001" & "000" & "00001";
			-- cbc2		                                                   
			regs(CBC_CNFG_SEL_OFST+ 1) <= "1000010" & '0' & x"000" & '0' & "001" & "000" & "00010";
			-- cbc3                                                      
			regs(CBC_CNFG_SEL_OFST+ 2) <= "1000011" & '0' & x"000" & '0' & "001" & "000" & "00011";
			-- cbc4                                                      
			regs(CBC_CNFG_SEL_OFST+ 3) <= "1000100" & '0' & x"000" & '0' & "001" & "000" & "00100";	 
			-- cbc5                                                      
			regs(CBC_CNFG_SEL_OFST+ 4) <= "1000101" & '0' & x"000" & '0' & "001" & "000" & "00101";
			-- cbc6		                                                   
			regs(CBC_CNFG_SEL_OFST+ 5) <= "1000110" & '0' & x"000" & '0' & "001" & "000" & "00110";	
			-- cbc7                                                      
			regs(CBC_CNFG_SEL_OFST+ 6) <= "1000111" & '0' & x"000" & '0' & "001" & "000" & "00111"; 
			-- cbc8                                                      
			regs(CBC_CNFG_SEL_OFST+ 7) <= "1001000" & '0' & x"000" & '0' & "001" & "000" & "01000";
			-- cbc9                                                      
			regs(CBC_CNFG_SEL_OFST+ 8) <= "1001001" & '0' & x"000" & '0' & "010" & "000" & "01001";
			-- cbc10		                                                 
			regs(CBC_CNFG_SEL_OFST+ 9) <= "1001010" & '0' & x"000" & '0' & "010" & "000" & "01010";  
			-- cbc11                                                     
			regs(CBC_CNFG_SEL_OFST+10) <= "1001011" & '0' & x"000" & '0' & "010" & "000" & "01011";
			-- cbc12                                                     
			regs(CBC_CNFG_SEL_OFST+11) <= "1001100" & '0' & x"000" & '0' & "010" & "000" & "01100";     
			-- cbc13                                                     
			regs(CBC_CNFG_SEL_OFST+12) <= "1001101" & '0' & x"000" & '0' & "010" & "000" & "01101";
			-- cbc14	                                                   
			regs(CBC_CNFG_SEL_OFST+13) <= "1001110" & '0' & x"000" & '0' & "010" & "000" & "01110";
			-- cbc15                                                     
			regs(CBC_CNFG_SEL_OFST+14) <= "1001111" & '0' & x"000" & '0' & "010" & "000" & "01111";
			-- cbc16                                                     
			regs(CBC_CNFG_SEL_OFST+15) <= "1010000" & '0' & x"000" & '0' & "010" & "000" & "10000";

			-- cbc_i2c
			regs(CBC_I2C_BM_CNFG_SEL) <= (others => '0');

			-- FAST COMMAND MANAGER settings
			regs(FCM_CNFG_SEL)        <= x"0000000F"; -- all enabled  
			regs(FCM_FCG_EN_CNFG_SEL) <= x"0000000F"; -- all enabled
			-- FAST COMMAND GENERATOR settings
			regs(FCM_FCG_NCYCLE_CNFG_SEL)       <= (others => '0');
			regs(FCM_FCG_CYCLE_PERIOD_CNFG_SEL) <= (others => '0');            
			regs(FCM_FCG_TRIG_TIMING_CNFG_SEL)  <= (others => '0');           
			regs(FCM_FCG_TPREQ_TIMING_CNFG_SEL) <= (others => '0');          
			regs(FCM_FCG_OR_TIMING_CNFG_SEL)    <= (others => '0');         
			-- sequencer                
			regs(SEQ_CNFG_SEL)    <= (others => '0');         
			-- raw data buffer
			regs(RDB_CNFG_SEL)    <= x"ffff00e1"; -- CBC_ID = 1, latency = 7, write block size = 0xffff 
			-- data buffer
			regs(DB_SAFE_NWORD_FREE_CNFG_SEL) <= std_logic_vector(to_unsigned(DEFAULT_SAFE_NWORD_FREE, 32));
			-- io
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 0) <= x"00000001";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 1) <= x"00000002";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 2) <= x"00000003";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 3) <= x"00000004";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 4) <= x"00000005";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 5) <= x"00000006";
			regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST+ 6) <= x"00000007";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 0)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 1)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 2)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 3)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 4)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 5)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 6)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 7)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 8)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+ 9)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+10)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+11)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+12)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+13)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+14)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+15)                  <= x"00008008";
			regs(CBC_DP_LAT_CNFG_SEL_OFST+16)                  <= x"00008008";

				
			ack   <= '0';
			timer    := 0;
			wsel     := 0;
				 
		elsif rising_edge(clk) then
			
			-- write
			if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
				regs(sel) <= ipb_mosi_i.ipb_wdata;
			end if;
			
			-- read 
			ipb_miso_o.ipb_rdata <= regs(sel);
			-- ack
			ack <= ipb_mosi_i.ipb_strobe and not ack;

		end if;
			
	end process;
    
  ipb_miso_o.ipb_ack <= ack;
  ipb_miso_o.ipb_err <= '0';

  cs_cnfg.be_id              <= regs(GLOBAL_CNFG_SEL)(BEID_WIDTH-1 downto 0);
  cs_cnfg.cbc_clock_ext      <= regs(GLOBAL_CNFG_SEL)(24);
  cs_cnfg.trigger_master_ext <= regs(GLOBAL_CNFG_SEL)(28);	
  cs_cnfg.probe_card         <= regs(GLOBAL_CNFG_SEL)(20);

  cs_cnfg.test_out_sel0      <= regs(TEST_OUT_CNFG_SEL)(15 downto  0);
  cs_cnfg.test_out_sel1      <= regs(TEST_OUT_CNFG_SEL)(31 downto 16);

  cs_cnfg.n_active_cbc       <= std_logic_vector(n_active_cbc(cbc_cnfg_array));

  cs_cnfg.cbc_array          <= cbc_cnfg_array;	

  g_cbc_cnfg_sig_gen :
  for i in 0 to MAX_N_CBC-1 generate
		cbc_cnfg_array(i)            <= set_cbc_cnfg(regs(CBC_CNFG_SEL_OFST + i));
  end generate;

  cs_cnfg.fe_array               <= fe_cnfg_array;
  fe_cnfg_array                  <= mk_fe_cnfg_array(cbc_cnfg_array);
        
  cs_cnfg.fcm.ext_en             <= regs(FCM_CNFG_SEL)(0);
  cs_cnfg.fcm.ipbus_en           <= regs(FCM_CNFG_SEL)(1);
  cs_cnfg.fcm.internal_en        <= regs(FCM_CNFG_SEL)(2);
  cs_cnfg.fcm.sequencer_en       <= regs(FCM_CNFG_SEL)(3);

  cs_cnfg.fcm.fcg_fast_reset_en  <= regs(FCM_FCG_EN_CNFG_SEL)(0);
  cs_cnfg.fcm.fcg_trigger_en     <= regs(FCM_FCG_EN_CNFG_SEL)(1);
  cs_cnfg.fcm.fcg_test_pulse_en  <= regs(FCM_FCG_EN_CNFG_SEL)(2);
  cs_cnfg.fcm.fcg_orbit_reset_en <= regs(FCM_FCG_EN_CNFG_SEL)(3);
  cs_cnfg.fcm.fcg_Ncycle         <= regs(FCM_FCG_NCYCLE_CNFG_SEL);
  cs_cnfg.fcm.fcg_cycle_T        <= regs(FCM_FCG_CYCLE_PERIOD_CNFG_SEL);
  cs_cnfg.fcm.fcg_trigger_t      <= regs(FCM_FCG_TRIG_TIMING_CNFG_SEL);
  cs_cnfg.fcm.fcg_test_pulse_t   <= regs(FCM_FCG_TPREQ_TIMING_CNFG_SEL);
  cs_cnfg.fcm.fcg_orbit_reset_t  <= regs(FCM_FCG_OR_TIMING_CNFG_SEL);


  cs_cnfg.db_safe_n_word_free    <= regs(DB_SAFE_NWORD_FREE_CNFG_SEL);

	cs_cnfg.rdb_cbc_id             <= regs(RDB_CNFG_SEL)(CBCID_WIDTH-1 downto 0);
	cs_cnfg.rdb_latency            <= regs(RDB_CNFG_SEL)(12 downto  5);
	cs_cnfg.rdb_write_block_size   <= regs(RDB_CNFG_SEL)(31 downto 16);


  cbc_data_rcv_iserdes_cnd_cnfg_gen:
  for j in 0 to MAX_N_FE-1 generate
		cs_cnfg.io.cbc_data_rcv.iserdes_cnd_fe_array(j).cbc_id        <= regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST + j)( CBCID_WIDTH-1 downto  0);
    cs_cnfg.io.cbc_data_rcv.iserdes_cnd_fe_array(j).idelay_offset <= regs(IO_CBC_DATA_RCV_ISERDES_CND_CNFG_SEL_OFST + j)( 9 downto  5);
	end generate;

  cbc_dp_cnfg_gen :
  for i in 0 to MAX_N_CBC-1 generate
    cs_cnfg.dp.cbc_dp_array(i).l1a_latency       <= regs(CBC_DP_LAT_CNFG_SEL_OFST + i)( 8 downto  0);
    cs_cnfg.dp.cbc_dp_array(i).trig_data_latency <= regs(CBC_DP_LAT_CNFG_SEL_OFST + i)(20 downto 12);
  end generate;
    
end Behavioral;
