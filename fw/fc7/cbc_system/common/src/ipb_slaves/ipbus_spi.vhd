library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_arith.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_unsigned.all;
use work.ipbus.all;
use work.system_package.all;
 

use work.user_package.all;
use work.user_version_package.all;

Library UNISIM;
use UNISIM.vcomponents.all;




entity ipbus_spi is
    Port ( 
           clk : in std_logic;
           reset : in std_logic;
           ipb_mosi_i : in ipb_wbus;
           ipb_miso_o : out ipb_rbus;
           
           spi_mosi_e : out STD_LOGIC_vector(1 downto 0);
           spi_miso_e : in STD_LOGIC_vector(1 downto 0);
           spi_clk_e  : out STD_LOGIC_vector(1 downto 0);
           spi_cs     : out std_logic_vector(5 downto 0);  
           b33_e1     : out std_logic;
           b33_e2     : out std_logic;
           b33_e3     : out std_logic;
           b33_e4     : out std_logic;
           pwr1_on_o   : out std_logic;
           pwr1_on_i   : in std_logic;
           pwr2_on_o   : out std_logic;
           pwr2_on_i   : in std_logic;
           
           trig       : in std_logic;
           done       : out std_logic;
           touchdown  : in std_logic_vector(1 downto 0)
           );
end ipbus_spi;

architecture rtl of ipbus_spi is
signal spi_mosi_i,spi_miso_i,spi_clk_i,spi_cs_delay_i,spi_cs_adac_i,spi_cs_dac_i,spi_run_i : std_logic;
signal spi_c:std_logic_vector(5 downto 0);
signal spi_delay:std_logic_vector(7 downto 0);

signal spi_din,spi_dout : std_logic_vector(63 downto 0);
signal spi_delay_v :  STD_LOGIC_VECTOR (7 downto 0);
signal spi_count :  STD_LOGIC_VECTOR (5 downto 0);
--           spi_end_clk_low : in std_logic;
signal spi_run,spi_done :  STD_LOGIC;
signal spi_cs_i : std_logic_vector(5 downto 0);
           
signal spi_end_clk_low : std_logic;       
signal spi_update_rising : std_logic;    
           
signal ipbus_ack :std_logic;

signal b33_i1,b33_i2,b33_i3,b33_i4 : std_logic;
signal pwr1_on_o_i,pwr2_on_o_i : std_logic;

signal delay_val,delay_c : std_logic_vector(15 downto 0);
signal sample_val,sample_c : std_logic_vector(15 downto 0);
signal trig_c : std_logic_vector(15 downto 0);
signal auto : std_logic;
signal trig_i,trig_re : std_logic;
signal trig_j : std_logic_vector(3 downto 0);
signal done_i : std_logic_vector(15 downto 0);
signal a_state : std_logic_vector(2 downto 0);
signal auto_cs : std_logic_vector(5 downto 0);

signal fifo_w,fifo_r : std_logic;
signal fifo_f,fifo_e : std_logic;
signal fifo_do : std_logic_vector(23 downto 0);

signal spi_bus : std_logic_vector(1 downto 0);

COMPONENT spi_adc_fifo
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;
 
begin


--=============================--
	process(reset, clk)
--=============================--
	begin
	 if reset='1' then
      spi_mosi_i<='0';
      spi_clk_i<='0';
      spi_done<='0';
      spi_dout<=(others => '0');
      spi_run_i<='0';
      spi_delay<=(others => '0');
	 elsif rising_edge(clk) then
	  case spi_bus is 
	    when "00" => 
	      spi_miso_i<=spi_miso_e(0);
	    when "01" => 
          spi_miso_i<=spi_miso_e(0);

	    when "10" => 
	      spi_miso_i<=spi_miso_e(1);
	    when "11" => 
          spi_miso_i<=spi_miso_e(1);
      end case;    
	      
      spi_mosi_e(1)<=spi_mosi_i;
      spi_mosi_e(0)<=spi_mosi_i;
      spi_clk_e(1)<=spi_clk_i;
      spi_clk_e(0)<=spi_clk_i;
        
      if spi_run='0' and spi_run_i='0' then
       spi_done<='0';
      end if;
    
	
	  if spi_delay="00000000" then 
	   if spi_run='1' and spi_run_i='0' and spi_done='0' then 
	    spi_c<=spi_count;
	    spi_mosi_i<=spi_din(63);
	    spi_dout<=spi_din(62 downto 0) & '0';
	    spi_run_i<='1';
	    spi_clk_i<='0';
	    spi_delay<=spi_delay_v;
	   elsif spi_run_i='1' then
	    spi_delay<=spi_delay_v;     
        if spi_clk_i='1' then
          if spi_c="000000" then
           spi_run_i<='0';
           spi_done<='1';
          else
           spi_c<=spi_c-1;           
          end if;
          if spi_update_rising='0' then
            spi_mosi_i<=spi_dout(63);
            spi_dout<=spi_dout(62 downto 0) & spi_miso_i;
          end if;  
          spi_clk_i<='0';          
        else

          if spi_update_rising='1' then
            spi_mosi_i<=spi_dout(63);
            spi_dout<=spi_dout(62 downto 0) & spi_miso_i;
          end if;  
        
                              
          if spi_end_clk_low='1' and spi_c="000000" then
            spi_run_i<='0';
            spi_done<='1';
          else 
            spi_clk_i<='1';
          end if;
           
        end if;      
	   end if;
	  else
	   spi_delay<=spi_delay-1; 
	  end if; 
     end if; 
    end process ; 

   
  
  
  --=============================--
  process(reset, clk)
  --=============================--
  begin
      if reset='1' then
          ipbus_ack      <= '0';
  
          spi_run<='0';
          spi_end_clk_low <='0';
          
          spi_delay_v<="00001001";          
          
          spi_cs<="111111";
          spi_cs_i<="111111";
    
    
          b33_e1<='0';
          b33_e2<='0';
          b33_i1<='0';
          b33_i2<='0';

          b33_e3<='0';
          b33_e4<='0';
          b33_i3<='0';
          b33_i4<='0';

         
    
          fifo_w<='0';
          
          auto<='0';
          a_state<="000";
    
      elsif rising_edge(clk) then
          ipbus_ack<='0';
          
          b33_e1<=b33_i1;
          b33_e2<=b33_i2;
          b33_e3<=b33_i3;
          b33_e4<=b33_i4;

          
          fifo_w<='0';
          fifo_r<='0';  
          
          pwr1_on_o<=pwr1_on_o_i;
          pwr2_on_o<=pwr2_on_o_i;
          
          
          done<=done_i(0);
          done_i<="0"&done_i(15 downto 1);
---          trig_i<=not trig; --because the trigger from the sequencer is inverted
          trig_i <= trig;
          trig_j<=trig_i&trig_j(3 downto 1);
          
          
          if trig_j(3 downto 2)="10"  then
            trig_re<='1';
          end if;
          
          if auto='1' then 
            case a_state is 
              when "000" =>  --external trigger received
                if trig_re='1' and done_i(0)='0' then
                  trig_re<='0';
                  a_state<="001";
                  trig_c<=std_logic_vector(unsigned(trig_c)-1);
                  delay_c<=delay_val;
                  sample_c<=sample_val;
                end if; 
                
              when "001" => 
                delay_c<=std_logic_vector(unsigned(delay_c)-1);
                if unsigned(delay_c)=0 then
                  a_state<="010";
                end if;      
                
                                               
              when "010" =>
                if spi_run='0' and spi_done='0' then
                  spi_cs_i <=auto_cs;
                  spi_din(63 downto 32)<= X"01201000";
                  spi_count<="011111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='0';
                  spi_update_rising<='0';
                  
                  sample_c<=std_logic_vector(unsigned(sample_c)-1);
                                                    
                  a_state<="011";                                                       
                end if; 
                
              when "011" =>
                if spi_run='0' and spi_done='0' then
                  a_state<="100";
                end if;  
                
              when "100" =>
                if spi_miso_i='0' then
                
                  spi_din(63 downto 32)<= X"44000000";  --we could send an early done signal here...
                  spi_count<="011111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='0';
                  spi_update_rising<='0';
                  a_state<="101";                  
                end if;
                
              when "101" => 
                if spi_run='0' and spi_done='0' then
                  spi_cs_i<=(others => '1');
                  fifo_w<='1';
                  if unsigned(sample_c)=0 then
                    a_state<="000";
                    done_i<=(others => '1');
                    if unsigned(trig_c)=0 then
                      auto<='0';
                    end if;
                  else 
                    delay_c<=X"0010";
                    a_state<="001";                    
                  end if;
                  
                end if;  
              when others => 
                a_state<="000"; 
            end case;  
          end if;    
          
          
          
          spi_cs <=spi_cs_i;
          
          if spi_run='1' and spi_done='1' then
            if auto='0' then
              spi_cs_i<=(others => '1');
            end if;  
            spi_run<='0';
          end if;
          
          -- write
          if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then
           
            case ipb_mosi_i.ipb_addr(3 downto 0) is  
              when "0000" =>    --write 24 bits to DAC 
                if spi_run='0' and spi_done='0' then                
                  spi_cs_i(4) <='0';
                  spi_din(63 downto 40)<= ipb_mosi_i.ipb_wdata(23 downto 0);
                  spi_count<="010111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='1';
                  spi_update_rising<='1';
                  ipbus_ack<='1';                  
                end if;

              when "0001" =>    --write 24 bits to DAC 
                if spi_run='0' and spi_done='0' then                
                  spi_cs_i(5) <='0';
                  spi_din(63 downto 40)<= ipb_mosi_i.ipb_wdata(23 downto 0);
                  spi_count<="010111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='1';
                  spi_update_rising<='1';
                  ipbus_ack<='1';                  
                end if;


              when "0010" =>   --reset ADCs by sending 64 ones
                if spi_run='0' and spi_done='0' then                
                  spi_cs_i(0) <='0';
                  spi_cs_i(1) <='0';
                  spi_cs_i(2) <='0';
                  spi_cs_i(3) <='0';

                  spi_din<= X"ffffffffffffffff";
                  spi_count<="111111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='0';
                  spi_update_rising<='0';
                  ipbus_ack<='1';
                end if;

                              
              when "0100" =>   --xfer 24 bits with ADC1
                if spi_run='0' and spi_done='0' then
                  spi_cs_i(to_integer(unsigned(spi_bus))) <='0';                                  
                  
                  spi_din(63 downto 40)<= ipb_mosi_i.ipb_wdata(23 downto 0);
                  spi_count<="010111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='0';
                  spi_update_rising<='0';
                  ipbus_ack<='1';
                end if;

              when "0101" =>   --xfer 32 bits with ADC1
                if spi_run='0' and spi_done='0' then                
                  spi_cs_i(to_integer(unsigned(spi_bus))) <='0';                             
                  spi_din(63 downto 32)<= ipb_mosi_i.ipb_wdata;
                  spi_count<="011111";
                  spi_delay_v<="00001000";
                  spi_run<='1';
                  spi_end_clk_low<='0';
                  spi_update_rising<='0';
                  ipbus_ack<='1';
                end if;


              when "1000" =>   
                b33_i1<=ipb_mosi_i.ipb_wdata(0);
                b33_i2<=ipb_mosi_i.ipb_wdata(1);
                b33_i3<=ipb_mosi_i.ipb_wdata(2);
                b33_i4<=ipb_mosi_i.ipb_wdata(3);

                pwr1_on_o_i<=ipb_mosi_i.ipb_wdata(4);
                pwr2_on_o_i<=ipb_mosi_i.ipb_wdata(5);
                ipbus_ack<='1';

              when "1001" => 
                sample_val<=ipb_mosi_i.ipb_wdata(31 downto 16);
                delay_val<=ipb_mosi_i.ipb_wdata(15 downto 0);
                ipbus_ack<='1';
                
              when "1010" =>   --launch single conversion on trigger, generate done signal once completed, then store result
                trig_c<=ipb_mosi_i.ipb_wdata(15 downto 0);
                trig_re<='0';
                
                if ipb_mosi_i.ipb_wdata(18 downto 16)/="111" then                
                  auto_cs<=(others => '1');
                  auto_cs(to_integer(unsigned(spi_bus))) <='0';                             
                  
                  if ipb_mosi_i.ipb_wdata(15 downto 0)=0 then
                    a_state<="001";
                    delay_c<=delay_val;
                    sample_c<=sample_val;
                  else
                    a_state<="000";
                  end if;
                  auto<='1';
                else 
                  auto<='0';  
                  spi_cs_i<=(others => '1');
                end if;  
                
                ipbus_ack<='1';
              when "1110" =>
                spi_bus<=ipb_mosi_i.ipb_wdata(1 downto 0);
                ipbus_ack<='1';
                
               
              when others => 
                ipbus_ack<='1';              
            end case;  
          end if;  
          
          --read
          if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
            ipb_miso_o.ipb_rdata<=(others => '0');
            case ipb_mosi_i.ipb_addr(3 downto 0) is
              when "0011" =>    --read last SPI result 
                ipb_miso_o.ipb_rdata <= spi_dout(31 downto 0); 
                ipbus_ack<='1';   --we know that data is available since we wait for the transfer to complete before acknowledging the transfer
                
              when "1000" =>    
                ipb_miso_o.ipb_rdata(0) <= b33_i1;
                ipb_miso_o.ipb_rdata(1) <= b33_i2;
                ipb_miso_o.ipb_rdata(2) <= b33_i3;
                ipb_miso_o.ipb_rdata(3) <= b33_i4;

                ipb_miso_o.ipb_rdata(4) <= pwr1_on_o_i;
                ipb_miso_o.ipb_rdata(5) <= pwr2_on_o_i;
                ipb_miso_o.ipb_rdata(6) <= pwr1_on_i;
                ipb_miso_o.ipb_rdata(7) <= pwr2_on_i;

                
                ipbus_ack<='1';                 


              when "1001" =>
                ipb_miso_o.ipb_rdata(23 downto 0) <=fifo_do;
                ipb_miso_o.ipb_rdata(31) <=fifo_e;
                ipb_miso_o.ipb_rdata(30) <= auto;
                ipb_miso_o.ipb_rdata(29) <= trig_i;
                ipb_miso_o.ipb_rdata(26 downto 24)<=a_state;
                ipbus_ack<='1';                 


              when "1010" =>
                ipb_miso_o.ipb_rdata(23 downto 0) <=fifo_do;
                ipb_miso_o.ipb_rdata(31) <=fifo_e;
                ipb_miso_o.ipb_rdata(30) <= auto;
                ipb_miso_o.ipb_rdata(29) <= trig_i;
                ipb_miso_o.ipb_rdata(26 downto 24)<=a_state;
                fifo_r<='1';  
                ipbus_ack<='1';                 

              when "1110" =>
                ipb_miso_o.ipb_rdata(1 downto 0) <= spi_bus;
                ipbus_ack<='1';


              when "1111" =>    --read spi status 
                ipb_miso_o.ipb_rdata(0) <= spi_done;
                ipb_miso_o.ipb_rdata(1) <= spi_run;                 
                ipb_miso_o.ipb_rdata(8) <= touchdown(0);
                ipb_miso_o.ipb_rdata(9) <= touchdown(1);
                ipbus_ack<='1';   --we know that data is available since we wait for the transfer to complete before acknowledging the transfer
                                   
              when others => 
                ipb_miso_o.ipb_rdata <=x"12345678";
                ipbus_ack<='1';  
                    
            end case;
          end if;
        end if;
  end process;
      
      ipb_miso_o.ipb_ack <= ipbus_ack;
      ipb_miso_o.ipb_err <= '0';
      
spi_adc_fifo_i : spi_adc_fifo
        PORT MAP (
          clk => clk,
          rst => reset,
          din => spi_dout(23 downto 0),
          wr_en => fifo_w,
          rd_en => fifo_r,
          dout => fifo_do,
          full => fifo_f,
          empty => fifo_e
        );      
  
end rtl;
 