library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

use work.user_package.all;
use work.user_version_package.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity relay_driver is
generic(addr_width : natural := 8);
    Port ( clk : in STD_LOGIC;
       reset : in STD_LOGIC;
       ipb_mosi_i : in ipb_wbus;
       ipb_miso_o : out ipb_rbus;
       
       gpio_in : in std_logic_vector(7 downto 0); 
       gpio_out : out std_logic_vector(7 downto 0);
 
       relay_out : out std_logic_vector(5 downto 0)              
       );
end relay_driver;




architecture Behavioral of relay_driver is

signal ipbus_ack : std_logic;
signal gpio_out_i : std_logic_vector(7 downto 0);
signal gpio_in_i : std_logic_vector(7 downto 0);
signal relayA_out_i : std_logic_vector(3 downto 0);
signal relayB_out_i : std_logic_Vector(1 downto 0);

begin

    relay_out <= relayB_out_i & relayA_out_i;
    gpio_out<=gpio_out_i;
    gpio_in_i<=gpio_in;

process(reset, clk)
variable i : integer;
variable relayA_inprocess : boolean;
variable relayA_reset  : boolean;
variable relayA_update : boolean;
variable relayA_value  : std_logic_vector(3 downto 0); 
variable relayA_timer  : unsigned(15 downto 0);
variable relayB_inprocess : boolean;
variable relayB_reset  : boolean;
variable relayB_update : boolean;
variable relayB_value  : std_logic_vector(1 downto 0); 
variable relayB_timer  : unsigned(19 downto 0);
begin
  if reset='1' then
    ipbus_ack 	 <= '0';

    relayA_out_i<=(others => '0');
    relayB_out_i<= (others => '0');
    gpio_out_i<=(others => '0');
    relayA_inprocess := false;
    relayA_reset  := false;
    relayA_update := false;
    relayA_value  := (others => '0');
    relayA_timer  := (others => '0');
    relayB_inprocess := false;
    relayB_reset  := false;
    relayB_update := false;
    relayB_value  := (others => '0');
    relayB_timer  := (others => '0');    
  elsif rising_edge(clk) then
      
        ipbus_ack<='0';

        if relayA_timer /= 0 then
            relayA_timer := relayA_timer + 1;
        end if;

        if relayA_update = true then
            if relayA_timer = x"8000" then
                relayA_update:= false;
                relayA_timer := (others => '0');
                relayA_inprocess := false;
            end if;
        end if;
        
        if relayA_reset = true then
            if relayA_timer = x"8000" then
                relayA_reset  := false;
                relayA_update := true;           
                relayA_timer  := x"0001";
                if relayA_value = "0001" or relayA_value = "0010" or relayA_value = "0100" or relayA_value = "1000" then              
                    relayA_out_i <= relayA_value;  
                end if;
            end if;
        end if;

      if relayB_timer /= 0 then
        relayB_timer := relayB_timer + 1;
      end if;
                 
      if relayB_update = true then
          if relayB_timer = x"80000" then
            relayB_update := false;
            relayB_timer  := (others => '0'); 
            relayB_inprocess := false;
          end if;
      end if;  

	  if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then
	       
        case ipb_mosi_i.ipb_addr(1 downto 0) is 
            when "00" =>                
                if relayA_inprocess = false then
                    relayA_value  := ipb_mosi_i.ipb_wdata(3 downto 0);
                    relayA_out_i <= (others => '0');              
                    relayA_timer := x"0001";
                    relayA_reset := true; 
                    relayA_inprocess := true;
                end if;
            when "01" =>
                if relayB_inprocess = false then
                    relayB_value  := ipb_mosi_i.ipb_wdata(1 downto 0);
                    relayB_out_i <= relayB_value; 
                    relayB_timer  := x"00001";
                    relayB_update := true;
                    relayB_inprocess := true;
                end if;
            when "10" =>
                gpio_out_i<=ipb_mosi_i.ipb_wdata(7 downto 0);
            when others =>
                ipbus_ack <= '1'; 
        end case;   
        ipbus_ack <= '1';         	       
      end if;  
                  
      if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
        ipb_miso_o.ipb_rdata<=(others => '0');
        case ipb_mosi_i.ipb_addr(1 downto 0) is
           when "00" => 
            ipb_miso_o.ipb_rdata(3 downto 0)<= relayA_out_i;   
           when "01" =>
            ipb_miso_o.ipb_rdata(1 downto 0)<= relayB_out_i;
           when "10" =>
            ipb_miso_o.ipb_rdata(7 downto 0)<= gpio_out_i;         
           when "11" =>   
            ipb_miso_o.ipb_rdata(7 downto 0)<= gpio_in_i;
         end case;      
       
         ipbus_ack<='1'; 
    
       end if;
       
  end if;
end process;               
           
           
ipb_miso_o.ipb_ack <=ipbus_ack;
ipb_miso_o.ipb_err <= '0';


end Behavioral;
