----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:39:38 09/24/2015 
-- Design Name: 
-- Module Name:    para_binary_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity para_binary_counter is
	generic( WIDTH : natural; INIT_VALUE : integer := 0; NEG_LOGIC : boolean := false );
    Port ( clk   : in  STD_LOGIC;
           reset : in  STD_LOGIC;
		   din   : in STD_LOGIC;
		   m     : in  STD_LOGIC_VECTOR( WIDTH-1 downto 0);
           q     : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end para_binary_counter;

architecture behavioral of para_binary_counter is
	signal r_reg, r_next, r_inc : unsigned( WIDTH-1 downto 0);
	signal sync_din : std_logic;
begin
	process( clk, reset )
	begin
		if ( reset = '1' ) then
			r_reg <= to_unsigned(INIT_VALUE, WIDTH);
		elsif ( rising_edge( clk ) and sync_din = '1' ) then
			r_reg <= r_next;
		end if;
	end process;
	r_inc <= r_reg + 1;
	r_next <= ( others=> '0' ) when r_reg=unsigned(m) else r_inc;
	q <= std_logic_vector(r_reg);

	sync_edge_detect : entity work.dff_sync_edge_detect
	generic map( NEG_LOGIC => NEG_LOGIC )
	port map(	reset	=> reset,
					clkb  => clk,
					dina  => din,
					doutb => sync_din );

end behavioral;

