--===========================--
-- dff_sync_edge_detect 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dff_sync_edge_detect is
generic ( NEG_LOGIC 	: boolean := false;
			 DELAY   : natural := 0
);
port (	reset	: in std_logic;
			clkb  : in std_logic;
			dina  : in std_logic;
			doutb : out std_logic
);
end dff_sync_edge_detect;

architecture Behavioral of dff_sync_edge_detect is

	signal edge      : std_logic;
	signal delayed_edge : std_logic;
    
    signal async_pulse_in : std_logic;
    signal sync_clk_in    : std_logic;
    signal sync_pulse_pipe: std_logic_vector(3 downto 0);
    signal async_pulse_int: std_logic;
    signal sync_pulse_sgl_clk : std_logic;
     
    attribute ASYNC_REG                   : string;
    attribute ASYNC_REG of async_stage      : label is "TRUE";

    attribute SHREG_EXTRACT : string;
    attribute SHREG_EXTRACT of ldpe_inst : label is "no";
    attribute SHREG_EXTRACT of async_stage : label is "no";

begin

    sync_clk_in <= clkb;

	process ( sync_clk_in, reset )
	variable timer : natural range 0 to DELAY;
	begin

	if reset = '1' then

		delayed_edge <= '0';
		timer := 0;
		
	elsif rising_edge( sync_clk_in ) then
	
		delayed_edge <= '0';
		
		if sync_pulse_sgl_clk = '1' or timer /= 0 then
			if DELAY /= 0 then
			timer := timer + 1;
			end if;
			if timer = DELAY then
				delayed_edge <= '1';
				timer := 0;
			end if;
		end if;
	end if;

	end process;

    pos_logic_gen: if NEG_LOGIC = false generate
        async_pulse_int <= dina;
        sync_pulse_sgl_clk <= not sync_pulse_pipe(3) and sync_pulse_pipe(2);
    end generate;
    
    neg_logic_gen: if NEG_LOGIC = true generate
        async_pulse_int <= not dina;
        sync_pulse_sgl_clk <= not( not sync_pulse_pipe(3) and sync_pulse_pipe(2) );
    end generate;
    
   LDPE_inst : LDPE
   generic map (
      INIT => '0') -- Initial value of latch ('0' or '1')  
   port map (
      Q => sync_pulse_pipe(0),      -- Data output
      PRE => async_pulse_int,  -- Asynchronous preset/set input
      D => async_pulse_int,      -- Data input
      G => '1',      -- Gate input
      GE => sync_pulse_pipe(3)     -- Gate enable input
   );

    async_stage: FD
        generic map (
                INIT => '0')
        port map (
                C    => sync_clk_in,
                D    => sync_pulse_pipe(0),
                Q    => sync_pulse_pipe(1)
        );

   
    fd_gen: for i in 1 to 2 generate
                sync_stage : FD
                generic map (
                  INIT => '0'
                )
                port map (
                  C    => sync_clk_in,
                  D    => sync_pulse_pipe(i),
                  Q    => sync_pulse_pipe(i+1)
                );
        end generate;

	doutb <=  sync_pulse_sgl_clk when DELAY = 0 else delayed_edge;

end Behavioral;

