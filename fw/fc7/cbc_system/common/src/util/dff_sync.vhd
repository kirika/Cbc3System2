----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.10.2018 14:10:11
-- Design Name: 
-- Module Name: diff_sync - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dff_sync is
generic ( NEG_LOGIC 	: boolean := false
);
port (	
			clkb  : in std_logic;
			dina  : in std_logic;
			doutb : out std_logic
);
end dff_sync;

architecture Behavioral of dff_sync is

signal edge      : std_logic;
signal delayed_edge : std_logic;

signal async_pulse_in : std_logic;
signal sync_clk_in    : std_logic;
signal sync_pulse_pipe: std_logic_vector(1 downto 0);
signal async_pulse_int: std_logic;
signal sync_pulse : std_logic;
 
attribute ASYNC_REG                   : string;
attribute ASYNC_REG of async_stage      : label is "TRUE";

attribute SHREG_EXTRACT : string;
attribute SHREG_EXTRACT of ldpe_inst : label is "no";
attribute SHREG_EXTRACT of async_stage : label is "no";

begin
    sync_clk_in <= clkb;

    pos_logic_gen: if NEG_LOGIC = false generate
        async_pulse_int <= dina;
        sync_pulse <= sync_pulse_pipe(1);
    end generate;
    
    neg_logic_gen: if NEG_LOGIC = true generate
        async_pulse_int <= not dina;
        sync_pulse <= not sync_pulse_pipe(1);
    end generate;

    LDPE_inst : LDPE
    generic map (
      INIT => '0') -- Initial value of latch ('0' or '1')  
    port map (
      Q => sync_pulse_pipe(0),      -- Data output
      PRE => async_pulse_int,  -- Asynchronous preset/set input
      D => async_pulse_int,      -- Data input
      G => '1',      -- Gate input
      GE => sync_pulse_pipe(1)     -- Gate enable input
    );

    async_stage: FD
    generic map (
            INIT => '0')
    port map (
            C    => sync_clk_in,
            D    => sync_pulse_pipe(0),
            Q    => sync_pulse_pipe(1)
    );

    doutb <=  sync_pulse;


end Behavioral;
