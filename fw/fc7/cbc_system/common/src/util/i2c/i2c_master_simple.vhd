--===========================--
-- i2c_master_simple 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_simple is
	generic(	PRESCALE : natural := 40; DATA_COUNTER_WIDTH	: natural := 2 );
    Port ( 	clk					: in 	 	STD_LOGIC;
				rst       			: in   	STD_LOGIC;
				input_ready       : in   	STD_LOGIC;
				saddr     			: in   	STD_LOGIC_VECTOR( 6 downto 0);
				rw						: in 		std_logic;
				ndata     			: in   	STD_LOGIC_VECTOR (DATA_COUNTER_WIDTH - 1 downto 0);
				data_i      		: in 		STD_LOGIC_VECTOR (7 downto 0);
				data_o				: out	 	std_logic_vector (7 downto 0);
				wdata_req		 	: out  	STD_LOGIC;
				rdata_en				: out		std_logic;
				done_o 				: out  	STD_LOGIC;
				err       			: out  	STD_LOGIC;
				sda_i 				: in 	 	STD_LOGIC;			  
				sda_o 				: out  	STD_LOGIC;
				scl_o 				: out  	STD_LOGIC
	);
end i2c_master_simple;

architecture Behavioral of i2c_master_simple is


type fsm_type is ( idle, start, addr_trans, data_trans, stop );
signal fsm 		: fsm_type;

signal ndata_int 	: integer;
signal latched_rw		: std_logic;
signal latched_ndata	: std_logic_vector(DATA_COUNTER_WIDTH - 1 downto 0);
signal latched_data 	: std_logic_vector( 7 downto 0 );
signal next_data 		: std_logic_vector( 7 downto 0 );
signal wbit				: std_logic;
signal scl_en			: std_logic;
signal done				: std_logic;

signal scl_pre			: std_logic;
signal dummy			: std_logic_vector( 7 downto 0 ) := "01010101";

signal ack_keep         : std_logic;

constant scl_start_count			: integer := 0;
constant scl_stop_count				: integer := 1;

constant ps_i2c_scl_fedge_count  : integer := PRESCALE / 2;
constant ps_i2c_start_count   	: integer := PRESCALE / 4;
-- constant ps_i2c_dw_count 			: integer := (PRESCALE / 4 ) * 3;
constant ps_i2c_dw_count 			: integer := (PRESCALE / 4 ) * 2 + 1;
constant ps_i2c_dr_count			: integer := PRESCALE / 4 + 1;
constant ps_i2c_stop_count 		: integer := PRESCALE / 4;
begin

	process ( clk, rst )
	variable bit_cntr 		: integer;
	variable data_cntr 		: integer;
	variable ps_cntr			: integer;
	variable input_latched 	: boolean;
	variable scl_start_cntr : integer;
	variable scl_stop_cntr	: integer;
	begin
		if( rst = '1' ) then

			ps_cntr				:= 0;
			input_latched		:= false;
			scl_start_cntr		:= 0;
			scl_stop_cntr		:= 0;
			data_o				<=	(others=>'0');
			wdata_req			<= '0';
			rdata_en				<= '0';
			done     			<= '0';
			err					<= '0';
			latched_rw			<= '0';
			latched_data 		<= (others=> '0');
			next_data			<= (others=>'0');
			wbit   				<= '1';
			scl_pre				<= '1';
			ack_keep            <= '1';
			fsm 					<= idle;

		elsif rising_edge(clk) then
				
			ps_cntr := ps_cntr + 1;
			if ps_cntr = PRESCALE then
				ps_cntr := 0;
			end if;

			if ps_cntr = 0  then
				scl_pre <= '1';
			elsif ps_cntr = PRESCALE / 2 then
				scl_pre <= '0';
			end if;

			wdata_req 		<= '0';
			rdata_en			<= '0';
			wbit 				<= '1';
			done 				<= '0';			
			
			case fsm is

				when idle =>

					if ps_cntr > 2 and input_ready = '1' then

						latched_rw	 	<= rw;
						latched_ndata 	<= ndata;
						latched_data 	<= saddr & rw;
						scl_pre 			<= '1';
						bit_cntr 		:= 0;
						data_cntr 		:= 0;
						input_latched	:= true;
					end if;
					if ps_cntr = 0 and input_latched = true then
						input_latched := false;
						fsm <= start;
					end if;

				when start =>
						
					if ps_cntr = 0 then
						scl_start_cntr := scl_start_cntr + 1;
					
					elsif scl_start_cntr = scl_start_count and ps_cntr >= ps_i2c_start_count and ps_cntr <= ps_i2c_scl_fedge_count then
						wbit <= '0';
						if ps_cntr = ps_i2c_scl_fedge_count then
							scl_start_cntr := 0;
							fsm <= addr_trans;
						end if;
					end if;

				when addr_trans =>

					if ps_cntr = ps_i2c_dw_count then
						bit_cntr := bit_cntr + 1;	
					end if;

					if bit_cntr = 0 then
						if  ps_cntr /= 0 and ps_cntr < ps_i2c_dw_count then
							wbit <= '0';
						end if;
					elsif bit_cntr < 9 then
						wbit <= latched_data(8-bit_cntr);

					elsif bit_cntr = 9 then
						if ps_cntr = ps_i2c_dw_count then
							-- request next data
							if latched_rw = '0' then
								wdata_req <= '1';	
							end if;						
						elsif ps_cntr = ps_i2c_dr_count then
							if sda_i  = '0' then
								-- latch next data
								if latched_rw = '0' then
									latched_data <= data_i;
								end if;
							else
								err <= '1';
--								fsm <= idle;
								fsm <= stop;
							end if;
						elsif ps_cntr = ps_i2c_scl_fedge_count then
							bit_cntr := 0;
							fsm <= data_trans;
						end if;
					end if;
	
				when data_trans =>

					if latched_rw = '0' then
					
						if ps_cntr = ps_i2c_dw_count then
							bit_cntr := bit_cntr + 1;
							if bit_cntr = 9 then
								bit_cntr := 0;
							end if;
						end if;

						if bit_cntr /= 0 then
							if data_cntr /= ndata_int then
								wbit <= latched_data(8-bit_cntr);
							else
								if bit_cntr = 1 then 
								    wbit <= ack_keep;
								    if ps_cntr = 0 then
                                        --wbit <= '0';
                                        ack_keep <= '1';
                                        fsm <= stop;
									end if;	
								end if;
							end if;
								
						elsif bit_cntr = 0 then
                            wbit <= ack_keep;
							if ps_cntr = ps_i2c_dw_count then
								data_cntr := data_cntr + 1;
							end if;
							
							if data_cntr /= ndata_int then
								if ps_cntr = ps_i2c_dw_count then
									wdata_req <= '1';
								end if;
							end if;
							
							if ps_cntr = ps_i2c_dr_count then	
								if sda_i  = '0' then	
									latched_data <= data_i;	
									ack_keep <= '0';	
								else
									err <= '1';
									fsm <= idle;
								end if;		
							end if;
							
							wbit <= ack_keep;
							
						end if;
							
					elsif latched_rw = '1' then		
						
						if ps_cntr = ps_i2c_dr_count then
							bit_cntr := bit_cntr + 1;
							if bit_cntr = 9 then
									bit_cntr := 0;
							end if;
							if bit_cntr = 0 then
								rdata_en <= '1';
							else
								data_o(8-bit_cntr) <= sda_i;	
--								data_o(8-bit_cntr) <= dummy(8-bit_cntr);
							end if;
						end if;

						if bit_cntr = 8 then
							if ps_cntr = ps_i2c_dr_count then
								data_cntr := data_cntr + 1;
							end if;
							-- aqc. write
							if data_cntr /= ndata_int then
								if ps_cntr >= ps_i2c_dw_count or ps_cntr < ps_i2c_dr_count then							
									wbit <= '0';
								end if;
							end if;
						elsif bit_cntr = 0 then
							-- aqc. write
							if data_cntr /= ndata_int then
								if data_cntr /= 0 then
									if ps_cntr >= ps_i2c_dr_count and ps_cntr < ps_i2c_dw_count then
										wbit <= '0';
									end if;
								end if;
							else
								if ps_cntr = 0 then
									wbit <= '0';
									fsm <= stop;
								end if;
							end if;
						end if;
					end if;
				
				when stop =>
					if ps_cntr = 0 then
						scl_stop_cntr := scl_stop_cntr + 1;
					end if;
					if scl_stop_cntr = 0 then
						if ps_cntr /= 0 and ps_cntr < ps_i2c_stop_count then
							wbit <= '0';						
						elsif ps_cntr = PRESCALE-1 then
							done <= '1';
						end if;
					end if;
					
					if scl_stop_cntr = scl_stop_count and ps_cntr = 0 then
						scl_stop_cntr := 0;
						fsm <= idle;	
					end if;
			end case;

		end if;
		
	end process;


--	scl_en <= '0' when fsm = idle else '1';
	scl_en <= '0' when fsm = idle or fsm = stop else '1';
	
	scl_o <= scl_pre when scl_en = '1' else '1';

	ndata_int <= to_integer( unsigned( latched_ndata ) );

	sda_o <= '0' when wbit='0' else '1';

	done_o <= done;
	
end Behavioral;

