--===========================--
-- cbc_data_deser 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity small_fifo is
    Generic (
			constant DATA_WIDTH : positive := 8;
			constant FIFO_DEPTH : positive := 2
			);
    Port ( 	clk   : in  STD_LOGIC;
				rst 	: in  STD_LOGIC;
				we 	: in  STD_LOGIC;
				din 	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
				re 	: in  STD_LOGIC;
				dout 	: out  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
				empty : out  STD_LOGIC;
				full 	: out  STD_LOGIC;
				werr 	: out STD_LOGIC;
				rerr 	: out STD_LOGIC
			  );
end small_fifo;

architecture Behavioral of small_fifo is

	type fifo_mem is array ( 0 to FIFO_DEPTH - 1 ) of std_logic_vector( DATA_WIDTH - 1 downto 0 );
	signal mem : fifo_mem;
begin

	process ( clk, rst )

		variable head : natural range 0 to FIFO_DEPTH - 1;
		variable tail : natural range 0 to FIFO_DEPTH - 1;
		variable looped : boolean;
	begin

		if rst = '1' then
				head := 0;
				tail := 0;
				full <= '0';
				empty <= '1';
				looped := false;
				werr <= '0';
				rerr <= '0';
				mem <= (others =>( others =>'0'));
		else
			if rising_edge( clk ) then

				if( re = '1' ) then
					if ( ( looped = true ) or ( head /= tail ) ) then
						dout <= mem( tail );
						if ( tail = FIFO_DEPTH - 1 ) then
							tail :=0;
							looped := false;
						else
							tail := tail + 1;
						end if;
						rerr <= '0';
					else
						rerr <= '1';
					end if;

				elsif( we = '1' ) then
					if( ( looped = false ) or ( head /= tail ) ) then
						mem(head) <= din;
						if( head = FIFO_DEPTH - 1 ) then
							head := 0;
							looped := true;
						else
							head := head + 1;
						end if;
						werr <= '0';
					else
						werr <= '1';
					end if;
				end if;

				if( head = tail ) then
					if looped then
						full <= '1';
					else
						empty <= '1';
					end if;
				else
					empty <= '0';
					full  <= '0';
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

