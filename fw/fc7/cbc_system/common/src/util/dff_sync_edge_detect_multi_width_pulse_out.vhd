----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:23:01 12/02/2015 
-- Design Name: 
-- Module Name:    DFFSyncEdgeDetectMultiWidthPulseOut - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dff_sync_edge_detect_multi_width_pulse_out is
generic ( NEG_LOGIC 	: boolean := false;
          DELAY   : natural := 0;
	      WIDTH	: natural := 2
);
port (	reset	: in std_logic;
			clkb  : in std_logic;
			dina  : in std_logic;
			doutb : out std_logic
);
end dff_sync_edge_detect_multi_width_pulse_out;

architecture Behavioral of dff_sync_edge_detect_multi_width_pulse_out is
	signal shift_reg : std_logic_vector( 1 downto 0 );
	signal edge 	  : std_logic;
	signal doutb0 : std_logic;
	signal doutb1 : std_logic;
begin

    single_pulse_gen : entity work.dff_sync_edge_detect
    generic map(NEG_LOGIC => NEG_LOGIC, DELAY => DELAY )
    port map( reset => '0', 
          clkb => clkb, 
          dina => dina, 
          doutb => doutb0 
    );   

	process ( clkb, reset )
	variable counter : natural range 0 to WIDTH;

	begin
	if reset = '1' then
		shift_reg <= ( others => '0');
		counter := 0;
		edge <= '0';
	elsif rising_edge( clkb ) then
	
		shift_reg <= shift_reg(0) & doutb0;
		if NEG_LOGIC = false then
			edge <= not shift_reg(0) and doutb0;			 
		else
			edge <= shift_reg(0) and not doutb0;
		end if;
	
		if edge = '1' then
			counter := counter + 1;
		end if;

		doutb1 <= '0';
		if counter /= 0 then
			doutb1 <= '1';
			if counter >= WIDTH-2 then
				counter := 0;
				doutb1 <= '0';
			else
				counter := counter + 1;
			end if;
		end if;			
	end if;
	end process;

	doutb <= doutb0 or shift_reg(0) or doutb1;

end Behavioral;


