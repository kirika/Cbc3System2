----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2017 17:58:45
-- Design Name: 
-- Module Name: data_clock_timing_tuning - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity cbc_data_iserdes_clock_tuning is
  generic ( TUNE_CLOCK : boolean := false );
  Port ( 
  iserdes_clk_d8            : in  std_logic;
  idelay_offset_i           : in  std_logic_vector(4 downto 0);
  reset_i                   : in  std_logic;
  tune_i                    : in  std_logic;
  scan_pulse_i               : in  std_logic;
  sync_pattern              : in  std_logic_vector(7 downto 0);
  mmcme2_drp_sclk           : in  std_logic;
  mmcme2_drp_srdy           : in  std_logic;
  mmcme2_drp_current_saddr  : in  std_logic_vector(1 downto 0);
  in_delay_tap_out          : in  std_logic_vector(5*6-1 downto 0);    
  cbc_par_data_slvs5        : in  std_logic_vector(7 downto 0);
  fsm_o                     : out std_logic_vector( 3 downto 0);
  iserdes_idelay_ctrl_fsm_o : out std_logic_vector( 2 downto 0);
	to_iserdes_in_delay_reset			: out std_logic;
	to_iserdes_in_delay_ce				: out std_logic_vector(5 downto 0);
	to_iserdes_in_delay_tap_in    : out std_logic_vector(5*6-1 downto 0);
	to_iserdes_in_delay_data_inc  : out std_logic_vector(5 downto 0);
	to_iserdes_iserdes_io_reset	  : out std_logic;
	to_iserdes_bitslip					  : out std_logic_vector(5 downto 0);     
  mmcme2_drp_rst_o          : out std_logic;
  mmcme2_drp_sen_o          : out std_logic;
  mmcme2_drp_saddr_o        : out std_logic_vector(1 downto 0);
  pattern_o                 : out std_logic_vector(7 downto 0);
  bitslip_counter_o         : out std_logic_vector(2 downto 0);
  done                      : out std_logic;
  pulse_scan_done_o         : out std_logic;
--  tuning_stat_set           : out cbcio_tuning_stat_set_type;
  test_out                  : out std_logic;
	from_bram_ready               : in  std_logic; 
	to_bram_req                   : out std_logic; 
  to_bram_wen                   : out std_logic;
  to_bram_waddr                 : out std_logic_vector(9 downto 0);
  to_bram_din                   : out std_logic_vector(15 downto 0)
);
end cbc_data_iserdes_clock_tuning;

architecture Behavioral of cbc_data_iserdes_clock_tuning is

    signal c_saddr               : integer range 0 to 2;

    -- tuning control
    type dctt_fsm_type is (init, wait_bram_ready, idelay_reset, idelay_scan, idelay_tuning, set_idelay, change_clock_phase, reset_iserdes_idelay_ctrl, bitslip_tuning, tuned);
    signal fsm                   : dctt_fsm_type;
    type next_saddr_op_type is ( zero, one, two, plus1, minus1 );
    signal next_saddr_op                 : next_saddr_op_type;
    signal pulse_scan_done               : boolean;
    signal send_mmcme2_drp_sen           : std_logic;
    signal send_in_delay_reset           : std_logic;
    signal send_in_delay_set             : std_logic;
    signal send_in_delay_ce              : std_logic;        
    signal iserdes_idelay_ctrl_reset     : std_logic;
    signal send_bitslip                  : std_logic; 
    signal idelay_tap_found              : std_logic;
    signal idelay_tap_to_set             : unsigned(4 downto 0);
            
    -- mmcme2_drp_sig_ctrl
    type mmcme2_drp_sig_ctrl_fsm_type is ( send_reset, wait_for_send_sen, wait_for_srdy );
    signal mmcme2_drp_sig_ctrl_fsm : mmcme2_drp_sig_ctrl_fsm_type;
    signal mmcme2_drp_rst        : std_logic;
    signal mmcme2_drp_sen        : std_logic;
    signal mmcme2_drp_ready      : std_logic;
    signal mmcme2_drp_saddr      : std_logic_vector(1 downto 0);
    
    -- iserdes_idelay_ctrl and iserdes output pattern check
--    signal dctt_to_iserdes_set           : dctt_to_iserdes_set_type;
    
    type iserdes_idelay_ctrl_fsm_type is ( wait_for_commands, in_delay_reset_process, in_delay_set_process, in_delay_ce_process, reset_iserdes_process, bitslip_process, pattern_check );
    signal iserdes_idelay_ctrl_fsm : iserdes_idelay_ctrl_fsm_type;
--    signal in_delay_reset                : std_logic; 
--    signal in_delay_ce                   : std_logic_vector(5 downto 0);   
--    signal in_delay_tap_in               : std_logic_vector(5*6-1 downto 0);  
--    signal iserdes_io_reset              : std_logic;
--    signal bitslip                       : std_logic_vector(5 downto 0); 
    signal bitslip_counter               : unsigned(2 downto 0);
    signal pattern                       : std_logic_vector(7 downto 0);        
    signal passed_max_scan_idelay_tap    : boolean;
    signal passed_max_tuning_idelay_tap  : boolean;
    signal good_pattern_found            : boolean;
    signal pattern_check_rdy             : boolean;
    signal all_counter                   : unsigned(6 downto 0);
    signal sync_good_counter             : unsigned(6 downto 0);    
    signal pattern_matched               : std_logic_vector(7 downto 0);
    signal pattern_unstable              : std_logic;
    signal pattern_bad                   : std_logic;
    signal pattern_changed               : std_logic; 
    signal sync_good                     : std_logic;   
    signal pattern0                      : std_logic_vector(7 downto 0);
    signal pre_pattern                   : std_logic_vector(7 downto 0);    
    signal idelay_tap_count              : integer range 0 to 31; 

    signal max_tuning_idelay_tap         : integer range 0 to 31;
    signal idelay_offset                 : integer range 0 to 31;
      
    constant MAX_SCAN_IDELAY_TAP         : integer := 19;
    --constant MAX_SCAN_IDELAY_TAP         : integer :=  3;   
    constant WAIT_COUNT                  : integer :=  6;

		signal reset                         : std_logic;
		signal mmcme2_drp_ctrl_reset         : std_logic;
		signal ctrl_tune       : std_logic;
		signal ctrl_scan_pulse : std_logic;
begin

	tuning_reset_gen : entity work.dff_sync_edge_detect	
	port map( reset => '0',
						clkb  => iserdes_clk_d8,
						dina  => reset_i,
						doutb => reset );
	tuning_tune_gen : entity work.dff_sync_edge_detect
  port map( reset => '0',
            clkb  => iserdes_clk_d8,
            dina  => tune_i,
            doutb => ctrl_tune );
 	scan_pulse : entity work.dff_sync_edge_detect
  port map( reset => '0',
             clkb  => iserdes_clk_d8, 
             dina  => scan_pulse_i,
             doutb => ctrl_scan_pulse );

	mmcme2_drp_ctrl_reset_gen : entity work.dff_sync_edge_detect	
	port map( reset => '0',
						clkb  => mmcme2_drp_sclk,
						dina  => reset_i,
						doutb => mmcme2_drp_ctrl_reset );

  -- from mmcme2_drp input
  c_saddr <= to_integer(unsigned(mmcme2_drp_current_saddr));

    
    --max_tuning_idelay_tap     <= 5;
    idelay_offset             <= to_integer(unsigned(idelay_offset_i));
    max_tuning_idelay_tap     <= 31 - idelay_offset;
    
    -- test signal
    test_out <= mmcme2_drp_ready;

    -- mmcme2_drp_controller
    mmcme2_drp_rst_o   <= mmcme2_drp_rst;
    mmcme2_drp_sen_o   <= mmcme2_drp_sen;
    mmcme2_drp_saddr_o <= mmcme2_drp_saddr;

    -- iserdes_idelay_ctrl
		fsm_o <=    "0000" when fsm = init else
                    "0001" when fsm = wait_bram_ready else
                    "0010" when fsm = idelay_reset else
                    "0011" when fsm = idelay_scan else
                    "0100" when fsm = idelay_tuning else
                    "0101" when fsm = set_idelay else
                    "0110" when fsm = change_clock_phase else
                    "0111" when fsm = reset_iserdes_idelay_ctrl else
                    "1000" when fsm = bitslip_tuning else
                    "1001" when fsm = tuned; 
    
		iserdes_idelay_ctrl_fsm_o <= "000" when iserdes_idelay_ctrl_fsm = wait_for_commands else
                                     "001" when iserdes_idelay_ctrl_fsm = in_delay_reset_process else
                                     "010" when iserdes_idelay_ctrl_fsm = in_delay_set_process else
                                     "011" when iserdes_idelay_ctrl_fsm = in_delay_ce_process else
                                     "100" when iserdes_idelay_ctrl_fsm = reset_iserdes_process else
                                     "101" when iserdes_idelay_ctrl_fsm = bitslip_process else
                                     "110" when iserdes_idelay_ctrl_fsm = pattern_check;

--    dctt_to_iserdes_set_o <= dctt_to_iserdes_set;
--    in_delay_data_inc         <= "111111";
--    in_delay_ce_o             <= in_delay_ce;
--    in_delay_tap_in_o         <= in_delay_tap_in;

--    bitslip_o                 <= bitslip;
--    iserdes_io_reset_o        <= iserdes_io_reset;

    bitslip_counter_o         <= std_logic_vector(bitslip_counter);

    -- timing tuning controller
    pattern_o <= pattern;  -- current pattern  
    pulse_scan_done_o <= '1' when pulse_scan_done else '0';

    -- mmcme2_drp controller
    process (mmcme2_drp_ctrl_reset,mmcme2_drp_sclk)
    begin
        if mmcme2_drp_ctrl_reset = '1' then
 
            mmcme2_drp_rst <= '0';
            mmcme2_drp_sen <= '0'; 
            mmcme2_drp_ready <= '0';
            mmcme2_drp_saddr <= "00";
            mmcme2_drp_sig_ctrl_fsm <= send_reset;                      

        elsif rising_edge(mmcme2_drp_sclk) then 

            mmcme2_drp_rst <= '0';
            mmcme2_drp_sen <= '0';    

           case mmcme2_drp_sig_ctrl_fsm is
           when send_reset =>
                mmcme2_drp_rst <= '1';
                mmcme2_drp_sig_ctrl_fsm <= wait_for_srdy;
           when wait_for_srdy =>
               if mmcme2_drp_srdy = '1' then
                    mmcme2_drp_ready <= '1';                                 
                    mmcme2_drp_sig_ctrl_fsm <= wait_for_send_sen;
               end if;            
           when wait_for_send_sen =>              
                if send_mmcme2_drp_sen = '1' then
                    mmcme2_drp_ready <= '0';
                    mmcme2_drp_sen <= '1';  
                    if next_saddr_op = zero then
                        mmcme2_drp_saddr <= "00";
                    elsif next_saddr_op = one then
                        mmcme2_drp_saddr <= "01";
                    elsif next_saddr_op = two then
                        mmcme2_drp_saddr <= "10";                                            
                    elsif next_saddr_op = minus1 then
                        mmcme2_drp_saddr <= std_logic_vector(unsigned(mmcme2_drp_current_saddr) - 1);     
                    elsif next_saddr_op = plus1 then
                        mmcme2_drp_saddr <= std_logic_vector(unsigned(mmcme2_drp_current_saddr) + 1);                             
                    end if;
                    mmcme2_drp_sig_ctrl_fsm <= wait_for_srdy;
                end if;
            end case;
        end if;        
    end process;
    
    -- iserdes control and data analysis
    -- in_delay_reset & in_delay_tap_in set the idelay tap value to in_delay_tap_in
    process  (reset,iserdes_clk_d8, mmcme2_drp_ready)
        variable timer  : integer range 0 to WAIT_COUNT; 
        variable bitslip_flag : boolean;
        variable in_delay_set_flag : boolean;
    begin
        if reset = '1' then        

            timer                  := WAIT_COUNT;           
            
            to_iserdes_in_delay_reset    <= '0';
            to_iserdes_in_delay_ce       <= (others => '0'); 
            to_iserdes_in_delay_tap_in   <= (others => '0'); 
            to_iserdes_in_delay_data_inc <= "111111";
            to_iserdes_iserdes_io_reset  <= '0';
            to_iserdes_bitslip           <= (others => '0'); 
            bitslip_flag := false;
            in_delay_set_flag := false;
            
            pattern                <= (others => '0');
            idelay_tap_count       <= 0;
            bitslip_counter        <= (others => '0');                        

            passed_max_scan_idelay_tap <= false; 
            passed_max_tuning_idelay_tap  <= false;
			good_pattern_found            <= false;
                    
            pre_pattern            <= (others => '0'); 
            pattern0               <= (others => '0');            
            all_counter            <= (others=> '0');
            sync_good_counter      <= (others=> '0');       
            pattern_matched        <= (others => '0');
            pattern_changed        <= '0';    
            pattern_unstable       <= '0';  
            pattern_bad            <= '0'; 
            sync_good              <= '0';             
          
            to_bram_wen   <= '0';
            to_bram_waddr <= (others => '0');
            to_bram_din   <= (others =>'0');

            pattern_check_rdy <= false;
            iserdes_idelay_ctrl_fsm <= wait_for_commands;
 
        elsif mmcme2_drp_ready = '0' then

		elsif rising_edge(iserdes_clk_d8) then


            to_iserdes_in_delay_reset <= '0';
            to_iserdes_in_delay_ce    <= (others => '0');
            to_iserdes_bitslip        <= (others => '0');
            
            pattern              <= cbc_par_data_slvs5;
            to_bram_wen    <= '0';
                        
            case iserdes_idelay_ctrl_fsm is

            when wait_for_commands =>


                all_counter                <= (others=> '0');                   
                pattern0                   <= (others => '0');   
                pattern_changed            <= '0'; 
                pattern_unstable           <= '0';  
                pattern_bad                <= '0';        
                sync_good                  <= '0'; 

                if send_in_delay_reset = '1' then
                    iserdes_idelay_ctrl_fsm <= in_delay_reset_process;  
                    to_iserdes_in_delay_tap_in <= (others => '0');              
                elsif send_in_delay_set = '1' then
                    iserdes_idelay_ctrl_fsm <= in_delay_set_process;
                    in_delay_set_flag := true;
                    for j in 0 to 5 loop
                        to_iserdes_in_delay_tap_in(5*(j+1)-1 downto 5*j) <= std_logic_vector(idelay_tap_to_set);
                    end loop;   
                elsif send_in_delay_ce = '1' then
                    iserdes_idelay_ctrl_fsm <= in_delay_ce_process;
                elsif iserdes_idelay_ctrl_reset = '1' then
                    iserdes_idelay_ctrl_fsm <=  reset_iserdes_process;                     
                elsif send_bitslip = '1' then
                    bitslip_flag := true;
                    iserdes_idelay_ctrl_fsm <= bitslip_process;
                end if;
 
            when in_delay_reset_process =>
               if timer = 0 then
                   timer := WAIT_COUNT;  
                   iserdes_idelay_ctrl_fsm <= pattern_check;                             
               elsif timer /= WAIT_COUNT then
                   timer := timer - 1;
               else
                   to_iserdes_in_delay_reset <= '1';
                   idelay_tap_count <= 0;
                   timer := WAIT_COUNT-1;
               end if;
           when in_delay_set_process =>
               if timer = 0 then
                   timer := WAIT_COUNT;  
                   iserdes_idelay_ctrl_fsm <= pattern_check;                             
               else
                   timer := timer - 1;               
               end if;               
               if in_delay_set_flag = true then
                   to_iserdes_in_delay_reset <= '1';
--                       for j in 0 to 5 loop
--                            to_iserdes_set(i).in_delay_tap_in(5*(j+1)-1 downto 5*j) <= std_logic_vector(idelay_tap_to_set);
--                       end loop;   
                   in_delay_set_flag := false;
               end if;

           when in_delay_ce_process =>
               if timer = 0 then
                   timer := WAIT_COUNT;
                   iserdes_idelay_ctrl_fsm <= pattern_check;                             
               elsif timer /= WAIT_COUNT then
                   timer := timer - 1;
               else
                   to_iserdes_in_delay_ce    <= (others => '1');                  
                   idelay_tap_count <= idelay_tap_count + 1; 
                   timer := WAIT_COUNT-1;
               end if;
           when reset_iserdes_process =>
               if timer = 0 then
                    to_iserdes_iserdes_io_reset <= '0';
                    timer := WAIT_COUNT;
                    iserdes_idelay_ctrl_fsm <= wait_for_commands;
               elsif timer /= WAIT_COUNT then
                   timer := timer -1;
               else
                   to_iserdes_iserdes_io_reset <= '1';
                   timer := WAIT_COUNT-1;
               end if;                          
            when bitslip_process =>
               if timer = 0 then
                    timer := WAIT_COUNT;  
                    iserdes_idelay_ctrl_fsm <= pattern_check;                             
                elsif timer /= WAIT_COUNT then
                    timer := timer - 1;
--                else                     
--                    for i in 0 to NDCTT_FAN-1 loop
--                        to_iserdes_set(i).bitslip <= (others => '1');
--                    end loop;
--                    bitslip_counter <= bitslip_counter + 1;          
--                    timer := WAIT_COUNT-1;
                end if;           
                if bitslip_flag = true then
                    to_iserdes_bitslip <= (others => '1');
                    bitslip_counter <= bitslip_counter + 1;          
                    timer := WAIT_COUNT-1;  
                    bitslip_flag := false;             
                end if; 
            
            when pattern_check =>

                if pattern_check_rdy = true then
                    to_bram_wen   <= '1';
                    to_bram_waddr <= std_logic_vector(to_unsigned((2-c_saddr) * (MAX_SCAN_IDELAY_TAP + 1) + idelay_tap_count, 10));
                    to_bram_din   <= "0000" & sync_good & pattern_bad & pattern_unstable & pattern_changed & pattern0;
--                    dcts_to_bram.din <= "000000" & std_logic_vector(to_unsigned(idelay_tap_count, 10));  
                    -- to_bram_din <= (others => '1');
                    pattern_check_rdy <= false;                        
                    iserdes_idelay_ctrl_fsm <= wait_for_commands;        
                    passed_max_scan_idelay_tap        <= false; 
                    passed_max_tuning_idelay_tap      <= false;                
                elsif all_counter = 100 then
                    pattern_check_rdy <= true;
                    
                    if idelay_tap_count >= MAX_SCAN_IDELAY_TAP then
                         passed_max_scan_idelay_tap <= true;
                    end if;
                    if idelay_tap_count >= max_tuning_idelay_tap then
                         passed_max_tuning_idelay_tap <= true;
                    end if; 
                    if pattern_bad = '0' and pattern_unstable = '0' then
                    	good_pattern_found <= true;
					end if;
 
                    if sync_good_counter = all_counter then
                        sync_good <= '1';
                    end if;
                    sync_good_counter <= (others=> '0'); 
                    pre_pattern <= pattern0;                  
                    pattern_matched <= (others => '0');                                    
                else
                    all_counter <= all_counter + 1;
                    if cbc_par_data_slvs5 = sync_pattern then
                        sync_good_counter <= sync_good_counter + 1;
                    end if;  
                    if cbc_par_data_slvs5 /= pre_pattern then
                        pattern_changed <= '1';
                    end if;
                    if all_counter = 0 then
                        pattern0 <= cbc_par_data_slvs5;
                        if cbc_par_data_slvs5 = sync_pattern then
                            pattern_matched(0) <= '1';
                        end if;
                        for i in 0 to 6 loop
                            if cbc_par_data_slvs5 = (sync_pattern(i downto 0) & sync_pattern(7 downto i+1)) then 
                                pattern_matched(i) <= '1';
                            end if;
                        end loop;
                    else
                        if all_counter = 1 then
                            if pattern_matched = x"00" then
                                pattern_bad <= '1';
                            end if;
                        end if;
                        if cbc_par_data_slvs5 /= pattern0 then
                            pattern_unstable <= '1';
                        end if;
                    end if;                                        
                end if;
            end case;
        end if;
    end process;

	-- timing tuning control
	process  (reset, ctrl_tune, ctrl_scan_pulse, iserdes_clk_d8, mmcme2_drp_ready)
    variable tuning_index     : integer range 0 to 1;  
    begin
        if reset = '1' then

            next_saddr_op             <= zero;          
            pulse_scan_done           <= false;
            send_mmcme2_drp_sen       <= '0';  
            send_in_delay_reset       <= '0'; 
            send_in_delay_set         <= '0';                                   
            send_in_delay_ce          <= '0'; 
            iserdes_idelay_ctrl_reset <= '0';
            send_bitslip              <= '0';          
            idelay_tap_found          <= '0';
            idelay_tap_to_set         <= (others => '0');
            done                      <= '0';                

            tuning_index := 0;

--            for i in 0 to 1 loop 
--                tuning_stat_set(i).idelay_reset_tuning_tap <= (others => '0');
--                tuning_stat_set(i).pre_idelay_tuning_tap <= (others => '0');
--                tuning_stat_set(i).pattern_at_idelay_reset <= (others => '0');
--                tuning_stat_set(i).fsm_processed <= (others => '0');
--            end loop;

            to_bram_req <= '0';

            fsm <= init;           

        elsif mmcme2_drp_ready = '0' then

            if fsm /= init then
                send_mmcme2_drp_sen <= '0';
                fsm <= reset_iserdes_idelay_ctrl;
            end if;
        elsif ctrl_tune = '1' then
            tuning_index := 0;
            idelay_tap_found <= '0';
            pulse_scan_done <= true;
            if TUNE_CLOCK  then        
                next_saddr_op <= zero;                
            else
                next_saddr_op <= minus1;                
            end if;
--             for i in 0 to 1 loop
--                tuning_stat_set(i).fsm_processed <= (others => '0');
--                tuning_stat_set(i).pre_idelay_tuning_tap <= (others => '0');
--                tuning_stat_set(i).pattern_at_idelay_reset <= (others => '0');
--            end loop;        
            fsm <= change_clock_phase;
        elsif ctrl_scan_pulse = '1' then
            pulse_scan_done <= false;
            next_saddr_op <= two;
            to_bram_req <= '1';
            fsm <= wait_bram_ready;
		elsif rising_edge(iserdes_clk_d8) then

 
            send_mmcme2_drp_sen <= '0';      
            done <= '0';       
 
            case fsm is
            when init =>
--                tuning_stat_set(tuning_index).fsm_processed(0) <= '1';
                if pattern_check_rdy = false then
                    to_bram_req <= '0';
                end if;
                send_in_delay_reset <= '0';
            when wait_bram_ready =>
                if from_bram_ready = '1' then
                    fsm <= change_clock_phase;
                end if;                
            when idelay_reset =>     
--                tuning_stat_set(tuning_index).fsm_processed(2) <= '1';     

                if iserdes_idelay_ctrl_fsm = wait_for_commands then
                    send_in_delay_reset <= '1'; 
                else
                    send_in_delay_reset <= '0'; 
                end if;
                if pattern_check_rdy  then
                    if pulse_scan_done then
                    --                        tuning_stat_set(tuning_index).pattern_at_idelay_reset <= cbc_par_data_slvs5;                                               
                    --                        tuning_stat_set(tuning_index).idelay_reset_tuning_tap <= in_delay_tap_out(5*5-1 downto 5*4);  
                        if pattern_unstable = '1' or pattern_bad = '1' then
                            if TUNE_CLOCK then
                                tuning_index := 1;
                                next_saddr_op <= plus1;
                                fsm <= change_clock_phase;
                            else
                                 fsm <= init;
                            end if;
                    --                            fsm <= pre_idelay_tuning;
                        else
                    --                            tuning_stat_set(tuning_index).pre_idelay_tuning_tap <= in_delay_tap_out(5*5-1 downto 5*4);                                                                                    
                            fsm <= idelay_tuning;          
                        end if;
                    else
                        fsm <= idelay_scan;
                    end if;
                end if;

            when idelay_scan =>
 
                if iserdes_idelay_ctrl_fsm = wait_for_commands then
                    send_in_delay_ce <= '1';
                else
                    send_in_delay_ce <= '0';
                end if;
                if pattern_check_rdy then
                    if passed_max_scan_idelay_tap then                     
                        if c_saddr = 0 then
                            pulse_scan_done <= true;
                            fsm <= init;
                        else
                             next_saddr_op <= minus1;
                             fsm <= change_clock_phase;
                        end if;
                    end if;
                end if;
--            when pre_idelay_tuning =>
                
--                tuning_stat_set(tuning_index).fsm_processed(3) <= '1';          
          
--                if iserdes_idelay_ctrl_fsm = wait_for_commands then
--                    send_in_delay_ce <= '1';
--                else                 
--                    send_in_delay_ce <= '0';
--                end if;
--                if pattern_check_rdy then                                                           
--                    if pattern_unstable = '0' and pattern_bad = '0' then
--                        tuning_stat_set(tuning_index).pre_idelay_tuning_tap <= in_delay_tap_out(5*5-1 downto 5*4);                                                             
--                        fsm <= idelay_tuning;                 
--                    end if;
--                end if;               

            when idelay_tuning => 
--                tuning_stat_set(tuning_index).fsm_processed(4) <= '1';                      
    
                if iserdes_idelay_ctrl_fsm = wait_for_commands then
                    send_in_delay_ce <= '1';                  
                else
                    send_in_delay_ce <= '0';
                end if;
                if pattern_check_rdy then                                                   
                    if pattern_changed = '1' then
                        if pattern_unstable = '1' or pattern_bad = '1' then
							if good_pattern_found then
								idelay_tap_found <= '1';
								idelay_tap_to_set    <= unsigned(in_delay_tap_out(5*5-1 downto 5*4)) + to_unsigned(idelay_offset, 5);                                
								fsm <= set_idelay;
                            else
                                if TUNE_CLOCK then
                                    tuning_index := 1;
                                    next_saddr_op <= plus1;
                                    fsm <= change_clock_phase;                                
                                else
                                    fsm <= init;
                                end if;
                            end if;
                        end if;
                    else
                        if passed_max_tuning_idelay_tap then
                            if TUNE_CLOCK then
                                tuning_index := 1;
                                next_saddr_op <= plus1;
                                fsm <= change_clock_phase;
                            else
                                fsm <= init;
                            end if;
                        end if;
                    end if;
                end if;
 
            when set_idelay =>
--                tuning_stat_set(tuning_index).fsm_processed(5) <= '1';       
               if iserdes_idelay_ctrl_fsm = wait_for_commands then                    
--                    idelay_tap_to_set    <= unsigned(in_delay_tap_out(5*5-1 downto 5*4)) + to_unsigned(idelay_offset, 5);
                    send_in_delay_set <= '1';
               else
                    send_in_delay_set <= '0';
               end if;
               if pattern_check_rdy then                        
                    next_saddr_op <= plus1;
                    fsm <= change_clock_phase;    
               end if;

            when change_clock_phase =>
                
--                tuning_stat_set(tuning_index).fsm_processed(6) <= '1';  
                   
                send_mmcme2_drp_sen <= '1';

            when reset_iserdes_idelay_ctrl =>
                
--                tuning_stat_set(tuning_index).fsm_processed(7) <= '1';  
                if mmcme2_drp_ready = '1' then
                    if iserdes_idelay_ctrl_fsm = reset_iserdes_process then
                         iserdes_idelay_ctrl_reset <= '0';         
                         if idelay_tap_found = '1' then
                            fsm <= bitslip_tuning;
                         else
                             fsm <= idelay_reset;   
                         end if;
                    else
                        iserdes_idelay_ctrl_reset <= '1';
                    end if;
                end if;
            when bitslip_tuning =>
--                tuning_stat_set(tuning_index).fsm_processed(8) <= '1';                      
                
                if iserdes_idelay_ctrl_fsm = wait_for_commands then
                    send_bitslip <= '1';
                else
                    send_bitslip <= '0';
                end if;

                if pattern_check_rdy then                   
                                            
                    if sync_good = '1' then       
                        done <= '1';
                        fsm <= tuned;                                 
                    end if;
                end if;

            when tuned =>

            end case; 
        end if;     
    end process;
    
    
end Behavioral;
