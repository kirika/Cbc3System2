----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.03.2017 14:55:00
-- Design Name: 
-- Module Name: mmcm2_drp_for_cbc_system - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mmcme2_drp_for_cbc_system is
port (
    SADDR    :  in   std_logic_vector(1 downto 0);
    SEN      :  in   std_logic;
    SCLK     :  in   std_logic;
    RST      :  in   std_logic;
    SRDY_O   :  out  std_logic;
-- These signals are to be connected to the MMCM_ADV by port name.
-- Their use matches the MMCM port description in the Device User Guide.
    DO       :  in   std_logic_vector(15 downto 0);
    DRDY     :  in   std_logic;
    LOCKED   :  in   std_logic;
    DWE      :  out  std_logic;
    DEN      :  out  std_logic;
    DADDR    :  out  std_logic_vector(6 downto 0);
    DI       :  out  std_logic_vector(15 downto 0);
    DCLK     :  out  std_logic;
    RST_MMCM :  out  std_logic;
    SADDR_O  :  out  std_logic_vector(1 downto 0)    
);
end mmcme2_drp_for_cbc_system;
 
architecture Behavioral of mmcme2_drp_for_cbc_system is

    signal next_saddr : std_logic_vector(1 downto 0);
    signal SRDY       : std_logic;
    
    component mmcme2_drp
    port(
    SADDR    : in std_logic_vector(1 downto 0);
    SEN      : in std_logic;
    SCLK     : in std_logic;
    RST      : in std_logic;
    SRDY     : out std_logic;
    -- These signals are to be connected to the MMCM_ADV by port name.
    -- Their use matches the MMCM port description in the Device User Guide.
    DO       : in std_logic_vector(15 downto 0);
    DRDY     : in std_logic;
    LOCKED   : in std_logic;
    DWE      : out std_logic;
    DEN      : out std_logic;
    DADDR    : out std_logic_vector(6 downto 0);
    DI       : out std_logic_vector(15 downto 0);
    DCLK     : out std_logic;
    RST_MMCM : out std_logic
    );
    end component;  
    
begin
    
    SRDY_O <= SRDY;
    
    process (SCLK)
    begin
        if rising_edge(SCLK) then
            if SEN = '1' then
                next_saddr <= SADDR;
            end if;
            if SRDY = '1' then
                SADDR_O <= next_saddr;
            end if;
        end if;
    end process;

    mmcme2_drp_inst : mmcme2_drp
    port map(
    SADDR    => SADDR,
    SEN      => SEN,
    SCLK     => SCLK,
--    RST      => mmcme2_drp_sigs_in.rst or ipb_cbc_system_ctrl.global.reset,
    RST      => RST,
    SRDY     => SRDY,
    -- These signals are to be connected to the MMCM_ADV by port name.
    -- Their use matches the MMCM port description in the Device User Guide.
    DO       => DO,
    DRDY     => DRDY,
    LOCKED   => LOCKED,
    DWE      => DWE,
    DEN      => DEN,
    DADDR    => DADDR,
    DI       => DI,
    DCLK     => DCLK,
    RST_MMCM => RST_MMCM
    );

end Behavioral;
