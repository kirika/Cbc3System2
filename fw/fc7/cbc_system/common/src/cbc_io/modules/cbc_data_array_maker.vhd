
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.cbc_system_package.all;
use work.cbc_system_config_package.all;
use work.cbc_io_package.all;

entity cbc_data_array_maker is
    generic ( invert : std_logic_vector(5 downto 0) := "000000");
	port(
		data_in_from_pins_p               : in  std_logic_vector(NCBC*6-1 downto 0); 
    data_in_from_pins_n                   : in  std_logic_vector(NCBC*6-1 downto 0); 
    cbc_par_data_o                        : out  std_logic_vector(NCBC*6*8-1 downto 0);
    cbc_data_clk                          : out  std_logic_vector(NCBC-1 downto 0);
    cbc_data_clk_m8                          : out  std_logic_vector(NCBC-1 downto 0);
		ref_clock			              : in  std_logic;
		not_ref_clock_locked              : in  std_logic;
		cbc_clk_in	                      : in  std_logic;
		cs_cnfg                           : in  cs_cnfg_type;
		cs_ctrl                           : in  cs_ctrl_type;
		cs_stat                           : out cs_io_stat_type;
		ipb_clk                           : in  std_logic;
		ipb_reset                         : in  std_logic;
		iserdes_cnd_scan_bram_ipb_mosi_i  : in  ipb_wbus;
		iserdes_cnd_scan_bram_ipb_miso_o  : out ipb_rbus;
		test_signal1                       : out std_logic;
		test_signal2                       : out std_logic

		);
end cbc_data_array_maker;

architecture Behavioral of cbc_data_array_maker is

	signal cbc_par_data                     : std_logic_vector(NCBC*6*8-1 downto 0);
	type cbc_data_to_iserdes_cnd_array_type is array (0 to NFE-1) of std_logic_vector(6*8-1 downto 0);
	signal cbc_data_to_iserdes_cnd_array    : cbc_data_to_iserdes_cnd_array_type;


	type cbc_index_to_iserdes_cnd_type is array (0 to NFE-1) of integer range 0 to NCBC;
    signal cbc_index_to_iserdes_cnd_array : cbc_index_to_iserdes_cnd_type;

	-- iserdes clocks 
	signal iserdes_clk	          : std_logic_vector(NFE-1 downto 0);
	signal iserdes_clk_d8		  : std_logic_vector(NFE-1 downto 0);

	-- iserdes data out
  type data_in_to_device_array_type is array(0 to NCBC-1) of std_logic_vector(8*6-1 downto 0);
  signal data_in_to_device_array          : data_in_to_device_array_type;

	-- iserdes tap out
	signal in_delay_tap_out        : std_logic_vector(NCBC*6*5-1 downto 0);
	signal in_delay_tap_out_to_cnd : std_logic_vector(NFE*6*5-1 downto 0); 
--	type in_delay_tap_out_array_type is array (0 to NCBC-1) of std_logic_vector(5*6-1 downto 0);
--  signal in_delay_tap_out_array           : in_delay_tap_out_array_type;
--	type in_delay_tap_out_to_iserdes_cnd_array_type is array (0 to NFE-1) of std_logic_vector(5*6-1 downto 0);
--	signal in_delay_tap_out_to_iserdes_cnd_array : in_delay_tap_out_to_iserdes_cnd_array_type; 

	-- cbc_data_iserdes_clock_and_delay to iserdes signals
	type iserdes_cnd_to_iserdes_type is
	record
		in_delay_reset          :  std_logic;
		in_delay_ce             :  std_logic_vector(5 downto 0);
		in_delay_tap_in         :  std_logic_vector(5*6-1 downto 0);
		in_delay_data_inc       :  std_logic_vector(5 downto 0);
		iserdes_io_reset        :  std_logic;
		bitslip                 :  std_logic_vector(5 downto 0);     
	end record;
	type iserdes_cnd_to_iserdes_array_type is array (0 to NFE-1) of iserdes_cnd_to_iserdes_type;
	signal iserdes_cnd_to_iserdes_array     : iserdes_cnd_to_iserdes_array_type;       

    
--	--cbc_data_iserdes_clk_and_delay to bram signals 
--  type bram_to_iserdes_cnd_type is
--  record
--      ready   : std_logic;
--  end record;
--  type bram_to_iserdes_cnd_fe_array_type is array (0 to NFE-1) of bram_to_iserdes_cnd_type;
--  signal bram_to_iserdes_cnd_fe_array        : bram_to_iserdes_cnd_fe_array_type;
--  type bram_to_iserdes_cnd_cbc_array_type is array (0 to NCBC-1) of bram_to_iserdes_cnd_type;
--  signal bram_to_iserdes_cnd_cbc_array        : bram_to_iserdes_cnd_cbc_array_type;
--
--	--bram to cbc_data_iserdes_clk_and_delay signals 
--	type iserdes_cnd_to_bram_type is
--  record
--      req    : std_logic;
--      wen    : std_logic;
--      waddr  : std_logic_vector(9 downto 0);
--      din    : std_logic_vector(15 downto 0);
--  end record;   
--	type iserdes_cnd_to_bram_array_type is array (0 to NFE-1) of iserdes_cnd_to_bram_type;
--	signal iserdes_cnd_to_bram_array        : iserdes_cnd_to_bram_array_type;
	signal iserdes_cnd_to_bram_array       : iserdes_cnd_to_bram_array_type;
	signal bram_to_iserdes_cnd_array       : bram_to_iserdes_cnd_array_type;

  signal iserdes_cnd_scan_bram_dout      : std_logic_vector(31 downto 0);
  signal iserdes_cnd_scan_bram_read_next : std_logic;

	signal cnd_tuning_reset                : std_logic_vector(NFE-1 downto 0); 
	signal cnd_tuning_tune                 : std_logic_vector(NFE-1 downto 0); 
	signal cnd_tuning_scan_pulse           : std_logic_vector(NFE-1 downto 0); 
	signal bram_reset                      : std_logic;
    signal bram_rd_reset                   : std_logic;
    signal bram_nrdata                      : std_logic_vector(9 downto 0);
    signal mmcm_locked                      : std_logic_vector(NFE-1 downto 0);
    signal mmcme2_drp_sen                   : std_logic_vector(NFE-1 downto 0);
    signal mmcme2_drp_rst                   : std_logic_vector(NFE-1 downto 0);  
    signal mmcme2_drp_srdy                  : std_logic_vector(NFE-1 downto 0); 
begin

	cbc_par_data_o <= cbc_par_data;
--	test_signal1 <= iserdes_cnd_to_iserdes_array(cbc_to_fe_index(0)).in_delay_ce(5);
--	test_signal2 <= iserdes_cnd_to_iserdes_array(0).in_delay_reset;
--    test_signal1 <= bram_reset;
    test_signal1 <= iserdes_clk(0);
    --test_signal1 <= iserdes_cnd_to_bram_array(0).req;
    test_signal2 <= iserdes_clk_d8(0);
    cs_stat.cbc_data_rcv.iserdes_cnd_bram_nrdata <= bram_nrdata;

    --===========================================--    
    cbc_data_iserdes_gen :
	--===========================================--
    for i in 0 to NCBC-1 generate 
    
	   cbc_data_clk(i) <= iserdes_clk_d8(cbc_to_fe_index(i));
	   cbc_data_clk_m8(i) <= iserdes_clk(cbc_to_fe_index(i));       
        -- data mapping of iserdes output 
                
        cbc_para_data_gen : for j in 0 to 5 generate
            slice_gen : for k in 0 to 7 generate
            
                swap_gen0 : if invert(j)='0' generate
                  cbc_par_data(i*6*8+j*8+7-k) <= data_in_to_device_array(i)(6*k+j);
                end generate;
                
                swap_gen1 : if invert(j)='1' generate
                  cbc_par_data(i*6*8+j*8+7-k) <= not data_in_to_device_array(i)(6*k+j);
                end generate;
                
                  
            end generate slice_gen;
        end generate cbc_para_data_gen;

        cs_stat.cbc_data_rcv.iserdes_cbc_array(i).slvs5 <= cbc_par_data(i*6*8 + 5*8 - 1 downto i*6*8 + 4*8);
        cs_stat.cbc_data_rcv.iserdes_cbc_array(i).in_delay_tap_out <= in_delay_tap_out((i+1)*6*5-1 downto i*6*5);         
	    
        -- iserdes 
        cbc_data_iserdes_inst : entity work.cbc_data_iserdes_pre
        port map 
        ( 
        clk_in              => iserdes_clk(cbc_to_fe_index(i)),                            
        clk_div_in          => iserdes_clk_d8(cbc_to_fe_index(i)),                        
        ref_clock			=> ref_clock,	
        not_ref_clock_locked => not_ref_clock_locked,
        data_in_from_pins_p => data_in_from_pins_p((i+1)*6-1 downto i*6),
        data_in_from_pins_n => data_in_from_pins_n((i+1)*6-1 downto i*6),
        data_in_to_device   => data_in_to_device_array(i),
		in_delay_reset      => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).in_delay_reset,                    
        in_delay_data_ce    => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).in_delay_ce,      
        in_delay_data_inc   => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).in_delay_data_inc,     
        in_delay_tap_in     => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).in_delay_tap_in,          
        bitslip             => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).bitslip,                                  
        io_reset            => iserdes_cnd_to_iserdes_array(cbc_to_fe_index(i)).iserdes_io_reset,
        in_delay_tap_out    => in_delay_tap_out((i+1)*6*5-1 downto i*6*5),
		delay_locked		=> cs_stat.cbc_data_rcv.iserdes_cbc_array(i).delay_locked
        );

    end generate cbc_data_iserdes_gen;       

	-- clock for cbc data inputs. the clock phase is tuned per front end.
	iserdes_clk_n_delay_gen:    
    for j in 0 to NFE-1 generate 

			cbc_index_to_iserdes_cnd_array(j)        <= get_cbc_index(cs_cnfg.cbc_array, cs_cnfg.io.cbc_data_rcv.iserdes_cnd_fe_array(j).cbc_id);
			cbc_data_to_iserdes_cnd_array(j)         <= cbc_par_data((cbc_index_to_iserdes_cnd_array(j)+1)*6*8-1 downto cbc_index_to_iserdes_cnd_array(j)*6*8);
			--in_delay_tap_out_to_iserdes_cnd_array(j) <= in_delay_tap_out_array(cbc_index_to_iserdes_cnd_array(j));
            in_delay_tap_out_to_cnd((j+1)*6*5-1 downto j*6*5) <= in_delay_tap_out((cbc_index_to_iserdes_cnd_array(j)+1)*6*5-1 downto cbc_index_to_iserdes_cnd_array(j)*6*5);

		cbc_data_iserdes_clock_and_delay_inst : entity work.cbc_data_iserdes_clock_and_delay
		Port map( 
			cbc_clk_in	                 => cbc_clk_in,
			idelay_offset                => cs_cnfg.io.cbc_data_rcv.iserdes_cnd_fe_array(j).idelay_offset,
			cnd_tuning_reset             => cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(j).reset,
			cnd_tuning_tune              => cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(j).tune,
			cnd_tuning_scan_pulse        => cs_ctrl.io.cbc_data_rcv.iserdes_cnd_fe_array(j).scan_pulse,
			cnd_tuning_fsm               => cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).fsm,
			cnd_tuning_idelay_ctrl_fsm   => cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).idelay_ctrl_fsm,
			cnd_tuning_mmcme2_drp_saddr  => cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).mmcme2_drp_saddr,
			in_delay_tap_out             => in_delay_tap_out_to_cnd((j+1)*6*5-1 downto j*6*5),
			cbc_par_data                 => cbc_data_to_iserdes_cnd_array(j),
			mmcm_iserdes_locked_o        => mmcm_locked(j),  
			pulse_scan_done              => cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).pulse_scan_done,
			to_iserdes_clk_d8            => iserdes_clk_d8(j),
			to_iserdes_clk 	             => iserdes_clk(j),
			to_iserdes_in_delay_reset    => iserdes_cnd_to_iserdes_array(j).in_delay_reset,
			to_iserdes_in_delay_ce       => iserdes_cnd_to_iserdes_array(j).in_delay_ce,
			to_iserdes_in_delay_tap_in   => iserdes_cnd_to_iserdes_array(j).in_delay_tap_in,
			to_iserdes_in_delay_data_inc => iserdes_cnd_to_iserdes_array(j).in_delay_data_inc,
			to_iserdes_iserdes_io_reset  => iserdes_cnd_to_iserdes_array(j).iserdes_io_reset,
			to_iserdes_bitslip           => iserdes_cnd_to_iserdes_array(j).bitslip,
			bitslip_counter              => cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).bitslip_counter,
			from_bram_ready              => bram_to_iserdes_cnd_array(j).ready,
			to_bram_req     	         => iserdes_cnd_to_bram_array(j).req,			
			to_bram_wen     	         => iserdes_cnd_to_bram_array(j).wen,			
			to_bram_waddr                => iserdes_cnd_to_bram_array(j).waddr,			
			to_bram_din     	         => iserdes_cnd_to_bram_array(j).din,			
			mmcme2_drp_sen_o             => mmcme2_drp_sen(j),
            mmcme2_drp_rst_o             => mmcme2_drp_rst(j),   
            mmcme2_drp_srdy_o            => mmcme2_drp_srdy(j)
		);
		cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).mmcme2_drp_srdy <= mmcme2_drp_srdy(j);
		cs_stat.cbc_data_rcv.iserdes_cnd_fe_array(j).mmcm_locked <= mmcm_locked(j);
	end generate iserdes_clk_n_delay_gen;       

  iserdes_cnd_scan_bram_reset_gen : entity work.dff_sync_edge_detect
  port map( reset => '0', 
            clkb => cbc_clk_in, 
            dina => cs_ctrl.io.cbc_data_rcv.timing_scan_bram_reset, 
            doutb => bram_reset
  );       
 iserdes_cnd_scan_bram_rd_reset_gen : entity work.dff_sync_edge_detect
   port map( reset => '0', 
             clkb => ipb_clk, 
             dina => cs_ctrl.io.cbc_data_rcv.timing_scan_bram_rd_reset, 
             doutb => bram_rd_reset
   );       

    
	iserdes_cnd_scan_bram_ctrl_inst : entity work.cbc_data_clock_timing_scan_bram_ctrl
	generic map( NCBC => NCBC, NFE => NFE )
    port map(   cbc_clk                   => cbc_clk_in,
                ipb_clk                   => ipb_clk,
                reset                     => bram_reset, 
                rd_reset                  => bram_rd_reset,
                bram_to_iserdes_cnd_array => bram_to_iserdes_cnd_array,
				iserdes_cnd_to_bram_array => iserdes_cnd_to_bram_array,
                read_next                 => iserdes_cnd_scan_bram_read_next,
                dout_o                    => iserdes_cnd_scan_bram_dout,
                nrdata_o                   => bram_nrdata
    );

	ipb_fifo_read_if_inst : entity work.ipb_fifo_read_if
	port map(
		clk					=> ipb_clk,
		reset				=> ipb_reset,
		ipb_mosi_i	        => iserdes_cnd_scan_bram_ipb_mosi_i,
		ipb_miso_o          => iserdes_cnd_scan_bram_ipb_miso_o,
		dout				=> iserdes_cnd_scan_bram_dout,
		read_next_o         => iserdes_cnd_scan_bram_read_next
	);
end Behavioral;
