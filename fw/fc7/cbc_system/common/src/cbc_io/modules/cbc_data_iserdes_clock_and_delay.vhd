----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.02.2018 18:40:21
-- Design Name: 
-- Module Name: cbc_data_iserdes_clock - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity cbc_data_iserdes_clock_and_delay is
  Port ( 
	cbc_clk_in										: in  std_logic;
	idelay_offset									: in  std_logic_vector(4 downto 0);
	cnd_tuning_reset              : in  std_logic;
	cnd_tuning_tune               : in  std_logic;
	cnd_tuning_scan_pulse         : in  std_logic;
	cnd_tuning_fsm                : out std_logic_vector( 3 downto 0); 
	cnd_tuning_idelay_ctrl_fsm    : out std_logic_vector( 2 downto 0); 
	cnd_tuning_mmcme2_drp_saddr   : out std_logic_vector(1 downto 0);
	in_delay_tap_out			  : in  std_logic_vector(5*6-1 downto 0);
	cbc_par_data		      : in  std_logic_vector(6*8-1 downto 0);
    mmcm_iserdes_locked_o         : out std_logic;
	pulse_scan_done               : out std_logic;
	to_iserdes_clk_d8             : out std_logic;
	to_iserdes_clk                : out std_logic;
	to_iserdes_in_delay_reset			: out std_logic;
	to_iserdes_in_delay_ce				: out std_logic_vector(5 downto 0);
	to_iserdes_in_delay_tap_in    : out std_logic_vector(5*6-1 downto 0);
	to_iserdes_in_delay_data_inc  : out std_logic_vector(5 downto 0);
	to_iserdes_iserdes_io_reset	  : out std_logic;
	to_iserdes_bitslip					  : out std_logic_vector(5 downto 0);     
	bitslip_counter               : out std_logic_vector(2 downto 0);
	from_bram_ready               : in  std_logic; 
	to_bram_req                   : out std_logic; 
  to_bram_wen                   : out std_logic;
  to_bram_waddr                 : out std_logic_vector(9 downto 0);
  to_bram_din                   : out std_logic_vector(15 downto 0);
  mmcme2_drp_sen_o                : out std_logic;
  mmcme2_drp_rst_o                : out std_logic;  	
  mmcme2_drp_srdy_o               : out std_logic  
  );
end cbc_data_iserdes_clock_and_delay;

architecture Behavioral of cbc_data_iserdes_clock_and_delay is

  signal iserdes_clk_d8                                   : std_logic;
  signal iserdes_clk                                      : std_logic;
    
	signal cbc_par_data_slvs5                          : std_logic_vector(7 downto 0);

	signal mmcm_iserdes_daddr                          : std_logic_vector(6 downto 0);
	signal mmcm_iserdes_dclk                           : std_logic;
	signal mmcm_iserdes_den                            : std_logic;
	signal mmcm_iserdes_din                            : std_logic_vector(15 downto 0);
	signal mmcm_iserdes_dout                           : std_logic_vector(15 downto 0);
	signal mmcm_iserdes_dwe                            : std_logic;
	signal mmcm_iserdes_drdy                           : std_logic;
	signal mmcm_iserdes_locked                         : std_logic;
	signal mmcm_iserdes_rst_mmcm                       : std_logic;    

	signal mmcme2_drp_sen     : std_logic;
	signal mmcme2_drp_saddr   : std_logic_vector(1 downto 0);
	signal mmcme2_drp_rst     : std_logic;  	
	signal mmcme2_drp_srdy    : std_logic;
	
begin
       
	to_iserdes_clk_d8 <= iserdes_clk_d8;
	to_iserdes_clk    <= iserdes_clk;
	cbc_par_data_slvs5 <= cbc_par_data(5*8-1 downto 4*8);
	mmcm_iserdes_locked_o <= mmcm_iserdes_locked;

    mmcme2_drp_sen_o <= mmcme2_drp_sen;
    mmcme2_drp_rst_o <= mmcme2_drp_rst;
    mmcme2_drp_srdy_o <= mmcme2_drp_srdy;
        
	cbc_data_iserdes_clocks_mmcm_inst: entity work.cbc_data_iserdes_clocks_mmcm
	port map
	 (-- Clock in ports
		clk_in1          => cbc_clk_in,
		-- Clock out ports
		clk_out1         => iserdes_clk_d8,
		clk_out2         => iserdes_clk,
		-- Dynamic phase shift ports
		daddr            => mmcm_iserdes_daddr,
		dclk             => mmcm_iserdes_dclk,
		den              => mmcm_iserdes_den,
		din              => mmcm_iserdes_din,
		dout             => mmcm_iserdes_dout,
		dwe              => mmcm_iserdes_dwe,
		drdy             => mmcm_iserdes_drdy,
		-- Status and control signals
		reset            => mmcm_iserdes_rst_mmcm,
		locked           => mmcm_iserdes_locked
	 );

	mmcme2_drp_for_cbc_system_inst : entity work.mmcme2_drp_for_cbc_system
	port map
	(
		SADDR    => mmcme2_drp_saddr,
		SEN      => mmcme2_drp_sen,
		SCLK     => cbc_clk_in,
		--    RST      => mmcme2_drp_sigs_in.rst or ipb_cbc_system_ctrl.global.reset,
		RST      => mmcme2_drp_rst,
		SRDY_O   => mmcme2_drp_srdy,
		-- These signals are to be connected to the MMCM_ADV by port name.
		-- Their use matches the MMCM port description in the Device User Guide.
		DO       => mmcm_iserdes_dout,
		DRDY     => mmcm_iserdes_drdy,
		LOCKED   => mmcm_iserdes_locked,
		DWE      => mmcm_iserdes_dwe,
		DEN      => mmcm_iserdes_den,
		DADDR    => mmcm_iserdes_daddr,
		DI       => mmcm_iserdes_din,
		DCLK     => mmcm_iserdes_dclk,
		RST_MMCM => mmcm_iserdes_rst_mmcm,
		SADDR_O  => cnd_tuning_mmcme2_drp_saddr
	);

	cbc_data_iserdes_clock_tuning_inst : entity work.cbc_data_iserdes_clock_tuning
	generic map( TUNE_CLOCK => true )
		port map
		(
		iserdes_clk_d8            => iserdes_clk_d8, 
		idelay_offset_i           => idelay_offset,
		reset_i                   => cnd_tuning_reset,
		tune_i                    => cnd_tuning_tune,
		scan_pulse_i              => cnd_tuning_scan_pulse,
		sync_pattern              => "10000000",                
		mmcme2_drp_sclk           => cbc_clk_in,
		mmcme2_drp_srdy           => mmcme2_drp_srdy,
		mmcme2_drp_current_saddr  => mmcme2_drp_saddr,          
		in_delay_tap_out          => in_delay_tap_out,
		cbc_par_data_slvs5        => cbc_par_data_slvs5,                            
		fsm_o                     => cnd_tuning_fsm,
		iserdes_idelay_ctrl_fsm_o => cnd_tuning_idelay_ctrl_fsm,
		to_iserdes_in_delay_reset    =>	to_iserdes_in_delay_reset,    
		to_iserdes_in_delay_ce			 =>	to_iserdes_in_delay_ce,			 
		to_iserdes_in_delay_tap_in   => to_iserdes_in_delay_tap_in,   
		to_iserdes_in_delay_data_inc => to_iserdes_in_delay_data_inc, 
		to_iserdes_iserdes_io_reset  =>	to_iserdes_iserdes_io_reset,  
		to_iserdes_bitslip				   => to_iserdes_bitslip,				   	
		mmcme2_drp_rst_o             => mmcme2_drp_rst,
		mmcme2_drp_sen_o             => mmcme2_drp_sen,
		mmcme2_drp_saddr_o           => mmcme2_drp_saddr,
		bitslip_counter_o         => bitslip_counter,
		done                      => open,
		pulse_scan_done_o         => pulse_scan_done,
		test_out                  => open,
		from_bram_ready     => from_bram_ready,
		to_bram_req     	  => to_bram_req  ,
		to_bram_wen     	  => to_bram_wen  ,
		to_bram_waddr       => to_bram_waddr,
		to_bram_din     	  => to_bram_din  
 );     

        -- this part may not be needed any more.
 --       process (clk_40MHz_in)
 --       begin
 --           if rising_edge(clk_40MHz_in) then
 --               mmcme2_drp_rst <= '0';
--                mmcme2_drp_sen <= '0';
--                if sigs_to_mmcme2_drp_from_dctt_set(j).sen = '1' then
--                    mmcme2_drp_saddr <= sigs_to_mmcme2_drp_from_dctt_set(j).saddr;
--                    mmcme2_drp_sen   <= '1';
--                end if;
--                if sigs_to_mmcme2_drp_from_dctt_set(j).rst = '1' then
--                    mmcme2_drp_rst <= '1';
--                end if;
--            end if;
--        end process;		


--        mmcme2_drp_sclk  <= mmcme2_drp_sclk when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).sclk;
--        mmcme2_drp_saddr <= ipb_cbc_system_cnfg.mmcme2_drp.saddr when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).saddr;
--        mmcme2_drp_rst   <= ipb_cbc_system_ctrl.mmcme2_drp.rst   when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).rst;
--        mmcme2_drp_sen   <= ipb_cbc_system_ctrl.mmcme2_drp.sen   when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).sen;    

--        ipb_cbc_system_stat.mmcme2_drps(i).srdy                  <= sigs_from_mmcme2_drp_set(i).srdy when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else '0';
--        ipb_cbc_system_stat.cbc_system_clocks_1_set(i).saddr     <= sigs_from_mmcme2_drp_set(i).saddr_o;
--        sigs_from_mmcme2_drp_to_io_set(i).srdy                   <= sigs_from_mmcme2_drp_set(i).srdy when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '0' else '0';
--        sigs_from_mmcme2_drp_to_io_set(i).saddr                  <= sigs_from_mmcme2_drp_set(i).saddr_o;
         
--    end generate;

end Behavioral;
