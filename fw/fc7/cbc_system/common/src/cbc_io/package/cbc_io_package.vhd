--===========================--
-- cbc_io_package
-- 28.10.2016 Kirika Uchida
--===========================--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

package cbc_io_package is

	--cbc_data_iserdes_clk_and_delay to bram signals 
  type bram_to_iserdes_cnd_type is
  record
      ready           : std_logic;
  end record;
  type bram_to_iserdes_cnd_array_type is array (0 to MAX_N_FE-1) of bram_to_iserdes_cnd_type;

	--bram to cbc_data_iserdes_clk_and_delay signals 
	type iserdes_cnd_to_bram_type is
  record
      req    : std_logic;
      wen    : std_logic;
      waddr  : std_logic_vector(9 downto 0);
      din    : std_logic_vector(15 downto 0);
  end record;   
	type iserdes_cnd_to_bram_array_type is array (0 to MAX_N_FE-1) of iserdes_cnd_to_bram_type;


end cbc_io_package;

