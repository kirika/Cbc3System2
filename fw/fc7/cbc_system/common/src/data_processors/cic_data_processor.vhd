----------------------------------------------------------------------------------
-- 
-- Author: Kirika Uchida
-- 
-- Create Date: 09.02.2018 
-- Design Name: 
-- Module Name: cic_data_processor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 	lpgbt data are put in shift registers which can store n_cluster_block (8 x n) clusters
-- 	8 x 14 = 7 x 16 --> 8 clusters match 7 words of lpgbt.
-- 	shift register lpgbt_sr outputs are fanned out to 16 groups (cic_frame_set) for 0 to 15 bit-shifts. 
-- 	cic frame is selected by the # of bit to shift (bscount) obtained in the fsm state : idle.
-- 	cluster data block is latched at get_header and every n clock while cluster to read in the frame is left.
-- 	The cluster data in lpgbt shift registers are erased for the next frame header search.
-- 	fsm 
-- 	idle             : searches for frame header	
--  	                  get the # of bit to shift from the frame header and go to get_header
-- 	get_header       : get event info and # of clusters and go to process_clusters.
-- 	process_clusters : get 2 clusters and write to fifo at 240 MHz. Once all the clusters are written,
--  	                  write the header to header fifo and go to the state idle.
-- 
-- Dependencies: cbc_system_package
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;


entity cic_data_processor is
generic (lpgbt_data_size : integer := 16; lpgbt_sr_size : integer := 4);
port (
     reset       : in std_logic;
     clk_in      : in std_logic;
     be_id       : in std_logic_vector(6 downto 0);
     fe_id       : in std_logic_vector(2 downto 0);
     cic_id      : in std_logic_vector(4 downto 0);
     cbc_active  : in std_logic;
     clk_320MHz  : in std_logic;
     lpgbt_data  : in std_logic_vector(15 downto 0);
     ipb_ctrl    : in ipb_cic_ctrl_type;
     ipb_cnfg    : in ipb_cic_cnfg_type;
     ipb_stat    : out ipb_cic_stat_type;
     l1a_320MHz  : in std_logic;
     dsoebi      : out dsoebi_type;
     dsiebo      : in  dsiebo_type
 );
end cic_data_processor;

architecture Behavioral of cic_data_processor is

	constant n_min_cluster_block : integer := 8;
	constant n_mcb               : integer := 2;
	constant n_cluster_block     : integer := n_min_cluster_block * n_mcb;
	constant lpgbt_sr_hp         : integer := 15;

    type fsm_fg_type        is (idle, get_header, process_clusters);
    type lpgbt_sr_type      is array (0 to lpgbt_sr_hp) of std_logic_vector(lpgbt_data_size-1 downto 0);
    type cic_frame_type     is array (1 to lpgbt_sr_hp) of std_logic_vector(lpgbt_data_size-1 downto 0);
    type cic_frame_set_type is array (0 to lpgbt_data_size - 1) of cic_frame_type;
    type clusters_type      is array (0 to n_cluster_block-1) of std_logic_vector(13 downto 0);
    
    signal fsm_fg        : fsm_fg_type;
    signal lpgbt_sr      : lpgbt_sr_type;
    signal ssdata0       : std_logic_vector(lpgbt_data_size-1 downto 0);
    signal ssdata1       : std_logic_vector(lpgbt_data_size-1 downto 0);
    signal cic_frame_set : cic_frame_set_type;
		
	-- header fifo signals
    signal hd_fifo_wr_en  : std_logic;
    signal hd_fifo_rd_en  : std_logic;
    signal hd_fifo_din    : std_logic_vector(63 downto 0);
    signal hd_fifo_dout   : std_logic_vector(63 downto 0);
    signal hd_fifo_full   : std_logic;
    signal hd_fifo_empty  : std_logic;
    signal hd_fifo_dcount : std_logic_vector(5 downto 0);
	-- data fifo signals
    signal data_fifo_wr_en   : std_logic;
    signal data_fifo_rd_en   : std_logic;    
    signal data_fifo_din     : std_logic_vector(31 downto 0);
    signal data_fifo_dout    : std_logic_vector(31 downto 0);
    signal data_fifo_full    : std_logic;
    signal data_fifo_empty   : std_logic;    
    signal data_fifo_rd_data_count : std_logic_vector(11 downto 0);
    signal data_fifo_wr_data_count : std_logic_vector(11 downto 0);

begin

	--===========================================--
	section_data_sender_inst : entity work.section_data_sender
	--===========================================--
	Port map(
		reset			              => reset,
		clk_320MHz 		          => clk_320MHz,
		hd_fifo_rd_en	          => hd_fifo_rd_en,
		hd_fifo_empty	          => hd_fifo_empty,
		hd_fifo_dout	          => hd_fifo_dout,
		data_fifo_empty 				=> data_fifo_empty,
		data_fifo_dout  				=> data_fifo_dout,
		from_eb_read_en         => dsiebo.read_en,
		to_eb_packet_ready      => dsoebi.data_packet_ready,
		to_eb_data_packet_nword => dsoebi.data_packet_nword,
		to_eb_l1_count          => dsoebi.l1_count,
		to_eb_data_packet_valid => dsoebi.data_packet_valid,
		to_eb_data_packet_out   => dsoebi.data_packet_out
	);

	------------------------------------------------
	-- cic data processor implementation follows.
	------------------------------------------------
	
	-- section_data_info_fifo : Independent Clocks Distributed RAM, read latency 1.
	-- 	in  : width = 64, depth = 64
	--	out : width = 64, depth = 64
	-- one word info per event : refer to section_data_sender.vhd
    hd_fifo_inst : entity work.section_data_info_fifo
    port map(     
        rst        => reset,
        wr_clk     => clk_in,
        rd_clk     => clk_320MHz,	
        din        => hd_fifo_din,
        wr_en      => hd_fifo_wr_en,
        rd_en      => hd_fifo_rd_en,
        dout       => hd_fifo_dout,
        full       => hd_fifo_full,
        empty      => hd_fifo_empty,
        wr_data_count => hd_fifo_dcount
    );    

    -- data fifo : Independent Clocks Block RAM
	--  in   : width = 32, depth = 4096
	--  out  : width = 32, depth = 4096
	-- 2K is enough for 32 events
    cic_data_fifo_inst: entity work.section_data_fifo
    PORT map(
        rst      => reset,
        wr_clk   => clk_in,
        rd_clk   => clk_320MHz,
        din   => data_fifo_din,
        wr_en => data_fifo_wr_en,
        rd_en => data_fifo_rd_en,
        dout  => data_fifo_dout,
        full  => data_fifo_full,
        empty => data_fifo_empty,
        rd_data_count => data_fifo_rd_data_count,
        wr_data_count => data_fifo_wr_data_count,
        wr_rst_busy => open,
        rd_rst_busy => open
    );
	
    ssdata0 <= not lpgbt_sr(lpgbt_sr_hp);				
    ssdata1 <= not lpgbt_sr(lpgbt_sr_hp-1);
    
	-- shift register lpgbt_sr outputs are fanned out to 16 groups for 0 to 15 bit-shifts. 
	cic_frame_set_gen:
	for i in 1 to lpgbt_sr_hp generate
        cic_frame_set(0)(i) <= lpgbt_sr(i);
         cic_frame_gen: 
		 for ii in 1 to lpgbt_data_size - 1 generate
             cic_frame_set(ii)(i) <= lpgbt_sr(i)(lpgbt_data_size-ii-1 downto 0) & lpgbt_sr(i-1)(lpgbt_data_size-1 downto lpgbt_data_size-ii);
         end generate cic_frame_gen;
     end generate cic_frame_set_gen;         

            	
    frame_getter : process (reset, clk_in)                
    variable bscount                : integer range 0 to 15;
	variable cic_frame              : cic_frame_type;
    variable clusters               : clusters_type;
    variable cic_frame_cleared      : cic_frame_type;
    variable clusters_cleared       : clusters_type;
    variable left_clust_count       : integer range 0 to 127;
    variable clust_count            : integer range 0 to n_cluster_block;  
    variable n_cl_the_block         : integer range 0 to n_cluster_block;
    variable data_frame_timer       : integer range 0 to lpgbt_sr_hp;
    variable cbc_error              : std_logic_vector(8 downto 0);
    variable l1id_slv               : std_logic_vector(8 downto 0);   
    variable l1id                   : unsigned(8 downto 0);      
    variable nb_clust               : unsigned(6 downto 0);  
	variable dsize                  : unsigned(6 downto 0);
    variable next_data              : std_logic_vector(31 downto 0);
    begin    
        if reset = '1' then
            data_frame_timer := 7;
            for i in 0 to lpgbt_sr_size - 1 loop
                lpgbt_sr(i) <= (others => '0');
            end loop;
            hd_fifo_wr_en <= '0';
            data_fifo_wr_en <= '0';
            data_frame_timer := 7;
        elsif rising_edge(clk_in) then

            -- clear fifo enable          
            hd_fifo_wr_en <= '0';
            data_fifo_wr_en <= '0';

           -- shift register for lpgbt data
            lpgbt_sr(0) <= lpgbt_data;    			
			for i in 0 to lpgbt_sr_hp - 1 loop
				lpgbt_sr(i+1) <= lpgbt_sr(i);
			end loop;

			if data_frame_timer = 0 then        
			-- This condition meets at the beginning of process_clusters and every 8 clock after that 
			-- only when left_clust_count /= 0 
			--
			-- The variable [clusters] are filled with the bscount shifted cic_frame.
			-- left_clust_count and clust_count are updated.
			-- the valid cluster data for this block in the shift register lpgbt_sr are errased.
				
				cic_frame := cic_frame_set(bscount);
				for i in 0 to n_mcb - 1 loop
					clusters(i*8+0) := cic_frame(lpgbt_sr_hp-i*7)  ( 6 downto 0) & cic_frame(lpgbt_sr_hp-i*7-1)(15 downto  9);
					clusters(i*8+1) := cic_frame(lpgbt_sr_hp-i*7-1)( 8 downto 0) & cic_frame(lpgbt_sr_hp-i*7-2)(15 downto 11);
					clusters(i*8+2) := cic_frame(lpgbt_sr_hp-i*7-2)(10 downto 0) & cic_frame(lpgbt_sr_hp-i*7-3)(15 downto 13);
					clusters(i*8+3) := cic_frame(lpgbt_sr_hp-i*7-3)(12 downto 0) & cic_frame(lpgbt_sr_hp-i*7-4)(15 downto 15);
					clusters(i*8+4) := cic_frame(lpgbt_sr_hp-i*7-4)(14 downto 1);
					clusters(i*8+5) := cic_frame(lpgbt_sr_hp-i*7-4)( 0 downto 0) & cic_frame(lpgbt_sr_hp-i*7-5)(15 downto  3);
					clusters(i*8+6) := cic_frame(lpgbt_sr_hp-i*7-5)( 2 downto 0) & cic_frame(lpgbt_sr_hp-i*7-6)(15 downto  5);
					clusters(i*8+7) := cic_frame(lpgbt_sr_hp-i*7-6)( 4 downto 0) & cic_frame(lpgbt_sr_hp-i*7-7)(15 downto  7);  
				end loop;
				
                -- check how many clusters I should read for this block.                
                if left_clust_count >= n_cluster_block then
                    n_cl_the_block := n_cluster_block;
                    left_clust_count := left_clust_count - n_cluster_block;
                else
                    n_cl_the_block := left_clust_count;
                    left_clust_count := 0;
                end if;  
                -- counter initialized
                clust_count := 0;       
				
                -- erase the data in lpgbt which was just latched.
                for i in 0 to n_cluster_block - 1 loop
                    if n_cl_the_block > i then
                        clusters_cleared(i) := (others => '0');
                    else
                        clusters_cleared(i) := clusters(i);
                    end if;
                end loop;
                
				cic_frame_cleared(lpgbt_sr_hp)(15 downto 7) := (others => '0');
                for i in 0 to n_mcb - 1 loop
					cic_frame_cleared(lpgbt_sr_hp-i*7)(6 downto 0) := clusters_cleared(i*8+0)(13 downto 7);
					cic_frame_cleared(lpgbt_sr_hp-i*7-1) := clusters_cleared(i*8+0)(6 downto 0) & clusters_cleared(i*8+1)(13 downto 5);
					cic_frame_cleared(lpgbt_sr_hp-i*7-2) := clusters_cleared(i*8+1)(4 downto 0) & clusters_cleared(i*8+2)(13 downto 3);
					cic_frame_cleared(lpgbt_sr_hp-i*7-3) := clusters_cleared(i*8+2)(2 downto 0) & clusters_cleared(i*8+3)(13 downto 1);    
					cic_frame_cleared(lpgbt_sr_hp-i*7-4) := clusters_cleared(i*8+3)(0 downto 0) & clusters_cleared(i*8+4)(13 downto 0) & clusters_cleared(i*8+5)(13 downto 13);
					cic_frame_cleared(lpgbt_sr_hp-i*7-5) := clusters_cleared(i*8+5)(12 downto 0) & clusters_cleared(i*8+6)(13 downto 11);
					cic_frame_cleared(lpgbt_sr_hp-i*7-6) := clusters_cleared(i*8+6)(10 downto 0) & clusters_cleared(i*8+7)(13 downto 9);
					cic_frame_cleared(lpgbt_sr_hp-i*7-7)(15 downto 7) := clusters_cleared(i*8+7)(8 downto 0);
				end loop;
                cic_frame_cleared(lpgbt_sr_hp-(n_mcb-1)*7-7)(6 downto 0) := cic_frame(lpgbt_sr_hp-(n_mcb-1)*7-7)(6 downto 0);

                for i in 3 to lpgbt_sr_hp loop
                    if bscount = 0 then
                        lpgbt_sr(i) <= cic_frame_cleared(i-1);
                    else
                        for j in 1 to 15 loop
                            if j = bscount then
                                lpgbt_sr(i) <= cic_frame_cleared(i)(j-1 downto 0) & cic_frame_cleared(i-1)(15 downto j); 
                            end if;
                        end loop;
                    end if;
                end loop;
                data_frame_timer := 1;  
            end if; 			
 
                     		
            -- data frame processor fsm
            case fsm_fg is
            when idle => -- time = 0
				
				if ssdata0 = x"0001" then
					fsm_fg <= get_header;
					bscount := 0;

				else	
					for i in 1 to 15 loop
						if ssdata0(15 - i downto 0) = std_logic_vector(to_unsigned(0, 16 - i)) and ssdata1(15 downto 16 - i) = std_logic_vector(to_unsigned(1, i)) then       
							fsm_fg <= get_header;
							bscount := i;
						end if;
					
					end loop;
                end if;
            when get_header => -- time = 1
				cic_frame   := cic_frame_set(bscount);
                cbc_error   := cic_frame(lpgbt_sr_hp)(15 downto 7);
                l1id_slv    := (cic_frame(lpgbt_sr_hp)(6 downto 0)) & (cic_frame(lpgbt_sr_hp-1)(15 downto 14));-- cannot convert to unsigned here?
                l1id        := unsigned(l1id_slv);
                nb_clust    := unsigned(cic_frame(lpgbt_sr_hp-1)(13 downto 7));
                clust_count := 0;
                left_clust_count := to_integer(nb_clust);
                dsize := unsigned('0' & nb_clust(6 downto 1));
				if nb_clust(0) = '1' then
					dsize := dsize + 1;
				end if;
				dsize := dsize + 1;
				hd_fifo_din <= "00000" & be_id & fe_id & cic_id & x"000" & x"000" & '0' & "000" &  std_logic_vector(dsize) & '0' & x"00";
                
				data_fifo_din <= "0000000" & cbc_error & l1id_slv & std_logic_vector(nb_clust);
                data_fifo_wr_en <= '1';
                
				lpgbt_sr(lpgbt_sr_hp) <= (others => '0');
				if bscount = 0 then
					lpgbt_sr(lpgbt_sr_hp)(6 downto 0) <= lpgbt_sr(lpgbt_sr_hp-1)(6 downto 0);
				else
					if bscount <= 7 then
						lpgbt_sr(lpgbt_sr_hp)(6 - bscount downto 0) <= lpgbt_sr(lpgbt_sr_hp-1)(6 - bscount downto 0);
					else
						lpgbt_sr(lpgbt_sr_hp) <= (others => '0');
						lpgbt_sr(lpgbt_sr_hp-1)<= ( others => '0');
						lpgbt_sr(lpgbt_sr_hp-1)(22 - bscount downto 0) <= lpgbt_sr(lpgbt_sr_hp-2)(22 - bscount downto 0);
					end if;
				end if;
				if nb_clust = "0000000" then				  
                    hd_fifo_wr_en <= '1'; 
					fsm_fg <= idle;
                else
                    data_frame_timer := 0;
                    fsm_fg <= process_clusters;
                end if;
            when process_clusters => 
                
                if data_frame_timer = lpgbt_sr_hp - 1 then
                    data_frame_timer := 0;
                else
                    if clust_count /= n_cl_the_block then
                        if clust_count = n_cl_the_block - 1 then
                            next_data := x"0000" & "00" & clusters(clust_count);                              
                            clust_count := clust_count + 1;                
                        else
                            next_data := "00" & clusters(clust_count+1) & "00" & clusters(clust_count);                              
                            clust_count := clust_count + 2;
                        end if;       
                        data_fifo_din <= next_data;
                        data_fifo_wr_en <= '1';  
                    else
                        if left_clust_count = 0 then
                            hd_fifo_wr_en <= '1';   
                            fsm_fg <= idle;
                        end if;
                    end if;
                    data_frame_timer := data_frame_timer + 1;
                end if; 
            end case;
		end if;
    end process;         
 
end Behavioral;
