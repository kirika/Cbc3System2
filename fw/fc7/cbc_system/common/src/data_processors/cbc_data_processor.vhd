--===========================--
-- cbc_data_processor
--  consists of
--   + cbc_ser_data_delay      :  cbc serial data timing (slvs5) to the 320MHz clock is tuned.
--   + cbc_data_deser          :  cbc serial data are deserialized 320MHz -> 40MHz
--   + cbc_data_frame_receiver :  data frames are identified.
--   + cbc_data_frame_processor:  doing nothing.  Just adding another clock delay.  For further extension.
--   + trig_data_shift_reg     :  trigger data are put into pipeline.
--   + trig_data_fifo          :  with L1A, the corresponding trigger data are put into the fifo buffer to be matched with the data.
--   + cbc_data_fifo           :  the data from cbc_data_frame_processor are put into the fifo buffer. 8bit in 32 bit out 
--   + hd_fifo      :  data info are put in fifo. One word per event.  
--   + cbc_data_fifo_write_fsm :  trigger data and triggered data are merged and put into the cbc_data_fifo with the header. 8bit in 32bit out
--   + cbc_data_packet_send_fsm:  data are sent out when the data fifo is not empty.  
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity cbc_data_processor is
generic ( CBCDATA : cbcdata_type := raw; DATA_SIZE_W : positive := 8 );
port ( 
    be_id                    : in  std_logic_vector(6 downto 0);
    fe_id                    : in  std_logic_vector(2 downto 0);
    cbc_id                   : in  std_logic_vector(4 downto 0);
    cbc_active               : in  std_logic;
    daq_clk                  : in  std_logic;
    cbc_data_clk             : in  std_logic;
    cbc_data_clk_m8          : in  std_logic;
    cbc_par_data             : in  std_logic_vector(6*8-1 downto 0);
    reset_i                  : in  std_logic;
    frame_counter_reset_i    : in  std_logic;
    l1a_latency              : in  std_logic_vector(8 downto 0);
    trig_data_latency        : in  std_logic_vector(8 downto 0);
    data_fifo_empty_o					 : out std_logic;
    data_fifo_full_o           : out std_logic;
    data_info_fifo_empty_o     : out std_logic;
    data_info_fifo_full_o      : out std_logic;        
    trig_data_fifo_empty_o     : out std_logic;
    trig_data_fifo_full_o      : out std_logic;
    cbc_data_frame_counter_o : out std_logic_vector(31 downto 0);
    l1a                      : in  std_logic;
    l1a_count_o              : out std_logic_vector(31 downto 0);
    to_eb_header_ready       : out std_logic; 
    to_eb_data_packet_ready  : out std_logic;
    to_eb_data_packet_nword  : out std_logic_vector(DATA_SIZE_W - 1 downto 0);  
    to_eb_l1_count           : out std_logic_vector(8 downto 0);  
    to_eb_data_packet_valid  : out std_logic; 
    to_eb_data_packet_out    : out std_logic_vector(31 downto 0); 
    from_eb_read_en          : in  std_logic;
    test1                    : out std_logic;
    test2                    : out std_logic 
);
end cbc_data_processor;

architecture fc7 of cbc_data_processor is

    constant trig_data_fifo_empty_flag_check_time : integer := 25;

  type cbcdata_type is ( raw, hit );    
    constant cbc_data_header_size      : unsigned(7 downto 0) := x"01";
    type cbc_triggered_data_buffer_type is array (0 to 34) of std_logic_vector(7 downto 0);

    -- data frame shift register & buffer
    type cbc_data_frame_buffer_type is array (0 to 34) of std_logic_vector(7 downto 0);
    signal cbc_par_data_sr             : cbc_data_frame_buffer_type;
    signal cbc_data_frame              : cbc_data_frame_buffer_type;
    signal cbc_data_frame_l1_count     : std_logic_vector(8 downto 0);
    signal cbc_data_frame_ready        : std_logic;
    
    signal cbc_data_frame_counter      : unsigned(31 downto 0);
    -- triggered data (result of data frame processing)

    signal cbc_triggered_data          : cbc_triggered_data_buffer_type;
    signal cbc_triggered_data_ready    : std_logic;
    
    -- trigger data pipeline    
    signal trig_data_pipe_l1a_lat      : unsigned(8 downto 0);
    signal trig_data_shift_out         : std_logic_vector(39 downto 0);
    -- trigger data buffer
    signal trig_data_fifo_we           : std_logic;
    signal trig_data_fifo_re           : std_logic;
    signal trig_data_fifo_empty        : std_logic;
    signal trig_data_fifo_full         : std_logic;
    signal trig_data_fifo_werr         : std_logic;
    signal trig_data_fifo_rerr         : std_logic;
    signal trig_data_fifo_din          : std_logic_vector(39 downto 0);
    signal trig_data_fifo_dout         : std_logic_vector(39 downto 0);
    signal trig_data_fifo_dout_tmp         : std_logic_vector(39 downto 0); 
    
    signal trig_data_fifo_empty_d      : std_logic;
          
    -- in byte
    constant cbc_trig_data_size        : unsigned(11 downto 0) := x"005"; -- 5
    constant cbc_data_frame_size       : unsigned(11 downto 0) := x"023"; --35
    signal cbc_triggered_data_size     : unsigned(11 downto 0);
    signal cbc_data_size               : unsigned(11 downto 0);

    signal cbc_data_l1_count_latched   : std_logic_vector(8 downto 0);

    signal cbc_data_packet_header      : std_logic_vector(31 downto 0);
    signal cbc_data_packet_size        : unsigned(11 downto 0);

	-- header fifo signals
    signal hd_fifo_wr_en  : std_logic;
    signal hd_fifo_rd_en  : std_logic;
    signal hd_fifo_din    : std_logic_vector(63 downto 0);
    signal hd_fifo_dout   : std_logic_vector(63 downto 0);
    signal hd_fifo_full   : std_logic;
    signal hd_fifo_empty  : std_logic;
    signal hd_fifo_wr_en_p  : std_logic;
    signal hd_fifo_din_p    : std_logic_vector(63 downto 0);
    
 --   signal hd_fifo_dcount : std_logic_vector(5 downto 0);	
	
    signal cbc_data_fifo_din         : std_logic_vector(7 downto 0);
    signal cbc_data_fifo_wr_en       : std_logic;
    signal cbc_data_fifo_rd_en       : std_logic;
    signal cbc_data_fifo_dout        : std_logic_vector(31 downto 0);
    signal cbc_data_fifo_full        : std_logic;
    signal cbc_data_fifo_empty       : std_logic;   
    signal cbc_data_fifo_rd_count    : STD_LOGIC_VECTOR(6 DOWNTO 0);
    signal cbc_data_fifo_wr_count    : STD_LOGIC_VECTOR(8 DOWNTO 0);        
    signal cbc_data_fifo_wr_index    : unsigned(DATA_SIZE_W-1 downto 0);
    signal data_fifo_dout_to_sender  : std_logic_vector(31 downto 0);
    signal cbc_data_fifo_din_1d         : std_logic_vector(7 downto 0);
    signal cbc_data_fifo_wr_en_1d       : std_logic;
    
    
    type cbc_data_fifo_write_fsm_type is (idle, write_trig_data, wait_triggered_data, write_triggered_data, write_data_paddings, write_data_info);
    type cbc_data_packet_send_fsm_type is (idle, send_data_ready, send_packet);
    signal cbc_data_fifo_write_fsm        : cbc_data_fifo_write_fsm_type;
    signal cbc_data_packet_send_fsm       : cbc_data_packet_send_fsm_type;  

   signal read_data_fifo_en              : std_logic;
	 signal daq_reset                       : std_logic;
	 signal trig_data_shift_reg_reset       : std_logic;
	 signal frame_counter_reset             : std_logic;
    signal write_reset                     : std_logic;
    signal proc_reset                     : std_logic;
    signal reset_cbc_data_clk             : std_logic;
    
	type trig_data_fifo_data_set_type is array (0 to to_integer(cbc_trig_data_size) - 1) of std_logic_vector(7 downto 0);
	signal trig_data_fifo_data_set        : trig_data_fifo_data_set_type;

	signal cbc_data_fifo_din_tmp    : std_logic_vector(7 downto 0); 
    
    signal l1a_count : unsigned(31 downto 0);
    signal l1a_cbc_daq_clk_latched : std_logic;
    
    signal cbc_data_clk_flip    : std_logic;
    signal trig_read_count : unsigned(7 downto 0);
 
begin

    l1a_count_o <= std_logic_vector(l1a_count);

	--===========================================--
	section_data_sender_inst : entity work.section_data_sender2
	--===========================================--
	generic map( DATA_SIZE_WIDTH => DATA_SIZE_W) 
	Port map(
		reset		     	          => daq_reset,
		clk_320MHz 		          => daq_clk,
		hd_fifo_rd_en	          => hd_fifo_rd_en,
		hd_fifo_empty	          => hd_fifo_empty,
		hd_fifo_dout	          => hd_fifo_dout,
		data_fifo_rd_en           => cbc_data_fifo_rd_en,
		data_fifo_empty			=> cbc_data_fifo_empty,
		data_fifo_dout	        => data_fifo_dout_to_sender,
		from_eb_read_en         => from_eb_read_en,
		to_eb_header_ready      => to_eb_header_ready,
		to_eb_data_packet_ready => to_eb_data_packet_ready,
		to_eb_data_packet_nword => to_eb_data_packet_nword,
		to_eb_l1_count          => to_eb_l1_count,
		to_eb_data_packet_valid => to_eb_data_packet_valid,
		to_eb_data_packet_out   => to_eb_data_packet_out
	);

    test1 <= trig_data_fifo_empty;
    test2 <= trig_data_fifo_re;

    data_fifo_dout_to_sender <= cbc_data_fifo_dout(7 downto 0) & cbc_data_fifo_dout(15 downto 8) & cbc_data_fifo_dout(23 downto 16) & cbc_data_fifo_dout(31 downto 24); 
-- cbc_data_fifo in : width = 8, out : width = 32 
    cbc_data_fifo_inst: entity work.cbc_data_fifo
    PORT map(
        rst      => daq_reset,
        wr_clk   => cbc_data_clk_m8, 
        rd_clk   => daq_clk, 
        din      => cbc_data_fifo_din_1d,
        wr_en    => cbc_data_fifo_wr_en_1d,
        rd_en    => cbc_data_fifo_rd_en,
        dout     => cbc_data_fifo_dout,
        full     => cbc_data_fifo_full,
        empty    => cbc_data_fifo_empty,
        rd_data_count => cbc_data_fifo_rd_count,
        wr_data_count => cbc_data_fifo_wr_count,
        wr_rst_busy => open,
        rd_rst_busy => open
    );
    
    data_fifo_full_o  <= cbc_data_fifo_full;
    data_fifo_empty_o <= cbc_data_fifo_empty;
    
-- hd_fifo in : width 64, out : width = 64
-- one word infor per event
    hd_fifo_inst : entity work.section_data_info_fifo_w320mhz_r320mhz
    port map(     
        rst        => daq_reset,
        wr_clk     => cbc_data_clk_m8, 
        rd_clk     => daq_clk,	
        din        => hd_fifo_din,
        wr_en      => hd_fifo_wr_en,
        rd_en      => hd_fifo_rd_en,
        dout       => hd_fifo_dout,
        full       => hd_fifo_full,
        empty      => hd_fifo_empty
--        wr_data_count => hd_fifo_dcount
    );    

	daq_reset_gen : entity work.dff_sync_edge_detect_multi_width_pulse_out
	generic map( WIDTH => 2)
	port map(   reset => '0',
							clkb  => daq_clk,
							dina  => reset_i, 
							doutb => daq_reset 
	);
	write_reset_gen : entity work.dff_sync_edge_detect
	port map(   reset => '0',
							clkb  => cbc_data_clk_m8,
							dina  => reset_i, 
							doutb => write_reset 
	);
	reset_cbc_data_clk_gen : entity work.dff_sync_edge_detect
    port map(   reset => '0',
                            clkb  => cbc_data_clk,
                            dina  => reset_i, 
                            doutb => reset_cbc_data_clk 
    );
	cdp_trig_data_shift_reg_reset : entity work.dff_sync_edge_detect
		port map(   reset => '0',
								clkb  => cbc_data_clk,
								dina  => reset_i,
								doutb => trig_data_shift_reg_reset 
		);        
	cdp_frame_counter_reset : entity work.dff_sync_edge_detect
	port map( reset => '0',
						clkb  => cbc_data_clk,
						dina  => frame_counter_reset_i,
						doutb => frame_counter_reset
						);
					
	l1a_cbc_daq_clk_latched_inst : entity work.dff_sync
                                port map(   
                                            clkb  => cbc_data_clk,
                                            dina  => l1a,
                                            doutb => l1a_cbc_daq_clk_latched 
                                );        					
						
					
    proc_reset <= write_reset;
--------------------
-- cbc data write
--------------------

    --cbc_data_fifo_write : process (write_reset, cbc_data_clk_m8)
    cbc_data_fifo_write : process (reset_cbc_data_clk, cbc_data_clk_m8)
        variable fifo_re_timer            : integer range 0 to 3;
        variable trig_data_fifo_out_ready : boolean;
        variable trig_data_fifo_empty_flag_check_timer : integer range 0 to 25;
        variable trig_data_fifo_empty_flag_valid : boolean;
--		variable cbc_data_fifo_din_pre    : std_logic_vector(7 downto 0);    
    begin
        trig_data_fifo_empty_d <= trig_data_fifo_empty;
        
        --if write_reset = '1' then
        if reset_cbc_data_clk = '1' then
            cbc_data_fifo_wr_index <= (others => '0');
            fifo_re_timer := 0;   
            cbc_data_size <= (others => '0');
            trig_data_fifo_re <= '0';        
            cbc_data_fifo_wr_en <= '0';
            cbc_data_fifo_wr_en_1d <= '0';
            hd_fifo_wr_en <= '0'; 
            hd_fifo_wr_en_p <= '0'; 
            cbc_data_packet_header <= (others=>'0');                
            cbc_data_fifo_write_fsm <= idle;
			trig_data_fifo_out_ready := false;
            trig_read_count <= (others => '0');
        elsif rising_edge(cbc_data_clk_m8) then
            
            trig_data_fifo_re <= '0';                         
            
            hd_fifo_wr_en_p <= '0';
            hd_fifo_wr_en <= hd_fifo_wr_en_p;
            hd_fifo_din <= hd_fifo_din_p;
            
            cbc_data_fifo_wr_en <= '0';
            cbc_data_fifo_wr_en_1d <= cbc_data_fifo_wr_en;
            cbc_data_fifo_din_1d <= cbc_data_fifo_din;
            
            case cbc_data_fifo_write_fsm is
            when idle =>
                cbc_data_fifo_wr_index <= (others => '0');            
                cbc_data_size <= (others => '0');                                                        

                --hd_fifo_wr_en <= '0';
                hd_fifo_wr_en_p <= '0';
                
                cbc_data_fifo_wr_en_1d <= '0';
                cbc_data_fifo_wr_en <= '0';
 
                cbc_data_l1_count_latched <= (others=>'0');                     
                cbc_data_packet_header <= (others=>'0');
                cbc_data_packet_size <= (others=>'0');

				if trig_data_fifo_out_ready then					
					for i in 0 to to_integer(cbc_trig_data_size)-1 loop
					   trig_data_fifo_data_set(i) <= trig_data_fifo_dout((i+1)*8-1 downto i*8);
					end loop;  
--					cbc_data_fifo_din_pre := trig_data_fifo_dout(7 downto 0);               
                    trig_data_fifo_out_ready := false;
                    fifo_re_timer := 0;
                    cbc_data_fifo_write_fsm <= write_trig_data;
                    cbc_data_fifo_din_tmp <= trig_data_fifo_dout(7 downto 0);
                    --cbc_data_fifo_din_tmp <= std_logic_vector(trig_read_count);
				elsif fifo_re_timer = 2 then
				    trig_data_fifo_out_ready := true; 
                    fifo_re_timer := fifo_re_timer + 1; 					
                elsif fifo_re_timer = 1 then                   
                    fifo_re_timer := fifo_re_timer + 1;                   
                elsif fifo_re_timer = 0 then 
                     if trig_data_fifo_empty_d = '0' then
                        trig_data_fifo_re <= '1';
                        trig_read_count <= trig_read_count + 1;
                        fifo_re_timer := fifo_re_timer + 1;
                    end if; 
                end if;
             when write_trig_data =>          
                
                if(cbc_data_fifo_wr_en = '0') then
				    cbc_data_fifo_wr_en <= '1';
					cbc_data_fifo_din <= cbc_data_fifo_din_tmp;
--					cbc_data_fifo_din <= (others => '1');
					cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;
					cbc_data_size <= cbc_data_size + 1;	
			        if cbc_data_fifo_wr_index = cbc_trig_data_size - 1 then
					   cbc_data_fifo_write_fsm <= wait_triggered_data;
				    end if;	
				else
					cbc_data_fifo_din_tmp <= trig_data_fifo_data_set(to_integer(cbc_data_fifo_wr_index));
				end if;				
--				cbc_data_fifo_din_pre := trig_data_fifo_data_set(to_integer(cbc_data_fifo_wr_index));			

            when wait_triggered_data =>
		        cbc_data_fifo_wr_index <= (others => '0');
                if cbc_triggered_data_ready = '1' then
                    cbc_data_l1_count_latched  <= std_logic_vector(cbc_data_frame_l1_count); 
                    cbc_data_fifo_write_fsm <= write_triggered_data;
                	cbc_data_fifo_din_tmp <= cbc_triggered_data(0);
                end if;
            when write_triggered_data =>
				
								if(cbc_data_fifo_wr_en = '0') then
									cbc_data_fifo_wr_en  <= '1';
									cbc_data_fifo_din <= cbc_data_fifo_din_tmp;                    
									cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;
									cbc_data_size <= cbc_data_size + 1;
                        	if cbc_data_fifo_wr_index = cbc_triggered_data_size - 1 then
										cbc_data_fifo_write_fsm <= write_data_paddings;
									end if;
								else
									cbc_data_fifo_din_tmp <= cbc_triggered_data(to_integer(cbc_data_fifo_wr_index));	
								end if;
--				cbc_data_fifo_din_pre := cbc_triggered_data(to_integer(cbc_data_fifo_wr_index));	 
                
                 
            when write_data_paddings =>
                if cbc_data_size(2 downto 0) /= 0 then
                    cbc_data_fifo_wr_en  <= '1';
                    cbc_data_fifo_din <= (others => '0');
                    cbc_data_size <= cbc_data_size + 1;                    
                else
                    cbc_data_fifo_wr_index <= ( others => '0' );
                    cbc_data_fifo_write_fsm <= write_data_info;    
                end if;                    
            when write_data_info =>
                if cbc_data_fifo_wr_index = 1 then
                    hd_fifo_wr_en_p <= '1';
                    hd_fifo_din_p <= cbc_data_packet_header & x"00" & "000" &"00" & std_logic_vector(cbc_data_size(11 downto 2)) & cbc_data_l1_count_latched; 
                    cbc_data_fifo_write_fsm <= idle;  
                else 
                    if cbc_data_fifo_wr_index = 0 then
                        cbc_data_packet_header <= "00000" & be_id & fe_id & cbc_id & "00" & std_logic_vector(cbc_data_size(11 downto 2));
                        cbc_data_packet_size <= ("00" & cbc_data_size(11 downto 2) ) + 1;
                    end if;
                    cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;                      
                end if;                
            end case;
        end if;
    end process;            
    


--------------------
-- cbc_data_frame_receiver
--------------------
    cbc_data_frame_receiver : process (frame_counter_reset, trig_data_shift_reg_reset, cbc_data_clk)
    variable frame_ready : boolean;
    begin
        if trig_data_shift_reg_reset = '1' then
            cbc_par_data_sr         <= (others => (others => '0'));
            cbc_data_frame          <= (others => (others => '0'));
            cbc_data_frame_l1_count <= (others => '0');            
            cbc_data_frame_ready    <= '0';   
            cbc_data_frame_counter <= (others=>'0');
            frame_ready := false;         
        elsif frame_counter_reset = '1' then
                cbc_data_frame_counter <= (others=>'0');
        elsif rising_edge(cbc_data_clk) then
            cbc_par_data_sr(0 to 33) <= cbc_par_data_sr(1 to 34);
            cbc_par_data_sr(34)      <= bitswap(cbc_par_data(6*8 - 1 downto 5*8));
            cbc_data_frame_ready     <= '0';        
            if frame_ready then
                cbc_data_frame_ready <= '1';
                frame_ready := false;
            end if;
            if cbc_par_data_sr(0)(1 downto 0) = "11" and cbc_par_data_sr(34)(7 downto 4) = "0000" then
                for i in 0 to 33 loop 
                    cbc_data_frame(i) <= cbc_par_data_sr(i+1)(1 downto 0) & cbc_par_data_sr(i)(7 downto 2);
                end loop;
                cbc_data_frame(34) <= "00" & cbc_par_data_sr(34)(7 downto 2);
                for i in 0 to 2 loop
                    cbc_data_frame_l1_count(8-i) <= cbc_par_data_sr(1)(5+i);
                end loop;
                for i in 0 to 5 loop
                    cbc_data_frame_l1_count(5-i) <= cbc_par_data_sr(2)(i);
                end loop;
                frame_ready := true;
                cbc_data_frame_counter <= cbc_data_frame_counter + 1;
                cbc_par_data_sr <= (others=>(others => '0'));
            end if;

        end if;
    end process;

-- cbc_data_frame_processor  
    cbc_data_frame_processor : process (proc_reset, cbc_data_clk_m8)
	variable counter : integer range 0 to 7;
	variable cbc_data_frame_ready_320MHz : boolean;
    begin
        if proc_reset = '1' then
            counter := 0;
            cbc_triggered_data <= (others=>(others => '0'));
            cbc_triggered_data_ready <= '0';
			      cbc_data_frame_ready_320MHz := false;
        elsif rising_edge(cbc_data_clk_m8) then
            cbc_triggered_data_ready <= '0';
            counter := counter + 1;
            if cbc_data_frame_ready_320MHz then
               for i in 0 to 34 loop
                    cbc_triggered_data(i) <= cbc_data_frame(i);
                end loop;
                -- have to be 1 only one cycle. cbc_data_frame_ready is 1 for 25 ns.  fix me later.
                cbc_triggered_data_ready <= '1';
            	cbc_data_frame_ready_320MHz := false;
            end if;

            if counter = 0 and cbc_data_frame_ready = '1' then
                cbc_data_frame_ready_320MHz := true; 
            end if;
            
 
        end if;            
    end process;
    cbc_triggered_data_size <= cbc_data_frame_size;


--------------------
-- trigger data
--------------------
-- trigger data pipeline - 40MHz   
    trig_data_shift_reg_inst : ENTITY work.trig_data_shift_reg
    PORT map(
        A   => std_logic_vector(trig_data_pipe_l1a_lat),
        D   => cbc_par_data(5*8-1 downto 0),
        CLK => cbc_data_clk,
        SCLR=> trig_data_shift_reg_reset,
        Q   => trig_data_shift_out
    );
    trig_data_pipe_l1a_lat <= unsigned(l1a_latency) - unsigned(trig_data_latency);     


-- trigger data buffer from l1a to cbc_data_frame_ready 
		-- write @ 40MHz
		-- read @ 320MHz
-- 40 x 32
    trig_data_fifo_inst : entity work.trig_data_fifo_w40mhz_r320mhz
      PORT map(
--        rst     => daq_reset,
        rst     => reset_cbc_data_clk,
        wr_clk	=> cbc_data_clk,
        rd_clk  => cbc_data_clk_m8,
        din     => trig_data_fifo_din,
        wr_en   => trig_data_fifo_we,
        rd_en   => trig_data_fifo_re,
        dout    => trig_data_fifo_dout,
        full    => trig_data_fifo_full,
        empty   => trig_data_fifo_empty
	
      );
--    trig_data_fifo_dout <= (others => '1');
    trig_data_fifo_empty_o <= trig_data_fifo_empty;
    trig_data_fifo_full_o  <= trig_data_fifo_full;
    
    process (cbc_data_clk)
    begin
        if rising_edge(cbc_data_clk) then
            cbc_data_clk_flip <= not cbc_data_clk_flip;
        end if;
    end process;
    
    process (cbc_data_clk, daq_reset)
    begin
        if daq_reset = '1' then
            l1a_count <= (others => '0');
        elsif rising_edge(cbc_data_clk) then 
            trig_data_fifo_we <= '0';
		   if l1a_cbc_daq_clk_latched = '1' then
		   --if l1a = '1' then
				trig_data_fifo_we  <= '1';     
				trig_data_fifo_din <= trig_data_shift_out;
				l1a_count <= l1a_count + 1;
			end if;
       end if;
    end process; 

    data_info_fifo_full_o  <= hd_fifo_full;
    data_info_fifo_empty_o <= hd_fifo_empty;


    cbc_data_frame_counter_o <= std_logic_vector(cbc_data_frame_counter) when cbc_active = '1' else (others => '1');                   
    
end fc7;
