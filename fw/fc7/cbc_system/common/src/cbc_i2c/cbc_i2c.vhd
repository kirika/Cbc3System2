--===========================--
-- cbc_i2c
-- is added.
-- rarp & i2c_eeprom are disabled
-- 28.10.2016 Kirika Uchida
--===========================--
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.cbc_system_config_package.all;
use work.cbc_system_package.all;
library unisim;
use unisim.vcomponents.all;

entity cbc_i2c is 
port
(
	clk_in                         : in  std_logic;
	cs_cnfg                        : in  cs_cnfg_type;
	cs_ctrl                        : in  cs_ctrl_type;
	cs_stat                        : out cs_cbc_i2c_bm_stat_array_type;
	bus_miso_array                 : in  cbc_i2c_bus_miso_array_type;
	bus_mosi_array                 : out cbc_i2c_bus_mosi_array_type; 
	other_com_count_reset          : in  std_logic_vector(NFE-1 downto 0);	
	other_com_fifo_reset           : in  std_logic_vector(NFE-1 downto 0);
	other_com_fifo_we			   : in  std_logic_vector(NFE-1 downto 0);	
	other_com_fifo_din			   : in  std_logic_vector(NFE*32-1 downto 0);	
	other_reply_fifo_reset         : in  std_logic_vector(NFE-1 downto 0);
	bus_waiting                    : out std_logic;
	bus_ready                      : out std_logic;
	bus_trans_all_done             : out std_logic;
	reply_ready                    : out std_logic_vector(NFE-1 downto 0);
	reply_data                     : out std_logic_vector(NFE*32-1 downto 0);
	ipb_clk                        : in  std_logic;
	ipb_reset                      : in  std_logic;
	ipb_mosi_i                     : in  ipb_wbus;
	ipb_miso_o                     : out ipb_rbus
);
end cbc_i2c;

architecture Behavioral of cbc_i2c is
    
	signal bm_reset                       : std_logic_vector(NFE-1 downto 0);
	signal bm_init                        : std_logic_vector(NFE-1 downto 0);
	signal cs_ctrl_bm_reset               : std_logic_vector(NFE-1 downto 0);
	signal cs_ctrl_bm_init                : std_logic_vector(NFE-1 downto 0);
	signal cs_ctrl_bm_reset_fifos         : std_logic_vector(NFE-1 downto 0);
	signal cs_ctrl_bm_com_count_reset     : std_logic_vector(NFE-1 downto 0);
	signal bm_com_fifo_reset              : std_logic_vector(NFE-1 downto 0);
	signal bm_com_count_reset             : std_logic_vector(NFE-1 downto 0);

	signal bm_com_fifo_we                 : std_logic_vector(NFE-1 downto 0);
    signal bm_com_fifo_din                : std_logic_vector(NFE*32-1 downto 0);
	signal bm_reply_fifo_reset            : std_logic_vector(NFE-1 downto 0);
	signal bm_reply_fifo_read_next        : std_logic_vector(NFE-1 downto 0);
	signal bm_reply_fifo_dout             : std_logic_vector(NFE*32-1 downto 0);
    signal bm_reply_fifo_empty            : std_logic_vector(NFE-1 downto 0);

    signal bm_bus_waitings                : std_logic_vector(NFE-1 downto 0);
	signal bm_bus_readies                 : std_logic_vector(NFE-1 downto 0);
	signal bm_trans_all_done              : std_logic_vector(NFE-1 downto 0);

    signal cibm_test_out                  : std_logic_vector(NFE-1 downto 0);
	-- for sequencer?
	signal ci2cbmgr_reply_ready           : std_logic_vector(NFE-1 downto 0);
    
    signal ipb_com_fifo_we                : std_logic_vector(NFE-1 downto 0);
    signal ipb_com_fifo_din               : std_logic_vector(NFE*32-1 downto 0);
begin

  bus_waiting <= get_fe_active_and(bm_bus_waitings, cs_cnfg.fe_array);
  bus_ready   <= get_fe_active_and(bm_bus_readies, cs_cnfg.fe_array);    
  bus_trans_all_done <= get_fe_active_and(bm_trans_all_done, cs_cnfg.fe_array);
	
	--===========================================--    
	cbc_i2c_bus_managers_gen :
	--===========================================--
	for i in 0 to NFE-1 generate 

      cs_stat(i).bus_ready <= bm_bus_readies(i);
      cs_stat(i).bus_waiting <= bm_bus_waitings(i);
      cs_stat(i).reply_fifo_empty <= bm_reply_fifo_empty(i);
      cs_stat(i).n_active_cbcs <= cs_cnfg.fe_array(i).n_active_cbcs;
      
		-- control signals
		bm_reset_pulse : entity work.dff_sync_edge_detect
			port map(reset => '0',
							 clkb  => clk_in,
							 dina  => cs_ctrl.cbc_i2c_bm_array(i).reset,
							 doutb => cs_ctrl_bm_reset(i)
			);
    bm_init_pulse : entity work.dff_sync_edge_detect
      port map(reset => '0',
               clkb  => clk_in,
               dina  => cs_ctrl.cbc_i2c_bm_array(i).init,
               doutb => cs_ctrl_bm_init(i) 
      );
		bm_reset_fifos_pulse : entity work.dff_sync_edge_detect
         port map(   reset => '0',
                     clkb  => clk_in,
                     dina  => cs_ctrl.cbc_i2c_bm_array(i).reset_fifos, 
                     doutb => cs_ctrl_bm_reset_fifos(i)
         );
		bm_com_count_reset_pulse : entity work.dff_sync_edge_detect
          port map(   reset => '0',
                      clkb  => clk_in,
                      dina  => cs_ctrl.cbc_i2c_bm_array(i).reset_com_count, 
                      doutb => cs_ctrl_bm_com_count_reset(i)
          );


        bm_reset(i) <= cs_ctrl_bm_reset(i);
        bm_init(i)  <= cs_ctrl_bm_init(i);

		bm_com_fifo_reset(i)                      <= cs_ctrl_bm_reset_fifos(i) or other_com_fifo_reset(i);
		bm_com_fifo_we(i)                         <= ipb_com_fifo_we(i) or other_com_fifo_we(i); 
		bm_com_fifo_din((i+1)*32 - 1 downto i*32) <= ipb_com_fifo_din((i+1)*32-1 downto i*32) when ipb_com_fifo_we(i) = '1' else
												     other_com_fifo_din((i+1)*32-1 downto i*32) when other_com_fifo_we(i) = '1' else 
													 								       (others => '0');
		bm_com_count_reset(i)                    <= cs_ctrl_bm_com_count_reset(i) or other_com_count_reset(i);
		bm_reply_fifo_reset(i)                    <= cs_ctrl_bm_reset_fifos(i) or other_reply_fifo_reset(i);
		-- control signals end
		cbc_i2c_bus_manager_inst : entity work.cbc_i2c_bus_manager
		generic map (NCBC => NCBC) 
		port map(
				base_clk                => clk_in,
		        bus_id                  => cs_cnfg.fe_array(i).id,
                n_active_cbcs           => cs_cnfg.fe_array(i).n_active_cbcs,
                active_cbcs             => cs_cnfg.fe_array(i).active_cbcs,
				reset                   => bm_reset(i),
			    init                    => bm_init(i), 
				com_fifo_wclk           => clk_in, 
				com_fifo_reset          => bm_com_fifo_reset(i),
				com_fifo_we             => bm_com_fifo_we(i),
				com_fifo_din            => bm_com_fifo_din((i+1)*32 - 1 downto i*32),
				com_fifo_empty_o        => cs_stat(i).com_fifo_empty,
				com_fifo_full_o         => cs_stat(i).com_fifo_full,
				com_count_reset         => bm_com_count_reset(i),
				trans_all_done          => bm_trans_all_done(i),
				reply_fifo_rclk         => ipb_clk, 
				reply_fifo_reset        => bm_reply_fifo_reset(i),
				reply_fifo_read_next    => bm_reply_fifo_read_next(i),
				reply_fifo_dout         => bm_reply_fifo_dout((i+1)*32 - 1 downto i*32), 
				reply_fifo_empty        => bm_reply_fifo_empty(i),
				reply_fifo_full         => cs_stat(i).reply_fifo_full,
				reply_fifo_ndata_o      => cs_stat(i).reply_fifo_ndata, 
				reply_fifo_nrdata_o     => cs_stat(i).reply_fifo_nrdata, 
				reply_ready_o           => reply_ready(i),
				reply_data              => reply_data((i+1)*32 - 1 downto i*32), 
				bus_waiting             => bm_bus_waitings(i),
				bus_ready               => bm_bus_readies(i),
				i2c_bus_miso            => bus_miso_array(i),
				i2c_bus_mosi            => bus_mosi_array(i) 
				
			);     
	end generate cbc_i2c_bus_managers_gen;

	--===========================================--		
	ipb_cbc_i2c_reg_inst: entity work.ipb_cbc_i2c_regs
	--===========================================--	
	port map(	ipb_clk 					  => ipb_clk,
						cbc_ctrl_clk          => clk_in,
						reset				  => ipb_reset,
						ipb_mosi_i	    	  => ipb_mosi_i,
						ipb_miso_o			  => ipb_miso_o,
						reply_fifo_reset      => bm_reply_fifo_reset,
						reply_fifo_read_next  => bm_reply_fifo_read_next,
						reply_fifo_empty      => bm_reply_fifo_empty,
						reply_fifo_dout       => bm_reply_fifo_dout,
						com_fifo_we           => ipb_com_fifo_we,
						com_fifo_din          => ipb_com_fifo_din
	);

end Behavioral;
