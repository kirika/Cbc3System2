--===========================--
-- cbc_i2c_transaction 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--use work.user_package.all;
use work.cbc_system_package.all;

use work.cbc_system_config_package.all;

entity cbc_i2c_transaction is
    generic( NCBC : positive := 1 );
	port ( 
		clk0							: in  std_logic;
		reset   						: in  std_logic;
		initialize                      : in  std_logic;
		bus_id                          : in  std_logic_vector(2 downto 0);
		n_active_cbcs                   : in  std_logic_vector(CBCID_WIDTH-1 downto 0);
		active_cbcs                     : in cs_cbc_cnfg_array_type;
        scl_o                           : out std_logic;
        sda_i                           : in  std_logic; 
        sda_o                           : out std_logic;
        command_fifo_empty              : in  std_logic;
        command_fifo_dout               : in  std_logic_vector(31 downto 0);
        command_fifo_re_o               : out std_logic;
        reset_com_done_count            : in std_logic;
        com_count                       : in  unsigned(9 downto 0);
        all_done                        : out std_logic;
        reply_ready                     : out std_logic;
        reply_fifo_din                  : out std_logic_vector(31 downto 0);
        initialized                     : out std_logic;
        fsm_state_cmd_wait              : out std_logic
--        n_active_cbc_o                  : out std_logic_vector(CBCID_WIDTH-1 downto 0)
     );
end cbc_i2c_transaction;

architecture Behavioral of cbc_i2c_transaction is

  constant I2C_BROADCAST_CBCID : std_logic_vector(CBCID_WIDTH-1 downto 0) := (others => '0');
  type cbci2c_fecdata_set_type    is array (0 to NCBC-1) of std_logic_vector(7 downto 0);

	-- from ipb_cbc_system_cnfg.  loaded at reset
--	signal n_active_cbc			  		    : std_logic_vector( CBCID_WIDTH - 1 downto 0 ) := ( others => '0' );
--    signal active_cbcs                      :  cs_cbc_cnfg_array_type;    

    -- from command fifo
	signal cbc_id							: std_logic_vector( CBCID_WIDTH - 1 downto 0 );
	signal rw							    : std_logic_vector( 1 downto 0 );	
	signal next_cbc_i2c_page				: std_logic;

    -- to command fifo
	signal command_fifo_re		 			: std_logic;		

    -- state
	type fsm_state_type is
		( idle, init, cmd_wait, cmd_prep, cmd_proc );
	signal fsm_state						: fsm_state_type;

    -- to cbc i2c single transaction
	signal next_cbc_i2c_input_ready		    : std_logic;
	signal next_cbc_i2c_cbc_addr			: std_logic_vector( 6 downto 0 );	
	signal next_cbc_i2c_page_enable		    : std_logic;
	signal next_cbc_i2c_page_data			: std_logic_vector( 7 downto 0 );
	signal next_cbc_i2c_rw					: std_logic;	
	signal next_cbc_i2c_regaddr			    : std_logic_vector( 7 downto 0 );
    signal next_cbc_i2c_regdata                : std_logic_vector( 7 downto 0 );
	signal cbc_i2c_regdata_o				: std_logic_vector (7 downto 0);
	signal cbc_i2c_single_trans_done		: std_logic;
	signal cbc_i2c_err						: std_logic;
    signal cbc_wr_ok                        : std_logic;
    -- Frontend Control register values and page information
	signal next_cbc_fec_data			    : std_logic_vector (7 downto 0);
	signal fec_data							: cbci2c_fecdata_set_type;
	signal prev_pages						: std_logic_vector( CBCID_WIDTH - 1 downto 0 );

    -- for reply fifo
    signal reply_cbc_id                      : std_logic_vector(CBCID_WIDTH-1 downto 0);
    signal reply_page                        : std_logic;
    signal reply_rw                          : std_logic;
    signal reply_addr                        : std_logic_vector(7 downto 0);
    signal reply_data                        : std_logic_vector(7 downto 0);
    signal reply_info                        : std_logic;            
--    signal reply_fifo_din                    : std_logic_vector(31 downto 0);
 
    signal com_done_count                    : unsigned(9 downto 0);
    signal com_count_sync                    : unsigned(9 downto 0);
    signal com_count_i2c                     : unsigned(9 downto 0);
            
    signal wdata                             : std_logic_vector(7 downto 0);
begin
	
	process ( clk0, reset )		
		variable ntrans_per_command 			: integer;
		variable done_cntr						: integer;
		variable acbc_index						: integer;
		variable failed                         : boolean;
	begin
	if( reset = '1' ) then
		done_cntr				 			:= 0;
		ntrans_per_command 				    := 0;
		acbc_index							:= NCBC;
		
		fec_data							<= ( others => ( others => '0' ) );	
		prev_pages							<= ( others => '0' );			

		command_fifo_re 					<= '0';

		next_cbc_i2c_input_ready 		<= '0';
		next_cbc_i2c_cbc_addr			    <= (others=>'0');
		next_cbc_i2c_page_enable 		<= '0';				
		next_cbc_fec_data					<= ( others => '0' );
		next_cbc_i2c_rw 					<= '0';		
	
		reply_ready                         <= '0';
		reply_cbc_id 	<= (others=>'0');
		reply_page   	<= '0';				
		reply_rw   		<= '0';	
		reply_addr   	<= (others=>'0');		
		reply_data   	<= (others=>'0');		
		reply_info   	<= '0';	

        com_count_i2c <= (others => '0');
        com_count_sync <= (others => '0');
	
	    wdata <= (others => '0');
		failed := false;
		fsm_state <= idle;	
		
	elsif rising_edge( clk0 ) then
	
	   com_count_sync <= com_count;
	   if com_count_sync = com_count then
	       com_count_i2c <= com_count_sync;
	   end if;
	
	   if reset_com_done_count = '1' then
	   
	       com_done_count <= (others => '0');
	   
	   end if;
			
		if com_count_i2c = com_done_count then
		  all_done <= '1';
		else
		  all_done <= '0';
		end if;	
			
		command_fifo_re <= '0';
		
		next_cbc_i2c_input_ready <= '0';
		reply_ready <= '0';	
		
		case fsm_state is

            when idle =>
                failed := false;

                if initialize = '1' then
                    fsm_state <= init;
                end if;
			-- getting the fec data.
			when init =>

--				cbc_addrset <= cbc_addrset_i;
				-- read the page data.
				if ntrans_per_command = 0 then
					
					next_cbc_i2c_page_enable 	<= '0';
					next_cbc_i2c_rw 			<= '1';
					ntrans_per_command 			:= to_integer(unsigned(n_active_cbcs));	
					acbc_index					:= 0;			
					next_cbc_i2c_cbc_addr 		<= active_cbcs(acbc_index).i2c_address;
					next_cbc_fec_data			<= fec_data(acbc_index);													
					next_cbc_i2c_input_ready <= '1';
				
				elsif cbc_i2c_single_trans_done = '1' then

					reply_ready <= '1';
					fec_data(acbc_index) 		<= cbc_i2c_regdata_o;
					reply_cbc_id	        <= active_cbcs(acbc_index).id;
					reply_page				<= next_cbc_i2c_page;
					reply_rw				<= next_cbc_i2c_rw;
					reply_addr				<= next_cbc_i2c_regaddr;
					reply_data				<= cbc_i2c_regdata_o;
					reply_info 				<= cbc_i2c_err;					
					prev_pages(acbc_index)    <= cbc_i2c_regdata_o(7);	
					if cbc_i2c_err = '1' then
					   failed := true;
					end if;				
					done_cntr := done_cntr + 1;				
					if ( done_cntr = ntrans_per_command ) then
						done_cntr := 0;
						ntrans_per_command := 0;
						if failed = false then
						  fsm_state <= cmd_wait;
						else
						  fsm_state <= idle;
						end if;
					else
						acbc_index						:= acbc_index + 1;
						next_cbc_i2c_cbc_addr 		    <= active_cbcs(acbc_index).i2c_address;
						next_cbc_fec_data				<= fec_data(acbc_index);
						next_cbc_i2c_input_ready 	<= '1';
					end if;
				end if;

			when cmd_wait =>
									
				-- creating one clock command fifo read enable pulse

				if  command_fifo_empty = '0' then 
					command_fifo_re <= '1';
				end if;

				if command_fifo_re = '1' then
					command_fifo_re <= '0';
					fsm_state <= cmd_prep;
				end if;
				
			when cmd_prep =>

				done_cntr				 	:= 0;
				ntrans_per_command 		    := 0;
				acbc_index 					:= 0;

                if ( rw = "00" ) then
                    fsm_state <= cmd_wait;
                    com_done_count <= com_done_count + 1;
                else
                    --fifo out is latched and the next command is created.
                    if ( cbc_id = I2C_BROADCAST_CBCID ) then -- broadcast
                        if ( rw = "11" ) then
                            ntrans_per_command := 2 * to_integer(unsigned(n_active_cbcs));
                        else
                            ntrans_per_command := to_integer(unsigned(n_active_cbcs));
                        end if;
                        next_cbc_i2c_cbc_addr 		<= active_cbcs(acbc_index).i2c_address;
                        next_cbc_fec_data			<= fec_data(acbc_index);
                        if prev_pages(acbc_index) = next_cbc_i2c_page or next_cbc_i2c_regaddr = "00000000" then
                            next_cbc_i2c_page_enable <= '0';
                        else
                            next_cbc_i2c_page_enable <= '1';
                        end if;									
                    else
                        if ( rw = "11" ) then
                            ntrans_per_command := 2;
                        else
                            ntrans_per_command := 1;	
                        end if;
                        acbc_index := NCBC;
                        for i in 0 to NCBC - 1 loop
                            if ( active_cbcs(i).id = cbc_id ) then 
                                acbc_index := i;   
                            end if;
                        end loop;	
                        if acbc_index = NCBC then
                            failed := true;
                            com_done_count <= com_done_count + 1;
                            fsm_state <= cmd_wait;
                        end if;
                        next_cbc_i2c_cbc_addr 		<= active_cbcs(acbc_index).i2c_address;
                        next_cbc_fec_data           <= fec_data(acbc_index);
                        if prev_pages(acbc_index) = next_cbc_i2c_page or next_cbc_i2c_regaddr = "00000000"  then
                            next_cbc_i2c_page_enable <= '0';
                        else
                            next_cbc_i2c_page_enable <= '1';
                        end if;											
                    end if;
                    
                    if ( rw = "11" or rw = "01" ) then
                        next_cbc_i2c_rw <= '0';
                        wdata <= command_fifo_dout(7 downto 0);
                    else
                        next_cbc_i2c_rw <= '1';
                    end if;
    
                    next_cbc_i2c_input_ready   <= '1';
            
                    fsm_state <= cmd_proc;				
                end if;
			-- cbc_i2c_single_transaction reply is latched with the cbc_i2c_trans_done
			-- and next cbc_i2c_command is sent or back to cmd_wait to wait for the next command.
			when cmd_proc =>

				next_cbc_i2c_input_ready <= '0';

				if ( cbc_i2c_single_trans_done = '1' ) then
					
					reply_ready <= '1';
	
					reply_cbc_id		     <= active_cbcs(acbc_index).id;
					reply_rw			     <= next_cbc_i2c_rw;
					reply_addr		         <= next_cbc_i2c_regaddr;
			
					if next_cbc_i2c_page_enable = '1' then
						fec_data(acbc_index) <= next_cbc_i2c_page_data;
					end if;					
					
					if next_cbc_i2c_rw = '0' then -- write
						reply_data	<= next_cbc_i2c_regdata;
						if next_cbc_i2c_regaddr = "00000000" then
							reply_page                  <= next_cbc_i2c_regdata(7);
							prev_pages(acbc_index) 		<= next_cbc_i2c_regdata(7);	
							fec_data(acbc_index) 		<= next_cbc_i2c_regdata;
						else
							reply_page			        <= next_cbc_i2c_page;						
							prev_pages(acbc_index) 		<= next_cbc_i2c_page;						
						end if;						
					else -- read
						reply_data	<= cbc_i2c_regdata_o;
						if next_cbc_i2c_regaddr = "00000000" then
							reply_page					<= cbc_i2c_regdata_o(7);
							prev_pages(acbc_index) 		<= cbc_i2c_regdata_o(7);	
						else
							reply_page					<= next_cbc_i2c_page;			
							prev_pages(acbc_index) 		<= next_cbc_i2c_page;						
						end if;												

					end if;
					
					reply_info 					<= cbc_i2c_err;	
					if cbc_i2c_err = '1' then
                       failed := true;
                    end if;    												

					done_cntr := done_cntr + 1;
					
					if( done_cntr = ntrans_per_command ) then
					
					    if failed = false then
					       com_done_count <= com_done_count + 1;
						   fsm_state <= cmd_wait;
					    else
					       fsm_state <= idle;
					    end if;
					else -- when [ second single cbc with rw = "11" ] or [ cbc_id = I2C_BROADCAST_CBCID ]						
						if cbc_id /= I2C_BROADCAST_CBCID then -- single cbc case
							next_cbc_i2c_rw <= '1';
							next_cbc_i2c_page_enable <= '0';
						else
							if rw = "11" then -- broadcast and w and r case
								if ( done_cntr mod 2 ) = 1 then -- next transaction is read.
									next_cbc_i2c_rw <= '1';				
								else  -- next is write, cbc_id changes.
									next_cbc_i2c_rw <= '0';
									acbc_index				    := acbc_index + 1;									
									next_cbc_i2c_cbc_addr 		<= active_cbcs(acbc_index).i2c_address;
									next_cbc_fec_data			<= fec_data(acbc_index);		
									if prev_pages(acbc_index) = next_cbc_i2c_page or next_cbc_i2c_regaddr = "00000000" then
										next_cbc_i2c_page_enable <= '0';
									else
										next_cbc_i2c_page_enable <= '1';
									end if;
								end if;
							else
								acbc_index					    := acbc_index + 1;							
								next_cbc_i2c_cbc_addr 		    <= active_cbcs(acbc_index).i2c_address;
								next_cbc_fec_data				<= fec_data(acbc_index);	
								if prev_pages(acbc_index) = next_cbc_i2c_page or next_cbc_i2c_regaddr = "00000000" then
									next_cbc_i2c_page_enable <= '0';
								else
									next_cbc_i2c_page_enable <= '1';
								end if;
							end if;
						end if;						
						next_cbc_i2c_input_ready <= '1';	
					end if;									
				end if;			
		end case;		
	end if;
	end process;


	cbc_i2c_single_trans : entity work.cbc_i2c_single_transaction
	generic map( I2C_SCL_PRESCALE => CBC_I2C_PRESCALE )
	port map(
--				i2c_data_fifo_we_o		=>i2c_data_fifo_we,	
--				i2c_wdata_req_o			=>i2c_wdata_req,	
            clk0						=> clk0,
            reset       			    => reset,
            cbc_i2c_input_ready	        => next_cbc_i2c_input_ready,
            cbc_i2c_addr_i 		        => next_cbc_i2c_cbc_addr,
            cbc_i2c_page_en_i		    => next_cbc_i2c_page_enable,
            cbc_i2c_page_data_i 	    => next_cbc_i2c_page_data,
            cbc_i2c_rw_i      	        => next_cbc_i2c_rw,
            cbc_i2c_regaddr_i 	        => next_cbc_i2c_regaddr,
            cbc_i2c_regdata_i 	        => next_cbc_i2c_regdata,
            cbc_i2c_regdata_o		    => cbc_i2c_regdata_o,
            done 						=> cbc_i2c_single_trans_done,
            err 						=> cbc_i2c_err,
            scl_o						=> scl_o,
            sda_i						=> sda_i,
            sda_o						=> sda_o
		);
	
    -- from input fifo
	cbc_id					    	<= command_fifo_dout(CBCID_WIDTH+18 downto 19);
	rw			 					<= command_fifo_dout(18 downto 17);
    -- signals to cbc_i2c_single_trans.
	next_cbc_i2c_page 			    <= command_fifo_dout(16);	
	next_cbc_i2c_page_data 		    <= next_cbc_i2c_page & next_cbc_fec_data(6 downto 0);
	next_cbc_i2c_regaddr 		    <= (others => '0') when fsm_state = init else command_fifo_dout(15 downto 8);
	next_cbc_i2c_regdata 		    <= command_fifo_dout(7 downto 0);
    -- to input fifo
	command_fifo_re_o               <= command_fifo_re;

    cbc_wr_ok <= '1' when wdata = reply_data else '0';

    reply_fifo_din( 31 downto 29 )                       <= bus_id;
    reply_fifo_din( 24 + CBCID_WIDTH - 1 downto 24 )     <= reply_cbc_id;
    reply_fifo_din( 23 downto 22 )                       <= ( others => '0' );    
    reply_fifo_din( 21 )                                 <= cbc_wr_ok;
    reply_fifo_din( 20 )                                 <= reply_info;    
    reply_fifo_din( 19 downto 18 )                       <= ( others => '0' );    
    reply_fifo_din( 17 )                                 <= reply_rw;
    reply_fifo_din( 16 )                                 <= reply_page;
    reply_fifo_din( 15 downto  8 )                       <= reply_addr;
    reply_fifo_din(  7 downto  0 )                       <= reply_data;         
    
    initialized <= '0' when ( fsm_state = idle or fsm_state = init ) else '1';
    fsm_state_cmd_wait <= '1' when fsm_state = cmd_wait else '0';
end Behavioral;

