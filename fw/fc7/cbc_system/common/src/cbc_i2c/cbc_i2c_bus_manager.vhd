--===========================--
-- cbc_i2c_bus_manager 
-- 28.10.2016 Kirika Uchida
--===========================--


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--use work.user_package.all;
use work.cbc_system_package.all;

entity cbc_i2c_bus_manager is
generic ( NCBC : positive := 1 );
	port ( 
        base_clk                        : in  std_logic;
		bus_id                          : in  std_logic_vector(2 downto 0);
        n_active_cbcs                   : in  std_logic_vector(CBCID_WIDTH-1 downto 0);
        active_cbcs                     : in  cs_cbc_cnfg_array_type;
		reset                           : in  std_logic;
		init                            : in  std_logic;
		com_fifo_wclk                   : in  std_logic;
		com_fifo_reset                  : in  std_logic;
		com_fifo_we                     : in  std_logic;
		com_fifo_din                    : in  std_logic_vector(31 downto 0);
		com_fifo_empty_o                  : out std_logic; 
		com_fifo_full_o                   : out std_logic; 
		com_count_reset                 : in  std_logic;
		trans_all_done                  : out std_logic;
		reply_fifo_rclk                 : in  std_logic;
		reply_fifo_reset                : in  std_logic;
		reply_fifo_read_next            : in  std_logic;
		reply_fifo_dout                 : out std_logic_vector(31 downto 0);
		reply_fifo_empty                : out std_logic;
		reply_fifo_full                 : out std_logic;
    reply_fifo_ndata_o              : out std_logic_vector(31 downto 0);
    reply_fifo_nrdata_o             : out std_logic_vector(31 downto 0);
    reply_ready_o                   : out std_logic;
    reply_data                      : out std_logic_vector(31 downto 0);
    bus_waiting                     : out std_logic;
    bus_ready                       : out std_logic;
    i2c_bus_miso                    : in  cbc_i2c_bus_miso_type;
    i2c_bus_mosi                    : out cbc_i2c_bus_mosi_type
    );
end cbc_i2c_bus_manager;

architecture Behavioral of cbc_i2c_bus_manager is

    constant REPLY_FIFO_ADDR_WIDTH           : integer := 16;
    signal com_fifo_re                   : std_logic;
    signal com_fifo_empty                : std_logic;
    signal com_fifo_full                 : std_logic;
    signal com_fifo_dout                 : std_logic_vector(31 downto 0);
    signal reply_ready                       : std_logic;
    signal reply_fifo_din                    : std_logic_vector(31 downto 0);

    signal reply_fifo_ndata                  : std_logic_vector(REPLY_FIFO_ADDR_WIDTH downto 0);
    signal reply_fifo_nrdata                 : std_logic_vector(REPLY_FIFO_ADDR_WIDTH downto 0);
   
    signal com_count                         : unsigned(9 downto 0);
    
begin

    com_fifo_empty_o <= com_fifo_empty;
    com_fifo_full_o  <= com_fifo_full;
 
    reply_fifo_ndata_o <= x"000" & "000" & reply_fifo_ndata;
    reply_fifo_nrdata_o <= x"000" & "000" & reply_fifo_nrdata;
 
 	reply_ready_o <= reply_ready;
 	reply_data <= reply_fifo_din;

 
    cbc_i2c_command_fifo_inst : entity work.cbc_i2c_command_fifo
    PORT map(   
                rst    => com_fifo_reset,
                clk    => com_fifo_wclk,
                din    => com_fifo_din,
                wr_en  => com_fifo_we,
                rd_en  => com_fifo_re,
                dout   => com_fifo_dout,
                full   => com_fifo_full,
                empty  => com_fifo_empty
    );

    process ( com_fifo_wclk, com_count_reset )		
    begin
    if( com_count_reset = '1' ) then
    
        com_count <= (others => '0');
    
    elsif rising_edge( com_fifo_wclk ) then
    
        if com_fifo_we = '1' then
            com_count <= com_count + 1;
        end if;
        
    end if;
    
    end process;
 
    cbc_i2c_transaction_inst : entity work.cbc_i2c_transaction
   generic map( NCBC => NCBC )
    port map(   clk0                    => base_clk,
                reset                   => reset,
                initialize              => init,        
		        bus_id                  => bus_id,
                n_active_cbcs           => n_active_cbcs,
                active_cbcs             => active_cbcs,
                scl_o                   => i2c_bus_mosi.scl,
                sda_i                   => i2c_bus_miso.sda,    
                sda_o                   => i2c_bus_mosi.sda,   
                command_fifo_empty      => com_fifo_empty,
                command_fifo_dout       => com_fifo_dout,
                command_fifo_re_o       => com_fifo_re,
                reset_com_done_count    => com_count_reset,
                com_count               => com_count,
                all_done                => trans_all_done,
                reply_ready             => reply_ready,
                reply_fifo_din          => reply_fifo_din,
                initialized             => bus_ready,
                fsm_state_cmd_wait      => bus_waiting
    );                             
--    ci2cbmgr_to_ipbif.reply_fifo_dout <= reply_fifo_din;
    cbc_i2c_reply_fifo_inst : entity work.cbc_i2c_reply_fifo
    port map ( ci2c_clk    => base_clk,
               read_clk    => reply_fifo_rclk,
               reset       => reply_fifo_reset,
               reply_ready => reply_ready,
               din_i       => reply_fifo_din,
               read_next   => reply_fifo_read_next,      
               dout        => reply_fifo_dout,
               empty_o     => reply_fifo_empty,
               full_o      => reply_fifo_full,
               ndata_o     => reply_fifo_ndata,
               nrdata_o    => reply_fifo_nrdata
               );

 
   

end Behavioral;
