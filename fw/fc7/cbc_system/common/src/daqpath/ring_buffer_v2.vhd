--===========================--
-- ring_buffer 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ring_buffer is
generic( constant ADDR_WIDTH : positive := 16 ); 
port(
    rst_i                      : in  std_logic;
    clk                      : in  std_logic; --320MHz
    clk_slow                 : in  std_logic;
    din                      : in  std_logic_vector(31 downto 0);
    we                       : in  std_logic;
    end_of_event             : in  std_logic;
    trig_readall             : in  std_logic;
    raddr                    : in  std_logic_vector(ADDR_WIDTH-1 downto 0);    
    next_raddr_o             : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    next_rdata               : out std_logic_vector(31 downto 0);
    load_next                : out std_logic;
--    waddr_o                  : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    n_word_all               : out std_logic_vector(ADDR_WIDTH downto 0);
    n_word_events            : out std_logic_vector(ADDR_WIDTH downto 0);    
    n_word_free              : out std_logic_vector(ADDR_WIDTH downto 0);
    n_word_all_slow               : out std_logic_vector(ADDR_WIDTH downto 0);
    n_word_events_slow            : out std_logic_vector(ADDR_WIDTH downto 0);    
    n_word_free_slow              : out std_logic_vector(ADDR_WIDTH downto 0);    
    werr                     : out std_logic;
    rerr                     : out std_logic;
    bram_clka                : out std_logic;
    bram_wea                 : out std_logic;
    bram_addra               : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    bram_dina                : out std_logic_vector(31 downto 0);
    bram_clkb                : out std_logic;
    bram_addrb               : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    bram_doutb               : in  std_logic_vector(31 downto 0);
    update_next_o            : out std_logic
);
end ring_buffer;

architecture Behavioral of ring_buffer is

    constant FIFO_DEPTH       : integer := 2**ADDR_WIDTH;

    signal raddr_1d           : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal raddr_rb           : std_logic_vector(ADDR_WIDTH-1 downto 0);  
    signal next_raddr         : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal next_raddr_1d      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal waddr              : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal waddr_event        : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal diff_addr        : unsigned(ADDR_WIDTH downto 0);
    signal diff_addr_event  : unsigned(ADDR_WIDTH downto 0);          
    signal n_word_all_fast    : std_logic_vector(ADDR_WIDTH downto 0);
    signal n_word_free_fast   : std_logic_vector(ADDR_WIDTH downto 0);
    signal n_word_events_fast : std_logic_vector(ADDR_WIDTH downto 0);    
    signal looped_diff        : boolean;
    signal looped_event_diff  : boolean;
    signal update_next : std_logic;

	type bram_in_count_sigs is
	record
    	bram_wea                 : std_logic_vector(0 downto 0);
    	bram_addra               : std_logic_vector(ADDR_WIDTH-1 downto 0);
    	bram_dina                : std_logic_vector(31 downto 0);
        n_word_all               : std_logic_vector(ADDR_WIDTH downto 0);
        n_word_free              : std_logic_vector(ADDR_WIDTH downto 0);
        n_word_events            : std_logic_vector(ADDR_WIDTH downto 0);       	
	end record;
	
	constant bram_in_npipe : integer := 0;
	type bram_in_count_sigs_pipe_type is array (0 to bram_in_npipe) of bram_in_count_sigs; 
    signal bram_in_count_sigs_pipe : bram_in_count_sigs_pipe_type;
    
    signal rst : std_logic;
    
begin

 db_reset : entity work.dff_sync_edge_detect
  port map(   reset => '0',
              clkb  => clk,
              dina  => rst_i,
              doutb => rst
  );
 
 
            bram_wea      <= bram_in_count_sigs_pipe(bram_in_npipe).bram_wea(0);
            bram_addra    <= bram_in_count_sigs_pipe(bram_in_npipe).bram_addra;
            bram_dina     <= bram_in_count_sigs_pipe(bram_in_npipe).bram_dina;  
            n_word_all    <= bram_in_count_sigs_pipe(bram_in_npipe).n_word_all;
            n_word_free   <= bram_in_count_sigs_pipe(bram_in_npipe).n_word_free;
            n_word_events <= bram_in_count_sigs_pipe(bram_in_npipe).n_word_events;         


--    n_word_all    <= n_word_all_fast;
--    n_word_free   <= n_word_free_fast;
--    n_word_events <= n_word_events_fast;
    next_raddr_o <= next_raddr;
    update_next_o <= update_next;
    bram_clka  <= clk;
--    bram_wea   <= we;
--    bram_addra <= waddr;
--    bram_dina  <= din;
    bram_clkb  <= clk;
    bram_addrb <= next_raddr_1d;
    next_rdata <= bram_doutb;
--    next_rdata <= x"0000" & next_raddr_1d; 

    
    process ( rst, clk_slow )
--    variable count : integer range 0 to 1400;
    begin
        if rst = '1' then
    --        count := 0;
            n_word_all_slow    <= (others => '0');
            n_word_free_slow   <= std_logic_vector(to_unsigned(FIFO_DEPTH, ADDR_WIDTH+1));
            n_word_events_slow <= (others => '0');

        elsif rising_edge( clk_slow ) then
--        if count = 1400 then
--            count := 0;
--        else
--            count := count + 14;
--        end if;
--        n_word_events_slow <= std_logic_vector(to_unsigned(count, 17) );  

        n_word_all_slow     <= n_word_all_fast;
        n_word_free_slow    <= n_word_free_fast;
        n_word_events_slow  <= n_word_events_fast;        
    end if;
    end process;

	process ( clk, rst, trig_readall )
		variable timer            : natural range 0 to 20;
		variable looped           : boolean;
		variable looped_event     : boolean;
		variable pre_looped_event : boolean;
	begin

		if rst = '1' then
	        waddr <= (others =>'0');	
	        waddr_event <= 	(others =>'0');	
            looped           := false;
            looped_event     := false;
            pre_looped_event := false;
            werr <= '0'; 
            next_raddr <= (others=>'0');
            rerr <= '0';
            timer := 1;
--            n_word_events <= (others => '0');
            update_next <= '0';   
            --bram_wea <= '0';
            --bram_addra <= (others =>'0');
            bram_in_count_sigs_pipe <= (others => (others => (others => '0') )); 	
            raddr_rb <= (others=>'0');    
            looped_diff       <= false;
            looped_event_diff <= false;  
            --n_word_free_fast <= std_logic_vector(to_unsigned(FIFO_DEPTH, ADDR_WIDTH+1));
	    elsif trig_readall = '1' then
            waddr <= (others =>'0');
            waddr_event <= (others =>'0');
            next_raddr <= (others => '0');
            update_next <= '0';
            --bram_wea <= '0';
            --bram_addra <= (others =>'0');
            bram_in_count_sigs_pipe <= (others => (others => (others => '0')) ); 	
            looped := true;
            looped_event := true;
            timer := 1;

		elsif rising_edge( clk ) then
            load_next <= '0';
            update_next <= '0';
            raddr_1d <= raddr;
            if raddr_1d = raddr then
                raddr_rb <= raddr_1d;
            end if;
            next_raddr_1d <= next_raddr;
            --bram_in_count_sigs_pipe(0).bram_wea(0) <= '0';

            bram_in_count_sigs_pipe(0).bram_wea(0)   <= we;
            bram_in_count_sigs_pipe(0).bram_addra    <= waddr;
            bram_in_count_sigs_pipe(0).bram_dina     <= din;
            bram_in_count_sigs_pipe(0).n_word_all    <= n_word_all_fast;
            bram_in_count_sigs_pipe(0).n_word_free   <= n_word_free_fast;
            bram_in_count_sigs_pipe(0).n_word_events <= n_word_events_fast;

--            for i in 0 to bram_in_npipe - 1 loop                                
--				bram_in_count_sigs_pipe(i+1).bram_wea(0)   <= bram_in_count_sigs_pipe(i).bram_wea(0);
--				bram_in_count_sigs_pipe(i+1).bram_addra    <= bram_in_count_sigs_pipe(i).bram_addra;
--				bram_in_count_sigs_pipe(i+1).bram_dina     <= bram_in_count_sigs_pipe(i).bram_dina;				
--                bram_in_count_sigs_pipe(i+1).n_word_all    <= bram_in_count_sigs_pipe(i).n_word_all;
--                bram_in_count_sigs_pipe(i+1).n_word_free   <= bram_in_count_sigs_pipe(i).n_word_free;
--                bram_in_count_sigs_pipe(i+1).n_word_events <= bram_in_count_sigs_pipe(i).n_word_events;
--            end loop;       			


            if timer /= 0 then
--                if timer = 3 then
--                    load_next <= '1';
--                    timer := 0;
--                else
--                    timer := timer + 1;
--                end if;

                if timer = 5 then
                    timer := 0;
                else
                    if timer = 4 then
                        load_next <= '1';
                    end if;
                    timer := timer + 1;
                end if;
            else
                -- read operation
                if ( ( looped = true ) or ( waddr /= next_raddr ) ) then

                    if update_next = '0' and raddr_rb = next_raddr then
                        update_next <= '1';
                    end if;
 
--                    rerr <= '0';
--                else
--                    rerr <= '1';
                end if;               
            end if;
            -- read & write address relations, number of data and free space
            if looped = false then
                diff_addr <= unsigned('0'&waddr) - unsigned('0'&raddr_rb); 
                looped_diff <= false;
            else
                diff_addr <= unsigned('0'&raddr_rb) - unsigned('0'&waddr);
                looped_diff <= true;
            end if;
            if looped_diff = false then
                n_word_all_fast <= std_logic_vector( diff_addr );
                n_word_free_fast <= std_logic_vector(FIFO_DEPTH - diff_addr);
            else
                n_word_all_fast  <= std_logic_vector(FIFO_DEPTH - diff_addr );
                n_word_free_fast <= std_logic_vector( diff_addr );
            end if;
            
            if looped_event = false then
                diff_addr_event <= unsigned('0'&waddr_event) - unsigned('0'&raddr_rb);
                looped_event_diff <= false;
            else
                diff_addr_event   <= unsigned('0'&raddr_rb) - unsigned('0'&waddr_event);
                looped_event_diff <= true;
            end if;
            if looped_event_diff = false then
                n_word_events_fast <= std_logic_vector( diff_addr_event );
            else
                n_word_events_fast <= std_logic_vector(FIFO_DEPTH - diff_addr_event );                
            end if;                                                    

           
           -- read operation
           if update_next = '1' then
                next_raddr <= std_logic_vector(unsigned(next_raddr) + 1);
                if looped = false and next_raddr = waddr then
                    rerr <= '0';
                end if;
                if ( to_integer(unsigned(raddr_rb)) = FIFO_DEPTH - 1 ) then
                    next_raddr <= (others => '0');
                    looped := false;
                    looped_event := false;
                end if;
            end if;
            
			-- write operation
			if( we = '1' ) then
                if( ( looped = false ) or ( waddr /= next_raddr ) ) then
                    waddr <= std_logic_vector(unsigned(waddr)+1);
                    if end_of_event = '1' then
                        waddr_event <= std_logic_vector(unsigned(waddr)+1);
                    else
                        waddr_event <= waddr_event;
                    end if;
                    if( to_integer(unsigned(waddr)) = FIFO_DEPTH - 1 ) then
                        waddr <= (others=>'0');
                        if end_of_event = '1' then
                            waddr_event <= (others=>'0');
                        end if;
                        looped := true;
                        pre_looped_event := true;
                    end if;
                    if pre_looped_event = true and end_of_event = '1' then
                        looped_event := true;
                        pre_looped_event := false;
                    end if;      
                    if ( waddr = next_raddr ) then
                        timer := 1;
                    end if;
                    werr <= '0';
                else
                    werr <= '1';
                end if;
          
                
  
--                if end_of_event = '1' then
--                    waddr_event <= std_logic_vector(unsigned(waddr)+1);
--                end if;
            end if;
        end if;
    end process;

--    waddr_o      <= bram_addra;
    

    

end Behavioral;
