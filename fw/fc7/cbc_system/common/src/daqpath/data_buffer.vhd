--===========================--
-- data_buffer 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;

--use work.cbc_system_package.all;

entity data_buffer is
generic(DB_ADDR_W : positive := 17);
port (
  daq_clk                        : in  std_logic; --320MHz
  clk_slow                       : in  std_logic;
  safe_n_word_free_i             : in  std_logic_vector(DB_ADDR_W downto 0);    
  reset_i	  				     : in  std_logic;	
  readall_i			 	         : in  std_logic;	
  we_i                           : in  std_logic;
  data_i                         : in  std_logic_vector(31 downto 0);
  end_of_event_i                 : in  std_logic;
  n_word_all_slow				 : out std_logic_vector(DB_ADDR_W downto 0);
  n_word_events_slow			 : out std_logic_vector(DB_ADDR_W downto 0);       
  n_word_free_slow				 : out std_logic_vector(DB_ADDR_W downto 0);
  n_word_free_o				     : out std_logic_vector(DB_ADDR_W downto 0);	
  werr							 : out std_logic;
  rerr							 : out std_logic;
  waddr							 : out std_logic_vector(DB_ADDR_W-1 downto 0);
  raddr_o						 : out std_logic_vector(DB_ADDR_W-1 downto 0);       
  l1a_veto                       : out std_logic;
  ipb_clk                        : in  std_logic;
  ipb_reset                      : in  std_logic;
  ipb_mosi_i                     : in  ipb_wbus;
  ipb_miso_o                     : out ipb_rbus;
  update_next_o                  : out std_logic
  );
end data_buffer;

architecture fc7_ring_buffer of data_buffer is
    signal n_word_all       : std_logic_vector(DB_ADDR_W downto 0);
    signal n_word_events    : std_logic_vector(DB_ADDR_W downto 0);
    signal n_word_free      : std_logic_vector(DB_ADDR_W downto 0);
    signal safe_n_word_free : std_logic_vector(DB_ADDR_W downto 0);    
    
--    signal waddr            : std_logic_vector(DB_ADDR_W-1 downto 0);
    signal raddr            : std_logic_vector(DB_ADDR_W-1 downto 0);    
    
    signal bram_clka        : std_logic;
    signal bram_we          : std_logic;
    signal bram_addra       : std_logic_vector(DB_ADDR_W - 1 downto 0);
    signal bram_dina        : std_logic_vector(31 downto 0);
    signal bram_clkb        : std_logic;    
    signal bram_addrb       : std_logic_vector(DB_ADDR_W - 1 downto 0);
    signal bram_doutb       : std_logic_vector(31 downto 0);
    
    signal we               : std_logic;
    signal data             : std_logic_vector(31 downto 0);
    signal end_of_event     : std_logic;

    signal reset  : std_logic;
	signal readall : std_logic;

    signal db_to_ipb_next_data             : std_logic_vector(31 downto 0);
    signal db_to_ipb_next_addr             : std_logic_vector(DB_ADDR_W - 1 downto 0);
    signal db_to_ipb_load_next             : std_logic;       
    signal ipb_to_db_addr                  : std_logic_vector(DB_ADDR_W - 1 downto 0);

    signal ipb_db_reset : std_logic;

begin

--   n_word_all_o    <= n_word_all;
   n_word_free_o   <= n_word_free;
   raddr <= ipb_to_db_addr;
   raddr_o <= raddr;


  db_reset : entity work.dff_sync_edge_detect
  port map(   reset => '0',
              clkb  => daq_clk,
              dina  => reset_i,
              doutb => reset
  );
	db_readall : entity work.dff_sync_edge_detect
  port map(   reset => '0',
              clkb  => daq_clk,
              dina  => readall_i,
              doutb => readall
  );

    ipb_db_reset <= reset or ipb_reset;

    process ( reset, daq_clk )
    begin
        if reset = '1' then
            l1a_veto <= '0';
            safe_n_word_free <= safe_n_word_free_i;               
        
        elsif rising_edge(daq_clk) then

            if unsigned(n_word_free) < unsigned(safe_n_word_free) then
                l1a_veto <= '1';
            else
                l1a_veto <= '0';
            end if;
        end if;
    end process;

    process (daq_clk)
    begin
        if rising_edge(daq_clk) then
            we           <= we_i;
            data         <= data_i;
            end_of_event <= end_of_event_i;
        end if;
    end process;


    ring_buffer_inst : entity work.ring_buffer
    generic map( ADDR_WIDTH => DB_ADDR_W )
    port map(
--        rst                        => reset,
        rst_i                        => reset_i,
        clk                        => daq_clk,
        clk_slow                   => clk_slow,
        din                        => data,
        we                         => we,
        end_of_event               => end_of_event,
        trig_readall               => readall,
        raddr                      => raddr,  
        next_raddr_o               => db_to_ipb_next_addr,
        next_rdata                 => db_to_ipb_next_data,
        load_next                  => db_to_ipb_load_next,
 --       waddr_o                    => waddr,
        n_word_all                 => n_word_all,
        n_word_events              => n_word_events,
        n_word_free                => n_word_free,
        n_word_all_slow            => n_word_all_slow,
        n_word_events_slow         => n_word_events_slow,
        n_word_free_slow           => n_word_free_slow,
        werr                       => werr,
        rerr                       => rerr,
        bram_clka                  => bram_clka,
        bram_wea                   => bram_we,
        bram_addra                 => bram_addra,
        bram_dina                  => bram_dina,
        bram_clkb                  => bram_clkb,
        bram_addrb                 => bram_addrb,
        bram_doutb                 => bram_doutb,
        update_next_o              => update_next_o         
    );

    waddr <= bram_addra;


	--===========================================--
	ipb_ring_buffer_data_readout_inst: entity work.ipb_ring_buffer_data_readout
	--===========================================--
	generic map( BUFFER_ADDR_WIDTH => DB_ADDR_W)
	port map
	(   clk                   => ipb_clk,
			reset                 => ipb_db_reset,
			ipb_mosi_i            => ipb_mosi_i,
			ipb_miso_o            => ipb_miso_o,
			next_data             => db_to_ipb_next_data,
			next_addr             => db_to_ipb_next_addr,
			load_next             => db_to_ipb_load_next,       
			addr                  => ipb_to_db_addr
	);



	d64k_data_buffer_gen : if DB_ADDR_W = 16 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d64k_data_buffer_gen;
     
	d128k_data_buffer_gen : if DB_ADDR_W = 17 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram_d128k
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d128k_data_buffer_gen;     

	d256k_data_buffer_gen : if DB_ADDR_W = 18 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram_d256k
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d256k_data_buffer_gen;     
      
      
end fc7_ring_buffer;
