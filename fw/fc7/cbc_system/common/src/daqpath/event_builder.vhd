--===========================--
-- event_builder 
--  dsoebi.data_packet_ready is set to '1' when dsoebi.data_packet_nword & dsoebi.cbc_data_l1_count are available and stays '1' until dsiebo.read_en is set to '1'    
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;
use work.cbc_system_config_package.all;

entity event_builder is
  generic
  ( 
    NDATASECTIONS           : positive := 1; 
    EVENT_BUILDER_ID        : integer  := 1; 
    DELAYED_DATA_TIMEOUT    : positive := 8;
    TDC_NBITS               : positive := 3;
    DS_DATA_ADDR_W          : positive := 17	
  );
  port
  (
    be_id                                 : in  std_logic_vector(BEID_WIDTH-1 downto 0);
    cbc_cnfg_set                          : in  cs_cbc_cnfg_array_type;
    n_active_cbcs                         : in  std_logic_vector(CBCID_WIDTH-1 downto 0);
    daq_clk                               : in  std_logic;
    cbc_clk                               : in  std_logic;
    l1a_fifo_dout                         : in  std_logic_vector(31 downto 0); 
    l1a_fifo_empty                        : in  std_logic; 
    l1a_fifo_re                           : out std_logic;
    dsoebi_array                          : in  dsoebi_array_type;
    dsiebo_array                          : out dsiebo_array_type;    
    ebidbo                                : in  ebidbo_type;    
    ebodbi                                : out ebodbi_type;
    reset_i                               : in  std_logic; 
    write_fsm                             : out std_logic_vector(2 downto 0);  
    packet_send_fsm                       : out std_logic_vector(1 downto 0) 
  );
end event_builder;

architecture Behavioral of event_builder is


  constant data_packet_all_not_ready     : std_logic_vector(NDATASECTIONS-1 downto 0) := (others => '0');

  constant BE_EVENT_DATA_SIZE            : positive := 3;
    --    constant max_event_size                : positive := MAX_SECTION_PACKET_SIZE * NDATASECTIONS + BE_EVENT_DATA_SIZE;

  signal data_packet_all_ready           : std_logic_vector(NDATASECTIONS-1 downto 0);

  signal data_packet_ready               : std_logic_vector(NDATASECTIONS-1 downto 0); 
  signal data_packet_ready_latched       : std_logic_vector(NDATASECTIONS-1 downto 0);         
  signal event_data_size                 : unsigned(EVENT_DATA_SIZE_WIDTH - 1 downto 0);

  signal event_data_fifo_din             : std_logic_vector(31 downto 0);
  signal event_data_fifo_wr_en           : std_logic;
  signal event_data_fifo_rd_en           : std_logic;
  signal event_data_fifo_dout            : std_logic_vector(31 downto 0);
  signal event_data_fifo_full            : std_logic;
  signal event_data_fifo_empty           : std_logic;

  signal event_data_size_fifo_din        : std_logic_vector(EVENT_DATA_SIZE_WIDTH - 1 downto 0);
  signal event_data_size_fifo_wr_en      : std_logic;
  signal event_data_size_fifo_rd_en      : std_logic;
  signal event_data_size_fifo_dout       : std_logic_vector(EVENT_DATA_SIZE_WIDTH - 1 downto 0);
  signal event_data_size_fifo_full       : std_logic;
  signal event_data_size_fifo_empty      : std_logic; 
  signal event_data_size_fifo_werr       : std_logic;   
  signal event_data_size_fifo_rerr       : std_logic;  
  signal event_data_size_fifo_rd_en_not_empty : std_logic;

  signal data_count                      : unsigned(EVENT_DATA_SIZE_WIDTH - 1 downto 0); 

  type event_buffer_write_fsm_type is (idle, wait_for_data, write_header_data_size, write_event_data, set_ds, read_ds);     
  type event_packet_send_fsm_type is (idle, check_data_buffer, send_data);     
  signal event_buffer_write_fsm          :  event_buffer_write_fsm_type;
  signal event_packet_send_fsm           : event_packet_send_fsm_type;

  signal we                              : std_logic;
  signal end_of_event                    : std_logic;

  signal buffer_ready                    : boolean;

  signal data_packet_nword               : std_logic_vector(SECTION_DATA_SIZE_WIDTH-1 downto 0);
  --    signal counter                         : unsigned(10 downto 0);
  signal reset : std_logic;

begin

  eb_reset : entity work.dff_sync_edge_detect
  port map(   
            reset => '0',
            clkb  => daq_clk,
            dina  => reset_i,
            doutb => reset 
          );


  event_data_fifo_inst : entity work.event_data_fifo
  port map (  
             clk   => daq_clk,
             srst  => reset,
             din   => event_data_fifo_din,
             wr_en => event_data_fifo_wr_en,
             rd_en => event_data_fifo_rd_en,
             dout  => event_data_fifo_dout,
             full  => event_data_fifo_full,
             empty => event_data_fifo_empty
           );

  event_data_size_fifo_rd_en_not_empty <= event_data_size_fifo_rd_en and not event_data_fifo_empty;
  event_data_size_fifo : entity work.small_fifo
  generic map (DATA_WIDTH => EVENT_DATA_SIZE_WIDTH, FIFO_DEPTH => 10)
  Port map( 	
            clk 	=> daq_clk,
            rst 	=> reset,
            we  	=> event_data_size_fifo_wr_en,
            din   => event_data_size_fifo_din,
            re 	  => event_data_size_fifo_rd_en_not_empty,
            dout  => event_data_size_fifo_dout,
            empty	=> event_data_size_fifo_empty,
            full	=> event_data_size_fifo_full,
            werr	=> event_data_size_fifo_werr,
            rerr	=> event_data_size_fifo_rerr
          );

--    l1a_count_to_fifo_process : process (clk_320MHz)
--    begin
--        if rising_edge(clk_320MHz) then
--            l1a_count_to_fifo <= std_logic_vector(unsigned(l1a_count)+1);
--        end if;
--    end process;
    
  event_buffer_write : process (reset, daq_clk)
    variable delayed_data_timer : integer range 0 to DELAYED_DATA_TIMEOUT:= 0;
    variable ds_index           : integer range 0 to NDATASECTIONS - 1 := 0;
    variable ds_data_req_index  : integer range 0 to 2**SECTION_DATA_SIZE_WIDTH - 1:= 0;
    variable fifo_timer         : integer range 0 to 1;
    variable add_cbc_data_packet_size : boolean;
    variable add_cbc_data_packet_size_cbc_index : integer range 0 to NDATASECTIONS - 1 := 0;
    variable read_flag          : boolean;	
    variable test_count         : unsigned(31 downto 0);     	
  begin
    if reset = '1' then
      ds_index := 0;
      ds_data_req_index := 0;
      fifo_timer := 0;
      l1a_fifo_re <= '0';
      event_data_fifo_wr_en <=   '0';
      event_data_fifo_din   <= (others => '0');
      event_data_size_fifo_wr_en <= '0'; 
      event_buffer_write_fsm <= idle; 
      data_packet_ready <= (others => '0'); 
      data_packet_ready_latched <= (others => '0');  
      for i in 0 to NDATASECTIONS-1 loop
        dsiebo_array(i).read_en <= '0';
      end loop;   
      data_packet_all_ready <= (others => '1');  
      add_cbc_data_packet_size := false; 
        --             counter <= (others => '0');                      
    elsif rising_edge(daq_clk) then

      l1a_fifo_re <= '0';
      event_data_fifo_wr_en <=   '0';  
      event_data_size_fifo_wr_en <= '0';                       

            --            dsiebo_array(ds_index).read_en <= '0';
      for i in 0 to NDATASECTIONS-1 loop
        data_packet_ready(i) <= dsoebi_array(i).data_packet_ready and cbc_cnfg_set(i).active;
        dsiebo_array(i).read_en <= '0';
      end loop;

      case event_buffer_write_fsm is
        when idle =>
          ds_index := 0;
          for i in 0 to NDATASECTIONS-1 loop
            data_packet_all_ready(i) <=  cbc_cnfg_set(i).active;
          end loop;   
          if fifo_timer = 1 then
            fifo_timer := 0;
            event_buffer_write_fsm <= wait_for_data;
          elsif fifo_timer = 0 then 
            if l1a_fifo_empty = '0' then
              l1a_fifo_re <= '1';
                        --                        data_packet_ready <= (others=>'0');
              delayed_data_timer := 0;
              event_data_size <= to_unsigned(2,EVENT_DATA_SIZE_WIDTH);        
              fifo_timer := fifo_timer + 1;
            end if;
          end if;
        when wait_for_data => -- 1 clock from l1a_fifo_empty = '0'

        -- event data size update for the data section with data_packet_ready set.
          if add_cbc_data_packet_size then
            event_data_size <= event_data_size + (to_unsigned(0, EVENT_DATA_SIZE_WIDTH - SECTION_DATA_SIZE_WIDTH) & unsigned(dsoebi_array(add_cbc_data_packet_size_cbc_index).data_packet_nword) );
            add_cbc_data_packet_size := false;
          end if;

        -- data sections with data_packet_ready set are searched for until data_packet_ready = data_packet_all_ready.
        -- timeout is set to DELAYED_DATA_TIMEOUT 
          if delayed_data_timer > DELAYED_DATA_TIMEOUT then
            event_buffer_write_fsm <= write_header_data_size;
          end if;
          if data_packet_ready /= data_packet_all_not_ready then
            if data_packet_ready_latched /= data_packet_all_ready then
              if data_packet_ready_latched(ds_index) = '0' and data_packet_ready(ds_index) = '1' then
                data_packet_ready_latched(ds_index) <= '1';
                            -- for event data size update
                add_cbc_data_packet_size := true;
                add_cbc_data_packet_size_cbc_index := ds_index;
              end if;                                        
              ds_index := ds_index + 1;
              if ds_index = NDATASECTIONS then                       
                ds_index := 0;
              end if;
              delayed_data_timer := delayed_data_timer + 1;
            else
              event_buffer_write_fsm <= write_header_data_size;                    
            end if;                          
          end if;

        when write_header_data_size => -- 1 clock after data_packet of all sections are ready. time=0           

          event_data_size_fifo_din <= std_logic_vector(event_data_size);
          event_data_size_fifo_wr_en <= '1';
          event_data_fifo_din   <=  x"0" & be_id & firmware_type & std_logic_vector(to_unsigned(EVENT_BUILDER_ID, 1)) & std_logic_vector(unsigned(n_active_cbcs)) & std_logic_vector(event_data_size);
          event_data_fifo_wr_en <=   '1';

          event_buffer_write_fsm <= write_event_data;

        when write_event_data => -- time=1 

          -- L1A info at BE
          event_data_fifo_wr_en  <= '1';      
          event_data_fifo_din    <= l1a_fifo_dout;                
          event_buffer_write_fsm <= set_ds;
          ds_index := 0;

        when set_ds => -- time=2  
          if data_packet_ready_latched = data_packet_all_not_ready then -- no data to read.
            fifo_timer := 0;
            event_buffer_write_fsm <= idle;
          else
            if data_packet_ready_latched(ds_index) = '1' then
              dsiebo_array(ds_index).read_en <= '1';
              ds_data_req_index := 1;
              data_packet_nword <= dsoebi_array(ds_index).data_packet_nword;

              event_buffer_write_fsm <= read_ds; 
              read_flag := true;
           --   test_count := (others => '0');
           --   test_count := test_count + 1;
            else
              if ds_index = NDATASECTIONS - 1 then
                ds_index := 0;
              else
                ds_index := ds_index + 1;
              end if;
            end if;
          end if;
        when read_ds => -- time=3 read_en = '1'

          if ds_data_req_index = to_integer(unsigned(data_packet_nword) ) then
            read_flag := false;
          end if;

          if dsoebi_array(ds_index).data_packet_valid = '1' then -- only when the data is ready.  time=4 to 4+data_packet_nword
            event_data_fifo_din <= dsoebi_array(ds_index).data_packet_out;
          --	event_data_fifo_din <= std_logic_vector(test_count);
          --  test_count := test_count + 1;
            event_data_fifo_wr_en <=   '1';

            if read_flag then -- when data is ready and read_flag is true, read_en <= '1' and ds_data_req_index is inclemented
              dsiebo_array(ds_index).read_en <= '1'; 
              ds_data_req_index := ds_data_req_index + 1;
            else -- when data is ready, and read_flag is false, reset the data_packet_ready flag and goes back to set_ds.
              data_packet_ready_latched(ds_index) <= '0';
              event_buffer_write_fsm <= set_ds; 
            end if;
          end if;
      end case;
    end if;
  end process;


  event_packet_send : process (reset, daq_clk)
    variable sent_count : integer range 0 to 2**EVENT_DATA_SIZE_WIDTH;
    variable timer      : integer range 0 to 2;
    variable read_data_flag : boolean;
  begin
    if reset = '1' then
      event_data_size_fifo_rd_en <= '0';
      event_data_fifo_rd_en <=   '0';
      data_count <= (others => '0');                         
      end_of_event <= '0';
      we <= '0';
      timer := 0;
      buffer_ready <= false;
      read_data_flag := false;
      event_packet_send_fsm <= idle;
    elsif rising_edge(daq_clk) then
      event_data_size_fifo_rd_en <= '0';
      event_data_fifo_rd_en <=   '0';                         
      end_of_event <= '0';
      we <= '0';
      case event_packet_send_fsm is
        when idle =>
          if timer = 2 then
            data_count <= unsigned(event_data_size_fifo_dout) + 1;
            buffer_ready <= false;
            event_packet_send_fsm <= check_data_buffer;
            timer := 0;                    
          else
            if event_data_size_fifo_rd_en = '0' then 
              if event_data_size_fifo_empty = '0' then
                event_data_size_fifo_rd_en <= '1';
                timer := 1;
              end if;                       
            else
              timer := timer + 1;
            end if;
          end if;
        when check_data_buffer =>
          if unsigned(ebidbo.n_word_free) >= ('0' & data_count) then
            buffer_ready <= true;
          end if;
          if buffer_ready then
            sent_count := 0;
            event_packet_send_fsm <= send_data;
            read_data_flag := true;
            event_data_fifo_rd_en <= '1';
          end if;
        when send_data =>
          if read_data_flag then
            event_data_fifo_rd_en <=   '1';     
            if event_data_fifo_empty = '0' then
              we <= '1';
              sent_count := sent_count + 1;
            end if;
          else
            event_packet_send_fsm <= idle;
          end if;

          if sent_count = to_integer(data_count) then
            read_data_flag := false;
            event_data_fifo_rd_en <= '0';
            end_of_event <= '1';				    
          end if;    
      end case;
    end if;
  end process;        

  ebodbi.data         <= event_data_fifo_dout;
  ebodbi.we           <= we;
  ebodbi.end_of_event <= end_of_event;
end Behavioral;
