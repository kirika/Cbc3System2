--===========================--
-- event_builder2 
--  dsoebi.data_packet_ready is set to '1' when dsoebi.data_packet_nword & dsoebi.cbc_data_l1_count are available and stays '1' until dsiebo.read_en is set to '1'    
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--use work.cbc_system_package.all;
--use work.cbc_system_config_package.all;

entity event_builder2 is
  generic
  ( 
    NDS                     : positive := 1; 
    EVENT_BUILDER_ID        : integer  := 1; 
    DELAYED_DATA_TIMEOUT    : positive := 8;
    SD_SIZE_W               : positive := 8;
    ED_SIZE_W               : positive := 15;
    DB_ADDR_W               : positive := 17;
    BEID_W                  : positive := 1;  
    DSID_W                  : positive := 6;
    FIRMWARE_T              : std_logic_vector(2 downto 0)  := "001"
  );
  port
  (
    reset_i                        : in  std_logic; 
    be_id                          : in  std_logic_vector(BEID_W-1 downto 0);
    ds_active                      : in  std_logic_vector(NDS-1 downto 0); 
    n_active_ds                    : in  std_logic_vector(DSID_W-1 downto 0);
    daq_clk                        : in  std_logic;
    l1a_fifo_dout                  : in  std_logic_vector(31 downto 0); 
    l1a_fifo_empty                 : in  std_logic; 
    l1a_fifo_re                    : out std_logic;
    ds_header_ready_i              : in  std_logic_vector(NDS-1 downto 0);
    ds_data_packet_ready_i         : in  std_logic_vector(NDS-1 downto 0);
    ds_data_packet_nword_i         : in  std_logic_vector(NDS*(SD_SIZE_W)-1 downto 0);
    ds_l1_count_i                  : in  std_logic_vector(NDS*9-1 downto 0);
    ds_data_packet_valid_i         : in  std_logic_vector(NDS-1 downto 0);
    ds_data_packet_out_i           : in  std_logic_vector(NDS*32-1 downto 0);
    ds_read_en_o                   : out std_logic_vector(NDS-1 downto 0);
    n_word_free                    : in  std_logic_vector(DB_ADDR_W downto 0);    
    we_o                           : out  std_logic;
    data_o                         : out  std_logic_vector(31 downto 0);
    end_of_event_o                 : out  std_logic;
    write_fsm_o                    : out std_logic_vector(2 downto 0);  
    packet_send_fsm_o              : out std_logic_vector(1 downto 0);
    event_data_fifo_empty_o        : out std_logic;
    event_data_fifo_full_o         : out std_logic;
    event_data_size_fifo_empty_o   : out std_logic;
    event_data_size_fifo_full_o    : out std_logic;
    test_out1                      : out std_logic;
    test_out2                      : out std_logic    
  );
end event_builder2;

architecture Behavioral of event_builder2 is


  constant data_packet_all_not_ready     : std_logic_vector(NDS-1 downto 0) := (others => '0');

  constant BE_EVENT_DATA_SIZE            : positive := 3;
    --    constant max_event_size                : positive := MAX_SECTION_PACKET_SIZE * NDS + BE_EVENT_DATA_SIZE;

  signal active_data_sections           : std_logic_vector(NDS-1 downto 0);

  signal ds_header_ready                 : std_logic_vector(NDS-1 downto 0);          
  signal ds_data_packet_ready            : std_logic_vector(NDS-1 downto 0); 

  signal ds_header_ready_latched         : std_logic_vector(NDS-1 downto 0);          
  signal data_packet_read_flags          : std_logic_vector(NDS-1 downto 0);

  signal event_data_size                 : unsigned(ED_SIZE_W - 1 downto 0);

  signal event_data_fifo_din             : std_logic_vector(31 downto 0);
  signal event_data_fifo_wr_en           : std_logic;
  signal event_data_fifo_rd_en           : std_logic;
  signal event_data_fifo_dout            : std_logic_vector(31 downto 0);
  signal event_data_fifo_full            : std_logic;
  signal event_data_fifo_empty           : std_logic;

  signal event_data_size_fifo_din        : std_logic_vector(ED_SIZE_W - 1 downto 0);
  signal event_data_size_fifo_wr_en      : std_logic;
  signal event_data_size_fifo_rd_en      : std_logic;
  signal event_data_size_fifo_dout       : std_logic_vector(ED_SIZE_W - 1 downto 0);
  signal event_data_size_fifo_full       : std_logic;
  signal event_data_size_fifo_empty      : std_logic; 
--  signal event_data_size_fifo_werr       : std_logic;   
--  signal event_data_size_fifo_rerr       : std_logic;  
  signal event_data_size_fifo_rd_en_not_empty : std_logic;

  signal event_data_size_fifo_din_36b        : std_logic_vector(35 downto 0);
  signal event_data_size_fifo_dout_36b       : std_logic_vector(35 downto 0);


  signal data_count                      : unsigned(ED_SIZE_W - 1 downto 0); 

  type write_fsm_type is (idle, wait_for_ds_headers, write_header_data_size, write_event_data, set_ds, read_ds);     
  type event_packet_send_fsm_type is (idle, check_data_buffer, send_data);     
  signal write_fsm                       :  write_fsm_type;
  signal event_packet_send_fsm           : event_packet_send_fsm_type;

  signal we                              : std_logic;
  signal end_of_event                    : std_logic;

  signal buffer_ready                    : boolean;

 signal data_packet_nword               : std_logic_vector(SD_SIZE_W-1 downto 0);
  --    signal counter                         : unsigned(10 downto 0);
  signal reset : std_logic;

begin

  write_fsm_o <= "000" when write_fsm = idle else
                 "001" when write_fsm = wait_for_ds_headers else
                 "010" when write_fsm = write_header_data_size else
                 "011" when write_fsm = write_event_data else
                 "100" when write_fsm = set_ds else
                 "101" when write_fsm = read_ds;
  
  packet_send_fsm_o <= "00" when event_packet_send_fsm = idle else
                       "01" when event_packet_send_fsm = check_data_buffer else
                       "10" when event_packet_send_fsm = send_data;             
                 
  event_data_fifo_empty_o <= event_data_fifo_empty;
  event_data_fifo_full_o  <= event_data_fifo_full;
  event_data_size_fifo_empty_o <= event_data_size_fifo_empty;
  
  test_out1 <= event_data_fifo_wr_en;
  test_out2 <= event_data_fifo_rd_en;
  
  eb_reset : entity work.dff_sync_edge_detect
  port map(   
            reset => '0',
            clkb  => daq_clk,
            dina  => reset_i,
            doutb => reset 
          );

  event_data_fifo_inst : entity work.event_data_fifo
  port map (  
             clk   => daq_clk,
             srst  => reset,
             din   => event_data_fifo_din,
             wr_en => event_data_fifo_wr_en,
             rd_en => event_data_fifo_rd_en,
             dout  => event_data_fifo_dout,
             full  => event_data_fifo_full,
             empty => event_data_fifo_empty
           );

  event_data_size_fifo_din_36b <= x"000000" & event_data_size_fifo_din;
  event_data_size_fifo_dout <= event_data_size_fifo_dout_36b(11 downto 0);

  event_data_size_fifo_rd_en_not_empty <= event_data_size_fifo_rd_en and not event_data_fifo_empty;
  event_data_size_fifo : entity work.fifo_36b_512d
  Port map( 	
          clk   => daq_clk,
          rst   => reset,
          din   => event_data_size_fifo_din_36b,
          wr_en => event_data_size_fifo_wr_en,
          rd_en => event_data_size_fifo_rd_en_not_empty,
          dout  => event_data_size_fifo_dout_36b,
          full  => event_data_size_fifo_full,
          empty => event_data_size_fifo_empty
          );

--    l1a_count_to_fifo_process : process (clk_320MHz)
--    begin
--        if rising_edge(clk_320MHz) then
--            l1a_count_to_fifo <= std_logic_vector(unsigned(l1a_count)+1);
--        end if;
--    end process;
    
  event_buffer_write : process (reset, daq_clk)
    variable delayed_data_timer : integer range 0 to DELAYED_DATA_TIMEOUT:= 0;
    variable ds_index           : integer range 0 to NDS - 1 := 0;
    variable ds_data_req_index  : integer range 0 to 2**SD_SIZE_W - 1:= 0;
    variable fifo_timer         : integer range 0 to 1;
    variable add_cbc_data_packet_size : boolean;
    variable index_for_dps_calc : integer range 0 to NDS - 1 := 0;
    variable read_flag          : boolean;	
    variable test_count         : unsigned(31 downto 0);  
    variable event_data_count   : integer range 0 to 2;   	
  begin
    if reset = '1' then
      ds_index := 0;
      ds_data_req_index := 0;
      fifo_timer := 0;
      l1a_fifo_re <= '0';
      event_data_fifo_wr_en <=   '0';
      event_data_fifo_din   <= (others => '0');
      event_data_size_fifo_wr_en <= '0'; 
      write_fsm <= idle; 

      ds_header_ready         <= (others => '0'); 
      ds_data_packet_ready    <= (others => '0'); 
      data_packet_read_flags  <= (others => '0');
      ds_header_ready_latched <= (others => '0');  
      
      event_data_count := 0;
      ds_read_en_o <= (others => '0');

      active_data_sections <= (others => '1');  
      add_cbc_data_packet_size := false; 

    elsif rising_edge(daq_clk) then

      l1a_fifo_re <= '0';
      event_data_fifo_wr_en <=   '0';  
      event_data_size_fifo_wr_en <= '0';                       
      event_data_size_fifo_full_o  <= event_data_size_fifo_full;
      for i in 0 to NDS-1 loop
        ds_header_ready(i)      <= ds_header_ready_i(i)      and ds_active(i);
        ds_data_packet_ready(i) <= ds_data_packet_ready_i(i) and ds_active(i);
        ds_read_en_o(i) <= '0';
      end loop;

      for i in 0 to NDS-1 loop
      end loop;

      case write_fsm is
        when idle =>
          ds_index := 0;
          for i in 0 to NDS-1 loop
            active_data_sections(i) <=  ds_active(i);
          end loop;   
          ds_header_ready_latched <= (others => '0');
          data_packet_read_flags <= (others => '0'); 
          if fifo_timer = 1 then
            fifo_timer := 0;
            write_fsm <= wait_for_ds_headers;
          elsif fifo_timer = 0 then 
            if l1a_fifo_empty = '0' then
              l1a_fifo_re <= '1';
              delayed_data_timer := 0;
              event_data_size <= to_unsigned(2,ED_SIZE_W);        
              fifo_timer := fifo_timer + 1;
            end if;
          end if;
        when wait_for_ds_headers => -- 1 clock from l1a_fifo_empty = '0'

        -- event data size update for the data section with data_packet_ready set.
          if add_cbc_data_packet_size then
            event_data_size <= event_data_size + (to_unsigned(0, ED_SIZE_W - SD_SIZE_W) & unsigned(ds_data_packet_nword_i(SD_SIZE_W * (index_for_dps_calc+1)-1 downto SD_SIZE_W * index_for_dps_calc) ));
            add_cbc_data_packet_size := false;
          end if;

        -- data sections with data_packet_ready set are searched for until data_packet_ready = active_data_sections.
        -- timeout is set to DELAYED_DATA_TIMEOUT 
          if delayed_data_timer > DELAYED_DATA_TIMEOUT then
            write_fsm <= write_header_data_size;
          end if;
          if ds_header_ready /= data_packet_all_not_ready then
            if ds_header_ready_latched /= active_data_sections then -- headers from all the active data section have not latched yet.
              if ds_header_ready_latched(ds_index) = '0' and ds_header_ready(ds_index) = '1' then
                ds_header_ready_latched(ds_index) <= '1';
                -- for event data size update
                add_cbc_data_packet_size := true;
                index_for_dps_calc := ds_index;
              end if;                                        
              if ds_index = NDS - 1 then                       
                ds_index := 0;
              else 
                ds_index := ds_index + 1;
              end if;
              delayed_data_timer := delayed_data_timer + 1;
            else
              write_fsm <= write_header_data_size;                    
            end if;                          
          end if;

        when write_header_data_size => -- 1 clock after data_packet of all sections are ready. time=0           

          event_data_size_fifo_din <= std_logic_vector(event_data_size);
          event_data_size_fifo_wr_en <= '1';
          event_data_fifo_din <= (others => '0');
          event_data_fifo_din( BEID_W + 3 + 1 + DSID_W + ED_SIZE_W - 1 downto 0) <= be_id & FIRMWARE_T & std_logic_vector(to_unsigned(EVENT_BUILDER_ID, 1)) & std_logic_vector(unsigned(n_active_ds)) & std_logic_vector(event_data_size);
          event_data_fifo_wr_en <=   '1';
          event_data_count := 0;
          write_fsm <= write_event_data;

        when write_event_data => -- time=1 

          -- L1A info at BE
          event_data_fifo_wr_en  <= '1';      
          event_data_count := event_data_count + 1;
          if event_data_count = 1 then
            event_data_fifo_din <= (others => '1');
          elsif event_data_count = 2 then
            event_data_fifo_din    <= l1a_fifo_dout;                
            write_fsm <= set_ds;
            ds_index := 0;
            data_packet_read_flags <= (others => '0');
          end if;
          
        when set_ds => -- time=2 to 
          if data_packet_read_flags = ds_header_ready_latched then -- no data to read for this event.
            fifo_timer := 0;
            ds_header_ready_latched <= (others => '0');
            data_packet_read_flags <= (others => '0');
            write_fsm <= idle;
          else
            if data_packet_read_flags(ds_index) = '0' and ds_data_packet_ready(ds_index) = '1' then
              ds_read_en_o(ds_index) <= '1';
              ds_data_req_index := 1;
              data_packet_nword <= ds_data_packet_nword_i(SD_SIZE_W * (ds_index+1)-1 downto SD_SIZE_W * ds_index);
              write_fsm <= read_ds; 
              read_flag := true;
           --   test_count := (others => '0');
           --   test_count := test_count + 1;
            else
              if ds_index = NDS - 1 then
                ds_index := 0;
              else
                ds_index := ds_index + 1;
              end if;
            end if;
          end if;
        when read_ds =>

          if ds_data_req_index = to_integer(unsigned(data_packet_nword) ) then
            read_flag := false;
          end if;

          if ds_data_packet_valid_i(ds_index) = '1' then
            event_data_fifo_din <= ds_data_packet_out_i(32 * (ds_index+1)-1 downto 32 * ds_index);
          --	event_data_fifo_din <= std_logic_vector(test_count);
          --  test_count := test_count + 1;
            event_data_fifo_wr_en <=   '1';

            if read_flag then
              ds_read_en_o(ds_index) <= '1';
              ds_data_req_index := ds_data_req_index + 1;
            else
              data_packet_read_flags(ds_index) <= '1';
              write_fsm <= set_ds; 
            end if;
          end if;
      end case;
    end if;
  end process;


  event_packet_send : process (reset, daq_clk)
    variable sent_count : integer range 0 to 2**ED_SIZE_W;
    variable timer      : integer range 0 to 2;
    variable read_data_flag : boolean;
  begin
    if reset = '1' then
      event_data_size_fifo_rd_en <= '0';
      event_data_fifo_rd_en <=   '0';
      data_count <= (others => '0');                         
      end_of_event <= '0';
      we <= '0';
      timer := 0;
      buffer_ready <= false;
      read_data_flag := false;
      event_packet_send_fsm <= idle;
    elsif rising_edge(daq_clk) then
      event_data_size_fifo_rd_en <= '0';
      event_data_fifo_rd_en <=   '0';                         
      end_of_event <= '0';
      we <= '0';
      case event_packet_send_fsm is
        when idle =>
          if timer = 2 then
            data_count <= unsigned(event_data_size_fifo_dout) + 1;
            buffer_ready <= false;
            event_packet_send_fsm <= check_data_buffer;
            timer := 0;                    
          else
            if event_data_size_fifo_rd_en = '0' then 
              if event_data_size_fifo_empty = '0' then
                event_data_size_fifo_rd_en <= '1';
                timer := 1;
              end if;                       
            else
              timer := timer + 1;
            end if;
          end if;
        when check_data_buffer =>
          if unsigned(n_word_free) >= ('0' & data_count) then
            buffer_ready <= true;
          end if;
          if buffer_ready then
            sent_count := 0;
            event_packet_send_fsm <= send_data;
            read_data_flag := true;
            event_data_fifo_rd_en <= '1';
          end if;
        when send_data =>
          if read_data_flag then
            event_data_fifo_rd_en <=   '1';     
            if event_data_fifo_empty = '0' then
              we <= '1';
              sent_count := sent_count + 1;
            end if;
          else
            event_packet_send_fsm <= idle;
          end if;

          if sent_count = to_integer(data_count) then
            read_data_flag := false;
            event_data_fifo_rd_en <= '0';
            end_of_event <= '1';				    
          end if;    
      end case;
    end if;
  end process;        

  data_o         <= event_data_fifo_dout;
  we_o           <= we;
  end_of_event_o <= end_of_event;
end Behavioral;
