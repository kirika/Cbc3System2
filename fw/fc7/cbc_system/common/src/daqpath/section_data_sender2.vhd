----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.02.2018 12:04:07
-- Design Name: 
-- Module Name: section_data_sender - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
--
-- The module read header information and data from fifos in data_section_processor and send those to event_builder.
--	This module assumes that data size is not 0.
--  Read latency must be 1 for both hd_fifo and data_fifo.
--
--	header information : hd_fifo_dout(64-bit-word) format
--	[63 downto 32] : section data header, whatever the section data needs.  
--					 Event builder just write the 32 bits before writing data from data_fifo.
--  [x+8 downto 9]   : x = DATA_SIZE_WIDTH(8).  data size in 32-bit-word without header.
--  [8 downto 0]   : L1A count
-- 
--  section_data_packet_send_fsm 
--   idle            : if hd_fifo is not empty, read one word from hd_fifo
--   send_data_ready : to_eb_data_packet_ready <= '1', fill to_eb_data_packet_nword and to_eb_l1_count from the hd_fifo_out.
--   send_packet     : read (should be done one clock before for the section data.) and send section data header and section data (to_eb_data_packet_out) if from_eb_read_en = '1' with to_eb_data_packet_valid = '1'
--
--  
---------------------------------------------------------------------------------------------
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity section_data_sender2 is
  generic( DATA_SIZE_WIDTH : integer := 12 );
  Port (
	reset		                 	 : in  std_logic;
	clk_320MHz 		             : in  std_logic;
	hd_fifo_rd_en              : out std_logic;
	hd_fifo_empty	             : in  std_logic;	
	hd_fifo_dout	             : in  std_logic_vector(63 downto 0);
	data_fifo_rd_en            : out std_logic;
	data_fifo_empty	           : in  std_logic;
	data_fifo_dout	           : in  std_logic_vector(31 downto 0);
	from_eb_read_en            : in  std_logic;
    to_eb_header_ready         : out std_logic;
	to_eb_data_packet_ready    : out std_logic;
	to_eb_data_packet_nword    : out std_logic_vector( DATA_SIZE_WIDTH-1 downto 0);
    to_eb_l1_count             : out std_logic_vector(8 downto 0);
	to_eb_data_packet_valid    : out std_logic;
    to_eb_data_packet_out      : out std_logic_vector(31 downto 0)
	);
end section_data_sender2;

architecture Behavioral of section_data_sender2 is

 -- constant DATA_SIZE_WIDTH  : integer :=  8;
    --    type section_data_packet_send_fsm_type is (idle, send_data_ready, send_packet);
    type fsm_type is (idle, send_data_ready, send_packet);
    signal fsm       : fsm_type;  
    signal data_packet_nword_pre          : unsigned(DATA_SIZE_WIDTH-1 downto 0);    
    signal data_packet_nword              : unsigned(DATA_SIZE_WIDTH-1 downto 0);
    signal data_packet_ready              : std_logic;
    signal data_packet_nword_sent         : unsigned(DATA_SIZE_WIDTH-1 downto 0);  
    signal read_data_fifo_en              : std_logic;	

begin

    sender : process (reset, clk_320MHz)
         variable index : integer range 1 to 80;
         variable fifo_re_timer : integer range 0 to 2;
         variable fifo_ready : boolean;
         variable l1_count_pre : std_logic_vector(8 downto 0);
     begin
         if reset = '1' then
             fifo_re_timer := 0;
             fifo_ready := false;
             hd_fifo_rd_en <= '0';
             data_packet_ready <= '0';                                             
             fsm <= idle;
             data_packet_nword_pre <= (others => '0');
             data_packet_nword_sent <= (others => '0');
             read_data_fifo_en <= '0';
         elsif rising_edge(clk_320MHz) then             
             to_eb_data_packet_valid      <= '0';
             hd_fifo_rd_en <= '0';
             read_data_fifo_en <= '0';
             case fsm is
             when idle =>
                 data_packet_ready <= '0';  
                 data_packet_nword_sent <= (others => '0');                                           
 
                 data_packet_nword_pre <= unsigned(hd_fifo_dout(DATA_SIZE_WIDTH + 8 downto 9)) + 1;
                 l1_count_pre := hd_fifo_dout(8 downto 0); 
 
                 if fifo_ready then
                     fsm <= send_data_ready;
                     fifo_re_timer := 0;
                     fifo_ready := false;
                 end if;
                 if fifo_re_timer = 1 then
                     fifo_re_timer := fifo_re_timer + 1;
                     fifo_ready := true;
                 elsif fifo_re_timer = 0 then
                     if hd_fifo_empty = '0' then
                         fifo_re_timer := fifo_re_timer + 1;
                         hd_fifo_rd_en <= '1';                       
                     end if;    
                 end if;
                
             when send_data_ready =>                                             
                 data_packet_nword <= data_packet_nword_pre;
                 to_eb_l1_count <= l1_count_pre;
                 data_packet_ready <= '1';               
                 fsm <= send_packet;
                 read_data_fifo_en <= '1'; -- never be effective.
             when send_packet =>
             
                 read_data_fifo_en <= '1';
                 if data_packet_nword_sent = data_packet_nword - 1 then -- read_data_fifo_en = '1' when data_packet_nword_sent = 0 to data_packet_nword-1
                     read_data_fifo_en <= '0';
                 end if;                
                 if from_eb_read_en = '1' then                   
 
                     if data_packet_ready = '1' then
                         data_packet_ready <= '0'; 
                     end if;
                     if data_packet_nword_sent = 0 then
                         to_eb_data_packet_valid      <= '1';
                         to_eb_data_packet_out <= hd_fifo_dout(63 downto 32); -- data section packet header 
                     else
                         to_eb_data_packet_valid      <= '1';
                         to_eb_data_packet_out <= data_fifo_dout;
                         --to_eb_data_packet_out <= x"000000" & std_logic_vector(data_packet_nword_sent);
                     end if;
                     data_packet_nword_sent <= data_packet_nword_sent + 1;   
                                                 
                 else
                     if data_packet_nword_sent = data_packet_nword then
                         fsm <= idle;
                     end if;            
                 end if;
              end case;
         end if;
     end process; 
        
     data_fifo_rd_en      <= from_eb_read_en and read_data_fifo_en;
 --    to_eb_data_packet_out   <= cbc_data_info_fifo_dout(63 downto 32) when data_packet_nword_sent = 0 else cbc_data_fifo_dout; 
     to_eb_data_packet_nword <= std_logic_vector(data_packet_nword);
 --    ipb_stat_o <= ipb_stat;
     to_eb_data_packet_ready <= data_packet_ready;
     to_eb_header_ready <= data_packet_ready;
end Behavioral;
