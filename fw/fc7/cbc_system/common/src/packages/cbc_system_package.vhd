--===========================--
-- cbc_system_package
-- 28.10.2016 Kirika Uchida
--===========================--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;

package cbc_system_package is

	--================--
	-- constants 
	--================--
  constant BEID_WIDTH                    : integer :=  7;
  constant FEID_WIDTH                    : integer :=  3;
  constant CBCID_WIDTH                   : integer :=  5;
  constant DSID_WIDTH                    : integer :=  6;

--  constant SECTION_DATA_SIZE_WIDTH       : integer :=  8;

    constant MAX_N_FE                      : integer :=  7; 
    constant MAX_N_CBC                     : integer := 16; 
    constant MAX_N_DATA_SECTION            : integer := 36; 
    
    constant DSID_CBC_DATA_RAW             : integer :=  1;
    
    --================--
    -- cbc data types
    --================--
    type cbcdata_type is ( raw, hit );    

	--================--
	-- data types for cbc_i2c_bus_manager
	--================--
  type cbc_i2c_bus_miso_type is
  record
		sda                : std_logic;
  end record;
  type cbc_i2c_bus_miso_array_type is array (0 to MAX_N_FE-1) of cbc_i2c_bus_miso_type;
  type cbc_i2c_bus_mosi_type is
  record
		scl                : std_logic;
		sda                : std_logic;
  end record;
  type cbc_i2c_bus_mosi_array_type is array (0 to MAX_N_FE-1) of cbc_i2c_bus_mosi_type;


	--================--
 	-- data types for fast_signal_manager  
	--================--
	type cbc_fast_com_type is
	record
		fast_reset  : std_logic;
		trigger     : std_logic;
		tpreq       : std_logic;
		orbit_reset : std_logic;
	end record;

    
--	type  ipbif_to_bram_type is
--	record
--        wen         : std_logic;
--		din         : std_logic_vector(31 downto 0);
--	end record;

--    type seqb_in_type is
--    record
--        wen         : std_logic;
--        din         : std_logic_vector(31 downto 0);
--        count       : unsigned(11 downto 0);
--        addr        : std_logic_vector(11 downto 0);
--    end record;
   
   -- for cic.  temporarily
    type ipb_cic_ctrl_type is
    record
        reset : std_logic;
    end record;
    type ipb_cic_stat_type is
    record
        cic_data_processor_fsm : std_logic_vector(2 downto 0);
    end record;
    
    type ipb_cic_ctrl_array_type is array (0 to MAX_N_DATA_SECTION-1) of ipb_cic_ctrl_type; 
    type ipb_cic_stat_array_type is array (0 to MAX_N_DATA_SECTION-1) of ipb_cic_stat_type;   
     
---------------------------     
-- ipb_cbc_system_cnfg_type
---------------------------    

	--================--
	-- cbc system basic config
	--================--
	-- cbc config
  type cs_cbc_cnfg_type is
  record
      id                    : std_logic_vector(CBCID_WIDTH-1 downto 0);
      fe_id                 : std_logic_vector(FEID_WIDTH-1 downto 0);
      i2c_address           : std_logic_vector(6 downto 0);
      active                : std_logic;
  end record;
  type cs_cbc_cnfg_array_type is array (0 to MAX_N_CBC-1) of cs_cbc_cnfg_type;

  type cs_ds_cnfg_type is
  record
      id                    : std_logic_vector(DSID_WIDTH-1 downto 0);
      active                : std_logic;
  end record;
  type cs_ds_cnfg_array_type is array (0 to MAX_N_DATA_SECTION-1) of cs_ds_cnfg_type;
	-- front end config
	-- front end config
  type cs_fe_cnfg_type is
  record
  	id                      : std_logic_vector(FEID_WIDTH-1 downto 0);
  	n_active_cbcs           : std_logic_vector(CBCID_WIDTH-1 downto 0);
  	active_cbcs             : cs_cbc_cnfg_array_type;
  end record;
  type cs_fe_cnfg_array_type is array (0 to MAX_N_FE-1) of cs_fe_cnfg_type;
  -- fast command manager
  type cs_fcm_cnfg_type is
  record
		ext_en                  : std_logic;
		ipbus_en                : std_logic;
		internal_en             : std_logic;
		sequencer_en            : std_logic;
		-- fast command generator
		fcg_fast_reset_en       : std_logic;
		fcg_trigger_en          : std_logic;
		fcg_test_pulse_en       : std_logic;  
		fcg_orbit_reset_en      : std_logic;
		fcg_Ncycle              : std_logic_vector( 31 downto 0 );
		fcg_cycle_T             : std_logic_vector( 31 downto 0 );
		fcg_trigger_t           : std_logic_vector( 31 downto 0 );
		fcg_test_pulse_t        : std_logic_vector( 31 downto 0 );
		fcg_orbit_reset_t       : std_logic_vector( 31 downto 0 );       
  end record;

	-- config for io
	-- cbc data out cnfg
  type cs_cbc_data_rcv_iserdes_cnd_cnfg_type is
  record
		--cbc data iserdes timing tuning 
		cbc_id             : std_logic_vector(CBCID_WIDTH-1 downto 0);
		idelay_offset      : std_logic_vector(4 downto 0);
  end record;
	type cs_cbc_data_rcv_iserdes_cnd_cnfg_fe_array_type is array (0 to MAX_N_FE-1) of cs_cbc_data_rcv_iserdes_cnd_cnfg_type;
	type cs_cbc_data_rcv_cnfg_type is
	record
			iserdes_cnd_fe_array : cs_cbc_data_rcv_iserdes_cnd_cnfg_fe_array_type;
	end record;

	type cs_cic_data_io_cnfg_type is
	record
		enable : std_logic;
	end record;
	type cs_cic_data_io_cnfg_fe_array_type is array (0 to MAX_N_FE-1) of cs_cic_data_io_cnfg_type;

	type cs_io_cnfg_type is
	record
		cbc_data_rcv        : cs_cbc_data_rcv_cnfg_type;
		cic_data_io_array   : cs_cic_data_io_cnfg_fe_array_type;
	end record;

	-- data processor configuration
  -- cbc data processor
  type cs_cbc_dp_cnfg_type is
  record
		l1a_latency             : std_logic_vector(8 downto 0);
		trig_data_latency       : std_logic_vector(8 downto 0);
  end record;
  type cs_cbc_dp_cnfg_array_type is array (0 to MAX_N_DATA_SECTION-1) of cs_cbc_dp_cnfg_type;
	-- cic data processor
	type cs_cic_dp_cnfg_type is
	record
		enable                  : std_logic;
    id                      : std_logic_vector(4 downto 0);
	end record;
	type cs_cic_dp_cnfg_array_type is array (0 to MAX_N_DATA_SECTION-1) of cs_cic_dp_cnfg_type;
	-- data_processor_cnfg
	type cs_dp_cnfg_type is
	record
		cbc_dp_array              : cs_cbc_dp_cnfg_array_type;
		cic_dp_array              : cs_cic_dp_cnfg_array_type;
	end record;

	-- cbc system config 
	type cs_cnfg_type is
  record
        probe_card              : std_logic;
		be_id                   : std_logic_vector(BEID_WIDTH-1 downto 0);
		cbc_clock_ext           : std_logic;
		trigger_master_ext      : std_logic;
		test_out_sel0           : std_logic_vector(15 downto  0);  
		test_out_sel1           : std_logic_vector(15 downto  0);                
		n_active_cbc            : std_logic_vector(CBCID_WIDTH - 1 downto 0 );
		fe_array                : cs_fe_cnfg_array_type;  
		cbc_array               : cs_cbc_cnfg_array_type;
        ds_array                : cs_ds_cnfg_array_type;
		fcm                     : cs_fcm_cnfg_type;
		db_safe_n_word_free     : std_logic_vector(31 downto 0);
		rdb_cbc_id              : std_logic_vector(CBCID_WIDTH-1 downto 0);
		rdb_latency             : std_logic_vector(7 downto 0);
        rdb_write_block_size    : std_logic_vector(15 downto 0);    
		io                      : cs_io_cnfg_type;
		dp                      : cs_dp_cnfg_type;
  end record;    
    

	--================--
	-- cbc system control signals
	--================--

	type cs_cbc_data_rcv_iserdes_cnd_ctrl_type is
	record
		-- iserdes timing tuning
	  reset      : std_logic;
		tune       : std_logic;
		scan_pulse : std_logic;
	end record;
  type cs_cbc_data_rcv_iserdes_cnd_ctrl_fe_array_type is array (0 to MAX_N_FE-1) of cs_cbc_data_rcv_iserdes_cnd_ctrl_type;

	---- cbc data output signal receiver control
  type cs_cbc_data_rcv_ctrl_type is
  record
		timing_scan_bram_reset    : std_logic;
		timing_scan_bram_rd_reset : std_logic;
		iserdes_cnd_fe_array      : cs_cbc_data_rcv_iserdes_cnd_ctrl_fe_array_type;
  end record;
	-- io control
	---------------------
  type cs_io_ctrl_type is
	---------------------
  record
		fc_to_cbc_oserdes_reset      : std_logic_vector(MAX_N_FE-1 downto 0);
		cbc_data_rcv                 : cs_cbc_data_rcv_ctrl_type;
		-- iserdes timing scan buffer
 	  cbc_hard_reset               : std_logic;
  end record;    

  -- fast command manager
	---------------------
  type cs_fcm_ctrl_type is
	---------------------
  record
		reset              : std_logic;
		start_trigger      : std_logic;
		stop_trigger       : std_logic;
		fast_com           : cbc_fast_com_type; 
		fcg_reset          : std_logic;
		fcg_load_cnfg      : std_logic;
		fcg_start          : std_logic;
		fcg_stop           : std_logic;
		veto_reset         : std_logic; 
  end record;  

	-- cbc_i2c
	---------------------
	type cs_cbc_i2c_bm_ctrl_type is
	---------------------
	record
		reset              : std_logic;
		init               : std_logic;
		reset_fifos        : std_logic;
		reset_com_count    : std_logic;
	end record;
	type cs_cbc_i2c_bm_ctrl_array_type is array (0 to MAX_N_FE-1) of cs_cbc_i2c_bm_ctrl_type;

  -- cbc data processor
	---------------------
  type cs_cbc_dp_ctrl_type is
	---------------------
  record
		reset                  : std_logic;
		frame_counter_reset    : std_logic;
  end record;
  type cs_cbc_dp_ctrl_array_type is array (0 to MAX_N_DATA_SECTION-1) of cs_cbc_dp_ctrl_type;

	-- control signal container   
	---------------------
	type cs_ctrl_type is
	---------------------
	record
        daq_reset                         : std_logic;
		-- io
		io                                : cs_io_ctrl_type;
		-- fast command manager         
		fcm                               : cs_fcm_ctrl_type;
		-- cbc i2c bus manager
		cbc_i2c_bm_array                  : cs_cbc_i2c_bm_ctrl_array_type;
		-- cbc data processor
		cbc_dp_array                      : cs_cbc_dp_ctrl_array_type;
		-- event builder
		eb_reset                          : std_logic; 
		-- data buffer
		db_reset                          : std_logic;
	 	db_readall                        : std_logic;	
		-- raw data buffer
		rdb_reset                         : std_logic; 
		-- sequencer
		seq_reset                         : std_logic;
	 	seq_start                         : std_logic;	
		seq_resume                        : std_logic;
		wt_fpp                            : std_logic;
	end record;
---- cbc i2c seu checker
--cbc_i2c_seu_chk_reset	            : std_logic;
--cbc_i2c_seu_chk_start							: std_logic;
--cbc_i2c_seu_chk_pause							: std_logic;
--cbc_i2c_seu_chk_resume						: std_logic;
--cbc_i2c_seu_chk_stop							: std_logic;	
--cbc_i2c_seu_chk_reset_ebram_raddr : std_logic;
	    

	--================--
	-- cbc system stat signals
	--================--
  -- fast command manager 
  type cs_fcm_stat_type is
  record
      fcg_fsm										 : std_logic;
      trigger_fsm								 : std_logic; 
  end record;
  -- cbc i2c bus manager 
  type cs_cbc_i2c_bm_stat_type is
  record
		bus_waiting                  : std_logic;
		bus_ready                    : std_logic;
		com_fifo_empty               : std_logic; 
		com_fifo_full                : std_logic; 
		reply_fifo_empty             : std_logic; 
		reply_fifo_full              : std_logic; 
		reply_fifo_ndata             : std_logic_vector(31 downto 0);
		reply_fifo_nrdata            : std_logic_vector(31 downto 0);
		n_active_cbcs                : std_logic_vector(CBCID_WIDTH-1 downto 0);
  end record;
  type cs_cbc_i2c_bm_stat_array_type is array (0 to MAX_N_FE-1) of cs_cbc_i2c_bm_stat_type;

  type eb_stat_type is
  record
    write_fsm                        : std_logic_vector(2 downto 0); 
    packet_send_fsm                  : std_logic_vector(1 downto 0);
    event_data_fifo_empty            : std_logic;
    event_data_fifo_full             : std_logic;
    event_data_size_fifo_empty       : std_logic;
    event_data_size_fifo_full        : std_logic;    
  end record;
	---- cbc data out receiver stat
	type cs_cbc_data_rcv_iserdes_stat_type is
	record
		-- iserdes
		delay_locked         : std_logic;
		in_delay_tap_out     : std_logic_vector(5*6-1 downto 0);         
		slvs5                : std_logic_vector(7 downto 0);
	end record;       
	type cs_cbc_data_rcv_iserdes_stat_cbc_array_type is array (0 to MAX_N_DATA_SECTION - 1) of cs_cbc_data_rcv_iserdes_stat_type;
	---- cbc data iserdes timing tuning 
	type cs_cbc_data_rcv_iserdes_cnd_stat_type is
	record
		fsm                          : std_logic_vector( 3 downto 0); 
		idelay_ctrl_fsm              : std_logic_vector( 2 downto 0); 
		mmcm_locked                  : std_logic;
        mmcme2_drp_saddr             : std_logic_vector( 1 downto 0);
        mmcme2_drp_srdy              : std_logic;
		bitslip_counter              : std_logic_vector(2 downto 0);
		pulse_scan_done              : std_logic;
	end record;
  type cs_cbc_data_rcv_iserdes_cnd_stat_fe_array_type is array (0 to MAX_N_FE-1) of cs_cbc_data_rcv_iserdes_cnd_stat_type;
	type cs_cbc_data_rcv_stat_type is
	record
		iserdes_cnd_bram_nrdata      : std_logic_vector(9 downto 0);
		iserdes_cnd_fe_array         : cs_cbc_data_rcv_iserdes_cnd_stat_fe_array_type;	
		iserdes_cbc_array            : cs_cbc_data_rcv_iserdes_stat_cbc_array_type;
	end record;
	-- io
	type cs_io_stat_type is
	record
		cbc_data_rcv                 : cs_cbc_data_rcv_stat_type; 
	end record;
	-- cbc data processor 
	type cs_cbc_dp_stat_type is
	record
        data_fifo_empty					     : std_logic;
        data_fifo_full						 : std_logic;
        data_info_fifo_empty				 : std_logic;
        data_info_fifo_full					 : std_logic;        
        trig_data_fifo_empty				 : std_logic;
        trig_data_fifo_full					 : std_logic;
        header_ready                         : std_logic;
        packet_ready                         : std_logic;
        cbc_data_frame_counter			     : std_logic_vector(31 downto 0);  
        l1a_counter                          : std_logic_vector(31 downto 0);  
  end record;
  type cs_cbc_dp_stat_array_type is array (0 to MAX_N_DATA_SECTION-1) of cs_cbc_dp_stat_type;
    
	-- status signal container   
	type cs_stat_type is
	record
		n_fe                     : std_logic_vector(FEID_WIDTH-1 downto 0);
		n_cbc                    : std_logic_vector(CBCID_WIDTH-1 downto 0);
		n_active_cbc             : std_logic_vector(CBCID_WIDTH-1 downto 0);   
		mmcm0_locked             : std_logic;
		-- fast command manager
		fcm                      : cs_fcm_stat_type;
		-- cbc i2c bus manager
		cbc_i2c_bm_array         : cs_cbc_i2c_bm_stat_array_type;
		n_active_ds             : std_logic_vector(DSID_WIDTH - 1 downto 0 );
		ds_active               : std_logic_vector(MAX_N_DATA_SECTION-1 downto 0);
		eb                      : eb_stat_type;
		-- data buffer
		db_n_word_all					 	 : std_logic_vector(31 downto 0);
		db_n_word_events			   : std_logic_vector(31 downto 0);       
		db_n_word_free				   : std_logic_vector(31 downto 0);
		db_werr								   : std_logic;
		db_rerr								   : std_logic;
		db_waddr							   : std_logic_vector(31 downto 0);
		db_raddr							   : std_logic_vector(31 downto 0);       
		-- raw data buffer
		rdb_read_ready           : std_logic;
		rdb_waddr                : std_logic_vector(15 downto 0);
		-- sequencer
    seq_fsm                  : std_logic_vector( 2 downto 0); 
    --seq_dout                 : std_logic_vector(11 downto 0);
    seq_bram_addra                 : std_logic_vector(11 downto 0);
    seq_keithley_cmp_timer         : std_logic_vector(19 downto 0);
		-- io
		io                       : cs_io_stat_type;
		-- cbc data processor
		cbc_dp_array             : cs_cbc_dp_stat_array_type;
	end record;
---- cbc i2c seu checker
--cisc_cbc_i2c_error            : std_logic;
--cisc_nword_ebram              : std_logic_vector(15 downto 0);
--cisc_dbram_ndata              : std_logic_vector(8 downto 0);
--cisc_fsm                      : std_logic_vector(1 downto 0); 


--	function mk_fe_cnfg_array( signal a : in cs_cbc_cnfg_array_type ) return cs_fe_cnfg_array_type;
  function get_cbc_cnfg( signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0); signal a : in cs_cbc_cnfg_array_type ) return cs_cbc_cnfg_type;
	function get_cbc_index( signal a : in cs_cbc_cnfg_array_type; signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0) ) return integer;
	function bitswap ( signal a : in std_logic_vector ) return std_logic_vector;
	function get_fe_active_and( signal sig : in std_logic_vector(MAX_N_FE-1 downto 0); signal fe_cnfg_array : in cs_fe_cnfg_array_type ) return std_logic;

end cbc_system_package;

package body cbc_system_package is

--	function mk_fe_cnfg_array( signal a : in cs_cbc_cnfg_array_type ) return cs_fe_cnfg_array_type is
--        variable fe_cnfg_array : cs_fe_cnfg_array_type;      
--        variable curr_index : integer;
--	begin
--        	for fe_index in fe_cnfg_array'RANGE loop
--        	    fe_cnfg_array(fe_index).id := std_logic_vector(to_unsigned(fe_index + 1, FEID_WIDTH));
--               	curr_index := 0;
--            	for cbc_index in a'RANGE loop
--                	if a(cbc_index).fe_id = std_logic_vector(to_unsigned(fe_index+1,FEID_WIDTH)) and a(cbc_index).active = '1' then
--                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).id := a(cbc_index).id;
--                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).fe_id := a(cbc_index).fe_id;
--                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).i2c_address := a(cbc_index).i2c_address;                    		
--                    		fe_cnfg_array(fe_index).active_cbcs(curr_index).active := a(cbc_index).active;   
--                    		curr_index := curr_index + 1;
--                	end if;
--                	fe_cnfg_array(fe_index).n_active_cbcs := std_logic_vector(to_unsigned(curr_index,CBCID_WIDTH));
--            	end loop;     
--        end loop;
--	return fe_cnfg_array;
--	end mk_fe_cnfg_array;


    function get_cbc_cnfg( signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0); signal a : in cs_cbc_cnfg_array_type ) return cs_cbc_cnfg_type is
    begin
        for cbc_index in a'RANGE loop
            if a(cbc_index).id = cbc_id then
                return a(cbc_index);
            end if;
        end loop;
        return a(0);
    end get_cbc_cnfg;


	function get_cbc_index( signal a : in cs_cbc_cnfg_array_type; signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0) ) return integer is
	variable cbc_index : integer  range 0 to 2**CBCID_WIDTH - 1;	
	begin	
		cbc_index := 0;
		for i in a'RANGE loop
			if cbc_id = a(i).id then
				cbc_index := i;
			end if;
		end loop;
		return cbc_index;
	end get_cbc_index;	


    function get_fe_active_and( signal sig : in std_logic_vector(MAX_N_FE-1 downto 0); signal fe_cnfg_array : in cs_fe_cnfg_array_type ) return std_logic is
        variable result : std_logic;
    begin
        result := '1';
        for fe_index in fe_cnfg_array'RANGE loop
            if( unsigned(fe_cnfg_array(fe_index).n_active_cbcs) > 0 ) then
                result := result and sig(fe_index);
            end if;
        end loop;
        return result;
    end get_fe_active_and;




	function bitswap ( signal a : in std_logic_vector ) return std_logic_vector is
		variable result : std_logic_vector( a'RANGE );
		alias ra : std_logic_vector( a'REVERSE_RANGE ) is a;
		begin
			for i in a'RANGE loop
				result(i) := ra(i);
			end loop;
			return result;	
	end bitswap;

end cbc_system_package;

