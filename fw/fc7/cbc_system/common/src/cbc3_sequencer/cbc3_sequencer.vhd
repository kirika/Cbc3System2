----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 20:40:44
-- Design Name: 
-- Module Name: cbc3_sequencer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ipbus.all;
use work.cbc_system_package.all;

entity cbc3_sequencer is
        generic (CBCID_WIDTH : positive := 5; NFE : positive := 1); 
        port(		
                    cbc_clk_m8               : in  std_logic;
                    cbc_clk                  : in  std_logic;
                    cbci2c_base_clk          : in  std_logic;
--                    clk_sync_bit             : in  std_logic;
                    cbc_cnfg_array_i         : in cs_cbc_cnfg_array_type;
                    fsm_o                    : out std_logic_vector(2 downto 0);
                    reset_i                  : in  std_logic;	
                    start_i                  : in  std_logic;
                    resume_i                 : in  std_logic;	
                    cbci2c_trans_all_done_i  : in  std_logic;    
                    cbci2c_com_count_reset   : out std_logic_vector(NFE-1 downto 0);
                    cbci2c_com_fifo_reset    : out std_logic_vector(NFE-1 downto 0);
                    cbci2c_com_fifo_we       : out std_logic_vector(NFE-1 downto 0);
                    cbci2c_com_fifo_din      : out std_logic_vector(NFE*32-1 downto 0);
                    cbci2c_reply_fifo_reset  : out std_logic_vector(NFE-1 downto 0);
                    fc_fast_reset           : out std_logic;
                    fc_trigger              : out std_logic;
                    fc_tpreq                : out std_logic;
                    fc_orbit_reset          : out std_logic;
                    rdb_write_trigger_o     : out std_logic;
                    rdb_write_ready         : in  std_logic;
                    ipb_clk                 : in  std_logic;
                    ipb_reset               : in  std_logic; 
                    ipb_mosi_i              : in  ipb_wbus; 
                    ipb_miso_o              : out ipb_rbus;
                    send_slow_com_o         : out std_logic;
                    test1                   : out std_logic;
                    seq_bram_addra          : out std_logic_vector(11 downto 0);
                    keithley_trig_o         : out std_logic;
                    keithley_cmp_i          : in  std_logic;
                    keithley_cmp_timer_o    : out std_logic_vector(19 downto 0);
                    ad7172_trig_o           : out std_logic;  -- a positive pulse with 50 ns width 
                    ad7172_done_i           : in  std_logic   -- a positive pulse with more than 3.125 ns width 
            );		
end cbc3_sequencer;		

architecture behavioral of cbc3_sequencer is		
		
	constant SLOW_CMD   : std_logic_vector(2 downto 0)  := "100";	
	constant FAST_CMD   : std_logic_vector(2 downto 0)  := "101";	
	constant PAUSE      : std_logic_vector(2 downto 0)  := "001";	
	constant STOP       : std_logic_vector(2 downto 0)  := "010";	
	constant LOOP_BEGIN : std_logic_vector(2 downto 0)  := "011";	
    constant LOOP_END   : std_logic_vector(2 downto 0)  := "110";
    constant END_DATA   : std_logic_vector(2 downto 0)  := "111";	
    constant OTHER_CMD  : std_logic_vector(2 downto 0)  := "000";
    
    constant WAIT_UNTIL_RESUME            : std_logic_vector(3 downto 0)  := "0001";
    constant WAIT_UNTIL_I2CBUS_WAITING    : std_logic_vector(3 downto 0)  := "0010";    
 	constant WAIT_UNTIL_RDB_WRITE_READY   : std_logic_vector(3 downto 0)  := "0100";  
    constant WAIT_UNTIL_KEITHLEY_CMP      : std_logic_vector(3 downto 0)  := "1001";
    constant WAIT_UNTIL_AD7172_DONE       : std_logic_vector(3 downto 0)  := "1000";

    -- BRAM for sequencer interface 
    signal seq_bram_wen         : std_logic;
    signal seq_bram_din         : std_logic_vector(31 downto 0);
    -- address to write commands
    signal uaddra               : unsigned(11 downto 0);	

    -- commands to the sequencer
    signal reset_m8             : std_logic;
    signal reset                : std_logic;
    signal start                : std_logic;
    signal resume_m8            : std_logic;
    signal resume               : std_logic;
        
    signal flip_40mhz           : std_logic;    
    signal flip_40mhz_1d           : std_logic;
    signal cbc_clk_2d           : std_logic;
    signal data_ready          : boolean;
    signal send_slow_com       : boolean;
    signal send_fast_com       : boolean;
    signal process_other_com   : boolean;
    signal reset_reply_fifo    : boolean;
	signal addr                : std_logic_vector(11 downto 0);	
	signal dout                : std_logic_vector(31 downto 0);	
	signal uaddr               : unsigned(11 downto 0);	

    type cbc3_sequencer_fsm_type is (sync40MHz, wait_data_ready, bram_ready, running, waiting);	
    --type cbc3_sequencer_fsm_type is (wait_data_ready, bram_ready, running, waiting);	

	signal fsm                 : cbc3_sequencer_fsm_type;	
	signal error               : std_logic;	

    signal cbci2c_com_cbc_data	       : std_logic_vector(31 downto 0);
    signal cbci2c_com_cbc_data_cbc_clk0 : std_logic_vector(31 downto 0);
    signal cbci2c_com_cbc_data_cbc_clk1 : std_logic_vector(31 downto 0);
	signal fe_index                    : integer range 0 to MAX_N_FE-1;		
--    signal cmd_fifo_din        : std_logic_vector(31 downto 0);
	signal fast_reset          : std_logic;
	signal trigger             : std_logic;
	signal test_pulse          : std_logic;
    signal orbit_reset         : std_logic;
 
    signal cbci2c_reset_reply_fifo : boolean;
    	
    type cbci2c_com_pre_fifo_data_type is array (0 to NFE-1) of std_logic_vector(31 downto 0);
    signal cbci2c_com_pre_fifo_din     : cbci2c_com_pre_fifo_data_type;
    signal cbci2c_com_pre_fifo_dout    : std_logic_vector(NFE*32-1 downto 0);
    signal cbci2c_com_pre_fifo_we      : std_logic_vector(NFE-1 downto 0);
    signal cbci2c_com_pre_fifo_rd_en   : std_logic_vector(NFE-1 downto 0);
    signal cbci2c_com_pre_fifo_empty   : std_logic_vector(NFE-1 downto 0);
--    signal slow_cmd_cbc_id_latched : std_logic_vector(CBCID_WIDTH-1 downto 0);		
		
	alias slow_cmd_cbc_id: std_logic_vector(CBCID_WIDTH-1 downto 0) is dout(28 downto 24);	
	alias slow_cmd_rw    : std_logic_vector(1 downto 0)             is dout(21 downto 20);	
	alias slow_cmd_page  : std_logic                                is dout(16);
	alias slow_cmd_addr  : std_logic_vector(7 downto 0)             is dout(15 downto  8);
	alias slow_cmd_value : std_logic_vector(7 downto 0)             is dout( 7 downto  0);	
	
	signal rdb_write_trigger      : std_logic;
	signal rdb_write_ready_320MHz : std_logic;  	

    signal cbc_cnfg_array          : cs_cbc_cnfg_array_type;
    signal cbci2c_trans_all_done_sync   : std_logic;
    signal cbci2c_trans_all_done_seq    : std_logic;
        
    signal slow_cmd_cbc_id_latched : std_logic_vector(CBCID_WIDTH-1 downto 0);
    signal fe_index_latched        : integer range 0 to MAX_N_FE-1;
    
    signal keithley_trig_com       : std_logic;
    signal keithley_trig           : std_logic;
    signal keithley_cmp_timer      : unsigned(19 downto 0);
    
    signal keithley_cmp_s          : std_logic_vector(1 downto 0);
    
    signal process_other_com_40MHz       : boolean;
    signal keithley_trig_com_40MHz       : std_logic;
    signal ad7172_trig_com_40MHz         : std_logic;

    signal ad7172_trig_com         : std_logic; 
    signal ad7172_trig_p           : std_logic_vector(1 downto 0); 
    signal ad7172_trig_s           : std_logic; 
    signal ad7172_done_s           : std_logic_vector(1 downto 0);
begin		

        fsm_o <= "000" when fsm = sync40MHz       else
                 "001" when fsm = wait_data_ready else
                 "010" when fsm = bram_ready      else
                 "011" when fsm = running         else
                 "100" when fsm = waiting;
            
        send_slow_com_o <= '1' when send_slow_com = true else '0';    
        --test1 <= cbci2c_com_pre_fifo_rd_en(0);
        test1 <= trigger;
        seq_bram_addra <= std_logic_vector(uaddra);
        keithley_trig_o <= keithley_trig;
        keithley_cmp_timer_o <= std_logic_vector(keithley_cmp_timer);

        ad7172_trig_o <= ad7172_trig_s;
        
        -- dout_o <= dout;
        seq_reset_m8 : entity work.dff_sync_edge_detect
                port map(   reset => '0',
                            clkb  => cbc_clk_m8,
                            dina  => reset_i,
                            doutb => reset_m8
                    );
        seq_reset : entity work.dff_sync_edge_detect
        port map(   reset => '0',
                    clkb  => cbc_clk,
                    dina  => reset_i,
                    doutb => reset
            );
        seq_start : entity work.dff_sync_edge_detect
        port map(   reset => '0',
                    clkb  => cbc_clk,
                    dina  => start_i,
                    doutb => start
            );    
        seq_resume : entity work.dff_sync_edge_detect
        port map(   reset => '0',
                    clkb  => cbc_clk,
                    dina  => resume_i, 
                    doutb => resume
            );      
        seq_resume_m8 : entity work.dff_sync_edge_detect
            port map(   reset => '0',
                        clkb  => cbc_clk_m8,
                        dina  => resume_i, 
                        doutb => resume_m8
                );      




        --==================
        -- BRAM to store commands for the sequencer 
        --==================

        ipb_fifo_write_if_inst : entity work.ipb_fifo_write_if
        port map( ipb_clk             => ipb_clk, 
                  ext_clk             => cbc_clk,  
                  reset               => ipb_reset, 
                  ipb_mosi_i          => ipb_mosi_i, 
                  ipb_miso_o          => ipb_miso_o, 
                  ext_fifo_wen        => seq_bram_wen,  
                  ext_fifo_din        => seq_bram_din 
          );

        seq_bram_inst : entity work.seq_bram
        PORT MAP (
                         clka   => cbc_clk,
                         wea(0) => seq_bram_wen,
                         addra  => std_logic_vector(uaddra),
                         dina   => seq_bram_din,
                         clkb   => cbc_clk_m8,
                         addrb  => addr,
                         doutb  => dout
                 );
                 
        
        
        --==================
        -- 320MHz cbc control clock domain
        -- read sequencer bram, analyze and process the commands
        --==================
        process (cbc_clk_m8, reset_m8)				
                variable timer            : integer range 0 to 7;	
                variable loop_begin_flag  : boolean;
                variable loop_end_flag    : boolean;
                variable pause_flag       : boolean;
                variable addr_inc         : boolean;
                variable addr_jump_to_loop_begin : boolean;					
                variable count_inc        : boolean;
                variable count_reset      : boolean;	
                variable loop_reset       : boolean;
                variable check_count      : boolean;

                variable wait_resume      : boolean;
                variable wait_ci2bmgr     : boolean;
                variable wait_rdb_write_ready : boolean;
                variable wait_keithley_cmp : boolean;
                variable wait_ad7172_done : boolean;

                variable keithley_cmp0    : boolean;
                variable ad7172_done_on   : boolean;
                variable jump_flag        : std_logic;
                variable count            : unsigned (11 downto 0);   
                variable ncount           : unsigned (11 downto 0);       
                variable n_loop           : unsigned (11 downto 0);
                variable loop_count       : unsigned (11 downto 0);  
                variable uaddr_loop_begin : unsigned (11 downto 0);  
                variable wait_type        : std_logic_vector(3 downto 0);  
                variable ctrl             : std_logic_vector(2 downto 0);	
                variable wait_count       : unsigned (7 downto 0);    
                variable check_cbci2c_trans_all_done : boolean;
                variable wait_count_0     : boolean;
        begin                                        
                if ( reset_m8 = '1' ) then    
                        loop_begin_flag  := false;
                        loop_end_flag    := false;
                        pause_flag       := false;   
                        addr_inc         := false;
                        addr_jump_to_loop_begin := false;  
                        count_inc        := false;                           
                        count_reset      := false;
                        check_count      := false; 
                        loop_reset       := false;
                        wait_resume      := false;
                        wait_ci2bmgr     := false;
                        wait_keithley_cmp := false;
                        keithley_cmp0    := false;
                        keithley_cmp_s     <= (others => '1');
                        wait_ad7172_done := false;
                        ad7172_done_on   := false;
                        ad7172_done_s    <= (others => '0');
                        jump_flag        := '0';
                        count            := (others => '0');
                        ncount           := (others => '0');
                        loop_count       := (others => '0');
                        uaddr_loop_begin := (others => '0'); 
                        wait_type        := (others => '0');
                        ctrl             := (others => '0');
                        error            <= '0'; 
                        uaddr            <= (others => '0');                       
                        send_slow_com    <= false;
                        send_fast_com    <= false;     
                        process_other_com <= false;                 
                        cbci2c_reset_reply_fifo <= false;
                        wait_count       := (others => '0');   
                        rdb_write_trigger  <= '0';      
                        fast_reset         <= '0';
                        trigger            <= '0';
                        test_pulse         <= '0';
                        orbit_reset        <= '0';   
        --            rdb_write_trigger <= '0';
                        cbc_cnfg_array <= cbc_cnfg_array_i;
                        fsm              <= sync40MHz;                                
                        --fsm              <= wait_data_ready;                                
                        check_cbci2c_trans_all_done := false;
                        wait_count_0 := true;
                        cbci2c_trans_all_done_sync <= '0';
                        cbci2c_trans_all_done_seq <= '0';                       
                elsif rising_edge(cbc_clk_m8) then     
                        
                        ad7172_done_s(1) <= ad7172_done_s(0);
                        ad7172_done_s(0) <= ad7172_done_i;
 
                        if timer = 0 then
                            cbci2c_trans_all_done_sync <=cbci2c_trans_all_done_i;
                            if cbci2c_trans_all_done_sync = cbci2c_trans_all_done_i then
                                cbci2c_trans_all_done_seq <= cbci2c_trans_all_done_sync;
                            end if;
                        end if;
                        
                        addr  <= std_logic_vector(uaddr);                      
            --            rdb_write_trigger <= '0';
                        rdb_write_ready_320MHz <= rdb_write_ready;           

                        flip_40mhz_1d <= flip_40mhz;
                        
                        keithley_cmp_s(0) <= keithley_cmp_i;
                        keithley_cmp_s(1) <= keithley_cmp_s(0);

                        timer := timer + 1;     
                        
  
                        
                        case fsm is    
                                when sync40MHz =>
--
--                                        if clk_sync_bit = '1' then
--                                                timer := 0;
--                                                fsm <= wait_data_ready;
--                                        end if;
                                            if flip_40mhz /= flip_40mhz_1d then
                                                timer := 1;
                                                fsm <=wait_data_ready;
                                            end if;
                                when wait_data_ready =>

                                        if timer = 0 and data_ready then
                                                fsm <= bram_ready;
                                        end if;

                                when bram_ready =>                                

                                        count      := (others => '0');
                                        ncount     := (others => '0');
                                        loop_count := (others => '0');                                                   
                                        uaddr      <= (others => '0');                       
                                        if start = '1' then                     
                                                fsm        <= running;                        
                                        end if;                            

                                when running =>        

                                        cbci2c_reset_reply_fifo <= false; 
                                        wait_count := (others => '0');      
                                        wait_count_0 := true;
                                        if count_reset then -- timer = 7
                                                count       := (others => '0');
                                                count_reset := false;
                                        end if;
                                        if addr_inc then -- timer = 7 for fc, 5 for sc                        
                                                uaddr <= uaddr + 1;
                                                addr_inc := false;
                                        end if;

                                        if addr_jump_to_loop_begin then
                                                uaddr <= uaddr_loop_begin;	
                                                addr_jump_to_loop_begin := false;
                                        end if;                

                                        if check_count then -- timer = 6
                                                if count = ncount then
                                                        count_reset := true;
                                                        addr_inc    := true; 
                                                end if;   
                                                check_count := false;
                                        end if;

                                        if count_inc then -- timer = 5         
                                                count := count + 1;
                                                check_count := true;
                                                count_inc := false;
                                        end if;               

                                        if pause_flag then  
                                                wait_count_0 := true;
                                                fsm <= waiting;
                                                if jump_flag = '1' then
                                                        uaddr <= unsigned(dout(11 downto 0)); 
                                                        jump_flag := '0';               
                                                end if;
                                                pause_flag := false;            
                                        end if;

                                        if loop_reset then
                                                loop_count := (others => '0');
                                                loop_reset := false;
                                        end if;

                                        if loop_end_flag then
                                                if loop_count /= n_loop or n_loop = 0 then
                                                        addr_jump_to_loop_begin := true;
                                                        --                        uaddr <= uaddr_loop_begin;
                                                else
                                                        addr_inc := true; 
                                                        loop_reset := true;
                                                end if;         
                                                loop_end_flag := false;
                                                loop_count := loop_count + 1;    
                                        end if;

                                        if loop_begin_flag then
                                                loop_count       := loop_count + 1;
                                                uaddr_loop_begin := uaddr + 1;        
                                                addr_inc         := true;             
                                                loop_begin_flag  := false;            
                                        end if;               

                                        if timer = 3 then
                                                send_slow_com      <= false;
                                                send_fast_com      <= false;
                                                process_other_com  <= false;
                                                ctrl := dout(31 downto 29);
                                        elsif timer = 4 then
                                                if ctrl = SLOW_CMD then                        
                                                        addr_inc      := true;
                                                        send_slow_com <= true;
                                                       
                                                        cbci2c_com_cbc_data  <=  "00000000" & slow_cmd_cbc_id & slow_cmd_rw & slow_cmd_page & slow_cmd_addr & slow_cmd_value; 
                                                        --slow_cmd_cbc_id_latched <= slow_cmd_cbc_id;
                                                        
                                                elsif ctrl = FAST_CMD then
                                                        count_inc := true;
                                                        ncount               := unsigned(dout(27 downto 16));
                                                        fast_reset           <= dout(12);                        
                                                        trigger              <= dout(8);                        
                                                        test_pulse           <= dout(4);                        
                                                        orbit_reset          <= dout(0); 
                                                        rdb_write_trigger    <= dout(9);  
                                                        send_fast_com <= true;
                                                elsif ctrl = PAUSE then                        
                                                        pause_flag := true;                 
                                                        wait_type  := dout(27 downto 24);    
                                                        jump_flag  := dout(16); 
                                                        addr_inc := true;                                                  
                                                elsif ctrl=LOOP_BEGIN then  
                                                        loop_begin_flag := true;         
                                                        n_loop := unsigned(dout(11 downto 0)); 
                                                elsif ctrl=LOOP_END then
                                                        loop_end_flag := true;                                      
                                                elsif ctrl=END_DATA then
                                                        fsm <= bram_ready; 
                                                elsif ctrl=OTHER_CMD then
                                                        process_other_com <= true;
                                                        keithley_trig_com <= dout(1);
                                                        ad7172_trig_com <= dout(0);
                                                        addr_inc         := true; 
                                                end if;                                                                        
                                        end if;

                                when waiting =>                                

                                        if timer = 6 then
                                                 
                                                 if wait_count = 255 then 
                                                     check_cbci2c_trans_all_done := true;
                                                     wait_count := (others => '0');                           
                                                 elsif  wait_count /= 0 then
                                                     wait_count := wait_count + 1; 
                                                 end if;
                                                                             
                                                -- initialize wait
                                                if wait_count_0 then
                                                        if wait_type = WAIT_UNTIL_RESUME then                                                      
                                                                wait_resume  := true;
                                                        elsif wait_type = WAIT_UNTIL_I2CBUS_WAITING then
                                                                wait_ci2bmgr := true;                                                                
                                                        elsif wait_type = WAIT_UNTIL_RDB_WRITE_READY then
                                                                wait_rdb_write_ready := true;
                                                        elsif wait_type = WAIT_UNTIL_KEITHLEY_CMP then
                                                                wait_keithley_cmp := true;
                                                        elsif wait_type = WAIT_UNTIL_AD7172_DONE then
                                                                wait_ad7172_done := true;
                                                        end if;
                                                        wait_count := wait_count + 1; 
                                                        wait_count_0 := false;
                                                end if;                                        

                                                if wait_resume and resume = '1' then  

                                                        wait_resume := false;
                                                        fsm <= running;                        
                                                end if;
                                                if wait_ci2bmgr and check_cbci2c_trans_all_done then
                                                       
                          
                                                                if cbci2c_trans_all_done_seq = '1' then                        
                                                                        --cbci2c_reset_reply_fifo <= true; 
                                                                        wait_ci2bmgr := false;                                                                                                                       
                                                                        check_cbci2c_trans_all_done := false;                      
                                                                        fsm <= running;                    
                                                                end if;       
               
                                                end if;
                                                if wait_rdb_write_ready and rdb_write_ready_320MHz = '1' then

                                                        wait_rdb_write_ready := false;                   
                                                        fsm <= running;   
                                                end if;
                                                if keithley_cmp0 and keithley_cmp_s(1) = '1' then
                                                    keithley_cmp0 := false;
                                                    wait_keithley_cmp := false;
                                                    fsm <= running;      
                                                end if;                                                 
                                                if wait_keithley_cmp and keithley_cmp_s = "00" then
                                                    keithley_cmp0 := true;
                                                end if;                            
                                                if ad7172_done_on and ad7172_done_s(1) = '0' then
                                                        ad7172_done_on   := false;
                                                        wait_ad7172_done := false;
                                                        fsm <= running;
                                                end if;
                                                if wait_ad7172_done and ad7172_done_s = "11" then
                                                    ad7172_done_on := true;
                                                end if;

                                        end if; 
                        end case;                                
                        
--                        if timer = 6 then                             
--                            if wait_count = 255 then 
--                                check_cbci2c_trans_all_done := true;
--                                wait_count := (others => '0');                           
--                            elsif  wait_count /= 0 then
--                                wait_count := wait_count + 1; 
--                            end if;
--                        end if;


                end if;                                    

        end process;                                        
        
        
         
        --==================
        -- 40MHz cbc control clock domain 
        -- write sequencer bram write i2c pre fifo and create fast command signals
        --==================
        process (cbc_clk, reset)	
                variable cbci2c_com_pre_fifo_we_flag : boolean;
                variable cbci2c_com_pre_fifo_get_fe_index_flag : boolean;
                variable end_data_flag : boolean;
                variable keithley_trig_timer  : integer range 0 to 127;	
                variable keithley_cmp_timer_on : boolean;
        begin
                if ( reset = '1' ) then    

                    end_data_flag := false;
                    data_ready <= false;
                    uaddra <= (others => '0'); 
                    cbci2c_com_pre_fifo_we_flag := false; 
                    cbci2c_com_pre_fifo_get_fe_index_flag := false; 
                    keithley_trig <= '1';
                    keithley_trig_timer := 0;
                    keithley_cmp_timer <= (others => '0');
                    
                    ad7172_trig_p <= (others => '0');
                    ad7172_trig_s <= '0';

                elsif rising_edge(cbc_clk) then

                        flip_40mhz <= not flip_40mhz;

                        process_other_com_40MHz <= process_other_com;
                        keithley_trig_com_40MHz <= keithley_trig_com;
                        ad7172_trig_com_40MHz   <= ad7172_trig_com;

                        cbci2c_com_pre_fifo_we  <= (others => '0');   
                        cbci2c_com_pre_fifo_din <= (others => (others => '0'));

                        
                        
                        data_ready              <= false;
                        fc_fast_reset           <= '0';                        
                        fc_trigger              <= '0';                        
                        fc_tpreq                <= '0';                        
                        fc_orbit_reset          <= '0';  
                        rdb_write_trigger_o     <= '0';  

                        ad7172_trig_p(0) <= '0';
                        ad7172_trig_p(1) <= ad7172_trig_p(0);
                        if ad7172_trig_p = "10" then
                                ad7172_trig_s <= '0';
                        end if;

                        if  end_data_flag then 
                                end_data_flag := false;                  
                                data_ready <= true;
                        end if;                     

                        if seq_bram_wen = '1' then
                                uaddra <= uaddra + 1;
                                if seq_bram_din(31 downto 29) = END_DATA then
                                        end_data_flag := true;
                                end if;
                        end if;            

                       if cbci2c_com_pre_fifo_we_flag then
                            cbci2c_com_pre_fifo_we(fe_index) <= '1';
                            cbci2c_com_pre_fifo_din(fe_index) <= cbci2c_com_cbc_data_cbc_clk1;   
                            cbci2c_com_pre_fifo_we_flag := false;                                             
                        end if;
                        if cbci2c_com_pre_fifo_get_fe_index_flag then
                            fe_index                <= to_integer(unsigned(get_cbc_cnfg(cbci2c_com_cbc_data_cbc_clk0(23 downto 19), cbc_cnfg_array).fe_id)-1); 
                            cbci2c_com_cbc_data_cbc_clk1 <= cbci2c_com_cbc_data_cbc_clk0;
                            cbci2c_com_pre_fifo_get_fe_index_flag := false;
                            cbci2c_com_pre_fifo_we_flag := true; 
                        end if;
                        if send_slow_com then
                             cbci2c_com_cbc_data_cbc_clk0 <= cbci2c_com_cbc_data;
                             cbci2c_com_pre_fifo_get_fe_index_flag := true;                            
                        end if;
                        
                        if send_fast_com then
                                fc_fast_reset           <= fast_reset;                        
                                fc_trigger              <= trigger;                        
                                fc_tpreq                <= test_pulse;                        
                                fc_orbit_reset          <= orbit_reset;    
                                rdb_write_trigger_o     <= rdb_write_trigger;                     
                        end if;

 
                         
                         if keithley_cmp_timer_on then
                            if keithley_cmp_s = "11" then
                                keithley_cmp_timer <= keithley_cmp_timer + 1;
                            else
                                keithley_cmp_timer_on := false;
                            end if;
                        end if;
                        
                        if keithley_trig_timer = 63 then
                            keithley_trig <= '1';
                            keithley_cmp_timer_on := true;
                            keithley_trig_timer := 0;
                            
                        end if;
 
                       if keithley_trig_timer /= 0 then
                              keithley_trig_timer := keithley_trig_timer + 1;
                       end if;
                       
                       if process_other_com_40MHz then
                               if keithley_trig_com_40MHz = '1' then
                                   keithley_trig <= '0';
                                   keithley_trig_timer := keithley_trig_timer + 1;
                                   keithley_cmp_timer <= (others => '0');
                               end if;
                               if ad7172_trig_com_40MHz = '1' then
                                   ad7172_trig_p(0) <= '1';
                                   ad7172_trig_s <= '1';
                               end if;
                       end if; 
                        

                end if;                         
        end process;


        --==================
        -- CBC I2C INTERFACE
        --==================

    cbci2c_com_fifo_din <= cbci2c_com_pre_fifo_dout;

    other_com_gen :
    for i in 0 to NFE-1 generate    
--        cbci2c_com_fifo_din(32 * (i+1) - 1 downto 32 * i) <= cbci2c_com_pre_fifo_dout       when fe_index = i else (others => '0'); 
        cbci2c_com_dp_fifo_inst : ENTITY work.dp_fifo_32b_512d -- temporary fifo to cross the clock domains
        PORT map(
                        rst    => '0',
                        wr_clk => cbc_clk,
                        rd_clk => cbci2c_base_clk,
                        din    => cbci2c_com_pre_fifo_din(i),
                        wr_en  => cbci2c_com_pre_fifo_we(i),
                        rd_en  => cbci2c_com_pre_fifo_rd_en(i),
                        dout   => cbci2c_com_pre_fifo_dout(32 * (i+1) - 1 downto 32 * i),
                        full   => open,
                        empty  => cbci2c_com_pre_fifo_empty(i)
                );

       process (reset, cbci2c_base_clk)
       variable timer                        : integer range 0 to 2;
       begin
            if reset='1' then                    
                timer := 0;       
            elsif rising_edge(cbci2c_base_clk) then
            
                cbci2c_com_fifo_we(i)        <= '0';   
                cbci2c_com_pre_fifo_rd_en(i) <= '0';               
                if timer = 2 then
                        cbci2c_com_fifo_we(i) <= '1';
                        timer := 0;
                elsif timer = 0 then
                        if cbci2c_com_pre_fifo_empty(i) = '0'  then
                                cbci2c_com_pre_fifo_rd_en(i) <= '1';
                                timer := timer + 1;
                        end if;
                else
                        timer := timer + 1;
                end if;
            end if;
       end process;

    end generate other_com_gen;


                                                -- cbc i2c interface
        process (reset, cbci2c_base_clk)         
                variable cbci2c_reset_com_count_flag  : boolean;
                variable cbci2c_reset_com_fifo_flag   : boolean;
                variable cbci2c_reset_reply_fifo_flag : boolean;
        begin
                if reset='1' then        
                        cbci2c_reset_com_count_flag  := true;
                        cbci2c_reset_reply_fifo_flag := true;

                elsif cbci2c_reset_reply_fifo then

                        cbci2c_reset_reply_fifo_flag := true;

                elsif rising_edge(cbci2c_base_clk) then

                        cbci2c_com_count_reset  <= (others => '0');   
                        cbci2c_com_fifo_reset   <= (others => '0');   
                        cbci2c_reply_fifo_reset <= (others => '0');   
  
    
                        if cbci2c_reset_com_count_flag then
                                cbci2c_com_count_reset  <= (others => '1');   
                                cbci2c_reset_com_count_flag := false;
                        end if;

                        if cbci2c_reset_com_fifo_flag then
                                cbci2c_com_fifo_reset  <= (others => '1');   
                                cbci2c_reset_com_fifo_flag := false;
                        end if;

                        if cbci2c_reset_reply_fifo_flag then
                                cbci2c_reply_fifo_reset <= (others => '1');               
                                cbci2c_reset_reply_fifo_flag := false;
                        end if;
                                                                -- command fifo interface
                end if;
        end process;

end Behavioral;
