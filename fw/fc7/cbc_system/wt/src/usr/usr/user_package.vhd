--===========================--
-- ipb_cbc_system_cnfg_sel
-- ipb_cbc_system_ctrl_sel
-- ipb_cbc_system_stat_sel
-- ipb_cbc_system_event_data_sel
-- ipb_cbc_system_cbc_i2c_regs_sel 
-- ipb_cbc_data_clock_timing_scan_bram_sel
-- ipb_cfscbram_sel
-- are added.
--
-- 28.10.2016 Kirika Uchida
--===========================--

library ieee;
use ieee.std_logic_1164.all;
--use work.cbc_system_package.all;
 
package user_package is

	constant sys_phase_mon_freq      : string   := "160MHz"; -- valid options only "160MHz" or "240MHz"    

	--=== system options ========--
	--constant sys_eth_p1_enable        : boolean  := false;   
	--constant sys_pcie_enable          : boolean  := false;      

    --=== i2c master components ==--
	--constant i2c_master_enable			  : boolean  := true;
	--constant auto_eeprom_read_enable	: boolean  := true;    

    --=== wishbone slaves ========--
	--constant number_of_wb_slaves		: positive := 1 ;
    
	--constant user_wb_regs				    : integer  := 0 ;
	--constant user_wb_timer				  : integer  := 1 ;
    
    
    --=== ipb slaves =============--
  constant nbr_usr_slaves		: positive := 13;

  constant user_ipb_stat_regs		                      : integer  :=  0;
  constant user_ipb_ctrl_regs		                      : integer  :=  1;
  constant cs_ipb_cnfg_sel		                          : integer  :=  2;
  constant cs_ipb_ctrl_sel		                          : integer  :=  3;	
  constant cs_ipb_stat_sel		                          : integer  :=  4;
  constant cs_ipb_l1_data_buf_sel                         : integer  :=  5;		
  constant cs_ipb_cbc_i2c_data_buf_sel                    : integer  :=  6;
  constant cs_ipb_data_from_cbc_iserdes_cnd_scan_bram_sel : integer  :=  7;
  constant cs_ipb_raw_data_buf_sel                        : integer  :=  8;
  constant cs_ipb_ctrl_seq_data_buf_sel                   : integer  :=  9;
  constant user_relay1_regs                               : integer  := 10;
  constant user_spi1_regs                                 : integer  := 11;
  constant user_spi2_regs                                 : integer  := 12;
end user_package;
        
package body user_package is
end user_package;
