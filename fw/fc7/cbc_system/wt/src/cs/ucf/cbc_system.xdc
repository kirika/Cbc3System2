#===========================--
# Temporary IOs. To be updated.  
#
# 28.10.2016 Kirika Uchida
#===========================--

#CBC0 IOs
set_property PACKAGE_PIN AL30    [get_ports {fmc_l8_la_p[27]}]
set_property PACKAGE_PIN AK28    [get_ports {fmc_l8_la_p[23]}]
set_property PACKAGE_PIN AL25    [get_ports {fmc_l8_la_p[18]}]
set_property PACKAGE_PIN AK26    [get_ports {fmc_l8_la_p[17]}]
set_property PACKAGE_PIN AN32    [get_ports {fmc_l8_la_p[14]}]
set_property PACKAGE_PIN AK34    [get_ports {fmc_l8_la_p[13]}]
set_property PACKAGE_PIN AK33    [get_ports {fmc_l8_la_p[26]}]
set_property PACKAGE_PIN AM33    [get_ports {fmc_l8_la_p[21]}]
set_property PACKAGE_PIN AP31    [get_ports {fmc_l8_la_p[10]}]
set_property PACKAGE_PIN AN29    [get_ports {fmc_l8_la_p[9]}]
set_property PACKAGE_PIN AP26    [get_ports {fmc_l8_la_p[5]}]
set_property PACKAGE_PIN AN25    [get_ports {fmc_l8_la_p[6]}]


set_property PACKAGE_PIN AL31    [get_ports {fmc_l8_la_n[27]}]
set_property PACKAGE_PIN AL28    [get_ports {fmc_l8_la_n[23]}]
set_property PACKAGE_PIN AL26    [get_ports {fmc_l8_la_n[18]}]
set_property PACKAGE_PIN AK27    [get_ports {fmc_l8_la_n[17]}]
set_property PACKAGE_PIN AP33    [get_ports {fmc_l8_la_n[14]}]
set_property PACKAGE_PIN AL34    [get_ports {fmc_l8_la_n[13]}]
set_property PACKAGE_PIN AL33    [get_ports {fmc_l8_la_n[26]}]
set_property PACKAGE_PIN AN34    [get_ports {fmc_l8_la_n[21]}]
set_property PACKAGE_PIN AP32    [get_ports {fmc_l8_la_n[10]}]
set_property PACKAGE_PIN AN30    [get_ports {fmc_l8_la_n[9]}]
set_property PACKAGE_PIN AP27    [get_ports {fmc_l8_la_n[5]}]
set_property PACKAGE_PIN AP25    [get_ports {fmc_l8_la_n[6]}]

#probably not needed as the iserdes has type LVDS25...
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[13]}]

#for differential cbc data inputs
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[27]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[23]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[18]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[17]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[14]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[13]}]




set_property PACKAGE_PIN AK24    [get_ports {fmc_l8_la_p[2]}]
set_property PACKAGE_PIN AM23    [get_ports {fmc_l8_la_p[3]}]
set_property PACKAGE_PIN AN24    [get_ports {fmc_l8_la_p[4]}]
set_property PACKAGE_PIN AN27    [get_ports {fmc_l8_la_p[8]}]
set_property PACKAGE_PIN AP29    [get_ports {fmc_l8_la_p[7]}]
set_property PACKAGE_PIN AM25    [get_ports {fmc_l8_la_p[12]}]
set_property PACKAGE_PIN AL29    [get_ports {fmc_l8_la_p[11]}]
set_property PACKAGE_PIN AJ32    [get_ports {fmc_l8_la_p[16]}]
set_property PACKAGE_PIN AH34    [get_ports {fmc_l8_la_p[15]}]
set_property PACKAGE_PIN AM27    [get_ports {fmc_l8_la_p[20]}]
set_property PACKAGE_PIN AG33    [get_ports {fmc_l8_la_p[19]}]
set_property PACKAGE_PIN AF30    [get_ports {fmc_l8_la_p[22]}]

set_property PACKAGE_PIN AL24    [get_ports {fmc_l8_la_n[2]}]
set_property PACKAGE_PIN AN23    [get_ports {fmc_l8_la_n[3]}]
set_property PACKAGE_PIN AP24    [get_ports {fmc_l8_la_n[4]}]
set_property PACKAGE_PIN AN28    [get_ports {fmc_l8_la_n[8]}]
set_property PACKAGE_PIN AM26    [get_ports {fmc_l8_la_n[12]}]
set_property PACKAGE_PIN AM30    [get_ports {fmc_l8_la_n[11]}]
set_property PACKAGE_PIN AK32    [get_ports {fmc_l8_la_n[16]}]
set_property PACKAGE_PIN AJ34    [get_ports {fmc_l8_la_n[15]}]
set_property PACKAGE_PIN AM28    [get_ports {fmc_l8_la_n[20]}]
set_property PACKAGE_PIN AH33    [get_ports {fmc_l8_la_n[19]}]
set_property PACKAGE_PIN AG30    [get_ports {fmc_l8_la_n[22]}]


set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[7]}]


set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[20]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[15]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[16]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[11]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[12]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[7]}]






set_property PACKAGE_PIN H32     [get_ports {fmc_l12_la_p[27]}]
set_property PACKAGE_PIN L33     [get_ports {fmc_l12_la_p[23]}]
set_property PACKAGE_PIN E31     [get_ports {fmc_l12_la_p[18]}]
set_property PACKAGE_PIN E32     [get_ports {fmc_l12_la_p[17]}]
set_property PACKAGE_PIN F33     [get_ports {fmc_l12_la_p[14]}]
set_property PACKAGE_PIN F34     [get_ports {fmc_l12_la_p[13]}]
set_property PACKAGE_PIN H29     [get_ports {fmc_l12_la_p[26]}]
set_property PACKAGE_PIN L31     [get_ports {fmc_l12_la_p[21]}]
set_property PACKAGE_PIN P30     [get_ports {fmc_l12_la_p[10]}]
set_property PACKAGE_PIN M30     [get_ports {fmc_l12_la_p[9]}]
set_property PACKAGE_PIN N33     [get_ports {fmc_l12_la_p[5]}]
set_property PACKAGE_PIN R33     [get_ports {fmc_l12_la_p[6]}]

set_property PACKAGE_PIN G32     [get_ports {fmc_l12_la_n[27]}]
set_property PACKAGE_PIN K33     [get_ports {fmc_l12_la_n[23]}]
set_property PACKAGE_PIN D31     [get_ports {fmc_l12_la_n[18]}]
set_property PACKAGE_PIN D32     [get_ports {fmc_l12_la_n[17]}]
set_property PACKAGE_PIN E33     [get_ports {fmc_l12_la_n[14]}]
set_property PACKAGE_PIN E34     [get_ports {fmc_l12_la_n[13]}]
set_property PACKAGE_PIN H30     [get_ports {fmc_l12_la_n[26]}]
set_property PACKAGE_PIN K31     [get_ports {fmc_l12_la_n[21]}]
set_property PACKAGE_PIN N30     [get_ports {fmc_l12_la_n[10]}]
set_property PACKAGE_PIN M31     [get_ports {fmc_l12_la_n[9]}]
set_property PACKAGE_PIN M33     [get_ports {fmc_l12_la_n[5]}]
set_property PACKAGE_PIN R34     [get_ports {fmc_l12_la_n[6]}]




set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[13]}]

#for differential cbc data inputs
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[27]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[23]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[18]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[17]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[14]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[13]}]



set_property PACKAGE_PIN T31     [get_ports {fmc_l12_la_n[2]}]
set_property PACKAGE_PIN P32     [get_ports {fmc_l12_la_n[3]}]
set_property PACKAGE_PIN U33     [get_ports {fmc_l12_la_n[4]}]
set_property PACKAGE_PIN T34     [get_ports {fmc_l12_la_n[8]}]
set_property PACKAGE_PIN N34     [get_ports {fmc_l12_la_n[7]}]
set_property PACKAGE_PIN J32     [get_ports {fmc_l12_la_n[12]}]
set_property PACKAGE_PIN K34     [get_ports {fmc_l12_la_n[11]}]
set_property PACKAGE_PIN G33     [get_ports {fmc_l12_la_n[16]}]
set_property PACKAGE_PIN H34     [get_ports {fmc_l12_la_n[15]}]
set_property PACKAGE_PIN C34     [get_ports {fmc_l12_la_n[20]}]
set_property PACKAGE_PIN A33     [get_ports {fmc_l12_la_n[19]}]
set_property PACKAGE_PIN M32     [get_ports {fmc_l12_la_n[22]}]

set_property PACKAGE_PIN T30     [get_ports {fmc_l12_la_p[2]}]
set_property PACKAGE_PIN R32     [get_ports {fmc_l12_la_p[3]}]
set_property PACKAGE_PIN U32     [get_ports {fmc_l12_la_p[4]}]
set_property PACKAGE_PIN T33     [get_ports {fmc_l12_la_p[8]}]
set_property PACKAGE_PIN P34     [get_ports {fmc_l12_la_p[7]}]
set_property PACKAGE_PIN K32     [get_ports {fmc_l12_la_p[12]}]
set_property PACKAGE_PIN L34     [get_ports {fmc_l12_la_p[11]}]
set_property PACKAGE_PIN H33     [get_ports {fmc_l12_la_p[16]}]
set_property PACKAGE_PIN J34     [get_ports {fmc_l12_la_p[15]}]
set_property PACKAGE_PIN D34     [get_ports {fmc_l12_la_p[20]}]
set_property PACKAGE_PIN B33     [get_ports {fmc_l12_la_p[19]}]
set_property PACKAGE_PIN N32     [get_ports {fmc_l12_la_p[22]}]


set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[20]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[15]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[16]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[11]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[12]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[7]}]

set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[7]}]









#FPP (for now)
#set_property PACKAGE_PIN AP30    [get_ports {fmc_l8_la_n[7]}]
#set_property PACKAGE_PIN AP29    [get_ports {fmc_l8_la_p[7]}]

#test signals
set_property PACKAGE_PIN AE31    [get_ports {fmc_l8_la_p[33]}]
set_property PACKAGE_PIN AE33    [get_ports {fmc_l8_la_p[31]}]
set_property PACKAGE_PIN AE32    [get_ports {fmc_l8_la_n[33]}]
set_property PACKAGE_PIN AF33    [get_ports {fmc_l8_la_n[31]}]

set_property PACKAGE_PIN C29     [get_ports {fmc_l12_la_p[33]}]
set_property PACKAGE_PIN B30     [get_ports {fmc_l12_la_p[31]}]
set_property PACKAGE_PIN C30     [get_ports {fmc_l12_la_n[33]}]
set_property PACKAGE_PIN A31     [get_ports {fmc_l12_la_n[31]}]




#NIM IO
#set_property PACKAGE_PIN J34     [get_ports {fmc_l12_la_p[15]}]
#set_property PACKAGE_PIN D34     [get_ports {fmc_l12_la_p[20]}]
#set_property PACKAGE_PIN B33     [get_ports {fmc_l12_la_p[19]}]
#set_property PACKAGE_PIN N32     [get_ports {fmc_l12_la_p[22]}]
#set_property PACKAGE_PIN H34     [get_ports {fmc_l12_la_n[15]}]
#set_property PACKAGE_PIN C34     [get_ports {fmc_l12_la_n[20]}]
#set_property PACKAGE_PIN A33     [get_ports {fmc_l12_la_n[19]}]
#set_property PACKAGE_PIN M32     [get_ports {fmc_l12_la_n[22]}]







#for SPI to DAC and ADCs
set_property PACKAGE_PIN AD31    [get_ports {fmc_l8_la_p[28]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[28]}]
set_property PACKAGE_PIN AE34    [get_ports {fmc_l8_la_p[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[25]}]
set_property PACKAGE_PIN AH30    [get_ports {fmc_l8_la_p[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[24]}]

set_property PACKAGE_PIN AD32    [get_ports {fmc_l8_la_n[28]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[28]}]
set_property PACKAGE_PIN AF34    [get_ports {fmc_l8_la_n[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[25]}]
set_property PACKAGE_PIN AJ30    [get_ports {fmc_l8_la_n[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[24]}]

#for B33 control
set_property PACKAGE_PIN AC34    [get_ports {fmc_l8_la_p[30]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[30]}]
set_property PACKAGE_PIN AD34    [get_ports {fmc_l8_la_n[30]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[30]}]

set_property PACKAGE_PIN D29     [get_ports {fmc_l12_la_p[30]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[30]}]
set_property PACKAGE_PIN D30     [get_ports {fmc_l12_la_n[30]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[30]}]



set_property PACKAGE_PIN F29     [get_ports {fmc_l12_la_p[28]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[28]}]
set_property PACKAGE_PIN C32     [get_ports {fmc_l12_la_p[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[25]}]
set_property PACKAGE_PIN G31     [get_ports {fmc_l12_la_p[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[24]}]

set_property PACKAGE_PIN E29     [get_ports {fmc_l12_la_n[28]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[28]}]
set_property PACKAGE_PIN C33     [get_ports {fmc_l12_la_n[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[25]}]
set_property PACKAGE_PIN F31     [get_ports {fmc_l12_la_n[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[24]}]


#power control signals
set_property PACKAGE_PIN AC32    [get_ports {fmc_l8_la_p[32]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[32]}]
set_property PACKAGE_PIN AC33    [get_ports {fmc_l8_la_n[32]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[32]}]
set_property PACKAGE_PIN A29     [get_ports {fmc_l12_la_p[32]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[32]}]
set_property PACKAGE_PIN A30     [get_ports {fmc_l12_la_n[32]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[32]}]



#for touchdown sensor (on any FMC)


set_property PACKAGE_PIN B32     [get_ports {fmc_l12_la_n[29]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[29]}]

set_property PACKAGE_PIN AJ29    [get_ports {fmc_l8_la_p[29]}] 
set_property IOSTANDARD LVCMOS25  [get_ports {fmc_l8_la_p[29]}]  

set_property PACKAGE_PIN AK29    [get_ports {fmc_l8_la_n[29]}] 
set_property IOSTANDARD LVCMOS25  [get_ports {fmc_l8_la_n[29]}]  


set_property PACKAGE_PIN B31     [get_ports {fmc_l12_la_p[29]}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[29]}]


#set_clock_groups -asynchronous \
#-group {clk_out1_cbc_mmcm0 clk_out2_cbc_mmcm0} \
#-group {clk_out1_cs_mmcm0 clk_out2_cs_mmcm0} \
#-group {clk_out3_cs_mmcm0} \
#-group {clk_out1_cbc_data_iserdes_clocks_mmcm clk_out2_cbc_data_iserdes_clocks_mmcm} \
#-group {clkout0}



set_clock_groups -asynchronous \
-group {clk_out1_cbc_mmcm0 clk_out2_cbc_mmcm0} \
-group {clk_out1_cs_mmcm0 clk_out2_cs_mmcm0} \
-group {clk_out3_cs_mmcm0} \
-group {clk_out1_cbc_data_iserdes_clocks_mmcm clk_out2_cbc_data_iserdes_clocks_mmcm} \
-group {clk_out1_cbc_data_iserdes_clocks_mmcm_1 clk_out2_cbc_data_iserdes_clocks_mmcm_1} \
-group {clk_out1_cbc_data_iserdes_clocks_mmcm_2 clk_out2_cbc_data_iserdes_clocks_mmcm_2} \
-group {clk_out1_cbc_data_iserdes_clocks_mmcm_3 clk_out2_cbc_data_iserdes_clocks_mmcm_3} \
-group {clkout0}

		
set_property IODELAY_GROUP cbc_data_iserdes_group1 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[0].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
set_property IODELAY_GROUP cbc_data_iserdes_group1 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[1].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
set_property IODELAY_GROUP cbc_data_iserdes_group1 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[2].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
set_property IODELAY_GROUP cbc_data_iserdes_group1 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[3].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]


#set_property IODELAY_GROUP cbc_data_iserdes_group1 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[1].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
#set_property IODELAY_GROUP cbc_data_iserdes_group2 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[2].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
#set_property IODELAY_GROUP cbc_data_iserdes_group2 [get_cells usr/io_map/cbc_data_array_maker_inst/cbc_data_iserdes_gen[3].cbc_data_iserdes_inst/cbc_data_iserdes_inst/inst/*delay*]
