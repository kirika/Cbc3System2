--===========================--
-- cbc_system_inst : entity work.cbc_system(single_cbc3)
-- is added.
-- rarp & i2c_eeprom are disabled
-- 28.10.2016 Kirika Uchida
--===========================--
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.cbc_system_package.all;
use work.cbc_system_config_package.all;
library unisim;
use unisim.vcomponents.all;

entity user_logic is 
        port
        (

                --# led
                usrled1_r                 : out std_logic; -- fmc_l12_spare[8]
                usrled1_g                 : out std_logic; -- fmc_l12_spare[9]
                usrled1_b                 : out std_logic; -- fmc_l12_spare[10]
                usrled2_r                 : out std_logic; -- fmc_l12_spare[11]
                usrled2_g                 : out std_logic; -- fmc_l12_spare[12]
                usrled2_b                 : out std_logic; -- fmc_l12_spare[13]

                --# on-board fabric clk
                fabric_clk_p              : in  std_logic; -- new port [PV 2015.08.19]
                fabric_clk_n              : in  std_logic; -- new port [PV 2015.08.19]
                fabric_coax_or_osc_p      : in  std_logic;
                fabric_coax_or_osc_n      : in  std_logic;

                --# on-board mgt clk
                pcie_clk_p                : in  std_logic;
                pcie_clk_n                : in  std_logic;
                osc_xpoint_a_p            : in  std_logic;
                osc_xpoint_a_n            : in  std_logic;
                osc_xpoint_b_p            : in  std_logic;
                osc_xpoint_b_n            : in  std_logic;
                osc_xpoint_c_p            : in  std_logic;
                osc_xpoint_c_n            : in  std_logic;
                osc_xpoint_d_p            : in  std_logic;
                osc_xpoint_d_n            : in  std_logic;
                ttc_mgt_xpoint_a_p        : in  std_logic;
                ttc_mgt_xpoint_a_n        : in  std_logic;
                ttc_mgt_xpoint_b_p        : in  std_logic;
                ttc_mgt_xpoint_b_n        : in  std_logic;
                ttc_mgt_xpoint_c_p        : in  std_logic;
                ttc_mgt_xpoint_c_n        : in  std_logic;

                --# fmc mgt clk     
                fmc_l12_gbtclk0_a_p       : in  std_logic; 
                fmc_l12_gbtclk0_a_n       : in  std_logic; 
                fmc_l12_gbtclk1_a_p       : in  std_logic; 
                fmc_l12_gbtclk1_a_n       : in  std_logic; 
                fmc_l12_gbtclk0_b_p       : in  std_logic; 
                fmc_l12_gbtclk0_b_n       : in  std_logic; 
                fmc_l12_gbtclk1_b_p       : in  std_logic; 
                fmc_l12_gbtclk1_b_n       : in  std_logic; 
                fmc_l8_gbtclk0_p          : in  std_logic; 
                fmc_l8_gbtclk0_n          : in  std_logic; 
                fmc_l8_gbtclk1_p          : in  std_logic; 
                fmc_l8_gbtclk1_n          : in  std_logic; 

                --# fmc mgt
                fmc_l12_dp_c2m_p          : out std_logic_vector(11 downto 0);
                fmc_l12_dp_c2m_n          : out std_logic_vector(11 downto 0);
                fmc_l12_dp_m2c_p          : in  std_logic_vector(11 downto 0);
                fmc_l12_dp_m2c_n          : in  std_logic_vector(11 downto 0);
                fmc_l8_dp_c2m_p           : out std_logic_vector( 7 downto 0);
                fmc_l8_dp_c2m_n           : out std_logic_vector( 7 downto 0);
                fmc_l8_dp_m2c_p           : in  std_logic_vector( 7 downto 0);
                fmc_l8_dp_m2c_n           : in  std_logic_vector( 7 downto 0);

                --# fmc fabric clk  
                fmc_l8_clk0               : in    std_logic; 
                fmc_l8_clk1               : in    std_logic;
                fmc_l12_clk0              : in    std_logic;
                fmc_l12_clk1              : in    std_logic;    

                --# fmc gpio        
                fmc_l8_la_p               : inout std_logic_vector(33 downto 0);
                fmc_l8_la_n               : inout std_logic_vector(33 downto 0);
                fmc_l12_la_p              : inout std_logic_vector(33 downto 0);
                fmc_l12_la_n              : inout std_logic_vector(33 downto 0);
                                          
                --# amc mgt               
                k7_amc_rx_p               : inout std_logic_vector(15 downto 1);
                k7_amc_rx_n               : inout std_logic_vector(15 downto 1);
                amc_tx_p                  : inout std_logic_vector(15 downto 1);
                amc_tx_n                  : inout std_logic_vector(15 downto 1);

                --# amc fabric
                k7_fabric_amc_rx_p03      : inout std_logic;
                k7_fabric_amc_rx_n03      : inout std_logic;
                k7_fabric_amc_tx_p03      : inout std_logic;
                k7_fabric_amc_tx_n03      : inout std_logic;

                --# ddr3
                ddr3_sys_clk_p            : in    std_logic;
                ddr3_sys_clk_n            : in    std_logic;
                ddr3_dq                   : inout std_logic_vector( 31 downto 0);
                ddr3_dqs_p                : inout std_logic_vector(  3 downto 0);
                ddr3_dqs_n                : inout std_logic_vector(  3 downto 0);
                ddr3_addr                 : out   std_logic_vector( 13 downto 0);
                ddr3_ba                   : out   std_logic_vector(  2 downto 0);
                ddr3_ras_n                : out   std_logic;
                ddr3_cas_n                : out   std_logic;
                ddr3_we_n                 : out   std_logic;
                ddr3_reset_n              : out   std_logic;
                ddr3_ck_p                 : out   std_logic_vector(  0 downto 0);
                ddr3_ck_n                 : out   std_logic_vector(  0 downto 0);
                ddr3_cke                  : out   std_logic_vector(  0 downto 0);
                ddr3_cs_n                 : out   std_logic_vector(  0 downto 0);
                ddr3_dm                   : out   std_logic_vector(  3 downto 0);
                ddr3_odt                  : out   std_logic_vector(  0 downto 0);

    --# cdce
    cdce_pll_lock_i           : in    std_logic; -- new port [PV 2015.08.19]  
  cdce_pri_clk_bufg_o         : out     std_logic; -- new port [PV 2015.08.19] 
  cdce_ref_sel_o            : out   std_logic; -- new port [PV 2015.08.19]   
  cdce_pwrdown_o            : out   std_logic; -- new port [PV 2015.08.19]  
  cdce_sync_o               : out   std_logic; -- new port [PV 2015.08.19]  
  cdce_sync_clk_o           : out   std_logic; -- new port [PV 2015.08.19]  

    --# system clk      
    osc125_a_bufg_i                   : in  std_logic;
    osc125_a_mgtrefclk_i            : in    std_logic;
    osc125_b_bufg_i                   : in  std_logic;
    osc125_b_mgtrefclk_i            : in    std_logic;
  clk_31_250_bufg_i           : in  std_logic; -- new port [PV 2015.08.19]
    
    --# ipbus comm    
    ipb_clk_o                       : out   std_logic;
    ipb_rst_i                       : in    std_logic;
    ipb_miso_o                  : out   ipb_rbus_array(0 to nbr_usr_slaves-1);
    ipb_mosi_i                  : in    ipb_wbus_array(0 to nbr_usr_slaves-1);

    --# ipbus conf
    ip_addr_o                           : out   std_logic_vector(31 downto 0);
  mac_addr_o              : out   std_logic_vector(47 downto 0);
  rarp_en_o               : out   std_logic;
  use_i2c_eeprom_o        : out   std_logic;
  
  fmc_i2c_scl             : inout  std_logic;
  fmc_i2c_sda             : inout  std_logic
   
);
end user_logic;

architecture usr of user_logic is

    -- mmcm & clock signals
    signal ipb_clk                          : std_logic;
    
  signal fabric_clk_ibufgds_o     : std_logic;                
  signal fabric_clk               : std_logic;
  
  signal ctrl_reg                     : array_32x32bit;
  signal stat_reg                     : array_32x32bit;

  signal cdce_pri_clk_bufg        : std_logic;

  signal cdce_sync_from_ipb       : std_logic;     
  signal cdce_sel_from_ipb        : std_logic;                    
  signal cdce_pwrdown_from_ipb    : std_logic;
  signal cdce_ctrl_from_ipb       : std_logic;  
--  signal cdce_sel_from_fabric     : std_logic;                    
--  signal cdce_pwrdown_from_fabric : std_logic;
--  signal cdce_ctrl_from_fabric    : std_logic; 

    -- daq clocks
    signal daq_clk                  : std_logic;
    signal daq_slow_clk            : std_logic;
  signal cs_mmcm0_locked          : std_logic;
  signal cs_mmcm0_clk_40MHz       : std_logic;
  signal cs_mmcm0_clk_320MHz      : std_logic;
  signal cs_mmcm0_clk_10MHz       : std_logic;
  -- clock for idelayctrl
  signal clk_200MHz               : std_logic;
  signal clk_200MHz_locked        : std_logic;
  signal not_clk_200MHz_locked    : std_logic;
    -- cbc clocks
    signal cbc_clk                  : std_logic;
    signal cbc_clk_m8               : std_logic;
  signal cbc_mmcm_clk_in          : std_logic;
  signal cbc_clk_select           : std_logic;
  signal ext_cbc_clk              : std_logic;
    -- clock for tdc
  -- signal clk_640MHz                     : std_logic;

--  -- external trigger in and veto out
  signal ext_async_l1a            : std_logic;   
  signal ext_l1a                  : std_logic;
  signal ext_l1a_tdc              : std_logic_vector(2 downto 0);
  signal ext_l1a_veto             : std_logic;   
--
--  -- section data input
--  signal cbc_par_data_array       : cbc_par_data_array_type;
    signal cbc_par_data           : std_logic_vector(NCBC*6*8-1 downto 0);
    signal cbc_data_clk           : std_logic_vector(NCBC-1 downto 0);
    signal cbc_data_clk_m8        : std_logic_vector(NCBC-1 downto 0);
 
    signal cbc_data_array_maker_test_signal1 : std_logic;    
    signal cbc_data_array_maker_test_signal2 : std_logic;
--
--  -- test signals
  signal test_signal_out          : std_logic_vector(1 downto 0);
--
  -- cbc system cnfg, ctrl, and stat signals
    signal cs_cnfg                  : cs_cnfg_type;
    signal cs_ctrl                  : cs_ctrl_type;
    signal cs_stat                  : cs_stat_type;
--
--  -- fast command manager signals 
  signal int_l1a_veto             : std_logic;   
  signal cbc_fast_com             : cbc_fast_com_type;
  signal cbc_fast_com_8bits       : std_logic_vector(7 downto 0);  
  signal l1a_tdc                  : std_logic_vector(2 downto 0);
  signal l1a_count                : std_logic_vector(8 downto 0);
  signal ext_fast_com             : cbc_fast_com_type;
  signal seq_fast_com             : cbc_fast_com_type;


  signal l1a_fifo_din             : std_logic_vector(31 downto 0);
  signal l1a_fifo_dout            : std_logic_vector(31 downto 0);
  signal l1a_fifo_re              : std_logic;
  signal l1a_fifo_empty           : std_logic;
--
--  -- i2c bus manager signals
  signal cbc_i2c_bus_miso_array   : cbc_i2c_bus_miso_array_type;
  signal cbc_i2c_bus_mosi_array   : cbc_i2c_bus_mosi_array_type;    
  signal other_com_count_reset    : std_logic_vector(NFE-1 downto 0);
  signal other_com_fifo_reset     : std_logic_vector(NFE-1 downto 0);
  signal other_com_fifo_we        : std_logic_vector(NFE-1 downto 0);
  signal other_com_fifo_din       : std_logic_vector(NFE*32-1 downto 0);
  signal other_reply_fifo_din     : std_logic_vector(NFE-1 downto 0);
  signal other_reply_fifo_reset   : std_logic_vector(NFE-1 downto 0);

  signal seq_to_cbci2c_com_count_reset  : std_logic;
  signal seq_to_cbci2c_com_fifo_reset   : std_logic;
  signal seq_to_cbci2c_com_fifo_we      : std_logic;
  signal seq_to_cbci2c_com_fifo_din     : std_logic_vector(31 downto 0);
  signal seq_to_cbci2c_reply_fifo_reset : std_logic;
  signal rdb_cbc_index                  : integer range 0 to MAX_N_CBC-1;
  signal seq_fsm                        : std_logic_vector(2 downto 0);
 
  signal seq_to_rdb_write_trigger       : std_logic;              
  signal cbc_par_data_to_rdb_ctrl       : std_logic_vector(6*8-1 downto 0);           
  signal rdb_we                         : std_logic;
  signal rdb_waddr                      : std_logic_vector(14 downto 0);	
  signal rdb_din                        : std_logic_vector(63 downto 0);
  signal rdb_raddr                      : std_logic_vector(15 downto 0);	
  signal rdb_write_ready                : std_logic;


  signal cbc_i2c_bus_waiting      : std_logic;
  signal cbc_i2c_bus_ready        : std_logic;
  signal cbc_i2c_bus_trans_all_done  : std_logic;
  signal reply_ready                    : std_logic_vector(NFE-1 downto 0);
  signal reply_data                     : std_logic_vector(NFE*32-1 downto 0);

--
--  -- event builder & data buffer signals
  signal ds_to_eb_header_ready       : std_logic_vector(NDS-1 downto 0); 
  signal ds_to_eb_data_packet_ready  : std_logic_vector(NDS-1 downto 0);
  signal ds_to_eb_data_packet_nword  : std_logic_vector(NDS*SECTION_DATA_SIZE_WIDTH-1 downto 0);  
  signal ds_to_eb_l1_count           : std_logic_vector(NDS*9-1 downto 0);  
  signal ds_to_eb_data_packet_valid  : std_logic_vector(NDS-1 downto 0); 
  signal ds_to_eb_data_packet_out    : std_logic_vector(NDS*32-1 downto 0); 
  signal eb_to_ds_read_en            : std_logic_vector(NDS-1 downto 0); 
  signal to_eb_header_ready       : std_logic_vector(NDS - 1 downto 0);
  signal eb_to_db_we              : std_logic;
  signal eb_to_db_data            : std_logic_vector(31 downto 0);
  signal eb_to_db_end_of_event    : std_logic;
  signal eb_test_out1              : std_logic;
  signal eb_test_out2              : std_logic;
  signal db_test_out              : std_logic;  
  signal to_db_safe_n_word_free   : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
  signal from_db_n_word_all_slow       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
  signal from_db_n_word_events_slow    : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
  signal from_db_n_word_free_slow      : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
  signal from_db_n_word_free      : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
  signal from_db_waddr            : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
  signal from_db_raddr            : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
  signal l1a_veto_from_db         : std_logic;
  
  signal update_next              : std_logic;
  signal send_slow_com            : std_logic;
  signal test1                    : std_logic;
  
  signal keithley_trig            : std_logic;
  signal keithley_cmp             : std_logic;


    signal dp_test1               : std_logic_vector(NDS-1 downto 0);
    signal dp_test2               : std_logic_vector(NDS-1 downto 0);
--
--   -- raw data buffer signals
--  signal rdb_we                   : std_logic;
--  signal rdb_waddr                : std_logic_vector(14 downto 0);
--  signal rdb_din                  : std_logic_vector(63 downto 0);
--  signal rdb_raddr                : std_logic_vector(15 downto 0);
--  signal rdb_write_trigger        : std_logic;
--  signal rdb_write_ready          : std_logic;    

    signal wt_fpp                 : std_logic;

begin


    --===========================================--
    -- board config
    --===========================================-- 
    ip_addr_o             <= x"c0_a8_00_40";
    mac_addr_o            <= x"aa_bb_cc_dd_ee_50";

  rarp_en_o             <= '0';

    use_i2c_eeprom_o      <= '0';

    --===========================================--
    -- ipbus management
    --===========================================--
    ipb_clk                   <= clk_31_250_bufg_i;     -- select the frequency of the ipbus clock from osc125_b_bufg
    ipb_clk_o             <= ipb_clk;                     -- always forward the selected ipb_clk to system core

    --===========================================--
    -- other clock inputs 
    --===========================================--
    -- fabric_clk -- on board oscillator, the same as LHC clock freq.
    fclk_ibuf:      ibufgds     generic map( DIFF_TERM => TRUE ) port map (i => fabric_clk_p, ib => fabric_clk_n, o => fabric_clk_ibufgds_o);
  fclk_bufg:      bufg        port map (i => fabric_clk_ibufgds_o, o => fabric_clk);
    --===========================================--
        
  --===================================--
  cdce_synch: entity work.cdce_synchronizer
  --===================================--
  generic map
  (    
      pwrdown_delay     => 1000,
      sync_delay        => 1000000    
  )
  port map
  (    
      reset_i           => ipb_rst_i,
      ------------------
      ipbus_ctrl_i      => not cdce_ctrl_from_ipb,          -- default: 1 (ipb controlled) 
      ipbus_sel_i       =>     cdce_sel_from_ipb,           -- default: 0 (select secondary clock)
      ipbus_pwrdown_i   => not cdce_pwrdown_from_ipb,       -- default: 1 (powered up)
      ipbus_sync_i      => not cdce_sync_from_ipb,          -- default: 1 (disable sync mode), rising edge needed to resync
      ------------------                                                                       
      user_sel_i        => '1', -- cdce_sel_from_fabric     -- effective only when ipbus_ctrl_i = '0'            
      user_pwrdown_i    => '1', -- cdce_pwrdown_from_fabric -- effective only when ipbus_ctrl_i = '0'
      user_sync_i       => '1', -- cdce_sync_from_fabric    -- effective only when ipbus_ctrl_i = '0'  
      ------------------
      pri_clk_i         => cdce_pri_clk_bufg,
      sec_clk_i         => fabric_clk,         -- copy of what is actually send to the secondary clock input
      pwrdown_o         => cdce_pwrdown_o,
      sync_o            => cdce_sync_o,
      ref_sel_o         => cdce_ref_sel_o,    
      sync_clk_o        => cdce_sync_clk_o
  );
        
    cdce_pri_clk_bufg_o <= cdce_pri_clk_bufg; cdce_pri_clk_bufg   <= '0'; -- [note: in this example, the cdce_pri_clk_bufg is not used]
  --===================================--

    --===========================================--
    stat_regs_inst: entity work.ipb_user_status_regs
    --===========================================--
    port map
    (
        clk                 => ipb_clk,
        reset               => ipb_rst_i,
        ipb_mosi_i          => ipb_mosi_i(user_ipb_stat_regs),
        ipb_miso_o          => ipb_miso_o(user_ipb_stat_regs),
        regs_i              => stat_reg
    );
    --===========================================--
    
    --===========================================--
    ctrl_regs_inst: entity work.ipb_user_control_regs
    --===========================================--
    port map
    (
        clk                 => ipb_clk,
        reset               => ipb_rst_i,
        ipb_mosi_i          => ipb_mosi_i(user_ipb_ctrl_regs),
        ipb_miso_o          => ipb_miso_o(user_ipb_ctrl_regs),
        regs_o              => ctrl_reg
    );
    --===========================================--


    --===================================--
    -- clocks for cbc_system
    --===================================--
    -- internal and external clocks are selected. 
    -- external clock has two types, defined by the firmware type at the synthesis time.
    --          input from fmc
    
 
    -- cbc system main clocks
    daq_clk <= cs_mmcm0_clk_320MHz;
    daq_slow_clk <= cs_mmcm0_clk_10MHz;
    cs_stat.mmcm0_locked <= cs_mmcm0_locked; 
  cs_mmcm0_inst : entity work.cs_mmcm0
    port map (    
        clk_in1 => fabric_clk,
        clk_out1 => cs_mmcm0_clk_40MHz,
        clk_out2 => cs_mmcm0_clk_320MHz,
        clk_out3 => cs_mmcm0_clk_10MHz,
        reset => '0',                
        locked => cs_mmcm0_locked
  );

    not_clk_200MHz_locked <= not clk_200MHz_locked;
   -- clock for idelay
    clk_40_200_pll : entity work.clk_40_200
    port map ( 
        clk_out1 => clk_200MHz,
        reset => '0',
        locked => clk_200MHz_locked,
        clk_in1 => fabric_clk 
    );
    -- clock for cbc
    cbc_mmcm0_inst : entity work.cbc_mmcm0
    port map (
        clk_out1 => cbc_clk,
        clk_out2 => cbc_clk_m8,
        reset    => '0',
        locked   => open,
        clk_in1  => cbc_mmcm_clk_in
    );

   -- internal or external cbc clock switch
   cbc_clk_select <= '0' when cs_cnfg.cbc_clock_ext = '0' else
                 '1';
   -- internal and external clock mux
--   BUFGMUX_0_inst : BUFGMUX
--   port map (
--      O  => cbc_mmcm_clk_in,   -- 1-bit output: Clock output
--      I0 => fabric_clk, -- 1-bit input: Clock input (S=0)
--      I1 => ext_cbc_clk, -- 1-bit input: Clock input (S=1)
--      S  => cbc_clk_select    -- 1-bit input: Clock select
--   );

    cbc_mmcm_clk_in <= fabric_clk;

   -- wafer test control signals
   -- WIDTH : 3600000 - 90 ms
   -- slow clock does not work for this?
   wt_fpp_inst : entity work.dff_sync_edge_detect_multi_width_pulse_out
    generic map( WIDTH => 900000 )
    port map(   reset => '0',
                clkb  => daq_slow_clk,
                dina  => cs_ctrl.wt_fpp,
                doutb => wt_fpp
        );

--
  --===================================--
   io_map : entity work.io
  --===================================--
   port map
   (
     cs_cnfg                   => cs_cnfg,
     cs_ctrl                   => cs_ctrl,
     cs_stat                   => cs_stat.io, 
     fmc_l8_la_n               => fmc_l8_la_n,
     fmc_l8_la_p               => fmc_l8_la_p,
     fmc_l12_la_n              => fmc_l12_la_n,
     fmc_l12_la_p              => fmc_l12_la_p,
     cbc_i2c_bus_mosi_array    => cbc_i2c_bus_mosi_array,
     cbc_i2c_bus_miso_array    => cbc_i2c_bus_miso_array,
     test_signal_out           => test_signal_out,
     -- external signal
     ---- cbc clock input
     ext_cbc_clk               => ext_cbc_clk,                
     ---- l1a input
     ext_async_l1a             => ext_async_l1a,
     ---- veto output
     ext_l1a_veto              => ext_l1a_veto,
     -- for cbc data in output
     cbc_fast_com_8bits        => cbc_fast_com_8bits,
     -- for cbc data out input
     clk_200MHz                => clk_200MHz,
     not_clk_200MHz_locked     => not_clk_200MHz_locked,
     cbc_clk                   => cbc_clk,
     cbc_clk_m8                => cbc_clk_m8,
     cbc_par_data_o            => cbc_par_data,
     cbc_data_clk              => cbc_data_clk,
     cbc_data_clk_m8           => cbc_data_clk_m8,
     ipb_clk                                        => ipb_clk,  
     ipb_reset                                      => ipb_rst_i, 
     data_from_cbc_iserdes_cnd_scan_bram_ipb_mosi_i => ipb_mosi_i(cs_ipb_data_from_cbc_iserdes_cnd_scan_bram_sel), 
     data_from_cbc_iserdes_cnd_scan_bram_ipb_miso_o => ipb_miso_o(cs_ipb_data_from_cbc_iserdes_cnd_scan_bram_sel),
     ipb_mosi_i_user_relay1_regs                    => ipb_mosi_i(user_relay1_regs),
     ipb_miso_o_user_relay1_regs                    => ipb_miso_o(user_relay1_regs),
     cbc_data_array_maker_test_signal1   =>  cbc_data_array_maker_test_signal1,
     cbc_data_array_maker_test_signal2   =>  cbc_data_array_maker_test_signal2,
     keithley_trig                       => keithley_trig,
     keithley_cmp                      => keithley_cmp,
     fmc_l12_clk0                        => fmc_l12_clk0,
     wt_fpp                              => wt_fpp
   );

       l1a_fifo_din <= x"0000000" & '0' & l1a_tdc;
      -- check l1a_tdc_data & l1a timing
      -- in cbc_clk, out daq_clk
      -- 32 x 512
      l1a_fifo : entity work.l1a_fifo_w40mhz_r320mhz
      PORT MAP (
                 rst    => cs_ctrl.daq_reset,
                 wr_clk => cbc_clk,
                 rd_clk => daq_clk,
                 din    => l1a_fifo_din,
                 wr_en  => cbc_fast_com.trigger,
                 rd_en  => l1a_fifo_re,
                 dout   => l1a_fifo_dout,
                 full   => open,
                 empty  => l1a_fifo_empty 
               );
                    
  --===========================================--    
  cbc_data_processor_gen :
  --===========================================--
  for i in 0 to NCBC-1 generate 
    
    cs_stat.cbc_dp_array(i).header_ready <= ds_to_eb_header_ready(i);
    cs_stat.cbc_dp_array(i).packet_ready <= ds_to_eb_data_packet_ready(i);    
    cbc_data_processor_inst : entity work.cbc_data_processor(fc7)
    port map ( 
        be_id                    => cs_cnfg.be_id,
        fe_id                    => cs_cnfg.cbc_array(i).fe_id,
        cbc_id                   => cs_cnfg.cbc_array(i).id,
        cbc_active               => cs_cnfg.cbc_array(i).active,
        daq_clk                  => daq_clk,
        cbc_data_clk             => cbc_data_clk(i),
        cbc_data_clk_m8          => cbc_data_clk_m8(i),
        cbc_par_data             => cbc_par_data((i+1)*6*8-1 downto i*6*8),
        reset_i                  => cs_ctrl.cbc_dp_array(i).reset,
        frame_counter_reset_i    => cs_ctrl.cbc_dp_array(i).frame_counter_reset,
        l1a_latency              => cs_cnfg.dp.cbc_dp_array(i).l1a_latency,
        trig_data_latency        => cs_cnfg.dp.cbc_dp_array(i).trig_data_latency,
        data_fifo_empty_o        => cs_stat.cbc_dp_array(i).data_fifo_empty,                
        data_fifo_full_o         => cs_stat.cbc_dp_array(i).data_fifo_full,        
        data_info_fifo_empty_o   => cs_stat.cbc_dp_array(i).data_info_fifo_empty,  
        data_info_fifo_full_o    => cs_stat.cbc_dp_array(i).data_info_fifo_full,   
        trig_data_fifo_empty_o   => cs_stat.cbc_dp_array(i).trig_data_fifo_empty,  
        trig_data_fifo_full_o    => cs_stat.cbc_dp_array(i).trig_data_fifo_full,   
        cbc_data_frame_counter_o => cs_stat.cbc_dp_array(i).cbc_data_frame_counter,
        l1a                      => cbc_fast_com.trigger,
        l1a_count_o              => cs_stat.cbc_dp_array(i).l1a_counter, 
        to_eb_header_ready       => ds_to_eb_header_ready(i),      
        to_eb_data_packet_ready  => ds_to_eb_data_packet_ready(i), 
        to_eb_data_packet_nword  => ds_to_eb_data_packet_nword((i+1)*SECTION_DATA_SIZE_WIDTH-1 downto i*SECTION_DATA_SIZE_WIDTH), 
        to_eb_l1_count           => ds_to_eb_l1_count((i+1)*9-1 downto i*9),          
        to_eb_data_packet_valid  => ds_to_eb_data_packet_valid(i), 
        to_eb_data_packet_out    => ds_to_eb_data_packet_out((i+1)*32-1 downto i*32),   
        from_eb_read_en          => eb_to_ds_read_en(i),
        test1                    => dp_test1(i),
        test2                    => dp_test2(i)            
    ); 
  end generate cbc_data_processor_gen;
                                                           
  ext_fast_com.fast_reset  <= '0';
  ext_fast_com.trigger     <= ext_l1a;
  ext_fast_com.tpreq       <= '0';
  ext_fast_com.orbit_reset <= '0';
--  --===========================================--    
--  -- TDC 
--  --===========================================--
--  tdc : entity work.l1a_tdc
--  Port map(   reset           => ipb_ctrl.reset,
--                          clk40           => cbc_clk,
--                          clk320          => cbc_clk_m8,
--                          l1a_nosync      => ext_async_l1a,
--                          l1a_o           => ext_l1a,
--                          l1a_tdccount_o  => ext_l1a_tdc
--  );

  --===========================================--    
  -- test signals
  --===========================================--
-- test_signal_out(0) <= fabric_clk;
   test_signal_out(0) <= cbc_clk_m8                                                   when cs_cnfg.test_out_sel0 = x"0001" else
                         cbc_clk                                                      when cs_cnfg.test_out_sel0 = x"0002" else
                         cbc_i2c_bus_mosi_array(0).scl                                when cs_cnfg.test_out_sel0 = x"0003" else
                         cbc_i2c_bus_mosi_array(0).sda                                when cs_cnfg.test_out_sel0 = x"0004" else
                         cbc_i2c_bus_miso_array(0).sda                                when cs_cnfg.test_out_sel0 = x"0005" else
                         cs_ctrl.io.cbc_hard_reset                                    when cs_cnfg.test_out_sel0 = x"0006" else
                         cbc_fast_com.fast_reset                                      when cs_cnfg.test_out_sel0 = x"0007" else
                         cbc_fast_com.trigger                                         when cs_cnfg.test_out_sel0 = x"0008" else
                         cbc_fast_com.tpreq                                           when cs_cnfg.test_out_sel0 = x"0009" else
                         cbc_fast_com.orbit_reset                                     when cs_cnfg.test_out_sel0 = x"000a" else
                         l1a_veto_from_db                                             when cs_cnfg.test_out_sel0 = x"000b" else
                         eb_test_out1                                                when cs_cnfg.test_out_sel0 = x"000c" else
                         db_test_out                                                 when cs_cnfg.test_out_sel0 = x"000d" else
                         test1                                                       when cs_cnfg.test_out_sel0 = x"000e" else
                         fabric_clk                                                  when cs_cnfg.test_out_sel0 = x"000f" else
                         dp_test1(0)                                                 when cs_cnfg.test_out_sel0 = x"0010" else
                         wt_fpp                                                      when cs_cnfg.test_out_sel0 = x"0012";
              
   test_signal_out(1) <= cbc_clk_m8                                                  when cs_cnfg.test_out_sel1 = x"0001" else
                         cbc_clk                                                     when cs_cnfg.test_out_sel1 = x"0002" else
                         cbc_i2c_bus_mosi_array(0).scl                               when cs_cnfg.test_out_sel1 = x"0003" else
                         cbc_i2c_bus_mosi_array(0).sda                               when cs_cnfg.test_out_sel1 = x"0004" else
                         cbc_i2c_bus_miso_array(0).sda                               when cs_cnfg.test_out_sel1 = x"0005" else
                         cs_ctrl.io.cbc_hard_reset                                   when cs_cnfg.test_out_sel1 = x"0006" else
                         cs_ctrl.fcm.fast_com.fast_reset                             when cs_cnfg.test_out_sel1 = x"0007" else
                         cbc_fast_com.trigger                                        when cs_cnfg.test_out_sel1 = x"0008" else
                         cbc_fast_com.tpreq                                          when cs_cnfg.test_out_sel1 = x"0009" else
                         cbc_fast_com.orbit_reset                                    when cs_cnfg.test_out_sel1 = x"000a" else
                         l1a_veto_from_db                                            when cs_cnfg.test_out_sel1 = x"000b" else
                         eb_test_out2                                                when cs_cnfg.test_out_sel1 = x"000c" else
                         db_test_out                                                 when cs_cnfg.test_out_sel1 = x"000d" else
                         seq_fast_com.trigger                                        when cs_cnfg.test_out_sel1 = x"000e" else
                         fabric_clk                                                  when cs_cnfg.test_out_sel1 = x"000f" else
                         dp_test2(0)                                                 when cs_cnfg.test_out_sel1 = x"0010" else
                         cbc_data_clk(0)                                             when cs_cnfg.test_out_sel1 = x"0011" else
                         cs_ctrl.wt_fpp                                              when cs_cnfg.test_out_sel1 = x"0012";
                         
    ipb_cbc_system_inst : entity work.ipb_cbc_system
    port map
    (
       ipb_rst_i              => ipb_rst_i,
       ipb_clk                => ipb_clk,
       ipb_mosi_i             => ipb_mosi_i,
       ipb_miso_o             => ipb_miso_o,
       cs_cnfg                => cs_cnfg,
       cs_stat                => cs_stat,
       cs_ctrl                => cs_ctrl
    );    
    cs_stat.n_active_cbc <= cs_cnfg.n_active_cbc;

    --===========================================--
  fast_command_manager : entity work.fast_command_manager
    --===========================================--
    generic map(TDC_NBITS => 3)
  port map (
    cbc_clk_m8                 => cbc_clk_m8,
    cbc_clk                    => cbc_clk,
    cs_ctrl                   => cs_ctrl.fcm,
    cs_cnfg                   => cs_cnfg.fcm,
    cs_stat                   => cs_stat.fcm,
    l1a_veto                   => int_l1a_veto,
    ext_com                    => ext_fast_com, 
    seq_com                    => seq_fast_com,
        l1a_tdc_i                  => ext_l1a_tdc, 
    l1a_count_o                => l1a_count,
    l1a_tdc                    => l1a_tdc,
    com_o                      => cbc_fast_com,
    cbc_fast_com_8bits          => cbc_fast_com_8bits
  );
  


    --===========================================--
    cbc_i2c_inst : entity work.cbc_i2c
    --===========================================--
    port map
    (
        clk_in                        => cbc_clk,
        cs_cnfg                       => cs_cnfg,
        cs_ctrl                       => cs_ctrl,
        cs_stat                       => cs_stat.cbc_i2c_bm_array,
        bus_miso_array                 => cbc_i2c_bus_miso_array,  
        bus_mosi_array                 => cbc_i2c_bus_mosi_array, 
        other_com_count_reset          => other_com_count_reset,
        other_com_fifo_reset           => other_com_fifo_reset,
        other_com_fifo_we              => other_com_fifo_we,
        other_com_fifo_din             => other_com_fifo_din,
        other_reply_fifo_reset         => other_reply_fifo_reset,
        bus_waiting                    => cbc_i2c_bus_waiting,
        bus_ready                      => cbc_i2c_bus_ready,
        bus_trans_all_done             => cbc_i2c_bus_trans_all_done,
        reply_ready                    => reply_ready,
        reply_data                     => reply_data,
        ipb_clk                        => ipb_clk, 
        ipb_reset                      => ipb_rst_i, 
        ipb_mosi_i                     => ipb_mosi_i(cs_ipb_cbc_i2c_data_buf_sel), 
        ipb_miso_o                     => ipb_miso_o(cs_ipb_cbc_i2c_data_buf_sel)
    );
 
    rdb_cbc_index <= get_cbc_index(cs_cnfg.cbc_array, cs_cnfg.rdb_cbc_id);

    

--
  ds_active_gen :
  for i in 0 to NDS-1 generate
    cs_stat.ds_active(i) <= cs_cnfg.cbc_array(i).active;
  end generate ds_active_gen;
  
  cs_stat.n_active_ds(DSID_WIDTH-1 downto CBCID_WIDTH) <= (others => '0');
  cs_stat.n_active_ds(CBCID_WIDTH-1 downto 0) <= cs_cnfg.n_active_cbc(CBCID_WIDTH-1 downto 0);

  --===========================================--
  event_builder_inst : entity work.event_builder2
  --===========================================--
  generic map(NDS => NDS, 
              SD_SIZE_W => SECTION_DATA_SIZE_WIDTH, 
              ED_SIZE_W => EVENT_DATA_SIZE_WIDTH,
              DB_ADDR_W => DATA_BUFFER_ADDR_WIDTH, 
              BEID_W => BEID_WIDTH, 
              DSID_W => DSID_WIDTH
              )    
  port map(
    reset_i                   => cs_ctrl.eb_reset,
    be_id                     => cs_cnfg.be_id,
    ds_active                 => cs_stat.ds_active(NDS-1 downto 0), 
    n_active_ds               => cs_stat.n_active_ds,
    daq_clk                   => daq_clk,
    l1a_fifo_dout             => l1a_fifo_dout,
    l1a_fifo_empty            => l1a_fifo_empty,
    l1a_fifo_re               => l1a_fifo_re,
    ds_header_ready_i         => ds_to_eb_header_ready, 
    ds_data_packet_ready_i    => ds_to_eb_data_packet_ready, 
    ds_data_packet_nword_i    => ds_to_eb_data_packet_nword, 
    ds_l1_count_i             => ds_to_eb_l1_count, 
    ds_data_packet_valid_i    => ds_to_eb_data_packet_valid, 
    ds_data_packet_out_i      => ds_to_eb_data_packet_out, 
    ds_read_en_o              => eb_to_ds_read_en, 
    n_word_free               => from_db_n_word_free, 
    we_o                      => eb_to_db_we,  
    data_o                    => eb_to_db_data,   
    end_of_event_o            => eb_to_db_end_of_event,
    write_fsm_o                  => cs_stat.eb.write_fsm,
    packet_send_fsm_o            => cs_stat.eb.packet_send_fsm,
    event_data_fifo_empty_o      => cs_stat.eb.event_data_fifo_empty,
    event_data_fifo_full_o       => cs_stat.eb.event_data_fifo_full,
    event_data_size_fifo_empty_o => cs_stat.eb.event_data_size_fifo_empty,
    event_data_size_fifo_full_o  => cs_stat.eb.event_data_size_fifo_full,
    test_out1                     => eb_test_out1,
    test_out2                     => eb_test_out2   

  );

  int_l1a_veto <= l1a_veto_from_db when cs_cnfg.trigger_master_ext = '0' else '0';
  ext_l1a_veto <= l1a_veto_from_db when cs_cnfg.trigger_master_ext = '1' else '0';    
--  cs_stat.int_l1a_veto <= int_l1a_veto;
--  cs_stat.ext_l1a_veto <= ext_l1a_veto;

  db_test_out <= eb_to_db_we;
  to_db_safe_n_word_free   <= cs_cnfg.db_safe_n_word_free(DATA_BUFFER_ADDR_WIDTH downto 0);
  cs_stat.db_n_word_all    <= std_logic_vector(to_unsigned(0, 31-DATA_BUFFER_ADDR_WIDTH)) & from_db_n_word_all_slow;  
  cs_stat.db_n_word_events <= std_logic_vector(to_unsigned(0, 31-DATA_BUFFER_ADDR_WIDTH)) & from_db_n_word_events_slow;
  cs_stat.db_n_word_free   <= std_logic_vector(to_unsigned(0, 31-DATA_BUFFER_ADDR_WIDTH)) & from_db_n_word_free_slow;
  cs_stat.db_waddr         <= std_logic_vector(to_unsigned(0, 32-DATA_BUFFER_ADDR_WIDTH)) & from_db_waddr;
  cs_stat.db_raddr         <= std_logic_vector(to_unsigned(0, 32-DATA_BUFFER_ADDR_WIDTH)) & from_db_raddr;
  --===========================================--
  data_buffer_inst : entity work.data_buffer(fc7_ring_buffer)
  --===========================================--
  generic map( DB_ADDR_W => DATA_BUFFER_ADDR_WIDTH)
  port map
  (
    daq_clk            => daq_clk,
    clk_slow           => daq_slow_clk,
    safe_n_word_free_i => to_db_safe_n_word_free,
    reset_i                      => cs_ctrl.db_reset,
    readall_i                    => cs_ctrl.db_readall,
    we_i               => eb_to_db_we,
    data_i             => eb_to_db_data,
    end_of_event_i     => eb_to_db_end_of_event,
    n_word_all_slow          => from_db_n_word_all_slow, 
    n_word_events_slow       => from_db_n_word_events_slow, 
    n_word_free_slow         => from_db_n_word_free_slow,
    n_word_free_o            => from_db_n_word_free, 
    werr                             => cs_stat.db_werr, 
    rerr                             => cs_stat.db_rerr, 
    waddr                            => from_db_waddr, 
    raddr_o                      => from_db_raddr, 
    l1a_veto           => l1a_veto_from_db,
    ipb_clk            => ipb_clk, 
    ipb_reset          => ipb_rst_i, 
    ipb_mosi_i         => ipb_mosi_i(cs_ipb_l1_data_buf_sel), 
    ipb_miso_o         => ipb_miso_o(cs_ipb_l1_data_buf_sel),
    update_next_o      => update_next
  );

  -- for ipbus
  process ( daq_slow_clk )
--    variable count : integer range 0 to 1400;
  begin
      if rising_edge( daq_slow_clk ) then
        cs_stat.seq_fsm     <= seq_fsm;
        end if;
    end process;
    
  -- sequencer
--  --===========================================--    
--  cbc3_sequencer_inst : entity work.cbc3_sequencer
--  --===========================================--    
--  port map(		
--                  cbc_clk_m8              => cbc_clk_m8,
--                  cbc_clk                 => cbc_clk,
--                  cbci2c_base_clk         => cbc_clk,
--                  cbc_cnfg_array_i        => cs_cnfg.cbc_array,
--                  fsm_o                   => seq_fsm,
--                  reset_i                 => cs_ctrl.seq_reset,
--                  start_i                 => cs_ctrl.seq_start,	
--                  resume_i                => cs_ctrl.seq_resume,
--                  cbci2c_trans_all_done_i => cbc_i2c_bus_trans_all_done,
--                  cbci2c_com_count_reset  => other_com_count_reset,
--                  cbci2c_com_fifo_reset   => other_com_fifo_reset,
--                  cbci2c_com_fifo_we      => other_com_fifo_we,
--                  cbci2c_com_fifo_din     => other_com_fifo_din,
--                  cbci2c_reply_fifo_reset => other_reply_fifo_reset,
--                  fc_fast_reset           => seq_fast_com.fast_reset,
--                  fc_trigger              => seq_fast_com.trigger,
--                  fc_tpreq                => seq_fast_com.tpreq,
--                  fc_orbit_reset          => seq_fast_com.orbit_reset,
--                  rdb_write_trigger_o     => seq_to_rdb_write_trigger,
--                  rdb_write_ready         => rdb_write_ready,
--                  ipb_clk                 => ipb_clk,
--                  ipb_reset               => ipb_rst_i,
--                  ipb_mosi_i              => ipb_mosi_i(cs_ipb_ctrl_seq_data_buf_sel),
--                  ipb_miso_o              => ipb_miso_o(cs_ipb_ctrl_seq_data_buf_sel),
--                  send_slow_com_o         => send_slow_com,
--                  test1                   => test1,
--                  seq_bram_addra          => cs_stat.seq_bram_addra,
--                  keithley_trig_o         => keithley_trig,
--                  keithley_cmp            => keithley_cmp,
--                  keithley_cmp_timer_o    => cs_stat.seq_keithley_cmp_timer
--          );		

--  cbc_par_data_to_rdb_ctrl <= cbc_par_data((rdb_cbc_index+1) * 6 * 8 - 1 downto rdb_cbc_index * 6 * 8);

--  rdb_ctrl_inst : entity work.rdb_controller
--  Port map( 
--                  reset_i		    => cs_ctrl.rdb_reset,
--                  cbc_clk			=> cbc_clk,
--                  rd_clk			=> ipb_clk,	
--                  trigger			=> seq_to_rdb_write_trigger,            
--                  latency			=> cs_cnfg.rdb_latency,
--                  write_block_size  => cs_cnfg.rdb_write_block_size,
--                  cbc_par_data      => cbc_par_data_to_rdb_ctrl,	
--                  write_ready		=> rdb_write_ready,
--                  read_ready_o      => cs_stat.rdb_read_ready,
--                  waddr_o           => cs_stat.rdb_waddr,
--                  ipb_clk           => ipb_clk,
--                  ipb_reset         => ipb_rst_i,
--                  ipb_mosi_i        => ipb_mosi_i(cs_ipb_raw_data_buf_sel),
--                  ipb_miso_o        => ipb_miso_o(cs_ipb_raw_data_buf_sel)                  
--          );


end usr;
