########################################################
set_property IOSTANDARD LVDS_25                [get_ports fabric_clk_p]
set_property PACKAGE_PIN AK18                  [get_ports fabric_clk_p]
create_clock -period 24.950 -name fabric_clk   [get_ports fabric_clk_p]

##############                                              
# external clock 40MHz                                      
#set_property PACKAGE_PIN R28 [get_ports {fmc_l12_la_p[0]}]  
#set_property PACKAGE_PIN R29 [get_ports {fmc_l12_la_n[0]}]  
#create_clock -period 25.000 -name ext_cbc_clk [get_ports {fmc_l12_la_p[0]}]               
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets usr/io_map/I1]                         

########################################################
set_clock_groups -asynchronous \
    -group [get_clocks -include_generated_clocks eth_txoutclk] \
    -group [get_clocks -include_generated_clocks osc125_a] \
    -group [get_clocks -include_generated_clocks osc125_b] \
    -group [get_clocks -include_generated_clocks fabric_clk]
#    -group [get_clocks -include_generated_clocks ext_cbc_clk]
########################################################
set_property BITSTREAM.CONFIG.OVERTEMPPOWERDOWN ENABLE [current_design]
########################################################
set_operating_conditions -airflow 0
set_operating_conditions -heatsink low
########################################################