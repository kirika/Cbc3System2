--===========================--
-- io 
-- Temporary IOs.  To be updated.
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;
use work.cbc_system_package.all;
use work.cbc_system_config_package.all;

entity io is
port ( 
	cs_cnfg                                        : in	   cs_cnfg_type; 
	cs_ctrl                                        : in	   cs_ctrl_type; 
	cs_stat                                        : out   cs_io_stat_type; 
    fmc_l8_la_p						               : inout std_logic_vector(33 downto 0);
    fmc_l8_la_n                                    : inout std_logic_vector(33 downto 0);
    fmc_l12_la_p                                   : inout std_logic_vector(33 downto 0);
    fmc_l12_la_n                                   : inout std_logic_vector(33 downto 0);
    cbc_i2c_bus_mosi_array                         : in    cbc_i2c_bus_mosi_array_type;
    cbc_i2c_bus_miso_array                         : out   cbc_i2c_bus_miso_array_type;
    test_signal_out                                : in    std_logic_vector(1 downto 0);
    ext_cbc_clk                                    : out   std_logic;
    ext_async_l1a                                  : out   std_logic;
    ext_l1a_veto                                   : in    std_logic;
    cbc_fast_com_8bits                             : in    std_logic_vector(7 downto 0);
    clk_200MHz                                     : in    std_logic;
    not_clk_200MHz_locked                          : in    std_logic;
    cbc_clk                 		               : in    std_logic;
    cbc_clk_m8			                           : in    std_logic;
    cbc_par_data_o                                 : out   std_logic_vector(NCBC*6*8-1 downto 0);
    cbc_data_clk                                   : out   std_logic_vector(NCBC-1 downto 0);
    cbc_data_clk_m8                                : out   std_logic_vector(NCBC-1 downto 0);
	ipb_clk                                        : in    std_logic;
	ipb_reset                                      : in    std_logic;
	data_from_cbc_iserdes_cnd_scan_bram_ipb_mosi_i : in    ipb_wbus;
	data_from_cbc_iserdes_cnd_scan_bram_ipb_miso_o : out   ipb_rbus;
	ipb_mosi_i_user_relay1_regs                    : in    ipb_wbus;
	ipb_miso_o_user_relay1_regs                    : out   ipb_rbus;
	cbc_data_array_maker_test_signal1              : out   std_logic;
	cbc_data_array_maker_test_signal2              : out   std_logic;
	keithley_trig                                  : in    std_logic;
	keithley_cmp                                   : out   std_logic;
	fmc_l12_clk0                                   : in    std_logic;
	wt_fpp                                         : in    std_logic
	);
end io;

architecture Behavioral of io is
    
   attribute IODELAY_GROUP1 : string;
   attribute IODELAY_GROUP1 of IDELAYCTRL_inst1 : label is "cbc_data_iserdes_group1";

   attribute IODELAY_GROUP2 : string;
   attribute IODELAY_GROUP2 of IDELAYCTRL_inst2 : label is "cbc_data_iserdes_group2";

    signal delay_locked1 : std_logic;
    signal delay_locked2 : std_logic;

        constant CBC_INDEX_0 : integer := 0;
        constant CBC_INDEX_1 : integer := 1;
        constant CBC_INDEX_2 : integer := 2;
        constant CBC_INDEX_3 : integer := 3;
        constant FE_INDEX_0 : integer := 0;
        constant FE_INDEX_1 : integer := 1;
        constant FE_INDEX_2 : integer := 2;
        constant FE_INDEX_3 : integer := 3;
                
        signal data_in_from_pins_p          : std_logic_vector(NCBC*6-1 downto 0); 
        signal data_in_from_pins_n          : std_logic_vector(NCBC*6-1 downto 0); 

        -- fast signal iserdes out      
        signal data_out_to_pins_p           : std_logic_vector(0 to NFE-1);
        signal data_out_to_pins_n           : std_logic_vector(0 to NFE-1);    
        signal clk_to_pins_p                : std_logic_vector(0 to NFE-1);
        signal clk_to_pins_n                : std_logic_vector(0 to NFE-1);    

        signal ext_async_l1a_int            : std_logic;
        signal fmc_l12_la_0                 : std_logic;

        signal fc_to_cbc_oserdes_reset      : std_logic_vector(NFE-1 downto 0);

        signal cbc_hard_reset_1us           : std_logic;
    
        signal fmc_l12_la_29                : std_logic;
        signal fmc_l12_clk0_bufo            : std_logic;

        signal relay1_nim_out                      : std_logic;
        signal relay1_nim_in                       : std_logic;
        signal relay1_nim_out_n                    : std_logic;
        signal relay1_nim_in_n                     : std_logic;        
        signal relay1_ttl_in                       : std_logic;
        signal relay1_gpio_out                     : std_logic_vector(7 downto 0);
        signal relay1_relay_out                    : std_logic_vector(5 downto 0);
        --signal relay1_test_relay_out               : std_logic_vector(5 downto 0);

        signal sda_to_cbc : std_logic_vector(NCBC-1 downto 0);
        signal scl_to_cbc : std_logic_vector(NCBC-1 downto 0);
        signal sda_from_cbc : std_logic_vector(NCBC-1 downto 0);
        signal reset_to_cbc : std_logic;
begin

        --===================--  
        -- data in
        --===================--  
        data_in_from_pins_p(CBC_INDEX_0*6+0) <= fmc_l8_la_p(27);
        data_in_from_pins_p(CBC_INDEX_0*6+1) <= fmc_l8_la_p(23);
        data_in_from_pins_p(CBC_INDEX_0*6+2) <= fmc_l8_la_p(18);
        data_in_from_pins_p(CBC_INDEX_0*6+3) <= fmc_l8_la_p(17);
        data_in_from_pins_p(CBC_INDEX_0*6+4) <= fmc_l8_la_p(14);
        data_in_from_pins_p(CBC_INDEX_0*6+5) <= fmc_l8_la_p(13);
        data_in_from_pins_n(CBC_INDEX_0*6+0) <= fmc_l8_la_n(27);
        data_in_from_pins_n(CBC_INDEX_0*6+1) <= fmc_l8_la_n(23);
        data_in_from_pins_n(CBC_INDEX_0*6+2) <= fmc_l8_la_n(18);
        data_in_from_pins_n(CBC_INDEX_0*6+3) <= fmc_l8_la_n(17);
        data_in_from_pins_n(CBC_INDEX_0*6+4) <= fmc_l8_la_n(14);
        data_in_from_pins_n(CBC_INDEX_0*6+5) <= fmc_l8_la_n(13);      

        data_in_from_pins_p(CBC_INDEX_1*6+0) <= fmc_l8_la_p(20);
        data_in_from_pins_p(CBC_INDEX_1*6+1) <= fmc_l8_la_p(15);
        data_in_from_pins_p(CBC_INDEX_1*6+2) <= fmc_l8_la_p(16);
        data_in_from_pins_p(CBC_INDEX_1*6+3) <= fmc_l8_la_p(11);
        data_in_from_pins_p(CBC_INDEX_1*6+4) <= fmc_l8_la_p(12);
        data_in_from_pins_p(CBC_INDEX_1*6+5) <= fmc_l8_la_p(07);
        data_in_from_pins_n(CBC_INDEX_1*6+0) <= fmc_l8_la_n(20);
        data_in_from_pins_n(CBC_INDEX_1*6+1) <= fmc_l8_la_n(15);
        data_in_from_pins_n(CBC_INDEX_1*6+2) <= fmc_l8_la_n(16);
        data_in_from_pins_n(CBC_INDEX_1*6+3) <= fmc_l8_la_n(11);
        data_in_from_pins_n(CBC_INDEX_1*6+4) <= fmc_l8_la_n(12);
        data_in_from_pins_n(CBC_INDEX_1*6+5) <= fmc_l8_la_n(07);      

        data_in_from_pins_p(CBC_INDEX_2*6+0) <= fmc_l12_la_p(27);
        data_in_from_pins_p(CBC_INDEX_2*6+1) <= fmc_l12_la_p(23);
        data_in_from_pins_p(CBC_INDEX_2*6+2) <= fmc_l12_la_p(18);
        data_in_from_pins_p(CBC_INDEX_2*6+3) <= fmc_l12_la_p(17);
        data_in_from_pins_p(CBC_INDEX_2*6+4) <= fmc_l12_la_p(14);
        data_in_from_pins_p(CBC_INDEX_2*6+5) <= fmc_l12_la_p(13);
        data_in_from_pins_n(CBC_INDEX_2*6+0) <= fmc_l12_la_n(27);
        data_in_from_pins_n(CBC_INDEX_2*6+1) <= fmc_l12_la_n(23);
        data_in_from_pins_n(CBC_INDEX_2*6+2) <= fmc_l12_la_n(18);
        data_in_from_pins_n(CBC_INDEX_2*6+3) <= fmc_l12_la_n(17);
        data_in_from_pins_n(CBC_INDEX_2*6+4) <= fmc_l12_la_n(14);
        data_in_from_pins_n(CBC_INDEX_2*6+5) <= fmc_l12_la_n(13);  

        data_in_from_pins_p(CBC_INDEX_3*6+0) <= fmc_l12_la_p(20);
        data_in_from_pins_p(CBC_INDEX_3*6+1) <= fmc_l12_la_p(15);
        data_in_from_pins_p(CBC_INDEX_3*6+2) <= fmc_l12_la_p(16);
        data_in_from_pins_p(CBC_INDEX_3*6+3) <= fmc_l12_la_p(11);
        data_in_from_pins_p(CBC_INDEX_3*6+4) <= fmc_l12_la_p(12);
        data_in_from_pins_p(CBC_INDEX_3*6+5) <= fmc_l12_la_p(07);
        data_in_from_pins_n(CBC_INDEX_3*6+0) <= fmc_l12_la_n(20);
        data_in_from_pins_n(CBC_INDEX_3*6+1) <= fmc_l12_la_n(15);
        data_in_from_pins_n(CBC_INDEX_3*6+2) <= fmc_l12_la_n(16);
        data_in_from_pins_n(CBC_INDEX_3*6+3) <= fmc_l12_la_n(11);
        data_in_from_pins_n(CBC_INDEX_3*6+4) <= fmc_l12_la_n(12);
        data_in_from_pins_n(CBC_INDEX_3*6+5) <= fmc_l12_la_n(07);    
 
        cbc_data_array_maker_inst : entity work.cbc_data_array_maker
        port map(
                        data_in_from_pins_p               => data_in_from_pins_p,
                        data_in_from_pins_n               => data_in_from_pins_n,
                        cbc_par_data_o                    => cbc_par_data_o,
                        cbc_data_clk                      => cbc_data_clk,
                        cbc_data_clk_m8                   => cbc_data_clk_m8,
                        ref_clock			              => clk_200MHz,	
                        not_ref_clock_locked              => not_clk_200MHz_locked,
                        cbc_clk_in	                      => cbc_clk,
                        cs_cnfg                           => cs_cnfg,
                        cs_ctrl                           => cs_ctrl,
                        cs_stat                           => cs_stat,
                        ipb_clk                           => ipb_clk,
                        ipb_reset                         => ipb_reset,
                        iserdes_cnd_scan_bram_ipb_mosi_i  => data_from_cbc_iserdes_cnd_scan_bram_ipb_mosi_i,
                        iserdes_cnd_scan_bram_ipb_miso_o  => data_from_cbc_iserdes_cnd_scan_bram_ipb_miso_o,
                        test_signal1                      => cbc_data_array_maker_test_signal1,
                        test_signal2                      => cbc_data_array_maker_test_signal2
                );
 
  IDELAYCTRL_inst1 : IDELAYCTRL
                  port map (
                     RDY => delay_locked1,       -- 1-bit output: Ready output
                     REFCLK => clk_200MHz, -- 1-bit input: Reference clock input
                     RST => not_clk_200MHz_locked        -- 1-bit input: Active high reset input
                  );

  IDELAYCTRL_inst2 : IDELAYCTRL
                  port map (
                     RDY => delay_locked2,       -- 1-bit output: Ready output
                     REFCLK => clk_200MHz, -- 1-bit input: Reference clock input
                     RST => not_clk_200MHz_locked        -- 1-bit input: Active high reset input
                  );


  
        --===================--  
        -- fast signals
        --===================--   
        fmc_l8_la_p(26) <= data_out_to_pins_p(FE_INDEX_0);
        fmc_l8_la_n(26) <= data_out_to_pins_n(FE_INDEX_0);
        fmc_l8_la_p(21) <= clk_to_pins_p(FE_INDEX_0);
        fmc_l8_la_n(21) <= clk_to_pins_n(FE_INDEX_0);    

        fmc_l8_la_p(19) <= data_out_to_pins_p(FE_INDEX_1);
        fmc_l8_la_n(19) <= data_out_to_pins_n(FE_INDEX_1);
        fmc_l8_la_p(22) <= clk_to_pins_p(FE_INDEX_1);
        fmc_l8_la_n(22) <= clk_to_pins_n(FE_INDEX_1);    

        fmc_l12_la_p(26) <= data_out_to_pins_p(FE_INDEX_2);
        fmc_l12_la_n(26) <= data_out_to_pins_n(FE_INDEX_2);
        fmc_l12_la_p(21) <= clk_to_pins_p(FE_INDEX_2);
        fmc_l12_la_n(21) <= clk_to_pins_n(FE_INDEX_2);    

        fmc_l12_la_p(19) <= data_out_to_pins_p(FE_INDEX_3);
        fmc_l12_la_n(19) <= data_out_to_pins_n(FE_INDEX_3);
        fmc_l12_la_p(22) <= clk_to_pins_p(FE_INDEX_3);
        fmc_l12_la_n(22) <= clk_to_pins_n(FE_INDEX_3);    

        fast_command_oserdes_gen:
        for i in 0 to NFE-1 generate
                -- oserdes reset should be deasserted synchronized with clk_div
                io_fc_oserdes_reset_gen : entity work.dff_sync_edge_detect
                port map( reset => '0', 
                          clkb => cbc_clk, 
                          dina => cs_ctrl.io.fc_to_cbc_oserdes_reset(i), 
                          doutb => fc_to_cbc_oserdes_reset(i) 
                  );   


                --===================--       
                fast_command_oserdes_cbc0_inst : entity work.fast_command_oserdes
                --===================--   
                port map 
                ( 
                        data_out_from_device =>  bitswap(cbc_fast_com_8bits),
                        data_out_to_pins_p(0) => data_out_to_pins_p(i),
                        data_out_to_pins_n(0) => data_out_to_pins_n(i),
                        clk_to_pins_p         => clk_to_pins_p(i),
                        clk_to_pins_n         => clk_to_pins_n(i),
                        clk_in                => cbc_clk_m8,                            
                        clk_div_in            => cbc_clk,                        
                        clk_reset             => fc_to_cbc_oserdes_reset(i),        
                        io_reset              => fc_to_cbc_oserdes_reset(i)
                );
        end generate fast_command_oserdes_gen;

--  6, 8 SDA from CBC
--  5, 4 RESET
--  10, 3 SDA to CBC
--  9, 2  SCLK  

        --===================--   
        -- cbc hard reset 1us pulse
        --===================--      
        fmcl8_la_obuf_hard_reset_FE_INDEX0 : obufds generic map(IOSTANDARD => "LVDS_25") port map (i => reset_to_cbc, o => fmc_l8_la_p(5), ob => fmc_l8_la_n(5));
        fmcl8_la_obuf_hard_reset_FE_INDEX1 : obufds generic map(IOSTANDARD => "LVDS_25") port map (i => reset_to_cbc, o => fmc_l8_la_p(4), ob => fmc_l8_la_n(4));
        fmcl12_la_obuf_hard_reset_FE_INDEX2 : obufds generic map(IOSTANDARD => "LVDS_25") port map (i => reset_to_cbc, o => fmc_l12_la_p(10), ob => fmc_l12_la_n(10));
        fmcl12_la_obuf_hard_reset_FE_INDEX3 : obufds generic map(IOSTANDARD => "LVDS_25") port map (i => reset_to_cbc, o => fmc_l12_la_p(4), ob => fmc_l12_la_n(4));
        hard_reset_gen : entity work.dff_sync_edge_detect_multi_width_pulse_out
        generic map( WIDTH => 40 )
        port map( reset => '0', 
                  clkb => cbc_clk, 
                  dina => cs_ctrl.io.cbc_hard_reset, 
                  doutb => cbc_hard_reset_1us 
          );
        reset_to_cbc <= not cbc_hard_reset_1us;

 
        --===================--                       
        -- i2c
        --===================--  
        fmcl8_la_obuf_cbc_out_sda_set_FE_INDEX0          : ibufds generic map(IOSTANDARD => "LVDS_25", DIFF_TERM => TRUE) port map (i=>fmc_l8_la_p(6), ib=>fmc_l8_la_n(6), o=> sda_from_cbc(FE_INDEX_0) ); 	
        fmcl8_la_ibuf_cbc_in_sda_set_FE_INDEX0           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>sda_to_cbc(FE_INDEX_0), o=>fmc_l8_la_p(10), ob=>fmc_l8_la_n(10) );
        fmcl8_la_ibuf_cbc_in_scl_set_FE_INDEX0           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>scl_to_cbc(FE_INDEX_0), o=>fmc_l8_la_p(9), ob=>fmc_l8_la_n(9) );

        fmcl8_la_obuf_cbc_out_sda_set_FE_INDEX1          : ibufds generic map(IOSTANDARD => "LVDS_25", DIFF_TERM => TRUE) port map (i=>fmc_l8_la_p(8), ib=>fmc_l8_la_n(8), o=> sda_from_cbc(FE_INDEX_1) ); 	
        fmcl8_la_ibuf_cbc_in_sda_set_FE_INDEX1           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>sda_to_cbc(FE_INDEX_1), o=>fmc_l8_la_p(3), ob=>fmc_l8_la_n(3) );
        fmcl8_la_ibuf_cbc_in_scl_set_FE_INDEX1           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>scl_to_cbc(FE_INDEX_1), o=>fmc_l8_la_p(2), ob=>fmc_l8_la_n(2) );


        fmcl12_la_obuf_cbc_out_sda_set_FE_INDEX2          : ibufds generic map(IOSTANDARD => "LVDS_25", DIFF_TERM => TRUE) port map (i=>fmc_l12_la_p(9), ib=>fmc_l12_la_n(9), o=> sda_from_cbc(FE_INDEX_2) ); 	
        fmcl12_la_ibuf_cbc_in_sda_set_FE_INDEX2           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>sda_to_cbc(FE_INDEX_2), o=>fmc_l12_la_p(5), ob=>fmc_l12_la_n(5) );
        fmcl12_la_ibuf_cbc_in_scl_set_FE_INDEX2          : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>scl_to_cbc(FE_INDEX_2), o=>fmc_l12_la_p(6), ob=>fmc_l12_la_n(6) );


        fmcl12_la_obuf_cbc_out_sda_set_FE_INDEX3          : ibufds generic map(IOSTANDARD => "LVDS_25", DIFF_TERM => TRUE) port map (i=>fmc_l12_la_p(8), ib=>fmc_l12_la_n(8), o=> sda_from_cbc(FE_INDEX_3) ); 	
        fmcl12_la_ibuf_cbc_in_sda_set_FE_INDEX3           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>sda_to_cbc(FE_INDEX_3), o=>fmc_l12_la_p(3), ob=>fmc_l12_la_n(3) );
        fmcl12_la_ibuf_cbc_in_scl_set_FE_INDEX3           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>scl_to_cbc(FE_INDEX_3), o=>fmc_l12_la_p(2), ob=>fmc_l12_la_n(2) );


        i2c_sigs_gen :
        for i in 0 to NCBC-1 generate 
            sda_to_cbc(i) <= not cbc_i2c_bus_mosi_array(i).sda;
            scl_to_cbc(i) <= not cbc_i2c_bus_mosi_array(i).scl;
            cbc_i2c_bus_miso_array(i).sda <= not sda_from_cbc(i);
        end generate;
                    
--        fpp_out : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>wt_fpp, o=>fmc_l8_la_p(7), ob=>fmc_l8_la_n(7) );


      test_signal_0_fmcl8           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(0), o=>fmc_l8_la_p(33), ob=>fmc_l8_la_n(33) );
      test_signal_1_fmcl8           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(1), o=>fmc_l8_la_p(31), ob=>fmc_l8_la_n(31) );

      test_signal_0_fmcl12           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(0), o=>fmc_l12_la_p(33), ob=>fmc_l12_la_n(33) );
      test_signal_1_fmcl12           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(1), o=>fmc_l12_la_p(31), ob=>fmc_l12_la_n(31) );




--        --===================--   
--        -- relay1 
--        --===================--    
--        -- external l1a
--        nim_in_d  : ibufds generic map( DIFF_TERM => TRUE ) port map (i => fmc_l12_la_p(22), ib => fmc_l12_la_n(22), o => relay1_nim_in_n  );
--        relay1_nim_in<=not relay1_nim_in_n;
--        ext_async_l1a <= relay1_nim_in;
--        -- veto out
--        relay1_nim_out_d  : obufds port map ( i=>relay1_nim_out_n, o=>fmc_l12_la_p(19), ob=>fmc_l12_la_n(19) );
--        relay1_nim_out_n<=not relay1_nim_out;
--        relay1_nim_out <= ext_l1a_veto;
        
--        -- for keithley multimeter trigger
--        relay1_ttl1_buf   : obuf port map( i=> keithley_trig, o=> fmc_l12_la_p(15)); 
--        -- for keithley multimeter complete
--        relay1_ttl_in_buf : ibuf port map( i=> fmc_l12_la_p(20), o=> relay1_ttl_in);
--        keithley_cmp <= relay1_ttl_in;   
        
--        -- test signal out
--        test_signal_0           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(0), o=>fmc_l12_la_p(4), ob=>fmc_l12_la_n(4) );
--        test_signal_1           : obufds generic map(IOSTANDARD => "LVDS_25") port map ( i=>test_signal_out(1), o=>fmc_l12_la_p(7), ob=>fmc_l12_la_n(7) );


--            relay1_relay_out0_buf : obuf port map( i => relay1_relay_out(0), o => fmc_l12_la_p(11) );
--            relay1_relay_out1_buf : obuf port map( i => relay1_relay_out(1), o => fmc_l12_la_n(11) );
--            relay1_relay_out2_buf : obuf port map( i => relay1_relay_out(2), o => fmc_l12_la_p(16) );
--            relay1_relay_out3_buf : obuf port map( i => relay1_relay_out(3), o => fmc_l12_la_n(16) );
--            relay1_relay_out4_buf : obuf port map( i => relay1_relay_out(4), o => fmc_l12_la_p(12) );
--            relay1_relay_out5_buf : obuf port map( i => relay1_relay_out(5), o => fmc_l12_la_n(12) );                                              
            
--            relay1_ttl2_buf         : obuf port map( i=> relay1_gpio_out(1), o=> fmc_l12_la_n(15));
--            relay1_i2c_power_en_buf : obuf port map( i=> relay1_gpio_out(6), o=> fmc_l12_la_n(33)); 
                   
--            relay1_inst :  entity  work.relay_driver 
--            PORT MAP (
            
--                    clk                   => ipb_clk,
--                    reset                 => ipb_reset,
--                    ipb_mosi_i            => ipb_mosi_i_user_relay1_regs,
--                    ipb_miso_o            => ipb_miso_o_user_relay1_regs,     
--                    relay_out             => relay1_relay_out,
--                    gpio_out               => relay1_gpio_out,               
--                    gpio_in(0)             => relay1_ttl_in, -- ttl in
--                    gpio_in(1)             => relay1_nim_in,
--                    gpio_in(2)             => '0',
--                    gpio_in(3)             => '0',
--                    gpio_in(4)             => '0',
--                    gpio_in(5)             => '0',
--                    gpio_in(6)             => '0',
--                    gpio_in(7)             => '0'
                    
--            );

--      -- external clock					
--        fmc_l12_la_0_buf : ibufds generic map(IOSTANDARD => "LVDS_25", DIFF_TERM => TRUE) port map (i => fmc_l12_la_p(0), ib => fmc_l12_la_n(0), o => fmc_l12_la_0 ); 
	
--	    ext_cbc_clk_BUFG_inst : BUFG
--        port map (
--                         O => ext_cbc_clk, -- 1-bit output: Clock output
--                         I => fmc_l12_la_0 -- 1-bit  input: Clock input
--                 );	

	    
end Behavioral;
