--===========================--
-- wt_package
-- 19.02.2018 Kirika Uchida
--===========================--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

package cbc_system_config_package is

    --=== version ========--
    constant cbc_system_ver_major:integer range 0 to 15 :=5;
    constant cbc_system_ver_minor:integer range 0 to 15 :=0;
    constant cbc_system_ver_build:integer range 0 to 255:=0;
    constant cbc_system_ver_year :integer range 0 to 99 :=19;
    constant cbc_system_ver_month:integer range 0 to 12 :=01;
    constant cbc_system_ver_day  :integer range 0 to 31 :=30;

  
	constant usr_id_0		 	:std_logic_vector(31 downto 0):= x"43_42_43_33";	-- 'C' 'B' 'C' '3'
	constant firmware_id	 	:std_logic_vector(31 downto 0):= std_logic_vector(to_unsigned(cbc_system_ver_major,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_minor,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_build,8)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_year ,7)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_month,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_day  ,5)) ;

		-- see constant definitions in cbc_system/common/src/packages/cbc_system_constants_package.vhd
    --=======================================--
    constant firmware_type                 : std_logic_vector(2 downto 0) := "001";
    constant NDS                           : integer := 4; -- 1 to MAX_N_DATA_SECTION 
    constant NCBC                          : integer := 4; -- 1 to MAX_N_CBC 
    constant NFE                           : integer := 4; -- 1 to MAX_N_FE 
    constant CBC_I2C_REPLY_FIFO_ADDR_WIDTH : integer := 16;
    constant SECTION_DATA_SIZE_WIDTH       : integer := 8;
    constant EVENT_DATA_SIZE_WIDTH         : integer := 12;
    constant DATA_BUFFER_ADDR_WIDTH        : integer := 17;    
	constant DSID                          : integer := DSID_CBC_DATA_RAW;

    type fe_index_type is array (0 to MAX_N_CBC-1) of integer range 0 to MAX_N_FE-1;
    constant cbc_to_fe_index            : fe_index_type := ( 0, 1, 2, 3, 0, 0, 0, 0,
                                                             0, 0, 0, 0, 0, 0, 0, 0);

	constant CBC_I2C_PRESCALE		            : integer := 40; -- 1MHz with 40MHz clock
--	constant CBC_I2C_PRESCALE		            : integer := 80; -- 0.5MHz
--	constant CBC_I2C_PRESCALE		            : integer := 400; -- 100kHz


end cbc_system_config_package;

package body cbc_system_config_package is

end cbc_system_config_package;

