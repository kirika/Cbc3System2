#!/bin/bash

function usage {

echo "$0 -s -0 [wafer test directory] [chiplist]"

}

if [ $# -lt 1 ]; then
    usage;
    exit 0; 
fi

short=0
ver=1
while getopts “s0” opt; do
    case $opt in
	s) short=1
	    ;;
	0) ver=0
	    ;;
    esac
done
ncol=36
nrow=17

if [ $ver -eq 0 ]; then
    ncol=15
    nrow=15
fi

shift $(($OPTIND - 1))
cd $1
odir="./"
if [ "$2" == "" ]; then
    clist=`ls -tr1 | grep '^[0-9]\+_[0-9]\+$'` 
else
    clist=`cat $2 | grep -v '^#'`
    odir=${2%%.txt}
    odir=${odir##*/}
    if [ ! -d ${odir} ]; then
	mkdir "$odir"
    fi
fi
echo $odir
### outputs
RESULTS_O=results.txt
BG_O=bg.txt
VDDA_RD_O=vdda_rd.txt
RESULTS_STAT_O=results_stat.txt
RESULTS_TEX=results.tex

rm -f ${odir}/${RESULTS_O} ${odir}/${BG_O} ${odir}/${VDDA_RD_O} 
touch ${odir}/${RESULTS_O} ${odir}/${BG_O} ${odir}/${VDDA_RD_O} 

for chip in $clist 
do 
    f=${chip}/wt_prog.log
    #    grep -H 'WAFER TEST RESULT' ${f} >> tmp.txt; 
    line=`sed -n 's/.*WAFER TEST RESULT \(.*\)$/\1/p' ${f}`
    if [ "$line" == "" ]; then
	line='0x80000000';
    fi
    printf "%s %s\n" "$chip" "$line"  >> ${odir}/${RESULTS_O} 

    awk '/frame14:pre-burn_bg /{bg=$2} /frame14:pre-burn_vdda /{vdda=$2}END{if(bg!=0){d=vdda-2*bg;printf("%s %.3f %.3f %.3f\n", FILENAME, bg, vdda, d)}}' $f | sed -n 's:^\([0-9]\+_[0-9]\+\)/wt_prog.log\(.*\):\1\2:p' >> ${odir}/${BG_O} 
    awk '/frame14:pre-burn_bg /{bg=$2} /frame14:pre-burn_vdda /{vdda=$2}END{if(bg!=0){t_vdda=2*bg;d=vdda-t_vdda;rd=d/t_vdda; printf("%s %.1f\n", FILENAME, rd*100)}}' $f | sed -n 's:^\([0-9]\+_[0-9]\+\)/wt_prog.log\(.*\):\1\2:p' >> ${odir}/${VDDA_RD_O} 
done 

#sed -n 's:^\([0-9]\+_[0-9]\+\)/wt_prog.log.* RESULT \(.*\):\1 \2:p' tmp.txt | sort -n > results.txt

#echo 'CBC3HAL_ROOT = ' ${CBC3HAL_ROOT}

if [ ${odir} != "." ]; then
    cd ${odir}
fi

${CBC3HAL_ROOT}/python/wt_bg_stat_plot.py ${BG_O} 
root -q -b '${CBC3HAL_ROOT}/root_scripts/analyze_results.C+("'${RESULTS_O}'", 0x00000000)' > ${RESULTS_STAT_O} 

root -q -b '${CBC3HAL_ROOT}/root_scripts/plotWMHist.C+("'${VDDA_RD_O}'", "vdda_rd", "rdiff VDDA;col;row;%", 10, 1, '${ncol}', '${nrow}')' 
root -q -b '${CBC3HAL_ROOT}/root_scripts/plotHist.C+("'${VDDA_RD_O}'", "vdda_rd:(VDDA - Bandgap x 2 )/(Bandgap x 2):entry", 100,-100.,0)'

${CBC3HAL_ROOT}/scripts/error_analysis.sh ${RESULTS_O}

cat <<EOF > ${RESULTS_TEX} 
\documentclass[12pt,a4paper]{article}
\usepackage{graphicx}
\begin{document}
\begin{center}
\verb|${PWD}|\\\\
EOF

for bit in `seq 0 30` 32
do
    root -q -b $CBC3HAL_ROOT'/root_scripts/plotWMResult.C+("'${RESULTS_O}'", '$bit', '${ncol}', '${nrow}')' 2>&1 > /dev/null
    bit=`printf "%02d" $bit`
    cat <<EOF >> ${RESULTS_TEX} 
    \includegraphics[width=0.48\textwidth]{result_${bit}.png}
EOF
done

cat <<EOF >> ${RESULTS_TEX} 

\end{center}
\end{document}
EOF

pdflatex ${RESULTS_TEX} 
#
#if [ ${odir} != '.' ]; then
#    mv vdda_rd_h1d.root result*.png bg*.png vdda*.png results.{root,pdf,aux,log} ${odir}/
#fi

if [ ${odir} != "." ]; then
    cd ../ 
fi

if [ $short -eq 1 ]; then
    cd -;
    exit 0
fi

cd -
