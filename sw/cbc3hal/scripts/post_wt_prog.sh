#!/bin/bash

#flag for test
texonly=0
if [ $# -ge 1 ];then
    texonly=${1}
fi
function stop_analysis(){
    echo '\end{center}' >> wt_result.tex
    echo '\end{document}' >> wt_result.tex
    pdflatex wt_result.tex  > /dev/null 2>&1
    exit;
}
# latex file 
cat <<EOF > wt_result.tex
\documentclass[12pt,a4paper]{article}
\usepackage{graphicx}
\begin{document}
\begin{center}
EOF

d=${PWD##*/}

###########
#frame14, bandgap scan, vdda check
ls frame14_*.txt
if [ $? -ne 0 ]; then stop_analysis; fi

if [ $texonly -ne 1 ]; then
    #bandgap scan check
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame14_bg_bgscan_pause_2.00ms.txt      Bandgap   V bo 
    cp frame14_bg_bgscan.png bg_bgscan_${d}.png
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame14_vdda_bgscan_pause_2.50ms.txt    VDDA      V bo 
    #VDDA/(2xbandgap) check
    join frame14_bg_bgscan_pause_2.00ms.txt frame14_vdda_bgscan_pause_2.50ms.txt > bgscan.txt
    awk '/^0x/{r=$4/$2; e = sqrt($3/$2 * $3/$2 + $5/$4 * $5/$4) * r; printf("%s %.3f %.3f\n", $1, r, e)}' bgscan.txt > bgscan_ratio.txt
    awk '{s=$2+s; se2 = $3*$3 + se2;n++}END{m=s/n;printf("%.3f %.4f\n", m, sqrt(se2/n))}' bgscan_ratio.txt > bgscan_ratio_mean.txt
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py bgscan_ratio.txt                        VDDA/BG   Ratio bo
fi


awk 'BEGIN{i=0}/^0x/{c[i]=$1; g[i]=$2; s+=g[i]; s2+=g[i]*g[i]; i++}END{m=s/i;e=sqrt(s2/i - m*m); printf("mean = %10.2f +/- %5.2f\n", m, e); for(ii=0; ii<i; ii++){if( g[ii] < m - 5*e || m + 5*e < g[ii]) printf("%s %10.2f\n", c[ii], g[ii]); }}' frame17_gain.txt > bad_gains.txt

echo 'Bandgap Scans\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame14_bg_bgscan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame14_vdda_bgscan}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{bgscan_ratio}\\' >> wt_result.ex

echo '\newpage' >> wt_result.tex
###########
#frame16, offsets
#offsets
ofst_tuning_rfile="offset_tuning_be1.root"
if [ ! -f $ofst_tuning_rfile ]; then stop_analysis;fi 

if [ $texonly -ne 1 ]; then
root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/plotOffsets.C+("'${ofst_tuning_rfile}'")'
fi
echo '\includegraphics[width=0.96\textwidth]{channels_offsets.png}\\' >> wt_result.tex

###########
#frame16 offsets and frame17 scurve & gain analysis
ofstfile=offset_tuning-be1cbc1.knt
pedfile=frame17_scurve_pedestal.txt
tp60file=frame17_scurve_tp.txt
gainfile=frame17_gain.txt
if [ ! -f $ofstfile ] || [ ! -f $pedfile ] || [ ! -f $tp60file ] || [ ! -f $gainfile ]; then
    stop_analysis;
fi;

if [ $texonly -ne 1 ]; then
    root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/plotChannelTunings.C+("offset_tuning-be1cbc1.knt", "frame17_scurve_pedestal.txt", "frame17_scurve_tp.txt", "frame17_gain.txt")'
#    root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/plotScurvesPedSig.C+("frame17_scurve_pedestal.root", "frame17_scurve_tp.root", 4,2,400, 600)'
    root -q -b ${CBC3HAL_ROOT}'/root_scripts/plotScurveStat.C+("'$pedfile'", "'$tp60file'")'
fi
#echo '\includegraphics[width=0.96\textwidth]{scurves_ped_sig.png}\\' >> wt_result.tex
echo '\includegraphics[width=0.96\textwidth]{channels_scurve_midp.png}\\' >> wt_result.tex
echo '\includegraphics[width=0.96\textwidth]{channels_scurve_sigma.png}\\' >> wt_result.tex
echo '\includegraphics[width=0.96\textwidth]{channels_gains.png}\\' >> wt_result.tex
echo '\newpage' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{offsets.png}' >> wt_result.tex 
echo '\includegraphics[width=0.48\textwidth]{scurve_stat_gain.png}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{scurve_stat_midp.png}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{scurve_stat_noise.png}\\' >> wt_result.tex


echo '\newpage' >> wt_result.tex
###########
#frame28 Voltage bias scan plots
ls frame28_*txt
if [ $? -ne 0 ]; then stop_analysis; fi

if [ $texonly -ne 1 ]; then
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame28_vcth_scan_pause_0.10ms.txt      VCTH      V bo 
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame28_vplus1_scan_pause_0.00ms.txt    VPLUS1    V bo 
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame28_vplus2_scan_pause_0.00ms.txt    VPLUS2    V bo 
    ${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame28_cal_vcasc_scan_pause_0.00ms.txt CAL_VCASC V bo 
fi
echo 'Voltage scans\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame28_vcth_scan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame28_vplus1_scan}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame28_vplus2_scan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame28_cal_vcasc_scan}\\' >> wt_result.tex


echo '\newpage' >> wt_result.tex
###########
#frame29 Current bias scan plots
ls frame29_*txt
if [ $? -ne 0 ]; then stop_analysis; fi

if [ $texonly -ne 1 ]; then
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_CAL_Ibias_scan_pause_1.00ms.txt CAL_Ibias mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Icomp_scan_pause_1.00ms.txt     Icomp     mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Ipaos_scan_pause_1.00ms.txt     Ipaos     mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Ipa_scan_pause_1.00ms.txt       Ipa       mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Ipre1_scan_pause_1.00ms.txt     Ipre1     mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Ipre2_scan_pause_1.00ms.txt     Ipre2     mA bo
${CBC3HAL_ROOT}/python/wt_bias_scan_plot.py frame29_Ipsf_scan_pause_1.00ms.txt      Ipsf      mA bo
fi
echo 'Current scans\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Ipre1_scan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Ipre2_scan}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Ipsf_scan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Ipa_scan}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Ipaos_scan}' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_Icomp_scan}\\' >> wt_result.tex
echo '\includegraphics[width=0.48\textwidth]{frame29_CAL_Ibias_scan}\\' >> wt_result.tex

echo '\newpage' >> wt_result.tex
###########
#frame30 AMUX measurements
nl=''
for type in IHYST Vpafb IBIAS ICOMP CAL_I IPSF CAL_VCASC VCTH PRE1 VPLUS1 VPLUS2 VBG_LDO IPRE2 IPA NC50 VBGBIAS IPAOS
#for type in CAL_I CAL_VCASC IBIAS ICOMP IHYST IPAOS IPA IPRE2 IPSF NC50 PRE1 VBGBIAS VBG_LDO VCTH Vpafb VPLUS1 VPLUS2
do
    if [ -f frame30_AMUX_${type}.txt ]; then
	if [ $texonly -ne 1 ]; then
	    root -q -b ${CBC3HAL_ROOT}'/root_scripts/plotTrend.C("frame30_AMUX_'$type'.txt", "'$type'", 20)'
	fi
	if [ -f Trend_${type}.png ]; then
	    echo '\includegraphics[width=0.48\textwidth]{Trend_'${type}'}'${nl}      >> wt_result.tex
	    if [ "$nl" = '' ]; then nl='\\';
	    else nl='';
	    fi
	fi;
    fi;
done

stop_analysis


