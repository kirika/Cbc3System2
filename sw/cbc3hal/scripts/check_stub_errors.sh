#!/bin/bash

function usage {

echo "$0 [\# th iteration]"
}

#if [ $# -ne 1 ]; then
#    usage;
#    exit 0; 
#fi
#
#sufix=
#if [ $1 -ne 0 ]; then
#    sufix="_${1}"
#fi
#echo "sufix=$sufix"


dirs=`awk '{buf_e = and( 0x00200000, strtonum($2) ); if(buf_e != 0) print $1}' results_final.txt`


printf "#Chip      StubAddress  ClusterWidthLayerSwap pT WindowOffset\n" | tee stub_errors.txt
for c in $dirs
do
    f21=0;f22=0;f23=0;f24=0;
    nf21=`sed -n '/frame21.* stub test failed./p' $c/wt_prog.log | wc -l`;
    if [ $nf21 -ne 0 ]; then 
	f21=1 
    fi
    nf22=`sed -n '/frame22.* stub test failed./p' $c/wt_prog.log | wc -l`;
    if [ $nf22 -ne 0 ]; then 
	f22=1 
    fi
    nf23=`sed -n '/frame23.* stub test failed./p' $c/wt_prog.log | wc -l`;
    if [ $nf23 -ne 0 ]; then 
	f23=1 
    fi
    nf24=`sed -n '/frame24.* stub test failed./p' $c/wt_prog.log | wc -l`;
    if [ $nf24 -ne 0 ]; then 
	f24=1 
    fi
    printf "%- 6s % 10d % 10d % 10d % 10d\n" $c $f21 $f22 $f23 $f24  | tee -a stub_errors.txt

done

