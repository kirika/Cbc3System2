#!/bin/bash

dirs=`awk '{buf_e = and( 0x00080000, strtonum($2) ); if(buf_e != 0) print $1}' results_final.txt`

Nbe=0;
Neb=0; Nch=0; Npa=0; Nl1a=0; Nhe=0;
echo "#Chip  EBits     Ch(nch)    PA  L1AC  HErr" | tee  buf_errors.txt
for c in $dirs
do 
    (( Nbe++ ))
    eb=0; ch=0; pa=0; l1a=0; he=0;
    neb=`sed -n '/frame19.*Some error bits are set./p' $c/wt_prog.log | wc -l`;
    if [ $neb -ne 0 ]; then 
	eb=1 
	(( Neb++ ))
    fi
    nch=`sed -n '/frame19.*Some hits are /p' $c/wt_prog.log | wc -l`;
    if [ $nch -ne 0 ]; then 
	ch=1 
	(( Nch++ ))
    fi
    npa=`sed -n '/frame19.*Pipeline Address is [0-9]\+, but [0-9]\+ is expected./p' $c/wt_prog.log | wc -l`; 
    if [ $npa -ne 0 ]; then 
	pa=1 
	(( Npa++ ))
    fi
    nl1a=`sed -n '/frame19.*L1A counter is not OK./p' $c/wt_prog.log | wc -l`;
    if [ $nl1a -ne 0 ]; then 
	l1a=1 
	(( Nl1a++ ))
    fi
    nhe=`sed -n '/frame19.*header_error at Buffer/p' $c/wt_prog.log | wc -l`;
    if [ $nhe -ne 0 ]; then 
	he=1 
	(( Nhe++ ))
    fi

    printf "%- 6s% 6d% 6d (% 4d)% 6d% 6d% 6d | " $c $eb $ch $nch $pa $l1a $he 

    #f errors per bufffer (seems clustered?)
    grep "delay\\["  $c/wt_prog.log |cut -d ']' -f 2- > tmp0.txt 



    if [ "`du -0 -b tmp0.txt |cut -f 1 `" -eq 66048  ] ; then
	cat tmp0.txt |awk 'BEGIN {i=0;bi=0;e=0;} {for(a=1;a<33;a++) {c1=strtonum("0x" $a); c2=(a+32*i+bi-1)%512; if (c1!=c2) {e=e+1;ace[a+32*i-1]=1} else ace[a+32*i-1]=0; }; i=i+1; if (i==16) { i=0; bi=bi+1; printf("% 4d", e); e=0;} }END{ ce=0;for(i=0; i<255; i++){if (ace[i] == 1) ce=1;} printf("|% d\n", ce)}' | tee -a buf_errors.txt
    else
	printf "\n" | tee -a buf_errors.txt

    fi
    rm tmp0.txt
done

printf "Total % 10d % 16d % 10d % 10d % 10d : % 10d Buffer Errors\n" $Neb $Nch $Npa $Nl1a $Nhe $Nbe | tee -a buf_errors.txt


