#!/bin/bash

nmax=${2}
for n in `seq 0 ${nmax}` 
do
    ${CBC3HAL_ROOT}/scripts/mk_err_info.sh ${1} $n
done

np=0
for n in `seq 1 ${nmax}` 
do
    if [ $n -eq 1 ];then
	join -a 1 -e none -o 1.1,1.2,2.2 results2.txt results2_${n}.txt > results2_0_1.txt 
    else
	join -a 1 -e none -o auto results2_0_${np}.txt results2_${n}.txt  > results2_0_${n}.txt
    fi
    np=$n

done
