#!/bin/bash

function usage {

    echo "$0 [file] [iteration] [error_mask]"

}


if [ $# -ne 3 ]; then
    usage;
    exit 0; 
fi

file=$1
sufix=
if [ $2 -ne 0 ]; then
    sufix="_${2}"
fi
echo "sufix=$sufix"

dirs=
en=`echo $3 | awk '{printf("%d", strtonum($1))}'`
echo $en
if [ $en -eq 0 ]; then
    dirs=`awk '{print $1}' $file`
else
    dirs=`awk '{buf_e = and( '${3}', strtonum($2) ); if(buf_e != 0) print $1}' $file`
fi

printf "% 5s % 15s %14s %14s\n" "chip" "error code" "Initial VDDA" "tuned VDDA" 
for d in $dirs
do
    errc=`sed -n 's/^'${d}' \(.*\)$/\1/p' ${file}`

    VDDA=`grep 'frame06:VDDA:V ' ${d}${sufix}/wt_prog.log | awk '{print $2}'`
    vdda=`grep 'pre-burn_vdda ' ${d}${sufix}/wt_prog.log | awk '{print $2}'`
    printf "% 5s% 15s %10.3f %10.3f\n" $d $errc $VDDA $vdda 
done

