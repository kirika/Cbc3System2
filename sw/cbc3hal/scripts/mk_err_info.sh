#!/bin/bash

function usage {

echo "$0 [wafer test directory] [\# th iteration]"
}

if [ $# -ne 2 ]; then
    usage;
    exit 0; 
fi

cd $1

sufix=
echo "second argument = $2" 
if [ $2 -ne 0 ]; then
    sufix="_${2}"
fi
echo "sufix=$sufix"
rm -f tmp.txt
for f in `ls -tr1 */wt_prog.log` 
do 
    grep -H 'WAFER TEST RESULT' $f >> tmp.txt; 
done 
sed -n 's:^\([0-9]\+_[0-9]\+\)'${sufix}'/wt_prog.log.* RESULT \(.*\):\1 \2:p' tmp.txt > results${sufix}.txt
#sed -n 's:^\([0-9]\+\)_\([0-9]\+\)'${sufix}'/wt_prog.log.* RESULT \(.*\):\1 \2 \3:p' tmp.txt | awk '{printf("%02d:%02d %s\n", $1, $2, $3)}'| sort -k 1b,2 > results2${sufix}.txt

#root -q -b '${CBC3HAL_ROOT}/root_scripts/analyze_results.C+("results'${sufix}'.txt", 0x00000000)' > results_stat${sufix}.txt

cd ../

