#!/bin/bash

function usage {

    echo "$0 [file1 : main result list file] [file2 : result list to be replaced in file1.]"

}

if [ $# -lt 2 ]; then
    usage;
    exit 0; 
fi

file1=${1}
file2=${2}

awk 'FNR==NR{a[$1]=$0;next}a[$1]{$0=a[$1]}1' $file2 $file1

