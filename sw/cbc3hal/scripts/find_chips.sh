#!/bin/bash

function usage {

    echo "$0 -n [file1 : chip list which searched for] [file2 : the chip list to be searched in file1]"

}

if [ $# -lt 2 ]; then
    usage;
    exit 0; 
fi

not_in=0
while getopts “n” opt; do
    case $opt in
	n) not_in=1;;
    esac
done

shift $(($OPTIND - 1))

file1=${1}
file2=${2}

if [ $not_in -eq 0 ]; then
    for c in `awk '{print $1}' $file2`
    do
	sed -n '/^'$c'/p' $file1
    done
else
    for c in `awk '{print $1}' $file2`
    do
	line=`grep $c $file2`
	awk  'BEGIN{found=0}/^'$c'/{found=1}END{ if(found==0){printf("%s\n", "'"${line}"'") } }' $file1
    done

fi


