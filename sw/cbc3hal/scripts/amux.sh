#!/bin/bash

types='CAL_I CAL_VCASC IBIAS ICOMP IHYST IPAOS IPA IPRE2 IPSF NC50 PRE1 VBGBIAS VBG_LDO VCTH Vpafb VPLUS1 VPLUS2'

function getLimL () {
    case $1 in
	"CAL_I")
	    echo -n '0.24';;
	"CAL_VCASC")
	    echo -n '0.25';;
	"IBIAS")
	    echo -n '0.23';;
	"ICOMP")
	    echo -n '0.24';;
	"IHYST")
	    echo -n '0.080';;
	"IPAOS")
	    echo -n '0.5';;
	"IPA")
	    echo -n '0.40';;
	"IPRE2")
	    echo -n '0.40';;
	"IPSF")
	    echo -n '0.28';;
	"NC50")
	    echo -n '0.45';;
	"PRE1")
	    echo -n '0.35';;
	"VBGBIAS")
	    echo -n '0.645';;
	"VBG_LDO")
	    echo -n '0.515';;
	"VCTH")
	    echo -n '0.30';;
	"Vpafb")
	    echo -n '0.1';;
	"VPLUS1")
	    echo -n '0.40';;
	"VPLUS2")
	    echo -n '0.40';;
    esac
}

function getLimH () {

    case $1 in
	"CAL_I")
	    echo -n '0.285';;
	"CAL_VCASC")
	    echo -n '0.45';;
	"IBIAS")
	    echo -n '0.26';;
	"ICOMP")
	    echo -n '0.28';;
	"IHYST")
	    echo -n '0.12';;
	"IPAOS")
	    echo -n '0.8';;
	"IPA")
	    echo -n '0.6';;
	"IPRE2")
	    echo -n '0.60';;
	"IPSF")
	    echo -n '0.32';;
	"NC50")
	    echo -n '0.56';;
	"PRE1")
	    echo -n '0.60';;
	"VBGBIAS")
	    echo -n '0.675';;
	"VBG_LDO")
	    echo -n '0.528';;
	"VCTH")
	    echo -n '0.45';;
	"Vpafb")
	    echo -n '0.20';;
	"VPLUS1")
	    echo -n '0.55';;
	"VPLUS2")
	    echo -n '0.55';;
    esac
}

function getMin () {

    case $1 in
	"CAL_I")
	    echo -n '0.23';;
	"CAL_VCASC")
	    echo -n '0.20';;
	"IBIAS")
	    echo -n '0.22';;
	"ICOMP")
	    echo -n '0.23';;
	"IHYST")
	    echo -n '0.05';;
	"IPAOS")
	    echo -n '0.45';;
	"IPA")
	    echo -n '0.35';;
	"IPRE2")
	    echo -n '0.35';;
	"IPSF")
	    echo -n '0.25';;
	"NC50")
	    echo -n '0.45';;
	"PRE1")
	    echo -n '0.30';;
	"VBGBIAS")
	    echo -n '0.64';;
	"VBG_LDO")
	    echo -n '0.514';;
	"VCTH")
	    echo -n '0.25';;
	"Vpafb")
	    echo -n '0.00';;
	"VPLUS1")
	    echo -n '0.30';;
	"VPLUS2")
	    echo -n '0.30';;
    esac
}

function getMax () {

    case $1 in
	"CAL_I")
	    echo -n '0.30';;
	"CAL_VCASC")
	    echo -n '0.46';;
	"IBIAS")
	    echo -n '0.27';;
	"ICOMP")
	    echo -n '0.30';;
	"IHYST")
	    echo -n '0.13';;
	"IPAOS")
	    echo -n '0.81';;
	"IPA")
	    echo -n '0.61';;
	"IPRE2")
	    echo -n '0.61';;
	"IPSF")
	    echo -n '0.35';;
	"NC50")
	    echo -n '0.57';;
	"PRE1")
	    echo -n '0.61';;
	"VBGBIAS")
	    echo -n '0.68';;
	"VBG_LDO")
	    echo -n '0.535';;
	"VCTH")
	    echo -n '0.46';;
	"Vpafb")
	    echo -n '0.25';;
	"VPLUS1")
	    echo -n '0.56';;
	"VPLUS2")
	    echo -n '0.56';;
    esac
}

function plot () {

    cfile=${1}
    sfx=${3}
    ch_to_check_f=amux_ch_to_check.txt
    rm -f ${ch_to_check_f}
    touch ${ch_to_check_f}

    for type in $types
    do 
	of=${type}.txt; rm -f $of; touch ${of} 
#	for c in `ls -1 | sed -n '/^[0-9]\+_[0-9]\+$/p'` 
	for c in `awk '{print $1}' ${cfile}` 
	do 
	    f=${c}${sfx}/frame30_AMUX_${type}.txt; 
	    lim_l=`getLimL ${type}`
	    lim_h=`getLimH ${type}`
	    if [ -f ${f} ]; then 
		awk '/Mean/{printf("%- 5s %s %s\n", "'${c}'", $3, $5)}' $f >> ${of};
		awk '/Mean/{ if( $3 < '$lim_l' || '$lim_h' < $3) printf("%- 5s %s %s\n", "'${c}'", $3, $5)}' $f >> ${ch_to_check_f};
	    fi; 
	done; 
	min=`getMin ${type}`
	max=`getMax ${type}`
	root -q -b '${CBC3HAL_ROOT}/root_scripts/plotHist.C+("'${of}'", "'${type}'", 40, '${min}', '${max}')' 1> /dev/null 2>&1
    done
}

function mkpdf() {

texfile=amux.tex

rm -f $texfile

cat <<EOF > ${texfile} 
\documentclass[12pt,a4paper]{article}
\usepackage{graphicx}
\begin{document}
\begin{center}
\verb|${PWD}|\\\\
EOF

    for type in $types
    do 
    echo "\includegraphics[width=0.48\textwidth]{${type}_h1d.png}" >> ${texfile}
    done

cat <<EOF >> ${texfile} 

\end{center}
\end{document}
EOF

pdflatex  ${texfile}

}

plot ${1} ${2} ${3}
mkpdf

dir=$2
if [ "${dir}" == "" ]; then
    exit
fi
if [ ! -d ${dir} ]; then
    mkdir ${dir}
fi

mv *_h1d.{png,root} ${dir} 
mv amux.* $dir 
mv amux_ch_to_check.txt ${dir}
for type in $types
do
    mv ${type}.txt ${dir}

done 


