#!/bin/bash

root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/FindBadScurves.C+("frame17_scurve_pedestal.txt")' > channel_test.txt 
root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/FindBadScurves.C+("frame17_scurve_tp.txt")' >> channel_test.txt
root -q -b '/home/kirika/Cbc3System2/sw/cbc3hal/root_scripts/FindBadGains.C+("frame17_gain.txt")' >> channel_test.txt

message=
grep Found channel_test.txt > /dev/null
if [ $? -eq 0 ]; then
    message="Bad channels found."
fi

echo "$message"  >>  channel_test.txt


