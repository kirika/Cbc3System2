#!/bin/bash

i=0
while [ 1 ]
do
    i=$((i+1))
    rm -f results_${i}.txt
    touch results_${i}.txt
    for d in `ls -1 | sed -n '/[0-9]\+_[0-9]\+_'$i'/p'` 
    do
	c=`echo $d | sed -n 's/\(.*\)_[0-9]\+/\1/p'`
	line=`sed -n 's:.*WAFER TEST RESULT \(.*\):\1:p' $d/wt_prog.log`
	if [ "$line" == "" ]; then
	    line='0x80000000';
	fi
	printf "%s %s %s\n" "$c" "$line" "$d" >> results_${i}.txt
    done
    count=`wc -l results_${i}.txt | awk '{print $1}'`

    if [ $count -eq 0 ]; then
	rm -f results_${i}.txt
	break;
    fi
    if [ $i -eq 1 ]; then
	cp results_${i}.txt results_o.txt
    else
	mv results_o.txt results_tmp.txt
	replace_results.sh results_tmp.txt results_${i}.txt > results_o.txt
    fi
done

echo "### FUSE ERROR test is replaced with the old result" > results_to_be_replaced.txt
find_chips.sh results_o.txt results_FUSE_ERRORS.txt >> results_to_be_replaced.txt
echo "### when error disappears, replace the results with the previous test" >> results_to_be_replaced.txt
for c in `awk '{print $1}' results_o.txt`
do
    result=`awk '/^'${c}' /{printf("%s", $2)}' results_o.txt`
    fuse_err=$((result & 1 << 14))
    if [ $fuse_err -eq 0 ]; then
	dir=`awk '/^'${c}' /{printf("%s", $3)}' results_o.txt`
	awk '/^'${c}' /{if($2 == "0x00000000") printf("%s %s %s\n", "'${c}'", "'${result}'", "'${dir}'")}' results.txt >> results_to_be_replaced.txt
    fi
done

if [ ! -d removed_runs ]; then
    mkdir removed_runs  
fi

for d in `awk '/^[0-9]/{print $1}' results_to_be_replaced.txt`
do
    mv $d removed_runs/

    r=`awk '/^'$d' /{print $3}' results_to_be_replaced.txt`
    ln -s $r $d
done

