#!/bin/bash

function usage {

    echo "$0 [file] [iteration] [error_mask]"

}

if [ $# -ne 3 ]; then
    usage;
    exit 0; 
fi

file=$1
sufix=
if [ $2 -ne 0 ]; then
    sufix="_${2}"
fi
#echo "sufix=$sufix"

en=`echo $3 | awk '{printf("%d", strtonum($1))}'`
#echo $en
if [ $en -eq 0 ]; then
    dirs=`awk '/0x00000000/{print $1}' $file`
else
    dirs=`awk '{buf_e = and( '${3}', strtonum($2) ); if(buf_e != 0) print $1}' $file`
fi
for d in $dirs
do
    errc=`sed -n 's/^'${d}' \(.*\)$/\1/p' ${file}`
    printf "% 5s% 15s\n" $d $errc
done

