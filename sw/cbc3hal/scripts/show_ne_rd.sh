#!/bin/bash

etype="WT_ERR_BUFRAM"
ne=`awk '/'${etype}'/{print $4}' results_stat.txt`
vdda_rd=`awk '{s+=$2;n++}END{printf("%f", s/n)}' vdda_rd.txt`

printf "[# of %s] [VDDA measured - expected in %%]\n" $etype
printf "% 10s%30.1f\n" $ne $vdda_rd

