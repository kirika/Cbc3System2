#!/bin/bash

ver=1
final=1
while getopts “0fh” opt; do
    case $opt in
	0) ver=0
	    ;;
	f) final=1
	    ;;
	h) echo '${0} (-h -0 -f)'; echo '-h : show help'; echo '-0 : for CBC3.0'; echo '-f : results_final.txt is not created'
	    ;;
    esac
done

if [ $final -eq 0 ]; then

    echo "The previous results are recovered." 

    if [ $ver -eq 0 ]; then
	$CBC3HAL_ROOT/scripts/mk_winfo.sh -0 -s .
    else
	$CBC3HAL_ROOT/scripts/mk_winfo.sh -s .
    fi
    $CBC3HAL_ROOT/scripts/recover_first_results.sh
    $CBC3HAL_ROOT/scripts/replace_results.sh results.txt results_to_be_replaced.txt > results_final.txt

else
    if [ $ver -eq 0 ]; then
	$CBC3HAL_ROOT/scripts/mk_winfo.sh -0 -s .
    else
	$CBC3HAL_ROOT/scripts/mk_winfo.sh -s .
    fi
    cp results.txt results_final.txt
fi

awk '{if($2 != "0x00000000") print $0}' results_final.txt > badchips_final.txt
awk '{if($2 == "0x00000000") print $0}' results_final.txt > goodchips_final.txt

$CBC3HAL_ROOT/scripts/error_analysis.sh results_final.txt | tee results_final_stat.txt

ncol=36
nrow=17

if [ $ver -eq 0 ]; then
    ncol=15
    nrow=15
fi
root -q -b ${CBC3HAL_ROOT}'/root_scripts/mkWM.C+("badchips_final.txt", "results_final.txt", "badchips_final","bad chips;col;row", '${ncol}', '${nrow}' )' 

#awk '{print $1}' results_final.txt > results_final_chiplist.txt
#$CBC3HAL_ROOT/scripts/mk_winfo.sh . results_final_chiplist.txt
#awk 'FNR==NR{a[$1]=$2;next}{if(a[$1] != $2) printf("%s %s %s\n", $1, $2, a[$1])}' results_final.txt results_o.txt > chips_diferr_rerun.txt

ng=`wc -l goodchips_final.txt | awk '{print $1}'`
N=`wc -l results_final.txt | awk '{print $1}'`
echo "GOODCHIPS ${ng}" | tee -a results_final_stat.txt
echo "TOTALCHIPS ${N}" | tee -a results_final_stat.txt
echo "YIELD  " `echo 'scale=3;'${ng}'/'${N}'*100' | bc`  | tee -a results_final_stat.txt

for bit in `seq 0 30` 32
do
    root -q -b $CBC3HAL_ROOT'/root_scripts/plotWMResult.C+("results_final.txt", '$bit', '${ncol}', '${nrow}')' 2>&1 > /dev/null
done

$CBC3HAL_ROOT/scripts/analyze_errs2.sh

