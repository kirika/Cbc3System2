#!/bin/bash

function nerr () {

error_bits=$1
error_type=$2
iftype=${3}
ifile=$4
ofile=$5
this_err_file=${iftype}_${error_type}.txt
rm -f $this_err_file
touch $this_err_file

tmpfile0=tmpfile0.txt
tmpfile1=tmpfile1.txt
cp $ifile $tmpfile0 

for eb in $error_bits 
do
    err=$((1<<eb))
    #    printf "0x%08x\n" $err
    awk '{buf_e = and( '${err}', strtonum($2) ); if(buf_e != 0) print $0}' $tmpfile0 >> $this_err_file 
    awk '{buf_e = and( '${err}', strtonum($2) ); if(buf_e == 0) print $0}' $tmpfile0 > $tmpfile1
    mv $tmpfile1 $tmpfile0
done
N=`wc -l ${this_err_file} | awk '{print $1}'`
printf "%40s % 10d\n" $error_type $N
mv $tmpfile0 $ofile
}

if [ $# -lt 1 ]; then

    echo "USAGE ${0} [input result file]"
    exit
fi

input=${1}
iftype=${input%%.*}
# WT_ERR_IPBUS                              1          0 E
# WT_ERR_SETTINGS                           5          0 E
# WT_ERR_KMM                                2          0 E
rm -f tmp1.txt;touch tmp1.txt
nerr '1 5 2' "SYSTEM_ERRORS" ${iftype} $input tmp1.txt

# WT_ERR_I2C                                3          6 E from CbcI2cRegIpbusException
# WT_ERR_BE                                 4          0 E from Cbc3BeException includes SEQ & DAQ, OTHERS
# WT_ERR_CBC                                6          0 E part of RAW_DATA
# WT_ERR_SEQ                                8          0 E
# WT_ERR_DAQ                                9          0 E
# WT_ERR_RAW_DATA                          10          0 E
# WT_ERR_BE_CONFIG                         11          0 not used
nerr '3 4 6 8 9 10 11' "CBC_DAQ_I2C_ERRROS_WITH_TERMINATION" ${iftype} tmp1.txt tmp2.txt 

# WT_ERR_I2C_STUCK_BITS                    12          0
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '12' "I2C_STUCK_BITS" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_FUSE_PROC                         14         10 E
# WT_ERR_AFTER_FUSE                        15          0 E
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '14 15' "FUSE_ERRORS" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_OTHER                              0          1 E
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '0' "OTHER_ERRORS" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_CURR                              13          1
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '13' "CURR" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_VDDA                              28          0
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '28' "VDDA" ${iftype} tmp1.txt tmp2.txt
# WT_ERR_OFFSET_TUNING                     16          0
# WT_ERR_VCTH_SCAN                         17          0 E fit failed
# WT_ERR_GAIN                              29          7 E
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '16 17 29' "CHANNEL" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_PIPELINE                          18          2
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '18' "PIPELINE" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_BUFRAM                            19         24
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '19' "BUFRAM" ${iftype} tmp1.txt tmp2.txt
# WT_ERR_CHANNEL_MASK                      20          0
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '20' "MASK" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_STUB                              21          2
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '21' "STUB" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_HDHIP                             22          0
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '22' "HDHIP" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_FCI                               23          1 E
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '23' "FCI" ${iftype} tmp1.txt tmp2.txt
# WT_ERR_DLL                               24          0 E
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '24' "DLL" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_AMUX_BIAS                         27          1
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '27' "AMUX" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_ORBIT_RESET                       30          4
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '30' "ORBIT_RESET" ${iftype} tmp1.txt tmp2.txt

# WT_ERR_CRASHED                       31
mv -f tmp2.txt tmp1.txt; touch tmp2.txt
nerr '31' "CRASHED" ${iftype} tmp1.txt tmp2.txt


# WT_ERR_BIAS                              25          0 not used
# WT_ERR_CURR_SCAN                         26          0 not used

