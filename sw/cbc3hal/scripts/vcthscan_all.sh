#!/bin/bash

function print_help {

echo "${0} (-b [BE system config file] -c [cbc config file] -o [cbc option config files] -r [run config file] -g [get pulse group] -d [debug level] -h (showing help)"

}


POSITIONAL=()
debug=0
wdir=vcthscan
cbc_cnfg_file=""
cbc_option_cnfg_files=""
be_system_cnfg_file=""
run_cnfg_file=""
no_run=0

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
	-h|--help)
	    print_help
	    exit
	    ;;
	-b|--be_cnfg)
	    be_system_cnfg_file=`readlink -f "$2"`
	    shift # past argument
	    shift # past value
	    ;;
	-c|--cbc_cnfg)
	    cbc_cnfg_file=`readlink -f "$2"`
	    shift
	    shift
	    ;;
	-o|--cbc_option_cnfg)
	    cbc_option_cnfg_files=$2
	    shift
	    shift
	    ;;
	-r|--run_cnfg)
	    run_cnfg_file=`readlink -f "$2"`
	    shift
	    shift
	    ;;
	-g|--group_type)
	    group_type="$2"
	    shift
	    shift
	    ;;
	-t|--test_group)
	    test_group="$2"
	    shift
	    shift
	    ;;
	-p|--tp_pot)
	    tppot="$2"
	    shift
	    shift
	    ;;
	-d|--debug)
	    debug="$2"
	    shift
	    shift
	    ;;
	-n|--no_run)
	    no_run=1
	    shift
	    ;;
	-m|--memo)
	    wdir="$2"
	    shift
	    shift
	    ;; 
    esac
done

if [ "$cbc_cnfg_file" = "" ]; then
    cbc_cnfg_file=${CBC3HAL_ROOT}/etc/CBC_I2CREGS_CNFG_FILE_LIST_FOR_VCTHSCAN.txt
fi
if [ "$be_system_cnfg_file" = "" ]; then
    be_system_cnfg_file=${CBC3HAL_ROOT}/etc/cbc3_system_config.txt
fi
if [ "$run_cnfg_file" = "" ]; then
    run_cnfg_file=${CBC3HAL_ROOT}/etc/cbc_param_tunings_and_vcthscan_run_config.txt 
fi

echo "BE  configuration file          : ${be_system_cnfg_file}"
echo "CBC configuration file          : ${cbc_cnfg_file}"
echo "CBC optional configuration file : ${cbc_option_cnfg_files}"
echo "RUN configuration file          : ${run_cnfg_file}"

if [ $no_run -eq 1 ]; then
    exit
fi

if [[ ! -d ${wdir} ]]; then
    mkdir ${wdir} 
fi


#cbc3daq 'cbc3be configure etc/cbc3_system_config.txt 0'

for dll in 04 
do
    #for tppot in 024 036 048 060 072 084 096 108 120
#    for tppot in `seq 0 5 255` 
#    for tppot in `seq 95 5 255` 
#    for tppot in `seq 0 5 100` 
    for tppot in 24 
    do 
	tppot=`printf "%03d" $tppot`
	for tpdelay in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 
#	for tpdelay in 22 
	do
	   for triglat in 195 196 197 198 199 200 201 
#          for triglat in 197 199 200 201 
#          for triglat in 196 195 
        #for triglat in 200 
	    do
		#statfile
		statfile=${wdir}/scurve_stat_tppot${tppot}_tpdelay${tpdelay}_triglat${triglat}_dll${dll}.txt
		if [ -f $statfile ]; then
		    rm $statfile
		fi
		touch $statfile

		#vcthscan
		time vcthscan --outDir ${wdir} \
              --configBe ${be_system_cnfg_file} \
              --configCbc ${cbc_cnfg_file} \
	      --optionCbc ${cbc_option_cnfg_files} \
	      --config ${run_cnfg_file} \
	      --tp_pot $tppot \
	      --tp_delay $tpdelay \
	      --trig_lat $triglat \
	      --dll $dll \
	      --group_type $group_type \
	      --test_group $test_group \
	      --debug ${debug} | tee -a $statfile 
	#	time vcthscan --outDir vcthscan --configCbc etc/CBC_I2CREGS_CNFG_FILE_LIST.txt --config etc/cbc_param_tunings_and_vcthscan_run_config.txt --tp_pot $tppot --tp_delay $tpdelay --trig_lat $triglat --dll ${dll} | tee -a $statfile

		fname=${wdir}/vcthscan-tppot${tppot}_tpdelay${tpdelay}_triglat${triglat}_dll${dll}.root
		echo "starting scurve analyzer"
		for cbc_id in 01
		do
		    time root -q -b '${CBC3HAL_ROOT}/root_scripts/scurve_analyzer.C+("'${fname}'", "be1cbc'$cbc_id'" )' 2>&1 | tee -a $statfile 
		done
	    done
	done
    done
done

