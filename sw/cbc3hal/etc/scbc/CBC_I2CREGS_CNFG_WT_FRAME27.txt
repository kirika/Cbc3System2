# Title: CBC I2C Register settings for wafer test [scurve scan setting] (frame 21)  
# Fields: Page Address Value
# FieldTypes: int int int
1 0x00 0x40
1 0x01 0x20
1 0x02 0x87
1 0x03 0x28
1 0x04 0x5a
1 0x05 0x6e
1 0x06 0x8c
1 0x07 0x2d
1 0x09 0x5a
1 0x0b 0x77
1 0x0c 0x08
1 0x0d 0x00
1 0x0e 0xf0
1 0x0f 0x60
1 0x10 0xf0
1 0x11 0x3f
1 0x12 0x0e
1 0x13 0x00
1 0x14 0x00
1 0x1b 0x04
1 0x1c 0x80
1 0x1d 0x00
1 0x4f 0x20
1 0x50 0x03
1 0x20 0x03
1 0x21 0x00
1 0x22 0x00
1 0x23 0x00
1 0x24 0x03
1 0x25 0x00
1 0x26 0x00
1 0x27 0x00
1 0x28 0x03
1 0x29 0x00
1 0x2a 0x00
1 0x2b 0x00
1 0x2c 0x00
1 0x2d 0x00
1 0x2e 0x00
1 0x2f 0x00
1 0x30 0x00
1 0x31 0x00
1 0x32 0x00
1 0x33 0x00
1 0x34 0x00
1 0x35 0x00
1 0x36 0x00
1 0x37 0x00
1 0x38 0x00
1 0x39 0x00
1 0x3a 0x00
1 0x3b 0x00
1 0x3c 0x00
1 0x3d 0x00
1 0x3e 0x00
1 0x3f 0x00

