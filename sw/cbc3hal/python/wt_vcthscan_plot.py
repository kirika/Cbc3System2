#!/usr/bin/python

import sys
import matplotlib.pyplot as plt
import re
import numpy as np
#from scipy.optimize import curve_fit
#from scipy.stats import chi2
from matplotlib.font_manager import FontProperties
#from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable
import wt_plot as wtp

if __name__ == "__main__":

    type = 'vcth'
    fnames = [
	     (type + '-> 0.00ms', 'frame28_'+type+'_vcthscan_pause_0.00ms.txt', 'ro'),   
	     (type + '<- 0.50ms', 'frame28_'+type+'_vcthscan_pause_0.50ms.txt', 'bo'),   
	     (type + '-> 0.60ms', 'frame28_'+type+'_vcthscan_pause_0.60ms.txt', 'go'),   
	     (type + '<- 0.70ms', 'frame28_'+type+'_vcthscan_pause_0.70ms.txt', 'ko'),   
	     (type + '-> 0.80ms', 'frame28_'+type+'_vcthscan_pause_0.80ms.txt', 'rx')   
	     ]
#    plt.subplot(1,2,1);
    plt.grid(True)
    for label, fname, fmt in fnames:
	print label, fname
	wtp.plot_xy( fname, label, fmt, 0, 1, 2, 1)

    plt.title('VCTH voltage vs. VCTH I2C setting.')
    plt.legend(loc='upper left')
    plt.show()

