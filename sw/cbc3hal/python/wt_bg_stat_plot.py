#!/usr/bin/python

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import numpy as np
import wt_plot_utils as wp

if __name__ == "__main__":

    fname = sys.argv[1]
    plt.grid(True)
    x = []
    y = []
    z = []
    x, y, ey = wp.read_xy( fname, 3, -1, -1, False)
    plt.hist(x, 50, facecolor='g')
    plt.xlabel("VDDA - 2 x Gandgap [V]")
    plt.savefig("bg_stat.png")
