#!/usr/bin/python

import sys
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import re
import glob
import numpy as np
#from scipy.stats import chi2
from matplotlib.font_manager import FontProperties
import wt_plot_utils as wp
#from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable

def line2d(x,a,b):
    return 0.5 * a + 0.001 * b * x

fmts = [ 'ro', 'bo', 'go', 'ko', 'rx', 'bx', 'gx', 'kx', 'r<', 'b<', 'g<', 'k<', 'r^', 'b^' ]

if __name__ == "__main__":

    ##########
    #  Bandgap 
    ##########

    type = 'bg'
    files = glob.glob('./frame14_'+type+'_bgscan_pause_*ms.txt')
    files.sort()

#    plt.subplot(1,2,1);
#    plt.figure(figsize=(50,50))
    plt.grid(True)
    i=0
    f18 = []
    for fname in files:
	match = re.search('pause_[0-9]\.[0-9]{2}ms', fname)
	if match :
	    label = match.group(0)
	    print label, fname
	    x, y, ey = wp.plot_xy( fname, label, fmts[i], 0, 1, 2, 5)
	    i = i + 1

	    p0 = [1.0, -1.0]
	    popt, pcov = curve_fit(line2d, np.array(x), np.array(y), p0, np.array(ey))
	    print popt
	    print pcov
	    f18.append(line2d(x[18], *popt))

    plt.title('Bandgap vs. BG I2C setting.')
    plt.legend()
    plt.savefig('bg_bgscan_tuning.png')

    f18 /= f18[-1]

    for i in xrange(len(f18)):
	if np.fabs(1-f18[i]) < 0.001:
	    print files[i] 

    #x, y, ey = wp.plot_xy( "frame14_bestbg_bgi2c.txt", "best bandgap vs. pause after each bg settings", 0, 1, -1)
    plt.gcf().clear()
    plt.grid(True)
    wp.plot_xy( "frame14_bestbg_bgi2c.txt", "", fmts[0], 0, 1, -1)
    plt.title('Bandgap I2C at best bandgap vs. pause duration after each bg settings.')
    plt.savefig('bgi2c_best_bg_vs_paus.png')

    ##########
    #  VDDA
    ##########

    type = 'vdda'
    files = glob.glob('./frame14_'+type+'_bgscan_pause_*ms.txt')
    files.sort()

    plt.gcf().clear()
    plt.grid(True)
    i=0
    f18 = []
    for fname in files:
	match = re.search('pause_[0-9]\.[0-9]{2}ms', fname)
	if match :
	    label = match.group(0)
	    print label, fname
	    x, y, ey = wp.plot_xy( fname, label, fmts[i], 0, 1, 2, 5)
	    i = i + 1

	    p0 = [2.0, -1.0]
	    popt, pcov = curve_fit(line2d, np.array(x), np.array(y), p0, np.array(ey))
	    print popt
	    print pcov
	    f18.append(line2d(x[18], *popt))

    plt.title('VDDA vs. BG I2C setting.')
    plt.legend()
    plt.savefig('vdda_bgscan_tuning.png')

    f18 /= f18[-1]

    for i in xrange(len(f18)):
	if np.fabs(1-f18[i]) < 0.001:
	    print files[i] 


    plt.gcf().clear()
    plt.grid(True)
    wp.plot_xy( "frame14_bestvdda_bgi2c.txt", "", fmts[0], 0, 1, -1)
    plt.title('Bandgap I2C at best VDDA vs. pause duration after each bg settings.')
    plt.savefig('bgi2c_best_vdda_vs_paus.png')

    sys.exit(0)

#    plt.xlim(0,100);
#    plt.ylim(0.0,1.5);
    plt.show()


