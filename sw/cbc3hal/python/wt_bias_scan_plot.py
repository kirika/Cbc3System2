#!/usr/bin/python

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import numpy as np
import wt_plot_utils as wp
import os.path

if __name__ == "__main__":

    fname = sys.argv[1]
    title = sys.argv[2]
    ylabel = sys.argv[3]
    fmt   = sys.argv[4]

    if not os.path.isfile(fname):
	exit(0) 

    plt.grid(True)

    wp.plot_xy( fname, "", fmt, 0, 1, 2)

    plt.xlabel("I2C UNIT")
    plt.ylabel(ylabel)
    plt.title(title)
#    plt.show()

    i = fname.find("_pause")
    if i != -1:
    	fig_fname = fname[0:i] + ".png"
    else:
	i = fname.find(".")
	fig_fname = fname[0:i] + ".png"

    print fig_fname
    plt.savefig(fig_fname)

    exit(0)

