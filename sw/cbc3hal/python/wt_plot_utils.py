#!/usr/bin/python

#import sys
import matplotlib.pyplot as plt
import re
import numpy as np

def read_xy( fname, xi, yi, eyi, x16 = True ):

    x = []
    y = []
    ey = []

    dfile = open( fname, 'r' )

    nd=0
    for line in dfile:
        p = re.compile('^(#| |\t)')
	if p.match(line) != None:
	    continue 
	items = line.split(' ')
	nd += 1
	if x16:
	    x.append(float(int(items[xi],0)))
	else:
	    x.append(float(items[xi]))
	if yi != -1:
	    y.append(float(items[yi]))
	if eyi != -1:
	    ey.append(float(items[eyi]))

    return x, y, ey


def plot_xy( fname, label, fmt, xi, yi, eyi, ms = 5, x16 = True ):

    x, y, ey = read_xy( fname, xi, yi, eyi, x16 )
#    plt.plot(x, func(x, *popt), 'g--')
    if len(ey) != 0:
	plt.errorbar(x,y, yerr=ey, fmt=fmt, markersize=ms, label=label)
    else:
	plt.plot(x,y, fmt, markersize=ms)

    return x, y, ey

