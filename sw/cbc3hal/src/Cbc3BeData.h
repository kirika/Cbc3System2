/*
 * Author : Kirika Uchida
 */

#ifndef __CBC3BEDATA_H__
#define __CBC3BEDATA_H__

#include <vector>
#include <map>
#include <stdint.h>
#include <iostream>

namespace cbc3hal{

	/* Event packet format from a single backend board 
	 * Header(1) : BE_ID(0x1FC00000), BeFrimwareType(0x00380000), CbcDataType(0x00040000), NumActiveCbcs(0x0003F000), EventDataSize(0x00000FFF) 
	 * EventData
	 * + BeStatus(1),
	 * + L1ACounter(1), 
	 * + CbcDataPacket(?)
	 * -----------------------------------------------
	 * Event packet size = 1(Header) + EventDataSize
	 */
	enum BeFirmwareTypeId { BeFwId_Cbc3BeFc7, BeFwId_SimCbc3BeFc7 };
	enum CbcDataTypeId { CbcData_Frame=1, CbcData_Hits=0 };
	enum { EventData_HeaderSize = 1, BeEventDataSize=2 };
	enum {
		Offset_EventData_BeStatus=1,
		//		Offset_EventData_BeEventCounter,
		Offset_EventData_BeL1ACounter,
		Offset_EventData_CbcEventData
	};

	/*
	 * Implementation 1 : CbcEventFrameData
	 * Header(1) : BE_ID(0x07F00000) FE_ID(0x000E0000) CBC_ID(0x0001F000) CbcDataSize(0x000003FF)  
	 * + DataTrigFrame(10) : 314bit data + 6bit padding
	 *                      [0-31] TrigData[0-31]
	 *                      [0-7] TrigData[32-39], [8-9]Error, [10-18]PipelineAddress, [19-27]L1ACounter, [28-31]ChannelHitData(4bits)  
	 *                      [0-31] x 7 ChannelHitData(224bits)
	 *                      [0-25] ChannelHitData(26bits) [26-31] paddings '0' (6bits) 
	 * -----------------------------------------------
	 * Cbc data packet size = 1(Header) + CbcDataSize 
	 * CbcDataSize = 10 
	 * data packet size in byte =(1+10)*4=44
	 */
	enum { 
		CbcData_Frame_HeaderSize = 1,
		CbcData_Frame_DataSize   = 10 
	};
	//Bit offsets in DataTrigFrame
	enum {
		BitOffset_CbcDataTrigFrame_TrigData        =   0, 
		BitOffset_CbcDataTrigFrame_Error           =  40, 
		BitOffset_CbcDataTrigFrame_PipelineAddress =  42,
		BitOffset_CbcDataTrigFrame_L1ACounter      =  51,
		BitOffset_CbcDataTrigFrame_ChannelData     =  60,
	};
	//Bit offsets in a cbc trig frame 
	enum {
		BitOffset_CbcTrigFrame_bend  = 24,
		BitOffset_CbcTrigFrame_sof   = 36,
		BitOffset_CbcTrigFrame_or254 = 37,
		BitOffset_CbcTrigFrame_err   = 38
	};

	//Other possible format
	//If we count continuous same bit to compress, the cbc channel data size varies from 9 bits to 9 * 254 bits 
	//2 to 72 words 
	//The format for each 9 bits
	//[0]   : 0 or 1
	//[1-8] : count of the continuous same bit logic, 0 - 254 
	//
	//If we record the channel with hit, the cbc hit data size varies from 0 to 8 * 254 bits 
	//0 to 64 words
	//The format for each 4 bits
	//[0-7] : address of the channel with hit
	//

	class CbcDataPacketDecoder;
	class BeDataDecoder{

		public: 
			BeDataDecoder(bool debug=false);
			~BeDataDecoder(){}
			/*
			 * data_size in byte
			 */
			void SetData( const void *data, int data_size );
			int SetNextEvent();
			void DumpEventBeData( std::ostream &os )const;
			int SetNextCbc();
			void ResetEvent();
			/*
			 * read event data while SetNextEvent() returns true.
			 * read cbc data in the event data while SetNextCbc() returns true through CbcDataPacketDecoder.  
			 */
			int                   EventDataSize()const{ return fEvent[0] & 0xFFF; }
			int                   NumCbcs()const{ return ((fEvent[0] >> 12) & 0x3F); }
			CbcDataTypeId         CbcDataType()const{ return (CbcDataTypeId) ((fEvent[0] >> 18) & 0x1); } 
			BeFirmwareTypeId      BeFirmwareType()const{ return (BeFirmwareTypeId) ((fEvent[0] >> 19) & 0x7); }
			int                   BeId()const{ return ((fEvent[0] >> 22) & 0x7F); } 
			int                   EventPacketSize()const{ return EventDataSize() + EventData_HeaderSize; }
			uint32_t              BeStatus()const{ return fEvent[Offset_EventData_BeStatus]; }
			uint32_t              BeL1ACounter() const{ return (fEvent[Offset_EventData_BeL1ACounter]  & 0xfFFFFFFF ); } //changed from 0x1fffffff to use this function to get TDC information for UA9
			int                   TriggerCounter() const{ return (fEvent[Offset_EventData_BeL1ACounter]>>18 ); }
//			int                   Triggers()    const{ return ((fEvent[Offset_EventData_BeL1ACounter]>>13)&31 ); }			
//			int                   Tdc1()           const{ if (Triggers()&4) return ((fEvent[Offset_EventData_BeL1ACounter]>>8)&31 ); else return -1; }			
//			int                   Tdc0()           const{ if (Triggers()&2) return ((fEvent[Offset_EventData_BeL1ACounter]>>3)&31 ); else return -1; }				
//			int                   Ua9count()       const{ 
//			  if (Triggers()&1) {
//			    if (Triggers()&6) return ((fEvent[Offset_EventData_BeL1ACounter]>>0)&7 );  else return (((fEvent[Offset_EventData_BeL1ACounter]>>0)&0x1fff)|0x00000000 );
//			  } else return -1;
//			}
//			int                   Stretch()           const{ if (!(Triggers()&7)) return ((fEvent[Offset_EventData_BeL1ACounter]>>0)&7 ); else return -1; }				
                        int                   UA9_count()    const{ return ((fEvent[Offset_EventData_BeL1ACounter]>>11) ); }			
                        int                   UA9_count_fix()    const{ return ((fEvent[Offset_EventData_BeL1ACounter]>>11) & 0x03ffff) | (fEvent[Offset_EventData_BeL1ACounter] & 0x3 ) << 18; }			
			int                   Triggers()    const{ return ((fEvent[Offset_EventData_BeL1ACounter]>>8)&7 ); }			
			// ua9_trig & or254(1) & or254(0)
			int                   TDC()    const{ return ((fEvent[Offset_EventData_BeL1ACounter]>>0)&255 ); }			
			int                   TDC_fix()    const{ return (fEvent[Offset_EventData_BeL1ACounter]& 0xf8) | ((fEvent[Offset_EventData_BeL1ACounter]>>29) & 7 ); }			
			  
			const CbcDataPacketDecoder * GetCbcDataPacketDecoder()const{ return fCbcDataPacketDecoder; } 
			const void*                 GetIncompleteEventBuffer(int &buffer_size)const;
			const uint32_t*              GetEventPacketPointer()const{ return fEvent; }
			const uint32_t*              GetCbcEventPacketPointer()const{ return fCbcEvent; }

		protected:
			bool fDebug;
			const uint32_t *fData;
			const uint32_t *fEvent;
			const uint32_t *fCbcEvent;
			int fDataSize;
			int fEventCount;
			int fCbcCount;
			int fDataSizeToDecode;
			CbcDataTypeId fCbcDataType;
			std::map<CbcDataTypeId, CbcDataPacketDecoder *> fCbcDataPacketDecoderMap;
			CbcDataPacketDecoder *fCbcDataPacketDecoder;
	};

	class CbcDataPacketDecoder{
		public:
			CbcDataPacketDecoder(): fData(0){;}
			virtual ~CbcDataPacketDecoder(){;}
			void SetData( const uint32_t *data, bool debug=false ){ fData = data; if(debug) DumpData(std::cerr); } 
			virtual void DumpData( std::ostream &os )const=0;
			virtual int                   DataSize()const{ return (fData[0] & 0xFFF); } 
			virtual int                   Id()const{ return (fData[0] & 0x1F000) >> 12; } 
			virtual int                   FeId()const{ return (fData[0] & 0xE0000) >> 17; } 
			virtual int                   BeId()const{ return (fData[0] & 0xF00000) >> 20; } 
			virtual int                   PacketSize()const = 0;
			virtual std::vector<unsigned> DataFrame()const{ return std::vector<unsigned>(); }    
			virtual bool                  Error( int i )const=0;
			virtual int                   PipelineAddress()const=0;
			virtual int                   L1ACounter()const=0;
			virtual bool                  ChannelData( int channel_id )const=0;
			virtual std::vector<unsigned> HitChannels()const=0;
			virtual std::vector<unsigned> StubPositions()const=0;
			virtual std::vector<unsigned> StubBends()const=0;
			virtual bool                  TrigSof()const=0; 
			virtual bool                  TrigOr254()const=0; 
			virtual bool                  TrigErr()const=0; 
		protected:
			const uint32_t *fData;
	};
	class CbcFrameDataPacketDecoder : public CbcDataPacketDecoder{
		public:
			CbcFrameDataPacketDecoder(){}
			virtual ~CbcFrameDataPacketDecoder(){;}
			void DumpData( std::ostream &os )const;
			int                   PacketSize()const{ return DataSize() + CbcData_Frame_HeaderSize; } 
			std::vector<unsigned> DataFrame()const;
			bool                  Error( int i )const;
			int                   PipelineAddress()const;
			int                   L1ACounter()const;
			bool                  ChannelData( int channel_id )const;
			std::vector<unsigned> HitChannels()const;
			virtual std::vector<unsigned> StubPositions()const;
			virtual std::vector<unsigned> StubBends()const;
			bool                  TrigSof()const; 
			bool                  TrigOr254()const; 
			bool                  TrigErr()const; 
	};

	class CbcDataPacketEncoder;
	class BeEventDataPacketEncoder{
		public:
			BeEventDataPacketEncoder(); 
			~BeEventDataPacketEncoder(){}
			void Configure( BeFirmwareTypeId befwtype, CbcDataTypeId id, int be_id, int ncbcs ); 
			int HeaderSize()const{ return fHeaderSize; } 
			int MaxDataSize()const{ return fMaxDataSize; }
			int DataSize()const{ return fDataSize; }
			int PacketSize()const{ return fHeaderSize+fDataSize; }
			void ResetBuffer();
			void *GetBuffer()const{ return fBuffer; }
			void SetBeStatus( uint32_t value ){  fBuffer[Offset_EventData_BeStatus] = value; }
			//			void SetBeEventCounter( int value ){ fBuffer[Offset_EventData_BeEventCounter] = value; } 
			void SetBeL1ACounter( int value ){ fBuffer[Offset_EventData_BeL1ACounter] = value; } 
			CbcDataPacketEncoder *GetCbcDataPacketEncoder()const{ return fCbcDataPacketEncoder; }
			bool SetCbcDataPacket(); 
			void SetHeader(){ fBuffer[0] = ( fBeId << 21 ) | ( fBeFirmwareType << 18 ) | ( fCbcDataType << 17 ) | ( fNumCbcs << 12 ) | ( fDataSize & 0xFFF ); } 

			CbcDataTypeId CbcDataType()const{ return fCbcDataType; }
		private:
			uint32_t       *fBuffer;
			int            fHeaderSize;
			int            fMaxDataSize;
			int            fDataSize;
			int            fNextCbcDataOffset;
			int            fCbcCount;
			BeFirmwareTypeId fBeFirmwareType;
			CbcDataTypeId  fCbcDataType;
			int            fBeId;
			int            fNumCbcs;
			CbcDataPacketEncoder *fCbcDataPacketEncoder;
			std::map<CbcDataTypeId, CbcDataPacketEncoder *> fCbcDataPacketEncoderMap;
	};

	class CbcDataPacketEncoder{
		public:
			CbcDataPacketEncoder():fBuffer(0), fHeaderSize(0), fMaxDataSize(0), fDataSize(0){;}
			virtual ~CbcDataPacketEncoder(){;}
			virtual void Configure();
			virtual void ResetBuffer();
			virtual uint32_t *GetBuffer()const{ return fBuffer; }
			virtual int HeaderSize()const{ return fHeaderSize; }
			virtual int MaxDataSize()const{ return fMaxDataSize; }
			virtual int DataSize()const{ return fDataSize; }
			virtual int PacketSize()const{ return fHeaderSize+fDataSize; }
			virtual void SetError( bool value0, bool value1 )=0;
			virtual void SetPipelineAddress( int value )=0;
			virtual void SetL1ACounter( int value )=0;
			virtual void SetChannelData( int channel_id, bool value )=0;
			virtual void SetTrigData( std::vector<int>& pos, std::vector<int>& bend, bool sof, bool or254, bool err )=0;
			virtual void SetHeader( int be_id, int fe_id, int cbc_id ){ 
				fBuffer[0] = ( be_id << 20 ) | ( fe_id << 17 )  | ( cbc_id << 12 ) | fDataSize;
			}
		protected:
			uint32_t *fBuffer;
			int       fHeaderSize;
			int       fMaxDataSize;
			int       fDataSize;
	};
	class CbcFrameDataPacketEncoder : public CbcDataPacketEncoder{
		public:
			CbcFrameDataPacketEncoder();
			virtual ~CbcFrameDataPacketEncoder(){}
			virtual void Configure();
			virtual void SetError( bool value0, bool value1 );
			virtual void SetPipelineAddress( int value );
			virtual void SetL1ACounter( int value );
			virtual void SetChannelData( int channel_id, bool value );
			virtual void SetTrigData( std::vector<int>& pos, std::vector<int>& bend, bool sof, bool or254, bool err );
	};
	
	void raw_data_dump_data( uint32_t *buffer, int n, std::ostream &os );
	void raw_data_get_slvs6(const uint32_t *buffer, int *slvs6, int n );
	int  raw_data_find_frame_header(int *slvs6, int n, int offset=0);
	void raw_data_get_error_bits(int *slvs6, int hp, bool &e1, bool &e2);
	int  raw_data_get_pipeline_address(int *slvs6, int hp);
	int  raw_data_get_l1counter(int *slvs6, int hp);
	bool raw_data_has_hit(int *slvs6, int ph, int ch );
	int  raw_data_find_stubs(const uint32_t *buffer, int n, std::vector<int> &saddr, std::vector<int> &sbend, int offset=0);
	void raw_data_dump_chdata( int *slvs6, int ph, std::ostream &os );
	bool raw_data_dump_l1_data_frame(int *slvs6, int n, std::ostream &os);

}

#endif

