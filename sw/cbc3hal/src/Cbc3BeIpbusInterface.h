#ifndef __CBC3BEIPBUSINTERFACE_H__
#define __CBC3BEIPBUSINTERFACE_H__

#include "CbcI2cRegIpbusInterface.h"
#include "Cbc3BeInterface.h"
#include <string>

namespace uhal{
    class HwInterface;
}

namespace cbc3hal{

    class Cbc3BeIpbusInterface : public Cbc3BeInterface{

	public:
	    enum { DEBUG_CBC3BEIPBUSINTERFACE_BIT=3 };

	public:
	    Cbc3BeIpbusInterface( const std::string &board_name, bool CBC3_0 = true, int debug=0 ):Cbc3BeInterface( board_name, CBC3_0, debug ), fBoard(0){}
	    virtual ~Cbc3BeIpbusInterface();
	    virtual void Initialize( const std::string& connection_file, const std::string& id );
	    virtual void WriteBeBoardConfig( const std::string &node, uint32_t value );
	    virtual void WriteBeBoardConfig( std::map<std::string, uint32_t>&regmap );
	    virtual void WriteBeBoardConfig( const std::string &fname );
	    virtual void ReadBeBoardConfig( const std::string &fname, std::map<std::string,uint32_t> &regmap );
	    virtual unsigned ReadBeBoardRegister( std::string node );
	    virtual void ReadBeBoardRegisters( std::vector<std::string> &regnodes, std::vector<uint32_t> &values );
	    virtual bool CbcI2cBusInit(); 

	    virtual const CBCI2C_NAMES & getCbcI2cRegNames()const{ return cbci2creg_formatter::getNames(); }
	    virtual CBCI2C_TYPE_DATA_MAP readCbcAllI2cRegsByType( unsigned cbc_id=1 );
	    virtual bool rwCbcI2cReg(unsigned r, unsigned w, unsigned page, unsigned addr, unsigned &value, unsigned cbc_id=1 );
	    virtual bool rwCbcI2cReg(struct cbc_i2c_command &command );
	    virtual bool rwCbcI2cRegs(const std::vector<struct cbc_i2c_command> &commands );
	    /* cbc_id = 0 is not supported for this function */
	    virtual bool rwCbcI2cRegs(unsigned r, unsigned w, const std::string &filename, unsigned cbc_id=1 );
	    /* cbc_id = 0 is not supported for this function */
	    virtual bool rwCbcI2cRegsByName(unsigned r, unsigned w, const std::string &name, unsigned &value, unsigned cbc_id=1 );
	    virtual bool rwCbcI2cRegsByType(unsigned r, unsigned w, CBCI2C_TYPE_DATA &type_data, unsigned cbc_id=1 );
	    virtual bool rwCbcI2cRegsByType(unsigned r, unsigned w, std::map<unsigned, CBCI2C_TYPE_DATA> &cbc_id_type_data );
	    virtual bool writeAllCbcI2cRegsByType( const std::string &type, CBCI2C_ITEM_DATA_MAP &item_data );
	    /* cbc_id = 0 is not supported for rwCbcI2c_Offset(s) */
	    virtual bool rwCbcI2c_Offset( unsigned r, unsigned w, unsigned ch, unsigned &offset, unsigned cbc_id=1 );
	    virtual bool rwCbcI2c_Offsets( unsigned r, unsigned w, std::vector<unsigned> &offsets, unsigned cbc_id=1 );
	    virtual bool rwCbcI2c_Offsets( unsigned r, unsigned w, std::map<unsigned,unsigned> &chofst_map, unsigned cbc_id=1 );
	    virtual std::vector<CBCI2C_REPLY> rwCbcI2c_reg( unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value=0, unsigned cbc_id=1 ); 
	    /* fe_id = 0 is not supported for this function */
	    virtual std::vector<CBCI2C_REPLY> rwCbcI2c_regs( const std::vector<uint32_t> &commands, int nreplies = -1, unsigned fe_id=1 ); 
	    /* cbc_id = 0 is not supported for this function */
	    virtual std::vector<CBCI2C_REPLY> rwCbcI2c_regs( unsigned r, unsigned w, const std::string &filename, unsigned cbc_id=1 ); 

	    uhal::HwInterface * GetHwInterface()const { return fBoard; }

	public:
	    uhal::HwInterface *fBoard;
    };
}
#endif

