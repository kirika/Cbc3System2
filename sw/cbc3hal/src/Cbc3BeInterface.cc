#include "Cbc3BeInterface.h"
#include "Cbc3BeData.h"
#include <fstream>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <sys/time.h>
#include "analysis_utils.h"
#include <TH1F.h>
#include <iomanip>
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace cbc3hal;

GROUP_CHANNELSET_MAP Cbc3BeInterface::fGroupChannelSetMap(InitGroupChannelSetMap());
const std::set<unsigned>   Cbc3BeInterface::fInvalidChannelSet = std::set<unsigned>();

void Cbc3BeInterface::ConfigureBeBoard( const std::string &be_config_fname, int fci_delay ){

    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
	cout << "*** Cbc3BeInterface::ConfigureBeBoard() ***" << endl; 

    WriteBeBoardConfig( be_config_fname );

    CbcReset();
    CbcI2cBusInit();

    CBCI2C_ITEM_DATA_MAP item_data;
    item_data["fci_delay"] = fci_delay;
    if( !writeAllCbcI2cRegsByType("FciAndError", item_data) ) return;

    usleep(1000);
    SendFastSignal( "reset" ); 

    char cnode[256];
    int fe_id(1);
    sprintf(cnode, "cs_cnfg.io.cbc_data.timing_tuning.fe%d.idelay_offset", fe_id); 
    //	    write_ipbus( fBoard, cnode, ofst ); // for cable test.
    unsigned ofst = ReadBeBoardRegister( cnode );
    cout << "TEST : idelay offset in the configuration file is used : " << ofst << endl;
    /*
    SendFastSignal( "reset" ); 

    unsigned value(0);
    unsigned ncbc = GetNCbcs();
    for( unsigned cbc_id=1; cbc_id <= ncbc; cbc_id++ ){
	if( IsCbcActive(cbc_id) == false ) continue; 
	rwCbcI2cRegsByName( 1, 0, "FciAndError.", value, cbc_id );
    }
    */
    TuneDataClockTiming(true);

    return;
}
bool Cbc3BeInterface::ConfigureCbcs( const std::string &cbci2creg_filelist_fname )
{
    ifstream fs( cbci2creg_filelist_fname.c_str() );
    std::size_t found = cbci2creg_filelist_fname.rfind("/");
    string dir0("");
    if( found != std::string::npos ){
	dir0 = cbci2creg_filelist_fname.substr(0, found); 
    }
    char line[256];
    unsigned be_id, cbc_id;
    if( fs.is_open() ){
	while( !fs.eof() ){
	    fs.getline( line, 256 );
	    if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ) continue;
	    istringstream iss( line );
	    string be_id_str, cbc_id_str, fname;
	    iss >> be_id_str >> cbc_id_str;
	    be_id = strtol( be_id_str.c_str(), 0, 0 );
	    if( be_id == GetBeId() ){
		cbc_id = strtol( cbc_id_str.c_str(), 0, 0 );
		while( !iss.eof() ){
		    iss >> fname; 
		    if( dir0 != "" && fname[0] != '/' ) fname = dir0 + "/" + fname;
		    fCbcConfigFiles[cbc_id].push_back(fname);
		}
	    }
	}
    }
    bool good(true);
    unsigned ncbc = GetNCbcs();
    for( unsigned cbc_id=1; cbc_id <= ncbc; cbc_id++ ){
	if( IsCbcActive(cbc_id) == false ) continue; 
	if( fCbcConfigFiles.find(cbc_id) == fCbcConfigFiles.end() ){
	    cerr << "Cbc3BeInterface::ConfigureCbcs() CBC " << cbc_id << " I2C register configuration files is not found." << endl;
	    continue;
	}
	std::vector<std::string> &files = fCbcConfigFiles[cbc_id];
	for( unsigned i=0; i < files.size(); i++){
	    cout << "Cbc3BeInterface::ConfigureCbcs() writing CBC I2C registers with " << files.at(i) << endl;
	    good = (good & rwCbcI2cRegs(1,1,files.at(i), cbc_id ) );
	}
    }
    if( !good ) cout << "Cbc3BeInterface::ConfigureCbcs() failed." << endl;
    return good;
}
void Cbc3BeInterface::ReadAllCbcConfig( CBC_ID cbc_id ) 
{
    vector<struct cbc_i2c_command> commands;
    for( unsigned i = 0; i <= 0x58; i++){
       if( i == 0x08 || i == 0x0a || i == 0x19 || i == 0x1a || i == 0x1e || i == 0x1F ) continue;  	
       struct cbc_i2c_command com = { cbc_id, 0, 1, 1, i, 0 }; 
       commands.push_back(com);

    }
    for( unsigned i = 1; i <= 0xFF; i++){
       struct cbc_i2c_command com = { cbc_id, 0, 1, 2, i, 0 }; 
       commands.push_back(com);
    }
    rwCbcI2cRegs(commands);
}
void Cbc3BeInterface::ReadCbcConfig( unsigned cbc_id, std::string fname ) 
{
    unsigned ncbc = GetNCbcs();
    for( unsigned id=1; id <= ncbc; id++ ){
	if( cbc_id != 0 && id != cbc_id ) continue;
	if( IsCbcActive(id) == false ) continue; 

	if( !rwCbcI2cRegs(1,0,fname,id ) ) cerr << "CBC I2C register reading failed." << endl;
    }
}
int Cbc3BeInterface::DisableGroupOffsets( unsigned group, unsigned cbc_id)
{
    //reset OFFSET values for this group to 0xFF
    for( unsigned cid = 1; cid <= GetNCbcs(); cid++ ){
	if( cbc_id != 0 && cid != cbc_id ) continue; 
	if( !IsCbcActive(cid) ) continue;

	map<unsigned,unsigned> offset_map;

	const set<unsigned> &channel_set = GetChannelSet(group);
	if(channel_set == fInvalidChannelSet ) return -1;

	set<unsigned>::const_iterator it_chset = channel_set.begin();
	for(;it_chset != channel_set.end(); it_chset++){
	    unsigned ch = *it_chset;
	    offset_map[ch] = 0xFF;
	}
	rwCbcI2c_Offsets(1,1,offset_map,cid);
    }
    return 0;
}
int Cbc3BeInterface::ActivateGroupOffsets( unsigned group, const std::map<CHANNEL_ID, unsigned> &offsets_map, unsigned cbc_id )
{
    const set<unsigned> &channel_set = GetChannelSet(group);
    if(channel_set == fInvalidChannelSet ) return -1;

    map<unsigned,unsigned> ofst_map;
    set<unsigned>::const_iterator it_chset = channel_set.begin();
    for(; it_chset != channel_set.end(); it_chset++){
	unsigned ch = *it_chset;
	if(offsets_map.find(ch)!=offsets_map.end()){
	    ofst_map[ch] = offsets_map.find(ch)->second; 
	}
    }

    if( !rwCbcI2c_Offsets(1,1, ofst_map, cbc_id) ) return -1;
    return 0;
}
int Cbc3BeInterface::ActivateGroupOffsets( unsigned group, const std::map<unsigned, std::vector<unsigned> > &offsets0, unsigned cbc_id )
{
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cout << "Cbc3BeInterface::ActivateGroupOffsets() group = " << group << endl;

    for( unsigned cid = 1; cid <= GetNCbcs(); cid++ ){
	if( cbc_id != 0 && cid != cbc_id ) continue; 
	if( !IsCbcActive(cid) ) continue;

	map<unsigned, vector<unsigned> >::const_iterator it_cbc = offsets0.find( cid );
	if( it_cbc == offsets0.end() ){
	    cerr << "Offsets for the CBC ID = " << cid << " are not found." << endl;
	    return -1;
	}
	const vector<unsigned> offsets = it_cbc->second;

	const set<unsigned> &channel_set = GetChannelSet(group);
	if(channel_set == fInvalidChannelSet ) return -1;

	map<unsigned,unsigned> offset_map;
	set<unsigned>::const_iterator it_chset = channel_set.begin();
	for(; it_chset != channel_set.end(); it_chset++){
	    unsigned ch = *it_chset;
	    offset_map[ch] = offsets.at(ch-1); 
	}
	if( !rwCbcI2c_Offsets(1,1,offset_map,cid) ) return -1;
    }
    return 0;
}
int Cbc3BeInterface::ActivateGroupOffsets( unsigned group, const std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > &offsets_map, CBC_ID cbc_id )
{
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cout << "Cbc3BeInterface::ActivateGroupOffsets() group = " << group << endl;

    map<CBC_ID, map<CHANNEL_ID, unsigned> >::const_iterator it = offsets_map.begin();
    const set<unsigned> &channel_set = GetChannelSet(group);
    if(channel_set == fInvalidChannelSet ) return -1;

    for(; it != offsets_map.end(); it++){
	if( cbc_id != 0 && it->first != cbc_id ) continue;
	map<unsigned,unsigned> ofst_map;
	set<unsigned>::const_iterator it_chset = channel_set.begin();
	for(; it_chset != channel_set.end(); it_chset++){
	    unsigned ch = *it_chset;
	    if(it->second.find(ch) != it->second.end()){
		ofst_map[ch] = it->second.find(ch)->second; 
	    }
	}
	if( !rwCbcI2c_Offsets(1,1, ofst_map, it->first) ) return -1;
    }
    return 0;
}
//offsets are all set to 0xff in the beginning if offsets_map is not provided or init_offsets are set to 1 
//
//Group by group,
//VCTH value is searched for offset values provided, if it is not provided, the values on the chip are read in the beginning and those are used. 
//
//offsests are all set to 0xff in the end in any case.
int Cbc3BeInterface::FindVcth( int sample_count, double occ, const map<CBC_ID, vector<unsigned> > *offsets_map, bool init_offsets ){

    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cout << "Cbc3BeInterface::FindVcth() begin" << endl;

    WriteBeBoardConfig( "cs_cnfg.fcm.fcg.Ncycle", sample_count );
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
       cout << "Cbc3BeInterface::FindVcth() sample count    = " << std::dec << sample_count << endl;
    }

    //the map to store vcth information is initialized
    map<unsigned, unsigned> vcth_mean;
    for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	if( !IsCbcActive(cbc_id) ) continue;
	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
	    cout << "VCTH map is initialized for " << "CBC ID = " << cbc_id << endl;
	}
	if(occ == 0 ) vcth_mean[cbc_id] = 1023;
	else if(occ == 1. ) vcth_mean[cbc_id] = 0;
	else vcth_mean[cbc_id] = 0;
    }

    //read offset information and set all the offsets to 0xFF for the case offsets_map == 0.
    //If offsets_map is given, the offsets are initialized to 0xFF if init_offsets == 1 
    map<unsigned, vector<unsigned> > offsets0_map;
    if( !offsets_map ){ 
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    rwCbcI2c_Offsets(1,0,offsets0_map[cbc_id],cbc_id);
	}
	offsets_map = &offsets0_map;
	unsigned offset(0xFF);
	rwCbcI2c_Offset(1,1,0,offset,0);
    }
    else if(init_offsets){
	unsigned offset(0xFF);
	rwCbcI2c_Offset(1,1,0,offset,0);
    }

    /**** VCTH bitonic scan is done each group ****/
    //set the offsets to this group
    //bitonic scan
    //set the offsets to this group to 0xff
    GROUP_CHANNELSET_MAP::const_iterator it_group = fGroupChannelSetMap.begin();
    unsigned nchannels(0);
    for(; it_group != fGroupChannelSetMap.end(); it_group++){
	GROUP_ID group = it_group->first;
	nchannels += it_group->second.size();
    //for( GROUP_ID group=0; group<fGroupChannelSetMap.size(); group++){

	//setting the test pulse group
	CBCI2C_ITEM_DATA_MAP item_data;
	item_data["group"] = group;
	if( !writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) ) return -1;

	//initialize VCTH and OFFSET values for this group
	ActivateGroupOffsets(group, *offsets_map, 0 );

	//VCTH bisection scan
	map<CBC_ID,unsigned> g_vcth;
	if( ScanVcthBisection( group, g_vcth, sample_count, occ ) != 0 ){
	    cerr << "VCTH finding failed." << endl;
	    return -1;
	}
	//get the tuned VCTH for this group
	map<unsigned,unsigned>::iterator it_vcth = vcth_mean.begin();
	for(; it_vcth != vcth_mean.end(); it_vcth++){
	    unsigned cbc_id = it_vcth->first;
	    if(occ == 0 ){
		//find VCTH gives occupancy = 0 
		if( it_vcth->second > g_vcth[cbc_id] ) it_vcth->second = g_vcth[cbc_id];
	    }
	    else if( occ == 1. ){
		//find VCTH gives occupancy = 1 
		if( it_vcth->second < g_vcth[cbc_id] ) it_vcth->second = g_vcth[cbc_id];
	    }
	    else{
		//average VCTH values for groups gives occupancy other than 0 nor 1 
		it_vcth->second += (g_vcth[cbc_id] * it_group->second.size());
	    }
	 if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
	     cout << std::dec 
		 << "Group " << group 
		 << " # of channels = " << it_group->second.size()
		 << " : (CBC ID, VCTH) = (" << cbc_id << ", " << std::dec << g_vcth[cbc_id] << ")" << endl;
	}
	//reset OFFSET values for this group to 0xFF
	DisableGroupOffsets(group);
    }

    //Set tuned VCTHs
    map<unsigned,unsigned>::iterator it_vcth = vcth_mean.begin();
    for(; it_vcth != vcth_mean.end(); it_vcth++){
	unsigned cbc_id = it_vcth->first;
	if(occ != 0 && occ != 1. ){
	    float tmp = it_vcth->second;
	    tmp /= nchannels; 
	    it_vcth->second = (tmp + 0.5);
	}
	unsigned vcth_mean = it_vcth->second;

	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) )cout << "Mean over " << nchannels 
		<< "  : (CBC ID, VCTH) = (" << cbc_id << ", " << std::dec << vcth_mean << ")" << endl;
//	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) )cout << "Mean   : (CBC ID, VCTH) = (" << cbc_id << ", " << std::dec << vcth_mean << ")" << endl;

	if(!rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth_mean, cbc_id) ) return -1;
    }

    return 0;
}

// For each CBC,
//     Unmask all the channels of this CBC
//     Bitonic scan is done taking data and VCTH is set to the tuned value in the end.
//     Mask this CBC
// Masks in all CBCs are set to the initial values
int Cbc3BeInterface::ScanVcthBisection( GROUP_ID group, std::map<CBC_ID, unsigned> &g_vcth, int sample_count, double occ )
{
    const set<unsigned> &channel_set = GetChannelSet(group);
    if(channel_set == fInvalidChannelSet ) return -1;

    // Set threshold hit count.
    int nhits_thr(1);
    if( occ == 1.) nhits_thr = sample_count * channel_set.size() - 1;
    else if( occ != 0 ) nhits_thr = (int) (sample_count * channel_set.size() * occ);

    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
	cout << "Cbc3BeInterface::ScanVcthBisection()"
	    << " sample count = " << std::dec << sample_count 
	    << " occupancy = " << occ 
	    << " threshold hit count = " << nhits_thr << endl;


    /*
    cout << "readout CBC I2C REGISTERS begin." << endl;
    print_regcnf( fCbcI2cRegReplies, cout, 1 );
    cout << "readout CBC I2C REGISTERS end." << endl;
    */
    //set the run configuration and initialize the run at the firmware.

    InitializeRun();

    //set the data acquisition parameters at the software side.
    //set read data size and reading parameters
    int event_data_size = 3 + 11 * GetNActiveCbcs();
    int sample_size = sample_count * event_data_size;
    if( sample_size > 10000 ) sample_size = 0; 
    MinNumWordToWait( sample_size );
    DataReadyFlagPollingInterval_usec(0);
    if(fDataBuffer==0)
	fDataBuffer = new uint32_t[fDataBufferWordSize];
    BeDataDecoder be_data_decoder(fDebug);

    map<CBC_ID, CbcParamTuningItem> vcth_data;
    map<CBC_ID, map<unsigned,unsigned> > masks;

    bool only_1cbc = (GetNActiveCbcs() == 1);
    if( !only_1cbc ){
	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
	    cout << "Masking other CBC channels" << endl;
	//Read masks
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    ClearCbcI2cRegReplies();
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 0, 1, 1, addr, 0 };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	    const CBCI2C_REGS & regs = GetCbcI2cRegReplies(); 
	    CBCI2C_REGS::const_iterator it = regs.begin();
	    for(; it != regs.end(); it++){
		CBCI2C_REPLY reply = it->second;
		unsigned addr = get_addr(reply);
		if( get_rw_flag(reply) == 1 && get_page(reply) == 1 && 0x20 <= addr && addr < 0x40 ){ 
		    masks[cbc_id][addr] = get_val(reply);
		}
	    }
	}
	//Mask all the channels of all CBCs
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    unsigned value = 0x00;
	    rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
    }

    for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){

	if( !IsCbcActive(cbc_id) ) continue;

	if( !only_1cbc ){
	    //Unmask all the channels of this CBC
	    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
		cout << "Un masking this CBC channels" << endl;
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	}

	bool stop(false);
	//if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cout << "Cbc3BeInterface::ScanVcthBisection() scan loop" << endl;

	//Initializing vcth tuning data.
	vcth_data[cbc_id] = CbcParamTuningItem(VcthNbits, nhits_thr, false); 
	if( fDebug & ( 1 << DEBUG_PARAM_TUNING_BIT ) ) vcth_data[cbc_id].Debug();
	unsigned vcth = vcth_data[cbc_id].GetValue();
	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cerr << "VCTH is set to " << vcth << endl;
	if(!rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth, cbc_id) ) return -1;

	
	//VCTH scan loop
	while( stop == false ){

	    //reset hit count
	    map<CBC_ID,CbcParamTuningItem>::iterator it_vcth_data = vcth_data.begin();
	    for(; it_vcth_data != vcth_data.end(); it_vcth_data++){
		it_vcth_data->second.ClearHits();
	    }

	    //start sending triggers
	    StartFastSignalGenerator();

	    //reading data
	    int ievt(0);
	    struct timeval t0, t, dt;
	    gettimeofday(&t0,0);
	    int leftBufferSize(0);
	    while( ievt < sample_count ){

		if( WaitData( 1 ) ){
		    //if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  )cout << "Cbc3BeInterface::ScanVcthBisection() # of next read = " << std::dec << GetNumNextDataWords() << endl; 

		    int nwords = leftBufferSize/4; 
		    uint32_t *Data = &fDataBuffer[nwords];
		    nwords += ReadData( Data, fDataBufferWordSize-nwords );

		    be_data_decoder.SetData( fDataBuffer, nwords * 4);
		    while( be_data_decoder.SetNextEvent() ){ 

			while( be_data_decoder.SetNextCbc() ){

			    const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			    //cbc_data_packet_decoder->DumpData(cout);
			    unsigned d_cbc_id = cbc_data_packet_decoder->Id(); 
			    if( d_cbc_id == cbc_id ){
				vector<unsigned> hit_channel_list = cbc_data_packet_decoder->HitChannels();
				//if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  )cout << "Cbc3BeInterface::ScanVcthBisection() # of hits = " << hit_channel_list.size() << " for CBC = " << cbc_id << endl; 
				//get hit count for the event.
				vcth_data[cbc_id].AddHits( hit_channel_list.size() );
				//if(ievt %1000 == 0 ) cout << ievt << " " << vcth_data[cbc_id].GetHits() << endl;
			    }
			}
			ievt++;
		    }
		    be_data_decoder.GetIncompleteEventBuffer(leftBufferSize);
		    memcpy( fDataBuffer, &fDataBuffer[nwords*4 - leftBufferSize], leftBufferSize );
		}
		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);
		if( dt.tv_sec > 10 ){
		    cerr << "Cbc3BeInterface::ScanVcthBisection() timeout at # of event = " << ievt << endl; 
		    return -1;
		}	
	    }

	    bool tuned(false);

	    //if( fDebug & ( 1 << DEBUG_PARAM_TUNING_BIT ) ) cout << "The parameters are updated for CBC ID = " << cbc_id << endl;
	    tuned = vcth_data[cbc_id].UpdateParam();

	    //set updated vcth values.
	    //CBCI2C_ITEM_DATA_MAP item_data;
	    unsigned vcth = vcth_data[cbc_id].GetValue();
	    if( tuned && fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
		cout << "Cbc3BeInterface::ScanVcthBisection() "
		    << "CBC ID = " << cbc_id << " | tuned VCTH value = " << std::dec << vcth << " (0x" << setfill('0') << std::right << std::hex << vcth << ")" << endl; 
	    }
	    //item_data["vcth"] = vcth;
	    if(!rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth, cbc_id) ) return -1;
	    g_vcth[cbc_id] = vcth;

	    if(tuned) stop = true; 
	}
	if( !only_1cbc ){
	    //Mask this CBC
	    unsigned value=0x00;
	    rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
    }
    if( !only_1cbc ){
	//Masks in all CBCs are set to the initial values
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	}
    }

    return 0;
}
int Cbc3BeInterface::ScanVcth( GROUP_ID group, int sample_count, CbcHistList<TH1F> &hlist, bool desc, int vcth_min, int vcth_max )
{
    double occ(0);
    if(desc) occ = 1.;
    map<CBC_ID,unsigned> g_vcth;
    if( ( !desc & (vcth_min < 0) ) || (desc & (vcth_max < 0) ) ){
       if( ScanVcthBisection( group, g_vcth, sample_count, occ ) != 0 ) return -1;
    }
    else{
	CBCI2C_ITEM_DATA_MAP item_data;
	if( !desc ){
	    item_data["vcth"] = vcth_min;
	}
	else{
	    item_data["vcth"] = vcth_max;
	}
	if(!writeAllCbcI2cRegsByType("VCTH", item_data) ) return -1;
	InitializeRun();
    }

    /*
    for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	if( !IsCbcActive(cbc_id) ) continue;
	g_vcth[cbc_id] = 1023;
//	CBCI2C_ITEM_DATA_MAP item_data;
	unsigned data(0);
	if(!rwCbcI2cRegsByName(1,0,"VCTH.vcth", data, cbc_id) ) return -1;
	if( data < vcth[cbc_id] ) vcth[cbc_id] = data; 
    }
    */
    map<CBC_ID, map<unsigned,unsigned> > masks;
    bool only_1cbc = (GetNActiveCbcs() == 1);
    if( !only_1cbc ){
	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
	    cout << "Masking other CBC channels" << endl;
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    ClearCbcI2cRegReplies();
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 0, 1, 1, addr, 0 };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	    const CBCI2C_REGS & regs = GetCbcI2cRegReplies(); 
	    CBCI2C_REGS::const_iterator it = regs.begin();
	    for(; it != regs.end(); it++){
		CBCI2C_REPLY reply = it->second;
		unsigned addr = get_addr(reply);
		if( get_rw_flag(reply) == 1 && get_page(reply) == 1 && 0x20 <= addr && addr < 0x40 ){ 
		    masks[cbc_id][addr] = get_val(reply);
		}
	    }
	}
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    unsigned value = 0x00;
	    rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
    }

//    cout << "VCTH SCAN FOR GROUP " << std::dec << group << " FROM VCTH = " << vcth << endl;

    const set<unsigned> &channel_set = GetChannelSet(group);
    if(channel_set == fInvalidChannelSet ) return -1;

    if(fDataBuffer==0)
	fDataBuffer = new uint32_t[66000];
    BeDataDecoder be_data_decoder(fDebug);

    int nbins(1024);
    float min(-0.5), max(1023.5);
    char hname[256];
    char htype[256];

    for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){

	if( !IsCbcActive(cbc_id) ) continue;

	if( !only_1cbc ){
	    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) 
		cout << "Un masking other CBC channels" << endl;
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	}
	bool stop(false);
	int be_id(0);
	int hit_sum(0);
	int channel_sum = channel_set.size();

	while( stop == false ){
	    hit_sum = 0;
	    //set vcth
	    CBCI2C_ITEM_DATA_MAP item_data;
	    item_data["vcth"] = g_vcth[cbc_id];
	    if(!writeAllCbcI2cRegsByType("VCTH", item_data) ) return -1;
	    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
		cout << "Cbc3BeInterface::ScanVcth() "
		    << "VCTH is set to " << g_vcth[cbc_id] << endl;
	    }
	    //start sending triggers
	    StartFastSignalGenerator();
	    //cout << "fast signal generator started." << endl;
	    //reading data
	    struct timeval t0, t, dt;
	    gettimeofday(&t0,0);
	    //int nth_read(0);
	    if( WaitData( 0 ) ){
		int nwords = ReadData( fDataBuffer );
		//cout << ++nth_read << " th read.  # of read words = " << nwords << endl;
		be_data_decoder.SetData( fDataBuffer, nwords * 4 );
		while( be_data_decoder.SetNextEvent() ){ 
		    if(be_id == 0 ) be_id = be_data_decoder.BeId();
		    /*
		       if( hmap.find(hname) == hmap.end() ){
		       hmap[hname] = new TH1F( hname, ";VCTH;count", nbins, min, max ); 
		       hmap[hname]->Sumw2();
		       }
		       hmap[hname]->Fill(vcth);
		       */

		    while( be_data_decoder.SetNextCbc() ){

			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			unsigned d_cbc_id = cbc_data_packet_decoder->Id(); 
			if( d_cbc_id == cbc_id ){

			    sprintf(hname, "hbe%dc%02dg%devtcount", be_id, cbc_id, group );
			    sprintf(htype, "c%02dg%devtcount", cbc_id, group);
			    TH1F *hist = hlist.GetHist( htype, be_id, cbc_id );
			    if(hist==0){
				hist = new TH1F( hname, ";VCTH;count", nbins, min, max ); 
				hist->Sumw2();
				hlist.AddHist(hist, htype, be_id, cbc_id );
			    }
			    hist->Fill(g_vcth[cbc_id]);

			    set<unsigned>::const_iterator it_chset = channel_set.begin();
			    for(; it_chset != channel_set.end(); it_chset++ ){
				sprintf(hname, "hbe%dcbc%02dch%03d", be_id, cbc_id, *it_chset );
				strcpy(htype, "hit");
				TH1F *hist = hlist.GetHist( htype, be_id, cbc_id, *it_chset );
				if(hist!=0) break;
				hist = new TH1F( hname, ";VCTH;count", nbins, min, max ); 
				hist->Sumw2();
				hlist.AddHist(hist, htype, be_id, cbc_id, *it_chset);
				/*
				   if( hmap.find(hname) != hmap.end() ) break;
				   hmap[hname] = new TH1F( hname, ";VCTH;count", nbins, min, max ); 
				   hmap[hname]->Sumw2();
				   */
			    }
			    vector<unsigned> hit_channel_list = cbc_data_packet_decoder->HitChannels();
			    for(unsigned i=0; i < hit_channel_list.size(); i++){
				if( channel_set.find(hit_channel_list[i] ) != channel_set.end()){
				    TH1F *hist = hlist.GetHist(htype, be_id, cbc_id, hit_channel_list[i]);
				    hist->Fill(g_vcth[cbc_id]);
				    /*
				       sprintf(hname, "hbe%dcbc%dch%03d", be_id, cbc_id, hit_channel_list[i] );
				       hmap[hname]->Fill(vcth);
				       */
				    hit_sum ++;
				    //			    if(hit_channel_list[i] == 1 ) cout << vcth << endl;
				}
			    }
			}
		    }
		}
	    }
	    else{
		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);
		if( dt.tv_sec > 1 ){
		    string node( "cs_stat.db.nword_events" );
		    int nword = ReadBeBoardRegister( node );
		    int nevt = nword / 14;
		    cout << "only " << nword << " words (" << nevt << " events) received." << endl;
		    return -1;
		}
	    }
	    if( desc ){
		g_vcth[cbc_id]--;
		if( (int)g_vcth[cbc_id] == -1 ) return -1;
		//set next vcth
		if(vcth_min < 0 ){
		    if( hit_sum == 0 ){
			stop = true;
		    }
		}
		else{
		    if( g_vcth[cbc_id] < (unsigned)vcth_min ) stop = true; 
		}
	    }
	    else{
		g_vcth[cbc_id]++;
		if( g_vcth[cbc_id] == 1024 ) return -1;
		//set next vcth
		if(vcth_max < 0 ){
		    if( hit_sum == sample_count * channel_sum ){
			//		    full_count_vcth_begin = vcth;
			stop = true;
		    }
		}
		else{
		    if( g_vcth[cbc_id] > (unsigned)vcth_max ) stop = true; 
		}
	    }
	    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  )
		cout << "Channel sum = " << std::dec << channel_sum << " Hit sum = " << hit_sum << endl;
	}
	if( !only_1cbc ){
	    unsigned value=0x00;
	    rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
	sprintf(htype, "c%02dg%devtcount", cbc_id, group);
	TH1F *hevt = hlist.GetHist( htype, be_id, cbc_id ); 
	//hevt->Print();
	/*
	   sprintf(hname, "hbe%dg%devtcount", be_id, group );
	   TH1F *hevt = hmap[hname];
	   */
	//    map<string,TH1F *>::iterator it_hmap = hmap.begin();
	set<unsigned>::const_iterator it_chset = channel_set.begin();
	for(; it_chset != channel_set.end(); it_chset++ ){
	    sprintf(hname, "hbe%dcbc%02dch%03d", be_id, cbc_id, *it_chset );
	    strcpy(htype, "hit");
	    TH1F *hch = hlist.GetHist( htype, be_id, cbc_id, *it_chset );
	    hch->Divide(hch, hevt, 1., 1., "B" );
	}
	/*
	map<unsigned,TH1F *> &hmap = hlist.GetHistMap();
	map<unsigned,TH1F *>::iterator it_hmap = hmap.begin();
	for( ; it_hmap != hmap.end(); it_hmap++ ){
	    if( it_hmap->second == hevt )continue;
	     
	    TH1F *hch = it_hmap->second; 
	    hch->Divide(hch, hevt, 1., 1., "B" );
	}
	*/
    }
    if( !only_1cbc ){
	for( unsigned cbc_id = 1; cbc_id <= GetNCbcs(); cbc_id++ ){
	    if( !IsCbcActive(cbc_id) ) continue;
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    rwCbcI2cRegs(commands);  
	}
    }
    return 0;
}
int Cbc3BeInterface::TuneOffsets( CBC_ID cbc_id, GROUP_ID group, int sample_count, std::map<CHANNEL_ID, unsigned> &offsets_map )
{
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT ) ) cout << "Cbc3BeInterface::TuneOffsets() begin" << endl;

    WriteBeBoardConfig( "cs_cnfg.fcm.fcg.Ncycle", sample_count );
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
       cout << "Cbc3BeInterface::TuneOffsets() cbc id = " << cbc_id << "; group id = " << group << "; sample count    = " << std::dec << sample_count << endl;
    }

    //getting the channel set for this group
    const set<unsigned> &channel_set = GetChannelSet(group);
    if(channel_set == fInvalidChannelSet ) return -1;

    //offset data object is initialized here
    map<CHANNEL_ID, CbcParamTuningItem> offset_data; 
    set<unsigned>::const_iterator it_chset = channel_set.begin();
    for(; it_chset != channel_set.end(); it_chset++){
	unsigned ch = *it_chset;
	offset_data[ch] = CbcParamTuningItem(OffsetNbits, sample_count/2, true, (fDebug >> 2 ) & 1 ); 
	offsets_map[ch] = offset_data[ch].GetValue();
    }

    //activate the group
    ActivateGroupOffsets( group, offsets_map, cbc_id );

    //run initialization
    InitializeRun();

    //set the data acquisition parameters at the software side.
    int event_data_size = 3 + 11 * GetNActiveCbcs();
    int sample_size = sample_count * event_data_size;
    if( sample_size > 10000 ) sample_size = 0; 
    MinNumWordToWait( sample_size );
    DataReadyFlagPollingInterval_usec(0);
    if(fDataBuffer==0)
	fDataBuffer = new uint32_t[fDataBufferWordSize];
    BeDataDecoder be_data_decoder(fDebug);

    bool stop(false);
    int be_id(0);

    while( stop == false ){

	//reset hit count
	set<CHANNEL_ID>::const_iterator it_chset = channel_set.begin();
	for(; it_chset != channel_set.end(); it_chset++){
	    unsigned ch = *it_chset;
	    offset_data[ch].ClearHits();
	}

	//start sending triggers
	StartFastSignalGenerator();
	//reading data
	int ievt(0);
	struct timeval t0, t, dt;
	gettimeofday(&t0,0);
	int leftBufferSize(0);
	//
	//Data taking and counting hits
	//
	while( ievt < sample_count ){

	    if( WaitData( 1 ) ){

		if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ) cout << "Cbc3BeInterface::TuneOffsets() # of read data in 32-bit word = " << std::dec << GetNumNextDataWords() << endl; 

		int nwords = leftBufferSize/4; 
		uint32_t *Data = &fDataBuffer[nwords];
		nwords += ReadData( Data, fDataBufferWordSize-nwords );

		be_data_decoder.SetData( fDataBuffer, nwords * 4);
		while( be_data_decoder.SetNextEvent() ){ 

		    if(be_id == 0 ) be_id = be_data_decoder.BeId();

		    while( be_data_decoder.SetNextCbc() ){

			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			unsigned d_cbc_id = cbc_data_packet_decoder->Id(); 
			if(d_cbc_id == cbc_id){
			    vector<unsigned> hit_channel_list = cbc_data_packet_decoder->HitChannels();
			    for(unsigned i=0; i < hit_channel_list.size(); i++){
				unsigned ch = hit_channel_list.at(i);
				if( offset_data.find(ch) != offset_data.end() )
				    offset_data[ch].AddHits();
			    }
			    ievt++;
			}
		    }
		}
		be_data_decoder.GetIncompleteEventBuffer(leftBufferSize);
		memcpy( fDataBuffer, &fDataBuffer[nwords*4 - leftBufferSize], leftBufferSize );
	    }
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > 10 ){
		cerr << "Cbc3BeInterface::TuneOffsets() timeout at # of event = " << ievt << endl; 
		return -1;
	    }	
	}
	bool tuned(false);

	//
	//Bitonic search calculation. Checking the next offset value for all the bits. 
	//
	it_chset = channel_set.begin();
	for(; it_chset != channel_set.end(); it_chset++){

	    unsigned ch = *it_chset;

	    /*
	       if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
	       cout << "CH ID = 0x" << setfill('0') << setw(2) << std::right << std::hex << ch 
	       << " OFFSET = 0x" << setfill('0') << setw(2) << std::right << std::hex << (it_offset_data->second)[ch].GetValue() 
	       << " Hits = " << std::dec << (it_offset_data->second)[ch].GetHits() << endl; 
	       }
	       */

	    tuned = offset_data[ch].UpdateParam();
	    offsets_map[ch] = offset_data[ch].GetValue();
	}
	if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  )cout << "Cbc3BeInterface::TuneOffsets() update offsets " << endl; 
	rwCbcI2c_Offsets(1,1, offsets_map,cbc_id);

	if(tuned) stop = true;
    }
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  )cout << "Cbc3BeInterface::TuneOffsets() Disable the group " << group << endl; 
    DisableGroupOffsets( group, 0 );
    return 0;
}
unsigned Cbc3BeInterface::GetBeId()
{
    if( !fCbcConfigValid )ReadBeCbcConfig();
    return fBeId;
}
unsigned Cbc3BeInterface::GetNFes()
{
    if( !fCbcConfigValid )ReadBeCbcConfig();
    return fNFes;
}
unsigned Cbc3BeInterface::GetNCbcs()
{
    if( !fCbcConfigValid )ReadBeCbcConfig();
    return fNCbcs;
}
const std::map<unsigned,unsigned> &Cbc3BeInterface::GetNFeCbcs()
{
    if( !fCbcConfigValid )ReadBeCbcConfig();
    return fNFeCbcs;
}

bool Cbc3BeInterface::IsCbcActive( unsigned cbc_id ){

    const map<unsigned,bool> &flags = *GetCbcActiveFlags();
    map<unsigned,bool>::const_iterator it = flags.find(cbc_id);
    if( it !=  flags.end() ) return it->second; 
    else return false;
}
const std::map<unsigned,bool>* Cbc3BeInterface::GetCbcActiveFlags(){

    if( !fCbcConfigValid )ReadBeCbcConfig();
    return &fCbcActiveFlags;
}
const std::map<unsigned,unsigned>* Cbc3BeInterface::GetCbcFeIds(){

    if( !fCbcConfigValid )ReadBeCbcConfig();
    return &fCbcFeIds;
}
unsigned Cbc3BeInterface::GetNActiveCbcs()
{
    if( !fCbcConfigValid )ReadBeCbcConfig();
    return fNActiveCbcs;
}

void Cbc3BeInterface::DumpCbcI2cReplies( std::ostream &os )const{

	dump_replies( fCbcI2cRegReplies, os ); 
}
void Cbc3BeInterface::PrintCbcI2cReplies( unsigned cbc_id, std::ostream &os )const{

	print_regcnf( fCbcI2cRegReplies, os, cbc_id ); 
}

int Cbc3BeInterface::DumpData( const void *data, int &evt_count, int &evt_dump_count, int level, int nevents, int offset, int buf_size )
{
    BeDataDecoder decoder;
    int cLeftBufferSize(0);
    decoder.SetData( data, buf_size );
    while( decoder.SetNextEvent() ){
	if( evt_count++ < offset ) continue; 
	if( level == 1 )
	    decoder.DumpEventBeData(cout);
	while( decoder.SetNextCbc() ){
	    const CbcDataPacketDecoder *cbc_decoder = decoder.GetCbcDataPacketDecoder();
	    if( level == 1 )
		cbc_decoder->DumpData(cout);
	}
	if(++evt_dump_count == nevents ){
	    break;
	}
    }
    decoder.GetIncompleteEventBuffer( cLeftBufferSize );
    return cLeftBufferSize;
}

void Cbc3BeInterface::DumpData( const std::string &data_file_name, int level, int nevents, int offset, int buf_size )
{
    char *cDataBuffer = new char[buf_size];
    int cFreeBufferSize(buf_size);
    int cDataSize(0);

    ifstream cDataFile( data_file_name.c_str(),ifstream::binary);
    if( !cDataFile.is_open() ){
	cerr << "Data file " << data_file_name << " cannot be opened." << endl;
	return;
    }

    cDataFile.read( cDataBuffer, cFreeBufferSize );
    cDataSize = cDataFile.gcount();
    int evt_count(0);
    int evt_dump_count(0);
    while( true ){
	int cLeftBufferSize = DumpData( cDataBuffer, evt_count, evt_dump_count, level, nevents, offset, cDataSize );
	if( cLeftBufferSize )
	    memcpy( cDataBuffer, &cDataBuffer[cDataSize - cLeftBufferSize], cLeftBufferSize );
	cFreeBufferSize = buf_size - cLeftBufferSize;
	if( !cDataFile.eof() ){
	    cDataFile.read( &cDataBuffer[cLeftBufferSize], cFreeBufferSize );
	    cDataSize = cLeftBufferSize + cDataFile.gcount();
	}
	else if( cLeftBufferSize ){
	    cerr << "Data is truncated." << cLeftBufferSize << " left." << endl;
	    break;
	}
	else{
	    break;
	}
	/*
	   if( cLeftBufferSize ){
	   memcpy( cDataBuffer, &cDataBuffer[cDataSize - cLeftBufferSize], cLeftBufferSize );
	   cFreeBufferSize = buf_size - cLeftBufferSize;
	   if( !cDataFile.eof() ){
	   cDataFile.read( &cDataBuffer[cLeftBufferSize], cFreeBufferSize );
	   cDataSize = cLeftBufferSize + cDataFile.gcount();
	   }
	   else if( cLeftBufferSize ){
	   cerr << "Data is truncated." << cLeftBufferSize << " left." << endl;
	   break;
	   }
	   else{
	   break;
	   }
	   }
	   */
	if(evt_dump_count == nevents ){
	    break;
	}
    }
    cDataFile.close();
    cout << "Total number of events is " << std::dec << evt_count << endl; 
    return;
}
const GROUP_CHANNELSET_MAP &Cbc3BeInterface::SetGroupChannelSetMap(unsigned ng )
{
    fGroupChannelSetMap = InitGroupChannelSetMap(ng);
    return fGroupChannelSetMap;
}

GROUP_CHANNELSET_MAP Cbc3BeInterface::InitGroupChannelSetMap(unsigned ng )
{

    GROUP_CHANNELSET_MAP gmap;
    if( ng == 8 ){
	for( unsigned group = 0; group < 8; group ++ ){
	    for(int i=1; i <= 241; i+= 16 ) {
		if( i+2*group > 254 ) continue;
		gmap[group].insert(i+2*group);
		gmap[group].insert(i+2*group+1);
	    }
	}
    }
    else if( ng == 16 ){
	for( unsigned group = 0; group < 16; group ++ ){
	    for(int i=1; i <= 241; i+= 16 ) {
		if( i+group > 254 ) continue;
		gmap[group].insert(i+group);
	    }
	}
    }
    else if( ng == 9 ){
	for(int i=1; i <= 254; i++ ) {
	    gmap[ng].insert(i);
	}
    }
    else if( ng == 10 ){
	for(int i=1; i <= 254; i++ ) {
	    if( i % 16 != 1 && i % 16 != 2 ) 
		gmap[ng].insert(i);
	}
	gmap[ng].insert(113);
	gmap[ng].insert(114);
    }
    else if( ng == 11 ){
	for(int i=1; i <= 254; i++ ) {
	    if( i % 16 != 1 && i % 16 != 2 ) 
		gmap[ng].insert(i);
	}
    }
    else if( ng == 12 ){
	for(int i=1; i <= 254; i++ ) {
	    if( i != 245 )
		gmap[ng].insert(i);
	}
    }
    else if( ng < 8 ){
	int group = ng;
	for(int i=1; i <= 254; i+= 16 ) {
	    if(i+2*group > 254) break; 
	    gmap[group].insert(i+2*group);
	    gmap[group].insert(i+2*group+1);
	}
    }
    return gmap;
}

const std::set<unsigned>& Cbc3BeInterface::GetChannelSet(unsigned group)
{
    GROUP_CHANNELSET_MAP::const_iterator it = fGroupChannelSetMap.find(group);
    if( it == fGroupChannelSetMap.end() ){
	cerr << "The group " << group << " is not found" << endl;
	return fInvalidChannelSet;
    }
    return it->second; 
}
