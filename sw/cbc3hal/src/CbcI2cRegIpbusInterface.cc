#include "CbcI2cRegIpbusInterface.h"
#include "ipbus_utils.h"
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include "libpq-fe.h"

static void
exit_nicely(PGconn *conn)
{
        PQfinish(conn);
	    exit(1);
}
using namespace std;
using namespace ipbutl;


//using namespace cbc3hal;
static std::string get_currtime(){
    time_t      rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);
    string stime(asctime(timeinfo));
    stime.erase( remove(stime.begin(),stime.end(),'\n'),stime.end() );
    return stime;
}

namespace cbc3hal{

    int reply_fifo_ndata_sum=0;

    static unsigned swap_bits( unsigned val, unsigned nbits )
    {
	unsigned sval(0);
	for( unsigned i=0; i<nbits; i++ ) {
	    sval |= ( ( ( val >> i ) & 1 ) << (nbits-1-i) );
	}
	return sval;
    }	
    int get_bus_id (CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_BUS_ID_BIT_MASK) >> CBC_I2C_REPLY_BUS_ID_BIT_OFFSET; };  
    int get_cbc_id (CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_CBC_ID_BIT_MASK) >> CBC_I2C_REPLY_CBC_ID_BIT_OFFSET; };
    int get_info   (CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_INFO_BIT_MASK)   >> CBC_I2C_REPLY_INFO_BIT_OFFSET; };
    int get_rw_flag(CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_RW_FLAG_BIT_MASK)>> CBC_I2C_REPLY_RW_FLAG_BIT_OFFSET; }; 
    int get_page   (CBCI2C_REPLY r){ return ((r&CBC_I2C_REPLY_PAGE_BIT_MASK) >> CBC_I2C_REPLY_PAGE_BIT_OFFSET) + 1; }; 
    int get_addr   (CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_ADDR_BIT_MASK)   >> CBC_I2C_REPLY_ADDR_BIT_OFFSET; };
    int get_val    (CBCI2C_REPLY r){ return (r&CBC_I2C_REPLY_VALUE_BIT_MASK)  >> CBC_I2C_REPLY_VALUE_BIT_OFFSET; };

    CBCI2C_REGID get_regid(CBCI2C_REPLY r)
    {
	if(get_addr(r) == 0 ) r &= ~(1 << CBC_I2C_REPLY_PAGE_BIT_OFFSET);
	return r&(CBC_I2C_REPLY_BUS_ID_BIT_MASK|CBC_I2C_REPLY_CBC_ID_BIT_MASK|CBC_I2C_REPLY_PAGE_BIT_MASK|CBC_I2C_REPLY_ADDR_BIT_MASK);
    }
    CBCI2C_REGID get_regid( unsigned bus_id, unsigned cbc_id, unsigned page, unsigned addr )
    {
	if(addr == 0) page = 1;
	return (bus_id<<CBC_I2C_REPLY_BUS_ID_BIT_OFFSET) | (cbc_id<<CBC_I2C_REPLY_CBC_ID_BIT_OFFSET) | ((page-1)<<CBC_I2C_REPLY_PAGE_BIT_OFFSET) | (addr<<CBC_I2C_REPLY_ADDR_BIT_OFFSET);
    }
    CBCI2C_NAMES cbci2creg_formatter::InitNames(){

	CBCI2C_NAMES names; 
	set<string> items;
	items.insert("trig_lat");
	items.insert("beta_mult");
	items.insert("comp_hyst");
	items.insert("comp_pol"); 
	items.insert("fc_page");
	names["FcCntrlCompBetaTrigLat"] = items;
	items.clear();
	items.insert("beta_mult");
	items.insert("slvs_curr");
	names["BetaMultiplierAndSLVS"] = items;
	items.clear();
	items.insert("curr");
	names["Ipre1"] = items;
	items.clear();
	items.insert("curr");
	names["Ipre2"] = items;
	items.clear();
	items.insert("curr");
	names["Ipsf"] = items;
	items.clear();
	items.insert("curr");
	names["Ipa"] = items;
	items.clear();
	items.insert("curr");
	names["Ipaos"] = items;
	items.clear();
	items.insert("curr");
	names["Icomp"] = items;
	items.clear();
	items.insert("vplus1");
	items.insert("vplus2");
	names["VPLUS"] = items;
	items.clear();
	items.insert("hip_count");
	items.insert("hip_supp");
	items.insert("hip_src");
	items.insert("sel_clk40_ref");
	items.insert("sel_clk40_dll");
	items.insert("slvs_off");
	names["HipAndTestMode"] = items;
	items.clear();
	items.insert("pot");
	names["TestPulsePotentiometer"] = items; 
	items.clear();
	items.insert("delay");
	items.insert("group");
	names["TestPulseDelayGroup"] = items;
	items.clear();
	items.insert("tp_pol");
	items.insert("tp_en");
	items.insert("tp_gnd");
	items.insert("amux");
	names["TestPulsePolEnAMux"] = items;
	items.clear();
	items.insert("curr");
	names["CAL_Ibias"] = items;
	items.clear();
	items.insert("volt");
	names["CAL_Vcasc"] = items;
	items.clear();
	items.insert("pl_sel");
	items.insert("st_sel");
	items.insert("pt_width");
	names["PlStLogicSelAndPtWidth"] = items;
	items.clear();
	items.insert("ofst4");
	items.insert("ofst3");
	items.insert("ofst2");
	items.insert("ofst1");
	names["CoinWindowOffsets"] = items;
	items.clear();
	items.insert("override");
	items.insert("sel");
	items.insert("bg_fuse");
	names["BandgapFuse"] = items;
	items.clear();
	items.insert("id");
	items.insert("sel");
	names["ChipIdFuse"] = items;
	items.clear();
	items.insert("lswap");
	items.insert("width");
	names["LayerSwapAndClusterWidth"] = items;
	items.clear();
	items.insert("dll");
	items.insert("test_out_40mhz_clk");
	items.insert("or254");
	items.insert("tpg");
	names["40MHzClockOr254DLL"] = items;
	items.clear();
	items.insert("err_bad_code");
	items.insert("err_sync_lost");
	items.insert("err_sync_stat");
	items.insert("err_lat_err");
	items.insert("err_ram_ofl");
	items.insert("fci_delay");
	names["FciAndError"] = items;
	items.clear();
	items.insert("ch_001_008");
	items.insert("ch_009_016");
	items.insert("ch_017_024");
	items.insert("ch_025_032");
	items.insert("ch_033_040");
	items.insert("ch_041_048");
	items.insert("ch_049_056");
	items.insert("ch_057_064");
	items.insert("ch_065_072");
	items.insert("ch_073_080");
	items.insert("ch_081_088");
	items.insert("ch_089_096");
	items.insert("ch_097_104");
	items.insert("ch_105_112");
	items.insert("ch_113_120");
	items.insert("ch_121_128");
	items.insert("ch_129_136");
	items.insert("ch_137_144");
	items.insert("ch_145_152");
	items.insert("ch_153_160");
	items.insert("ch_161_168");
	items.insert("ch_169_176");
	items.insert("ch_177_184");
	items.insert("ch_185_192");
	items.insert("ch_193_200");
	items.insert("ch_201_208");
	items.insert("ch_209_216");
	items.insert("ch_217_224");
	items.insert("ch_225_232");
	items.insert("ch_233_240");
	items.insert("ch_241_248");
	items.insert("ch_249_254");
	names["MaskChannels"] = items;
	items.clear();
	items.insert("cl_cntr_m7_m6h");
	items.insert("cl_cntr_m6_m5h");
	items.insert("cl_cntr_m5_m4h");
	items.insert("cl_cntr_m4_m3h");
	items.insert("cl_cntr_m3_m2h");
	items.insert("cl_cntr_m2_m1h");
	items.insert("cl_cntr_m1_m0h");
	items.insert("cl_cntr_c_p0h" );
	items.insert("cl_cntr_p1_p1h");
	items.insert("cl_cntr_p2_p2h");
	items.insert("cl_cntr_p3_p3h");
	items.insert("cl_cntr_p4_p4h");
	items.insert("cl_cntr_p5_p5h");
	items.insert("cl_cntr_p6_p6h");
	items.insert("cl_cntr_p7"    );
	names["Bends"] = items;
	items.clear();
	items.insert("vcth");
	names["VCTH"] = items;
	items.clear();
	items.insert("in_A");
	items.insert("in_B");
	items.insert("out_A");
	items.insert("out_B");
	items.insert("mode_sel");
	names["TestIONNTop"] = items;
	items.clear();
	items.insert("in_A");
	items.insert("in_B");
	items.insert("out_A");
	items.insert("out_B");
	items.insert("mode_sel");
	names["TestIONNBottom"] = items;
	items.clear();
	for( int i=1; i <= 255; i++ ){
	    char item[256];
	    if( i == 255 ) sprintf(item, "dummy");
	    else sprintf(item, "ch_%03d", i);
	    items.insert( item );
	}
	names["Offsets"] = items;
	return names;
    }
    CBCI2C_NAMES cbci2creg_formatter::fNames(cbci2creg_formatter::InitNames());

    std::set<std::string> cbci2creg_formatter::getTypeNames(){

	map<string,set<string> >::iterator it = fNames.begin();
	set<string> types;
	for(; it != fNames.end(); it++){ 
	    types.insert(it->first);
	}
	return types;
    }
    const std::set<std::string>& cbci2creg_formatter::getItemNames(const std::string &type){
	return fNames[type];
    }
    void cbci2creg_formatter::printNames(std::ostream &os, bool ofst){
	const CBCI2C_NAMES names = getNames();
	CBCI2C_NAMES::const_iterator it = names.begin();
	for(; it != names.end(); it++){
	    string type = it->first;
	    if( type == "Offsets" && ofst == false ) continue;
	    set<string> items = it->second;
	    set<string>::const_iterator it_items = items.begin();
	    for(; it_items != items.end(); it_items++){
		os << type << "." << *it_items << endl;
	    }
	}
    }
    void cbci2creg_formatter::printTypeNames(std::ostream &os){
	set<string> types = getTypeNames(); 
	set<string>::iterator it = types.begin();
	for(; it != types.end(); it++){
	    os << *it << endl;
	}
	os << endl;
    }
    void cbci2creg_formatter::printItemNames(const std::string &type, std::ostream &os){
	const set<string> items = getItemNames(type);
	set<string>::const_iterator it = items.begin();
	for(; it!= items.end(); it++){
	    os << *it << " ";
	}
	os << endl;
    }
    std::vector<unsigned> cbci2creg_formatter::getAddrs( const std::string &type ){

	std::vector<unsigned> addrs;
	if( type == "FcCntrlCompBetaTrigLat" ){
	    addrs.push_back( 0x00 );
	    addrs.push_back( 0x01 );
	}
	else if( type == "BetaMultiplierAndSLVS" ){
	    addrs.push_back( 0x02 );
	}
	else if( type == "Ipre1" ){
	    addrs.push_back( 0x03 );
	}
	else if( type == "Ipre2" ){
	    addrs.push_back( 0x04 );
	}
	else if( type == "Ipsf" ){
	    addrs.push_back( 0x05 );
	}
	else if( type == "Ipa" ){
	    addrs.push_back( 0x06 );
	}
	else if( type == "Ipaos" ){
	    addrs.push_back( 0x07 );
	}
	else if( type == "Icomp" ){
	    addrs.push_back( 0x09 );
	}
	else if( type == "VPLUS" ){
	    addrs.push_back( 0x0B );
	}
	else if( type == "HipAndTestMode" ){
	    addrs.push_back( 0x0C );
	}
	else if( type == "TestPulsePotentiometer" ){
	    addrs.push_back( 0x0D );
	}
	else if( type == "TestPulseDelayGroup" ){
	    addrs.push_back( 0x0E );
	}
	else if( type == "TestPulsePolEnAMux" ){
	    addrs.push_back( 0x0F );
	}
	else if( type == "CAL_Ibias" ){
	    addrs.push_back( 0x10 );
	}
	else if( type == "CAL_Vcasc" ){
	    addrs.push_back( 0x11 );
	}
	else if( type == "PlStLogicSelAndPtWidth" ){
	    addrs.push_back( 0x12 );
	}
	else if( type == "CoinWindowOffsets" ){
	    addrs.push_back( 0x13 );
	    addrs.push_back( 0x14 );
	}
	else if( type == "BandgapFuse" ){
	    addrs.push_back( 0x15 );
	}
	else if( type == "ChipIdFuse" ){
	    addrs.push_back( 0x16 );
	    addrs.push_back( 0x17 );
	    addrs.push_back( 0x18 );
	}
	else if( type == "LayerSwapAndClusterWidth" ){
	    addrs.push_back( 0x1b );
	}
	else if( type == "40MHzClockOr254DLL"){
	    addrs.push_back( 0x1C );
	}
	else if( type == "FciAndError" ){
	    addrs.push_back( 0x1D );
	}
	else if( type == "MaskChannels" ){
	    for( int i=0x20; i <= 0x3F; i++ ){
		addrs.push_back( i );
	    }
	}
	else if( type == "Bends" ){
	    for( int i=0x40; i < 0x4F; i++ ){
		addrs.push_back( i );
	    }
	}
	else if( type == "VCTH" ){
	    addrs.push_back( 0x4F );
	    addrs.push_back( 0x50 );
	}
	else if( type == "TestIONNTop" ){
	    addrs.push_back( 0x51 );
	    addrs.push_back( 0x52 );
	    addrs.push_back( 0x53 );
	    addrs.push_back( 0x54 );
	}
	else if( type == "TestIONNBottom" ){
	    addrs.push_back( 0x55 );
	    addrs.push_back( 0x56 );
	    addrs.push_back( 0x57 );
	    addrs.push_back( 0x58 );
	}
	else if( type == "Offsets" ){
	    for( int i=0x01; i <= 0xFF; i++ ){
		addrs.push_back( i );
	    }
	}
	return addrs;
    }
    std::vector<unsigned> cbci2creg_formatter::getReadAddrs( const CBCI2C_TYPE_DATA &type_data ){

	const string &type                 = type_data.first; 
	const CBCI2C_ITEM_DATA_MAP tid_map = type_data.second; 
	/*
	cout << "type=" << type << endl;
	cout << "tid_map.size() = " << tid_map.size() << endl; 
	*/

	std::vector<unsigned> addrs;
	if( type == "FcCntrlCompBetaTrigLat" ){
	    bool trig_lat(false), beta_mult(false), comp_hyst(false), comp_pol(false), fc_page(false);
	    if(tid_map.find("trig_lat")  != tid_map.end()) trig_lat  = true; 
	    if(tid_map.find("beta_mult") != tid_map.end()) beta_mult = true; 
	    if(tid_map.find("comp_hyst") != tid_map.end()) comp_hyst = true; 
	    if(tid_map.find("comp_pol")  != tid_map.end()) comp_pol  = true; 
	    if(tid_map.find("fc_page")   != tid_map.end()) fc_page   = true; 
	    if(trig_lat || beta_mult || comp_hyst || comp_pol || fc_page) 
		addrs.push_back( 0x00 );
	    if( trig_lat ) addrs.push_back( 0x01 );
	}
	else if( type == "BetaMultiplierAndSLVS" ){
	    addrs.push_back( 0x02 );
	}
	else if( type == "Ipre1" ){
	    addrs.push_back( 0x03 );
	}
	else if( type == "Ipre2" ){
	    addrs.push_back( 0x04 );
	}
	else if( type == "Ipsf" ){
	    addrs.push_back( 0x05 );
	}
	else if( type == "Ipa" ){
	    addrs.push_back( 0x06 );
	}
	else if( type == "Ipaos" ){
	    addrs.push_back( 0x07 );
	}
	else if( type == "Icomp" ){
	    addrs.push_back( 0x09 );
	}
	else if( type == "VPLUS" ){
	    addrs.push_back( 0x0B );
	}
	else if( type == "HipAndTestMode" ){
	    addrs.push_back( 0x0C );
	}
	else if( type == "TestPulsePotentiometer" ){
	    addrs.push_back( 0x0D );
	}
	else if( type == "TestPulseDelayGroup" ){
	    addrs.push_back( 0x0E );
	}
	else if( type == "TestPulsePolEnAMux" ){
	    addrs.push_back( 0x0F );
	}
	else if( type == "CAL_Ibias" ){
	    addrs.push_back( 0x10 );
	}
	else if( type == "CAL_Vcasc" ){
	    addrs.push_back( 0x11 );
	}
	else if( type == "PlStLogicSelAndPtWidth" ){
	    addrs.push_back( 0x12 );
	}
	else if( type == "CoinWindowOffsets" ){
	    bool ofst1(false), ofst2(false), ofst3(false), ofst4(false);
	    if( tid_map.find("ofst1") != tid_map.end() ) ofst1 = true;
	    if( tid_map.find("ofst2") != tid_map.end() ) ofst2 = true;
	    if( tid_map.find("ofst3") != tid_map.end() ) ofst3 = true;
	    if( tid_map.find("ofst4") != tid_map.end() ) ofst4 = true;
	    if( ofst3 || ofst4 ) addrs.push_back( 0x13 );
	    if( ofst1 || ofst2 ) addrs.push_back( 0x14 );
	}
	else if( type == "BandgapFuse" ){
	    addrs.push_back( 0x15 );
	}
	else if( type == "ChipIdFuse" ){
	    bool id(false);
	    if(tid_map.find("id")  != tid_map.end()) id = true;
	    //if(tid_map.find("sel") != tid_map.end()) sel = true;
	    if( id ){
		addrs.push_back( 0x16 );
		addrs.push_back( 0x17 );
	    }
	    addrs.push_back( 0x18 );
	}
	else if( type == "LayerSwapAndClusterWidth" ){
	    addrs.push_back( 0x1b );
	}
	else if( type == "40MHzClockOr254DLL"){
	    addrs.push_back( 0x1C );
	}
	else if( type == "FciAndError" ){
	    addrs.push_back( 0x1D );
	}
	else if( type == "MaskChannels" ){
	    for( unsigned addr=0x20; addr <= 0x3F; addr++ ){
		char item[256];
		unsigned offset = (addr-0x20) * 8 + 1;
		sprintf(item, "ch_%03d_%03d", offset, offset + 7 );
		if(addr == 0x3F)sprintf(item, "ch_249_254"); 
		if(tid_map.find(item) != tid_map.end()) 
		    addrs.push_back( addr );
	    }
	}
	else if( type == "Bends" ){
	    if(tid_map.find("cl_cntr_m7_m6h") != tid_map.end()) addrs.push_back(0x40);
	    if(tid_map.find("cl_cntr_m6_m5h") != tid_map.end()) addrs.push_back(0x41);
	    if(tid_map.find("cl_cntr_m5_m4h") != tid_map.end()) addrs.push_back(0x42);
	    if(tid_map.find("cl_cntr_m4_m3h") != tid_map.end()) addrs.push_back(0x43);
	    if(tid_map.find("cl_cntr_m3_m2h") != tid_map.end()) addrs.push_back(0x44);
	    if(tid_map.find("cl_cntr_m2_m1h") != tid_map.end()) addrs.push_back(0x45);
	    if(tid_map.find("cl_cntr_m1_m0h") != tid_map.end()) addrs.push_back(0x46);
	    if(tid_map.find("cl_cntr_c_p0h" ) != tid_map.end()) addrs.push_back(0x47);
	    if(tid_map.find("cl_cntr_p1_p1h") != tid_map.end()) addrs.push_back(0x48);
	    if(tid_map.find("cl_cntr_p2_p2h") != tid_map.end()) addrs.push_back(0x49);
	    if(tid_map.find("cl_cntr_p3_p3h") != tid_map.end()) addrs.push_back(0x4a);
	    if(tid_map.find("cl_cntr_p4_p4h") != tid_map.end()) addrs.push_back(0x4b);
	    if(tid_map.find("cl_cntr_p5_p5h") != tid_map.end()) addrs.push_back(0x4c);
	    if(tid_map.find("cl_cntr_p6_p6h") != tid_map.end()) addrs.push_back(0x4d);
	    if(tid_map.find("cl_cntr_p7"    ) != tid_map.end()) addrs.push_back(0x4e);
	}
	else if( type == "VCTH" ){
	    addrs.push_back( 0x4F );
	    addrs.push_back( 0x50 );
	}
	else if( type == "TestIONNTop" ){
	    bool in_A(false), in_B(false), mode_sel(false);
	    bool out_A(false), out_B(false);
	    if(tid_map.find("in_A")  != tid_map.end()) in_A = true;
	    if(tid_map.find("in_B")  != tid_map.end()) in_B = true;
	    if(tid_map.find("mode_sel")  != tid_map.end()) mode_sel = true;
	    if(tid_map.find("out_A")  != tid_map.end()) out_A = true;
	    if(tid_map.find("out_B")  != tid_map.end()) out_B = true;
	    if(in_A | in_B) addrs.push_back(0x51);
	    if(in_B) addrs.push_back(0x52);
	    if(out_A|out_B|mode_sel) addrs.push_back(0x53);
	    if(out_B) addrs.push_back(0x54);
	}
	else if( type == "TestIONNBottom" ){
	    bool in_A(false), in_B(false), mode_sel(false);
	    bool out_A(false), out_B(false);
	    if(tid_map.find("in_A")  != tid_map.end()) in_A = true;
	    if(tid_map.find("in_B")  != tid_map.end()) in_B = true;
	    if(tid_map.find("mode_sel")  != tid_map.end()) mode_sel = true;
	    if(tid_map.find("out_A")  != tid_map.end()) out_A = true;
	    if(tid_map.find("out_B")  != tid_map.end()) out_B = true;
	    if(in_A | in_B) addrs.push_back(0x55);
	    if(in_B | mode_sel) addrs.push_back(0x56);
	    if(out_A|out_B) addrs.push_back(0x57);
	    if(out_B) addrs.push_back(0x58);
	}
	else if( type == "Offsets" ){
	    for( int i=0x01; i <= 0xFF; i++ ){
		char item[256];
		if( i == 0xFF ) sprintf( item, "dummy" );
		else sprintf(item, "ch_%03d", i);
		if(tid_map.find(item) != tid_map.end()) addrs.push_back(i); 
	    }
	}
	return addrs;
    }
    //Returns addresses of only the registers which share the values with other items than the one which is going to be written.
    std::vector<unsigned> cbci2creg_formatter::getReadAddrsToWrite( const CBCI2C_TYPE_DATA &type_data ){

	const string &type              = type_data.first; 
	const CBCI2C_ITEM_DATA_MAP tid_map = type_data.second; 

	std::vector<unsigned> addrs;
	if( type == "FcCntrlCompBetaTrigLat" ){
	    bool trig_lat(false), beta_mult(false), comp_hyst(false), comp_pol(false), fc_page(false);
	    if(tid_map.find("trig_lat")  != tid_map.end()) trig_lat  = true; 
	    if(tid_map.find("beta_mult") != tid_map.end()) beta_mult = true; 
	    if(tid_map.find("comp_hyst") != tid_map.end()) comp_hyst = true; 
	    if(tid_map.find("comp_pol")  != tid_map.end()) comp_pol  = true; 
	    if(tid_map.find("fc_page")   != tid_map.end()) fc_page   = true; 
	    if( !( trig_lat && beta_mult && comp_hyst && comp_pol && fc_page) )
		addrs.push_back( 0x00 );
	}
	else if( type == "BetaMultiplierAndSLVS" ){
	    bool beta_mult(false), slvs_curr(false);
	    if(tid_map.find("slvs_curr") != tid_map.end()) slvs_curr = true; 
	    if(tid_map.find("beta_mult") != tid_map.end()) beta_mult = true; 
	    if( !(slvs_curr && beta_mult) )
		addrs.push_back( 0x02 );
	}
	else if( type == "VPLUS" ){
	    bool vplus1(false), vplus2(false);
	    if(tid_map.find("vplus1") != tid_map.end()) vplus1 = true; 
	    if(tid_map.find("vplus2") != tid_map.end()) vplus2 = true; 
	    if(!(vplus1 && vplus2))
		addrs.push_back( 0x0B );
	}
	else if( type == "HipAndTestMode" ){
	    bool hip_count(false), hip_supp(false), hip_src(false); 
	    bool sel_clk40_ref(false), sel_clk40_dll(false), slvs_off(false);
	    if(tid_map.find("hip_count") != tid_map.end()) hip_count = true; 
	    if(tid_map.find("hip_supp") != tid_map.end()) hip_supp = true; 
	    if(tid_map.find("hip_src") != tid_map.end()) hip_src = true; 
	    if(tid_map.find("sel_clk40_ref") != tid_map.end()) sel_clk40_ref = true; 
	    if(tid_map.find("sel_clk40_dll") != tid_map.end()) sel_clk40_dll = true; 
	    if(tid_map.find("slvs_off") != tid_map.end()) slvs_off = true; 
	    if( !(hip_count && hip_supp && hip_src && sel_clk40_ref && sel_clk40_dll & slvs_off) )
		addrs.push_back( 0x0C );
	}
	else if( type == "TestPulseDelayGroup" ){
	    bool delay(false), group(false);
	    if(tid_map.find("delay") != tid_map.end()) delay = true; 
	    if(tid_map.find("group") != tid_map.end()) group = true; 
	    if(!(delay && group) )
		addrs.push_back( 0x0E );
	}
	else if( type == "TestPulsePolEnAMux" ){
	    bool tp_pol(false), tp_en(false), tp_gnd(false), amux(false);
	    if( tid_map.find("amux")   != tid_map.end() ) amux   = true; 
	    if( tid_map.find("tp_gnd") != tid_map.end() ) tp_gnd = true; 
	    if( tid_map.find("tp_en")  != tid_map.end() ) tp_en  = true; 
	    if( tid_map.find("tp_pol") != tid_map.end() ) tp_pol = true; 
	    if( !( amux && tp_gnd && tp_en && tp_pol) )
		addrs.push_back( 0x0F );
	}
	else if( type == "PlStLogicSelAndPtWidth" ){
	    bool pl_sel(false), st_sel(false), pt_width(false);
	    if( tid_map.find("pl_sel")   != tid_map.end() ) pl_sel   = true; 
	    if( tid_map.find("st_sel")   != tid_map.end() ) st_sel   = true; 
	    if( tid_map.find("pt_width") != tid_map.end() ) pt_width = true; 
	    if( !(pl_sel && st_sel && pt_width) )
		addrs.push_back( 0x12 );
	}
	else if( type == "CoinWindowOffsets" ){
	    bool ofst1(false), ofst2(false), ofst3(false), ofst4(false);
	    if( tid_map.find("ofst1") != tid_map.end() ) ofst1 = true;
	    if( tid_map.find("ofst2") != tid_map.end() ) ofst2 = true;
	    if( tid_map.find("ofst3") != tid_map.end() ) ofst3 = true;
	    if( tid_map.find("ofst4") != tid_map.end() ) ofst4 = true;
	    if( ofst3 != ofst4 ) addrs.push_back( 0x13 );
	    if( ofst1 != ofst2 ) addrs.push_back( 0x14 );
	}
	else if( type == "BandgapFuse" ){
	    bool ovrd(false), sel(false), bg_fuse(false); 
	    if(tid_map.find("override") != tid_map.end()) ovrd    = true; 
	    if(tid_map.find("sel")      != tid_map.end()) sel     = true; 
	    if(tid_map.find("bg_fuse")  != tid_map.end()) bg_fuse = true; 
	    if( !(ovrd && sel && bg_fuse) )
		addrs.push_back( 0x15 );
	}
	else if( type == "ChipIdFuse" ){
	    bool id(false), sel(false);
	    if(tid_map.find("id")  != tid_map.end()) id = true;
	    if(tid_map.find("sel") != tid_map.end()) sel = true;
	    if( !( id && sel ) )
		addrs.push_back( 0x18 );
	}
	else if( type == "LayerSwapAndClusterWidth" ){
	    bool lswap(false), width(false); 
	    if(tid_map.find("lswap") != tid_map.end()) lswap = true;
	    if(tid_map.find("width") != tid_map.end()) width = true;
	    if( !(lswap && width) )
		addrs.push_back( 0x1b );
	}
	else if( type == "40MHzClockOr254DLL"){
	    bool dll(false), test_out_40mhz_clk(false), or254(false), tpg(false);
	    if(tid_map.find("dll")                != tid_map.end()) dll                = true; 
	    if(tid_map.find("test_out_40mhz_clk") != tid_map.end()) test_out_40mhz_clk = true;
	    if(tid_map.find("or254")              != tid_map.end()) or254              = true;
	    if(tid_map.find("tpg")                != tid_map.end()) tpg                = true;
	    if( ! (dll && test_out_40mhz_clk && or254 && tpg ) )
		addrs.push_back( 0x1C );
	}
	else if( type == "FciAndError" ){
	    addrs.push_back( 0x1D );
	}
	else if( type == "TestIONNTop" ){
	    bool in_A(false), in_B(false);
	    if(tid_map.find("in_A")                != tid_map.end()) in_A                = true; 
	    if(tid_map.find("in_B")                != tid_map.end()) in_B                = true; 
	    if( in_A | in_B )
	    addrs.push_back(0x51);
	}
	else if( type == "TestIONNBottom" ){
	    bool in_A(false), in_B(false), mode_sel(false);
	    if(tid_map.find("in_A")                != tid_map.end()) in_A                = true; 
	    if(tid_map.find("in_B")                != tid_map.end()) in_B                = true; 
	    if(tid_map.find("mode_sel")            != tid_map.end()) mode_sel            = true; 
	    if( in_A | in_B ) addrs.push_back(0x55);
	    if( in_B | mode_sel ) addrs.push_back(0x56);
	}
	return addrs;
    }
//    std::vector<unsigned> cbci2creg_formatter::geti2cregvaluestowrite( const cbci2c_type_data &type_data, const std::vector<unsigned> *reg_values0 )
    CBCI2C_ADDR_DATA_MAP cbci2creg_formatter::getI2cRegValuesToWrite( const CBCI2C_TYPE_DATA &type_data, const CBCI2C_ADDR_DATA_MAP &reg_values0, bool CBC3_0 )
    {
	const string &type = type_data.first;
	//	    cout << "cbci2creg_formatter::getI2cRegValuesToWrite() type = " << type << endl; 
	const CBCI2C_ITEM_DATA_MAP &idmap = type_data.second;
	std::map<unsigned,unsigned> new_values;
//	cout << "size of idmap = " << idmap.size() << endl;

	if( type == "FcCntrlCompBetaTrigLat" ){
	    unsigned trig_lat(0xFFFF), beta_mult(0xFF), comp_hyst(0xFF), comp_pol(0xFF), fc_page(0xFF);
	    if(idmap.find("trig_lat") != idmap.end()) trig_lat   = idmap.find("trig_lat")->second; 
	    if(idmap.find("beta_mult") != idmap.end()) beta_mult = idmap.find("beta_mult")->second; 
	    if(idmap.find("comp_hyst") != idmap.end()) comp_hyst = idmap.find("comp_hyst")->second; 
	    if(idmap.find("comp_pol") != idmap.end()) comp_pol   = idmap.find("comp_pol")->second; 
	    if(idmap.find("fc_page") != idmap.end())  fc_page    = idmap.find("fc_page")->second; 

	    unsigned addr0 = 0x00;
	    unsigned value0(0);
	    if(reg_values0.find(addr0) != reg_values0.end()) 
		value0=reg_values0.find(addr0)->second;
	    unsigned new_value0(0);

	    if( trig_lat == 0xFFFF ){
		new_value0 |= (value0 & 0x01);
	    }
	    else{
		new_value0 |= (0x100 & trig_lat >> 8 );
		new_values[0x01] = trig_lat & 0xFF;
	    }
	    if( beta_mult == 0xFF ) new_value0 |= (value0 & 0x02);
	    else new_value0 |= ((beta_mult&1) << 1);
	    if( comp_hyst == 0xFF ) new_value0 |= (value0 & 0x3C);
	    else new_value0 |= ((comp_hyst & 0xF) << 2);
	    if( comp_pol == 0xFF ) new_value0 |= (value0 & 0x40);
	    else new_value0 |= ((comp_pol & 0x1) << 6);
	    if( fc_page == 0xFF ) new_value0 |= (value0 & 0x80);
	    else new_value0 |= ((fc_page &0x1) << 7);
	    new_values[addr0] = new_value0;
	}
	else if( type == "BetaMultiplierAndSLVS" ){
	    unsigned beta_mult(0xFF), slvs_curr(0xFF);
	    if(idmap.find("slvs_curr") != idmap.end()) slvs_curr = idmap.find("slvs_curr")->second;  
	    if(idmap.find("beta_mult") != idmap.end()) beta_mult = idmap.find("beta_mult")->second;  
	    unsigned addr(0x02);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if(slvs_curr == 0xFF) new_value |= (value0 & 0x0F);
	    else new_value |= (slvs_curr & 0x0F);
	    if(beta_mult == 0xFF) new_value |= (value0 & 0xF0);
	    else new_value |= ((beta_mult & 0x0F) << 4);
	    new_values[addr] = new_value;
	}
	else if( type == "Ipre1" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x03] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "Ipre2" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x04] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "Ipsf" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x05] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "Ipa" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x06] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "Ipaos" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x07] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "Icomp" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x09] = (idmap.find("curr")->second & 0xFF);
	}
	else if( type == "VPLUS" ){
	    unsigned vplus1(0xFF), vplus2(0xFF);
	    if(idmap.find("vplus1") != idmap.end()) vplus1 = idmap.find("vplus1")->second;  
	    if(idmap.find("vplus2") != idmap.end()) vplus2 = idmap.find("vplus2")->second;  
	    unsigned addr(0x0B);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if(vplus1 == 0xFF) new_value |= (value0 & 0x0F);
	    else new_value |= (vplus1 & 0x0F);
	    if(vplus2 == 0xFF) new_value |= (value0 & 0xF0);
	    else new_value |= ((vplus2 & 0x0F) << 4);
	    new_values[addr] = new_value;
	}
	else if( type == "HipAndTestMode" ){
	    unsigned hip_count(0xFF), hip_supp(0xFF), hip_src(0xFF);
	    unsigned sel_clk40_ref(0xFF), sel_clk40_dll(0xFF), slvs_off(0xFF);
	    if(idmap.find("hip_count") != idmap.end()) hip_count = idmap.find("hip_count")->second;  
	    if(idmap.find("hip_supp") != idmap.end()) hip_supp = idmap.find("hip_supp")->second;  
	    if(idmap.find("hip_src") != idmap.end()) hip_src = idmap.find("hip_src")->second;  
	    if(idmap.find("sel_clk40_ref") != idmap.end()) sel_clk40_ref = idmap.find("sel_clk40_ref")->second;  
	    if(idmap.find("sel_clk40_dll") != idmap.end()) sel_clk40_dll = idmap.find("sel_clk40_dll")->second;  
	    if(idmap.find("slvs_off") != idmap.end()) slvs_off = idmap.find("slvs_off")->second;  
	    unsigned addr(0x0C);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if( hip_count == 0xFF) new_value |= (value0 & 0xE0);
	    else new_value |= ((hip_count & 0x07) << 5);
	    if( hip_supp == 0xFF) new_value |= (value0 & 0x10);
	    else new_value |= ((hip_supp & 0x01) << 4);
	    if( hip_src == 0xFF) new_value |= (value0 & 0x08);
	    else new_value |= ((hip_src & 0x01) << 3);
	    if( sel_clk40_ref == 0xFF) new_value |= (value0 & 0x04);
	    else new_value |= ((sel_clk40_ref & 0x01) << 2);
	    if( sel_clk40_dll == 0xFF) new_value |= (value0 & 0x02);
	    else new_value |= ((sel_clk40_dll & 0x01) << 1);
	    if( slvs_off == 0xFF) new_value |= (value0 & 0x01);
	    else new_value |= (slvs_off & 0x01);
	    new_values[addr] = new_value;
	}
	else if( type == "TestPulsePotentiometer" ){
	    if(idmap.find("pot") != idmap.end() ){
		unsigned pot = idmap.find("pot")->second;
		new_values[0x0D] = 255-pot; 
	    }
	}
	else if( type == "TestPulseDelayGroup" ){
	    unsigned delay(0xFF), group(0xFF);
	    if(idmap.find("delay") != idmap.end()) delay = idmap.find("delay")->second;
	    if(idmap.find("group") != idmap.end()) group = idmap.find("group")->second;
	    unsigned addr(0x0E);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if(group == 0xFF) new_value |= (value0 & 0x07); 
	    else new_value |= (swap_bits(group,3) & 0x07);
	    if(delay == 0xFF) new_value |= (value0 & 0xF8);
	    else new_value |= ((swap_bits(delay,5) << 3) & 0xF8);
	    new_values[addr] = new_value;
	}
	else if( type == "TestPulsePolEnAMux" ){
	    unsigned tp_pol(0xFF), tp_en(0xFF), tp_gnd(0xFF), amux(0xFF);
	    if( idmap.find("amux") != idmap.end() ) amux = idmap.find("amux")->second;
	    if( idmap.find("tp_gnd") != idmap.end() ) tp_gnd = idmap.find("tp_gnd")->second;
	    if( idmap.find("tp_en") != idmap.end() ) tp_en = idmap.find("tp_en")->second;
	    if( idmap.find("tp_pol") != idmap.end() ) tp_pol = idmap.find("tp_pol")->second;
	    unsigned addr(0x0F);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if(amux == 0xFF) new_value |= (value0 & 0x1F);
	    else new_value |= (amux & 0x1F);
	    if(tp_gnd == 0xFF) new_value |= (value0 & 0x20);
	    else new_value |= ((tp_gnd & 0x1) << 5);
	    if(tp_en == 0xFF) new_value |= (value0 & 0x40);
	    else new_value |= ((tp_en & 0x1) << 6);
	    if(tp_pol == 0xFF) new_value |= (value0 & 0x80);
	    else new_value |= ((tp_pol & 0x1) << 7 );
	    new_values[addr] = new_value;
	}
	else if( type == "CAL_Ibias" ){
	    if(idmap.find("curr") != idmap.end()) new_values[0x10] = idmap.find("curr")->second;
	}
	else if( type == "CAL_Vcasc" ){
	    if(idmap.find("volt") != idmap.end()) new_values[0x11] = idmap.find("volt")->second;
	}
	else if( type == "PlStLogicSelAndPtWidth" ){
	    unsigned pl_sel(0xFF), st_sel(0xFF), pt_width(0xFF);
	    if( idmap.find("pl_sel") != idmap.end() ) pl_sel = idmap.find("pl_sel")->second;
	    if( idmap.find("st_sel") != idmap.end() ) st_sel = idmap.find("st_sel")->second;
	    if( idmap.find("pt_width") != idmap.end() ) pt_width = idmap.find("pt_width")->second;
	    //cout << pl_sel << " " << st_sel << " " << pt_width << endl;
	    unsigned addr(0x12);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if( pl_sel == 0xFF ) new_value |= (value0 & 0xC0);
	    else new_value |= ((pl_sel & 0x3) << 6);
	    if( st_sel == 0xFF ) new_value |= (value0 & 0x30);
	    else new_value |= ((st_sel & 0x3) << 4);
	    if( pt_width == 0xFF ) new_value |= (value0 & 0x0F);
	    else new_value |= (pt_width & 0xF);

	    new_values[addr] = new_value;
	}
	else if( type == "CoinWindowOffsets" ){
	    unsigned ofst1(0xFF), ofst2(0xFF), ofst3(0xFF), ofst4(0xFF);
	    if( idmap.find("ofst1") != idmap.end() ) ofst1 = idmap.find("ofst1")->second;
	    if( idmap.find("ofst2") != idmap.end() ) ofst2 = idmap.find("ofst2")->second;
	    if( idmap.find("ofst3") != idmap.end() ) ofst3 = idmap.find("ofst3")->second;
	    if( idmap.find("ofst4") != idmap.end() ) ofst4 = idmap.find("ofst4")->second;
	    if( !(ofst3 == 0xFF && ofst4 == 0xFF) ){
		unsigned addr(0x13);
		unsigned value0(0);
		if(reg_values0.find(addr) != reg_values0.end()) 
		    value0=reg_values0.find(addr)->second;
		unsigned new_value(0);
		if(ofst3 == 0xFF) new_value |= (value0 & 0x0F);
		else new_value |= (ofst3 & 0x0F);
		if(ofst4 == 0xFF) new_value |= (value0 & 0xF0);
		else new_value |= ((ofst4 & 0x0F) << 4);
		new_values[addr] = new_value;
	    }
	    if( !(ofst1 == 0xFF && ofst2 == 0xFF) ){
		unsigned addr(0x14);
		unsigned value0(0);
		if(reg_values0.find(addr) != reg_values0.end()) 
		    value0=reg_values0.find(addr)->second;
		unsigned new_value(0);
		if(ofst1 == 0xFF) new_value |= (value0 & 0x0F);
		else new_value |= (ofst1 & 0x0F);
		if(ofst2 == 0xFF) new_value |= (value0 & 0xF0);
		else new_value |= ((ofst2 & 0x0F) << 4);
		new_values[addr] = new_value;
	    }
	}
	else if( type == "BandgapFuse" ){
	    unsigned ovrd(0xFF), sel(0xFF), bg_fuse(0xFF); 
	    if(idmap.find("override") != idmap.end()) ovrd = idmap.find("override")->second; 
	    if(idmap.find("sel") != idmap.end()) sel = idmap.find("sel")->second; 
	    if(idmap.find("bg_fuse") != idmap.end()) bg_fuse = idmap.find("bg_fuse")->second; 
	    unsigned addr(0x15);
	    unsigned value0(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value0=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if( ovrd == 0xFF ) new_value |= (value0 & 0x80);
	    else new_value |= ( ( ovrd & 1 ) << 7 );
	    if( sel == 0xFF ) new_value |= (value0 & 0x40);
	    else new_value |= ( ( sel & 1 ) << 6 );
	    if( bg_fuse == 0xFF ) new_value |= (value0 & 0x3F);
	    else new_value |= ( bg_fuse & 0x3F );
	    new_values[addr] = new_value;
	}
	else if( type == "ChipIdFuse" ){
	    unsigned id(0xFFFFFF), sel(0xFFFFFF);
	    if(idmap.find("id") != idmap.end()) id = idmap.find("id")->second;
	    if(idmap.find("sel") != idmap.end()) sel = idmap.find("sel")->second;
	    unsigned addr2(0x18);
	    unsigned value2(0);
	    if(reg_values0.find(addr2) != reg_values0.end()) 
		value2=reg_values0.find(addr2)->second;
	    unsigned new_value2(0);
	    if(id == 0xFFFFFF){
	       new_value2 |= (value2 & 0x07);
	    }
	    else{
	       new_values[0x16] = (id & 0x0000FF); 
	       new_values[0x17] = ( (id & 0x00FF00) >> 8 ); 
	       new_value2 |= (id & 0x070000) >> 16;
	    }
	    if(sel == 0xFFFFFF){
		new_value2 |= (value2 & 0x08);
	    }
	    else{
		new_value2 |= ((sel & 1) << 3);
	    }
	    new_values[addr2] = new_value2;
	}
	else if( type == "LayerSwapAndClusterWidth" ){
	    unsigned lswap(0xFF), width(0xFF); 
	    if(idmap.find("lswap") != idmap.end()) lswap = idmap.find("lswap")->second;
	    if(idmap.find("width") != idmap.end()) width = idmap.find("width")->second;
	    unsigned addr(0x1b);
	    unsigned value(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if( lswap == 0xFF ) new_value |= (value & 0x08);
	    else new_value |= ((lswap & 1) << 3);
	    if( width == 0xFF ) new_value |= (value & 0x07);
	    else new_value |= (width & 7);
	    new_values[addr] = new_value;
	}
	else if( type == "40MHzClockOr254DLL" ){
	    unsigned dll(0xFF), test_out_40mhz_clk(0xFF), or254(0xFF), tpg(0xFF);
	    if(idmap.find("dll") != idmap.end()) dll = idmap.find("dll")->second;
	    if(idmap.find("test_out_40mhz_clk") != idmap.end()) test_out_40mhz_clk = idmap.find("test_out_40mhz_clk")->second;
	    if(idmap.find("or254") != idmap.end()) or254 = idmap.find("or254")->second;
	    if(idmap.find("tpg") != idmap.end()) tpg = idmap.find("tpg")->second;
	    unsigned addr(0x1C);
	    unsigned value(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    if(dll == 0xFF) new_value |= (value & 0x1F); 
	    else {
		dll = swap_bits(dll,5);
		new_value |= ( dll & 0x1F);
	    }
	    if( test_out_40mhz_clk == 0xFF ) new_value |= (value & 0x20); 
	    else new_value |= (( test_out_40mhz_clk & 0x01) << 5 );
	    if( or254 == 0xFF ) new_value |= (value & 0x40); 
	    else new_value |= ((or254 & 0x01) << 6 );
	    if( tpg == 0xFF ) new_value |= (value & 0x80); 
	    else new_value |= ((tpg & 0x01) << 7 );
	    new_values[addr] = new_value;
	}
	else if( type == "FciAndError" ){
	    unsigned fci_delay(0xFF);
	    if(idmap.find("fci_delay") != idmap.end()) fci_delay = idmap.find("fci_delay")->second;
	    unsigned addr(0x1D);
	    unsigned value(0);
	    if(reg_values0.find(addr) != reg_values0.end()) 
		value=reg_values0.find(addr)->second;
	    unsigned new_value(0);
	    new_value |= (value & 0x3F);
	    if(fci_delay != 0xFF) new_values[0x1D] = ( new_value | ( (fci_delay & 0x03) << 6 ) );
	}
	else if( type == "MaskChannels" ){
	    for( unsigned addr=0x20; addr <= 0x3F; addr++ ){
		char item[256];
		unsigned offset = (addr-0x20) * 8 + 1;
		sprintf(item, "ch_%03d_%03d", offset, offset + 7 );
		if(addr == 0x3F)sprintf(item, "ch_249_254"); 
		if(idmap.find(item) != idmap.end()) new_values[addr] = idmap.find(item)->second;
	    }
	}
	else if( type == "Bends" ){
	    if(idmap.find("cl_cntr_m7_m6h") != idmap.end()) new_values[0x40] = idmap.find("cl_cntr_m7_m6h")->second;
	    if(idmap.find("cl_cntr_m6_m5h") != idmap.end()) new_values[0x41] = idmap.find("cl_cntr_m6_m5h")->second;
	    if(idmap.find("cl_cntr_m5_m4h") != idmap.end()) new_values[0x42] = idmap.find("cl_cntr_m5_m4h")->second;
	    if(idmap.find("cl_cntr_m4_m3h") != idmap.end()) new_values[0x43] = idmap.find("cl_cntr_m4_m3h")->second;
	    if(idmap.find("cl_cntr_m3_m2h") != idmap.end()) new_values[0x44] = idmap.find("cl_cntr_m3_m2h")->second;
	    if(idmap.find("cl_cntr_m2_m1h") != idmap.end()) new_values[0x45] = idmap.find("cl_cntr_m2_m1h")->second;
	    if(idmap.find("cl_cntr_m1_m0h") != idmap.end()) new_values[0x46] = idmap.find("cl_cntr_m1_m0h")->second;
	    if(idmap.find("cl_cntr_c_p0h" ) != idmap.end()) new_values[0x47] = idmap.find("cl_cntr_c_p0h" )->second;
	    if(idmap.find("cl_cntr_p1_p1h") != idmap.end()) new_values[0x48] = idmap.find("cl_cntr_p1_p1h")->second;
	    if(idmap.find("cl_cntr_p2_p2h") != idmap.end()) new_values[0x49] = idmap.find("cl_cntr_p2_p2h")->second;
	    if(idmap.find("cl_cntr_p3_p3h") != idmap.end()) new_values[0x4a] = idmap.find("cl_cntr_p3_p3h")->second;
	    if(idmap.find("cl_cntr_p4_p4h") != idmap.end()) new_values[0x4b] = idmap.find("cl_cntr_p4_p4h")->second;
	    if(idmap.find("cl_cntr_p5_p5h") != idmap.end()) new_values[0x4c] = idmap.find("cl_cntr_p5_p5h")->second;
	    if(idmap.find("cl_cntr_p6_p6h") != idmap.end()) new_values[0x4d] = idmap.find("cl_cntr_p6_p6h")->second;
	    if(idmap.find("cl_cntr_p7"    ) != idmap.end()) new_values[0x4e] = idmap.find("cl_cntr_p7"    )->second;
	}
	else if( type == "VCTH" ){
	    unsigned vcth(0xFFFF);
	    if(idmap.find("vcth") != idmap.end()) vcth = idmap.find("vcth")->second;
	    if(vcth != 0xFFFF){
		new_values[0x4F] = ( vcth & 0xFF );
		new_values[0x50] = ( ( vcth >> 8 ) & 3 );
	    }
	}
	else if( type == "TestIONNTop" ){
	    unsigned in_A(0xFFFF), in_B(0xFFFF), mode_sel(0xFFFF);
	    if(idmap.find("in_A") != idmap.end()) in_A = idmap.find("in_A")->second;
	    if(idmap.find("in_B") != idmap.end()) in_B = idmap.find("in_B")->second;
	    if(idmap.find("mode_sel") != idmap.end()) mode_sel = idmap.find("mode_sel")->second;

	    unsigned addr2(0x51);
	    unsigned value2(0);
	    if(reg_values0.find(addr2) != reg_values0.end()){

		value2=reg_values0.find(addr2)->second;
	    }
	    unsigned new_value2(0);

	    if(in_B == 0xFFFF){
		new_value2 |= (value2 & 0xF8);
	    }
	    else{
		new_values[0x52] = (in_B >> 5); 
		new_value2 |= (in_B & 0x1F) << 3;
	    }
	    if(in_A == 0xFFFF){
		new_value2 |= (value2 & 0x07);
	    }
	    else{
		new_value2 |= (in_A & 0x07);
	    }
	    new_values[addr2] = new_value2;

	    if(mode_sel != 0xFFFF){
		new_values[0x53] = (mode_sel & 3);
	    }
	}
	else if( type == "TestIONNBottom" ){
	    unsigned in_A(0xFFFF), in_B(0xFFFF), mode_sel(0xFFFF);
	    if(idmap.find("in_A") != idmap.end()) in_A = idmap.find("in_A")->second;
	    if(idmap.find("in_B") != idmap.end()) in_B = idmap.find("in_B")->second;
	    if(idmap.find("mode_sel") != idmap.end()) mode_sel = idmap.find("mode_sel")->second;

	    unsigned addr2(0x55);
	    unsigned value2(0);
	    if(reg_values0.find(addr2) != reg_values0.end()){
		value2=reg_values0.find(addr2)->second;
	    }
	    unsigned new_value2(0);
	    if(in_B == 0xFFFF){
		new_value2 |= (value2 & 0xFC);
	    }
	    else{
		new_value2 |= ((in_B & 0x3F) << 2);
	    }
	    if(in_A == 0xFFFF){
		new_value2 |= (value2 & 0x03);
	    }
	    else{
		new_value2 |= (in_A & 0x03);
	    }
	    new_values[addr2] = new_value2;

	    unsigned addr3(0x56);
	    unsigned value3(0);
	    if(reg_values0.find(addr3) != reg_values0.end()){
		value3=reg_values0.find(addr3)->second;
	    }
	    unsigned new_value3(0);
	    if(in_B == 0xFFFF){
		new_value3 |= (value3 & 0x3F);
	    }
	    else{
		new_value3 |= ((in_B & 0xFC0) >> 6);
	    }
	    if(mode_sel == 0xFFFF){
		new_value3 |= (value3 & 0xC0);
	    }
	    else{
		new_value3 |= ((mode_sel & 3) << 6);
	    }
	    new_values[addr3] = new_value3;
	}
	else if( type == "Offsets" ){
	    for( int i=0x01; i <= 0xFF; i++ ){
		char item[256];
		if( i == 0xFF ) sprintf(item, "dummy");
		else sprintf(item, "ch_%03d", i);
		if(idmap.find(item) != idmap.end()) new_values[i] = idmap.find(item)->second;
	    }
	}
	return new_values;
    }
    void cbci2creg_formatter::getValues( CBCI2C_TYPE_DATA &type_data, const CBCI2C_ADDR_DATA_MAP &reg_addr_values, bool CBC3_0 )
    {
	const string &type              = type_data.first; 
	CBCI2C_ITEM_DATA_MAP &tid_map   = type_data.second; 
	//cout << "getValues() type = " << type << endl; 

	if( type == "FcCntrlCompBetaTrigLat" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x00) == reg_addr_values.end() ) tid_map.clear();
	    else{
		if( tid_map.find("trig_lat")  != tid_map.end() ) tid_map["trig_lat"]  = (((reg_addr_values.find(0x00)->second & 0x01) << 8) | reg_addr_values.find(0x01)->second);  
		if( tid_map.find("beta_mult")  != tid_map.end() ) tid_map["beta_mult"] =  ((reg_addr_values.find(0x00)->second & 0x02) >> 1); 
		if( tid_map.find("comp_hyst") != tid_map.end() ) tid_map["comp_hyst"] =  ((reg_addr_values.find(0x00)->second & 0x3C) >> 2);
		if( tid_map.find("comp_pol")  != tid_map.end() ) tid_map["comp_pol"]  =  ((reg_addr_values.find(0x00)->second & 0x40) >> 6);
		if( tid_map.find("fc_page")   != tid_map.end() ) tid_map["fc_page"]   =  ((reg_addr_values.find(0x00)->second & 0x80) >> 7);
	    }
	}
	else if( type == "BetaMultiplierAndSLVS" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x02) == reg_addr_values.end() ) tid_map.clear();
	    else{
		if( tid_map.find("slvs_curr") != tid_map.end() ) tid_map["slvs_curr"] =  (reg_addr_values.find(0x02)->second & 0x0F);
		if( tid_map.find("beta_mult") != tid_map.end() ) tid_map["beta_mult"] = ((reg_addr_values.find(0x02)->second & 0xF0) >> 4);
	    }
	}
	else if( type == "Ipre1" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x03) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x03)->second;
	}
	else if( type == "Ipre2" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x04) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x04)->second;
	}
	else if( type == "Ipsf" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x05) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x05)->second;
	}
	else if( type == "Ipa" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x06) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x06)->second;
	}
	else if( type == "Ipaos" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x07) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x07)->second;
	}
	else if( type == "Icomp" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x09) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["curr"] = reg_addr_values.find(0x09)->second;
	}
	else if( type == "VPLUS" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x0B) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("vplus1") != tid_map.end() ) tid_map["vplus1"] = (reg_addr_values.find(0x0B)->second & 0x0F);
		if( tid_map.find("vplus2") != tid_map.end() ) tid_map["vplus2"] = ((reg_addr_values.find(0x0B)->second & 0xF0) >> 4);
	    }
	}
	else if( type == "HipAndTestMode" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x0C) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("hip_count") != tid_map.end() )tid_map["hip_count"] = ((reg_addr_values.find(0x0C)->second & 0xE0) >> 5);
		if( tid_map.find("hip_supp")  != tid_map.end() )tid_map["hip_supp"]  = ((reg_addr_values.find(0x0C)->second & 0x10) >> 4);
		if( tid_map.find("hip_src")   != tid_map.end() )tid_map["hip_src"]   = ((reg_addr_values.find(0x0C)->second & 0x08) >> 3);
		if( tid_map.find("sel_clk40_ref")   != tid_map.end() )tid_map["sel_clk40_ref"]   = ((reg_addr_values.find(0x0C)->second & 0x04) >> 2);
		if( tid_map.find("sel_clk40_dll")   != tid_map.end() )tid_map["sel_clk40_dll"]   = ((reg_addr_values.find(0x0C)->second & 0x02) >> 1);
		if( tid_map.find("slvs_off")  != tid_map.end() )tid_map["slvs_off"]  =  (reg_addr_values.find(0x0C)->second & 0x01);
	    }
	}
	else if( type == "TestPulsePotentiometer" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x0D) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["pot"] = (255-reg_addr_values.find(0x0D)->second);
	}
	else if( type == "TestPulseDelayGroup" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x0E) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("delay")  != tid_map.end() )tid_map["delay"] = swap_bits( reg_addr_values.find(0x0E)->second >> 3, 5 );
		if( tid_map.find("group")  != tid_map.end() )tid_map["group"] = swap_bits( reg_addr_values.find(0x0E)->second & 0x07, 3 );
	    }
	}
	else if( type == "TestPulsePolEnAMux" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x0F) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("amux")   != tid_map.end() )tid_map["amux"]    = ( reg_addr_values.find(0x0F)->second & 0x1F );
		if( tid_map.find("tp_gnd") != tid_map.end() )tid_map["tp_gnd"]  = ((reg_addr_values.find(0x0F)->second & 0x20) >> 5);
		if( tid_map.find("tp_en")  != tid_map.end() )tid_map["tp_en"]   = ((reg_addr_values.find(0x0F)->second & 0x40) >> 6);
		if( tid_map.find("tp_pol") != tid_map.end() )tid_map["tp_pol"]  = ((reg_addr_values.find(0x0F)->second & 0x80) >> 7);
	    }
	}
	else if( type == "CAL_Ibias" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x10) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		tid_map["curr"] = reg_addr_values.find(0x10)->second;
	    }
	}
	else if( type == "CAL_Vcasc" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x11) == reg_addr_values.end() ) tid_map.clear(); 
	    else tid_map["volt"] = reg_addr_values.find(0x11)->second;
	}
	else if( type == "PlStLogicSelAndPtWidth" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x12) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("pl_sel")   != tid_map.end() ) tid_map["pl_sel"]   = ((reg_addr_values.find(0x12)->second & 0xC0) >> 6);
		if( tid_map.find("st_sel")   != tid_map.end() ) tid_map["st_sel"]   = ((reg_addr_values.find(0x12)->second & 0x30) >> 4);
		if( tid_map.find("pt_width") != tid_map.end() ) tid_map["pt_width"] =  (reg_addr_values.find(0x12)->second & 0x0F);
	    }
	}                                                                                          
	else if( type == "CoinWindowOffsets" ){
	    if( (tid_map.find("ofst3") != tid_map.end() || tid_map.find("ofst4") != tid_map.end()) && reg_addr_values.find(0x13) == reg_addr_values.end()){ 
		tid_map.erase("ofst3"); tid_map.erase("ofst4");
	    }
	    else{
		if( tid_map.find("ofst3") != tid_map.end() ) tid_map["ofst3"]   = (reg_addr_values.find(0x13)->second & 0x0F);
		if( tid_map.find("ofst4") != tid_map.end() ) tid_map["ofst4"]   = ((reg_addr_values.find(0x13)->second & 0xF0) >> 4);
	    }
	    if( (tid_map.find("ofst1") != tid_map.end() || tid_map.find("ofst2") != tid_map.end()) && reg_addr_values.find(0x14) == reg_addr_values.end()){
		tid_map.erase("ofst1"); tid_map.erase("ofst2");
	    }
	    else{
		if( tid_map.find("ofst1") != tid_map.end() ) tid_map["ofst1"]   = (reg_addr_values.find(0x14)->second & 0x0F);
		if( tid_map.find("ofst2") != tid_map.end() ) tid_map["ofst2"]   = ((reg_addr_values.find(0x14)->second & 0xF0) >> 4);
	    }
	}
	else if( type == "BandgapFuse" ){
	    if(tid_map.size() != 0 && reg_addr_values.find(0x15) == reg_addr_values.end() ) tid_map.clear(); 
	    else{
		if( tid_map.find("override") != tid_map.end()) tid_map["override"] = ((reg_addr_values.find(0x15)->second & 0x80) >> 7);
		if( tid_map.find("sel")      != tid_map.end()) tid_map["sel"]      = ((reg_addr_values.find(0x15)->second & 0x40) >> 6);
		if( tid_map.find("bg_fuse")  != tid_map.end()) tid_map["bg_fuse"]  =  (reg_addr_values.find(0x15)->second & 0x3F);
	    }
	}
	else if( type == "ChipIdFuse" ){
	    if( tid_map.size() != 0 && reg_addr_values.find(0x18) == reg_addr_values.end() ) tid_map.clear(); 
	    else if( tid_map.find("id") != tid_map.end() && ( reg_addr_values.find(0x17) == reg_addr_values.end() || reg_addr_values.find(0x16) == reg_addr_values.end() ) ){
		tid_map.erase("id");
	    }
	    else{
		if( tid_map.find("id")  != tid_map.end()){
		    tid_map["id"]  = ((reg_addr_values.find(0x18)->second & 0x07) << 16) | (reg_addr_values.find(0x17)->second << 8) | reg_addr_values.find(0x16)->second;
		    //cout << std::hex << tid_map["id"] << endl;
		    //cout << std::hex << reg_addr_values.find(0x16)->second << " " << reg_addr_values.find(0x17)->second << " " << ((reg_addr_values.find(0x18)->second & 0x07) << 16) << " " << endl; 
		}
		if( tid_map.find("sel") != tid_map.end()) tid_map["sel"] = (reg_addr_values.find(0x18)->second & 0x08) >> 3;
	    }
	}
	else if( type == "LayerSwapAndClusterWidth" ){
	    if( tid_map.size() != 0 && reg_addr_values.find(0x1b) == reg_addr_values.end() )tid_map.clear(); 
	    else{
		if( tid_map.find("lswap") != tid_map.end()) tid_map["lswap"] = (reg_addr_values.find(0x1b)->second & 0x08) >> 3;
		if( tid_map.find("width") != tid_map.end()) tid_map["width"] = (reg_addr_values.find(0x1b)->second & 0x07);
	    }
	}
	else if( type == "40MHzClockOr254DLL" ){
	    if( tid_map.size() != 0 && reg_addr_values.find(0x1C) == reg_addr_values.end() )tid_map.clear();
	    else{
		if( tid_map.find("dll")                != tid_map.end()) {
		    unsigned dll = reg_addr_values.find(0x1C)->second & 0x1F;
		    dll = swap_bits(dll,5);
		    tid_map["dll"]                = dll;
		}
		if( tid_map.find("test_out_40mhz_clk") != tid_map.end()) tid_map["test_out_40mhz_clk"] = ((reg_addr_values.find(0x1C)->second & 0x20) >> 5 );
		if( tid_map.find("or254")              != tid_map.end()) tid_map["or254"]              = ((reg_addr_values.find(0x1C)->second & 0x40) >> 6);
		if( tid_map.find("tpg")                != tid_map.end()) tid_map["tpg"]                = ((reg_addr_values.find(0x1C)->second & 0x80) >> 7);
	    }
	}
	else if( type == "FciAndError" ){
	    if( tid_map.size() != 0 && reg_addr_values.find(0x1D) == reg_addr_values.end() )tid_map.clear();
	    else{
		if( tid_map.find("err_bad_code")  != tid_map.end()) tid_map["err_bad_code"] = ((reg_addr_values.find(0x1D)->second >> 0) & 1); 
		if( tid_map.find("err_sync_stat") != tid_map.end()) tid_map["err_sync_stat"]= ((reg_addr_values.find(0x1D)->second >> 1) & 1); 
		if( tid_map.find("err_sync_lost") != tid_map.end()) tid_map["err_sync_lost"]= ((reg_addr_values.find(0x1D)->second >> 2) & 1); 
		if( tid_map.find("err_lat_err")   != tid_map.end()) tid_map["err_lat_err"]  = ((reg_addr_values.find(0x1D)->second >> 3) & 1); 
		if( tid_map.find("err_ram_ofl")   != tid_map.end()) tid_map["err_ram_ofl"]  = ((reg_addr_values.find(0x1D)->second >> 4) & 1); 
		if( tid_map.find("fci_delay")     != tid_map.end()) tid_map["fci_delay"]    = ((reg_addr_values.find(0x1D)->second >> 6) & 3);
	    }
	}
	else if( type == "MaskChannels" ){
	    for( unsigned addr=0x20; addr <= 0x3F; addr++ ){
		char item[256];
		unsigned offset = (addr-0x20) * 8 + 1;
		sprintf(item, "ch_%03d_%03d", offset, offset + 7 );
		//cout << item << " " << addr << endl;
		if(addr == 0x3F)sprintf(item, "ch_249_254"); 
		if(tid_map.find(item) != tid_map.end() && reg_addr_values.find(addr) == reg_addr_values.end() )tid_map.erase(item);
		else{
		    if(tid_map.find(item) != tid_map.end()) tid_map[item] = reg_addr_values.find(addr)->second; 
		}
	    }
	}
	else if( type == "Bends" ){
	    unsigned addr(0);
	    string item;
	    item = "cl_cntr_m7_m6h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x40;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m6_m5h";
	    if(tid_map.find(item) != tid_map.end()){ 
		addr = 0x41;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m5_m4h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x42;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m4_m3h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x43;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m3_m2h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x44;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m2_m1h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x45;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_m1_m0h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x46;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_c_p0h" ; 
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x47;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p1_p1h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x48;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p2_p2h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x49;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p3_p3h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x4a;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p4_p4h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x4b;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p5_p5h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x4c;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p6_p6h";
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x4d;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	    item = "cl_cntr_p7"    ;
	    if(tid_map.find(item) != tid_map.end()){
		addr = 0x4e;
		if( reg_addr_values.find(addr) == reg_addr_values.end() ) tid_map.erase(item);
		else tid_map[item] = reg_addr_values.find(addr)->second;
	    }
	}                                                              
	else if( type == "VCTH" ){                                     
	    if( reg_addr_values.find(0x50) == reg_addr_values.end() || reg_addr_values.find(0x4f) == reg_addr_values.end()) tid_map.erase("vcth"); 
	    else tid_map["vcth"] = ((( reg_addr_values.find(0x50)->second & 3 ) << 8) | reg_addr_values.find(0x4f)->second);  
	}
	else if( type == "TestIONNTop" ){
	    if( tid_map.find("in_A") != tid_map.end() && ( reg_addr_values.find(0x51) == reg_addr_values.end() ) ){
		tid_map.erase("in_A");
	    }
	    else if(tid_map.find("in_A") != tid_map.end()) 
		tid_map["in_A"] = ( reg_addr_values.find(0x51)->second & 7 ); 

	    if( tid_map.find("in_B") != tid_map.end() && 
		    ( reg_addr_values.find(0x51) == reg_addr_values.end() || 
		      reg_addr_values.find(0x52) == reg_addr_values.end() ) ){ 
		tid_map.erase("in_B");
	    }
	    else if(tid_map.find("in_B") != tid_map.end())  
		tid_map["in_B"] = (reg_addr_values.find(0x52)->second << 5 ) | ((( reg_addr_values.find(0x51)->second ) >> 3 ) & 0x1F ); 

	    if( tid_map.find("mode_sel") != tid_map.end() && ( reg_addr_values.find(0x53) == reg_addr_values.end() ) ){
		tid_map.erase("mode_sel");
	    }
	    else if(tid_map.find("mode_sel") != tid_map.end()){
	       	tid_map["mode_sel"] = (reg_addr_values.find(0x53)->second & 0x03);
	    }

	    if( tid_map.find("out_A") != tid_map.end() && ( reg_addr_values.find(0x53) == reg_addr_values.end() ) ){
		tid_map.erase("out_A");
	    }
	    else if(tid_map.find("out_A") != tid_map.end()){
		tid_map["out_A"] = ( reg_addr_values.find(0x53)->second >> 2 ) & 0x03; 
	    }

	    if( tid_map.find("out_B") != tid_map.end() && 
		    ( reg_addr_values.find(0x53) == reg_addr_values.end() || 
		      reg_addr_values.find(0x54) == reg_addr_values.end()  ) ){
		tid_map.erase("out_B");
	    }
	    else if( tid_map.find("out_B") != tid_map.end()){ 
		tid_map["out_B"] = (reg_addr_values.find(0x54)->second << 4 ) | (( reg_addr_values.find(0x53)->second >> 4 ) & 0x0F ); 
	    }
	}
	else if( type == "TestIONNBottom" ){
	    if( tid_map.find("in_A") != tid_map.end() && ( reg_addr_values.find(0x55) == reg_addr_values.end() ) ){
		tid_map.erase("in_A");
	    }
	    else if(tid_map.find("in_A") != tid_map.end()) 
		tid_map["in_A"] = ( reg_addr_values.find(0x55)->second & 3 ); 

	    if( tid_map.find("in_B") != tid_map.end() && 
		    ( reg_addr_values.find(0x55) == reg_addr_values.end() || 
		      reg_addr_values.find(0x56) == reg_addr_values.end()  ) ){
		tid_map.erase("in_B");
	    }
	    else if(tid_map.find("in_B") != tid_map.end())  
		tid_map["in_B"] = (( reg_addr_values.find(0x56)->second << 6 ) & 0xFC0) | ((( reg_addr_values.find(0x55)->second ) >> 2 ) & 0x3F ); 

	    if( tid_map.find("mode_sel") != tid_map.end() && ( reg_addr_values.find(0x56) == reg_addr_values.end() ) ){
		tid_map.erase("mode_sel");
	    }
	    else if(tid_map.find("mode_sel") != tid_map.end())
		tid_map["mode_sel"] = (reg_addr_values.find(0x56)->second >> 6) & 0x03; 

	    if( tid_map.find("out_A") != tid_map.end() && ( reg_addr_values.find(0x57) == reg_addr_values.end() ) ){
		tid_map.erase("out_A");
	    }
	    else if(tid_map.find("out_A") != tid_map.end()){
		tid_map["out_A"] = reg_addr_values.find(0x57)->second & 0x07; 
	    }

	    if( tid_map.find("out_B") != tid_map.end() && 
		    ( reg_addr_values.find(0x57) == reg_addr_values.end() || 
		      reg_addr_values.find(0x58) == reg_addr_values.end()  ) ){
		tid_map.erase("out_B");
	    }
	    else if( tid_map.find("out_B") != tid_map.end()){ 
		tid_map["out_B"] = (reg_addr_values.find(0x58)->second << 5 ) | (( reg_addr_values.find(0x57)->second >> 3 ) & 0x1F ); 
	    }
	}
	else if( type == "Offsets" ){
	    for( int i=1; i <= 255; i++ ){
		char item[256];
		if( i == 255 ) sprintf(item, "dummy");
		else sprintf(item, "ch_%03d", i);
		if( tid_map.find(item) != tid_map.end() && reg_addr_values.find(i) == reg_addr_values.end() )tid_map.erase(item); 
		else if(tid_map.find(item) != tid_map.end()) tid_map[item] = reg_addr_values.find(i)->second;
	    }
	}
    }
    uint32_t get_ipbus_command_i2cregrw( unsigned w, unsigned r, unsigned page, unsigned addr, unsigned value, unsigned fe_id, unsigned cbc_id )
    {
	return ( (fe_id&CBC_I2C_COMMAND_BUS_ID_BIT_MASK) << CBC_I2C_COMMAND_BUS_ID_BIT_OFFSET) 
	    | ( (cbc_id&CBC_I2C_COMMAND_CBC_ID_BIT_MASK) << CBC_I2C_COMMAND_CBC_ID_BIT_OFFSET) 
	    | ( (r&CBC_I2C_COMMAND_READ_BIT_MASK) << CBC_I2C_COMMAND_READ_BIT_OFFSET) 
	    | ( (w&CBC_I2C_COMMAND_WRITE_BIT_MASK) << CBC_I2C_COMMAND_WRITE_BIT_OFFSET) 
	    | ( ((page-1)&CBC_I2C_COMMAND_PAGE_BIT_MASK) << CBC_I2C_COMMAND_PAGE_BIT_OFFSET) 
	    | ( (addr&CBC_I2C_COMMAND_ADDR_BIT_MASK) << CBC_I2C_COMMAND_ADDR_BIT_OFFSET) 
	    | ( (value&CBC_I2C_COMMAND_VALUE_BIT_MASK) << CBC_I2C_COMMAND_VALUE_BIT_OFFSET); 
    }
    void get_ipbus_commands_i2cregrw( unsigned w, unsigned r, const std::string &filename, std::vector<uint32_t> &commands, unsigned &nreplies, unsigned fe_id, unsigned cbc_id )
    {
	if( fe_id == 0 || cbc_id == 0 ){
	    cerr << "broadcast is not allowed for this function." << endl;
	    return;
	}
	/*
	if( w == 1 ){
	    cout << "+++++++++++++++++++++++++++" << endl;
	    cout << "IPBUS commands are created to write CBCI2C registers for CBC ID = " << cbc_id << endl;
	}
	*/
	ifstream fs( filename.c_str() );
	char line[256];
	int page, addr, value(0);
	if( fs.is_open() ){
	    while( !fs.eof() ){
		fs.getline( line, 256 );
		//		    cout << line << endl;
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
	//	    if( w == 1 && line[0] == '#' ) cout << line << endl;
		    continue;
		}
		istringstream iss( line );
		string addr_str, value_str;
		iss >> page >> addr_str >> value_str;
		if( !( page == 1 || page == 2 ) ) continue;
		addr = strtol( addr_str.c_str(), 0, 0 );
		value = strtol( value_str.c_str(), 0, 0 );
		commands.push_back( get_ipbus_command_i2cregrw( w, r, page, addr, value, fe_id, cbc_id ) ); 
	    }
	}
	else{
	    ostringstream oss;
	    oss << "File " << filename << " could not be opened.";
	    throw CbcI2cRegIpbusException(oss.str());
	}
	/*
	if( w == 1 ){
	    cout << "+++++++++++++++++++++++++++" << endl;
	}
	*/
	if( w == 1 ) nreplies += commands.size();
	if( r == 1 ) nreplies += commands.size();
    }
    void get_cbci2c_type_data( string &name, unsigned value, CBCI2C_TYPE_DATA *type_data )
    {
	char name_c[256];
	strcpy(name_c, name.c_str());
	string type = strtok( name_c, "." ); 
	char *tmp = strtok( 0, "." ); 

	if(tmp){
	    string type_item(tmp);
	    type_data->first = type;
	    type_data->second[type_item] = value;
	}
	else{
	//    cout << "tmp is null" << endl;
	    const CBCI2C_NAMES &names = cbci2creg_formatter::getNames();
	    if( names.find(type) != names.end() ){
		set<CBCI2C_ITEM> items = names.find(type)->second;
		set<CBCI2C_ITEM>::iterator it = items.begin();
		for(; it != items.end(); it++){
		    type_data->first = type;
		    type_data->second[*it] = value; 
		}
	    }
	}
    }
    void get_cbci2c_type_data( string &name, unsigned value, CBCI2C_TYPE_DATA_MAP *map )
    {
	char name_c[256];
	strcpy(name_c, name.c_str());
	string type = strtok( name_c, "." ); 
	CBCI2C_TYPE_DATA type_data;
	get_cbci2c_type_data(name,value, &type_data );
	(*map)[type] = type_data.second;
	/*
	char *tmp = strtok( 0, "." ); 
	if(tmp){
	    string type_item(tmp);
	    (*map)[type][type_item] = value;
	}
	else{
	    const CBCI2C_NAMES &names = cbci2creg_formatter::getNames();
	    if( names.find(type) != names.end() ){
		set<CBCI2C_ITEM> items = names.find(type)->second;
		set<CBCI2C_ITEM>::iterator it = items.begin();
		for(; it != items.end(); it++){
		    (*map)[type][*it] = value; 
		}
	    }
	}
	*/
    }
    void get_cbci2c_name_value_map( const CBCI2C_TYPE_DATA_MAP &map, std::map<std::string,unsigned> *name_value_map )
    {
	char name[256];
	CBCI2C_TYPE_DATA_MAP::const_iterator it = map.begin();
	for(; it != map.end(); it++){
	    string type = it->first;
	    const CBCI2C_ITEM_DATA_MAP &item_data_map = it->second;
	    CBCI2C_ITEM_DATA_MAP::const_iterator item_it = item_data_map.begin();
	    for(; item_it != item_data_map.end(); item_it++){
		CBCI2C_ITEM item = item_it->first;
		CBCI2C_DATA data = item_it->second;	
		sprintf(name,"%s.%s", type.c_str(), item.c_str()); 
		(*name_value_map)[name] = data;
	    }
	}
    }

    void get_cbci2c_type_data_map( const std::map<std::string,unsigned> &name_value_map, CBCI2C_TYPE_DATA_MAP *map )
    {
	char name_c[256];
	std::map<std::string,unsigned>::const_iterator it = name_value_map.begin();
	for(; it != name_value_map.end(); it++){
	    string name = it->first;
	    unsigned value = it->second;
	    strcpy(name_c, name.c_str());
	    string type = strtok( name_c, "." ); 
	    char *tmp = strtok( 0, "." ); 
	    if(tmp){
		string type_item(tmp);
		CBCI2C_ITEM_DATA item_data(type_item,value);
		(*map)[type][type_item] = value;
	    }
	    else{
		const CBCI2C_NAMES &names = cbci2creg_formatter::getNames();
		if( names.find(type) != names.end() ){
		    set<CBCI2C_ITEM> items = names.find(type)->second;
		    set<CBCI2C_ITEM>::iterator it = items.begin();
		    for(; it != items.end(); it++){
			(*map)[type][*it] = value; 
		    }
		}
	    }
	}
    }

    void get_cbci2c_type_data_map( const std::string &filename, CBCI2C_TYPE_DATA_MAP *map )
    {
	ifstream fs( filename.c_str() );
	char line[256];
	if( fs.is_open() ){
	    while( !fs.eof() ){
		fs.getline( line, 256 );
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		    continue;
		}
		istringstream iss( line );
		int id(0);
		char name[256];
		string value_str;
		iss >> id >> name >> value_str;
		if( id != 3 ) continue;
		unsigned value = strtol( value_str.c_str(), 0, 0 );
		string type = strtok( name, "." ); 
		char *tmp = strtok( 0, "." ); 
		if(tmp){
		    string type_item(tmp);
		    (*map)[type][type_item] = value;
		}
		else{
		    const CBCI2C_NAMES &names = cbci2creg_formatter::getNames();
		    if( names.find(type) != names.end() ){
			set<CBCI2C_ITEM> items = names.find(type)->second;
			set<CBCI2C_ITEM>::iterator it = items.begin();
			for(; it != items.end(); it++){
			    (*map)[type][*it] = value; 
			}
		    }
		}
	    }
	}
	else{
	    ostringstream oss;
	    oss << "File " << filename << " could not be opened.";
	    throw CbcI2cRegIpbusException(oss.str());
	}
    }
    std::string get_title_regcnf( const std::string &filename ){

	string title;

	ifstream fs( filename.c_str() );
	char line[256];
	if( fs.is_open() ){
	    while( !fs.eof() ){
		fs.getline( line, 256 );
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		    string line_s(line);
		    if( line_s.find("Title") != string::npos )
			title = line_s;
		}
		else break;
	    }
	}
	else{
	    ostringstream oss;
	    oss << "File " << filename << " could not be opened.";
	    throw CbcI2cRegIpbusException(oss.str());
	}
	return title;
    }
    void print_preamble( const std::string &title, std::ostream &os ){

	os << title << endl;
	os << "# Fields: Page Address Value" << endl;
	os << "# FieldTypes: int int int" << endl;
    }
    std::map<string, unsigned > get_name_regcnf( const std::string &filename, bool CBC3_0 ){

	map<string,unsigned> regmap;

	map<unsigned,map<unsigned,unsigned> > p1p2reg_map;
	map<string,unsigned> p3reg_map;

	ifstream fs( filename.c_str() );
	char line[256];
	int page, addr, value(0);
	if( fs.is_open() ){
	    while( !fs.eof() ){
		fs.getline( line, 256 );
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		    continue;
		}
//		cout << line << endl;
		istringstream iss( line );
		string addr_str, value_str, name;
		iss >> page;
		if( page == 1 || page == 2 ){
		    iss >> addr_str >> value_str;
		    addr = strtol( addr_str.c_str(), 0, 0 );
		    if(addr == 0x00) page = 1;
		    value = strtol( value_str.c_str(), 0, 0 );
		    p1p2reg_map[page][addr] = value;
		 //   cout << page << " " << addr << endl;
		}
		else if( page == 3 ){
		    iss >> name >> value_str;
		    value = strtol( value_str.c_str(), 0, 0 );
//		    cout << name << " " << value << endl;
		    p3reg_map[name] = value;
		}
	    }
	    //address value map to type data map 
	    CBCI2C_TYPE_DATA_MAP type_data_map;
	    //unsigned nitems(0);
	    const set<CBCI2C_TYPE> types = cbci2creg_formatter::getTypeNames();
	    set<CBCI2C_TYPE>::const_iterator it_types = types.begin();
	    for(; it_types != types.end(); it_types ++ ){
		const string &type = *it_types;
		//cout << type << endl;
		unsigned page(1);
		if(type == "Offsets") page = 2;
		//vector<unsigned> addrs = cbci2creg_formatter::getAddrs(type);
		CBCI2C_ITEM_DATA_MAP item_data;
		const std::set<CBCI2C_ITEM> &items = cbci2creg_formatter::getItemNames( type );
		//nitems += items.size();
		std::set<CBCI2C_ITEM>::const_iterator it_items = items.begin();
		for(; it_items != items.end(); it_items++) item_data[*it_items] = -1; 
		CBCI2C_TYPE_DATA type_data(type, item_data);
		cbci2creg_formatter::getValues(type_data, p1p2reg_map[page], CBC3_0);
		type_data_map[type_data.first] = type_data.second;
	    }
	    //the type data map to item data map
	    get_cbci2c_name_value_map( type_data_map, &regmap );
	    //item data configuration in the files are put in the item data map from address value configuration. 
	    //These item data configuration overwrite the address value configuration.
	    map<string,unsigned>::iterator it = p3reg_map.begin();
	    for(; it != p3reg_map.end(); it++){
		regmap[it->first] = it->second;
	    }	
	}
	else{
	    ostringstream oss;
	    oss << "File " << filename << " could not be opened.";
	    throw CbcI2cRegIpbusException(oss.str());
	}
	return regmap;

    }
    void print_name_regcnf( const std::string &filename, std::ostream &os ){

	string title = get_title_regcnf(filename);
	title = title + " *** Converted to name data list at " + get_currtime();
	print_preamble( title, os );

	map<string,unsigned> regmap = get_name_regcnf(filename);

	map<string,unsigned>::iterator it = regmap.begin();
	for(; it != regmap.end(); it++){
	    os << "3" << setw(50) << it->first << setw(10) << it->second << endl;
	}
    }
	
    void print_name_regcnf( const std::vector<std::string> &filenames, std::ostream &os ){

	string title("# Title: CBC I2C Register setting from merging files : ");

	map<string,unsigned> regmap;

	char full_path[PATH_MAX];

	for(unsigned i=0; i < filenames.size(); i++){
	    realpath(filenames.at(i).c_str(), full_path);
	    title = title + full_path + " ";
	    map<string,unsigned> tmpmap = get_name_regcnf(filenames.at(i));
	    map<string,unsigned>::iterator it_tmpmap = tmpmap.begin();
	    for(; it_tmpmap != tmpmap.end(); it_tmpmap++){
		regmap[it_tmpmap->first] = it_tmpmap->second;
	    }
	}

	title += get_currtime();
	os << title << endl;
	os << "# Fields: Page Address Value" << endl;
	os << "# FieldTypes: int int int" << endl;

	map<string,unsigned>::iterator it = regmap.begin();
	for(; it != regmap.end(); it++){
	    os << "3" << setw(50) << it->first << setw(10) << it->second << endl;
	}
    }
    void print_regcnf( const std::string &filename, std::ostream &os, bool CBC3_0 ){

	string title = get_title_regcnf(filename);
	title = title + " *** Converted to address data list at " + get_currtime();
	print_preamble( title, os );

	map<string,unsigned> regmap = get_name_regcnf(filename);

	CBCI2C_TYPE_DATA_MAP type_data_map;
	get_cbci2c_type_data_map( regmap, &type_data_map );

	map<unsigned,map<unsigned,unsigned> > p1p2reg_map;

	CBCI2C_TYPE_DATA_MAP::iterator it = type_data_map.begin();
	for(; it != type_data_map.end(); it++){
	    CBCI2C_ITEM_DATA_MAP item_data_map = it->second;
	    const std::set<CBCI2C_ITEM> &items = cbci2creg_formatter::getItemNames( it->first );
	    std::set<CBCI2C_ITEM>::const_iterator it_items = items.begin();
	    for(; it_items != items.end(); it_items++){
		if( item_data_map.find(*it_items) == item_data_map.end() ){
		    cerr << it->first << "." << *it_items << " is not defined." << endl;
		} 
	    }
	    CBCI2C_ADDR_DATA_MAP reg_values0;
	    /*
	    vector<unsigned> addrs = cbci2creg_formatter::getReadAddrsToWrite(*it);
	    for(unsigned i=0; i<addrs.size(); i++) reg_values0[addrs.at(i)] = 0;
	    */
	    map<unsigned,unsigned> reg_addr_values = cbci2creg_formatter::getI2cRegValuesToWrite( *it, reg_values0, CBC3_0 ); 
	    unsigned page(1);
	    if( it->first == "Offsets" )page = 2;
	    map<unsigned,unsigned> &map_the_page = p1p2reg_map[page];
	    map<unsigned,unsigned>::iterator it_reg_addr_values = reg_addr_values.begin();
	    for(; it_reg_addr_values != reg_addr_values.end(); it_reg_addr_values++){
		map_the_page[it_reg_addr_values->first] = it_reg_addr_values->second;
	    }
	}
	map<unsigned,map<unsigned,unsigned> >::iterator it_p1p2reg_map = p1p2reg_map.begin();
	for(; it_p1p2reg_map != p1p2reg_map.end(); it_p1p2reg_map++){
	    unsigned page = it_p1p2reg_map->first;
	    map<unsigned, unsigned> &reg_map = it_p1p2reg_map->second;
	    map<unsigned,unsigned>::iterator it_reg_map = reg_map.begin();
	    for(; it_reg_map != reg_map.end(); it_reg_map++){
		os << page << " "
		    << "0x" << setfill('0') << setw(2) << std::right << std::hex << it_reg_map->first << " "
		    << "0x" << setfill('0') << setw(2) << std::right << std::hex << it_reg_map->second << endl; 
	    }
	}
    }
    void print_regcnf( const std::vector<std::string> &filenames, std::ostream &os, bool CBC3_0 ){

	string title("# Title: CBC I2C Register setting from merging files : ");

	map<string,unsigned> regmap;

	char full_path[PATH_MAX];

	for(unsigned i=0; i < filenames.size(); i++){
	    realpath(filenames.at(i).c_str(), full_path);
	    title = title + full_path + " ";
	    map<string,unsigned> tmpmap = get_name_regcnf(filenames.at(i));
	    map<string,unsigned>::iterator it_tmpmap = tmpmap.begin();
	    for(; it_tmpmap != tmpmap.end(); it_tmpmap++){
		regmap[it_tmpmap->first] = it_tmpmap->second;
	    }
	}
	CBCI2C_TYPE_DATA_MAP type_data_map;
	get_cbci2c_type_data_map( regmap, &type_data_map );

	map<unsigned,map<unsigned,unsigned> > p1p2reg_map;

	CBCI2C_TYPE_DATA_MAP::iterator it = type_data_map.begin();
	for(; it != type_data_map.end(); it++){
	    CBCI2C_ITEM_DATA_MAP item_data_map = it->second;
	    const std::set<CBCI2C_ITEM> &items = cbci2creg_formatter::getItemNames( it->first );
	    std::set<CBCI2C_ITEM>::const_iterator it_items = items.begin();
	    for(; it_items != items.end(); it_items++){
		if( item_data_map.find(*it_items) == item_data_map.end() ){
		    cerr << it->first << "." << *it_items << " is not defined." << endl;
		} 
	    }
	    CBCI2C_ADDR_DATA_MAP reg_values0;
	    /*
	    vector<unsigned> addrs = cbci2creg_formatter::getReadAddrsToWrite(*it);
	    for(unsigned i=0; i<addrs.size(); i++) reg_values0[addrs.at(i)] = 0;
	    */
	    map<unsigned,unsigned> reg_addr_values = cbci2creg_formatter::getI2cRegValuesToWrite( *it, reg_values0, CBC3_0 ); 
	    unsigned page(1);
	    if( it->first == "Offsets" )page = 2;
	    map<unsigned,unsigned> &map_the_page = p1p2reg_map[page];
	    map<unsigned,unsigned>::iterator it_reg_addr_values = reg_addr_values.begin();
	    for(; it_reg_addr_values != reg_addr_values.end(); it_reg_addr_values++){
		map_the_page[it_reg_addr_values->first] = it_reg_addr_values->second;
	    }
	}
	title += get_currtime();
	os << title << endl;
	os << "# Fields: Page Address Value" << endl;
	os << "# FieldTypes: int int int" << endl;
	map<unsigned,map<unsigned,unsigned> >::iterator it_p1p2reg_map = p1p2reg_map.begin();
	for(; it_p1p2reg_map != p1p2reg_map.end(); it_p1p2reg_map++){
	    unsigned page = it_p1p2reg_map->first;
	    map<unsigned, unsigned> &reg_map = it_p1p2reg_map->second;
	    map<unsigned,unsigned>::iterator it_reg_map = reg_map.begin();
	    for(; it_reg_map != reg_map.end(); it_reg_map++){
		os << page << " "
		    << "0x" << setfill('0') << setw(2) << std::right << std::hex << it_reg_map->first << " "
		    << "0x" << setfill('0') << setw(2) << std::right << std::hex << it_reg_map->second << endl; 
	    }
	}
    }

    void dump_cbci2c_type_data( const CBCI2C_TYPE_DATA &type_data, std::ostream &os){  

	string type = type_data.first;
	const CBCI2C_ITEM_DATA_MAP &item_data = type_data.second;
	CBCI2C_ITEM_DATA_MAP::const_iterator item_it = item_data.begin();
	for(; item_it != item_data.end(); item_it++){
	    string item = type + "." + item_it->first;
	    os << std::setfill(' ') << setw(50) << std::left << item << " "; 
	    os << setw(3) << std::right << std::dec << item_it->second << endl;
	}
    }
    void dump_cbci2c_type_data_map( const CBCI2C_TYPE_DATA_MAP &map, std::ostream &os){  

	CBCI2C_TYPE_DATA_MAP::const_iterator map_it = map.begin();
       for(; map_it != map.end(); map_it++){
	   string type = map_it->first;
	   const CBCI2C_ITEM_DATA_MAP &item_data = map_it->second;
	   CBCI2C_ITEM_DATA_MAP::const_iterator item_it = item_data.begin();
	   for(; item_it != item_data.end(); item_it++){
	       string item = type + "." + item_it->first;
	       os << std::setfill(' ') << setw(50) << std::left << item << " "; 
	       os << setw(3) << std::right << std::dec << item_it->second << endl;
	   }
       }
    }

    void dump_reply( const CBCI2C_REPLY &reply, std::ostream &os ){
	os  << "fe_id : "     <<  get_bus_id(reply) 
	    << " | cbc_id : " <<  get_cbc_id(reply) 
	    << " | info : "   <<    get_info(reply) 
	    << " | rw : "     << get_rw_flag(reply) 
	    << " | page : "   <<    get_page(reply)
	    << " | addr : "   << "0x" << setfill('0') << setw(2) << std::hex <<  get_addr(reply)
	    << " | value : "  << "0x" << setfill('0') << setw(2) << std::hex <<  get_val(reply) 
	    << std::endl;
    }
    void dump_replies( const std::vector<CBCI2C_REPLY> &replies, std::ostream &os )
    {
	for(unsigned i=0; i<replies.size(); i++ ){
	    dump_reply( replies.at(i), os );
	}
    }
    bool good_replies( const std::vector<CBCI2C_REPLY> &replies)
    {
	for(unsigned i=0; i<replies.size(); i++ ){
	    if( get_info(replies.at(i)) != 0 ) return false;
	}
	return true;
    }
    void dump_replies( const CBCI2C_REGS &regs, std::ostream &os )
    {
	CBCI2C_REGS::const_iterator it = regs.begin();
	for(; it != regs.end(); it++){
	    dump_reply( it->second, os );
	}
    }
    void print_regcnf( const CBCI2C_REGS &regs, std::ostream &os, int cbc_id ) 
    {
	char title[256];
	sprintf( title, "# Title: CBC I2C Register setting of BE_ID = 1 CBC_ID = %d at %s", cbc_id, get_currtime().c_str() );
	os << title << endl;
	os << "# Fields: Page Address Value" << endl;
	os << "# FieldTypes: int int int" << endl;
	CBCI2C_REGS::const_iterator it = regs.begin();
	for(; it != regs.end(); it++){
	    const CBCI2C_REPLY &reply = it->second;
	    if(get_cbc_id(reply) != cbc_id || get_info(reply) != 0 || get_rw_flag(reply) == 0) continue;
	    os << get_page(reply) << " "
		<< "0x" << setfill('0') << setw(2) << std::right << std::hex << get_addr(reply) << " "
		<< "0x" << setfill('0') << setw(2) << std::right << std::hex << get_val(reply) << endl; 
	}
    }
    bool write_postgresql( unsigned be_id, const CBCI2C_REGS &regs, std::string conninfo )
    {
	PGconn *conn = PQconnectdb(conninfo.c_str());
	/* Check to see that the backend connection was successfully made */
	if (PQstatus(conn) != CONNECTION_OK)
	{
	    fprintf(stderr, "Connection to database failed: %s",
		    PQerrorMessage(conn));
	    exit_nicely(conn);
	}
	char command[256];
	unsigned cbc_id(0), val(0), page(0), addr(0);
	CBCI2C_REGS::const_iterator it_reg = regs.begin();
	for(; it_reg != regs.end(); it_reg++){
	    const CBCI2C_REPLY &reply = it_reg->second;
	    if( get_info(reply) == 1 || get_rw_flag(reply) == 0 ) continue;
	    cbc_id = get_cbc_id(reply);
	    val    = get_val(reply);
	    page   = get_page(reply);
	    addr   = get_addr(reply);
	    sprintf(command, "update only I2CREGS_BE%dCBC%d set value = %d where page = %d and address = %d;", be_id, cbc_id, val, page, addr);

	    //cout << "writing database " << conninfo << " " << command << endl;
	    PGresult *res = PQexec( conn, command );

	    if (PQresultStatus(res) != PGRES_COMMAND_OK)
	    {
		fprintf(stderr, "update failed: %s", PQerrorMessage(conn));
		PQclear(res);
		exit_nicely(conn);
	    }
	    PQclear(res);
	}

	/* close the connection to the database and cleanup */
	PQfinish(conn);

	return 0;
    }
    bool check_rw_replies(CBCI2C_REPLY w_reply, CBCI2C_REPLY r_reply, ostream &os ){
//	cout << "check_rw_replies()" << endl;
	bool match(false);
	if( get_val(w_reply) == get_val(r_reply) ||
		( get_page(w_reply) == 1 && get_addr(w_reply) == 0x1d && ( get_val(w_reply) & 0xC0 ) == ( get_val(r_reply) & 0xC0 ) ) 
	  ) match = true;
	/*
	else if( get_addr(w_reply) == 0 ){
	    if( ( get_val(w_reply) & 0x7F ) == ( get_val(r_reply) & 0x7F ) ) match = true;
	}
	*/
	//if( get_page(w_reply) == 1 && get_addr(w_reply) == 2 ) match = false; 
	if( !match ){
	    os  << "INCONSISTENT WRITE READ" 
		<< " | fe_id : "     <<  get_bus_id(w_reply) 
		<< " | cbc_id : " <<  get_cbc_id(w_reply) 
		<< " | page : "   <<    get_page(w_reply)
		<< " | addr : "   << "0x" << setfill('0') << setw(2) << std::hex <<  get_addr(w_reply)
		<< " | w value : "  << "0x" << setfill('0') << setw(2) << std::hex <<  get_val(w_reply) 
		<< " | r value : "  << "0x" << setfill('0') << setw(2) << std::hex <<  get_val(r_reply) 
		<< std::endl;
	}
	return match;
    }
    std::map<CHANNEL_ID, unsigned> get_chmasks( const CBCI2C_REGS &regs, FE_ID fe_id, CBC_ID cbc_id)
    {
	std::map<CHANNEL_ID, unsigned> masks; 
	for(int i=0; i < 32; i++){
	    CBCI2C_REGID regid = get_regid( fe_id, cbc_id, 1, 0x20+i); 
	    CBCI2C_REGS::const_iterator it = regs.find(regid);
	    if( it != regs.end() ){
		for(int j=1; j < 9; j++){
		    masks[i*8+j] = (it->second >> (j-1)) & 1; 
		    if(i*8+j == 254)break;
		}
	    }
	}
	return masks;
    }
    bool get_comlist_for_chmask( CHANNEL_ID channel_id, unsigned value, std::vector<struct cbc_i2c_command> &comlist, FE_ID fe_id, CBC_ID cbc_id)
    {
	unsigned mask = ((channel_id - 1)%8) << 1;

	for(unsigned i=0; i < comlist.size(); i++){ 
	    struct cbc_i2c_command &com = comlist.at(i);
	    if( com.addr == (0x20 + (channel_id-1) / 8) && com.page == 1 ){
		if( value == 1 ) com.value |= mask;
		else com.value &= ~mask;
		return true;
	    }
	}
	return false;
    }

    std::map<unsigned,unsigned> get_cbcfeids(uhal::HwInterface *hw){

	std::map<unsigned,unsigned> cbcfeids;
	cbcfeids[0] = 0;
	string node = "cs_stat.global.ncbc";
	unsigned ncbcs = read_ipbus(hw,node);
	char cnode[256];
	for( unsigned cbc_id=1; cbc_id <= ncbcs; cbc_id++ ){
	    sprintf( cnode, "cs_cnfg.cbc%02d.fe_id", cbc_id );
	    unsigned fe_id = read_ipbus(hw,cnode);
	    cbcfeids[cbc_id] = fe_id;
	}
	return cbcfeids;
    }

    bool init_cbc_i2c_bus_manager( uhal::HwInterface *hw, CBCI2C_REGS *pregs, bool debug )
    {
	bool good(true);
	if(pregs==0) pregs = new CBCI2C_REGS();
	CBCI2C_REGS &regs = *pregs;

	string node = "cs_stat.global.nfe";
	unsigned nfe = read_ipbus(hw,node);
	node = "cs_stat.global.ncbc";
	unsigned ncbc = read_ipbus(hw,node);
	if(debug){
	    cerr << "# of FE = " << nfe << ", # of CBC = " << ncbc << endl;
	}

	for( unsigned fe_id=1; fe_id <= nfe; fe_id++ ){
	    char cnode[256];
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%1d.n_active_cbcs", fe_id);
	    unsigned nactive_cbcs = read_ipbus( hw, cnode );
	    if( nactive_cbcs == 0 ) continue;

	    sprintf(cnode, "cs_ctrl.cbc_i2c_bm.bus%1d.reset", fe_id);
	    write_ipbus( hw, cnode, 1 );
	    sprintf(cnode, "cs_ctrl.cbc_i2c_bm.bus%1d.reset_fifos", fe_id);
	    write_ipbus( hw, cnode, 1 );

	    sprintf(cnode, "cs_ctrl.cbc_i2c_bm.bus%1d.init", fe_id);
	    write_ipbus( hw, cnode, 1 );


	    std::vector<CBCI2C_REPLY> replies = read_cbc_i2c_replies( hw, fe_id, nactive_cbcs ); 

	    if(debug){
		cerr << "FE ID = " << fe_id << " # OF ACTIVE CBCs = " << nactive_cbcs << endl;
		cerr << "FE ID = " << fe_id << " # of replies = " << replies.size() << endl;
	    }
	    cerr << "----- cbc i2c bus initialization result -----" << endl;
	    for(unsigned i=0; i < nactive_cbcs; i++){
		CBCI2C_REPLY reply = replies.at(i);
		dump_reply( reply, cerr );
		if( get_info(reply) == 1 ){
		    good = false; 
		    dump_replies(replies, cerr);
		    throw CbcI2cRegIpbusException("init_cbc_i2c_bus_manager() reply error.");
		}
		regs[get_regid(reply)] = reply; 
	    }
	}
	if(pregs==0) delete pregs;
	return good;
    }
    bool is_cbci2c_ready( uhal::HwInterface *hw, unsigned fe_id, bool debug ){

	string node = "cs_stat.global.nfe";
	unsigned nfe = read_ipbus(hw,node);

	bool ready(true);
	char cnode[256];
	for( unsigned i=1; i <= nfe; i++ ){
	    if( fe_id != 0 && i != fe_id ) continue;
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.n_active_cbcs", i);
	    unsigned n_active_cbcs = read_ipbus(hw, cnode); 
	    //cout << "FE ID, N active CBCs = " << i << " " << n_active_cbcs << endl;
	    if(n_active_cbcs == 0) continue;
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.com_fifo_empty", i);
	    uint32_t com_fifo_empty = read_ipbus(hw, cnode); 
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.reply_fifo_empty", i);
	    uint32_t reply_fifo_empty = read_ipbus(hw, cnode); 
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.waiting", i);
	    uint32_t bus_waiting = read_ipbus(hw, cnode); 
//	    cout << "feid = " << i << " cbci2c command fifo empty, reply fifo empty, bus waiting flags are " << com_fifo_empty << " " << reply_fifo_empty << " " << bus_waiting << endl;
	    if( !( com_fifo_empty && reply_fifo_empty && bus_waiting ) ){
		if(debug) {
		    cerr << "feid = " << i << " cbci2c command fifo empty, reply fifo empty, bus waiting flags are " << com_fifo_empty << " " << reply_fifo_empty << " " << bus_waiting << endl;
		    if(!reply_fifo_empty){ 
			sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%1d.reply_fifo_ndata", i);
			uint32_t ndata = read_ipbus( hw, cnode );
			cerr << "reply fifo has " << ndata << " replies." << endl;
			sprintf(cnode, "cbc_i2c_regs.reply_fifos.bus%1d", i);
			std::vector<uint32_t> replies = readBlock_ipbus( hw, cnode, ndata );
			dump_replies(replies, cerr);
			sprintf(cnode, "cs_ctrl.cbc_i2c_bm.bus%1d.reset_fifos", i);
			write_ipbus( hw, cnode, 1 );

		    }
		}
	       	ready = false;
	    }
	}
	//cout << "ready " << ready << endl;
	return ready;
    }
    bool wait_for_cbci2c_ready( uhal::HwInterface *hw, unsigned fe_id, int timeout_usec, bool debug ){

	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	bool cbci2c_ready = is_cbci2c_ready(hw,fe_id, debug);

	while(cbci2c_ready==false){

	    usleep(100);

	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( timeout_usec > 0 && dt.tv_sec * 1000000. + dt.tv_usec > timeout_usec ){
		cerr << "wait_for_cbci2c_ready() timeout." << endl;   
		break;
	    }
	    cbci2c_ready = is_cbci2c_ready(hw,fe_id, debug);
	}
	if(!cbci2c_ready){
		cerr << "--- CbcI2cRegIpbusInterface ---" << endl;
		is_cbci2c_ready(hw,fe_id,1);
		throw CbcI2cRegIpbusException("[CbcI2cRegIpbusException] cbci2c is not ready.");
	}
	return cbci2c_ready;
    }
    bool rw_cbc_i2c_regs( uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug )
    {
	//cout << "rw_cbc_i2c_regs() cbc i2c rw from file" << endl;
	bool rb_good(true);
	std::vector<CBCI2C_REPLY> cbc_i2c_replies = rw_cbc_i2c_regs(hw,r,w,filename,cbc_id,cbcfeids, debug);

	unsigned w_reply(0);
	for(unsigned j=0; j<cbc_i2c_replies.size(); j++) {
	    CBCI2C_REPLY reply = cbc_i2c_replies.at(j);
	    if(debug) dump_reply(reply, cerr);
	    if(get_info(reply)){
		dump_replies(cbc_i2c_replies, cerr);
		throw CbcI2cRegIpbusException("rw_cbc_i2c_regs() i2c error.");
	    }
	    if( r && w ){
		if( get_rw_flag(reply) == 0 ) w_reply = reply; 
		else{
		    bool good = check_rw_replies(w_reply, reply, cerr);
		    if( !good ){
//			dump_reply(w_reply, cerr);
//			dump_reply(reply, cerr);
			rb_good = false;
		    }
		}
	    }
	    regs[get_regid(reply)] = reply; 
	}
	return rb_good;
    }
    bool rw_cbc_i2c_Offsets( uhal::HwInterface *hw, unsigned r, unsigned w, std::vector<unsigned> &offsets, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug )
    {
	//	    cout << "rw_cbc_i2c_Offsets() cbc id = " << cbc_id << endl;

	if ( w == 0 && offsets.size() == 0 ) offsets.resize(254, 0xFF); 
	else if( offsets.size() != 254 ){
	    cerr << "The size of offset vector is either 0(read) or 254." << endl;
	    return false;
	}
	vector<struct cbc_i2c_command> commands;
	for( unsigned i=1; i <= 254; i++ ){
	    struct cbc_i2c_command com = { cbc_id, w, r, 2, i, offsets.at(i-1) }; 
	    commands.push_back(com);
	}
	bool good = rw_cbc_i2c_regs( hw, commands, regs, cbcfeids, debug );
	if( good ){
	    if( r == 1 ){
		unsigned fe_id(0);
		if(cbcfeids){
		    std::map<unsigned,unsigned>::const_iterator it = cbcfeids->find(cbc_id);
		    if( it != cbcfeids->end() ) fe_id = it->second;
		    else{
			ostringstream oss;
			oss << "rw_cbc_i2c_Offsets() The CBC " << cbc_id << " is not found in the cbcfeids.";
			throw CbcI2cRegIpbusException(oss.str());
		    }
		}
		else fe_id = get_cbcfeids(hw)[cbc_id]; 
		for( unsigned i=1; i <= 254; i++ ){
		    CBCI2C_REGID regid = get_regid( fe_id, cbc_id, 2, i ); 
		    CBCI2C_REGS::iterator it = regs.find(regid);
		    if( it != regs.end() ) offsets.at(i-1) = get_val(it->second);
		}
	    }
	}
	return good;
    }
    bool rw_cbc_i2c_Offsets( uhal::HwInterface *hw, unsigned r, unsigned w, std::map<CHANNEL_ID,unsigned> &chofst_map, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug )
    {
	vector<struct cbc_i2c_command> commands;
	map<CHANNEL_ID,unsigned>::iterator it = chofst_map.begin();
	for( ; it != chofst_map.end(); it++ ){
	    struct cbc_i2c_command com = { cbc_id, w, r, 2, it->first, it->second };
	    commands.push_back( com );
	}
	bool good = rw_cbc_i2c_regs( hw, commands, regs, cbcfeids, debug );
	if( good ){
	    if( r == 1 ){
		unsigned fe_id(0);
		if(cbcfeids){
		    std::map<unsigned,unsigned>::const_iterator it = cbcfeids->find(cbc_id);
		    if( it != cbcfeids->end() ) fe_id = it->second;
		    else{
			ostringstream oss;
			oss << "rw_cbc_i2c_Offsets() The CBC " << cbc_id << " is not found in the cbcfeids.";
			throw CbcI2cRegIpbusException(oss.str());
		    }
		}
		else fe_id = get_cbcfeids(hw)[cbc_id]; 
		for( it = chofst_map.begin(); it != chofst_map.end(); it++ ){
		    CBCI2C_REGID regid = get_regid( fe_id, cbc_id, 2, it->first ); 
		    CBCI2C_REGS::iterator regs_it = regs.find(regid);
		    if( it != regs.end() ) it->second = get_val(regs_it->second);
		}
	    }
	}
	return good;
    }

    bool rw_cbc_i2c_Offset( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned ch, unsigned &offset, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug )
    {
	bool good(true);
	if( ch == 0 ){
	    std::vector<unsigned> offsets;
	    offsets.resize(254, offset); 
	    good = rw_cbc_i2c_Offsets(hw,r,w,offsets,regs,cbc_id,cbcfeids,debug);
	    offset = -1;
	}
	else{
	    struct cbc_i2c_command com = {cbc_id,r,w,2,ch,offset};
	    good = rw_cbc_i2c_reg(hw,com,regs,cbcfeids,debug);
	    if(good && cbc_id!=0 && r==1){
		unsigned fe_id(0);
		if(cbcfeids) fe_id = cbcfeids->find(cbc_id)->second;
		else fe_id = get_cbcfeids(hw)[cbc_id]; 
		CBCI2C_REGID regid = get_regid( fe_id, cbc_id, 2, ch ); 
		CBCI2C_REGS::iterator it = regs.find(regid);
		if( it != regs.end() ) offset = get_val(it->second);
	    }
	}
	return good;
    }
    bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, string name, unsigned &value, CBCI2C_REGS &regs, unsigned cbc_id,
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug, bool CBC3_0 )
    {
	CBCI2C_TYPE_DATA_MAP type_data_map;
	get_cbci2c_type_data(name,value, &type_data_map);
	CBCI2C_TYPE_DATA_MAP::iterator it = type_data_map.begin();
	CBCI2C_TYPE_DATA type_data = *it;
	bool good = rw_cbc_i2c_regs_by_type(hw, r, w, type_data, regs, cbc_id, cbcfeids, debug, CBC3_0);
	CBCI2C_ITEM_DATA_MAP &item_data_map = type_data.second;
	value       = item_data_map.begin()->second;
	return good;
    }
    bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, map<std::string,unsigned> &name_value_map, CBCI2C_REGS &regs, unsigned cbc_id,
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug, bool CBC3_0 )
    {
	bool good(true);
	CBCI2C_TYPE_DATA_MAP map;
	get_cbci2c_type_data_map(name_value_map, &map);

	CBCI2C_TYPE_DATA_MAP::iterator it = map.begin();
	for(; it != map.end(); it++){
	    /*
	    CBCI2C_TYPE type = it->first;
	    CBCI2C_ITEM_DATA_MAP &item_data_map = it->second;
	    */
	    CBCI2C_TYPE_DATA type_data = *it;
	    bool result = rw_cbc_i2c_regs_by_type(hw, r, w, type_data, regs, cbc_id, cbcfeids, debug, CBC3_0);
	    if( result == 0 ) good = false;
	}
	get_cbci2c_name_value_map(map, &name_value_map); 
	return good;
    }

    bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug, bool CBC3_0 )
    {
	bool good(true);
	if( cbc_id == 0 ){
	    cerr << "broadcast is not allowed for this function." << endl;
	    return false;
	}
	CBCI2C_TYPE_DATA_MAP type_data_map;
	get_cbci2c_type_data_map( filename, &type_data_map );
	CBCI2C_TYPE_DATA_MAP::iterator it = type_data_map.begin();
	for(; it != type_data_map.end(); it++ ){
	    std::map<CBC_ID, CBCI2C_TYPE_DATA> cbcid_type_data;
	//    cout << "rw_cbc_i2c_regs_by_name type = " << it->first << " " << it->second.size() << endl; 
	    cbcid_type_data[cbc_id] = *it;
	    bool result = rw_cbc_i2c_regs_by_type(hw, r, w, cbcid_type_data, regs, cbcfeids, debug, CBC3_0);
	    if(!result) good = false;
	}
	return good;
    }
//    bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &type, CBCI2C_ITEM_DATA_MAP &item_data_map, CBCI2C_REGS &regs, unsigned cbc_id, 
    bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, CBCI2C_TYPE_DATA &type_data, CBCI2C_REGS &regs, unsigned cbc_id, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug, bool CBC3_0 )
    {
	//cout << "rw_cbc_i2c_regs_by_type()" << endl;
	if( cbc_id == 0 ) cerr << "CBC ID = 0 is not supported." << endl;
	map<unsigned,unsigned> feids; 
	if(cbcfeids== 0 ) feids = get_cbcfeids(hw); 
	else feids = *cbcfeids;

//	CBCI2C_TYPE_DATA type_data(type, item_data_map);
	string type = type_data.first;
	CBCI2C_ITEM_DATA_MAP &tid_map   = type_data.second;
	if( r==1 && w==0 && tid_map.size() == 0 ) 
	    get_cbci2c_type_data(type, 0, &type_data);

	unsigned page(1);
	if(type == "Offsets") page = 2;

	if( w == 1 ){
	    //read register values for mask

	    vector<unsigned> addrs = cbci2creg_formatter::getReadAddrsToWrite(type_data);

	    CBCI2C_ADDR_DATA_MAP reg_addr_values0;
	    if( addrs.size() ){
		/*
		cout << "type_data.first " << type_data.first << endl;
		cout << "type_data.second.size() " << type_data.second.size() << endl;
		cout << "size of address vector : " << addrs.size() << endl;
		*/

		std::vector<struct cbc_i2c_command> commands;
		for( unsigned i=0; i < addrs.size(); i++ ){
		    struct cbc_i2c_command com = {cbc_id, 0, 1, page, addrs.at(i), 0 };
		    commands.push_back(com);
		}
		if( !rw_cbc_i2c_regs(hw,commands,regs,&feids,debug) ) return false;

		for( unsigned i=0; i < addrs.size(); i++ ){
		    CBCI2C_REGID regid = get_regid( feids[cbc_id], cbc_id, page, addrs.at(i) ); 
		    CBCI2C_REGS::iterator it_regs = regs.find(regid);
		    //if( it_regs != regs.end() ) reg_values0.at(i) = get_val(it_regs->second);
		    if( it_regs != regs.end() ) reg_addr_values0[addrs.at(i)] = get_val(it_regs->second);
		}
	    }
	    CBCI2C_ADDR_DATA_MAP reg_addr_values  = cbci2creg_formatter::getI2cRegValuesToWrite( type_data, reg_addr_values0, CBC3_0 );

	    std::vector<struct cbc_i2c_command> commands;
	    map<unsigned,unsigned>::iterator it = reg_addr_values.begin();
	    for(; it!=reg_addr_values.end(); it++){
		struct cbc_i2c_command com = { cbc_id, w, r, page, it->first, it->second }; 
		//cout << "cbc_id, w, r, addrs.at(i) " << cbc_id << " " << w << " " << r << " " << it->first << endl;
		commands.push_back(com); 
	    }
	    if( !rw_cbc_i2c_regs( hw, commands, regs, &feids, debug )) return false; 
	}
	else{
	    vector<unsigned> addrs = cbci2creg_formatter::getReadAddrs(type_data);

	    vector<struct cbc_i2c_command> commands;
	    for(unsigned i=0; i < addrs.size(); i++){
		struct cbc_i2c_command com = { cbc_id, w, r, page, addrs.at(i), 0 }; 
		commands.push_back(com); 
//		cout << "cbc_id, w, r, addrs.at(i) " << cbc_id << " " << w << " " << r << " " << addrs.at(i) << endl;
	    }
	    if( !rw_cbc_i2c_regs( hw, commands, regs, &feids, debug ) ) return false; 
	}

	if( r == 1 ){

	    vector<unsigned> addrs = cbci2creg_formatter::getReadAddrs(type_data);

	    CBCI2C_ADDR_DATA_MAP reg_values;
	    for( unsigned i=0; i < addrs.size(); i++ ){
		CBCI2C_REGID regid = get_regid( feids[cbc_id], cbc_id, page, addrs.at(i) ); 
		CBCI2C_REGS::iterator it_regs = regs.find(regid);
		if( it_regs != regs.end() ){
		    reg_values[addrs.at(i)] = get_val(it_regs->second);
		}
	    }
	    cbci2creg_formatter::getValues(type_data, reg_values, CBC3_0 );
	    //const string &type              = type_data.first; 
	    //CBCI2C_ITEM_DATA_MAP &tid_map   = type_data.second; 
	}
	//item_data_map = type_data.second;
	return true;
    }
    bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, std::map<CBC_ID, CBCI2C_TYPE_DATA> &cbc_id_type_data, CBCI2C_REGS &regs, 
	    const std::map<unsigned,unsigned> *cbcfeids, bool debug, bool CBC3_0 )
    {
	map<unsigned,unsigned> feids; 
	if(cbcfeids== 0 ) feids = get_cbcfeids(hw); 
	else feids = *cbcfeids;

	std::map<unsigned, CBCI2C_TYPE_DATA> read_cbc_id_type_data;

	map<unsigned, CBCI2C_TYPE_DATA>::iterator it = cbc_id_type_data.begin();
	for(; it != cbc_id_type_data.end(); it++){ 

	    unsigned cbc_id = it->first;
	    //
	    CBCI2C_TYPE_DATA &type_data = it->second;
	    string                type  = type_data.first; 
	    CBCI2C_ITEM_DATA_MAP &tid_map   = type_data.second;
	    if( r==1 && w==0 && tid_map.size() == 0 ) 
		get_cbci2c_type_data(type, 0, &type_data);

	    unsigned page(1);
	    if(type == "Offsets") page = 2;

	    if( w == 1 ){

		vector<unsigned> addrs = cbci2creg_formatter::getReadAddrsToWrite(type_data);

		std::vector<struct cbc_i2c_command> commands;
		for( unsigned i=0; i < addrs.size(); i++ ){
		    struct cbc_i2c_command com = {cbc_id, 0, 1, page, addrs.at(i), 0 };
		    commands.push_back(com);
		}
		if( !rw_cbc_i2c_regs(hw,commands,regs,&feids,debug) ) return false;

		if(cbc_id == 0){
		    string node = "cs_stat.global.ncbc";
		    unsigned ncbcs = read_ipbus(hw,node);
		    for( unsigned cbcid=1; cbcid <= ncbcs; cbcid++ ){

			char cnode[256];
			sprintf( cnode, "cs_cnfg.cbc%02d.active", cbcid);
			uint32_t value = read_ipbus( hw, cnode );
			if( value == 0 ) continue; 

			map<unsigned,unsigned> reg_addr_values0;
			for( unsigned i=0; i < addrs.size(); i++ ){
			    CBCI2C_REGID regid = get_regid( feids[cbcid], cbcid, page, addrs.at(i) ); 
			    CBCI2C_REGS::iterator it_regs = regs.find(regid);
			    if( it_regs != regs.end() ) reg_addr_values0[addrs.at(i)] = get_val(it_regs->second);
			}
			map<unsigned,unsigned> reg_addr_values  = cbci2creg_formatter::getI2cRegValuesToWrite( type_data, reg_addr_values0, CBC3_0 );
			//cout << "size of reg_addr_values = " << reg_addr_values.size() << endl;
			vector<struct cbc_i2c_command> commands;
			map<unsigned,unsigned>::iterator it = reg_addr_values.begin();
			for(; it!=reg_addr_values.end(); it++){
			    struct cbc_i2c_command com = { cbc_id, w, r, page, it->first, it->second }; 
			    //cout << "cbc_id, w, r, addrs.at(i) " << cbc_id << " " << w << " " << r << " " << it->first << endl;
			    commands.push_back(com); 
			}
			if( !rw_cbc_i2c_regs( hw, commands, regs, &feids, debug )) return false; 
		    }
		}//cbc_id == 0
		else{
		    map<unsigned,unsigned> reg_addr_values0;
		    for( unsigned i=0; i < addrs.size(); i++ ){
			CBCI2C_REGID regid = get_regid( feids[cbc_id], cbc_id, page, addrs.at(i) ); 
			CBCI2C_REGS::iterator it_regs = regs.find(regid);
			if( it_regs != regs.end() ) reg_addr_values0[addrs.at(i)] = get_val(it_regs->second);
		    }
		    CBCI2C_ADDR_DATA_MAP reg_addr_values  = cbci2creg_formatter::getI2cRegValuesToWrite( type_data, reg_addr_values0, CBC3_0 );

		    vector<struct cbc_i2c_command> commands;
		    map<unsigned,unsigned>::iterator it = reg_addr_values.begin();
		    for(; it!=reg_addr_values.end(); it++){
			struct cbc_i2c_command com = { cbc_id, w, r, page, it->first, it->second }; 
			commands.push_back(com); 
		    }
		    if( !rw_cbc_i2c_regs( hw, commands, regs, &feids, debug )) return false; 
		}
	    }//w == 1
	    else{

		vector<unsigned> addrs = cbci2creg_formatter::getReadAddrs(type_data);

		vector<struct cbc_i2c_command> commands;
		for(unsigned i=0; i < addrs.size(); i++){
		    struct cbc_i2c_command com = { cbc_id, w, r, page, addrs.at(i), 0 }; 
		    commands.push_back(com); 
		}
		if( !rw_cbc_i2c_regs( hw, commands, regs, &feids, debug ) ) return false; 
	    }

	    if( r == 1 ){

		if ( tid_map.size() == 0 ){
		    const CBCI2C_NAMES &names = cbci2creg_formatter::getNames();
		    if( names.find(type) != names.end() ){
			set<CBCI2C_ITEM> items = names.find(type)->second;
			set<CBCI2C_ITEM>::iterator it = items.begin();
			for(; it != items.end(); it++){
			    tid_map[*it] = 0; 
			}
		    }
		} 
		vector<unsigned> addrs = cbci2creg_formatter::getReadAddrs(type_data);

		if(cbc_id == 0){

		    string node = "cs_stat.global.ncbc";
		    unsigned ncbcs = read_ipbus(hw,node);
		    for( unsigned cbcid=1; cbcid <= ncbcs; cbcid++ ){

			char cnode[256];
			sprintf( cnode, "cs_cnfg.cbc%02d.active", cbcid);
			uint32_t value = read_ipbus( hw, cnode );
			if( value == 0 ) continue; 

			map<unsigned,unsigned> reg_addr_values;
			for( unsigned i=0; i < addrs.size(); i++ ){
			    CBCI2C_REGID regid = get_regid( feids[cbcid], cbcid, page, addrs.at(i) ); 
			    CBCI2C_REGS::iterator it_regs = regs.find(regid);
			    if( it_regs != regs.end() ) {
				reg_addr_values[addrs.at(i)] = get_val(it_regs->second);
			    }
			}
			read_cbc_id_type_data[cbcid] = type_data;
			cbci2creg_formatter::getValues(read_cbc_id_type_data[cbcid], reg_addr_values, CBC3_0 );
		    }
		}
		else{
		    CBCI2C_ADDR_DATA_MAP reg_values;
		    for( unsigned i=0; i < addrs.size(); i++ ){
			CBCI2C_REGID regid = get_regid( feids[cbc_id], cbc_id, page, addrs.at(i) ); 
			CBCI2C_REGS::iterator it_regs = regs.find(regid);
			if( it_regs != regs.end() ) reg_values[addrs.at(i)] = get_val(it_regs->second);
		    }
		    read_cbc_id_type_data[cbc_id] = type_data;
		    cbci2creg_formatter::getValues(read_cbc_id_type_data[cbc_id], reg_values, CBC3_0 );
		}
	    }
	}
	cbc_id_type_data = read_cbc_id_type_data;
	return true;
    }
    bool rw_cbc_i2c_reg( uhal::HwInterface *hw, struct cbc_i2c_command &com, CBCI2C_REGS &regs, const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug )
    {
	bool rb_good(true);
	std::vector<CBCI2C_REPLY> replies = rw_cbc_i2c_reg(hw, com.r, com.w, com.page, com.addr, com.value, com.cbc_id, cbcfeids ); 
	unsigned w_reply(0);
	for(unsigned j=0; j<replies.size(); j++) {
	    CBCI2C_REPLY reply = replies.at(j);
	    if(debug) dump_reply(reply, cerr);
	    if(get_info(reply)){
		dump_replies(replies, cerr);
		throw CbcI2cRegIpbusException("rw_cbc_i2c_regs() i2c error.");
	    }
	    regs[get_regid(reply)] = reply; 
	    if( com.r && com.w ){
		if( get_rw_flag(reply) == 0 ) w_reply = reply; 
		else{
		    bool good = check_rw_replies(w_reply, reply, cerr);
		    if( !good ){
//			dump_reply(w_reply, cerr);
//			dump_reply(reply, cerr);
			rb_good = false;
		    }
		}
	    }
	    if(get_rw_flag(reply) == 1) com.value = get_val(reply);
	}
	return rb_good;
    }
    bool rw_cbc_i2c_regs( uhal::HwInterface *hw, const std::vector<struct cbc_i2c_command> &commands, CBCI2C_REGS &regs, const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug )
    {
	bool rb_good(true);

	std::map<unsigned,unsigned> feid_map;
	if(cbcfeids) feid_map = *cbcfeids; 
	else feid_map =  get_cbcfeids(hw); 

	string node = "cs_stat.global.nfe";
	unsigned nfe = read_ipbus(hw,node);

	std::vector<uint32_t> ipbus_commands;
	std::map<int, int> nreplies;
	std::map<int, int> n_active_cbcs;
	char cnode[256];
	for(unsigned i=1; i<=nfe;i++){
	    nreplies[i]=0;
	    sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.n_active_cbcs", i);
	    n_active_cbcs[i] = read_ipbus(hw, cnode); 
	}
	for(unsigned com_i=0; com_i < commands.size(); com_i++){
	    const struct cbc_i2c_command &com = commands.at(com_i);
	    int fe_id(0);
	    if(com.cbc_id != 0){
		fe_id =feid_map[com.cbc_id];
		if(fe_id == 0 ) { 
		    cerr << "The CBC with ID = " << com.cbc_id << " is not configured." << endl;
		}
	    }
	    ipbus_commands.push_back( get_ipbus_command_i2cregrw( com.w, com.r, com.page, com.addr, com.value, fe_id, com.cbc_id ) );
	    int n(1);
	    if( com.cbc_id == 0 ){
		for(unsigned i=1; i<=nfe;i++){
		    n = n_active_cbcs[i];
		    if( com.r ) nreplies[i] += n;
		    if( com.w ) nreplies[i] += n;
		}
	    }
	    else{
		if( com.r ) nreplies[fe_id] += 1;
		if( com.w ) nreplies[fe_id] += 1;
	    }
	}
	for(unsigned fe_id=1; fe_id<=nfe;fe_id++){
//	    cout << "rw_cbc_i2c_regs() fe id = " << std::dec << fe_id << "; # of replies = " << std::dec << nreplies[fe_id] << endl;
	    if( nreplies[fe_id] != 0 && wait_for_cbci2c_ready(hw, fe_id, 1, debug) == false ){
	       	return false; 
	    }
	}
	writeBlock_ipbus( hw, "cbc_i2c_regs.command_fifos", ipbus_commands );

	for(unsigned i=1; i<=nfe;i++){
	    if( nreplies[i] == 0 ) continue;
	    //cout << "FE ID = " << i << endl;
	    std::vector<CBCI2C_REPLY> cbc_i2c_replies = read_cbc_i2c_replies( hw, i, nreplies[i] );
	    unsigned w_reply(0);
	    for(unsigned j=0; j<cbc_i2c_replies.size(); j++) {
		CBCI2C_REPLY reply = cbc_i2c_replies.at(j);
		if(debug) dump_reply(reply, cerr);
		if(get_info(reply)){
		    dump_replies(cbc_i2c_replies, cerr);
		    throw CbcI2cRegIpbusException("rw_cbc_i2c_regs() i2c error.");
		}
		if( get_rw_flag(reply) == 0 ) w_reply = reply; 
		else{
		    if(get_regid(w_reply) == get_regid(reply)){
			    bool good = check_rw_replies(w_reply, reply, cerr);
			if( !good ){
//			    dump_reply(w_reply, cerr);
//			    dump_reply(reply, cerr);
			    rb_good = false;
			}
		    }
		}
		regs[get_regid(reply)] = reply; 
	    }
	}
	return rb_good;
    }
    std::vector<CBCI2C_REPLY> rw_cbc_i2c_reg( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value, unsigned cbc_id, 
	    const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug ) 
    {
	unsigned fe_id(0);
	if(cbc_id != 0){
	    if(cbcfeids){
		std::map<unsigned,unsigned>::const_iterator it = cbcfeids->find(cbc_id);
		if( it != cbcfeids->end() ) fe_id = it->second;
		else{
		    cerr << "rw_cbc_i2c_reg() The CBC " << cbc_id << " is not configured." << endl;
		}
	    }
	    else fe_id = get_cbcfeids(hw)[cbc_id]; 
	}

	if( wait_for_cbci2c_ready(hw, fe_id, debug) == false ) {
	    return std::vector<CBCI2C_REPLY>();
	}

	uint32_t ipbus_command = get_ipbus_command_i2cregrw( w, r, page, addr, value, fe_id, cbc_id ); 
	write_ipbus( hw, "cbc_i2c_regs.command_fifos", ipbus_command );

	if( fe_id == 0 ){
	    std::vector<CBCI2C_REPLY> replies;
	    string node = "cs_stat.global.nfe";
	    unsigned nfe = read_ipbus(hw,node);
	    char cnode[256];
	    for( unsigned i=1; i <= nfe; i++){ 
		sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%d.n_active_cbcs", i);
		uint32_t ncbcs = read_ipbus(hw, cnode); 
		unsigned nreplies(0);
		if( w == 1 ) nreplies += ncbcs; 
		if( r == 1 ) nreplies += ncbcs; 
		std::vector<CBCI2C_REPLY> r = read_cbc_i2c_replies(hw, i, nreplies);
		replies.insert( replies.end(), r.begin(), r.end() );
	    }
	    return replies;
	}
	else{
	    unsigned nreplies(0);
	    if( w == 1 ) nreplies += 1; 
	    if( r == 1 ) nreplies += 1; 
	    return read_cbc_i2c_replies(hw, fe_id, nreplies);
	}
    }
    std::vector<CBCI2C_REPLY> rw_cbc_i2c_regs( uhal::HwInterface *hw, const std::vector<uint32_t> &ipbus_commands, int nreplies, unsigned fe_id, bool debug ) 
    {
	if( fe_id == 0 ) cerr << "rw_cbc_i2c_regs() fe_id == 0 is not supported." << endl;

	if( wait_for_cbci2c_ready(hw, fe_id, 1000, debug) == false ) return std::vector<CBCI2C_REPLY>();
	writeBlock_ipbus( hw, "cbc_i2c_regs.command_fifos", ipbus_commands );
	return read_cbc_i2c_replies(hw, fe_id, nreplies);
    }
    std::vector<CBCI2C_REPLY> rw_cbc_i2c_regs( uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, unsigned cbc_id, const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug ) 
    {
	if( cbc_id == 0 ) cerr << "rw_cbc_i2c_regs() cbc_id == 0 is not supported." << endl;

	unsigned fe_id(0);
	if(cbcfeids){
	    std::map<unsigned,unsigned>::const_iterator it = cbcfeids->find(cbc_id);
	    if( it != cbcfeids->end() ) fe_id = it->second;
	    else{
		cerr << "The CBC " << cbc_id << " is not configured." << endl;
	    }
	}
	else fe_id = get_cbcfeids(hw)[cbc_id]; 

	std::vector<uint32_t> ipbus_commands;
	unsigned nreplies(0);
	get_ipbus_commands_i2cregrw(w,r,filename,ipbus_commands,nreplies,fe_id,cbc_id);
	if( ipbus_commands.size() ){
	    //cout << "# of ipbus_commands = " << ipbus_commands.size() << endl;
	    return rw_cbc_i2c_regs( hw, ipbus_commands, nreplies, fe_id, debug );
	}
	else{
	    return std::vector<CBCI2C_REPLY>();
	}
    }
    void send_cbc_i2c_command( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value, unsigned cbc_id,
	    const std::map<CBC_ID,FE_ID> *cbcfeids )
    {
	unsigned fe_id(0);
	if(cbc_id != 0){
	    if(cbcfeids){
		std::map<unsigned,unsigned>::const_iterator it = cbcfeids->find(cbc_id);
		if( it != cbcfeids->end() ) fe_id = it->second;
		else{
		    cerr << "The CBC " << cbc_id << " is not configured." << endl;
		}
	    }
	    else fe_id = get_cbcfeids(hw)[cbc_id]; 
	}

	uint32_t ipbus_command = get_ipbus_command_i2cregrw( w, r, page, addr, value, fe_id, cbc_id ); 
	write_ipbus( hw, "cbc_i2c_regs.command_fifos", ipbus_command );
    }
    std::vector<CBCI2C_REPLY> read_cbc_i2c_replies(uhal::HwInterface *hw, unsigned fe_id, int nreplies ){

	char cnode[256];
	std::vector<CBCI2C_REPLY> replies;

	sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%1d.reply_fifo_ndata", fe_id);
	uint32_t ndata = read_ipbus( hw, cnode );
//	cout << "ndata = " << ndata << endl;
	reply_fifo_ndata_sum += ndata;

	int timeout_sec(1);
	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	bool re(false);
	if( nreplies != -1 ){
	    while( (int)ndata < nreplies ){
		usleep( 80 * nreplies );
		ndata = read_ipbus( hw, cnode );
		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);
		if( timeout_sec > 0 && dt.tv_sec > timeout_sec )
		    break;
	    }
	    if( (int) ndata != nreplies ){
		cerr << "Error: read_cbc_i2c_replies() " << ndata << " replies are returned where " << nreplies << " expected." << endl;
		re = true;
	    }
	}
	if( ndata ){
	    sprintf(cnode, "cbc_i2c_regs.reply_fifos.bus%1d", fe_id);
	    std::vector<uint32_t> tmp = readBlock_ipbus( hw, cnode, ndata );
	    replies.insert( replies.end(), tmp.begin(), tmp.end() );
	    sprintf(cnode, "cs_ctrl.cbc_i2c_bm.bus%1d.reset_fifos", fe_id);
	    write_ipbus( hw, cnode, 1 );
	}
	if(re) {
	    cerr << " read_cbc_i2c_replies() dump begin -----------" << endl;
	    dump_replies( replies, cerr );
	    cerr << " read_cbc_i2c_replies() dump end   -----------" << endl;
	}
	return replies;
    }
    void check_cbci2c_reply_fifo( uhal::HwInterface *hw, std::ostream &os, unsigned bus_id, int debug ){

	char tmp[256];
	sprintf(tmp, "cs_stat.cbc_i2c_bm.bus%d.reply_fifo_ndata", bus_id);
	unsigned n_replies = read_ipbus(hw, tmp );
//	cout << "N_replies = " << n_replies << endl;
	std::vector<CBCI2C_REPLY> replies = read_cbc_i2c_replies( hw, bus_id, n_replies );
	if(debug){
	    dump_replies(replies, os);
	}

	bool good = good_replies(replies);
	if(!good){
	    dump_replies(replies, os);
	    throw CbcI2cRegIpbusException( "CBC I2C replies from sequencer are not good." );
	}
    }
}
