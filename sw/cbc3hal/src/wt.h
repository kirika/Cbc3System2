#ifndef __WT_H__
#define __WT_H__

#include <string>
#include <set>
#include <iostream>
#include <cstring>
#include <map>
#include <set>
#include <vector>
#include <sys/time.h>
#include "analysis_utils.h"

extern "C" {
#include "ut.h"
}

//error numbers
#define WT_ERR_OTHER                     0x00000001 
#define WT_ERR_IPBUS                     0x00000002 
#define WT_ERR_KMM                       0x00000004
#define WT_ERR_I2C                       0x00000008
#define WT_ERR_BE                        0x00000010
#define WT_ERR_SETTINGS                  0x00000020
#define WT_ERR_CBC                       0x00000040
#define WT_ERR_TEST                      0x00000080
#define WT_ERR_SEQ                       0x00000100
#define WT_ERR_DAQ                       0x00000200
#define WT_ERR_RAW_DATA                  0x00000400 
#define WT_ERR_BE_CONFIG                 0x00000800
#define WT_ERR_I2C_STUCK_BITS            0x00001000
#define WT_ERR_CURR                      0x00002000
#define WT_ERR_FUSE_PROC                 0x00004000
#define WT_ERR_AFTER_FUSE                0x00008000
#define WT_ERR_OFFSET_TUNING             0x00010000
#define WT_ERR_VCTH_SCAN                 0x00020000
#define WT_ERR_PIPELINE                  0x00040000
#define WT_ERR_BUFRAM                    0x00080000
#define WT_ERR_CHANNEL_MASK              0x00100000
#define WT_ERR_STUB                      0x00200000
#define WT_ERR_HDHIP                     0x00400000
#define WT_ERR_FCI                       0x00800000
#define WT_ERR_DLL                       0x01000000
#define WT_ERR_BIAS                      0x02000000
#define WT_ERR_CURR_SCAN                 0x04000000
#define WT_ERR_AMUX_BIAS                 0x08000000
#define WT_ERR_VDDA                      0x10000000
#define WT_ERR_GAIN                      0x20000000
#define WT_ERR_ORBIT_RESET               0x40000000

//relayA values
#define RELAYA_VDDD_I  1
#define RELAYA_VLDOI_I 2 
#define RELAYA_AMUX    4
#define RELAYA_VDDA    8
#define RELAYB_ON      1
#define RELAYB_OFF     0 

class TH2F;

namespace cbc3hal{
    class Cbc3BeFc7;
}
namespace uhal{
    class HwInterface;
}
namespace wt{

    //set of wrong stub addresses for CBC3
    extern std::set<int> waddrs;

    //for log
    extern const std::string RED;
    extern const std::string YELLOW;
    extern const std::string GREEN;
    extern const std::string BLUE;
    extern const std::string NC;
    std::string time_string();

    class WaferTestException : public std::exception{
	public:
	    WaferTestException()throw():_ecode(-1) {}
	    WaferTestException( const std::string &estr, int code )throw(){err_str = estr; _ecode = code;}
	    WaferTestException(const WaferTestException &e)throw(){ err_str = e.what(); _ecode = e.ecode(); }
	    WaferTestException& operator= (const WaferTestException &e) throw(){ err_str = e.what(); _ecode = e.ecode(); return *this; }
	    virtual ~WaferTestException()throw(){}
	    virtual const char * what() const throw(){ return err_str.c_str(); }
	    virtual int ecode()const{ return _ecode;}
	private:
	    std::string err_str;
	    int _ecode;
    };

    void print_delta_t( const std::string &test_name, const struct timeval &dt, std::ostream &os);
    void print_test( const std::string &test_name, const std::string &log,  std::ostream &os, const std::string &color=NC );

    class KeithleyMultiMeter{
	public:
	    static int km_ext_trig_delay;
	public:
	    KeithleyMultiMeter(const char *sern, bool debug = false);
	    ~KeithleyMultiMeter();
	    usbtmc_device_data *ut_handle(){ return _ut_handle;}
	    struct timeval print_sumdt(){ return _print_sumdt;}
	    struct timeval read_sumdt(){ return _read_sumdt;}
	    void kmm_printf(const char *c);
	    int kmm_read(); 
	    int kmm_read_ndata(int ndata); 
	    const unsigned char * get_rbuf()const{ return _rbuf_copy; }
	    void set_DC_measurement(std::string nplc, std::string range, int sample_count, int trig_count, bool ext, int delay=0);
	    std::vector<float> get_kmm_vdata();
	    float get_kmm_DC_err(float v);
	    void set_range(std::string r){ _range = r; }
	    void re_range(float re){ _re_range = re; }
	    std::string get_range()const{ return _range; }
	    float re_range()const{ return _re_range; }
	    float get_DC_err(float v)const;
	private:
	    bool _debug;
	    int _buflen;
	    unsigned char *_rbuf;
	    unsigned char *_rbuf_copy;
	    usbtmc_device_data *_ut_handle;
	    struct timeval _print_sumdt;
	    struct timeval _read_sumdt;
	    float _re_DC;
	    float _re_Reg;
	    std::string _range;
	    float _re_range;
    };

    class WaferTestSettings{

	public:
	    WaferTestSettings(){}
	    WaferTestSettings( const std::string &test_name ): _test_name(test_name){;}
	    virtual ~WaferTestSettings(){}
	    void set( const std::string &item, std::string &value ){ settings[item] = value; }
	    std::map<std::string, std::string> &get_settings(){ return settings; }

	private:
	    std::string _test_name;
	    std::map<std::string, std::string> settings;
    };
    class WaferTestParameters{
	public:
	    WaferTestParameters(){}
	    virtual ~WaferTestParameters(){}
	    void set( std::string item, std::string value );
	    void set( std::string item, float value );
	    void set( std::string item, std::vector<float> value );
	    const std::map<std::string, std::string> & get_str_params()const { return str_params; }
	    const std::map<std::string, float> & get_num_params()const { return num_params; }
	    const std::map<std::string, std::vector<float> > & get_vnum_params()const { return vnum_params; }
	    void dump(std::ostream &os);
	private:
	    std::map<std::string, std::string> str_params;
	    std::map<std::string, float> num_params;
	    std::map<std::string, std::vector<float> > vnum_params;
    };


    class WaferTest{

	public:
	    static const std::vector<std::string> test_list;
		    
	public:
	    WaferTest(const std::string & config_file_name, const std::string &setting_file_dir, std::ostream &os, bool tune=false, bool CBC3_0=true, int debug=0);
	    virtual ~WaferTest();
	    void set_test_setting( const std::string &test_name, const std::string &item, const std::string &value);
	    int run_test( const std::string &test_names );

	    cbc3hal::Cbc3BeFc7 *init_be( std::string &connection_file, std::string &id);
	    usbtmc_device_data *init_kmm(const std::string &s);

	    void measure_currents( const std::string &test_name, float &vddd_cur, float &vldoi_cur );
	    int config_be( std::string &be_config_file, int fci);
	    int cbci2c_write_read_test( const std::string &test_name, std::map<std::string, std::string> &settings ); 
	    int bgmeas_bgscan( const std::string &test_name, const std::string &fname, std::map<unsigned,float> &bg_meas, std::ostream &os, bool inc=true, int pause_n100us = 10 );
	    int vddameas_bgscan( const std::string &test_name, const std::string &fname, std::map<unsigned,float> &bg_meas, std::ostream &os, bool inc=true, int pause_n100us = 10 );
	    int find_best_bgi2c( std::map<unsigned,float> &meas, float target);
	    bool offset_tuning( const std::string &test_name, int sample_count);
	    bool vcth_scan_scurve_test( const std::string &test_name, int sample_count, bool tp_en, 
	    std::map<cbc3hal::CHANNEL_ID, double> &midp, std::map<cbc3hal::CHANNEL_ID, double> &midp_e, double &mean, double &mean_e, double &sigma, double &sigma_e, double noise_lim );
	    int vcth_scan( const std::string &test_name, const std::string &fname, std::map<unsigned,float> &vcth_meas, std::ostream &os, bool set_relayA=true, bool inc=true, int pause_n100us=0);
	    int vplus_scan( const std::string &test_name, const std::string &fname, bool vplus1, unsigned VPLUS0, std::map<unsigned,float> &vplus_meas, std::ostream &os, bool set_relayA=true, bool inc=true, int pause_n100us=0);
	    int cal_vcasc_scan( const std::string &test_name, const std::string &fname, std::map<unsigned,float> &cal_vcasc_meas, std::ostream &os, bool set_relayA=true, bool inc=true, int pause_n100us=0);
	    int curr_scan( const std::string &test_name, std::ostream &os, bool set_relayA, bool inc, int pause_n100us);
	    void read_raw_data( const std::string &test_name, unsigned N, uint32_t *rawdata, int err_code );
	    void read_data( const std::string &test_name, unsigned nwords, uint32_t *buffer, int err_code );
	    int check_pipeline( const std::string &test_name, int hit_type);
	    int check_buffer_err_hit( const std::string &test_name, bool all_hit);
	    int check_buffer_pa( const std::string &test_name);
	    int check_buffer_l1c( const std::string &test_name);
	    /*
	    int generate_stubs( const std::string &test_name, int ibend, std::map<cbc3hal::CHANNEL_ID, unsigned> &msks, int max_cw, int pt_width, int nss, int nsw, int ls, 
		    std::vector<int> &addr0, std::vector<int> &bend0, std::vector<int> &v_max_cw, std::vector<int> &v_nss, std::vector<int> &v_nsw, std::vector<int> &v_ibend );
		    */
	    int generate_stubs( const std::string &test_name, int ibend, std::map<cbc3hal::CHANNEL_ID, unsigned> &msks, int max_cw, int pt_width, int nss, int nsw, int ls, 
		    std::vector<int> &addr0, std::vector<int> &bend0, std::vector<int> &v_max_cw, std::vector<int> &v_nss, std::vector<int> &v_nsw, std::vector<int> &v_ibend, int cwo_i = -1, unsigned *ibnd=0 );
	    int generate_stubs(const std::string &test_name, int nss, int nsw, int ibend, int ls, int max_cw, int pt_width, bool l1_supp, bool stub_supp, std::map<cbc3hal::CHANNEL_ID, unsigned> &msks, 
		    std::vector<std::vector<int> > &addr0, std::vector<std::vector<int> > &bend0, std::vector<std::set<int> > &ch0 ); 
	    int read_data_and_check_stubs(const std::string &test_name, int err_code, int nevt, 
		    std::vector<int> &addr0, std::vector<int> &bend0, std::vector<int> &v_max_cw, std::vector<int> &v_nss, std::vector<int> &v_nsw, std::vector<int> &v_ibend,
		    TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend);
	    int read_data_and_check_stubs_and_hits( const std::string &test_name, int err_code, 
		    bool stop_at_fail, bool stub_addr_check, bool stub_bend_check, bool hit_check, int nevt, 
		    std::vector<std::vector<int> > &addr0, std::vector<std::vector<int> > &bend0, std::vector<std::set<int> > &ch0,
		    TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend );
	    int read_amux( std::vector<std::string> &bias, std::vector<int> &bias_index, int sample_count, const std::string &test_name, int stat_start_s);
	    bool write_amux_values( std::vector<float> &vmeas, std::vector<std::string> &bias, int sample_count, const std::string &test_name, int start_s = 5);

	    int frame03( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame06( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame10( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame11( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame14( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame15( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame16( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame17( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame18( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame19( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame20( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame21( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame22( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame23( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame24( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame25( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame26( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame27( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame28( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame29( const std::string &test_name, std::map<std::string, std::string> &settings);
	    int frame30( const std::string &test_name, std::map<std::string, std::string> &settings);

	    int test( const std::string &test_name, std::map<std::string, std::string> &settings );
	    std::string current_test()const{ return _current_test; }
	    int result()const{ return _result; }

	    uhal::HwInterface *hw(){ return _hw; }
	    std::map<std::string, WaferTestSettings> test_settings(){ return _test_settings; }
	    int cbc_id(){ return _cbc_id; }
	    std::ostream & get_os(){ return _os; }

	    void print_wafer_test();

	private:
	    std::string _setting_file_dir;
	    std::map<std::string, WaferTestSettings> _test_settings;

	    cbc3hal::Cbc3BeFc7 *_cbc3be;
	    uhal::HwInterface *_hw;
	    KeithleyMultiMeter *_kmm;
	    usbtmc_device_data *_ut_handle;
	    int _cbc_id;
	    std::ostream &_os;
	    int _tune;
	    bool _CBC3_0;
	    int _debug;
	    int _result;
	    std::string _cbci2c_page1_main;
	    std::string _cbci2c_page2;
	    std::string _run_config;
	    bool _i2c_bg;
	    std::string _current_test;

	    WaferTestParameters _parameters;

    };
}


#endif

