/*
 * Author : Kirika Uchida
 */

#ifndef __CBC3BEFC7INTERFACE_H__
#define __CBC3BEFC7INTERFACE_H__

#include "Cbc3BeIpbusInterface.h"
#include "Cbc3BeData.h"
#include <iostream>

namespace cbc3hal{

    class Cbc3BeFc7 : public Cbc3BeIpbusInterface{
	public:
	    enum {DEBUG_CBC3BEFC7_BIT = 4};
	public:
	    Cbc3BeFc7(bool CBC3_0 = 0, int debug = 0);
	    virtual ~Cbc3BeFc7(){;}
	    virtual void DumpBeBoardConfig( std::ostream &os, int level = DUMP_LEVEL_NORMAL );
	    virtual void DumpBeBoardStatus( std::ostream &os, int level = DUMP_LEVEL_NORMAL );
	    virtual void PrintVersion( std::ostream &os ); 
	    virtual std::string ReadVersion(); 
	    virtual void ReadBeCbcConfig();
	    virtual std::string ReadId(); 
	    virtual void WriteFscBram( const std::vector<uint32_t> &data );
            virtual void WriteFscBram( const std::string &fname );
            virtual void WriteCiscBram( const std::string &fname );
	    virtual void CbcReset(int fe_id=-1);
	    virtual void SendFastSignal( std::string type );
	    virtual bool HasData( int min_word = 0 );
	    virtual bool WaitData(unsigned timeout_sec = 0, bool usec = false );
	    virtual int ReadDataBufferAll( uint32_t*& Data );
	    virtual int  ReadData( uint32_t*& Data, int nword = 0 );
	    virtual std::vector<uint32_t> ReadData();
	    virtual void InitializeRun();
	    virtual void FinalizeRun();
	    virtual bool ScanDataClockTiming(unsigned fe_id=1); 
	    virtual bool TuneDataClockTiming(bool scan=false); 
	    virtual void StopTrigger();
	    virtual void StartTrigger();
	    virtual void ResetFscBramController();
	    virtual void StartFscBramController(); 
	    virtual bool WaitSequencerReady(int timeout_sec=1);
	    virtual void StartFastSignalGenerator();
	    virtual void StopFastSignalGenerator();

	    virtual void PrintCbcSystemVersion( std::ostream &os ); 

	    static std::string id( uint32_t value ); 
	    static std::string version( uint32_t value );

	    virtual int ReadRawData( uint32_t*& Data, int nword, bool dcheck=false );
	public:
	    static std::vector<std::string>& Cbc3SystemConfigRegs();
	    static std::vector<std::string>& Cbc3SystemStatusRegs();
	    static std::map<unsigned,std::string>& TestOutMapping();
	    static std::map<unsigned,std::string>& DataClockTimingTuningModuleFsmMapping();
	    static std::map<unsigned,std::string>& DataClockTimingTuningModuleIserdesIdelayCtrlFsmMapping();
	protected:
    };
}

#endif


