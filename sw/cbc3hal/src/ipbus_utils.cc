#include "ipbus_utils.h"
#include <uhal/uhal.hpp>
#include <pugixml/pugixml.hpp>
#include <fstream>
#include <iomanip>

using namespace std;
using namespace uhal;
namespace ipbutl {

    unsigned long n_read(0);
    unsigned long n_write(0);
    unsigned long n_readBlock(0);
    unsigned long n_writeBlock(0);


    uint32_t read_ipbus( uhal::HwInterface *hw, const string &node_name ) 
    {
	ValWord<uint32_t> value = hw->getNode( node_name ).read();
	hw->dispatch();
	n_read++;

	return value.value();
    }

    void write_ipbus( uhal::HwInterface *hw, const std::string &node, uint32_t value )  
    {
	try{
	    hw->getNode( node ).write( value );
	    hw->dispatch();
	    n_write++;
	}
	catch(std::exception& e){
	    cerr << e.what() << endl;
	}
	catch(...){
	    cerr << "unknown error in ipbus_util write_ipbus()" << endl;
	}
    }

    std::vector<uint32_t> readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords )
    {
	uhal::ValVector<uint32_t> values = hw->getNode( node_name ).readBlock( nwords );
	hw->dispatch();
	n_readBlock++;
	return values.value();
    }

    void readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords, void *buffer )
    {
	uhal::ValVector<uint32_t> values = hw->getNode( node_name ).readBlock( nwords );
	hw->dispatch();
	n_readBlock++;
	for( unsigned i=0; i < nwords; i++ ){
	    ( (uint32_t*)buffer)[i] = values.at(i);
	}
    }
    void writeBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, const std::vector<uint32_t> &data )
    {
	hw->getNode( node_name ).writeBlock( data );
	hw->dispatch();
	n_writeBlock++;
    }

    void read_ipbus( uhal::HwInterface *hw, std::map<std::string,uint32_t> &regmap ) 
    {
	std::map<std::string,uint32_t>::iterator it = regmap.begin();
	map<string,ValWord<uint32_t> > values;
	for( ; it != regmap.end(); it++ ){
	    values[it->first] = hw->getNode( it->first ).read();
	}
	hw->dispatch();
	n_read++;
	for( it = regmap.begin(); it != regmap.end(); it++ ){
	    it->second = values[it->first];
	}
    }
    void     write_ipbus( uhal::HwInterface *hw, std::map<std::string,uint32_t> &regmap )  
    {
	std::map<std::string,uint32_t>::iterator it = regmap.begin();
	for( ; it != regmap.end(); it++ ){
	    hw->getNode( it->first ).write( it->second );
	}
	hw->dispatch();
	n_write++;
    }

    void     read_ipbus( uhal::HwInterface *hw, const std::vector<std::string> &nodes, std::vector<uint32_t> &values ) 
    {
	vector<ValWord<uint32_t> > valwords;
	for( unsigned i=0; i < nodes.size(); i++ ){
	    valwords.push_back( hw->getNode( nodes.at(i) ).read() );
	}
	hw->dispatch();
	n_read++;
	values.resize(nodes.size());
	for( unsigned i=0; i < nodes.size(); i++ ){
	    values.at(i) = valwords.at(i).value();
	}
    }

    void read_ipbus( uhal::HwInterface *hw, const std::string fname, std::map<string,uint32_t> &regmap )
    {
	//	regmap.clear();
	ifstream ifile( fname.c_str() );
	if( ifile.is_open() ){
	    char line[256];
	    while( !ifile.eof() ){
		ifile.getline(line, 256 );
		istringstream iss( line );
		string node;
		iss >> node;
		if( node == "" || node[0] == '#' ) continue;
		regmap[node] = 0; 
	    }
	    read_ipbus( hw, regmap );
	}
	else{
	    ostringstream oss;
	    oss << "read_ipbus() " << fname << " could not be opened.";
	    throw IpbusUtilsException(oss.str());
	}
    }
    std::map<string,uint32_t> write_ipbus( uhal::HwInterface *hw, const string fname )
    {
	map<string,uint32_t> regmap;
	ifstream ifile( fname.c_str() );
	if( ifile.is_open() ){
	    char line[256];
	    while( !ifile.eof() ){
		ifile.getline(line, 256 );
		istringstream iss( line );
		string node, value_str;
		iss >> node >> value_str;
		if( node == "" || node[0] == '#' ) continue;
		uint32_t value = strtol( value_str.c_str(), 0, 0 );
		regmap[node] = value; 
		//cout << setw(80) << setfill(' ') << node << " : 0x" << std::hex << setw(8) << setfill('0')  << right << value << endl;
	    }
	    write_ipbus( hw, regmap );
	    //cout << std::dec << regmap.size() << " registers has been configured." << endl;
	}
	else{
	    ostringstream oss;
	    oss << "read_ipbus() " << fname << " could not be opened.";
	    throw IpbusUtilsException(oss.str());
	}
	return regmap;
    }
    std::vector<std::string>  get_nodes( uhal::HwInterface *hw, const std::string &expression ) 
    {
	vector<string> nodelist = hw->getNodes( expression );
	for(unsigned i=0; i < nodelist.size(); i++){
	    if( hw->getNode( nodelist.at(i) ).getTags().find( "noleaf" ) != string::npos ) nodelist.erase(nodelist.begin() + i--);
	}
	return nodelist; 
    }

    void get_nodes( uhal::HwInterface *hw, const std::string &expression, const std::string &delimiter ) 
    {
	vector<string> nodelist = get_nodes(hw, expression); 
	for( unsigned i=0; i < nodelist.size(); i++ ){
	    cout <<  nodelist.at(i) <<  delimiter;
	}
    }

    int find_child_nodes( const uhal::Node &ipbus_node, pugi::xml_node &node )
    {

	int nchildren(0);
	string node_name = ipbus_node.getPath();
	string parent_path = node_name;
	string prev_node_name;
	string prev_name;

	uhal::Node::const_iterator it = ipbus_node.begin();
	for( it++; it != ipbus_node.end(); it++ ){

	    const uhal::Node &next_ipbus_node = *it;
	    string next_node_name = next_ipbus_node.getPath();
	    string next_node_desc = next_ipbus_node.getDescription();
	    std::size_t found = next_node_name.find_last_of(".");
	    string path = next_node_name.substr( 0, found );
	    string name = next_node_name.substr( found+1 );

	    //child of ipbus_node
	    if( parent_path.compare( path ) == 0 ){
		pugi::xml_node child_node = node.append_child( "node" );	
		child_node.append_attribute( "id" ) = name.c_str();
		child_node.append_attribute( "path" ) = parent_path.substr(6).c_str();	
		if( next_node_desc.compare("") != 0 ) child_node.append_attribute( "description" ) = next_node_desc.c_str();
		prev_node_name = next_node_name;
		prev_name = name;
		nchildren++;
	    }
	    //child of previous child
	    else{
		pugi::xml_node this_node = node.find_child_by_attribute( "node", "id", prev_name.c_str() ); 
		uhal::Node::const_iterator it2 = ipbus_node.begin();
		int nchildren2(0);
		for( ; it2 != ipbus_node.end(); it2++ ){
		    if( it2->getPath().compare( prev_node_name ) == 0 ){
			nchildren2 = find_child_nodes( *it2, this_node ); 
			nchildren += nchildren2;
			break;
		    }
		}
		for( int n=0; n < nchildren2 - 1; n++ )it++;
	    }
	}
	return nchildren;
    }

    void print_register( std::ostream &os, const std::string &node, uint32_t value )
    {
	os << setw(80) << setfill(' ') << node << " : 0x" << std::hex << setw(8) << setfill('0') << std::right << value << endl;
    }
    void print_registers( std::ostream &os, const std::map<std::string,uint32_t> &regmap )
    {
	map<string,uint32_t>::const_iterator it = regmap.begin();
	for(;it!= regmap.end(); it++) print_register( os, it->first, it->second );
    }
    void print_registers( std::ostream &os, const std::vector<std::string> &nodes, const std::vector<uint32_t> &values )
    {
	for(unsigned i=0; i < nodes.size(); i++) print_register( os, nodes.at(i), values.at(i) );
    }
    void read_ipbus( uhal::HwInterface *hw, pugi::xml_node &node )
    {
	for( pugi::xml_node n = node.child( "node" ); n; n = n.next_sibling( "node" ) ){

	    if( n.child( "node" ) ) read_ipbus( hw, n );
	    else{
		string node_name = string( n.attribute("path").value() ) + "." + n.attribute( "id" ).value();
		if( hw->getNode( node_name ).getPermission() & defs::READ ){
		    ValWord<uint32_t> value = hw->getNode( node_name ).read();
		    hw->dispatch();
		    n_read++;
		    n.append_attribute("value") = value;
		}
	    }
	}
    }
    void pugiattr_to_json( std::ostream &os, pugi::xml_node &node, const string &indent )
    {
	for (pugi::xml_attribute_iterator ait = node.attributes_begin(); ait != node.attributes_end();)
	{
	    if( strcmp( ait->name(), "path" ) != 0 ){
		os << indent << "\"@" << ait->name() <<  "\": \"" << ait->value() << "\"";
		if( ++ait != node.attributes_end() ) os << ",";
		os << endl;
	    }
	    else{
		++ait;
	    }
	}
    }
    void pugixml_to_json( std::ostream &os, pugi::xml_node &node, std::string name, const std::string &indent )
    {
	os << indent;
	if ( !name.empty() ) {
	    os << "\"" << name << "\": {";
	}
	else{
	    os << "{" << endl;
	}
	const std::string indent2 = indent + "  ";
	pugiattr_to_json( os, node, indent2 );

	if( node.begin() != node.end() ){

	    os << indent2 << "\"" << node.begin()->name() << "\": [" << endl;
	    for (pugi::xml_node_iterator it = node.begin(); it != node.end(); )
	    {
		const std::string indent3 = indent2 + "  ";
		pugixml_to_json( os, *it, "", indent3 ); 

		if( ++it != node.end() ) os << "," << endl;
		else os << endl;
	    }
	    os << indent2 << "]" << endl;
	}
	if( name.empty() ){
	    os << indent << "}";
	}
	else os << indent << "}" << endl;

    }
};
