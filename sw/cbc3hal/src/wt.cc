#include "wt.h"
#include <map>
#include <uhal/uhal.hpp>
#include "Cbc3BeFc7.h"
#include "Fc7SystemRegInterface.h" 
#include "CbcI2cRegIpbusInterface.h"
#include <fstream>
#include <iomanip>
#include "seq_utils.h"
#include "cbc3daq_heproot_utils.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include "ipbus_utils.h"

using namespace std;

namespace wt{

    set<int> init_waddrs(){
	set<int> wa;
	wa.insert(108);
	wa.insert(226);
	wa.insert(173);
	wa.insert(227);
	wa.insert(239);
	return wa;
    }
    set<int> waddrs(init_waddrs());

    //set of wrong stub bends for address 226 & 227 
    map<int,set<int> > init_oss_osw_wbends(){

	map<int,set<int> > ws;
	set<int> wb;
	wb.insert(1);
	wb.insert(3);
	wb.insert(5);
	wb.insert(7);
	wb.insert(9);
	wb.insert(11);
	wb.insert(13);
	wb.insert(15);
	ws[226] = wb;
	return ws;
    }
    map<int,set<int> > init_oss_esw_wbends(){

	map<int,set<int> > ws;
	set<int> wb;
	wb.insert(2);
	wb.insert(4);
	wb.insert(6);
	wb.insert(12);
	wb.insert(14);
	ws[226] = wb;
	return ws;

    }

    map<int,set<int> > init_ess_osw_wbends(){

	map<int,set<int> > ws;
	set<int> wb;
	wb.insert(2);
	wb.insert(4);
	wb.insert(6);
	wb.insert(12);
	wb.insert(14);
	ws[227] = wb;
	return ws;

    }
    map<int,set<int> > init_ess_esw_wbends(){

	map<int,set<int> > ws;
	set<int> wb;
	wb.insert(1);
	wb.insert(3);
	wb.insert(5);
	wb.insert(7);
	wb.insert(9);
	wb.insert(11);
	wb.insert(13);
	wb.insert(15);
	ws[227] = wb;
	return ws;
    }
    //nss: # of strips in the cluster on the seed layer
    int get_seed_address( int i, int nss ){
	int cs = i * 2 + 2;
	if(!(nss%2)) return cs+1;//even number of hits on seed 
	else return cs;
    }
    //ch [0, -1] = BotIn_A [1, 2]
    //ch [-10, -11, -13] = TopIn_A [1, 2, 3]
    bool get_seed_channels( int i, int nss, std::vector<int> &chs, int ls, bool CBC3_0 = false ){

	int cs = i * 2 + 1 + ls;
	//if(nss%2) cs += 2;
	chs.push_back(cs);
	if(nss > 1) chs.push_back(cs+2);
	if(nss > 2) chs.push_back(cs-2);
	if(nss > 3) chs.push_back(cs+4);
	if(nss > 4) chs.push_back(cs-4);//invalid cluster
	if(nss > 5) chs.push_back(cs+6);//invalid cluster

	for(unsigned s_i=0; s_i < chs.size(); s_i++){
	    if(CBC3_0){
		if(chs.at(s_i) < 1 || chs.at(s_i)>254){
		    chs.clear();
		    return false;
		}	
	    }
	    else{
		if(chs.at(s_i) < -3 || chs.at(s_i)>259){
		    chs.clear();
		    return false;
		}
		if(chs.at(s_i) <1) chs.at(s_i) = chs.at(s_i) / 2;
		if(chs.at(s_i) >254) chs.at(s_i) = (255 - chs.at(s_i))/2 -10;  
	    }
	}
	return true;
    }
    //ch [-20:-31] = BotIn_B [1:12]
    //ch [-40, -52] = TopIn_B [1:13]
    bool get_window_channels( int saddr, int ibend, int nsw, std::vector<int> &chs, int ls, bool CBC3_0 = false ){

	bool nss_odd = !(saddr % 2);
	//int bend_sign = (ibend < 0) ? -1 : 1;
	int half = (nss_odd + nsw)%2;

	//    cout << "nss_odd, bend_sign, half = " << nss_odd << " " << bend_sign << " " << half << endl;
	int cw = (saddr/2 + !nss_odd * half +ibend)*2-ls; 

	chs.push_back(cw);
	if( nsw > 1 ) chs.push_back(cw+2); 
	if( nsw > 2 ) chs.push_back(cw-2);
	if( nsw > 3 ) chs.push_back(cw+4);
	if( nsw > 4 ) chs.push_back(cw-4);
	if( nsw > 5 ) chs.push_back(cw+6);

	for(unsigned w_i=0; w_i < chs.size(); w_i++){
	    if(CBC3_0){
		if(chs.at(w_i) < 1 || chs.at(w_i)>254){
		    chs.clear();
		    return false;
		}
	    }
	    else{
		if(chs.at(w_i) < -23 || chs.at(w_i)>280){
		    chs.clear();
		    return false;
		}
		if(chs.at(w_i) <1) chs.at(w_i) = chs.at(w_i) / 2 - 20;
		if(chs.at(w_i) >254) chs.at(w_i) = (255 - chs.at(w_i))/2 - 40;  
	    }
	}	
	return true;
    }

    std::vector<int> get_stub_channels( int i, int ibend, int nss, int nsw, int ls, bool CBC3_0 = false ){

	std::vector<int> chs_seed;
	bool valid_seed = get_seed_channels(i, nss, chs_seed, ls, CBC3_0); 
	int saddr = get_seed_address(i, nss);
	std::vector<int> chs_window;
	bool valid_window = get_window_channels(saddr, ibend, nsw, chs_window, ls, CBC3_0); 

	if( valid_seed && valid_window ) 
	    chs_seed.insert(chs_seed.end(), chs_window.begin(), chs_window.end());
	else chs_seed.clear();

	return chs_seed;
    }

    const string RED="\033[0;31m";
    const string YELLOW="\033[1;33m";
    const string GREEN="\033[0;32m";
    const string BLUE="\033[0;34m";
    const string NC="\033[0m";

    void print_delta_t( const string &test_name, const struct timeval &dt, std::ostream &os ){

	cout << wt::NC << wt::time_string() << test_name << " took " << dt.tv_sec + dt.tv_usec / 1000000. << " sec." << endl;
    }
    string time_string(){

	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime (&rawtime);

	string TimeFormat = "%d %b %Y %H:%M:%S %Z";

	char Buffer[256];
	Buffer[0] = '[';
	strftime(&Buffer[1], sizeof(Buffer)-2, TimeFormat.c_str(), timeinfo);
	string stime(Buffer);
	stime += "] ";
	return stime;
    }
    void print_test( const string &test_name, const string &log,  std::ostream &os, const std::string &color ){
	os << color << wt::time_string() << test_name << " " << log << endl; 
    }
    void print_buff_hit( bool hit[32][254], std::ostream &os ){

	for(unsigned bi=0; bi<32; bi++){
	    os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	    for(unsigned ch=1; ch<255; ch++){
		if( ch % 32 == 1 ){
		    int ech(0);
		    if( ch / 32 != 7 ) ech = ch + 31;
		    else ech = ch + 29; 
		    os << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
		}
		os << hit[bi][ch-1] << " ";
		if( ch % 32 == 0 ) os << endl;
	    }
	    os << endl;
	}

    }
    void print_buff_pipeline( int paddr_values[32][512], std::ostream &os ){

	for(unsigned bi=0; bi<32; bi++){
	    os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	    for(unsigned i=0; i<512; i++){
		if( i % 32 == 0 ){
		    int ei = i + 31;
		    os << "delay[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
		}
		os << std::setw(3) << std::setfill('0') << std::hex << paddr_values[bi][i] << " ";
		if( i%32 == 31 ) os << endl;
	    }
	}
    }
    void print_buff_l1a_count( int l1a_count[32][512], std::ostream &os ){

	for(unsigned bi=0; bi<32; bi++){
	    os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	    for(unsigned i=0; i<512; i++){
		if( i % 16 == 0 ){
		    int ei = i + 15;
		    os << "data block[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
		}
		os << std::setw(3) << std::setfill('0') << std::hex << l1a_count[bi][i] << " ";
		if( i%16 == 15 ) os << endl;
	    }
	}

    }
    int KeithleyMultiMeter::km_ext_trig_delay = 300000;
    KeithleyMultiMeter::KeithleyMultiMeter(const char *s, bool debug):
	_debug(debug),
	_buflen(50000),_rbuf(0), _rbuf_copy(0), 
	_re_DC(0.00012), _re_Reg(0.00020), _range("1"), _re_range(0.0000003){

	_print_sumdt.tv_sec = 0;
	_print_sumdt.tv_usec = 0;

	_read_sumdt.tv_sec = 0;
	_read_sumdt.tv_usec = 0;

	usbtmc_init();
	//usbtmc timeout in ms 
	int usbtmc_timeout=5000; 
	//open a link per instrument 
	char bus_dev[2] = "";
	char product[256];
	char sern[256];
	strcpy(product, "2110 Multimeter");
	strcpy(sern, s);
	_ut_handle=usbtmc_open(bus_dev,product,sern,usbtmc_timeout);
	if (_ut_handle){
	    usbtmc_clear(_ut_handle); 
	    kmm_printf("*RST"); 
	    kmm_printf("*CLS");
	    kmm_printf(":SENSE:VOLT:NPLC 0.001");      //time to average the input for each measurement, in power line cycles
	    _re_range = 0.0003;
	    kmm_printf(":SENSE:ZERO:AUTO OFF");              //autozero on request only
	    kmm_printf(":TRIGger:DELay:AUTO OFF");     //trigger auto delay off 
	    kmm_printf(":TRIGger:DELay 0");            //trigger delay  
	}
	else{
	    throw WaferTestException( "Keithley multimeter is not found.", WT_ERR_KMM );
	}
	_rbuf = new unsigned char[_buflen+1];
	_rbuf_copy = new unsigned char[_buflen+1]; 
    }
    KeithleyMultiMeter::~KeithleyMultiMeter(){ 

	if(_ut_handle)
	    usbtmc_close(_ut_handle); /*delete _rbuf;*/

	delete [] _rbuf;
	delete [] _rbuf_copy;
    }

    void KeithleyMultiMeter::kmm_printf(const char *c){ 

	char com[256]; 
	strcpy(com, c); 

	if(_debug)
	    cout << "kmm_printf() com = " << com << endl;
	struct timeval t0, t, dt, prev_sumdt;
	gettimeofday(&t0,0);

	if( usbtmc_printf(_ut_handle, com) == -1 ) 
	    throw WaferTestException( "KEITHLEY write error.", WT_ERR_KMM );

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	prev_sumdt = _print_sumdt;
	timeradd(&prev_sumdt, &dt, &_print_sumdt);
    }

    int KeithleyMultiMeter::kmm_read(){ 

	struct timeval t0, t, dt, prev_sumdt;
	gettimeofday(&t0,0);

	int reason;
	int nread(0);
	while(nread==0){
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if(dt.tv_sec > 10) throw WaferTestException( "KEITHLEY read error. No data.", WT_ERR_KMM );
	    nread = usbtmc_read(_ut_handle, _rbuf, _buflen, &reason);
	    if(_debug)
		cout << "usbtmc_read() returned : " << std::dec << nread << " " << reason << endl;
	    if(nread < 0 || nread > _buflen ) 
		throw WaferTestException( "KEITHLEY read error.", WT_ERR_KMM );
	}
	strncpy((char*)_rbuf_copy,(char *) _rbuf, nread);
	_rbuf_copy[nread] = 0;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	prev_sumdt = _read_sumdt;
	timeradd(&prev_sumdt, &dt, &_read_sumdt);
	return nread;
    }
    int KeithleyMultiMeter::kmm_read_ndata(int ndata){ 

	bool cmp(false);
	kmm_printf("*OPC?");
	while(!cmp){
	    kmm_read();
	    if(_debug)
		cout << _rbuf_copy << endl;
	    cmp = (bool) strtof((char *)_rbuf_copy,0);
	}

	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	int points = 0;
	while(1){
	    kmm_printf(":DATA:POINts?");

	    int nread = kmm_read();
	    if(nread != 14) throw WaferTestException( string("KEITHLEY :DATA:POINts returned invalid answer") + (char *)_rbuf_copy , WT_ERR_KMM );
	    if(_debug)
		printf("data points = %s\n", _rbuf_copy);
	    points = (int)( strtof( (char *)_rbuf_copy, 0 ));

	    if(points == ndata) break;

	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > (ndata / 10 + 1) ){
		ostringstream oss;
		oss << points << ".";
		throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	    }
	}
	kmm_printf(":FETCH?");
	int nlen_read = kmm_read();
	gettimeofday(&t0,0);
	while(nlen_read != 16 * ndata){
	    kmm_printf(":FETCH?");
	    nlen_read = kmm_read();
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > (ndata / 10 + 1) ){
		ostringstream oss;
		throw WaferTestException( "KEITHLEY FETCH does not complete.", WT_ERR_KMM );
	    }
	}

	return nlen_read;
    }
    void KeithleyMultiMeter::set_DC_measurement(std::string nplc, std::string range, int sample_count, int trig_count, bool ext, int delay){

	char tmp[256];

	kmm_printf(":SENSE:FUNC \"VOLT:DC\"");     //dc voltage measurements

	sprintf(tmp, ":SENSE:VOLT:NPLC %s", nplc.c_str());
	kmm_printf(tmp);

	if( nplc == "0.001" )
	    _re_range = 0.0003;
	else if( nplc == "0.2" )
	    _re_range = 0.00001;
	else if( nplc == "0.6" )
	    _re_range = 0.000005;
	else if( nplc == "1" )
	    _re_range = 0.000003;

	if(_debug){
	    kmm_printf(":SENSE:VOLT:NPLC?");
	    kmm_read();
	    cout << "NPLC is set to " << _rbuf_copy << " and the resolution is " << _re_range << endl;
	}

	sprintf(tmp, ":SENSE:VOLT:RANG %s", range.c_str());
	kmm_printf(tmp);
	set_range(range);

	sprintf(tmp, ":TRIGger:DELay %d", delay);
	kmm_printf(tmp);            //trigger delay  

	if(ext) kmm_printf(":TRIGger:SOURce EXTernal");
	else kmm_printf(":TRIGger:SOURce IMMediate");

	sprintf(tmp, ":SAMPle:COUNt %d", sample_count);
	kmm_printf(tmp);
	sprintf(tmp, ":TRIGger:COUNt %d", trig_count);
	kmm_printf(tmp);

	bool cmp(false);
	kmm_printf("*OPC?");
	while(!cmp){
	    kmm_read();
	    if(_debug)
		cout <<_rbuf_copy << endl;
	    cmp = (bool) strtof((char *)_rbuf_copy,0);
	}
    }
    std::vector<float> KeithleyMultiMeter::get_kmm_vdata(){

//	cout << _rbuf << endl;
	std::vector<float> meas;
	char *p;
	p = strtok((char *)_rbuf_copy, ",");
	while (p != 0){
	    float fv = strtof(p,0);
	    meas.push_back(fv);
	    p = strtok(0, ",");
	}
	return meas;
    }

    using namespace uhal;
    using namespace cbc3hal;

    void WaferTestParameters::set( std::string item, std::string value ){ str_params[item] = value; }
    void WaferTestParameters::set( std::string item, float value ){ num_params[item] = value; }
    void WaferTestParameters::set( std::string item, std::vector<float> value ){ vnum_params[item] = value; }

    void WaferTestParameters::dump(ostream &os){

	char tmp[256];
	std::map<std::string, std::string>::iterator it_str = str_params.begin();
	for(; it_str != str_params.end(); it_str++){
	    sprintf(tmp, "%-20s%50s", it_str->first.c_str(), it_str->second.c_str());
	    os << tmp << endl;
	}

	std::map<std::string, float>::iterator it_num = num_params.begin();
	for(; it_num != num_params.end(); it_num++){
	    sprintf(tmp, "%-20s %6f", it_num->first.c_str(), it_num->second);
	    os << tmp << endl;
	}
	std::map<std::string, std::vector<float> >::iterator it_vec = vnum_params.begin();
	for(; it_vec != vnum_params.end(); it_vec++){
	    sprintf(tmp, "%-20s", it_vec->first.c_str());
	    os << tmp; 
	    std::vector<float> vec = it_vec->second;
	    for(unsigned i=0; i < vec.size(); i++){
		os << std::setw(10) << vec.at(i);
	    }	
	    os << endl;
	}
	
    }
    float KeithleyMultiMeter::get_DC_err(float v)const{ 
	float range = strtof(_range.c_str(), 0);
	return v * _re_DC + range * _re_range; 
    } 
    std::vector<string> init_test_list(){

	vector<string> test_list;
	//CBC hard reset, FC7 configuration, CBC I2C communication initialization, data clock timing tuning with FCI delay 0. 
	test_list.push_back("frame03");

	//Frame 4 - fast command sequencer configuration moved to Frame 11 in my procedure.
	
	//stuck bits test - main bias settings 
	test_list.push_back("frame05");

	//set up external MUX for Keithley DMM to read VDDA
	test_list.push_back("frame06");

	//stuck bits test - main bias settings
	test_list.push_back("frame07");

	//load default values for main bias settings
	test_list.push_back("frame08");

	//Frame 9 - no operation
	
	//check VDDD and VLDOI currents (<50mA & <200mA) by Keithley
	test_list.push_back("frame10");

	//(Mark's Frame 4, and Frame 11 are merged here. 
	//Just to see if the CBC data frame comes and header is found.
	test_list.push_back("frame11");
	//Frame 12 - no operation
	
	//Stuck bit test - bend LUTs, channel offsets, channel mask 1
	test_list.push_back("frame13_a");

	//Stuck bit test - bend LUTs, channel offsets, channel mask 0
	test_list.push_back("frame13_b");

	//bandgap tuning
	test_list.push_back("frame14");

	//after burn test
	test_list.push_back("frame15");
	//offset tuning
	test_list.push_back("frame16");
	//vcth scan scurve analysis
	test_list.push_back("frame17");
	//pipeline check
	test_list.push_back("frame18");
	//buffer check
	test_list.push_back("frame19");
	//channel mask check
	test_list.push_back("frame20");
	//stub check
	test_list.push_back("frame21");
	test_list.push_back("frame22");
	test_list.push_back("frame23");
	test_list.push_back("frame24");
	//hip test
	test_list.push_back("frame25");
	//FCI check
	test_list.push_back("frame26");
	//DLL check
	test_list.push_back("frame27");
	//bias scan
	test_list.push_back("frame28");
	//current scan
	test_list.push_back("frame29");
	//AMUX
	test_list.push_back("frame30");

	return test_list;
    }
    const std::vector<string> WaferTest::test_list(init_test_list());

    WaferTest::WaferTest(const std::string & config_file_name, const std::string &setting_file_dir, std::ostream &os, bool tune, bool CBC3_0, int debug):
	_setting_file_dir(setting_file_dir + '/'), _cbc3be(0), _hw(0), _kmm(0), _ut_handle(0), _cbc_id(1), _os(os), _tune(tune), _CBC3_0(CBC3_0), _debug(debug),_result(0), 
	_cbci2c_page1_main(""), _cbci2c_page2(""), _run_config(""), _i2c_bg(false){

	    string wtype("CBC3.1");
	    if( _CBC3_0 ) wtype = "CBC3.0";
	_os << "----------------------------" << endl;
	_os << "Wafer type : " << wtype << endl;
	_os << "----------------------------" << endl;
	_os << "WaferTest configuration file is " << config_file_name << endl;
	ifstream fs( config_file_name.c_str() );
	char line[256];
	string test_name, item, value;
	if(fs.is_open()){
	    while(!fs.eof()){
		fs.getline(line,256);
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		    continue;
		}
		istringstream iss( line );
		iss >> test_name >> item >> value;
		WaferTestSettings &settings = _test_settings[test_name];  
		settings.set(item, value);
	    }
	}
	else{
	    ostringstream oss;
	    oss << "File " << config_file_name << " could not be opened.";
	    throw WaferTestException(oss.str(), WT_ERR_SETTINGS);
	}
	_os << "----------------------------" << endl;
	_os << "Wafer test configurations" << endl;
	map<string, WaferTestSettings>::iterator it = _test_settings.begin();
	for(; it != _test_settings.end(); it++){

	    WaferTestSettings &wt_settings = it->second;
	    map<string,string> settings = wt_settings.get_settings();

	    map<string,string>::iterator it_2 = settings.begin();
	    for( ; it_2 != settings.end(); it_2++){
		_os << "[" << it->first << "]";
		_os << " " << std::setw(20) << it_2->first << " " << it_2->second << endl;
	    }
	}
	_os << "----------------------------" << endl;
    }
    WaferTest::~WaferTest(){ delete _cbc3be; delete _hw; delete _kmm; };

    void WaferTest::set_test_setting( const std::string &test_name, const std::string &item, const std::string &value){
	WaferTestSettings &wt_settings = _test_settings[test_name];
	map<string,string> &the_settings = wt_settings.get_settings();
	the_settings[item] = value;
	_os << "[" << test_name << "] setting updated";
	_os << "" << setw(20) << item << " " << value << endl;
    }

    int WaferTest::run_test( const string &test_names ){

	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	for(unsigned int i=0; i < test_list.size(); i++){

	    const string &this_test = test_list.at(i);
	    std::map<std::string, std::string> this_settings = _test_settings[this_test].get_settings(); 
	    if(this_test == "frame03" || test_names.find(this_test)!=std::string::npos){
		_result |= test(this_test, this_settings); 
	    }
	}

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	print_delta_t( "wafer_test", dt, _os );

	print_test("wafer_test", "END-----------------------------", _os);
	return _result;

    }
    void WaferTest::measure_currents( const std::string &test_name, float &vddd_cur, float &vldoi_cur ){

	char tmp[256];
	//--------------------
	//VDDD current measurement
	//--------------------
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDD_I );
	usleep( 2000 );

	_kmm->set_DC_measurement("0.001", "0.1", 1, 1, false);
	_kmm->kmm_printf(":INIT");	
	_kmm->kmm_read_ndata(1); 

	float Rres(0.2);

	float val = strtof((const char *)_kmm->get_rbuf(),0);
	vddd_cur = val / Rres * 1000;//mA

	sprintf( tmp, "%s:VDDD_I:mA", test_name.c_str());
	_parameters.set(tmp, vddd_cur);

	char tmpbuf[256];
	sprintf( tmpbuf, "VDDD current : %3.1f mA", vddd_cur );
	print_test(test_name, tmpbuf, _os); 

	//--------------------
	//VLDOI measurement
	//--------------------
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VLDOI_I );
	usleep( 2000 );
	_kmm->kmm_printf(":INIT");	
	_kmm->kmm_read_ndata(1); 

	val = strtof((char *)_kmm->get_rbuf(),0);
	vldoi_cur = val / Rres * 1000;//mA
	sprintf( tmp, "%s:VLDOI_I:mA", test_name.c_str());
	_parameters.set(tmp, vldoi_cur);

	sprintf( tmpbuf, "VLDOI current : %3.1f mA", vldoi_cur );
	print_test(test_name, tmpbuf, _os); 
    }
    cbc3hal::Cbc3BeFc7 *WaferTest::init_be( std::string &connection_file, std::string &id){

	ConnectionManager cm( string( "file://" ) + connection_file ); 
	_hw = new HwInterface( cm.getDevice( id ) );
	_os << wt::NC << wt::time_string() << "FC7     firmware id       : " << fc7::read_board_id( _hw ) << endl;
	_os << wt::NC << wt::time_string() << "FC7     firmware revision : " << fc7::read_rev_id( _hw )   << endl;
	_os << wt::NC << wt::time_string() << "FC7     firmware version  : " << fc7::read_version( _hw )  << endl;


//	_cbc3be = new Cbc3BeFc7(_CBC3_0, _debug);
	_cbc3be = new Cbc3BeFc7(true, _debug);
	if(!_CBC3_0){
	    wt::waddrs.clear();
	}
	_cbc3be->Initialize( connection_file, id );
	_os << wt::NC << wt::time_string() << "CBC3 BE firmware id       : " << _cbc3be->ReadId()      << endl;
	_os << wt::NC << wt::time_string() << "CBC3 BE firmware version  : " << _cbc3be->ReadVersion() << endl;
	//temporary.
	string node = "cs_ctrl.seq.reset";
	ipbutl::write_ipbus( _cbc3be->GetHwInterface(), node, 1 );
	return _cbc3be;
    }
    usbtmc_device_data *WaferTest::init_kmm(const std::string &s){ 
	_kmm = new KeithleyMultiMeter(s.c_str(), _debug); 
	return _kmm->ut_handle(); 
    }

    int WaferTest::config_be( std::string &be_config_file, int fci){

	//BE board config, CBC reset, BE CBCI2C bus init, fast reset to CBC, CBC data timing tuning.
	_cbc3be->ConfigureBeBoard( be_config_file, fci );
	_cbc_id = _cbc3be->ReadBeBoardRegister( "cs_cnfg.cbc01.id" );
	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	_run_config        = "default";

	return 0;
    }
    int WaferTest::cbci2c_write_read_test( const std::string &test_name, std::map<std::string, std::string> &settings ){ 

	string fname = _setting_file_dir + settings["cbci2c_file"];

	print_test( test_name, string("CBC I2C write and readback test with ") + fname, _os);

	_cbc3be->ClearCbcI2cRegReplies();
	bool good = _cbc3be->rwCbcI2cRegs( 1, 1, fname, _cbc_id );
	_cbci2c_page1_main = fname;

	int result(0);

	result = !good ? WT_ERR_I2C_STUCK_BITS : 0;

	return result;
    }
    int WaferTest::bgmeas_bgscan( const string &test_name, const string &fname, map<unsigned,float> &bg_meas, ostream &os, bool inc, int pause_n100us){

	char tmp[256];

	sprintf(tmp, "Bandgap setting scan and bandgap voltage measurements with pause = %.2f ms", pause_n100us * 0.1); 
	print_test(test_name, tmp, os); 

	//band gap read setting for relayA
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	usleep( 2000 ); 
	usleep( 10000 ); 
	
	//keithley multimeter settings
	_kmm->set_DC_measurement("0.001", "1", 1, 64, true);
	_kmm->kmm_printf(":INIT");

	vector<uint32_t> fscbuf_data;

	unsigned bg_reg_addr = 21; 
	unsigned reg_value(0);
	int ntrig(0);
	for( unsigned i=0; i < 64; i++){
	    int bg(i);
	    if(!inc) bg = 63 - i; 
	    reg_value = 0x81100000 | (bg_reg_addr << 8) | (1 << 7) | bg;//override(1:override), sel(0:register)
	    fscbuf_data.push_back(reg_value);
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    int p(0);
	    while (p < pause_n100us){
		fscbuf_data.push_back((unsigned) 0xafa00000);
		p++;
	    }
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);

//	dump_seq_data(fscbuf_data,_os);

	_cbc3be->WriteFscBram(fscbuf_data);

	//run sequencer 
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);

	//keithley read
	_kmm->kmm_read_ndata(ntrig); 

	//writing the measurements to a file 
	ofstream ofile(fname.c_str());
	ofile << "# Title: Bandgap voltage values for BC I2C setting 0 - 63 " << endl;
	ofile << "# Fields: BandGapI2C BandGap(V)" << endl; 
	vector<float> vmeas = _kmm->get_kmm_vdata();
	for(unsigned int bg=0; bg < 64; bg++){
	    int i(bg);
	    if(!inc) i = 63-bg;
	    bg_meas[bg] = vmeas.at(i);
	    float err = _kmm->get_DC_err( vmeas.at(i) );
	    sprintf( tmp, "0x%02x %1.4f %1.4f\n", bg, vmeas.at(i), err );
	    ofile << tmp;
	}
	return 0;
    }
    int WaferTest::vddameas_bgscan( const string &test_name, const string &fname, map<unsigned,float> &vdda_meas, ostream &os, bool inc, int pause_n100us){

	char tmp[256];
	sprintf(tmp, "Bandgap setting scan and VDDA voltage measurements with pause = %.2f ms", pause_n100us * 0.1); 
	print_test(test_name, tmp, os); 

	//VDDA read setting for relayA
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
	usleep( 2000 ); 
	usleep(10000);
	//change KMM state to trigger waiting
	int sample_count(1);
	int trig_count(64);
	_kmm->set_DC_measurement("0.001", "10", sample_count, trig_count, true);
	_kmm->kmm_printf(":INIT");

	vector<uint32_t> fscbuf_data;

	unsigned bg_reg_addr = 21; 
	unsigned reg_value(0);
	int ntrig(0);
	for( unsigned i=0; i < 64; i++){
	    int bg(i);
	    if(!inc) bg = 63 - i; 
	    reg_value = 0x81100000 | (bg_reg_addr << 8) | (1 << 7) | bg;//override(1:override), sel(0:register)
	    fscbuf_data.push_back(reg_value);
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    int p(0);
	    while (p < pause_n100us){
		fscbuf_data.push_back((unsigned) 0xafa00000);
		p++;
	    }
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	_cbc3be->WriteFscBram(fscbuf_data);

	//run sequencer 
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(ntrig*100000);

	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);

	//wait keithley read
	_kmm->kmm_read_ndata(ntrig); 

	//writing the measurements to a file 
	ofstream ofile(fname.c_str());
	ofile << "# Title: VDDA voltage values for BC I2C setting 63 - 0 " << endl;
	ofile << "# Fields: BandGapI2C VDDA(V)" << endl; 
	vector<float> vmeas = _kmm->get_kmm_vdata();
	for(unsigned int bg=0; bg < 64; bg++){
	    int i(bg);
	    if(!inc) i = 63-bg;
	    vdda_meas[bg] = vmeas.at(i);
	    float err = _kmm->get_DC_err( vmeas.at(i) );
	    sprintf( tmp, "0x%02x %1.4f %1.4f\n", bg, vmeas.at(i), err );
	    ofile << tmp;
	}
	return 0;
    }
    int WaferTest::find_best_bgi2c( map<unsigned,float> &meas, float target){

	int best_bgi2c(0);
	float best(0);
	float diff(1);

	for(unsigned int bg=0; bg < 64; bg++){
	    //	cout << i << ", " << bg_meas.at(i) << fabs(bg_meas.at(i) - target_bg) << ", " << diff << endl;
	    if( fabs(meas[bg] - target) < diff ){
		//	    cout << "best found." << endl;
		best_bgi2c = bg;
		best = meas[bg];
		diff = fabs(best-target);
	    } 
	}
	return best_bgi2c;
    }
    bool WaferTest::offset_tuning( const string &test_name, int sample_count){

	char tmp[256];

	int group_type(8);
	const GROUP_CHANNELSET_MAP &group_map = Cbc3BeInterface::SetGroupChannelSetMap(group_type);

	CBCI2C_ITEM_DATA_MAP item_data;
	//Get initial VCTH to recover the value after the tuning
	item_data.clear();
	CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
	_cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, _cbc_id );
	//Finding pedestal VCTH
	map<CBC_ID, vector<unsigned> > offsets0_map;
	vector<unsigned> offsets0(254,0x80);
	offsets0_map[_cbc_id] = offsets0;
	_cbc3be->FindVcth( sample_count, 0.5, &offsets0_map );
	unsigned target_ped_vcth(0);
	_cbc3be->rwCbcI2cRegsByName( 1, 0, "VCTH.vcth", target_ped_vcth, _cbc_id );
	_parameters.set( test_name + ":target_pedestal_vcth", target_ped_vcth);

	map<CBC_ID, map<CHANNEL_ID,unsigned> > all_offsets_map;

	for( unsigned g=0; g<group_map.size(); g++){

	    map<CBC_ID, map<CHANNEL_ID,unsigned> > offsets_map;
	    _cbc3be->TuneOffsets(_cbc_id, g,sample_count, offsets_map[_cbc_id]);

	    //add the tuned values in the global offset map
	    all_offsets_map[_cbc_id].insert(offsets_map[_cbc_id].begin(), offsets_map[_cbc_id].end());
	}

	//Writing the tuned values to CBC 
	_cbc3be->rwCbcI2c_Offsets(1,1,all_offsets_map[_cbc_id],_cbc_id);
	//Initial VCTH value is written to CBC 
	_cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, _cbc_id );

	//Making the offset distribution histogram and saved in a root file.
	sprintf(tmp, "offset_tuning_be%d.root", _cbc3be->GetBeId() );
	_parameters.set( test_name + ":offset_root_file", tmp);
	TFile *froot = new TFile(tmp, "recreate");
	//offset histogram is filled.
	TH1F *h = new TH1F( Form("hoffsets_be%dcbc%d",_cbc3be->GetBeId(), _cbc_id), "; offset; count", 256, -0.5, 255.5 );
	map<CHANNEL_ID,unsigned> the_map =  all_offsets_map[_cbc_id];
	map<CHANNEL_ID,unsigned>::iterator it_the_map = the_map.begin();
	for(; it_the_map != the_map.end(); it_the_map++){
	    h->Fill(it_the_map->second);
	}
	froot->Write();
	double mean_offset = h->GetMean();
	double mean_offset_err = h->GetMeanError();
	_parameters.set( test_name + ":mean_offset", mean_offset);
	_parameters.set( test_name + ":mean_offset_err", mean_offset_err);
	froot->Close();

	//New configuration file is opened.
	sprintf(tmp, "offset_tuning-be%dcbc%d.knt", _cbc3be->GetBeId(), _cbc_id );
	_parameters.set( test_name + ":cbci2c_offset_file", tmp);
//	_parameters.dump(_os);
	ofstream ofile(tmp);
	_cbc3be->ReadAllCbcConfig(_cbc_id);
	CBCI2C_REGS regs = _cbc3be->GetCbcI2cRegReplies();
	print_regcnf(regs, ofile, _cbc_id);
	ofile.close();

	return 0;
    }
    bool WaferTest::vcth_scan_scurve_test( const std::string &test_name, int sample_count, bool tp_en, 
	    std::map<CHANNEL_ID, double> &midp, std::map<CHANNEL_ID, double> &midp_e, double &mean, double &mean_e, double &sigma, double &sigma_e, double noise_lim ){

	int result(0);
	char tmp[256];
	bool fit_failed(false);
	int n_goodfits(0);
	double gmin(1023), gmax(0);
	double sum_m(0), sum2_m(0), sum_s(0), sum2_s(0), sum2_me(0), sum2_se(0);

	CBCI2C_ITEM_DATA_MAP item_data;
	//Get initial VCTH to recover the value after the tuning
	item_data.clear();
	CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
	_cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, _cbc_id );

	unsigned i2c_tp_en(0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", i2c_tp_en, _cbc_id );

	unsigned value = 0;
	if(tp_en) value = 1;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );

	//Get initial offsets
	map<unsigned, vector<unsigned> > offsets0;
	_cbc3be->rwCbcI2c_Offsets(1,0,offsets0[_cbc_id],_cbc_id);
	//Set all offsets to 0xFF
	unsigned offset(0xFF);
	_cbc3be->rwCbcI2c_Offset(1,1,0,offset,_cbc_id);

	//Opening ROOT file to save histograms.
	string type("pedestal");
	if(tp_en) type = "tp";

	string ofname( test_name + "_scurve_" + type + ".root");
	_parameters.set( test_name + ":"+type+"_root_file", ofname);

	TFile fout(ofname.c_str(), "recreate");

	string oftxtname( test_name + "_scurve_" + type + ".txt");
	_parameters.set( test_name + ":"+type+"_text_file", oftxtname);
	ofstream oftxt(oftxtname.c_str());
	oftxt << "# Title: VCTH scan s-curve analysis result." << endl;
	oftxt << "# Fields: CHANNEL RCHI2 MIDP MIDP_ERR SIGMA SIGMA_ERR" << endl;   


	int group_type(8);
	Cbc3BeInterface::SetGroupChannelSetMap(group_type);
	int ng = Cbc3BeInterface::GetGroupChannelSetMap().size(); 
	//VCTH scan
	for( int g=0; g<ng; g++){
	    if( tp_en ){
		CBCI2C_ITEM_DATA_MAP item_data;
		item_data["group"] = g;
		_cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data);
		// cout << "group = " << std::dec << g << " delay = " << tp_delay << endl;
	    }
	    _cbc3be->ActivateGroupOffsets( g, offsets0, 0 );

	    CbcHistList<TH1F> hlist;
	    //	map<string, TH1F *> hmap;
	    bool desc(false);
	    int min(-1);
	    int max(-1);
	    _cbc3be->ScanVcth(g, sample_count, hlist, desc, min, max );

	    int be_id(0), cbc_id(0), ch(0);
	    map<unsigned, TH1F*> &hmap = hlist.GetHistMap();
	    map<unsigned,TH1F *>::iterator it_hmap = hmap.begin();
	    for( ; it_hmap != hmap.end(); it_hmap++ ){
		TH1F *hist = it_hmap->second;
		TString hname = hist->GetName();
		if(hname.Contains("ch")){
		    sscanf( hname, "hbe%dcbc%02dch%03d", &be_id, &cbc_id, &ch );
		    double m(0), m_e(0), s(0), s_e(0);
		    double rchi2 = analyze_scurve(hist, m, m_e, s, s_e, gmin, gmax, noise_lim );
		    sprintf(tmp, "%03d %10.4f %10.4f %10.4f %10.4f %10.4f\n", ch, rchi2, m, m_e, s, s_e); 
		    oftxt << tmp; 

		    if(rchi2> 0){
			midp[ch] = m;
			midp_e[ch] = m_e;
			sum_m += m;
			sum2_m += m*m;
			sum2_me += m_e*m_e;
			sum_s += s;
			sum2_s += s*s;
			sum2_se += s_e*s_e;
			n_goodfits++;
		    }
		    else{
			midp[ch] = -2000;
			midp_e[ch] = -2000;
			fit_failed = true;
			sprintf(tmp, " Fit failed for channel %d.", ch);
			//throw WaferTestException( test_name + tmp, WT_ERR_VCTH_SCAN ); 
		    }
		}
		it_hmap->second->Write();
//		it_hmap->second->Print();
	    }

	    _cbc3be->DisableGroupOffsets( g, 0 );
	}
	fout.Close();

//	cout << "n_goodfits = " << n_goodfits << endl;
	mean = sum_m / n_goodfits;
	mean_e = sqrt( (sum2_m/n_goodfits - mean*mean)) + sqrt(sum2_me/n_goodfits);
	sigma = sum_s / n_goodfits;
	sigma_e = sqrt( (sum2_s/n_goodfits - sigma*sigma)) + sqrt(sum2_se/n_goodfits);

	_cbc3be->rwCbcI2c_Offsets(1,1,offsets0[_cbc_id],_cbc_id);
	_cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, _cbc_id );

	sprintf( tmp, "# Statistics %d good fits (mean, mean err, sigma, sigma err) = (%.3f, %.3f, %.3f, %.3f)\n", n_goodfits, mean, mean_e, sigma, sigma_e );
	oftxt << tmp;
	oftxt.close();
	//print_test(test_name, tmp, _os);

	/*
	string command = "root -q -b '";
	command = command + cbc3hal_dir + "/root_scripts/scurve_analyzer_wt.C+(\"";
	command = command + fname + "\", \"be1cbc01\",\"\", 1,0 )' 2>&1 >& rootlog.txt";
	system( command.c_str() );
	*/
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", i2c_tp_en, _cbc_id );

	result = fit_failed ? WT_ERR_VCTH_SCAN : 0;
	return result;
    }

    int WaferTest::vcth_scan( const string &test_name, const string &fname, map<unsigned,float> &vcth_meas, ostream &os, bool set_relayA, bool inc, int pause_n100us){

//	cout << "pause_n100us = " << pause_n100us << endl;

	char tmp[256];

	if(set_relayA){
	    //relayA setting to amux 
	    _cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	    usleep( 2000 );
	}
	unsigned amux_reg_addr = 15; 
	//amux setting 5, VCTH
	unsigned reg_value(37);
	_cbc3be->rwCbcI2cReg( 1, 1, 1, amux_reg_addr, reg_value, _cbc_id );
	usleep(10000);

	//keithley multimeter settings
	_kmm->set_DC_measurement("0.2", "10", 1, 512, true);

	vector<uint32_t> fscbuf_data;
	vector<float> vmeas;
	for( unsigned i_h9=0; i_h9 < 2; i_h9++){
	    _kmm->kmm_printf(":INIT");
	    int ntrig(0);
	    for( unsigned i_h8=0; i_h8 < 2; i_h8++){
		for( unsigned i_l=0; i_l < 256; i_l++){
		    unsigned vcth(i_h9 << 9 | i_h8 << 8| i_l);
		    if(!inc)vcth = 1023-vcth;
		    mk_seqdata_write_vcth(vcth, fscbuf_data);
		    fscbuf_data.push_back((unsigned) 0x22000000);
		    int p(0);
		    while (p < pause_n100us){
			fscbuf_data.push_back((unsigned) 0xafa00000);
			p++;
		    }
		    if( (inc && (vcth == 512 || vcth == 256 || vcth == 768))
			|| (!inc && (vcth == 511 || vcth == 255 || vcth == 767))
			|| (i_h9 == 0 && i_h8 == 0 && i_l == 0 )
		       	){
			int pp(0);
			while(pp<100){
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    fscbuf_data.push_back((unsigned) 0xafa00000);
			    pp++;
			}
		    }
		    fscbuf_data.push_back((unsigned) 0x00000001);
		    fscbuf_data.push_back((unsigned) 0x28000000);
		    ntrig++;
		}
		fscbuf_data.push_back((unsigned) 0xe0000000);
		_cbc3be->WriteFscBram(fscbuf_data);

		_cbc3be->StartFscBramController();
		_cbc3be->WaitSequencerReady(60);
		//    reset_cbc_i2c_reply_fifos(_hw,1);
		check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
		fscbuf_data.clear();
	    }
//	    cout << "ntrig = " << ntrig << endl;
	    //keithley read
	    _kmm->kmm_read_ndata(ntrig); 
	    //cout << "kmm nread = " << nread << endl;
	    vector<float> this_vmeas = _kmm->get_kmm_vdata();
	    vmeas.insert(vmeas.end(), this_vmeas.begin(), this_vmeas.end());
	}

//	const unsigned char * buf = _kmm->get_rbuf(); 
//	cout << buf << endl;

	//writing the measurements to a file 
	ofstream ofile(fname.c_str());
	ofile << "# Title: VCTH voltage values for VCTH I2C setting 0 - 1023 " << endl;
	ofile << "# Fields: VCTH I2C VCTH(V) VCTHerror(V)" << endl; 
	//cout << "vmeas.size()=" << vmeas.size() << endl;
	for(unsigned int vcth=0; vcth < 1024; vcth++){
	    int i(vcth);
	    if(!inc) i = 1023-vcth;
	    vcth_meas[vcth] = vmeas.at(i);
	    float err = _kmm->get_DC_err( vmeas.at(i) );
	    sprintf( tmp, "0x%02x %1.4f %1.4f\n", vcth, vmeas.at(i), err );
	    ofile << tmp;
	}
	ofile.close();

	return 0;
    }
    int WaferTest::vplus_scan( const string &test_name, const string &fname, bool vplus1, unsigned VPLUS0, map<unsigned,float> &vplus_meas, ostream &os, bool set_relayA, bool inc, int pause_n100us){

	char tmp[256];

	if(set_relayA){
	    //relayA setting to amux 
	    _cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	    usleep( 2000 );
	}

	//keithley multimeter settings
	_kmm->set_DC_measurement("0.2", "10", 1, 16, true);
	_kmm->kmm_printf(":INIT");

	//amux setting
	unsigned amux_reg_addr = 15; 
	//amux setting 17, vplus1 
	unsigned reg_value(49);
	//amux setting 16, vplus2 
	if(vplus1 == false) reg_value = 48;
	/*
	unsigned value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
	fscbuf_data.push_back(value);
	*/
	_cbc3be->rwCbcI2cReg( 1, 1, 1, amux_reg_addr, reg_value, _cbc_id );
	usleep(10000);

	//scan
	vector<uint32_t> fscbuf_data;
	unsigned vplus_reg_addr = 11; 
	int ntrig(0);
	for( unsigned i=0; i < 16; i++){
	    // i2c setting
	    unsigned vplus(i);
	    if(!inc)vplus = 15-vplus;
	    if(i!=0){
		if(vplus1 == true)
		    reg_value = 0x81100000 | (vplus_reg_addr << 8) | (VPLUS0 & 0xf0) | vplus;//vplus1
		else
		    reg_value = 0x81100000 | (vplus_reg_addr << 8) | vplus << 4 | (VPLUS0 & 0x0f);//vplus2
		fscbuf_data.push_back(reg_value);
		fscbuf_data.push_back((unsigned) 0x22000000);

		int p(0);
		while (p < pause_n100us){
		    fscbuf_data.push_back((unsigned) 0xafa00000);
		    p++;
		}
	    }

	    //triger kmm
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	_cbc3be->WriteFscBram(fscbuf_data);

	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(60);
	//    reset_cbc_i2c_reply_fifos(_hw,1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	fscbuf_data.clear();

	//cout << "ntrig = " << ntrig << endl;
	//keithley read
	_kmm->kmm_read_ndata(ntrig); 
	//cout << "kmm nread = " << nread << endl;
	vector<float> vmeas = _kmm->get_kmm_vdata();

	//writing the measurements to a file 
	ofstream ofile(fname.c_str());
	if(vplus1){
	    ofile << "# Title: VPLUS1 voltage values for VPLUS1 I2C setting 0 - 15" << endl;
	    ofile << "# Fields: VPLUS1(I2C) VPLUS1(V) PLLUS1_ERROR(V)" << endl; 
	}
	else{
	    ofile << "# Title: VPLUS2 voltage values for VPLUS2 I2C setting 0 - 15" << endl;
	    ofile << "# Fields: VPLUS2(I2C) VPLUS2(V) PLLUS2_ERROR(V)" << endl; 
	}
	//cout << "vmeas.size()=" << vmeas.size() << endl;
	for(unsigned int vplus=0; vplus < 16; vplus++){
	    int i(vplus);
	    if(!inc) i = 15-vplus;
	    vplus_meas[vplus] = vmeas.at(i);
	    float err = _kmm->get_DC_err( vmeas.at(i) );
	    sprintf( tmp, "0x%02x %1.4f %1.4f\n", vplus, vmeas.at(i), err );
	    ofile << tmp;
	}
	ofile.close();

	return 0;
    }
    int WaferTest::cal_vcasc_scan( const string &test_name, const string &fname, map<unsigned,float> &cal_vcasc_meas, ostream &os, bool set_relayA, bool inc, int pause_n100us){

	char tmp[256];

	//amux setting
	unsigned amux_reg_addr = 15; 
	//amux setting 15, CAL_VCASC 
	unsigned reg_value(47);
	/*
	unsigned value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
	fscbuf_data.push_back(value);
	*/
	_cbc3be->rwCbcI2cReg( 1, 1, 1, amux_reg_addr, reg_value, _cbc_id );
	usleep(10000);
	
	if(set_relayA){
	    //relayA setting to amux 
	    _cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	    usleep( 2000 );
	}

	//keithley multimeter settings
	_kmm->set_DC_measurement("0.001", "1", 1, 256, true);
	_kmm->kmm_printf(":INIT");

	vector<uint32_t> fscbuf_data;
	unsigned reg_addr = 17; 
	int ntrig(0);
	for( unsigned i=0; i < 256; i++){
	    // i2c setting
	    unsigned v(i);
	    if(!inc)v = 255-v;
	    reg_value = 0x81100000 | (reg_addr << 8) | v;
	    fscbuf_data.push_back(reg_value);
	    fscbuf_data.push_back((unsigned) 0x22000000);

	    //triger kmm
	    int p(0);
	    while (p < pause_n100us){
		fscbuf_data.push_back((unsigned) 0xafa00000);
		p++;
	    }
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	_cbc3be->WriteFscBram(fscbuf_data);

	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(60);
	//    reset_cbc_i2c_reply_fifos(_hw,1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	fscbuf_data.clear();

	//cout << "ntrig = " << ntrig << endl;
	//keithley read
	_kmm->kmm_read_ndata(ntrig); 
	//cout << "kmm nread = " << nread << endl;
	vector<float> vmeas = _kmm->get_kmm_vdata();

	//writing the measurements to a file 
	ofstream ofile(fname.c_str());
	ofile << "# Title: CAL_VCASC voltage values for CAL_VCASC I2C setting 0 - 255" << endl;
	ofile << "# Fields: CAL_VCASC(I2C) CAL_VCASC(V) CAL_VCASC_ERROR(V)" << endl; 
	for(unsigned int v=0; v < 256; v++){
	    int i(v);
	    if(!inc) i = 255-v;
	    cal_vcasc_meas[v] = vmeas.at(i);
	    float err = _kmm->get_DC_err( vmeas.at(i) );
	    sprintf( tmp, "0x%02x %1.4f %1.4f\n", v, vmeas.at(i), err );
	    ofile << tmp;
	}
	ofile.close();

	return 0;
    }
    int WaferTest::curr_scan( const std::string &test_name, std::ostream &os, bool set_relayA, bool inc, int pause_n100us){

//	cout << "pause_n100us = " << pause_n100us << endl;

	char tmp[256];

	//amux setting : nothing is selected. 
	unsigned value = 0; 
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.amux", value, _cbc_id );


	//bias addresses
	map<string,unsigned> bias_addr_map;
	bias_addr_map["Ipre1"] = 0x03;
	bias_addr_map["Ipre2"] = 0x04;
	bias_addr_map["Ipsf"]  = 0x05;
	bias_addr_map["Ipa"]   = 0x06;
	bias_addr_map["Ipaos"] = 0x07;
	bias_addr_map["Icomp"] = 0x09;
	bias_addr_map["CAL_Ibias"] = 0x10;

	if(set_relayA){
	    //relayA setting to VLDOI 
	    _cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VLDOI_I );
	    usleep( 2000 );
	}
	//keithley multimeter settings
	//_kmm->set_DC_measurement("0.001", "0.1", 1, 1, false);
	_kmm->set_DC_measurement("0.2", "0.1", 1, 364, true);
	_kmm->kmm_printf(":INIT");

//	vector<float> vmeas;

	//sequencer data
	vector<uint32_t> fscbuf_data;
	int ntrig = 0;
	map<string,unsigned>::iterator it = bias_addr_map.begin();
	for(; it != bias_addr_map.end(); it++){

	    unsigned value0;
	    _cbc3be->rwCbcI2cRegsByName( 1, 0, it->first+".curr", value0, _cbc_id );
//	    cout << it->first << " original value = " << value0 << endl; 
	    value = 0;
	    if(!inc) value = 255;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, it->first+".curr", value, _cbc_id );

	    for(unsigned i=0; i < 256; i+=5){
		unsigned v(i);
		if(!inc)v = 255-v;
		value = 0x81100000 | (it->second << 8) | v;
		fscbuf_data.push_back(value);
		fscbuf_data.push_back((unsigned) 0x22000000);
		int p(0);
		while (p < pause_n100us){
		    fscbuf_data.push_back((unsigned) 0xafa00000);
		    p++;
		}
		/*
		if(i==0){
		    int pp(0);
		    while (pp < 100){
			fscbuf_data.push_back((unsigned) 0xafa00000);
			pp++;
		    }
		}
		*/
		//triger kmm
		fscbuf_data.push_back((unsigned) 0x00000001);
		fscbuf_data.push_back((unsigned) 0x28000000);
		ntrig++;
	    }
	    fscbuf_data.push_back((unsigned) 0xe0000000);
	    _cbc3be->WriteFscBram(fscbuf_data);

	    if(_debug) dump_seq_data(fscbuf_data,_os);
	    _cbc3be->StartFscBramController();
	    _cbc3be->WaitSequencerReady(60);
	    check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	    fscbuf_data.clear();
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, it->first+".curr", value0, _cbc_id );
	}
	if(_debug)
	    cout << "ntrig = " << ntrig << endl;
	//keithley read
	int nread = _kmm->kmm_read_ndata(ntrig); 
	if(_debug)
	    cout << "kmm nread = " << nread << endl;
	vector<float> vmeas = _kmm->get_kmm_vdata();

	float Rres(0.2);
	//writing the measurements to a file 
	int i0(0);
	int nth_bias(0);
	it = bias_addr_map.begin();
	for(; it != bias_addr_map.end(); it++){

	    string type = it->first;

	    sprintf(tmp, "frame29_%s_scan_pause_%1.2fms.txt", type.c_str(), 0.1 * pause_n100us);
	    string ofname(tmp);
	    ofstream ofile(ofname.c_str());

	    sprintf(tmp, "# Title: Current values for %s I2C setting 0 - 255 in step 5\n", type.c_str());
	    ofile << tmp; 
	    sprintf(tmp, "# Fields: %s(I2C) %s(mA) %s(mA)\n", type.c_str(), type.c_str(), type.c_str() );
	    ofile << tmp; 
	    //cout << "vmeas.size()=" << vmeas.size() << endl;
	    for(unsigned int v=0; v < 256; v+=5){
		int i(v/5);
		if(!inc) i = (255-v)/5;
		i += i0;
		float err = _kmm->get_DC_err( vmeas.at(i) ) / Rres * 1000;
		sprintf( tmp, "0x%02x %1.4f %1.4f\n", v, vmeas.at(i) / Rres *1000, err );
		ofile << tmp;
		if(v == 255) i0 = 52 * ++nth_bias;
	    }
	    ofile.close();
//	    cout << ofname << " is closed." << endl;
	    sprintf( tmp, "%s:%s_scan_file_pause_%1.2fms", test_name.c_str(), type.c_str(), pause_n100us * 0.1); 
	    _parameters.set(tmp, ofname);
	}

	return 0;
    }
    void WaferTest::read_raw_data( const std::string &test_name, unsigned N, uint32_t *rawdata, int err_code ){

	    int read_nwords = _cbc3be->ReadRawData( rawdata, N );
	    if(read_nwords == -1 || read_nwords != (int)N) 
		throw WaferTestException( test_name + "Raw data buffer problem.", err_code | WT_ERR_DAQ );
    }
    void WaferTest::read_data( const std::string &test_name, unsigned nwords, uint32_t *buffer, int err_code ){

	int read_nwords(0); 
	_cbc3be->MinNumWordToWait(nwords);
	if( _cbc3be->WaitData( 1, false ) ){
	    read_nwords = _cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    stringstream oss;
	    oss << nwords << " words requested and " << read_nwords << " read.";
	    throw WaferTestException( test_name + " Data read failed." + oss.str(), err_code | WT_ERR_DAQ | WT_ERR_OTHER ); 
	}

    }

    int WaferTest::check_pipeline( const std::string &test_name, int hit_type){

	char tmp[256];
	stringstream oss;

	int result(0);

	unsigned value(20);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "FcCntrlCompBetaTrigLat.trig_lat", value, _cbc_id );

	vector<uint32_t> fscbuf_data;
	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
//	fscbuf_data.push_back((unsigned) 0xa0e00000);
	fscbuf_data.push_back((unsigned) 0xa1ff0000);
	fscbuf_data.push_back((unsigned) 0xa0010000);
	fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
	fscbuf_data.push_back((unsigned) 0xa0160000);
//	fscbuf_data.push_back((unsigned) 0xa0010200);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	fscbuf_data.push_back((unsigned) 0xe0000000);

	int npipe[512];
	for(unsigned i=0; i < 512; i++) npipe[i]=0;
	int pa[512];
	int ncbcdata(0);
	unsigned sum_err_bits(0);
	bool hits_ok(true);

	unsigned nwords = 14 * 512;
	uint32_t *buffer = new uint32_t[nwords];
	BeDataDecoder be_data_decoder;

	_cbc3be->InitializeRun();

	for(unsigned i=0; i < 512; i++){
	    fscbuf_data[3] = (((i + 1) << 16) | 0xa0000000 );//1 clock delay 
	    if(hit_type==2)
		fscbuf_data[4] = 0xa0010010;//test pulse request
	    _cbc3be->WriteFscBram(fscbuf_data);
	    _cbc3be->StartFscBramController();
//	    fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 
	}

	_cbc3be->MinNumWordToWait(nwords);
	int read_nwords(0);
	if( _cbc3be->WaitData( 1, false ) ){
	    read_nwords = _cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << " Data read failed. " << read_nwords << " words read.";
	    throw WaferTestException( oss.str(), WT_ERR_DAQ | WT_ERR_OTHER); 
	}
	be_data_decoder.SetData( buffer, nwords * 4 );
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){

		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		if(_debug)cbc_dpd->DumpData(_os);
		sum_err_bits += cbc_dpd->Error(0);
		sum_err_bits += cbc_dpd->Error(1);
		int paddr = cbc_dpd->PipelineAddress();
		if(paddr >=0 && paddr <= 511) npipe[paddr]++; 
		pa[ncbcdata] = paddr;
		if( (hit_type == 1 && cbc_dpd->HitChannels().size() != 254) || (hit_type == 0 && cbc_dpd->HitChannels().size() != 0)
		 || (hit_type == 2 && cbc_dpd->HitChannels().size() != 32) ){
		    hits_ok = false;
		    cbc_dpd->DumpData(_os);
		    if( hit_type == 1 ){
			sprintf( tmp, "Pipeline[%3d] 254 hits are expected but some hits are missing.", paddr);
		       	print_test(test_name, tmp, _os);
		    }
		    else if(hit_type == 2 ){
			sprintf( tmp, "Pipeline[%3d] 32 hits are expected but some hits are missing.", paddr);
		       	print_test(test_name, tmp, _os);
		    }
		    else {
			sprintf( tmp, "Pipeline[%3d] 0 hits is expected but some hits are found.", paddr);
		       	print_test(test_name, tmp, _os);
		    }
		}
		ncbcdata++;
	    }
	}
	if(_debug){
	    unsigned wpl = 4;
	    _os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
	    for(unsigned ii=0; ii < nwords; ii++){
		_os << setbase(16) << setfill('0') << setw(8) << buffer[ii] << ' ';
		//			if( i == 0 ) count = buffer[i];
		if( ii % wpl == wpl - 1 ){
		    _os << endl;
		    if( ii != nwords-1) 
			_os << setbase(16) << setfill('0') << setw(8) << ii+1 << ' ';
		}
	    }
	}
	delete buffer;
	if(ncbcdata != 512){
	    sprintf(tmp, " # of CBC DATA is not 512, but %d", ncbcdata); 
	    throw WaferTestException( test_name + tmp, WT_ERR_DAQ | WT_ERR_OTHER );
	}
	if(sum_err_bits != 0){
	    sprintf(tmp, " %d error bits are set.", sum_err_bits); 
	    print_test( test_name, tmp, _os );
	    result = WT_ERR_PIPELINE; 
	}
	if(!hits_ok) result = WT_ERR_PIPELINE;

	oss.str("");
	oss << "Pipeline data.";
	for(unsigned i=0; i < 512; i++){
	    if(i%32==0){
		print_test( test_name, oss.str(), _os );
		int eevt = i + 31;
		oss.str("");
		oss << "evt[" << std::setw(3) << std::setfill('0') << std::dec << i << "-" << std::setw(3) << std::setfill('0') << eevt << "]";
	    }
	    oss << std::setw(3) << std::setfill('0') << std::hex << pa[i] << " ";
	}
	oss << endl;
	print_test(test_name, oss.str(), _os);

	bool pipe_ok(true);
	for(unsigned i=0; i < 512; i++){
	    if(npipe[i] != 1) {
		pipe_ok = false;
	    }
	}

	if(!pipe_ok){
	    oss.str("");
	    oss << "Pipeline stats.";
	    for(unsigned i=0; i < 512; i++){
		if(i%32==0){
		    print_test( test_name, oss.str(), _os );
		    int eaddr = i + 31;
		    oss.str("");
		    oss << "addr[" << std::setw(3) << std::setfill('0') << std::hex << i << "-" << std::setw(3) << std::setfill('0') << eaddr << "]";
		}
		oss << npipe[i] << " ";
	    }
	    oss << endl;
	    print_test(test_name, oss.str(), _os);

	    result = WT_ERR_PIPELINE; 
	}
	return result;
    }
    int WaferTest::check_buffer_err_hit( const std::string &test_name, bool all_hit){

	char tmp[256];
	stringstream oss;

	int result(0);

	vector<uint32_t> fscbuf_data;
	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
	fscbuf_data.push_back((unsigned) 0xa0df0000);
	fscbuf_data.push_back((unsigned) 0xa0010200);
	fscbuf_data.push_back((unsigned) 0xa0200100);// 32 consecutive triggers
	fscbuf_data.push_back((unsigned) 0xe0000000);
	_cbc3be->WriteFscBram(fscbuf_data);

	int N = 2600;
	int n = N/2;
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

	bool hit[32][254];
	for(unsigned ch=0; ch < 254; ch++){
	    for(unsigned bi=0; bi<32; bi++){
		hit[bi][ch]=false;
	    }
	}

	_cbc3be->InitializeRun();
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1000);

	uint32_t *rawdata = new uint32_t[N];
	int read_nwords = _cbc3be->ReadRawData( rawdata, N );
	if(read_nwords == -1 || read_nwords != (int)N) 
	    throw WaferTestException( test_name + "Raw data buffer problem.", WT_ERR_RAW_DATA | WT_ERR_CBC );
	if(_debug){
	    raw_data_dump_data(rawdata, N, _os);
	}
	int *slvs6 = new int[n];
	raw_data_get_slvs6(rawdata, slvs6, n);

	int ne1(0);
	int ne2(0);
	bool e1=false;
	bool e2=false;
	bool hit_failed(false);

	int hp=0;
	int ofst_incr=0;
	for(unsigned bi=0; bi<32; bi++){
	    if(bi!=0) ofst_incr = 35;
	    hp = raw_data_find_frame_header(slvs6, n, hp + ofst_incr);
	    if(hp<0) {
		raw_data_dump_data(rawdata,N, _os);
		delete rawdata;
		delete slvs6;
		oss.str("");
		oss << "header_error at Buffer[" << bi << "]";
		throw WaferTestException( oss.str(), WT_ERR_RAW_DATA | WT_ERR_CBC );
		break;
	    }
	    raw_data_get_error_bits(slvs6, hp, e1, e2);
	    ne1+=e1;
	    ne2+=e2;
	    for(int ch=1; ch < 255; ch++){
		bool the_hit = raw_data_has_hit(slvs6,hp,ch);
		hit[bi][ch-1]=the_hit;
		if( (all_hit && !the_hit) || (!all_hit && the_hit)){
		    cout << "failed at buffer, channel " << bi << " " << ch << endl;
		    hit_failed = true;
		}
	    }
	}
	delete rawdata;
	delete slvs6;

	if( !(ne1 == 0 && ne2 == 0)){
	    sprintf(tmp, "Some error bits are set. (ne1, ne2)=(%d, %d)", ne1, ne2); 
	    print_test(test_name, tmp, _os);
	    result = WT_ERR_BUFRAM; 
	}
	if(hit_failed){
	    print_buff_hit(hit,_os);
	    if( all_hit ) print_test(test_name, "Some hits are missing.", _os);
	    else print_test(test_name, "Some hits are found.", _os);
	    result = WT_ERR_BUFRAM; 
	}
	return result;
    }
    int WaferTest::check_buffer_pa( const std::string &test_name){

	stringstream oss;

	int result(0);

	unsigned value(20);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "FcCntrlCompBetaTrigLat.trig_lat", value, _cbc_id );

	_cbc3be->SendFastSignal( "orbit_reset" ); 
	_cbc3be->SendFastSignal( "reset" ); 

	//pipeline address data
	int paddr_values[32][512];
	for(unsigned bi=0; bi<32; bi++){
	    for(unsigned i=0; i<512; i++){
		paddr_values[bi][i] = -1;
	    }
	}
	//sequencer data
	vector<uint32_t> fscbuf_data;
	//tlat = 0x14
	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
	fscbuf_data.push_back((unsigned) 0xa0200100);// 32 consecutive triggers 
	fscbuf_data.push_back((unsigned) 0xa0100000);
	fscbuf_data.push_back((unsigned) 0xe0000000);

	_cbc3be->InitializeRun();

	unsigned nwords = 14*32*256;
	_cbc3be->MinNumWordToWait(nwords);
	uint32_t *buffer = new uint32_t[nwords];
	BeDataDecoder be_data_decoder;

	bool failed(false);
	for(unsigned j=0; j<2;j++){
	    for(unsigned i=0; i<256; i++){

		unsigned pause = 0x16+i+j*256;
		unsigned pause_com = (pause<<16) | 0xa0000000;
		fscbuf_data[2] = pause_com;

		if(_debug)dump_seq_data(fscbuf_data,_os);

		_cbc3be->WriteFscBram(fscbuf_data);
		_cbc3be->StartFscBramController();
		_cbc3be->WaitSequencerReady(1000);

	    }
	    int read_nwords(0);
	    if( _cbc3be->WaitData( 1, false ) ){
		read_nwords = _cbc3be->ReadData( buffer );
	    }
	    if( read_nwords != (int)nwords ){
		oss.str("");
		oss << " Data read failed. " << read_nwords << " words read.";
		throw WaferTestException( oss.str(), WT_ERR_DAQ | WT_ERR_OTHER); 
	    }
	    be_data_decoder.SetData( buffer, nwords * 4 );
	    int cd_i(0);
	    while( be_data_decoder.SetNextEvent() ){ 

		while( be_data_decoder.SetNextCbc() ){
		    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		    if(_debug)cbc_dpd->DumpData(_os);
		    int paddr = cbc_dpd->PipelineAddress();
		    int bi = cd_i % 32;         //id of the buffer location
		    int i = cd_i / 32 + j * 256;//pipeline incrementation at the buffer location.
		    int r_paddr = (i + bi)%512; //pipeline address at the buffer location
		    paddr_values[bi][i] = paddr;
		    if(paddr != r_paddr) {
			oss.str("");
			oss << "Pipeline Address is " << paddr << ", but " << r_paddr << " is expected.";
			print_test( test_name, oss.str(), _os );
			failed = true; 
		    }
		    cd_i++;
		}
	    }
	}
	delete buffer;

	if(failed){
	    print_buff_pipeline(paddr_values, _os);
	    result = WT_ERR_BUFRAM;
	}

	for(unsigned j=0; j<2;j++){
	    for(unsigned i=0; i<256; i++){
		oss.str("");
		oss << "Trigger train [" << std::setw(3) << std::setfill('0') << std::dec << i + j * 256 << "] ";
		for(unsigned bi=0; bi<32; bi++){
		    oss << std::setw(3) << std::setfill('0') << std::hex << paddr_values[bi][i+j*256] << " ";
		}
		print_test( test_name, oss.str(), _os );
	    }
	}

	if(_debug){

	    int addr(0);
	    for(unsigned j=0; j<2;j++){
		for(unsigned i=0; i<256; i++){
		    addr = i + j * 256;
		    for(unsigned bi=0; bi<32; bi++){
			if(addr == 512) addr = 0;
			if(paddr_values[bi][i+j*256] != addr ) {
			    oss.str("");
			    oss << "Buffer[" << std::dec << std::setw(2) << std::setfill('0') << bi << "] iteration " 
				<< i << "+" << j << "*256 should have address " << addr << ", but " << paddr_values[bi][i+j*256] << " was found.";
			    print_test( test_name, oss.str(), _os );
			}
			addr ++;
		    }
		}
	    }
	}
	return result;
    }
    int WaferTest::check_buffer_l1c( const std::string &test_name){

	ostringstream oss;

	int result(0);
	unsigned nwords = 14*32*16;
	uint32_t *buffer = new uint32_t[nwords];
	BeDataDecoder be_data_decoder;

	bool l1ac_ok(true);
	int l1a_count[32][512];
	for(unsigned bi=0; bi<32; bi++){
	    for(unsigned i=0; i<512; i++){
		l1a_count[bi][i] = -1;
	    }
	}
	vector<uint32_t> fscbuf_data;
	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
	fscbuf_data.push_back((unsigned) 0xa0200100);// 32 consecutive triggers 
	fscbuf_data.push_back((unsigned) 0xa5000000);
	fscbuf_data.push_back((unsigned) 0xe0000000);

	_cbc3be->InitializeRun();
	_cbc3be->SendFastSignal( "orbit_reset" ); 
	/*
	_cbc3be->SendFastSignal( "orbit_reset" ); 
	_cbc3be->SendFastSignal( "orbit_reset" );  //added at 15:18 07/09/2018
	*/
//	_cbc3be->SendFastSignal( "reset" ); 

	int cd_i(0);
	int l1ac0(0);
	bool or_ok(true);
	for(unsigned j=0; j<32;j++){
	    // 32 consecutive triggers are sent for 16 times for a single read out from BE.
	    for(unsigned i=0; i<16; i++){

		_cbc3be->WriteFscBram(fscbuf_data);
		_cbc3be->StartFscBramController();
	    }
	    int read_nwords(0);
	    _cbc3be->MinNumWordToWait(nwords);
	    if( _cbc3be->WaitData( 1, false ) ){
		read_nwords = _cbc3be->ReadData( buffer );
	    }
	    if( read_nwords != (int)nwords ){
		oss.str("");
		oss << std::dec << nwords << " words expected to read but " << read_nwords << " read.";
		throw WaferTestException( " Data read failed." + oss.str(), WT_ERR_DAQ | WT_ERR_OTHER ); 
	    }

	    be_data_decoder.SetData( buffer, nwords * 4 );
	    while( be_data_decoder.SetNextEvent() ){ 

		while( be_data_decoder.SetNextCbc() ){
		    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		    int l1ac = cbc_dpd->L1ACounter();
		    //		cout << "CBC data " << std::dec << cd_i << " counter " << l1ac << endl;
		    int bi = cd_i % 32;
		    int ci = cd_i / 32;
		    if(cd_i == 0){
		      if( l1ac != 1) or_ok = false; 
		      l1ac0 = l1ac;
		    }
		    if( l1ac != (l1ac0++ +(int)j)%512 ) l1ac_ok = false; 
		    cd_i++;
		    l1a_count[bi][ci] = l1ac;
		}
	    }
	    _cbc3be->SendFastSignal( "trigger" ); 
	    _cbc3be->MinNumWordToWait(14);
	    if( _cbc3be->WaitData( 1, false )){
		read_nwords = _cbc3be->ReadData( buffer );
	    }
	    if( read_nwords != 14 ){
		oss.str("");
		oss << std::dec << nwords << " words expected to read but " << read_nwords << " read.";
		throw WaferTestException( " Data read failed." + oss.str(), WT_ERR_DAQ | WT_ERR_OTHER); 
	    }

	    //_cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
	}
	delete buffer;
	if(_debug){
	    print_buff_l1a_count(l1a_count, _os);
	}
	if(!l1ac_ok || !or_ok ){
	    print_test(_current_test, "L1A counter is not OK.", _os);
	    print_buff_l1a_count(l1a_count, _os);
	    if(!l1ac_ok) result |= WT_ERR_BUFRAM;
	    if(!or_ok) result |= WT_ERR_ORBIT_RESET;
	}

	return result;
    }
    //for cwo_i != -1, only ibend = 0 is suported.
    int WaferTest::generate_stubs( const std::string &test_name, int ibend, map<CHANNEL_ID, unsigned> &msks, int max_cw, int pt_width, int nss, int nsw, int ls, 
	    vector<int> &addr0, vector<int> &bend0, vector<int> &v_max_cw, vector<int> &v_nss, vector<int> &v_nsw, vector<int> &v_ibend, int cwo_i, unsigned *ibnd )
    {
	int ni0(43);
	int nm(3);

	char tmp[256];
	if(_debug){
	    sprintf(tmp, "(nss, nsw, ls, max_cw)=(%d, %d, %d, %d)", nss, nsw, ls, max_cw);
	    _os << tmp << endl;
	}

	vector<uint32_t> fscbuf_data;
	int nevt(0);
	int nch(0);

	bool bend_half = (nss+nsw)%2;

	//seed loop
	//stub index i = i0 + ni0 * im
	//where im = stub index for each event (0 to 2)
	for(int i0 = 0; i0 < ni0; i0++){

	    if(_debug) _os << "i0, ibend = " << std::dec << i0 << ", " << ibend << endl;

	    //single event check 

	    //making 3 stubs at most with masking channels.

	    //create unmasked channel maps
	    //initialization of the channel maps from the previous test.
	    //unmasked channels are masked. already masked channels are erased.
	    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	    while(it_msks != msks.end()){
		//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
		if(it_msks->second != 0) {
		    it_msks->second = 0;
		    it_msks++;
		}
		else {
		    msks.erase(it_msks++); 
		}
	    }
	    for( int im = 0; im < nm; im++){

		//get channels for the stub index (i,j)
		int i = i0 + ni0 * im;

		unsigned saddr = get_seed_address(i,nss);
		if(saddr < 2 || saddr > 255) continue;
		if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls, _CBC3_0);
		if(!chs.size()) continue;

		//update the masks and registers
		for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		    msks[chs.at(ch_i)] = 1;
		}

		int ibend_cwo = ibend;
		if(cwo_i != -1){
		    ibend_cwo = ibnd[cwo_i];
		}
		unsigned sbend = ibend_cwo & 0xf; 

		//calculate addr0 -- the created hits may not pass the cluster width condition.
		if(nss <= max_cw && nsw <= max_cw){

		    if(_CBC3_0){

			if(!(bend_half && (ibend_cwo == 7 || ibend_cwo == -8) ) ){//2 stubs at the edge of bend limits are not created in CBC3. 

			    //unsigned saddr = get_seed_address(i,nss);
			    //unsigned sbend = ibend & 0xf; 
			    if( pt_width != 0 ){
				if(bend_half && (ibend_cwo >= pt_width/2 + pt_width%2 || ibend_cwo <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
				if(!bend_half && abs(ibend_cwo) >= pt_width/2 + 1 ) sbend = 8;
			    }

			    addr0.push_back(saddr);
			    bend0.push_back(sbend);

			    v_max_cw.push_back(max_cw);
			    v_nss.push_back(nss);
			    v_nsw.push_back(nsw);
			    v_ibend.push_back(ibend_cwo);

			    if(_debug){

				_os << " stub (im,addr,bend)=(" << std::dec << im << ", " << saddr << "," << sbend << ") ch[";

				for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
				    _os << chs.at(ch_i) << ",";
				}
				_os << "] " << endl;
			    }
			}
		    }
		    else{
			//unsigned saddr = get_seed_address(i,nss);
			//unsigned sbend = ibend & 0xf; 
			if( pt_width != 0 ){
			    if(bend_half && (ibend_cwo >= pt_width/2 + pt_width%2 || ibend_cwo <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
			    if(!bend_half && abs(ibend_cwo) >= pt_width/2 + 1 ) sbend = 8;
			}
			if(sbend != 8){

			    addr0.push_back(saddr);
			    bend0.push_back(sbend);
			    v_max_cw.push_back(max_cw);
			    v_nss.push_back(nss);
			    v_nsw.push_back(nsw);
			    v_ibend.push_back(ibend_cwo);
			}
			if(_debug){
			    _os << " stub (im,addr,bend)=(" << std::dec << im << ", " << saddr << "," << sbend << ") ch[";
			    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
				_os << chs.at(ch_i) << ",";
			    }
			    _os << "] " << endl;
			}
		    }
		}
		nch += chs.size();
	    }
	    if(_debug) _os << "Total # of stubs = " << addr0.size() << endl;

	    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

	    //test position
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
	    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	    fscbuf_data.push_back((unsigned) 0xa0bb0000);
	    fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	    fscbuf_data.push_back((unsigned) 0xa0100000);
	    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	    nevt++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	if(_debug) _os << "size of seq data = " << fscbuf_data.size() << endl;

	if(_debug>=2) dump_seq_data(fscbuf_data, _os);

	_cbc3be->WriteFscBram(fscbuf_data);
	//cbc3be->InitializeRun();
	_cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
	//cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
	/*
	   system( "cbc3daq 'ipbus read node=cs_stat.*cbc1.*'");
	   system( "cbc3daq 'ipbus read node=cs_stat.*eb.*'");
	   system( "cbc3daq 'ipbus read node=cs_stat.*db.*'");
	   */
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	sprintf(tmp, "cluster width <= %d, pt width <= %d, ibend = %d, n seed strips = %d, n window strips = %d,  %d channels are unmasked.", max_cw, pt_width, ibend, nss, nsw, nch );
	print_test( test_name, tmp, _os );

	return nevt;
    }
    int WaferTest::generate_stubs( const std::string &test_name, int nss, int nsw, int ibend, int ls, int max_cw, int pt_width, bool l1_supp, bool stub_supp, map<CHANNEL_ID, unsigned> &msks, 
	    vector<vector<int> > &addr0, vector<vector<int> > &bend0, vector<set<int> > &ch0 ) 
    {
	int ni0(43);
	int nm(3);

	char tmp[256];
	if(_debug){
	    sprintf(tmp, "(nss, nsw, ibend, ls, max_cw, pt_width)=(%d, %d, %d, %d, %d, %d)", nss, nsw, ibend, ls, max_cw, pt_width);
	    _os << tmp << endl;
	}

	vector<uint32_t> fscbuf_data;
	int nevt(0);

	bool bend_half = (nss+nsw)%2;


	//seed loop
	for(int i0 = 0; i0 < ni0; i0++){

	    if(i0 > 2) break; 

	    vector<int> evt_addr0;
	    vector<int> evt_bend0;
	    set<int> evt_ch0;

	    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	    while(it_msks != msks.end()){
		//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
		if(it_msks->second != 0) {
		    it_msks->second = 0;
		    it_msks++;
		}
		else {
		    msks.erase(it_msks++); 
		}
	    }
	    for( int im = 0; im < nm; im++){
		//get channels for the stub index (i,j)
		int i = i0 + ni0 * im;

		unsigned saddr = get_seed_address(i,nss);
		if(saddr < 2 || saddr > 255) continue;
		if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		//if(saddr < 2 || saddr > 255) continue;
		vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls, _CBC3_0);
		if(!chs.size()) continue;
		if(!l1_supp) evt_ch0.insert(chs.begin(), chs.end());

		//update the masks
		for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		    msks[chs.at(ch_i)] = 1;
		}
		if(!stub_supp){
		    //the created hits may not pass the cluster width condition.
		    if(nss <= max_cw && nsw <= max_cw){
			if(!(bend_half && (ibend == 7 || ibend == -8) ) ){//2 stubs at the edge of bend limits are not created in CBC3. 

			    unsigned saddr = get_seed_address(i,nss);
			    unsigned sbend = ibend & 0xf; 
			    if( pt_width != 0 ){
				if(bend_half && (ibend >= pt_width/2 + pt_width%2 || ibend <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
				if(!bend_half && abs(ibend) >= pt_width/2 + 1 ) sbend = 8;
			    }
			    evt_addr0.push_back(saddr);
			    evt_bend0.push_back(sbend);
			}
		    }
		}
	    }
	    addr0.push_back(evt_addr0);
	    bend0.push_back(evt_bend0);
	    ch0.push_back(evt_ch0);

	    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
	    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	    fscbuf_data.push_back((unsigned) 0xa0bb0000);
	    fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	    fscbuf_data.push_back((unsigned) 0xa0100000);
	    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	    nevt++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	if(_debug) _os << "size of seq data = " << fscbuf_data.size() << endl;

	if(_debug>=2) dump_seq_data(fscbuf_data, _os);

	_cbc3be->WriteFscBram(fscbuf_data);
	_cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);

	return nevt;
    }
    int WaferTest::read_data_and_check_stubs(const string &test_name, int err_code, int nevt, 
	    vector<int> &addr0, vector<int> &bend0, vector<int> &v_max_cw, vector<int> &v_nss, vector<int> &v_nsw, vector<int> &v_ibend,
	    TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend)
    {
	stringstream oss;
	char tmp[256];

	bool stub_ok(true);


	if(_debug >= 2){
	    unsigned N = _cbc3be->ReadBeBoardRegister("cs_cnfg.rdb.write_block_size"); 
	    uint32_t *rawdata(0);
	    rawdata = new uint32_t[N];
	    _cbc3be->ReadRawData( rawdata, N, true );
	    raw_data_dump_data(rawdata, N, _os);
	    delete rawdata;
	}

	unsigned nwords = 14 * nevt;
	if(_debug) _os << "nwords = " << nwords << endl;

	uint32_t *buffer = new uint32_t[nwords];
	_cbc3be->MinNumWordToWait(nwords);
	int read_nwords(0);
	if( _cbc3be->WaitData( 1, false ) ){
	    read_nwords = _cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << nwords << " words requested and " << read_nwords << " read.";
	    throw WaferTestException( "Data read failed." + oss.str(), WT_ERR_DAQ | err_code ); 
	}

	BeDataDecoder be_data_decoder;
	be_data_decoder.SetData( buffer, nwords * 4 );

	int n_stub(0);
	unsigned stub_i(0);

	while( be_data_decoder.SetNextEvent() ){ 

	    if(addr0.size() != 0 && stub_i >= addr0.size()) {
		stub_ok = false;
		break;
	    }

	    while( be_data_decoder.SetNextCbc() ){

		if(addr0.size() != 0 && stub_i >= addr0.size()) {
		    stub_ok = false;
		    break;
		}
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		if(_debug)cbc_dpd->DumpData(_os);
		vector<unsigned> sp = cbc_dpd->StubPositions();
		vector<unsigned> sb = cbc_dpd->StubBends();

		for(unsigned s_i=0; s_i < 3; s_i++){

		    if(sp.at(s_i) == 0) continue;
		    n_stub++;
		    if(addr0.size() == 0){
			stub_ok = false;
			_os << "stub[" << s_i << "] found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			    << " for stubs should be rejected." << endl;
			break;
		    }
		    if( stub_i >= addr0.size() ){
			stub_ok = false;
			break;
		    }

		    h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
		    h_addr->Fill( addr0.at(stub_i), sp.at(s_i));

		    // stub address on CBC3 is not listed as bad.
		    bool this_addr_ok(true);

		    //if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		    //check the stub. fill bend hist if address is o.k.
		    if((int)sp.at(s_i) != addr0.at(stub_i)){
			stub_ok = false;
			this_addr_ok = false;
			sprintf( tmp,  "(nss, nsw, ibend, max_cw)=(%d, %d, %d, %d) ", v_nss.at(stub_i), v_nsw.at(stub_i), v_ibend.at(stub_i), v_max_cw.at(stub_i));  
			_os << tmp << "stub[" << s_i << "] found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			    << " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		    }
		    if((int)sb.at(s_i) != bend0.at(stub_i)){
			stub_ok = false;
			if( this_addr_ok == false ) _os << "\t";
			sprintf( tmp,  "(nss, nsw, ibend, max_cw)=(%d, %d, %d, %d) ", v_nss.at(stub_i), v_nsw.at(stub_i), v_ibend.at(stub_i), v_max_cw.at(stub_i));  
			_os << tmp << "stub[" << s_i << "] found with a wrong bend addr=0x" 
			    << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			    << " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		    }
		    if(this_addr_ok)h_bend->Fill( bend0.at(stub_i), sb.at(s_i) );
		    stub_i++;
		}
	    }
	}
	sprintf( tmp, "Total # of stubs created = %d", n_stub );
	print_test( test_name, tmp, _os );
	if(n_stub != (int)addr0.size()) stub_ok = false;

	delete buffer;
	int result = stub_ok ? 0 : err_code; 
	return result; 
    }
    int WaferTest::read_data_and_check_stubs_and_hits( const string &test_name, int err_code,  
	    bool stop_at_fail, bool stub_addr_check, bool stub_bend_check, bool hit_check, 
	    int nevt, 
	    vector<vector<int> > &addr0, vector<vector<int> > &bend0, vector<set<int> > &ch0,
	    TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend )
    {
	int result(0);
	bool stub_ok(true);
	bool hit_ok(true);

	char tmp[256];
	stringstream oss;

	if(_debug >= 2){
	    unsigned N = _cbc3be->ReadBeBoardRegister("cs_cnfg.rdb.write_block_size"); 
	    uint32_t *rawdata(0);
	    rawdata = new uint32_t[N];
	    read_raw_data(test_name, N, rawdata, err_code);
	    /*
	    int read_nwords = _cbc3be->ReadRawData( rawdata, N );
	    if(read_nwords == -1 || read_nwords != (int)N) 
		throw WaferTestException( test_name + "Raw data buffer problem.", err_code | WT_ERR_DAQ );
		*/
	    if(_debug){
		raw_data_dump_data(rawdata, N, _os);
	    }
	    delete rawdata;
	}

	unsigned nwords = 14 * nevt;
	uint32_t *buffer = new uint32_t[nwords];
	read_data(test_name, nwords, buffer, err_code);
	/*
	int read_nwords(0); 
	_cbc3be->MinNumWordToWait(nwords);
	if( _cbc3be->WaitData( 1, false ) ){
	    read_nwords = _cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << nwords << " words requested and " << read_nwords << " read.";
	    throw WaferTestException( "Data read failed." + oss.str(), err_code | WT_ERR_DAQ ); 
	}
	*/

	BeDataDecoder be_data_decoder;
	be_data_decoder.SetData( buffer, nwords * 4 );

	int total_hits(0);
	int stub_i(0);
	int evt_i(0);
	while( be_data_decoder.SetNextEvent() ){ 

	    if(_debug) _os << "Event[" << evt_i << "]" << endl;

	    vector<int> evt_addr0 = addr0.at(evt_i);
	    vector<int> evt_bend0 = bend0.at(evt_i);
	    set<int> evt_ch0 = ch0.at(evt_i);
	    int evt_hit_i(0);
	    while( be_data_decoder.SetNextCbc() ){

		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		if(_debug)cbc_dpd->DumpData(_os);

		if(hit_check){
		    vector<unsigned> hits = cbc_dpd->HitChannels(); 
		    evt_hit_i = hits.size();
		    if(hits.size() != evt_ch0.size() ){
			hit_ok = false;
			sprintf(tmp, "EVT[%04d] Unexpected # of hits created. %d hits are created where %d hits are expected.", evt_i, (int)hits.size(), (int)evt_ch0.size() );
			cbc_dpd->DumpData(_os);
			if(stop_at_fail)
			    throw WaferTestException( test_name + " " + tmp, err_code ); 
			else
			    print_test( test_name, tmp, _os );
		    }
		    else{
			for(unsigned h_i=0; h_i < hits.size(); h_i++){
			    if(evt_ch0.find(hits.at(h_i))==evt_ch0.end()){
				hit_ok = false;
				sprintf(tmp, "EVT[%04d] Unexpected hit is found at channel %d.", evt_i, hits.at(h_i) );
				cbc_dpd->DumpData(_os);
				if(stop_at_fail)
				    throw WaferTestException( test_name + " " + tmp, err_code ); 
				else
				    print_test( test_name, tmp, _os );
			    }
			}
		    }
		}
		if( !stub_addr_check && !stub_bend_check ) continue;

		//if(debug) os << "stub check" << endl;

		vector<unsigned> sp = cbc_dpd->StubPositions();
		vector<unsigned> sb = cbc_dpd->StubBends();

		for(unsigned s_i=0; s_i < 3; s_i++){
		    //os << "stub[" << s_i << "]" << endl;
		    if(sp.at(s_i) == 0){
			if(evt_addr0.size() > s_i){
			    stub_ok = false;
			    sprintf(tmp, "EVT[%04d] Stub at addr=0x%02x bend=0x%02x is missing.", evt_i, evt_addr0.at(s_i), evt_bend0.at(s_i) );
			    cbc_dpd->DumpData(_os);
			    if(stop_at_fail)
				throw WaferTestException( test_name + " " + tmp, err_code ); 
			    else
				print_test( test_name, tmp, _os );
			}
		    }
		    else{
			stub_i++;
			if( s_i+1 > evt_addr0.size() ){
			    stub_ok = false;
			    sprintf(tmp, "EVT[%04d] Unexpected stub is found at addr=0x%02x bend=0x%02x.", evt_i, sp.at(s_i), sb.at(s_i) );
			    cbc_dpd->DumpData(_os);
			    if(stop_at_fail)
				throw WaferTestException( test_name + " " + tmp, err_code ); 
			    else
				print_test( test_name, tmp, _os );
			    continue;
			}
			if(h_addr_bend) h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
			if(h_addr) h_addr->Fill( evt_addr0.at(s_i), sp.at(s_i));

			if(wt::waddrs.find(evt_addr0.at(s_i)) != wt::waddrs.end() ) continue;

			//check the stub. fill bend hist if address is o.k.
			bool this_addr_ok(true);
			if( stub_addr_check && (int)sp.at(s_i) != evt_addr0.at(s_i)){
			    stub_ok = false;
			    this_addr_ok = false;
			    sprintf(tmp, "EVT[%04d] Stub is found at a wrong place addr=0x%02x bend=0x%02x for addr=0x%02x bend=0x%02x.", 
				    evt_i, sp.at(s_i), sb.at(s_i), evt_addr0.at(s_i), evt_bend0.at(s_i) );
			    cbc_dpd->DumpData(_os);
			    if(stop_at_fail)
				throw WaferTestException( test_name + " " + tmp, err_code ); 
			    else
				print_test( test_name, tmp, _os );
			}
			if( stub_bend_check && (int)sb.at(s_i) != evt_bend0.at(s_i)){
			    stub_ok = false;
			    oss.str("");
			    sprintf(tmp, "EVT[%04d] Stub is found at a wrong bend bend=0x%02x for addr=0x%02x bend=0x%02x.", 
				    evt_i, sb.at(s_i), evt_addr0.at(s_i), evt_bend0.at(s_i) );
			    cbc_dpd->DumpData(_os);
			    if(stop_at_fail)
				throw WaferTestException( test_name + " " + tmp, err_code ); 
			    else
				print_test( test_name, tmp, _os );
			}
			if(this_addr_ok && h_bend) h_bend->Fill( evt_bend0.at(s_i), sb.at(s_i) );
		    }
		}
	    }
	    evt_i++;
	    total_hits += evt_hit_i;
	}
	delete buffer;

	if(_debug && hit_check){
	    sprintf( tmp, "Total # of hits created = %d", total_hits );
	    print_test( test_name, tmp, _os );
	}
	if(_debug && (stub_addr_check || stub_bend_check) ){
	    sprintf( tmp, "Total # of stubs created = %d", stub_i );
	    print_test( test_name, tmp, _os );
	}

	bool ok = hit_ok && stub_ok; 
	if(!ok) result = err_code;

	return result;
    }
    int WaferTest::read_amux( std::vector<std::string> &bias, std::vector<int> &bias_index, int sample_count, const std::string &test_name, int stat_start_s){

	vector<uint32_t> fscbuf_data;

	unsigned amux_reg_addr = 15; 
	unsigned value(0);
	int ntrig(0);
	// wait for 10 ms after changing amux.
	int pause_n100us(100);
	for(unsigned i=0; i < bias.size(); i++){
	    value = 0x81100000 | (amux_reg_addr << 8) | (32+bias_index.at(i));
	    fscbuf_data.push_back(value);
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    int p(0);
	    while (p < pause_n100us){
		fscbuf_data.push_back((unsigned) 0xafa00000);
		p++;
	    }
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    for(int i_s = 0; i_s < sample_count; i_s++){
		fscbuf_data.push_back((unsigned) 0x28000000);
	    }
	    ntrig+=sample_count;
	}
	if(_debug) cout << "# of triggers = " << ntrig << endl;
	fscbuf_data.push_back((unsigned) 0xe0000000);
	if(_debug>=2) dump_seq_data(fscbuf_data, _os);
	_cbc3be->WriteFscBram(fscbuf_data);
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(60);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	fscbuf_data.clear();
	//keithley read
	_kmm->kmm_read_ndata(ntrig); 
	//cout << "kmm nread = " << nread << endl;
	vector<float> vmeas = _kmm->get_kmm_vdata();
	
	bool ok = write_amux_values( vmeas, bias, sample_count, test_name, stat_start_s );

	int result(0);
	if(!ok) result = WT_ERR_AMUX_BIAS; 
	return result; 

    }	
    bool WaferTest::write_amux_values( std::vector<float> &vmeas, std::vector<std::string> &bias, int sample_count, const std::string &test_name, int start_s){

	bool ok(true);
	char tmp[256];
	for(unsigned i=0; i<bias.size(); i++){
	    sprintf(tmp, "frame30_AMUX_%s.txt", bias.at(i).c_str());
	    string ofname(tmp);
	    ofstream ofile(ofname.c_str());

	    sprintf(tmp, "%s:amux_%s_file", test_name.c_str(), bias.at(i).c_str());
	    _parameters.set( tmp, ofname);

	    sprintf(tmp, "# Title: AMUX values for %s.\n", bias.at(i).c_str());
	    ofile << tmp; 
	    sprintf(tmp, "# Fields: %10s %10s %10s\n", "index", "value", "error" );
	    ofile << tmp; 
	    double sum(0), sume2(0), mean(0), dmean(0), sigma(0), min(1.0),max(0.0);
	    bool this_ok(true);
	    for(int i_smpl=0; i_smpl < sample_count; i_smpl++){ 
		double val = vmeas.at(i*sample_count + i_smpl);
		double val_err = _kmm->get_DC_err( val );
		sprintf(tmp, "%d %10.5f %10.5f\n", i_smpl, val, val_err);
		ofile << tmp;
		if(i_smpl<start_s) continue;
		min = (val < min ) ? val : min;
		max = (val > max ) ? val : max;
		sum+=val;
		sume2+=val_err*val_err;
		sigma+=val*val;
	    }
	    mean = sum / (sample_count-start_s);
	    dmean = sqrt(sume2) / (sample_count-start_s);
	    sigma = sqrt(sigma/(sample_count-start_s) - mean*mean);

	    if( min < mean * ( 1 - 0.005 ) || mean * ( 1 + 0.005 ) < max ){
		sprintf(tmp, "%s does not settle in 0.5%% : (mean-min)/mean = %f (max-mean)/mean = %f", bias.at(i).c_str(), (mean-min)/mean, (max-mean)/mean);
		print_test(test_name, tmp, _os);
		this_ok = false; 
	    }
	    sprintf(tmp, "# STATS from %02d to %02d ; %02d points\n",  start_s, sample_count-1, (sample_count - start_s) ); 
	    ofile << tmp;
	    sprintf(tmp, "#%9s %8.5f +/- %8.5f\n", "Mean", mean, dmean);
	    ofile << tmp;
	    sprintf(tmp, "#%9s %10.5f %10.2f(%%)\n", "Sigma", sigma, sigma/mean*100);
	    ofile << tmp;
	    if(this_ok)
		sprintf(tmp, "#%9s\n", "passed"); 
	    else{
		sprintf(tmp, "#%9s\n", "failed"); 
		ok = false;
	    }
	    ofile << tmp;
	    ofile.close();
	}
	return ok;
    }
    int WaferTest::frame03( const string &test_name, map<string, string> & settings ){


	string node = "sysreg.ctrl2.fmc_l12_pwr_en"; 
	ipbutl::write_ipbus( _cbc3be->GetHwInterface(), node, 1 );

	float vddd_cur(0), vldoi_cur(0);
	measure_currents( test_name + "_before_cbc_reset" , vddd_cur, vldoi_cur );

	int fci(0);
	char tmpstr[256];
	sprintf( tmpstr, "FC7 is configured, CBC reset is sent, CBC I2C bus is initialized, CBC data timing is tuned with FCI=%d", fci);
	print_test(test_name, tmpstr, _os );
	string fname = _setting_file_dir + settings["be_config_file"];
	int result = config_be(fname, 0);

	measure_currents( test_name, vddd_cur, vldoi_cur );

	return result;
    }
    int WaferTest::frame06( const std::string &test_name, std::map<std::string, std::string> &settings){

	_os << "frame06 begin" << endl;
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );

	/*
	_kmm->kmm_printf(":SENSE:FUNC \"VOLT:DC\"");     //dc voltage measurements
	_kmm->kmm_printf(":SENSE:VOLT:RANG 10");         //input voltage range
	_kmm->set_range("10");
	_kmm->kmm_printf(":SENSE:ZERO:AUTO OFF");        //autozero on request only
	_kmm->kmm_printf(":TRIGger:SOURce IMMediate");
	_kmm->kmm_printf(":SAMPle:COUNt 1");
	_kmm->kmm_printf(":TRIGger:COUNt 1");
	_kmm->kmm_printf(":SENSE:ZERO:AUTO ONCE");        //autozero 
	*/
	_kmm->set_DC_measurement("0.001", "10", 1, 1, false);

	_kmm->kmm_printf(":INIT");	
	_kmm->kmm_read_ndata(1); 
	float vdda = strtof((char *)_kmm->get_rbuf(),0);
	_parameters.set("frame06:VDDA:V", vdda);
	char tmp[256];
	sprintf(tmp, "VDDA reading. %.3f V", vdda);
	print_test(test_name, tmp, _os);
	_os << "frame06 end" << endl;
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	return 0;
    }
    int WaferTest::frame10( const std::string &test_name, std::map<std::string, std::string> &settings){

	//check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
	_kmm->set_DC_measurement("0.001", "10", 1, 1, false);

	_kmm->kmm_printf(":INIT");	
	_kmm->kmm_read_ndata(1); 
	float vdda = strtof((char *)_kmm->get_rbuf(),0);
	_parameters.set("frame10:VDDA:V", vdda);
	char tmp[256];
	sprintf(tmp, "VDDA reading. %.3f V", vdda);
	print_test(test_name, tmp, _os);
	//check_cbci2c_reply_fifo(_hw, _os, 1, _debug);

	int result(0);

	float vddd_cur(0), vldoi_cur(0);
	measure_currents( test_name, vddd_cur, vldoi_cur );

	if(vddd_cur >= 50) result = WT_ERR_CURR; 
	if(vldoi_cur >= 200) result = WT_ERR_CURR; 

	return result;
    }
    int WaferTest::frame11( const std::string &test_name, std::map<std::string, std::string> &settings){

	string seq_fname = _setting_file_dir + settings["seq_file"];

	if(settings.find("cbci2c_file") != settings.end()){
	    string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	/*
	unsigned value = (unsigned)0;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );
	unsigned fci[3] = { 1, 3, 0 };
	for(unsigned i=1; i<3; i++){
	    unsigned value = fci[i];
	    _cbc3be->rwCbcI2cRegsByName(1,1,"FciAndError.fci_delay", value);
	    _cbc3be->TuneDataClockTiming(1);
	    _cbc3be->InitializeRun();
	}
	value = (unsigned)1;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );
	*/

	string ofname( test_name + "_rawdata_slvs6_dump.txt");
	_parameters.set(test_name+":ofile", ofname);
	ofstream ofile(ofname.c_str());

/*
	unsigned N(120);
*/
	unsigned N(1000);
	unsigned n = N/2;//n bx data to save.

	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.cbc_id", 1 );
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.latency", 0 );
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

	_cbc3be->WriteFscBram(seq_fname);
	_cbc3be->InitializeRun();

	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1000);

	uint32_t *rawdata(0);
	rawdata = new uint32_t[N];
	read_raw_data(test_name, N, rawdata, WT_ERR_RAW_DATA);

	int *slvs6(0);
	slvs6 = new int[n];
	raw_data_get_slvs6(rawdata, slvs6, n);

	if(raw_data_dump_l1_data_frame(slvs6, n, ofile) != 0){
	    raw_data_dump_data(rawdata, N, ofile);
	    delete rawdata;
	    delete slvs6;
	    throw WaferTestException( test_name + " failed.", WT_ERR_RAW_DATA | WT_ERR_OTHER); 
	}
	vector<int> saddr;
	vector<int> sbend;
	raw_data_find_stubs(rawdata, n, saddr, sbend);
	ofile << "----stubs" << endl;
	for(unsigned int i=0; i < saddr.size(); i++){
		ofile << "stub[" << i << "]" << saddr.at(i) << "," << sbend.at(i) << endl;
	}
	ofile << "=========================" << endl;
	raw_data_dump_data(rawdata, N, ofile);
	ofile.close();
	if(_debug){
	    raw_data_dump_data(rawdata, N, _os);
	}

	delete rawdata;
	delete slvs6;
	return 0;
    }
    int WaferTest::frame14( const std::string &test_name, std::map<std::string, std::string> &settings){

	string tmpstr;
	char tmp[256];
	int result(0);

	if(settings.find("cbci2c_file1") == settings.end())
	    throw WaferTestException( test_name + " CBC I2C setting file cbci2c_file1 is missing.", WT_ERR_SETTINGS ); 
	if(settings.find("cbci2c_file2") == settings.end())
	    throw WaferTestException( test_name + " CBC I2C setting file cbci2c_file2 is missing.", WT_ERR_SETTINGS ); 

	string cbci2c_fname1 = _setting_file_dir + settings["cbci2c_file1"];
	string cbci2c_fname2 = _setting_file_dir + settings["cbci2c_file2"];

	tmpstr = settings["no_fuse"];
	cout << "no_fuse = " << tmpstr << endl;
	bool no_fuse = atoi(tmpstr.c_str()); 
	tmpstr = settings["chip_id"];
	cout << "chip_id = " << tmpstr << endl;
	unsigned chip_id = strtol(tmpstr.c_str(), 0, 0);
	tmpstr = settings["target_bg"];
	float target_bg = strtof(tmpstr.c_str(), 0);
	tmpstr = settings["i2c_bg"];
	unsigned i2c_bg = strtol(tmpstr.c_str(), 0, 0); 

	vector<unsigned> bestbg_bgi2c;
	if( no_fuse && i2c_bg > 0 ){
	    _i2c_bg = true; 
	    bestbg_bgi2c.push_back(i2c_bg);
	}
	int pause_measbg_bgscan(10);
	int pause_measvdda_bgscan(15);
	if(settings.find("pause_measbg_bgscan")!= settings.end()) pause_measbg_bgscan = strtol(settings["pause_measbg_bgscan"].c_str(), 0, 0);
	if(settings.find("pause_measvdda_bgscan")!= settings.end()) pause_measvdda_bgscan = strtol(settings["pause_measvdda_bgscan"].c_str(), 0, 0);

	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname1, _cbc_id );
	_cbci2c_page1_main = cbci2c_fname1;

	//bring the bandgap value to the initial value in the scan.
	unsigned value = 1;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, 1 );
	//setting to read fused bangap setting.
	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, 1 );
	//write fuse setting.
	unsigned reg_bg_i2c(0);
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.bg_fuse", reg_bg_i2c, 1 );

	if(!_i2c_bg){
	    //----------------------------------
	    // band gap read for band gap scan
	    //----------------------------------
	    int pa[] = {0,10,11,13,15,20,25,30};
	    vector<int> pause(pa, pa + sizeof(pa)/sizeof(int));
//	    vector<unsigned> bestbg_bgi2c;
	    vector<float> bestbg;
	    bool inc(true);
	    for(unsigned i=0; i < pause.size(); i++){
		if(!_tune && (pause.at(i) != pause_measbg_bgscan) ) continue;
		map<unsigned, float> bg_meas;
		sprintf(tmp, "frame14_bg_bgscan_pause_%.2fms.txt", pause.at(i) * 0.1 );
		string fname(tmp);
		sprintf( tmp, "%s:bg_bgscan_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
		_parameters.set(tmp, fname);
		bgmeas_bgscan(test_name, fname, bg_meas, _os, inc, pause.at(i) );
		int best_bgi2c = find_best_bgi2c( bg_meas, target_bg);
		bestbg_bgi2c.push_back(best_bgi2c);
		bestbg.push_back(bg_meas[best_bgi2c]);
		inc = inc ? false : true;
	    }
	    if(_tune){
		sprintf(tmp, "frame14_bestbg_bgi2c.txt");
		ofstream bestbg_ofile(tmp);
		_parameters.set("frame14:best_bg_pause_scan_file", tmp);
		sprintf(tmp, "# Title: Bandgap values for best bandgap setting for different pause.\n");
		bestbg_ofile << tmp; 
		sprintf(tmp, "# Fields: %10s %10s %10s %10s\n", "Pause(100ns)", "best_bg_i2c", "Bandgap(V)", "diff(bg-target_bg)(V)" );
		bestbg_ofile << tmp; 
		for(unsigned i=0; i < bestbg_bgi2c.size(); i++){
		    float diff = bestbg.at(i) - target_bg;
		    bestbg_ofile << pause.at(i) << " " << bestbg_bgi2c.at(i) << " " << bestbg.at(i) << " " << diff << endl; 
		}
		bestbg_ofile.close();
	    }

	    float e = _kmm->get_DC_err(bestbg.back()); 
	    sprintf(tmp, "The best BG setting is found with I2C value %d at BG %.4f+/-%.4f V for target %.4f.", bestbg_bgi2c.back(), bestbg.back(), e, target_bg);
	    print_test(test_name, tmp, _os); 

	    //----------------------------------
	    // VDDA read for band gap scan
	    //----------------------------------
	    vector<unsigned> bestvdda_bgi2c;
	    vector<float> bestvdda;
	    for(unsigned i=0; i < pause.size(); i++){
		if(!_tune && (pause.at(i) != pause_measvdda_bgscan)) continue;
		map<unsigned,float> vdda_meas;
		sprintf(tmp, "frame14_vdda_bgscan_pause_%1.2fms.txt", pause.at(i) * 0.1 );
		string fname(tmp);
		sprintf( tmp, "%s:vdda_bgscan_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
		_parameters.set(tmp, fname);
		vddameas_bgscan(test_name, fname, vdda_meas, _os, inc, pause.at(i) );
		int best_bgi2c = find_best_bgi2c( vdda_meas, target_bg*2);
		bestvdda_bgi2c.push_back(best_bgi2c);
		bestvdda.push_back(vdda_meas[best_bgi2c]);
		inc = inc ? false : true;
	    }
	    if(_tune){
		sprintf(tmp, "frame14_bestvdda_bgi2c.txt");
		ofstream bestvdda_ofile(tmp);
		_parameters.set("frame14:best_vdda_pause_scan_file", tmp);
		sprintf(tmp, "# Title: VDDA values for best bandgap setting for different pause.\n");
		bestvdda_ofile << tmp; 
		sprintf(tmp, "# Fields: %10s %10s %10s %10s\n", "Pause(100ns)", "best_bg_i2c", "VDDA(V)", "diff(VDDA-target_bg*2)" );
		bestvdda_ofile << tmp; 
		for(unsigned i=0; i < bestvdda_bgi2c.size(); i++){

		    float diff = bestvdda.at(i) - target_bg*2;
		    bestvdda_ofile << pause.at(i) << " " << bestvdda_bgi2c.at(i) << " " << bestvdda.at(i) << " " << diff << endl; 
		}
		bestvdda_ofile.close();
	    }
	}
	
             
	    
	//bring the bandgap value to the best value
	value = 1;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, 1 );
	//setting to read fused bangap setting.
	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, 1 );
	//read fuse setting.
	unsigned best_bg_i2c = bestbg_bgi2c.back(); 
//	unsigned best_bg_i2c = 0;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.bg_fuse", best_bg_i2c, 1 );

	_parameters.set("frame14:best_bg_i2c", (float) best_bg_i2c );

	//usleep(500000);
	usleep(10000);

	//----------------------------------
	//read preburn bg & vdda values with the best bg setting
	//----------------------------------
	//VDDA reading
	int sample_count=1;
	int trig_count=1;
	_kmm->set_DC_measurement("0.001", "10", sample_count, trig_count, false);
	_kmm->kmm_printf(":INIT");
	_kmm->kmm_read_ndata(1); 
	float preburn_vdda = strtof((char *)_kmm->get_rbuf(),0);
	float preburn_vdda_err = _kmm->get_DC_err( preburn_vdda );

	//bandgap reading
	//band gap read setting for relayA
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	usleep( 2000 );
	usleep( 10000 );

	_kmm->set_DC_measurement("0.001", "1", sample_count, trig_count, false);
	_kmm->kmm_printf(":INIT");	
	_kmm->kmm_read_ndata(1); 
	float preburn_bg = strtof((char *)_kmm->get_rbuf(),0);
	float preburn_bg_err = _kmm->get_DC_err( preburn_bg );
	sprintf(tmp, "(Bandgap, VDDA) with the best BG I2C setting %d : (%.4f+/-%.4f, %.4f+/-%.4f)", bestbg_bgi2c.back(), preburn_bg, preburn_bg_err, preburn_vdda, preburn_vdda_err);
	print_test(test_name, tmp, _os); 
	_parameters.set("frame14:pre-burn_vdda", preburn_vdda);
	_parameters.set("frame14:pre-burn_vdda_err", preburn_vdda_err);
	_parameters.set("frame14:pre-burn_bg", preburn_bg);
	_parameters.set("frame14:pre-burn_bg_err", preburn_bg_err);

	float vddd_cur(0), vldoi_cur(0);
	measure_currents( test_name, vddd_cur, vldoi_cur );
	_parameters.set("frame14:pre-burn_vddd_cur:mA", vddd_cur);
	_parameters.set("frame14:pre-burn_vldoi_cur:mA", vldoi_cur);


	/*
	if( fabs(preburn_vdda - 2. * (preburn_bg))  > 0.03 * 2 * preburn_bg ){ 
//	VDDA measurement dropped by 3%
//	    throw WaferTestException( "VDDA value is low.", WT_ERR_VDDA ); --- fix me
	}
	*/

	if( fabs(preburn_vdda - 2. * (preburn_bg))  > 0.10 * 2 * preburn_bg ){ 
//	VDDA measurement dropped by 10%
	    print_test( test_name, "VDDA value is low.", _os);
	    result += WT_ERR_VDDA; 
	}

	unsigned bg_reg_addr = 21; 
	unsigned blow_bgi2c = bestbg_bgi2c.back();
	unsigned fuse_bg_i2c(0);
	unsigned fuse_chip_id(0);

	//----------------------------------
	// read fuse values - bandgap is configured by fuse value. override=0
	//----------------------------------
	//read bandgap fuse setting.
	//do not override the fused bandgap setting with the I2C register.
	//overrride(0), sel(1:fuse)
	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, _cbc_id );
	//setting to read fused bangap setting.
	value = 1;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, _cbc_id );
	_cbc3be->rwCbcI2cRegsByName( 1, 0, "BandgapFuse.bg_fuse", fuse_bg_i2c, _cbc_id );
	//read chip id fuse setting
	value = 1;

	_cbc3be->rwCbcI2cRegsByName( 0, 1, "ChipIdFuse.sel", value, _cbc_id ); 
	_cbc3be->rwCbcI2cRegsByName( 1, 0, "ChipIdFuse.id", fuse_chip_id, _cbc_id ); 

	sprintf(tmp, "Bandgap (Initial fuse value, tuned value) : (%d(0x%02x), %d(0x%02x)).", 
		fuse_bg_i2c, fuse_bg_i2c, blow_bgi2c, blow_bgi2c);
	print_test(test_name, tmp, _os );
	sprintf(tmp, "Chip id (Initial fuse value, value to be blown) : (%d(0x%05x), %d(0x%05x)).", 
		fuse_chip_id, fuse_chip_id, chip_id, chip_id);
	print_test(test_name, tmp, _os );

	int fuse_bg_dlim(2);
	if( !no_fuse){

	    if( fuse_chip_id == chip_id && fuse_bg_i2c != 0 ){
		if( abs((int)fuse_bg_i2c - (int)blow_bgi2c) <= fuse_bg_dlim ){ 
		    print_test(test_name, "Fuse is already blown with a good bg value. Skipping fuse blowing.", _os);
		    return result;
		}
		else{
		    sprintf(tmp, "Fuse bandgap invalid.");
		    throw WaferTestException( tmp, WT_ERR_FUSE_PROC ); 
		}
	    }
	    else if ( fuse_chip_id != chip_id && fuse_bg_i2c != 0 ){
		if( abs((int)fuse_bg_i2c - (int)blow_bgi2c) <= fuse_bg_dlim ){ 
		    blow_bgi2c = fuse_bg_i2c;
		}
		else{
		    sprintf(tmp, "Fuse bandgap invalid.");
		    throw WaferTestException( tmp, WT_ERR_FUSE_PROC ); 
		}
	    }
	    int max_nfuse(10), nfuse(0);

	    for(int j = 0; j < max_nfuse; j++){
		//		    sprintf(tmp, "Fuse blowing");
		//		    print_test(test_name, tmp, _os); 
		//turn on burn33
		_cbc3be->WriteBeBoardConfig("relay1.relayB", RELAYB_ON );
		//Mark wait for 1 sec.  I will wait for 15ms.
		usleep(15000);
		//---------------------------------------------------
		unsigned value = 0x80 | blow_bgi2c;
		_cbc3be->rwCbcI2c_reg( 0, 1, 1, bg_reg_addr, value, 1 ); 
		value = chip_id;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "ChipIdFuse.id", value, 1 ); 
		usleep(500000);
		//	    usleep(10000);
		_cbc3be->WriteBeBoardConfig("cs_ctrl.wt.fpp", 1); //Mark waits for 0.5 sec.  I will wait 10ms.
		//		usleep(10000); 
		//		usleep(100000); 
		usleep(500000); 
		//turn off burn33
		_cbc3be->WriteBeBoardConfig("relay1.relayB", RELAYB_OFF );
		//Mark does not wait.  I will wait for 10ms.
		usleep(10000);

		nfuse++;

		//----------------------------------
		// Fuse burning check
		//----------------------------------
		_cbc3be->CbcReset();
		_cbci2c_page1_main = "default";
		_cbci2c_page2      = "default";
		//initialize CBC I2C bus at BE
		_cbc3be->CbcI2cBusInit();
		value = 0;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, _cbc_id );
		//setting to read fused bangap setting.
		value = 1;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, _cbc_id );
		fuse_bg_i2c=0;
		_cbc3be->rwCbcI2cRegsByName( 1, 0, "BandgapFuse.bg_fuse", fuse_bg_i2c, _cbc_id );
		//read chip id fuse setting
		value = 1;

		_cbc3be->rwCbcI2cRegsByName( 0, 1, "ChipIdFuse.sel", value, _cbc_id ); 
		_cbc3be->rwCbcI2cRegsByName( 1, 0, "ChipIdFuse.id", fuse_chip_id, _cbc_id ); 

		sprintf(tmp, "Fuse Bandgap (blown and readback) : (%d(0x%02x), %d(0x%02x)).", 
			blow_bgi2c, blow_bgi2c, fuse_bg_i2c, fuse_bg_i2c);
		print_test(test_name, tmp, _os );
		sprintf(tmp, "Fuse Chip id (blown and readback) : (%d(0x%05x), %d(0x%05x)).", 
			chip_id, chip_id, fuse_chip_id, fuse_chip_id);
		print_test(test_name, tmp, _os );

		if(fuse_bg_i2c == blow_bgi2c && fuse_chip_id == chip_id){
		    break;
		}
	    }
	    if(fuse_bg_i2c != blow_bgi2c || fuse_chip_id != chip_id) result += WT_ERR_FUSE_PROC; 
	    //modify this for real operation to stop testing.
	    if(fuse_bg_i2c != blow_bgi2c || fuse_chip_id != chip_id){
		//if(abs((int)fuse_bg_i2c - (int)blow_bgi2c)>1){
		sprintf(tmp, "Fuse blow failed. (%d)", nfuse);
		throw WaferTestException( tmp,  WT_ERR_FUSE_PROC ); 
	    }
	    else{
		sprintf(tmp, "Fuse blow o.k. (%d)", nfuse);
		print_test(test_name, tmp, _os );
	    }

	    //after burn check is done with a different register settings
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname2, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname2;

	    //measure Bandgap 
	    _kmm->set_DC_measurement("0.001", "1", sample_count, trig_count, false);
	    //wait for 1ms to measure bandgap.
	    usleep(1000);
	    _kmm->kmm_printf(":INIT");
	    _kmm->kmm_read_ndata(1); 
	    float afterburn_bg = strtof((char *)_kmm->get_rbuf(),0);
	    float afterburn_bg_err = _kmm->get_DC_err( afterburn_bg );

	    //measure VDDA 
	    _cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
	    usleep( 2000 );
	    _kmm->set_DC_measurement("0.001", "10", sample_count, trig_count, false);
	    _kmm->kmm_printf(":INIT");
	    _kmm->kmm_read_ndata(1); 
	    float afterburn_vdda = strtof((char *)_kmm->get_rbuf(),0);
	    float afterburn_vdda_err = _kmm->get_DC_err( afterburn_vdda );

	    sprintf(tmp, "(Bandgap, VDDA) with the after burn fuse bg setting %d : (%.4f+/-%.4f, %.4f+/-%.4f)", fuse_bg_i2c, afterburn_bg, afterburn_bg_err, afterburn_vdda, afterburn_vdda_err);
	    print_test(test_name, tmp, _os );

	    _parameters.set("frame14:after-burn_vdda", afterburn_vdda);
	    _parameters.set("frame14:after-burn_vdda_err", afterburn_vdda_err);
	    _parameters.set("frame14:after-burn_bg", afterburn_bg);
	    _parameters.set("frame14:after-burn_bg_err", afterburn_bg_err);

	    _parameters.set("frame14:blown_chip_id", (float)chip_id);
	    _parameters.set("frame14:blown_bandgap", (float)blow_bgi2c );
	    return result;
	 }

	 return result;
    }
    int WaferTest::frame15( const std::string &test_name, std::map<std::string, std::string> &settings){

    int result(0);

    string tmpstr;
    tmpstr = settings["no_fuse"];
    cout << "no_fuse = " << tmpstr << endl;
    bool no_fuse = atoi(tmpstr.c_str()); 

    if(no_fuse ) return result;
    if(_i2c_bg ) return result;

    const map<string, float> &num_params = _parameters.get_num_params();

    if(num_params.find("frame14:blown_chip_id") == num_params.end() ){
	throw WaferTestException( "blown chip id is not found in the parameter map.", WT_ERR_AFTER_FUSE ); 
    }
    if(num_params.find("frame14:blown_bandgap") == num_params.end() ){
	throw WaferTestException( "blown bandgap is not found in the parameter map.", WT_ERR_AFTER_FUSE ); 
    }
    unsigned chip_id = (unsigned) num_params.find("frame14:blown_chip_id")->second;
    unsigned blow_bgi2c = (unsigned) num_params.find("frame14:blown_bandgap")->second;

	_cbc3be->CbcReset();
	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	_cbc3be->CbcI2cBusInit();

	//read chip id fuse setting
	unsigned value = 1;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "ChipIdFuse.sel", value, _cbc_id ); 
	unsigned fuse_chip_id(0);
	_cbc3be->rwCbcI2cRegsByName( 1, 0, "ChipIdFuse.id", fuse_chip_id, _cbc_id ); 

	//read bandgap fuse setting.
	//do not override the fused bandgap setting with the I2C register.
	//overrride(0), sel(1:fuse)
	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, _cbc_id );
	//setting to read fused bangap setting.
	value = 1;
	_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, _cbc_id );
	unsigned fuse_bg_i2c(0);
	_cbc3be->rwCbcI2cRegsByName( 1, 0, "BandgapFuse.bg_fuse", fuse_bg_i2c, _cbc_id );

	char tmp[256];
	sprintf(tmp, "Bandgap (Blown and readback fuse) : (%d, %d).", blow_bgi2c, fuse_bg_i2c);
	print_test(test_name, tmp, _os );
	sprintf(tmp, "Chip id (Blown and readback fuse) : (%d, %d).", chip_id, fuse_chip_id);
	print_test(test_name, tmp, _os );

	if(fuse_bg_i2c != blow_bgi2c || fuse_chip_id != chip_id) result = WT_ERR_AFTER_FUSE; 
	/*
	   if(fuse_bg_i2c != blow_bgi2c || fuse_chip_id != _chip_id){
	   throw WaferTestException( "Fuse blow failed.", WT_ERR_AFTER_FUSE ); 
	   }
	   */
	_cbc3be->CbcReset();
	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	_cbc3be->CbcI2cBusInit();

	//AMUX = 7, IPRE1=100
	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	_cbci2c_page1_main = cbci2c_fname;

	//read afterburn bandgap
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	usleep( 2000 );
	/*
	_kmm->kmm_printf(":SENSE:VOLT:RANG 1");         //input voltage range
	_kmm->set_range("1");
	*/
	int sample_count=1;
	int trig_count=1;

	//wait for 1ms to measure bandgap.
	usleep(10000);

	_kmm->set_DC_measurement("0.001", "1", sample_count, trig_count, false);

	_kmm->kmm_printf(":INIT");
	_kmm->kmm_read_ndata(1); 
	float afterburn_bg = strtof((const char *)_kmm->get_rbuf(),0);
	float afterburn_bg_err = _kmm->get_DC_err( afterburn_bg );
	sprintf(tmp, "Bandgap voltage with frame15, AMUX=7, IPRE1=100 setting : %.4f+/-%.4f", afterburn_bg, afterburn_bg_err);
	print_test(test_name, tmp, _os );

	//read VDDA
	_cbc3be->CbcReset();
	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	_cbc3be->CbcI2cBusInit();

	_kmm->set_DC_measurement("0.001", "10", sample_count, trig_count, false);

	_kmm->kmm_printf(":INIT");
	_kmm->kmm_read_ndata(1); 
	float afterburn_vdda = strtof((const char *)_kmm->get_rbuf(),0);
	float afterburn_vdda_err = _kmm->get_DC_err( afterburn_vdda );

	sprintf(tmp, "VDDA voltage with default i2c setting with hard reset : %.4f+/-%.4f", afterburn_vdda, afterburn_vdda_err);
	print_test(test_name, tmp, _os );

	return result;
    }
    int WaferTest::frame16( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	string run_config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(run_config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(run_config_fname);
	    _run_config = run_config_fname;
	}

	if(settings.find("cbci2c_file") == settings.end()){
	    throw WaferTestException( test_name + " CBC I2C setting is missing.", WT_ERR_SETTINGS ); 
	}
	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];

	//Get sample count
	int sample_count(100);
	std::map<std::string,uint32_t> regmap;
	_cbc3be->ReadBeBoardConfig( run_config_fname, regmap );
	std::map<std::string,uint32_t>::iterator it_regmap = regmap.begin();
	it_regmap = regmap.find( "cs_cnfg.fcm.fcg.Ncycle" );
	if( it_regmap != regmap.end() ) sample_count = it_regmap->second; 

	_cbc3be->CbcReset();//Mark's proceduer
	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	//CBC I2C BUS INITIALIZATION AND CONFIGURATION
	_cbc3be->CbcI2cBusInit();
	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	_cbci2c_page1_main = cbci2c_fname;
	if(_i2c_bg){
	    unsigned value = 1;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, 1 );
	    //setting to read fused bangap setting.
	    value = 0;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, 1 );
	    const std::map<std::string, float> &num_params = _parameters.get_num_params();
	    if(num_params.find("frame14:best_bg_i2c") == num_params.end() ){
		throw WaferTestException( "best_bg_i2c is not found in the parameter map.", WT_ERR_OTHER ); 
	    }
	    value = num_params.find("frame14:best_bg_i2c")->second;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.bg_fuse", value, _cbc_id );
	}

	if( offset_tuning(test_name, sample_count) != 0 ) result = WT_ERR_OFFSET_TUNING;

	return result;

    }
    int WaferTest::frame17( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	string run_config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(run_config_fname)){
	    _cbc3be->WriteBeBoardConfig(run_config_fname);
	    _run_config = run_config_fname;
	}
	const std::map<std::string, std::string> &str_params = _parameters.get_str_params();
	string ofst_file;
	if( str_params.find("frame16:cbci2c_offset_file") == str_params.end() ){
	    if( settings.find("cbci2c_offset_file") == settings.end() ){
		print_test(test_name, "offset file name is not found in the parameters nor settings.", _os);
		return WT_ERR_OFFSET_TUNING;
	    }
	    else{
		ofst_file = settings["cbci2c_offset_file"];
	    }
	}
	else{
	    ofst_file = str_params.find("frame16:cbci2c_offset_file")->second;
	}
	//cout << "cbci2c_page2 " << _cbci2c_page2 << endl; 
	if(_cbci2c_page2.compare(ofst_file) != 0 ){
	    _cbc3be->rwCbcI2cRegs( 1, 1, ofst_file, _cbc_id );
	    _cbci2c_page2 = ofst_file;
	}
	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	double noise_lim(10), gain_cut(20);
	if( settings.find("noise_lim") != settings.end()) noise_lim = strtof( settings["noise_lim"].c_str(), 0 );
	if( settings.find("gain_cut") != settings.end()) gain_cut = strtof( settings["gain_cut"].c_str(), 0 );

	//Get sample count
	int sample_count(100);
	std::map<std::string,uint32_t> regmap;
	_cbc3be->ReadBeBoardConfig( run_config_fname, regmap );
	std::map<std::string,uint32_t>::iterator it_regmap = regmap.begin();
	it_regmap = regmap.find( "cs_cnfg.fcm.fcg.Ncycle" );
	if( it_regmap != regmap.end() ) sample_count = it_regmap->second; 

	map<CHANNEL_ID, double> ped_midp, ped_midp_e;
	map<CHANNEL_ID, double> tp_midp, tp_midp_e;
	map<CHANNEL_ID, double> gain, gain_e;
	double ped_mean(0), ped_mean_e(0), ped_sigma(0), ped_sigma_e(0);
	double tp_mean(0), tp_mean_e(0), tp_sigma(0), tp_sigma_e(0);

	if(_debug) cerr << "Pedestal VCTH scan" << endl;
	bool tp_en(false);
	vcth_scan_scurve_test( test_name, sample_count, tp_en, ped_midp, ped_midp_e, ped_mean, ped_mean_e, ped_sigma, ped_sigma_e, noise_lim ); 

	float vddd_cur(0), vldoi_cur(0);

	unsigned int vcth = 0x000; 
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );
	measure_currents( test_name + " vcth_0x000", vddd_cur, vldoi_cur );

	vcth = 0x2FF;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );
	measure_currents( test_name + " vcth_0x2FF", vddd_cur, vldoi_cur );
	vcth = (unsigned int) ped_mean;

	vcth = (unsigned int) ped_mean;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );
	measure_currents( test_name + " vcth_ped", vddd_cur, vldoi_cur );

	/*
	unsigned fci[3] = { 1, 3, 0 };
	for(unsigned i=0; i<3; i++){
	    unsigned value = fci[i];
	    _cbc3be->rwCbcI2cRegsByName(1,1,"FciAndError.fci_delay", value);
	    _cbc3be->TuneDataClockTiming(1);
	    _cbc3be->InitializeRun();
	}
	*/
	if(_debug) cerr << "Test pulse VCTH scan" << endl;

	tp_en = true;
	vcth_scan_scurve_test( test_name, sample_count, tp_en, tp_midp, tp_midp_e, tp_mean, tp_mean_e, tp_sigma, tp_sigma_e, noise_lim ); 

	char tmp[256];
	string ofname( test_name + "_gain.txt");
	_parameters.set(test_name+":gain_file", ofname);
	ofstream ofile(ofname.c_str());
	int ngood(0);
	double m(0), s(0);
	vector<double> v_gain;
	for(int i=1; i < 255; i++){
	    double g(-1), g_e(-1);
	    if(tp_midp[i] != -2000 && ped_midp[i] != -2000){
		g = tp_midp[i] - ped_midp[i];
		g_e = sqrt( tp_midp_e[i] * tp_midp_e[i] + ped_midp_e[i] * ped_midp_e[i] );
		m += g;
		s += (g*g);
		ngood++;
		gain[i] = g;
	    }
	    sprintf(tmp, "0x%02x %1.4f %1.4f\n", i, g, g_e);
	    ofile << tmp;
	}
	if(ngood){
	    m = m / ngood;
	    s = sqrt(s/ngood - m*m);
	}
	sprintf(tmp, "# stat gain mean = %.4f+/-%.4f\n", m, s);
	ofile << tmp;
	_parameters.set( test_name + ":pedestal_midp_mean", ped_mean);
	_parameters.set( test_name + ":pedestal_sigma_mean", ped_sigma);
	_parameters.set( test_name + ":tp_pot60_midp_mean", tp_mean);
	_parameters.set( test_name + ":tp_pot60_sigma_mean", tp_sigma);
	_parameters.set( test_name + ":tp_pot60_gain_mean", fabs(m));
	_parameters.set( test_name + ":tp_pot60_gain_mean_sigma", s);

	ofile.close();
	for(int i=1; i < 255; i++){
//	    if( fabs(gain[i] - m) > 10 * s)
	    if( fabs(gain[i] - m) > gain_cut)  {
	    
	    
//		throw WaferTestException( test_name + " a channel has a gain problem " + tmp, WT_ERR_GAIN ); 
         	  
//Made a failed frame 17 not stop the whole test, Johan
	    cout  << test_name + " a channel has a gain problem " + tmp;
	    result |= WT_ERR_GAIN;

            sprintf( tmp, "RESULT = 0x%08x", result);
            print_test(test_name, tmp, _os);
          }
	
	
	}

	return result;
    }
    int WaferTest::frame18( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	int dllscan(0);
	if(settings.find("dllscan")!= settings.end()) dllscan = strtol(settings["dllscan"].c_str(), 0, 0);


	unsigned vcth1(200);
	if(settings.find("vcth1")!= settings.end()) vcth1 = strtol(settings["vcth1"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth1, _cbc_id );
	result |= check_pipeline( test_name, 0 );

	unsigned vcth2(800);
	if(settings.find("vcth2")!= settings.end()) vcth2 = strtol(settings["vcth2"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth2, _cbc_id );
	result |= check_pipeline( test_name, 1 );

	unsigned pot(60);
	unsigned vcth(540);
	const std::map<std::string, float> &num_params = _parameters.get_num_params();
	if( num_params.find("frame17:tp_pot60_gain_mean") == num_params.end() || num_params.find("frame17:pedestal_midp_mean") == num_params.end() ){
	    if( settings.find("tp_pot") == settings.end()|| settings.find("vcth") == settings.end() )
		throw WaferTestException( test_name + " gain for tp_pot=60 is not found in the parameter, and test pulse & VCTH settings are not found in the settings.", WT_ERR_SETTINGS ); 
	    pot = strtol(settings["tp_pot"].c_str(), 0, 0);
	    vcth = strtol(settings["vcth"].c_str(), 0, 0);
	}
	else{
	    float ped = num_params.find("frame17:pedestal_midp_mean")->second;
	    float gain = num_params.find("frame17:tp_pot60_gain_mean")->second;
	    vcth = (int)(ped - 0.5 * gain);
	}
	unsigned value = (unsigned)0;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );
	value = (unsigned)1;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );

	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePotentiometer.pot", pot, _cbc_id );
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );

	result |= check_pipeline( test_name, 2 );

	value = (unsigned)0;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );

	char tmp[256];
	char tmp_test_name[256];
	if(dllscan!=0){

	    for(unsigned dll=0; dll < 26; dll++ ){ 

		int dll_result(0);

		sprintf( tmp_test_name, "%s_dll%02d", test_name.c_str(), dll);
		sprintf(tmp, "DLL setting %02d", dll);
		print_test(tmp_test_name, tmp, _os);

		_cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", dll, _cbc_id );

		_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth1, _cbc_id );
		dll_result |= check_pipeline( tmp_test_name, false );

		_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth2, _cbc_id );
		dll_result |= check_pipeline( tmp_test_name, true );

		sprintf( tmp, "RESULT = 0x%08x", dll_result);
		print_test(tmp_test_name, tmp, _os);
	    }
	}


	return result;

    }
    int WaferTest::frame19( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	unsigned dll(16);
	if(settings.find("dll")!= settings.end()) dll = strtol(settings["dll"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", dll, _cbc_id );

	print_test(test_name, "buffer test no error all hits", _os);
	unsigned vcth1(200);
	if(settings.find("vcth1")!= settings.end()) vcth1 = strtol(settings["vcth1"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth1, _cbc_id );
	result |= check_buffer_err_hit( test_name, false );

	print_test(test_name, "buffer test no error no hit", _os);
	unsigned vcth2(800);
	if(settings.find("vcth2")!= settings.end()) vcth2 = strtol(settings["vcth2"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth2, _cbc_id );
	result |= check_buffer_err_hit( test_name, true );

	print_test(test_name, "buffer test for pipeline address", _os);
	result |= check_buffer_pa( test_name);

	print_test(test_name, "buffer test for L1 counter", _os);
	result |= check_buffer_l1c( test_name);


	int dllscan(0);
	if(settings.find("dllscan")!= settings.end()) dllscan = strtol(settings["dllscan"].c_str(), 0, 0);
	char tmp[256];
	char tmp_test_name[256];
	if(dllscan!=0){

	    for(unsigned dll=0; dll < 26; dll++ ){ 

		int dll_result(0);

		sprintf( tmp_test_name, "%s_dll%02d", test_name.c_str(), dll);
		sprintf(tmp, "DLL setting %02d", dll);
		print_test(tmp_test_name, tmp, _os);

		_cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", dll, _cbc_id );

		print_test(tmp_test_name, "buffer test no error all hits", _os);
		_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth1, _cbc_id );
		dll_result |= check_buffer_err_hit( tmp_test_name, false );

		print_test(tmp_test_name, "buffer test no error no hit", _os);
		_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth2, _cbc_id );
		dll_result |= check_buffer_err_hit( tmp_test_name, true );

		print_test(tmp_test_name, "buffer test for pipeline address", _os);
		dll_result |= check_buffer_pa( tmp_test_name);

		print_test(tmp_test_name, "buffer test for L1 counter", _os);
		dll_result |= check_buffer_l1c( tmp_test_name);

		sprintf( tmp, "RESULT = 0x%08x", dll_result);
		print_test(tmp_test_name, tmp, _os);
	    }
	}

	return result;
    }
    int WaferTest::frame20( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	unsigned vcth(800);
	if(settings.find("vcth")!= settings.end()) vcth = strtol(settings["vcth"].c_str(), 0, 0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );

	int nwords(14*2);
	uint32_t *buffer = new uint32_t[nwords];
	BeDataDecoder be_data_decoder;
	
	vector<uint32_t> fscbuf_data;
	for(int a=31; a<64;a++){
	    unsigned value = 0x81100000 | (a << 8) | 0xff;
	    fscbuf_data.push_back(value);
	}
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	fscbuf_data.push_back((unsigned) 0xa0e00000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	//fscbuf_data.push_back((unsigned) 0xa0260000);
	//fscbuf_data.push_back((unsigned) 0xa1000000);
	for(int a=31; a<64;a++){
	    unsigned value = 0x81100000 | (a << 8);
	    fscbuf_data.push_back(value);
	}
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	fscbuf_data.push_back((unsigned) 0xa0e00000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	fscbuf_data.push_back((unsigned) 0xe0000000);

	_cbc3be->InitializeRun();
	//_cbc3be->SendFastSignal( "orbit_reset" ); 
	//_cbc3be->WriteBeBoardConfig("cs_ctrl.ds1.frame_counter_reset", 1);
	_cbc3be->WriteFscBram(fscbuf_data);
	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1);

	read_data(test_name, nwords, buffer, WT_ERR_CHANNEL_MASK);

	be_data_decoder.SetData( buffer, nwords * 4 );
	int ei(0);
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		if(_debug) cbc_dpd->DumpData(_os);
		int nhit = cbc_dpd->HitChannels().size();
		if(( ei==0 && nhit != 254) || ( ei==1 && nhit != 0 )){
		    result = WT_ERR_CHANNEL_MASK; 
		    cbc_dpd->DumpData(_os);
		}
	    }
	    ei++;
	}
	delete buffer;
	return result;
    }
    int WaferTest::frame21( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	TFile *froot = new TFile( "frame21.root", "recreate");
	TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1.5, 255.5, 16, 0, 16); 
	TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1.5, 255.5, 254, 1.5, 255.5); 
	TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

	unsigned N(512);
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

	_cbc3be->InitializeRun();
	int nevt(0);

	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;

	vector<int> v_max_cw;
	vector<int> v_nss;
	vector<int> v_nsw;
	vector<int> v_ibend;
	vector<int> addr0;
	vector<int> bend0;

	int max_cw(4);
	int pt_width(0);
	int ls(0);

	char tmp[256];

	//loop for # of strips in each cluster on the seed layer
	for( int nss = 1; nss < 5; nss ++){

	    //loop for # of strips in each cluster on the window layer
	    for( int nsw = 1; nsw < 5; nsw ++){

		if(nss != 1 && nsw != 1) continue;

		bool bend_half = (nss+nsw)%2;

		for(int ibend = -7; ibend < 8; ibend ++){ 

		    if(bend_half && ibend == 7) continue;

		    nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 
		}
	    }
	}
	sprintf( tmp, "Total # of stubs to be = %d", (int)addr0.size() );
	print_test( test_name, tmp, _os );

	result |= read_data_and_check_stubs(test_name, WT_ERR_STUB, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend);

	h_addr_bend->Write();
	h_addr->Write();
	h_bend->Write();
	froot->Close();

	//check the result.
	if(result){
	    print_test( test_name, " stub test failed.", _os );
	}
	return result;
    }
    int WaferTest::frame22( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	TFile *froot = new TFile( "frame22.root", "recreate");
	TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1.5, 255.5, 16, 0, 16); 
	TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1.5, 255.5, 254, 1.5, 255.5); 
	TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

	unsigned N(512);
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

	_cbc3be->InitializeRun();
	int nevt(0);

	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;

	vector<int> v_max_cw;
	vector<int> v_nss;
	vector<int> v_nsw;
	vector<int> v_ibend;
	vector<int> addr0;
	vector<int> bend0;

	int pt_width(0);
	int ls(0);

	char tmp[256];

	for(int max_cw = 4; max_cw > 0; max_cw--){

	    unsigned value = (unsigned)max_cw;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.width", value, 1 );

	    int nss = max_cw + 1;
	    int nsw = 1;
	    bool bend_half = (nss+nsw)%2;
	    int ibend = 0;
	    if(bend_half) ibend = -1;
	    nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 

	    nss = 1; 
	    nsw = max_cw + 1;
	    ibend = 0;
	    nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 
	}
	//layer swap test.  I do not do non swap test, since this is done already in frame21
	int max_cw(4), nss(1), nsw(1);
	ls = 1;
	unsigned value = max_cw; 
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.width", value, 1 );
	value = ls;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.lswap", value, 1 );
	int ibend = 0;
	nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 

	nss=2; 
	ibend = -1;
	nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 

	sprintf( tmp, "Total # of stubs to be created = %d", (int)addr0.size() );
	print_test( test_name, tmp, _os );

	result |= read_data_and_check_stubs(test_name, WT_ERR_STUB, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend);

	h_addr_bend->Write();
	h_addr->Write();
	h_bend->Write();
	froot->Close();

	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.lswap", value, 1 );

	//check the result.
	if(result){
	    print_test( test_name, " stub test failed.", _os );
	}
	return result;

    }
    int WaferTest::frame23( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	TFile *froot = new TFile( "frame23.root", "recreate");
	TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1.5, 255.5, 16, 0, 16); 
	TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1.5, 255.5, 254, 1.5, 255.5); 
	TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

	unsigned N(512);
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

	_cbc3be->InitializeRun();
	int nevt(0);

	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;

	vector<int> v_max_cw;
	vector<int> v_nss;
	vector<int> v_nsw;
	vector<int> v_ibend;
	vector<int> addr0;
	vector<int> bend0;

	int ls(0), max_cw(4);

	char tmp[256];

	for(int pt_width=1; pt_width < 14; pt_width++){

	    unsigned value = (unsigned) pt_width;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "PlStLogicSelAndPtWidth.pt_width", value, 1 );

	    for(int nss = 1; nss < 3; nss++){

		for(int nsw = 1; nsw < 3; nsw++){

		    for( int neg = 0; neg < 2; neg++){

			bool bend_half = (nss+nsw)%2;

			int ibend(0);
			if(bend_half){
			    ibend = pt_width / 2 + pt_width%2;
			    if(neg) ibend = -1 * ibend - 1; 
			}
			else{
			    ibend = pt_width / 2 + 1;
			    ibend = -1 * ibend;
			}

			nevt+=generate_stubs(test_name, ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend); 
		    }
		}
	    }
	}
	sprintf( tmp, "Total # of stubs to be created = %d", (int)addr0.size() );
	print_test( test_name, tmp, _os );

	result |= read_data_and_check_stubs(test_name, WT_ERR_STUB, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend);

	h_addr_bend->Write();
	h_addr->Write();
	h_bend->Write();
	froot->Close();

	if(result){
	    print_test( test_name, " stub test failed.", _os );
	}
	return result;
    }
    int WaferTest::frame24( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	stringstream oss;

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	if(settings.find("cbci2c_bends0_file") == settings.end()) throw WaferTestException(test_name + " bends file, cbci2c_bends0_file is not specified.", WT_ERR_SETTINGS);
	string cbci2c_bends0_fname = _setting_file_dir + settings["cbci2c_bends0_file"];

	// pT window offset settings
	if(settings.find("cbci2c_bends1_file") == settings.end()) throw WaferTestException(test_name + " bends file, cbci2c_bends1_file is not specified.", WT_ERR_SETTINGS);
	string cbci2c_bends1_fname = _setting_file_dir + settings["cbci2c_bends1_file"];
	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_bends1_fname, _cbc_id );
	unsigned cwo[12] = {1,2,3,4,5,6,15,14,13,12,11,10};//window offset
//	unsigned bnd[12] = {15,14,13,12,11,10,1,2,3,4,5,6};
	unsigned ibnd[12] = {(unsigned)-1,(unsigned)-2,(unsigned)-3,(unsigned)-4,(unsigned)-5,(unsigned)-6,1,2,3,4,5,6};

	TFile *froot = new TFile( "frame24.root", "recreate");
	TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1.5, 255.5, 16, 0, 16); 
	TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1.5, 255.5, 254, 1.5, 255.5); 
	TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

	int max_cw(4); 
	int pt_width(0);

	vector<int> v_max_cw;
	vector<int> v_nss;
	vector<int> v_nsw;
	vector<int> v_ibend;
	vector<int> addr0;
	vector<int> bend0;

	_cbc3be->InitializeRun();
	int nevt(0);

	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;

	for(int cwo_i = 0; cwo_i < 12; cwo_i++){

	    //create sequencer data
	    unsigned cworeg_value = (cwo[cwo_i] & 0xf) | ((cwo[cwo_i] & 0xf ) << 4); 
	    unsigned value = 0x81100000 | (0x14 << 8) | cworeg_value;

	    vector<uint32_t> fscbuf_data;
	    fscbuf_data.push_back(value);
	    value = 0x81100000 | (0x13 << 8) | cworeg_value;
	    fscbuf_data.push_back(value);
	    fscbuf_data.push_back((unsigned) 0xe0000000);
	    _cbc3be->WriteFscBram(fscbuf_data);
	    //cbc3be->InitializeRun();
	    _cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
	    _cbc3be->StartFscBramController();

	    for(int nss = 1; nss < 3; nss++){
		int nsw(nss);
		int ls(0);
		nevt+=generate_stubs(test_name, 0, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cwo_i, ibnd); 
	    }
	}
	/*
	int ni0(43);
	int nm(3);
	//
	unsigned N(512);
	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

	_cbc3be->InitializeRun();
	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;



	int nevt(0);
	for(int cwo_i = 0; cwo_i < 12; cwo_i++){

	    //create sequencer data
	    vector<uint32_t> fscbuf_data;
	    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

	    unsigned cworeg_value = (cwo[cwo_i] & 0xf) | ((cwo[cwo_i] & 0xf ) << 4); 
	    unsigned value = 0x81100000 | (0x14 << 8) | cworeg_value;
	    fscbuf_data.push_back(value);
	    value = 0x81100000 | (0x13 << 8) | cworeg_value;
	    fscbuf_data.push_back(value);

	    for(int nss = 1; nss < 3; nss++){

		if(_debug)cout << "NSS = " << nss << endl;

		for(int i0 = 0; i0 < ni0; i0++){

	//	       int stub_id[nm];
	//	       for( int im = 0; im < nm; im++)stub_id[im] = -1;

		    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
		    while(it_msks != msks.end()){
			//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
			if(it_msks->second != 0) {
			    it_msks->second = 0;
			    it_msks++;
			}
			else {
			    msks.erase(it_msks++); 
			}
		    }

		    int nth_stub(0);
		    for( int im = 0; im < nm; im++){

			int nss_even = !(nss%2);
			unsigned saddr = (i0 + im * ni0+1)*2 + nss_even;
			if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

			vector<int> chs; 
			//get channels for the stub
			for(int sch_i = 0; sch_i < nss; sch_i++){
			    int ch = (i0 + ni0 * im + sch_i)*2 + 1;
			    if(ch < 1 || ch > 254) break;
			    chs.push_back(ch);
			}
			if((int)chs.size()!=nss) continue;

			for(int wch_i = 0; wch_i < nss; wch_i++){
			    int ch = (i0 + ni0 * im + wch_i)*2 + 2;
			    if(ch < 1 || ch > 254) break;
			    chs.push_back(ch);
			}
			if((int)chs.size()!=(2*nss)) continue;

			//update the masks
			for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
			    msks[chs.at(ch_i)] = 1;
			}
			addr0.push_back(saddr);
			bend0.push_back(bnd[cwo_i]);
			v_max_cw.push_back(max_cw);
			v_nss.push_back(nss);
			v_nsw.push_back(nss);
			v_ibend.push_back(ibnd[cwo_i]);
			//stub_id[nth_stub++] = im;
		    }
		    if(_debug) cout << "Total # of stubs = " << nth_stub << endl;

		    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

		    //test position
		    fscbuf_data.push_back((unsigned) 0x22000000);
		    //fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
		    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
		    fscbuf_data.push_back((unsigned) 0xa0bb0000);
		    fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
		    fscbuf_data.push_back((unsigned) 0xa0100000);
		    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
		    nevt++;
		}
	    }
	    fscbuf_data.push_back((unsigned) 0xe0000000);
	    //	cout << "size of sequencer data = " << fscbuf_data.size() << endl;

	    if(_debug>=2) dump_seq_data(fscbuf_data, _os);

	    _cbc3be->WriteFscBram(fscbuf_data);
	    _cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
	    //	cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );

	    _cbc3be->StartFscBramController();
	    _cbc3be->WaitSequencerReady(1);
	    check_cbci2c_reply_fifo(_hw, _os, 1, _debug);
	}
	*/

	char tmp[256];
	sprintf( tmp, "Total # of stubs to be created = %d", (int)addr0.size() );
	print_test( test_name, tmp, _os );

	result |= read_data_and_check_stubs(test_name, WT_ERR_STUB, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend);

	froot->Close();

	//check the result.
	if(result){
	    print_test( test_name, " stub test failed.", _os );
	}
	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_bends0_fname, _cbc_id );
	unsigned value(0);
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "CoinWindowOffsets.ofst1", value, _cbc_id );
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "CoinWindowOffsets.ofst2", value, _cbc_id );
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "CoinWindowOffsets.ofst3", value, _cbc_id );
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "CoinWindowOffsets.ofst4", value, _cbc_id );

	return result;
    }
    int WaferTest::frame25( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	char tmp[256];
	stringstream oss;

	TFile *froot = new TFile( "frame25.root", "recreate");
	TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1.5, 255.5, 16, 0, 16); 
	TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1.5, 255.5, 254, 1.5, 255.5); 
	TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

	//int hip_reg_addr(12);
	//int hip_reg_val(0);

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	CBCI2C_TYPE_DATA type_data;
	CBCI2C_ITEM_DATA_MAP items;

	int nevt(0);
	int nevt_prev(0);
	vector<vector<int> > addr0;
	vector<vector<int> > bend0;
	vector<set<int> > ch0;

	vector<uint32_t> fscbuf_data;
	BeDataDecoder be_data_decoder;

	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;

	//cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
	_cbc3be->InitializeRun();

	//bool stop_at_fail = (debug == 0) ? true : false;
	bool stop_at_fail(false);
	bool stub_addr_check(true);
	bool stub_bend_check(false);
	bool hit_check(true);

	int ibend(0);
	int max_cw(4);
	int pt_width(0);
	int nss(1);
	int nsw(1);
	int ls(0);

	//HIP supp. enabled and source is or, 
	//but pipe and stub logic inputs are sampled (not HIP supp. logic output. HIP suppression logic is not selected).
	items["hip_count"] = 3;
	items["hip_supp"]  = 1;
	items["hip_src"]   = 1;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );
	items.clear();
	items["pl_sel"] = 0;
	items["st_sel"] = 0;
	type_data.first = "PlStLogicSelAndPtWidth";
	type_data.second = items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id);

	//unsigned value(0);
	//_cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, value, _cbc_id);
	//CBCI2C_REGID reg_id = get_regid(1,_cbc_id,1,hip_reg_addr);
	//unsigned hip_reg_val = get_val(_cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
	//cout << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;
	bool l1_supp(false);
	bool stub_supp(false);

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'SAMPLED'. Hits and stubs should be created.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;

	items.clear();
	items["pl_sel"] = 3;
	items["st_sel"] = 3;
	type_data.first = "PlStLogicSelAndPtWidth";
	type_data.second = items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id);
	l1_supp = true;
	stub_supp = true;

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'FIXED PULSE WIDTH'. No hit, no stub.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;

	items.clear();
	items["pl_sel"] = 1;
	items["st_sel"] = 1;
	type_data.first = "PlStLogicSelAndPtWidth";
	type_data.second = items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id);
	l1_supp = false;
	stub_supp = false;

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'OR'. Hits and stubs should be created.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;

	items.clear();
	items["pl_sel"] = 2;
	items["st_sel"] = 2;
	type_data.first = "PlStLogicSelAndPtWidth";
	type_data.second = items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id);
	l1_supp = false;
	stub_supp = false;

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. Hits and stubs should be created.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;

	items.clear();
	items["hip_src"] = 0;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );

	//_cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, value, _cbc_id);
	//reg_id = get_regid(1,_cbc_id,1,hip_reg_addr);
	//hip_reg_val = get_val(_cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
	//_os << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;

	l1_supp = false;
	stub_supp = false;

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='SAMPLED', count=3). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. Hits and stubs should be created.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;

	items.clear();
	items["hip_count"] = 0;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );

	//_cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, value, _cbc_id);
	//reg_id = get_regid(1,_cbc_id,1,hip_reg_addr);
	//hip_reg_val = get_val(_cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
	//_os << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;

	l1_supp = true;
	stub_supp = true;

	nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='SAMPLED', count=0). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. No hit, no stub.", nevt_prev, nevt-1 );
	print_test( test_name, tmp, _os );
	nevt_prev = nevt;


	items.clear();
	items["hip_supp"]  = 0;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );

	l1_supp = true;
	stub_supp = true;
	for(int hip_count = 0; hip_count < 8; hip_count ++ ){
	    items.clear();
	    items["hip_count"] = hip_count;
	    type_data.first = "HipAndTestMode"; 
	    type_data.second= items;
	    _cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );

	    //_cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, value, _cbc_id);
	    //reg_id = get_regid(1,_cbc_id,1,hip_reg_addr);
	    //hip_reg_val = get_val(_cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
	    //_os << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;


	    nevt+=generate_stubs(test_name, nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0); 
	    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic enabled (source='SAMPLED', count=%d). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. No hit, no stub.", nevt_prev, nevt-1, hip_count );
	    print_test( test_name, tmp, _os );
	    nevt_prev = nevt;
	}

	result |= read_data_and_check_stubs_and_hits(test_name, WT_ERR_HDHIP, stop_at_fail, stub_addr_check, stub_bend_check, hit_check,
		nevt, addr0, bend0, ch0, h_addr_bend, h_addr, h_bend);

	h_addr_bend->Write();
	h_addr->Write();
	h_bend->Write();
	froot->Close();

	items["hip_count"] = 0;
	items["hip_supp"]  = 0;
	items["hip_src"]   = 1;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id );
	items.clear();
	items["pl_sel"] = 0;
	items["st_sel"] = 0;
	type_data.first = "PlStLogicSelAndPtWidth";
	type_data.second = items;
	_cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, _cbc_id);
	//check the result.
	if(result){
	    print_test( test_name, "HIT detect HIP suppression test failed.", _os );
	}
	return result;
    }
    int WaferTest::frame26( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	char tmp[256];
	stringstream oss;

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _run_config = config_fname;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname) != 0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	bool stop_at_fail = (_debug == 0 ) ? true : false;
	bool stub_addr_check = true;
	bool stub_bend_check = true;
	bool hit_check = true;
	int nevt = 1;

	vector<uint32_t> fscbuf_data;

	vector<vector<int> > addr0;
	vector<vector<int> > bend0;
	vector<set<int> >    ch0;

	vector<int> evt_addr0;
	vector<int> evt_bend0;
	set<int>    evt_ch0;

	//make three stubs
	map<CHANNEL_ID, unsigned> msks;
	for(unsigned i=1; i<255; i++) msks[i] = 0xff;
	int i0(3);
	int ni0(43);
	int nm(3);
	int nss(1);
	int nsw(1);
	int ibend(0);
	int ls(0);
	map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	while(it_msks != msks.end()){
	    //	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	    if(it_msks->second != 0) {
		it_msks->second = 0;
		it_msks++;
	    }
	    else {
		msks.erase(it_msks++); 
	    }
	}
	for( int im = 0; im < nm; im++){
	    //get channels for the stub index (i,j)
	    int i = i0 + ni0 * im;

	    unsigned saddr = get_seed_address(i,nss);
	    unsigned sbend = ibend & 0xf; 

	    vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls, _CBC3_0);
	    if(!chs.size()) continue;
	    evt_ch0.insert(chs.begin(), chs.end());

	    //update the masks
	    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		msks[chs.at(ch_i)] = 1;
	    }
	    evt_addr0.push_back(saddr);
	    evt_bend0.push_back(sbend);
	}
	mk_seqdata_channel_mask(msks, fscbuf_data, 0 );
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xe0000000);

	addr0.push_back(evt_addr0);
	bend0.push_back(evt_bend0);
	ch0.push_back(evt_ch0);

	_cbc3be->WriteFscBram(fscbuf_data);
	_cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

	_cbc3be->StartFscBramController();
	_cbc3be->WaitSequencerReady(1);
	check_cbci2c_reply_fifo(_hw, _os, 1, _debug);

	int bitslip0 = _cbc3be->ReadBeBoardRegister( "cs_stat.io.cbc_data.timing_tuning.fe1.bitslip_counter" );
	sprintf( tmp, "cs_stat.io.cbc_data.iserdes.cbc%d.in_delay_tap_out", _cbc_id ); 
	int idelay0 = _cbc3be->ReadBeBoardRegister( tmp );
	sprintf( tmp, "cs_stat.io.cbc_data.timing_tuning.fe1.mmcme2_drp_saddr");
	int saddr0 = _cbc3be->ReadBeBoardRegister( tmp );


	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", 512); 

	//3->0 has problem with test pulse.  The next test pulse request does not seem to work.
//	const unsigned fci[3] = { 1, 3, 0 };
	const unsigned fci[3] = { 3, 1, 0 };
//	int delay[3] = { 1, 2, 0 };
	const int delay[3] = { 2, 1, 0 };
	const int bitslip_diff[3] = { 2, 1, 0 }; 

	int bitslip(0);
	for(int fci_i = 0; fci_i < 3; fci_i++){

	    unsigned value = fci[fci_i];
	    _cbc3be->rwCbcI2cRegsByName(1,1,"FciAndError.fci_delay", value);
	    _cbc3be->TuneDataClockTiming(1);
	    _cbc3be->InitializeRun();

	    bitslip = _cbc3be->ReadBeBoardRegister( "cs_stat.io.cbc_data.timing_tuning.fe1.bitslip_counter" );
	    sprintf(tmp, "FCI %d, BITSLIP COUNTER = %d", fci[fci_i], bitslip); 
	    print_test(test_name, tmp, _os);

	    sprintf( tmp, "cs_stat.io.cbc_data.iserdes.cbc%d.in_delay_tap_out", _cbc_id ); 
	    int idelay = _cbc3be->ReadBeBoardRegister( tmp );
	    sprintf( tmp, "cs_stat.io.cbc_data.timing_tuning.fe1.mmcme2_drp_saddr");
	    int saddr = _cbc3be->ReadBeBoardRegister( tmp );

	    int diff = bitslip - bitslip0; 
	    int idelay_2 = idelay + (saddr0-saddr) * 18; 
	    if( abs(idelay_2 - idelay0) > 20 ){
		if ( idelay_2 < idelay0 ) diff++;
		else diff--;
	    }	
	    while(diff < 0) diff += 8;
	    diff = (diff % 8 );

	    if(diff != bitslip_diff[fci_i]){ 
		sprintf(tmp, "data timing is not expected. bitslip counter changed from %d to %d where %d 320MHz clock delay is expected.", bitslip0, bitslip, delay[fci_i] );
		throw WaferTestException( test_name + " " + tmp, WT_ERR_FCI ); 
	    }
	    fscbuf_data.clear();
	    fscbuf_data.push_back((unsigned) 0xa0011200);//fast reset & rdb recording
	    fscbuf_data.push_back((unsigned) 0xa0300000);
	    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	    fscbuf_data.push_back((unsigned) 0xe0000000);

	    _cbc3be->WriteFscBram(fscbuf_data);
	    _cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
	    //_cbc3be->InitializeRun();

	    _cbc3be->StartFscBramController();
	    _cbc3be->WaitSequencerReady(1);

	    result |= read_data_and_check_stubs_and_hits(test_name, WT_ERR_FCI, stop_at_fail, stub_addr_check, stub_bend_check, hit_check,
		    nevt, addr0, bend0, ch0, 0, 0, 0);
	}

	return result;

    }
    int WaferTest::frame27( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	char tmp[256];
	stringstream oss;

	_cbc3be->CbcReset();
	usleep(100000);

	_cbci2c_page1_main = "default";
	_cbci2c_page2      = "default";
	_cbc3be->CbcI2cBusInit();
	_cbc3be->TuneDataClockTiming(1);


	const std::map<std::string, std::string> &str_params = _parameters.get_str_params();
	string ofst_file;
	if( str_params.find("frame16:cbci2c_offset_file") == str_params.end() ){

	    if( settings.find("cbci2c_offset_file") == settings.end() ){
		print_test(test_name, "offset file name is not found in the parameters nor settings.", _os);
		throw WaferTestException( test_name + " CBC I2C offset file is not found in the parameter nor settings.", WT_ERR_SETTINGS ); 
	    }

	    ofst_file = settings["cbci2c_offset_file"];
	}
	else{
	    ofst_file = str_params.find("frame16:cbci2c_offset_file")->second;
	}

	_os << "OFFSET FILE NAME " << ofst_file << endl;
	print_test(test_name, string("offset file ") + ofst_file + " is written.", _os);  

	_cbc3be->rwCbcI2cRegs( 1, 1, ofst_file, _cbc_id );
	_cbci2c_page2 = ofst_file;

	if(settings.find("cbci2c_file") == settings.end())
	    throw WaferTestException( test_name + " CBC I2C setting file cbci2c_file is missing.", WT_ERR_SETTINGS ); 

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	_cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	_cbci2c_page1_main = cbci2c_fname;
	if(_i2c_bg){
	    unsigned value = 1;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, 1 );
	    //setting to read fused bangap setting.
	    value = 0;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, 1 );
	    const std::map<std::string, float> &num_params = _parameters.get_num_params();
	    if(num_params.find("frame14:best_bg_i2c") == num_params.end() ){
		throw WaferTestException( "best_bg_i2c is not found in the parameter map.", WT_ERR_OTHER ); 
	    }
	    value = num_params.find("frame14:best_bg_i2c")->second;
	    _cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.bg_fuse", value, _cbc_id );
	}


	//_cbc3be->SendFastSignal("test_pulse_req");

	unsigned pot(60);
	unsigned vcth(540);
	const std::map<std::string, float> &num_params = _parameters.get_num_params();
	if( num_params.find("frame17:tp_pot60_gain_mean") == num_params.end() || num_params.find("frame17:pedestal_midp_mean") == num_params.end() ){
	    if( settings.find("tp_pot") == settings.end()|| settings.find("vcth") == settings.end() )
		throw WaferTestException( test_name + " gain for tp_pot=60 is not found in the parameter, and test pulse & VCTH settings are not found in the settings.", WT_ERR_SETTINGS ); 
	    pot = strtol(settings["tp_pot"].c_str(), 0, 0);
	    vcth = strtol(settings["vcth"].c_str(), 0, 0);
	}
	else{
	    float ped = num_params.find("frame17:pedestal_midp_mean")->second;
	    float gain = num_params.find("frame17:tp_pot60_gain_mean")->second;
	    vcth = (int)(ped - 0.5 * gain);
	}

	vector<uint32_t> fscbuf_data;
	int NFRAMES(1);
	if( !_CBC3_0 ) NFRAMES = 3;
	int FRAME_LENGTH(38*2);//76
	unsigned N = NFRAMES * FRAME_LENGTH + 50;
	if(N>60000){ 
	    sprintf( tmp, "%s The number of frames %d is too large for the raw data buffer(60000).", test_name.c_str(), N ); 
	    throw WaferTestException( tmp, result | WT_ERR_OTHER );
	}
	unsigned n = N/2;
	uint32_t *rawdata(0);
	int *slvs6(0);
	rawdata = new uint32_t[N];
	slvs6 = new int[n];

	string config_fname = _setting_file_dir + settings["run_config_file"];
	if(_run_config.compare(config_fname)!= 0){
	    _cbc3be->WriteBeBoardConfig(config_fname);
	    _cbc3be->InitializeRun();
	    _run_config = config_fname;
	}

	_cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 
	_cbc3be->InitializeRun();

	if(_debug){
		_cbc3be->ReadAllCbcConfig(_cbc_id);
		_cbc3be->PrintCbcI2cReplies(_cbc_id, _os);
		map<string,uint32_t> regmap;
		_cbc3be->ReadBeBoardConfig(config_fname, regmap);
		map<string,uint32_t>::iterator it=regmap.begin();
		for(; it != regmap.end(); it++) _os << it->first << " " << it->second << endl;
		system("cbc3daq 'ipbus read node=cs_cnfg.*'");
	}

	sprintf(tmp, "%s_dll_scan.txt", test_name.c_str() );
	string ofname(tmp);
	sprintf(tmp, "%s:dll_scan_file", test_name.c_str() );
	_parameters.set(tmp, ofname);
	ofstream ofile(ofname);
	sprintf(tmp, "# Title: DLL scan\n");
	ofile << tmp;
	sprintf(tmp, "$ Fields: %10s %20s %20s\n", "DLL", "L1dataHeaderPos.", "StubDataPos.");


//	_os << "dll loop" << endl;
//	unsigned dll_reg_val[26] = { 128, 144, 136, 152, 132, 148, 140, 156, 130, 146, 138, 154, 134, 150, 142, 158, 129, 145, 137, 153, 133, 149, 141, 157, 131, 147 }; 
	//unsigned value = 0;
	int *hp, *last_hp;
	int *sp, *last_sp;
	hp = new int[NFRAMES];
	last_hp = new int[NFRAMES];
	sp = new int[NFRAMES];
	last_sp = new int[NFRAMES];

	for(int i = 0; i < 26; i++){

	    for(int ii=0; ii < NFRAMES; ii++){
		hp[ii] = 0; last_hp[ii] = 0;
		sp[ii] = 0; last_sp[ii] = 0;
	    }

	    unsigned value = (unsigned)i;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", value, _cbc_id );
	    value = 17;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.amux", value, _cbc_id );
	    value = (unsigned)0;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );
	    value = (unsigned)1;
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, _cbc_id );
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePotentiometer.pot", pot, _cbc_id );
	    _cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, _cbc_id );

	    usleep(10);

	    fscbuf_data.clear();
	    fscbuf_data.push_back((unsigned) 0xa01f0000);
	    //fscbuf_data.push_back((unsigned) 0xa0011200);//fast reset & rdb recording
	    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	    fscbuf_data.push_back((unsigned) 0xabb00000);
	    fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	    fscbuf_data.push_back((unsigned) 0xa0010011);//test pulse request
	    fscbuf_data.push_back((unsigned) 0xa0010200);//rdb recording
	    fscbuf_data.push_back((unsigned) 0xa0210000);
	    //fscbuf_data.push_back((unsigned) 0xa0020100);//T1
	    unsigned T1_com = 0xa0000100 | (NFRAMES << 16);
	    fscbuf_data.push_back((unsigned) T1_com);//T1
	    fscbuf_data.push_back((unsigned) 0xa0050000);
	    fscbuf_data.push_back((unsigned) 0xe0000000);

	    _cbc3be->WriteFscBram(fscbuf_data);
	    _cbc3be->InitializeRun();
	    _cbc3be->StartFscBramController();
	    _cbc3be->WaitSequencerReady(1);

	    read_raw_data(test_name, N, rawdata, WT_ERR_DLL);

	    unsigned reg28(0); 
	    _cbc3be->rwCbcI2cReg( 1, 0, 1 ,28, reg28, _cbc_id );

	    if(_debug){
		sprintf(tmp, "--DLL (reg value, dll) = 0x%02x, %d)", reg28, i);
		_os << tmp << endl;
		raw_data_dump_data(rawdata, N, _os);
	    }

	    raw_data_get_slvs6(rawdata, slvs6, n);
	    for(int ii=0; ii < NFRAMES; ii++){

		last_hp[ii] = hp[ii];

		if(ii==0){
		    hp[ii] = raw_data_find_frame_header( slvs6, n, 0 );
		    if(hp[ii] == -1) 
			throw WaferTestException( test_name + " L1 data is not found.", WT_ERR_RAW_DATA | WT_ERR_OTHER | WT_ERR_DLL );
		    if(_debug) _os << "Header position " << hp << endl;
		    if( last_hp[ii] != 0 && hp[ii] != last_hp[ii]){
			oss.str("");
			oss << "L1 triggerd data frame header position changed at " << std::dec << "(reg val = " << reg28 << ")" << i << " from " << last_hp[ii] << " to " << hp[ii];
			print_test(test_name, oss.str(), _os);
		    } 
		}
		else{
		    //cout << "offset : " << hp[ii-1] + FRAME_LENGTH/2 << endl;
		    hp[ii] = raw_data_find_frame_header( slvs6, n, hp[ii-1]+FRAME_LENGTH/2 );
		    if(hp[ii] == -1){ 
			raw_data_dump_data(rawdata, N, _os);
			sprintf( tmp, "%s L1 data is not found for frame[%d], DLL=%d", test_name.c_str(), ii, i);
			throw WaferTestException( tmp, WT_ERR_RAW_DATA | WT_ERR_OTHER | WT_ERR_DLL );
		    }
		    if(_debug) _os << "Header position " << hp[ii] << endl;
		    if( hp[ii] != hp[ii-1] + 38 ){
			oss.str("");
			oss << "L1 triggerd data frame [" << std::dec << ii << "] header position comes " << hp[ii] - hp[ii-1] << " after frame[" << ii-1 << "] ";
			print_test(test_name, oss.str(), _os);
			result |= WT_ERR_DLL;
		    }
		    if( last_hp[ii] != 0 && hp[ii] != last_hp[ii]){
			oss.str("");
			oss << "L1 triggerd data frame header position changed at " << std::dec << "(reg val = " << reg28 << ")" << i << " from " << last_hp[ii] << " to " << hp[ii] << " for frame " << ii;
			print_test(test_name, oss.str(), _os);
		    } 
		}
		vector<int> saddr;
		vector<int> sbend;
		last_sp[ii] = sp[ii];
		if(ii==0){
		    sp[ii] = raw_data_find_stubs( rawdata, n, saddr, sbend, 0 );
		    if(sp[ii] == -1){ 

			sprintf(tmp, "--DLL (reg value, dll) = (0x%02x, %d)", reg28, i);
			_os << tmp << endl;
			raw_data_dump_data(rawdata, N, cerr);
			/*
			   _cbc3be->ReadAllCbcConfig(_cbc_id);
			   _cbc3be->PrintCbcI2cReplies(_cbc_id, ofile);
			   _cbc3be->DumpBeBoardConfig(ofile);
			   _cbc3be->DumpBeBoardStatus(ofile);
			   */
			throw WaferTestException( test_name + " Stub data is not found.", WT_ERR_RAW_DATA | WT_ERR_OTHER | WT_ERR_DLL );
		    }
		    if(_debug) _os << "SP " << sp << endl;
		    if( last_sp[ii] != 0 && sp[ii] != last_sp[ii] ){
			oss.str("");
			oss << "Stub position changed at " << std::dec << "(reg val = " << reg28 << ")" << i << " from " << last_sp[ii] << " to " << sp[ii];
			print_test(test_name, oss.str(), _os);
		    }
		    sprintf(tmp, "%d %d %d\n", i, hp[ii], sp[ii]);
		    ofile << tmp;
		}
		else{
		    sp[ii] = raw_data_find_stubs( rawdata, n-(sp[ii-1]+2), saddr, sbend, sp[ii-1]+2 );
		}
	    }
	}
	delete []rawdata;
	delete []slvs6;
	delete []hp;
	delete []last_hp;
	delete []sp;
	delete []last_sp;

	ofile.close();

	return result;
    }
    int WaferTest::frame28( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);
	char tmp[256];
	stringstream oss;

	int pause_vcthscan(1);
	if(settings.find("pause_vcthscan")!= settings.end()) pause_vcthscan = strtol(settings["pause_vcthscan"].c_str(), 0, 0);
	int pause_vplus_scan(0);
	if(settings.find("pause_vplus_scan")!= settings.end()) pause_vplus_scan = strtol(settings["pause_vplus_scan"].c_str(), 0, 0);
	int pause_cal_vcasc_scan(0);
	if(settings.find("pause_cal_vcasc_scan")!= settings.end()) pause_cal_vcasc_scan = strtol(settings["pause_cal_vcasc_scan"].c_str(), 0, 0);
	
	const std::map<std::string, std::string> &str_params = _parameters.get_str_params();
	string ofst_file;
	if( str_params.find("frame16:cbci2c_offset_file") == str_params.end() ){
	    if( settings.find("cbci2c_offset_file") == settings.end() ){
		print_test(test_name, "offset file name is not found in the parameters nor settings.", _os);
		return WT_ERR_OFFSET_TUNING;
	    }
	    else{
		ofst_file = settings["cbci2c_offset_file"];
	    }
	}
	else{
	    ofst_file = str_params.find("frame16:cbci2c_offset_file")->second;
	}
	_os << "OFFSET FILE NAME " << ofst_file << endl;
	if(_cbci2c_page2.compare(ofst_file) != 0 ){
            print_test(test_name, string("offset file ") + ofst_file + " is written.", _os);  
	    _cbc3be->rwCbcI2cRegs( 1, 1, ofst_file, _cbc_id );
	    _cbci2c_page2 = ofst_file;
	}
	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	unsigned value(0);

	int pa[] = {0,1,2,5};
	vector<int> pause(pa, pa + sizeof(pa)/sizeof(int));

	//---------------
	//VCTH scan
	//---------------
	bool inc(true);
	for(unsigned i=0; i < pause.size(); i++){
	    if(!_tune && (pause.at(i) != pause_vcthscan)) continue;
	    map<unsigned,float> vcth_meas;
	    sprintf(tmp, "frame28_vcth_scan_pause_%1.2fms.txt", pause.at(i) * 0.1 );
	    string fname(tmp);
	    sprintf( tmp, "%s:vcth_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
	    _parameters.set(tmp, fname);
	    vcth_scan(test_name, fname, vcth_meas, _os, true, inc, pause.at(i));
	    inc = inc ? false : true;
	}
	value = 0;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, _cbc_id );
	//---------------
	//VPLUS1 scan
	//---------------
	inc = true;
	for(unsigned i=0; i < pause.size(); i++){
	    if(!_tune && (pause.at(i) != pause_vplus_scan)) continue;
	    map<unsigned,float> vplus1_meas;
	    sprintf(tmp, "frame28_vplus1_scan_pause_%1.2fms.txt", pause.at(i) * 0.1 );
	    string fname(tmp);
	    sprintf( tmp, "%s:vplus1_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
	    _parameters.set(tmp, fname);
	    vplus_scan(test_name, fname, true, 0x0f, vplus1_meas, _os, true, inc, pause.at(i));
	    inc = inc ? false : true;
	}
	value=7;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VPLUS.vplus1", value, _cbc_id );
	//---------------
	//VPLUS2 scan
	//---------------
	inc = true;
	for(unsigned i=0; i < pause.size(); i++){
	    if(!_tune && (pause.at(i) != pause_vplus_scan)) continue;
	    map<unsigned,float> vplus2_meas;
	    sprintf(tmp, "frame28_vplus2_scan_pause_%1.2fms.txt", pause.at(i) * 0.1 );
	    string fname(tmp);
	    sprintf( tmp, "%s:vplus2_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
	    _parameters.set(tmp, fname);
	    vplus_scan(test_name, fname, false, 0xf7, vplus2_meas, _os, true, inc, pause.at(i));
	    inc = inc ? false : true;
	}
	value=7;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VPLUS.vplus2", value, _cbc_id );
	//---------------
	//CAL_VCASC scan
	//---------------
	inc = true;
	for(unsigned i=0; i < pause.size(); i++){
	    if(!_tune && (pause.at(i) != pause_cal_vcasc_scan)) continue;
	    map<unsigned,float> cal_vcasc_meas;
	    sprintf(tmp, "frame28_cal_vcasc_scan_pause_%1.2fms.txt", pause.at(i) * 0.1 );
	    string fname(tmp);
	    sprintf( tmp, "%s:cal_vcasc_scan_file_pause_%1.2fms", test_name.c_str(), pause.at(i) * 0.1); 
	    _parameters.set(tmp, fname);
	    cal_vcasc_scan( test_name, fname, cal_vcasc_meas, _os, true, inc, pause.at(i));
	    inc = inc ? false : true;
	}
	value = 63;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "CAL_Ibias.volt", value, _cbc_id );

	value = 0; 
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.amux", value, _cbc_id );

	return result;
    }
    int WaferTest::frame29( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);

	int pause_curr_scan(10);
	if(settings.find("pause_curr_scan")!= settings.end()) pause_curr_scan = strtol(settings["pause_curr_scan"].c_str(), 0, 0);

	const std::map<std::string, std::string> &str_params = _parameters.get_str_params();
	string ofst_file;
	if( str_params.find("frame16:cbci2c_offset_file") == str_params.end() ){
	    if( settings.find("cbci2c_offset_file") == settings.end() ){
		print_test(test_name, "offset file name is not found in the parameters nor settings.", _os);
		return WT_ERR_OFFSET_TUNING;
	    }
	    else{
		ofst_file = settings["cbci2c_offset_file"];
	    }
	}
	else{
	    ofst_file = str_params.find("frame16:cbci2c_offset_file")->second;
	}
	_os << "OFFSET FILE NAME " << ofst_file << endl;
	if(_cbci2c_page2.compare(ofst_file) != 0 ){
            print_test(test_name, string("offset file ") + ofst_file + " is written.", _os);  
	    _cbc3be->rwCbcI2cRegs( 1, 1, ofst_file, _cbc_id );
	    _cbci2c_page2 = ofst_file;
	}
	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)!=0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}

	int pa[] = {0, 1, 5, 10, 15, 20};
	vector<int> pause(pa, pa + sizeof(pa)/sizeof(int));

	bool inc(true);
	for(unsigned i=0; i < pause.size(); i++){
	    if(!_tune && (pause.at(i) != pause_curr_scan)) continue;
	    curr_scan(test_name, _os, true, inc, pause.at(i));
	    inc = inc ? false : true;
	}
	return result;
    }
    int WaferTest::frame30( const std::string &test_name, std::map<std::string, std::string> &settings){

	int result(0);

	ostringstream oss;

	const std::map<std::string, std::string> &str_params = _parameters.get_str_params();
	string ofst_file;
	if( str_params.find("frame16:cbci2c_offset_file") == str_params.end() ){
	    if( settings.find("cbci2c_offset_file") == settings.end() ){
		print_test(test_name, "offset file name is not found in the parameters nor settings.", _os);
		return WT_ERR_OFFSET_TUNING;
	    }
	    else{
		ofst_file = settings["cbci2c_offset_file"];
	    }
	}
	else{
	    ofst_file = str_params.find("frame16:cbci2c_offset_file")->second;
	}
	//cout << "cbci2c_page2 " << _cbci2c_page2 << endl; 
	if(_cbci2c_page2.compare(ofst_file) != 0 ){
	    _cbc3be->rwCbcI2cRegs( 1, 1, ofst_file, _cbc_id );
	    _cbci2c_page2 = ofst_file;
	}

	string cbci2c_fname = _setting_file_dir + settings["cbci2c_file"];
	if(_cbci2c_page1_main.compare(cbci2c_fname)!=0){
	    _cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_fname, _cbc_id );
	    _cbci2c_page1_main = cbci2c_fname;
	}
	_cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
	usleep( 2000 );
	
	//vcth setting
	unsigned value = 400;
	_cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, _cbc_id );

	
	int sample_count(20);
	int stat_start_s(5);
	//1 measurement takes 4 ms
	vector<string> bias;
	vector<int> bias_index;

	unsigned amux_reg_addr = 15; 

	bias.push_back("IHYST");
	bias_index.push_back(14);
	value = 14;
	_cbc3be->rwCbcI2cReg(1,1,1,amux_reg_addr, value); 
	usleep(100000);
	usleep(100000);
	usleep(100000);
	usleep(100000);
	_kmm->set_DC_measurement("0.2", "1", sample_count, 1, false);
	_kmm->kmm_printf(":INIT");
	//keithley read
	_kmm->kmm_read_ndata(sample_count); 
	//cout << "kmm nread = " << nread << endl;
	vector<float> vmeas_ihyst = _kmm->get_kmm_vdata();
	bool ok_ihyst = write_amux_values( vmeas_ihyst, bias, sample_count, test_name, stat_start_s );


	bias.clear();
	bias_index.clear();
	bias.push_back("Vpafb");
	bias_index.push_back(8);
	value = 8;
	_cbc3be->rwCbcI2cReg(1,1,1,amux_reg_addr, value); 
	sleep(1);
	usleep(100000);
	usleep(100000);
	usleep(100000);
	usleep(100000);

	_kmm->set_DC_measurement("0.2", "1", sample_count, 1, false);
	_kmm->kmm_printf(":INIT");
	//keithley read
	_kmm->kmm_read_ndata(sample_count); 
	//cout << "kmm nread = " << nread << endl;
	vector<float> vmeas_vpafb = _kmm->get_kmm_vdata();
	bool ok_vpafb = write_amux_values( vmeas_vpafb, bias, sample_count, test_name, stat_start_s );

	if(!(ok_ihyst && ok_vpafb)) result = WT_ERR_AMUX_BIAS;

	_kmm->set_DC_measurement("0.6", "10", sample_count, 15, true);
	_kmm->kmm_printf(":INIT");

	bias.clear();
	bias_index.clear();
	bias.push_back("IBIAS");
	bias.push_back("ICOMP");
	bias.push_back("CAL_I");
	bias.push_back("IPSF");
	bias.push_back("CAL_VCASC");
	bias.push_back("VCTH");
	bias.push_back("PRE1");
	bias.push_back("VPLUS1");
	bias.push_back("VPLUS2");
	bias.push_back("VBG_LDO");
	bias.push_back("IPRE2");
	bias.push_back("IPA");
	bias.push_back("NC50");
	bias.push_back("VBGBIAS");
	bias.push_back("IPAOS");

	bias_index.push_back(4);
        bias_index.push_back(13);
        bias_index.push_back(3);
        bias_index.push_back(11);
        bias_index.push_back(15);
        bias_index.push_back(5);
        bias_index.push_back(10);
        bias_index.push_back(17);
        bias_index.push_back(16);
        bias_index.push_back(7);
        bias_index.push_back(2);
        bias_index.push_back(1);
        bias_index.push_back(9);
        bias_index.push_back(6);
        bias_index.push_back(12);
	result |= read_amux(bias, bias_index, sample_count, test_name, stat_start_s);

	return result;
    }
    int WaferTest::test( const string &test_name, std::map<std::string, std::string> &settings ){

	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	_current_test = test_name;
	int result(0);
	if(test_name == "frame03"){
	    result = frame03(test_name, settings);
	}
	else if(test_name == "frame05"){
	    print_test(test_name, "Stuck bits test - main bias settings.", _os);
	    result = cbci2c_write_read_test(test_name, settings);
	}
	else if(test_name == "frame06"){
	    result = frame06(test_name, settings);
	}
	else if(test_name == "frame07"){
	    print_test(test_name, "Stuck bits test - main bias settings.", _os);
	    result = cbci2c_write_read_test(test_name, settings);
	}
	else if(test_name == "frame08"){
	    print_test(test_name, "Main bias settings is set to default values.", _os);
	    result = cbci2c_write_read_test(test_name, settings);
	}
	else if(test_name == "frame10"){
	    print_test(test_name, "Current check - criteria [VDDD<52mA and VLDOI<200mA].", _os);
	    result = frame10(test_name, settings);
	}
	else if(test_name == "frame11"){
	    print_test(test_name, "Raw data check", _os);
	    result = frame11(test_name, settings);
	}
	else if(test_name == "frame13_a"){
	    print_test(test_name, "Stuck bits test - bend LUTs, channel offsets, channel mask 1", _os);
	    result = cbci2c_write_read_test(test_name, settings);
	}
	else if(test_name == "frame13_b"){
	    print_test(test_name, "Stuck bits test - bend LUTs, channel offsets, channel mask 0", _os);
	    result = cbci2c_write_read_test(test_name, settings);
	}
	else if(test_name == "frame14"){
	    print_test(test_name, "Bandgap tune, bandgap register and chip ID fuse blowing", _os);
	    result = frame14(test_name, settings);
	}
	else if(test_name == "frame15"){
	    print_test(test_name, "After fuse blowing check", _os);
	    result = frame15(test_name, settings);
	}
	else if(test_name == "frame16"){
	    print_test(test_name, "Offset tuning", _os);
	    result = frame16(test_name, settings);
	}
	else if(test_name == "frame17"){
	    print_test(test_name, "VCTH scan and s-curve analysis", _os);
	    result = frame17(test_name, settings);
	}
	else if(test_name == "frame18"){
	    print_test(test_name, "Pipeline check - check all pipeline address, error bits, and all channel data.", _os);
	    result = frame18(test_name, settings);
	}
	else if(test_name == "frame19"){
	    print_test(test_name, "Buffer check - check all error bits, and all channel data.", _os);
	    result = frame19(test_name, settings);
	}
	else if(test_name == "frame20"){
	    print_test(test_name, "Channel masking check", _os);
	    result = frame20(test_name, settings);
	}
	else if(test_name == "frame21"){

	    //This may not be necessary.
	    _cbc3be->CbcReset();//Mark's proceduer
	    _cbci2c_page1_main = "default";
	    _cbci2c_page2      = "default";
	    //CBC I2C BUS INITIALIZATION AND CONFIGURATION
	    _cbc3be->CbcI2cBusInit();

	    if(_i2c_bg){
		unsigned value = 1;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.override", value, 1 );
		//setting to read fused bangap setting.
		value = 0;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.sel", value, 1 );
		const std::map<std::string, float> &num_params = _parameters.get_num_params();
		if(num_params.find("frame14:best_bg_i2c") == num_params.end() ){
		    throw WaferTestException( "best_bg_i2c is not found in the parameter map.", WT_ERR_OTHER ); 
		}
		value = num_params.find("frame14:best_bg_i2c")->second;
		_cbc3be->rwCbcI2cRegsByName( 0, 1, "BandgapFuse.bg_fuse", value, _cbc_id );
	    }

	    print_test(test_name, "Stub address and bend check", _os);
	    result = frame21(test_name, settings);
	}
	else if(test_name == "frame22"){

	    print_test(test_name, "cluster width discrimination and layer swap check", _os);
	    result = frame22(test_name, settings);
	}
	else if(test_name == "frame23"){

	    print_test(test_name, "pT discrimination check", _os);
	    result = frame23(test_name, settings);
	}
	else if(test_name == "frame24"){

	    print_test(test_name, "pT window offset check", _os);
	    result = frame24(test_name, settings);
	}
	else if(test_name == "frame25"){

	    print_test(test_name, "Hit detection and HIP suppression logic check", _os);
	    result = frame25(test_name, settings);
	}
	else if(test_name == "frame26"){

	    print_test(test_name, "FCI delay check", _os);
	    result = frame26(test_name, settings);
	}
	else if(test_name == "frame27"){

	    print_test(test_name, "DLL check", _os);
	    result = frame27(test_name, settings);
	}
	else if(test_name == "frame28"){
	    print_test(test_name, "Sweep and measure bias voltages via Analogue Mux", _os);
	    result = frame28(test_name, settings);
	}
	else if(test_name == "frame29"){
	    print_test(test_name, "Sweep and measure bias currents via Analogue Mux", _os);
	    result = frame29(test_name, settings);
	}
	else if(test_name == "frame30"){
	    print_test(test_name, "Measure Analogue Mux parameters", _os);
	    result = frame30(test_name, settings);
	}

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	print_delta_t( test_name, dt, _os );
	char tmp[256];
	sprintf( tmp, "RESULT = 0x%08x", result);
	print_test(test_name, tmp, _os);
	print_test(test_name, "END-----------------------------", _os);

	return result;
    }

    void WaferTest::print_wafer_test(){
	if(_kmm){
	    struct timeval print_sumdt = _kmm->print_sumdt();
	    struct timeval read_sumdt = _kmm->read_sumdt();
	    print_delta_t( "Keithley print", print_sumdt, _os);
	    print_delta_t( "Keithley read", read_sumdt, _os);
	}
	_parameters.dump(_os);
    }
}




