#include "Fc7SystemRegInterface.h"
#include "ipbus_utils.h"
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace ipbutl;

namespace fc7{

    std::string board_id( uint32_t value ){ 

	char cvalue[5];
	for( int i=0; i<4; i++ ){
	    cvalue[i] = ( value >> (( 3 - i ) * 8) ) & 0xFF;
	}
	cvalue[4] = 0;
	return std::string( cvalue );
    }

    std::string rev_id( uint32_t value ){ 

	char cvalue[5];
	for( int i=0; i<4; i++ ){
	    cvalue[i] = ( value >> ( 3 - i ) * 8 ) & 0xFF;
	}
	cvalue[4] = 0;
	return std::string( cvalue );
    }
    std::string version( uint32_t value ){

	ostringstream oss;
	oss 
	    << ( value >> 28 ) << "." 
	    << ( ( value >> 24 ) & 0xF ) << "." 
	    << ( ( value >> 16 ) & 0xFF ) 
	    << ", date: " 
	    << setfill( '0' )
	    << setw(2) << ( value & 0x1F ) << "."
	    << setw(2) << ( ( value >> 5 ) & 0xF ) << "."
	    << setw(2) << ( ( value >> 9 ) & 0x7F ); 
	return std::string( oss.str() );
    }
    std::string read_board_id( uhal::HwInterface *hw ){
	return board_id(read_ipbus(hw,"sysreg.board_id"));
    }
    std::string read_rev_id( uhal::HwInterface *hw ){
	return rev_id(read_ipbus(hw,"sysreg.rev_id"));
    }
    std::string read_version( uhal::HwInterface *hw ){
	return version(read_ipbus(hw,"sysreg.version"));
    }
    std::string ctrl_config( uint32_t value ){

	ostringstream oss;
	int i(0);
	oss << setw(20) << setfill(' ') << left << "+ pcieclk_pll_sel" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ pcieclk_mr" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ pcieclk_fsel1" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ pcieclk_fsel0" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ cdce_powerup" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ cdce_refsel" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ cdce_sync" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	oss << setw(20) << setfill(' ') << left << "+ cdce_ctrl_sel" << ": ";
	oss << ( value >> i++ & 1 ) << endl;
	return std::string( oss.str() );
    }

    void print_version( uhal::HwInterface *hw, std::ostream &os ){

	os << setw(10) << "[board_id] " << read_board_id(hw)   << endl;
	os << setw(10) << "[rev_id]   " << read_rev_id(hw)     << endl;
	os << setw(10) << "[version]  " << read_version(hw)    << endl;
    }

    void dump_config( uhal::HwInterface *hw, std::ostream &os ){

	os << "FC7 system configurations" << endl;
	os << ( "---------------------------------------------------------" ) << endl;
	os << ctrl_config( read_ipbus(hw,"sysreg.ctrl") );
	os << ( "---------------------------------------------------------" ) << endl;
    }
}
