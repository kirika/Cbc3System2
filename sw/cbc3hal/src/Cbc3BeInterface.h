#ifndef __CBC3BEBINTERFACE_H__
#define __CBC3BEBINTERFACE_H__

#include <string>
#include <map>
#include <vector>
#include <stdint.h>
#include <string>
#include <set>
#include "analysis_utils.h"
#include <exception>

class TH1F;

namespace cbc3hal{

        typedef uint32_t CBCI2C_REPLY;
        typedef CBCI2C_REPLY CBCI2C_REGID;
	typedef std::map<CBCI2C_REGID, CBCI2C_REPLY> CBCI2C_REGS;
	struct cbc_i2c_command;

	typedef unsigned CBC_ID;
	typedef unsigned GROUP_ID;
	typedef unsigned CHANNEL_ID;

	typedef std::string CBCI2C_TYPE;
	typedef std::string CBCI2C_ITEM;
	typedef unsigned    CBCI2C_DATA;
	typedef unsigned    CBCI2C_ADDR;

	typedef std::map<CBCI2C_TYPE,  std::set<CBCI2C_ITEM> >         CBCI2C_NAMES;
	typedef std::pair<CBCI2C_ITEM,  CBCI2C_DATA>               CBCI2C_ITEM_DATA;
	typedef std::map<CBCI2C_ITEM,  CBCI2C_DATA>            CBCI2C_ITEM_DATA_MAP;
	typedef std::pair<CBCI2C_TYPE, CBCI2C_ITEM_DATA_MAP>       CBCI2C_TYPE_DATA;
	typedef std::map<CBCI2C_TYPE, CBCI2C_ITEM_DATA_MAP>    CBCI2C_TYPE_DATA_MAP;
	typedef std::map<CBCI2C_ADDR,  CBCI2C_DATA >           CBCI2C_ADDR_DATA_MAP;

	typedef std::map<GROUP_ID, std::set<CHANNEL_ID> > GROUP_CHANNELSET_MAP;

	class BeEventDataPacketEncoder;

	class Cbc3BeException : public std::exception{
	    public:
		enum ERR_TYPES { DAQ_ERR, SEQ_ERR };
	    public:
		Cbc3BeException()throw(){}
		Cbc3BeException( const std::string &estr, int type )throw(){err_str = estr; _type = type; }
		Cbc3BeException(const Cbc3BeException &e)throw(){ err_str = e.what(); }
		Cbc3BeException& operator= (const Cbc3BeException &e) throw(){ err_str = e.what(); _type = e.type(); return *this; }
		virtual ~Cbc3BeException()throw(){}
		virtual const char * what() const throw(){ return err_str.c_str(); }
		virtual int type() const { return _type; }
	    private:
		std::string err_str;
		int _type;
	};

	class Cbc3BeInterface{
	    public:
	     enum DUMP_LEVEL { DUMP_LEVEL_DEBUG, DUMP_LEVEL_NORMAL };
	     enum { DEBUG_CBC3BEINTERFACE_BIT=0, DEBUG_CBCI2C_BIT=1, DEBUG_PARAM_TUNING_BIT=2 };
	    public:
		Cbc3BeInterface( const std::string& board_name, bool CBC3_0 = true, int debug = 0 )
		    : fBoardName(board_name), 
		    fCBC3_0(CBC3_0),
		    fDataBufferWordSize(65536),
		    fWaitEvents(true),
		    fDebug(debug),
		    fCbcConfigValid(false), 
		    fDataReadyFlagPollingInterval_usec(1), 
		    fMinNumWordToWait(1), 
		    fNumNextDataWords(0), 
		    fDataBuffer(0),
		    fWriteCbcI2cRegsToDB(false)
		    {;}
		virtual ~Cbc3BeInterface(){ delete fDataBuffer;}
		virtual void Initialize( const std::string& connection_file, const std::string& id )=0;
		/*
		 *   This procedure ensure the CBC to be ready by the default setting, I2C communication.  
		 *   If the trigger is started and L1A is sent, the corresponding data should be received 
		 *   at the backend board, the event data should be stored in the data buffer. 
		 */
		virtual void ConfigureBeBoard( const std::string &be_config_fname, int fci_delay = 0 );
		virtual bool ConfigureCbcs( const std::string &cbci2creg_filelist_fname );
		virtual void ReadAllCbcConfig(CBC_ID cbc_id = 0); 
		virtual void ReadCbcConfig( unsigned cbc_id, const std::string cbci2creg_file="" );

		virtual void WriteCbcI2cRegsToDB(bool flag=true){ fWriteCbcI2cRegsToDB = flag; }
		virtual void WriteBeBoardConfig( const std::string &node, uint32_t value )=0;
		virtual void WriteBeBoardConfig( std::map<std::string, uint32_t>&regmap )=0;
		virtual void WriteBeBoardConfig( const std::string &setting_file )=0;
		virtual void WriteFscBram( const std::vector<uint32_t> &data )=0;
		virtual void WriteFscBram( const std::string &file )=0;
		virtual void ReadBeBoardConfig( const std::string &fname, std::map<std::string,uint32_t> &regmap )=0;
		virtual unsigned ReadBeBoardRegister( std::string node )=0;
		virtual void ReadBeBoardRegisters( std::vector<std::string> &regnodes, std::vector<uint32_t> &values  )=0;
		virtual void DumpBeBoardConfig( std::ostream& os, int level = DUMP_LEVEL_NORMAL )=0;
		virtual void DumpBeBoardStatus( std::ostream& os, int level = DUMP_LEVEL_NORMAL )=0;
		virtual void ReadBeCbcConfig()=0;
		virtual bool CbcI2cBusInit()=0; 
		virtual bool ScanDataClockTiming(unsigned fe_id=1)=0;
		virtual bool TuneDataClockTiming(bool scan=false)=0;

		virtual const CBCI2C_NAMES &getCbcI2cRegNames()const=0;
		virtual CBCI2C_TYPE_DATA_MAP readCbcAllI2cRegsByType( unsigned cbc_id=1 )=0;
		virtual bool rwCbcI2cReg(struct cbc_i2c_command &command )=0;
		virtual bool rwCbcI2cRegs(const std::vector<struct cbc_i2c_command> &commands )=0;
		/* cbc_id = 0 is not supported for this function */
		virtual bool rwCbcI2cRegs(unsigned r, unsigned w, const std::string &filename, unsigned cbc_id=1 )=0;
		/* cbc_id = 0 is not supported for this function */
		virtual bool rwCbcI2cRegsByName(unsigned r, unsigned w, const std::string &name, unsigned &value, unsigned cbc_id=1 )=0;
		virtual bool rwCbcI2cRegsByType(unsigned r, unsigned w, CBCI2C_TYPE_DATA &type_data, unsigned cbc_id=1 )=0;
		/* cbc_id can be 0 for this function */
		virtual bool rwCbcI2cRegsByType(unsigned r, unsigned w, std::map<unsigned, CBCI2C_TYPE_DATA> &cbc_id_type_data )=0;
		virtual bool writeAllCbcI2cRegsByType( const std::string &type, CBCI2C_ITEM_DATA_MAP &item_data )=0;
		/* cbc_id = 0 is not supported for rwCbcI2c_Offset(s) */
		virtual bool rwCbcI2c_Offset( unsigned r, unsigned w, unsigned ch, unsigned &offset, unsigned cbc_id=1 )=0;
		virtual bool rwCbcI2c_Offsets( unsigned r, unsigned w, std::vector<unsigned> &offsets, unsigned cbc_id=1 )=0;
		virtual bool rwCbcI2c_Offsets( unsigned r, unsigned w, std::map<unsigned,unsigned> &chofst_map, unsigned cbc_id=1 )=0;

		virtual std::vector<CBCI2C_REPLY> rwCbcI2c_reg( unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value=0, unsigned cbc_id=1 )=0; 
		/* fe_id = 0 is not supported for this function */
		virtual std::vector<CBCI2C_REPLY> rwCbcI2c_regs( const std::vector<uint32_t> &commands, int nreplies = -1, unsigned fe_id=1 )=0; 
		/* cbc_id = 0 is not supported for this function */
		virtual std::vector<CBCI2C_REPLY> rwCbcI2c_regs( unsigned r, unsigned w, const std::string &filename, unsigned cbc_id=1 )=0; 

		virtual void CbcReset( int fe_id=-1 )=0; 
		virtual void SendFastSignal( std::string type )=0; 
		virtual void PrintVersion( std::ostream &os )=0;
		/*
		 * To read data, 
		 * call DataReadyFlagPollingInterval_usec() to set polling interval other than the default value. 
		 * call MinNumWordToWait() to set the min. # of words to read at once.
		 * call WaitData(timeout_sec, usec) in while loop and if it returns true, call ReadData()
		 */
		virtual bool HasData( int min_nword = 0 )=0;
		virtual bool WaitData(unsigned timeout_sec = 0, bool usec = false )=0;
		virtual int ReadDataBufferAll( uint32_t*& Data )=0;
		virtual int ReadData( uint32_t*& Data, int nword=0 )=0;
		virtual std::vector<uint32_t> ReadData()=0;
		virtual void InitializeRun()=0;
		virtual void FinalizeRun()=0;
		virtual void StopTrigger()=0;
		virtual void StartTrigger()=0;
		virtual void ResetFscBramController()=0;
		virtual void StartFscBramController()=0;
		virtual bool WaitSequencerReady(int timeout_usec=1)=0;
		virtual void StartFastSignalGenerator()=0;
		virtual void StopFastSignalGenerator()=0;

		virtual int DisableGroupOffsets( unsigned group, unsigned cbc_id=0);
		virtual int ActivateGroupOffsets( unsigned group, const std::map<CHANNEL_ID, unsigned> &offsets_map, CBC_ID cbc_id );
		virtual int ActivateGroupOffsets( unsigned group, const std::map<unsigned, std::vector<unsigned> > &offsets0, unsigned cbc_id=0 );
		virtual int ActivateGroupOffsets( unsigned group, const std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > &offsets_map, CBC_ID cbc_id=0 );
		/* 
		 * Tunings and Scanning functions.
		 * fast_signal_generator is supposed to be used and configured beforehand.  
		 */ 
		/* 
		 * VCTH with the occupancy is found for each active CBC taking the mean for all groups.  If occupancy is set to 0, minimum VCTH which give a hit is returned. 
		 * The result is set to the CBC in the end. 0 is returned if it is successful, -1 othrwise. 
		 * The offsets  are initialized to 0xFF (deactivated) in the beginning of the function if offsets_map = 0 or init_offsets = 1
		 * In the end, all the offsets are still 0xFF.
		 * Ncycle is overwritten by sample_count, where sample_count is the # of events for each parameter measurement.
		 */
		virtual int FindVcth( int sample_count = 100, double occ = 0.5, const std::map<CBC_ID, std::vector<unsigned> > *offsets_map=0, bool init_offsets=1 );
		/* 
		 * VCTH bisection scan for the occupancy for the group in each active CBC. 
		 * VCTH tuning only. Other (offsets, group) settings must be done outside this function. 
		 * the g_vcth_data is a map of pair of channel and param_tuning_item.
		 */
		virtual int ScanVcthBisection( GROUP_ID group, std::map<CBC_ID, unsigned> &g_vcth, int sample_count, double occ = 0.5 );
		/*
		 * VCTH tuning only. Other (offsets, group) settings must be done outside this function. 
		 * if desc == true, VCTH is scanned from large number to smaller number.
		 * 
		 */
//		virtual int ScanVcth( GROUP_ID group, int sample_count, std::map<std::string, TH1F *> &hmap, bool desc = false, int vcth_min = -1, int vcth_max = -1 );
		virtual int ScanVcth( GROUP_ID group, int sample_count, CbcHistList<TH1F> &hlist, bool desc = false, int vcth_min = -1, int vcth_max = -1 );

		/*
		 * Ncycle is overwritten by sample_count, where sample_count is the # of events for each parameter measurement.
		 * Offsets should be initialized to 0xFF before calling this function. the offset values are 0xFF when this function finishes, but offsets_map is filled
		 * with the tuned values.
		 */
		virtual int TuneOffsets( CBC_ID cbc_id, GROUP_ID group, int sample_count, std::map<CHANNEL_ID, unsigned> &offsets_map );

		virtual int ReadRawData( uint32_t*& Data, int nword, bool dcheck=false )=0;

		unsigned GetBeId();
		unsigned GetNFes();
		unsigned GetNCbcs();
		const std::map<unsigned,unsigned> &GetNFeCbcs();
		bool IsCbcActive(unsigned cbc_id);
		const std::map<unsigned,bool>* GetCbcActiveFlags();
		const std::map<unsigned,unsigned>* GetCbcFeIds();
		unsigned GetNActiveCbcs();
		void SetWaitEvents(bool events=true){ fWaitEvents = events; }
		void SetDebug( int debug ){ fDebug = debug; }
		void DataReadyFlagPollingInterval_usec(unsigned value){ fDataReadyFlagPollingInterval_usec = value; }
		void MinNumWordToWait(int value){ fMinNumWordToWait = value; }
		void ClearCbcI2cRegReplies(){ fCbcI2cRegReplies.clear(); }
		int GetMinNumWordToWait()const{ return fMinNumWordToWait; }
		int  DataBufferSize()const{ return fDataBufferSize; }
		const CBCI2C_REGS &GetCbcI2cRegReplies()const{ return fCbcI2cRegReplies; }
		int GetNumNextDataWords()const{ return fNumNextDataWords; }
		int GetDataBufferWordSize()const{ return fDataBufferWordSize; }
		void DumpCbcI2cReplies(std::ostream &os = std::cout)const;
		void PrintCbcI2cReplies( unsigned cbc_id, std::ostream &os )const;

		static void DumpData( const std::string &data_file_name, int level, int nevents=0, int offset=0, int buf_size = 1000 );
		static int  DumpData( const void *data, int &evt_count, int &evt_dump_count, int level, int nevents, int offset, int buf_size );
		static const std::set<unsigned>& GetChannelSet(unsigned group);
		static const GROUP_CHANNELSET_MAP &SetGroupChannelSetMap(unsigned ng = 8);
		static const GROUP_CHANNELSET_MAP &SetGroupChannelSetMap( const GROUP_CHANNELSET_MAP &gmap ){ fGroupChannelSetMap = gmap;  return fGroupChannelSetMap;}
		static GROUP_CHANNELSET_MAP InitGroupChannelSetMap(unsigned ng = 8);
		static const GROUP_CHANNELSET_MAP &GetGroupChannelSetMap(){ return fGroupChannelSetMap; }
	    protected:
		std::string fBoardName;
		bool        fCBC3_0;
		int      fDataBufferWordSize;
		bool     fWaitEvents;
		int      fDebug;
		bool     fCbcConfigValid;
		unsigned fBeId;
		unsigned fNFes;
		std::map<unsigned,unsigned> fNFeCbcs;
		unsigned fNCbcs;
		unsigned fNActiveCbcs;
		std::map<unsigned,bool> fCbcActiveFlags;
		std::map<unsigned,unsigned> fCbcFeIds;
		std::map<unsigned, std::vector<std::string> > fCbcConfigFiles;

		unsigned fDataReadyFlagPollingInterval_usec;
		int fMinNumWordToWait;
		int fNumNextDataWords;
		unsigned fDataBufferSize;//in word
		CBCI2C_REGS fCbcI2cRegReplies;
		uint32_t *fDataBuffer;
		static GROUP_CHANNELSET_MAP fGroupChannelSetMap;
		static const std::set<unsigned> fInvalidChannelSet;
		bool fWriteCbcI2cRegsToDB;
	};
}
#endif
