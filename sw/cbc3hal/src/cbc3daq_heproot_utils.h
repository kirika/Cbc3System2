#ifndef __CBC3DAQ_HEPROOT_UTILS_H__
#define __CBC3DAQ_HEPROOT_UTILS_H__

#include <TH1F.h>

double analyze_scurve( TH1F *h, double &mean, double &mean_e, double &sigma, double &sigma_e, double &gmin, double &gmax, double noise_lim = 4 );

#endif

