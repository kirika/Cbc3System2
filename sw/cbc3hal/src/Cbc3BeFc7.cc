#include "Cbc3BeFc7.h"
#include "Fc7SystemRegInterface.h"
#include "CbcI2cRegIpbusInterface.h"
#include "ipbus_utils.h"
#include <cstdio>
#include <iomanip>
#include <sys/time.h>
#include "uhal/uhal.hpp"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;
std::vector<string>& Cbc3BeFc7::Cbc3SystemConfigRegs()
{
    static std::vector<std::string> _nodes;
    if( _nodes.size() == 0 ){
	_nodes.push_back("cs_cnfg.global.be_id");
	_nodes.push_back("cs_cnfg.cbc01.active");
	_nodes.push_back("cs_cnfg.cbc01.id");
	_nodes.push_back("cs_cnfg.cbc01.fe_id");
	_nodes.push_back("cs_cnfg.cbc01.i2c_address");
	_nodes.push_back("cs_cnfg.cbc02.active");
	_nodes.push_back("cs_cnfg.cbc02.id");
	_nodes.push_back("cs_cnfg.cbc02.fe_id");
	_nodes.push_back("cs_cnfg.cbc02.i2c_address");
	_nodes.push_back("cs_cnfg.cbc03.active");
	_nodes.push_back("cs_cnfg.cbc03.id");
	_nodes.push_back("cs_cnfg.cbc03.fe_id");
	_nodes.push_back("cs_cnfg.cbc03.i2c_address");
	_nodes.push_back("cs_cnfg.cbc04.active");
	_nodes.push_back("cs_cnfg.cbc04.id");
	_nodes.push_back("cs_cnfg.cbc04.fe_id");
	_nodes.push_back("cs_cnfg.cbc04.i2c_address");
	_nodes.push_back("cs_cnfg.cbc05.active");
	_nodes.push_back("cs_cnfg.cbc05.id");
	_nodes.push_back("cs_cnfg.cbc05.fe_id");
	_nodes.push_back("cs_cnfg.cbc05.i2c_address");
	_nodes.push_back("cs_cnfg.cbc06.active");
	_nodes.push_back("cs_cnfg.cbc06.id");
	_nodes.push_back("cs_cnfg.cbc06.fe_id");
	_nodes.push_back("cs_cnfg.cbc06.i2c_address");
	_nodes.push_back("cs_cnfg.cbc07.active");
	_nodes.push_back("cs_cnfg.cbc07.id");
	_nodes.push_back("cs_cnfg.cbc07.fe_id");
	_nodes.push_back("cs_cnfg.cbc07.i2c_address");
	_nodes.push_back("cs_cnfg.cbc08.active");
	_nodes.push_back("cs_cnfg.cbc08.id");
	_nodes.push_back("cs_cnfg.cbc08.fe_id");
	_nodes.push_back("cs_cnfg.cbc08.i2c_address");
	_nodes.push_back("cs_cnfg.test_out.1");
	_nodes.push_back("cs_cnfg.test_out.2");
	_nodes.push_back("cs_cnfg.global.trigger_master_external");
	_nodes.push_back("cs_cnfg.fcm.enable");
	/*
	_nodes.push_back("cs_cnfg.fcm.fcg.enable.fast_reset");
	_nodes.push_back("cs_cnfg.fcm.fcg.enable.trigger");
	_nodes.push_back("cs_cnfg.fcm.fcg.enable.test_pulse");
	_nodes.push_back("cs_cnfg.fcm.fcg.enable.orbit_reset");
	*/
	_nodes.push_back("cs_cnfg.fcm.fcg.Ncycle");
	_nodes.push_back("cs_cnfg.fcm.fcg.cycle_period");
	_nodes.push_back("cs_cnfg.fcm.fcg.enable");
	_nodes.push_back("cs_cnfg.fcm.fcg.trigger_timing");
	_nodes.push_back("cs_cnfg.fcm.fcg.test_pulse_timing");
	_nodes.push_back("cs_cnfg.fcm.fcg.orbit_reset_timing");
    }
    return _nodes;
}
std::vector<string>& Cbc3BeFc7::Cbc3SystemStatusRegs()
{
    static std::vector<std::string> _nodes;
    if( _nodes.size() == 0 ){
	_nodes.push_back("cs_stat.global.nfe");
	_nodes.push_back("cs_stat.global.ncbc");
	_nodes.push_back("cs_stat.global.n_active_cbc");
	_nodes.push_back("cs_stat.fcm.fsm");
	_nodes.push_back("cs_stat.fcm.trigger_fsm");
	_nodes.push_back("cs_stat.eb.write_data_fsm");
	_nodes.push_back("cs_stat.eb.send_data_fsm");
	_nodes.push_back("cs_stat.db.flags.werr");
	_nodes.push_back("cs_stat.db.flags.rerr");
	_nodes.push_back("cs_stat.db.nword_all");
	_nodes.push_back("cs_stat.db.nword_events");
	_nodes.push_back("cs_stat.db.nword_free");
	_nodes.push_back("cs_stat.db.waddr");
	_nodes.push_back("cs_stat.db.raddr");
	_nodes.push_back("cs_stat.rdb.read_ready"); 
	_nodes.push_back("cs_stat.rdb.waddr"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.data_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.data_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.data_info_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.data_info_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.trig_data_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.trig_data_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.fifo_write_fsm"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.packet_send_fsm"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc1.data_frame_counter"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.data_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.data_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.data_info_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.data_info_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.trig_data_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.trig_data_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.fifo_write_fsm"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.packet_send_fsm"); 
	_nodes.push_back("cs_stat.cbc_dp.cbc2.data_frame_counter"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.com_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.com_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.reply_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.reply_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.reply_fifo_ndata"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.ready"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.waiting"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus1.n_active_cbcs"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.com_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.com_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.reply_fifo_empty"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.reply_fifo_full"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.reply_fifo_ndata"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.ready"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.waiting"); 
	_nodes.push_back("cs_stat.cbc_i2c_bm.bus2.n_active_cbcs"); 
	_nodes.push_back("cs_stat.io.cbc_data.timing_tuning.fe1.fsm");
	_nodes.push_back("cs_stat.io.cbc_data.timing_tuning.fe1.idelay_ctrl_fsm");
	_nodes.push_back("cs_stat.io.cbc_data.timing_tuning.fe1.mmcme2_drp_saddr");
	_nodes.push_back("cs_stat.io.cbc_data.timing_tuning.fe1.bitslip_counter");
	_nodes.push_back("cs_stat.io.cbc_data.iserdes.cbc1.delay_locked"); 
	_nodes.push_back("cs_stat.io.cbc_data.iserdes.cbc1.in_delay_tap_out"); 
	_nodes.push_back("cs_stat.io.cbc_data.iserdes.cbc1.slvs5"); 
    }
    return _nodes;
}
std::map<unsigned,std::string>& Cbc3BeFc7::TestOutMapping()
{
    static std::map<unsigned,string> _map;
    if( _map.size() == 0 ){
	_map[0xFFFF] = "unknown";
	_map[0x0001] = "clk320MHz";
	_map[0x0002] = "clk40MHz";
	_map[0x0003] = "fast reset";
	_map[0x0004] = "trigger";
	_map[0x0005] = "test pulse request";
	_map[0x0006] = "orbit reset";
	_map[0x0007] = "scl";
	_map[0x0008] = "sda to cbc";
	_map[0x0009] = "sda from cbc";
	_map[0x0100] = "cbc hard reset";
    }
    return _map;
}
std::map<unsigned,std::string>& Cbc3BeFc7::DataClockTimingTuningModuleFsmMapping()
{
    static std::map<unsigned,string> _map;
    if( _map.size() == 0 ){
	_map[0] = "init";
	_map[1] = "wait_bram_ready";
	_map[2] = "idelay_reset";
	_map[3] = "idelay_scan";
	_map[4] = "idelay_tuning";
	_map[5] = "set_idelay";
	_map[6] = "change_clock_phase";
	_map[7] = "reset_iserdes_idelay_ctrl";
	_map[8] = "bitslip_tuning";
	_map[9] = "tuned";
    }
    return _map;
}
std::map<unsigned,std::string>& Cbc3BeFc7::DataClockTimingTuningModuleIserdesIdelayCtrlFsmMapping()
{
    static std::map<unsigned,string> _map;
    if( _map.size() == 0 ){
	_map[0] = "wait_for_commands";
	_map[1] = "in_delay_reset_process";
	_map[2] = "in_delay_set_process";
	_map[3] = "in_delay_ce_process";
	_map[4] = "reset_iserdes_process";
	_map[5] = "bitslip_process";
	_map[6] = "pattern_check";
    }
    return _map;
}
std::string Cbc3BeFc7::id( uint32_t value ){ 

    char cvalue[5];
    for( int i=0; i<4; i++ ){
	cvalue[i] = ( value >> ( 3 - i ) * 8 ) & 0xFF;
    }
    cvalue[4] = 0;
    return std::string( cvalue );
}
std::string Cbc3BeFc7::version( uint32_t value ){

    ostringstream oss;
    oss 
	<< ( value >> 28 ) << "." 
	<< ( ( value >> 24 ) & 0xF ) << "." 
	<< ( ( value >> 16 ) & 0xFF ) 
	<< ", date: " 
	<< setfill( '0' )
	<< setw(2) << ( value & 0x1F ) << "."
	<< setw(2) << ( ( value >> 5 ) & 0xF ) << "."
	<< setw(2) << ( ( value >> 9 ) & 0x7F ); 
    return std::string( oss.str() );
}

Cbc3BeFc7::Cbc3BeFc7(bool CBC3_0, int debug):Cbc3BeIpbusInterface("Fc7", CBC3_0, debug)
{
    fDataBufferSize = 65536;
} 
void Cbc3BeFc7::DumpBeBoardConfig( std::ostream &os, int level )
{
    fc7::dump_config(fBoard, os);
    os << "Cbc3BeFc7 configurations" << endl;
    os << ( "---------------------------------------------------------" ) << endl;
    if( level == DUMP_LEVEL_DEBUG ){
	vector<string> nodelist = fBoard->getNodes( "cs_cnfg.*" );
	for(unsigned i=0; i < nodelist.size(); i++){
	    if( fBoard->getNode( nodelist.at(i) ).getTags().find( "noleaf" ) != string::npos ) nodelist.erase(nodelist.begin() + i--);
	}
	vector<uint32_t> valuelist;
	read_ipbus( fBoard, nodelist, valuelist );
	for( unsigned i=0; i < nodelist.size(); i++ ){
	    cout <<  setw(80) << setfill(' ') << nodelist.at(i) 
		<< "  0x" << setw(8) << setfill('0') << std::hex << std::right << valuelist.at(i)
		<< endl;
	}
    }
    else if( level == DUMP_LEVEL_NORMAL ){
	std::vector<uint32_t> values;
	std::vector<string> &nodes = Cbc3SystemConfigRegs();
	ReadBeBoardRegisters( nodes, values ); 
	string node = "cs_stat.global.ncbc";
	unsigned ncbc = read_ipbus(fBoard,node);
	cout << "NCBC = " << ncbc << endl;
	char cnode[256];
	for( unsigned i=0; i < values.size(); i++ ){
	    if( nodes.at(i).find( "cs_cnfg.global" ) != std::string::npos ){
		if( nodes.at(i).find("be_id" ) != std::string::npos ){
		    os << "+GLOBAL" << endl;
		    os << "  + BE ID=" << values.at(i) << endl;
		}
	    }
	    else if( nodes.at(i) == "cs_cnfg.global.trigger_master_external" ){
		os << "+ TRIGGER MASTER                          : ";
		if( values.at(i) == 0 ) os << "INTERNAL";
		else os << "EXTERNAL";
		os << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.test_out.1" ){
		os << "+ TEST OUTPUT 1                           : ";
		std::map<unsigned,string> &map = TestOutMapping();
		if( map.find( values.at(i) ) != map.end() ) os << map[values.at(i)];
		else os << "unknown";
		os << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.test_out.2" ){
		os << "+ TEST OUTPUT 2                           : ";
		std::map<unsigned,string> &map = TestOutMapping();
		if( map.find( values.at(i) ) != map.end() ) os << map[values.at(i)];
		else os << "unknown";
		os << endl;
	    }
	    else if( nodes.at(i).find("cs_cnfg.cbc") != std::string::npos ){ 

		for( unsigned cbc_i=1; cbc_i <= ncbc; cbc_i++ ){ 
		    sprintf(cnode, "cbc%02d", cbc_i);
		    if( nodes.at(i).find(cnode) != std::string::npos ){ 
			if( nodes.at(i).find("active") != std::string::npos ){
			    os << "  + CBC" << cbc_i << " : ";
			    if( values.at(i) == 1 ) os << "ACTIVE\t";
			    else os << "NOT ACTIVE\t";
			}
			else if( nodes.at(i).find(".id") != std::string::npos ){
			    os << " [ID]=" << values.at(i);
			}
			else if( nodes.at(i).find("fe_id") != std::string::npos ){
			    os << " [FE ID]=" << values.at(i);
			}
			else if( nodes.at(i).find("i2c_address") != std::string::npos ){
			    os << " [I2C ADDRESS]=0x" << std::hex << std::right << std::setfill('0') << std::setw(2) << values.at(i) << endl;
			}
		    }
		}
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.enable" ){
		os << "+ FAST COMMAND MANAGER SETTINGS" << endl;
		os << "  + ENABLED SOURCES                       : "; 
		if( values.at(i) & 1 )        os << "[EXTERNAL]  ";
		if( (values.at(i) >> 1) & 1 ) os << "[IPBUS]  ";
		if( (values.at(i) >> 2) & 1 ) os << "[INTERNAL]  ";
		if( (values.at(i) >> 3) & 1 ) os << "[SEQUENCER]  ";
		os << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.enable" ){
		os << "  + FAST COMMAND GENERATOR SETTINGS" << endl;
		os << "    + ENABLED SIGNALS                     : "; 
		if( values.at(i) & 1 )        os << "[FAST RESET]  ";
		if( (values.at(i) >> 1) & 1 ) os << "[TRIGGER]  ";
		if( (values.at(i) >> 2) & 1 ) os << "[TEST PULSE REQUEST]  ";
		if( (values.at(i) >> 3) & 1 ) os << "[ORBIT RESET]  ";
		os << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.Ncycle" ){
		os << "    + NUMBER OF CYCLES TO BE GENERATED    : " << std::dec << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.cycle_period" ){
		os << "    + PERIOD OF THE CYCLE IN 40 MHz CLOCK : " << std::dec << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.trigger_timing"){
		os << "    + TRIGGER SIGNAL TIMING               : " << std::dec << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.test_pulse_timing"){
		os << "    + TEST PULSE TIMING                   : " << std::dec << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_cnfg.fcm.fcg.orbit_reset_timing"){
		os << "    + ORBIT RESET TIMING                  : " << std::dec << values.at(i) << endl;
	    }
	}
    }
    os << ( "---------------------------------------------------------" ) << endl;
}
void Cbc3BeFc7::DumpBeBoardStatus( std::ostream &os, int level )
{
    os << "Cbc3BeFc7 status" << endl;
    os << ( "---------------------------------------------------------" ) << endl;
    if( level == DUMP_LEVEL_DEBUG ){
	vector<string> nodelist = fBoard->getNodes( "cs_stat.*" );
	for(unsigned i=0; i < nodelist.size(); i++){
	    if( fBoard->getNode( nodelist.at(i) ).getTags().find( "noleaf" ) != string::npos ) nodelist.erase(nodelist.begin() + i--);
	}
	vector<uint32_t> valuelist;
	read_ipbus( fBoard, nodelist, valuelist );
	for( unsigned i=0; i < nodelist.size(); i++ ){
	    cout <<  setw(80) << setfill(' ') << nodelist.at(i) 
		<< "  0x" << setw(8) << setfill('0') << std::hex << std::right << valuelist.at(i)
		<< endl;
	}
    }
    else if( level == DUMP_LEVEL_NORMAL ){
	std::vector<uint32_t> values;
	std::vector<string> &nodes = Cbc3SystemStatusRegs();
	ReadBeBoardRegisters( nodes, values ); 
	string node = "cs_stat.global.ncbc";
	unsigned ncbc = read_ipbus(fBoard,node);
	char cnode[256];
	for( unsigned i=0; i < values.size(); i++ ){
	    if( nodes.at(i) == "cs_stat.global.nfe" ){
		os << "+ GLOBAL" << endl;
		os << "  + # of FE  is " << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_stat.global.ncbc" ){
		os << "  + # of CBC is " << values.at(i) << endl;
	    }
	    else if( nodes.at(i) == "cs_stat.global.n_active_cbc" ){
		os << "  + # of active CBC is " << values.at(i) << endl;
	    }
	    else if( nodes.at(i).find("fcm.trigger_fsm") != std::string::npos ){
		os << "+ TRIGGER FSM                               : ";
		if( values.at(i) == 0 ) os << "NOT RUNNING" << endl;
		else os << "RUNNING" << endl;
	    }
	    else if( nodes.at(i).find("cs_stat.cbc_dp") != std::string::npos ){
		for( unsigned cbc_i=1; cbc_i <= ncbc; cbc_i++ ){ 
		    sprintf(cnode, "cbc%1d", cbc_i);
		    if( nodes.at(i).find(cnode) != std::string::npos ){
			if( nodes.at(i).find( "data_frame_counter" ) != std::string::npos ){
			    if(cbc_i==1) os << "+ CBC DATA PROCESSORS" << endl;
			    os << "  + CBC" << cbc_i << endl;
			    os << "    + DATA FRAME COUNTER                    : " << std::dec << std::setw(10) << std::right << std::setfill(' ') << values.at(i) << endl;
			}
		    }
		}
	    }
	    else if( nodes.at(i) == "cs_stat.db.nword_all" ){
		os << "+ DATA BUFFER" << endl;
		os << "  + NUMBER OF WORDS READY TO BE READ        : " << std::dec << std::setw(10) << std::right << std::setfill(' ') << values.at(i) << endl;  
	    }
	    else if( nodes.at(i) == "cs_stat.db.nword_all" ){
		os << "  + NUMBER OF WORDS READY TO BE READ FOR COMPLETE EVENTS : " << std::dec << std::setw(10) << std::right << std::setfill(' ') << values.at(i) << endl;  
	    }
	    else if( nodes.at(i) == "cs_stat.db.nword_free" ){
		os << "  + NUMBER OF WORDS FREE IN DATA BUFFER     : " << std::dec << std::setw(10) << std::right << std::setfill(' ') << values.at(i) << endl;  
	    }
	}
    }
}
void Cbc3BeFc7::PrintVersion( std::ostream &os ) 
{
    os << "Fc7 version" << endl;
    os << ( "---------------------------------------------------------" ) << endl;
    fc7::print_version(fBoard,os);
    os << ( "---------------------------------------------------------" ) << endl;
    os << "Cbc3BeFc7 version" << endl;
    os << ( "---------------------------------------------------------" ) << endl;
    PrintCbcSystemVersion( os );
    os << ( "---------------------------------------------------------" ) << endl;

}
string Cbc3BeFc7::ReadVersion() 
{
    return  version( read_ipbus( fBoard, "cs_stat.system.version" ) );
}
string Cbc3BeFc7::ReadId() 
{
    return  id( read_ipbus( fBoard, "cs_stat.system.id" ) );
}
void Cbc3BeFc7::PrintCbcSystemVersion( std::ostream &os ) 
{
    os << setw(10) << "[id]      " << id( read_ipbus( fBoard, "cs_stat.system.id" ) )           << endl;
    os << setw(10) << "[version] " << ReadVersion() << endl;
    os << setw(10) << "[type]    " << read_ipbus( fBoard, "cs_stat.system.type" )               << endl;
}
void Cbc3BeFc7::WriteFscBram( const std::vector<uint32_t> &data )
{
    write_ipbus( fBoard, "cs_ctrl.seq.reset", 1 );
    usleep(1);
    writeBlock_ipbus( fBoard, "seqbram", data ); 
}

void Cbc3BeFc7::WriteFscBram( const std::string &fname )
{
    std::vector<uint32_t> data;
    ifstream ifile( fname.c_str() );
    if( ifile.is_open() ){
	char line[256];
	while( !ifile.eof() ){
	    ifile.getline(line, 256 );
	    if( strlen(line)==0 || line[0] == ' ' || line[0] == '#' ) continue;
	    istringstream iss( line );
	    string addr_str, value_str;
	    iss >> addr_str >> value_str;
	    unsigned addr, value;
	    addr = strtol( addr_str.c_str(), 0, 0 );
	    value = strtol( value_str.c_str(), 0, 0 );
	    if( 4096 <= addr ) break; 
	    data.push_back(value);
	    //cout << std::hex << std::setfill('0') << value << endl;
	}
    }
    ifile.close();
    write_ipbus( fBoard, "cs_ctrl.seq.reset", 1 );
    usleep(1);
    writeBlock_ipbus( fBoard, "seqbram", data ); 
}
void Cbc3BeFc7::WriteCiscBram( const std::string &fname )
{
    std::vector<uint32_t> data;
    ifstream ifile( fname.c_str() );
    if( ifile.is_open() ){
	char line[256];
	while( !ifile.eof() ){
	    ifile.getline(line, 256 );
	    if( strlen(line)==0 || line[0] == ' ' || line[0] == '#' ) continue;
	    istringstream iss( line );
	    int page;
	    string addr_str, value_str;
	    iss >> page >> addr_str >> value_str;
	    unsigned addr, value;
	    addr = strtol( addr_str.c_str(), 0, 0 );
	    if( page != 1 || page != 2 ) continue;
	    if( addr == 0x00 || ( page == 1 && ( addr == 0x1d || addr == 0x15 || addr == 0x16 || addr == 0x17 || addr == 0x18 ))) continue;
	    value = strtol( value_str.c_str(), 0, 0 );
	    unsigned word(0);
	    word = (page-1) << 16 | addr << 8 | value; 
	    cout << std::hex << std::setw(8) << std::setfill('0') << word << endl;
	    data.push_back(word);
	}
    }
    ifile.close();
    write_ipbus( fBoard, "cs_ctrl.cisc.reset", 1 );
 //   usleep(1);
    writeBlock_ipbus( fBoard, "ciscdbram", data ); 
}

void Cbc3BeFc7::CbcReset(int fe_id){

    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFc7::CbcReset()" << endl;

    write_ipbus( fBoard, "cs_ctrl.global.cbc_hard_reset", 1 );
    usleep(2);

}
void Cbc3BeFc7::SendFastSignal( std::string type )
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFc7::SendFastSginal() : " << type << endl;

    char node[256];
    sprintf( node, "cs_ctrl.fcm.fc_%s", type.c_str() );
    WriteBeBoardConfig( node, 1 );
//    write_ipbus( fBoard, node, 1 ); 
}

bool Cbc3BeFc7::HasData( int min_nword )
{
    /*
    int ndata = read_ipbus( fBoard, "cs_stat.data_buffer.nword_all" );
    int div(0), mod(0);
    div = fNumNextDataWords / 14;
    mod = fNumNextDataWords % 14;
    cout << "HasData : nword_all = " << std::dec << ndata << " : nword_events = " << std::dec << fNumNextDataWords << " : div = " << div << " : mod = " << mod << " : min words = " << min_nword << endl;

    if( fNumNextDataWords % 14 != 0 ){
       	cout << "nword_events is not valid. " << std::dec << fNumNextDataWords << endl;
	cout << std::dec << fNumNextDataWords << " " << min_nword << endl;
    }
    */
    string node;
    if(fWaitEvents){
       	node = "cs_stat.db.nword_events";
    }
    else node = "cs_stat.db.nword_all";
    //usleep(10);
    fNumNextDataWords = read_ipbus( fBoard, node );
    if( fNumNextDataWords != 0 && fNumNextDataWords >= min_nword ){
	//cout << "Cbc3eFc7::HasData() Next data in word(nword) = " << std::dec << fNumNextDataWords << ". Min. # of words to read = " << min_nword << endl;
	return true;
    }
    else return false;
}

bool Cbc3BeFc7::WaitData(unsigned timeout_sec, bool usec )
{
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);

    while( true ){

//	if( HasData(fMinNumWordToWait) && fNumNextDataWords >= fMinNumWordToWait ){
	if( HasData(fMinNumWordToWait) ){ 
//	    cout << "WaitData : " << " fNumNextDataWords = " << fNumNextDataWords << " fMinNumWordToWait = " <<  fMinNumWordToWait << endl; 
	    /*
	     int nframe = ReadBeBoardRegister( "cs_stat.cbc_data_processors.cbc1.data_frame_counter" );
	    cout << std::dec << nframe << endl;
	    */
	    return true;
	}
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( timeout_sec > 0 && ( ( !usec && dt.tv_sec > timeout_sec ) || ( usec && dt.tv_sec * 1000000. + dt.tv_usec > timeout_sec )) ){
	    int nframe = ReadBeBoardRegister( "cs_stat.cbc_dp.cbc1.data_frame_counter" );
	    cout << "timeout : cbc1 data_frame_counter : " << std::dec << nframe << endl;

	    CBCI2C_ITEM_DATA_MAP item_data;
	    CBCI2C_TYPE_DATA type_data( "FciAndError", item_data );
	    rwCbcI2cRegsByType(1,0, type_data, 1);
	    dump_cbci2c_type_data( type_data, cout );
	    char cnode[256];
	    sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.fsm", 1 );
	    int dctt_fsm = read_ipbus( fBoard, cnode );
	    sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.mmcme2_drp_saddr", 1 );
	    unsigned saddr = read_ipbus( fBoard, cnode );
	    cout << "cbc data timing control fsm, saddr = " << dctt_fsm << ", " << saddr << endl;
	    sprintf( cnode, "cs_stat.io.cbc_data.iserdes.cbc%d.slvs5", 1 );
	    unsigned slvs5 = read_ipbus( fBoard, cnode );
	    cout << "Cbc3BeFc7::WaitData() : CBC ID = " << 1 << " SLVS5 pattern = 0x" 
		<< std::hex << setfill('0') << setw(2) << slvs5 << endl;
	    // cout << "dt " << dt.tv_sec << endl;
	    //	    cout << "HasData : " << HasData() << " fNumNextDataWords = " << fNumNextDataWords << " fMinNumWordToWait = " <<  fMinNumWordToWait << endl; 
//	    if( HasData() > 0 ){
	    if( fNumNextDataWords > 0 ){
//		cout << "WaitData timeout : "  << " fNumNextDataWords = " << fNumNextDataWords << " fMinNumWordToWait = " <<  fMinNumWordToWait << endl; 
	       	return true;
	    }
	    else return false;
	}
	if( fDataReadyFlagPollingInterval_usec != 0 ) 
	    cout << "Sleep " << fDataReadyFlagPollingInterval_usec << " usec." << endl; 
	    usleep( fDataReadyFlagPollingInterval_usec ); 
    }

    return false;
}
int Cbc3BeFc7::ReadDataBufferAll( uint32_t *& Data )
{
    fNumNextDataWords = fDataBufferWordSize;
    write_ipbus( fBoard, "cs_ctrl.db.trig_readall", 1 );
    std::vector<uint32_t> data = ReadData();
    for( unsigned i=0; i < data.size(); i++ ){
	Data[i] = data.at(i); 
    }
    return data.size();
}

int Cbc3BeFc7::ReadData( uint32_t *& Data, int nword )
{
    if(nword && nword < fNumNextDataWords) fNumNextDataWords = nword;
    std::vector<uint32_t> data = ReadData();
    usleep(10);
    for( unsigned i=0; i < data.size(); i++ ){
	Data[i] = data.at(i); 
    }
    return data.size();
}

std::vector<uint32_t> Cbc3BeFc7::ReadData()
{
    //cout << "Reading " << std::dec << fNumNextDataWords << endl;
    string node = "data";
    uhal::ValVector<uint32_t> data = fBoard->getNode(node).readBlock( fNumNextDataWords );
    fBoard->dispatch();
    //cout << "Read " << std::dec << fNumNextDataWords << endl;
    return data.value();

}

void Cbc3BeFc7::ReadBeCbcConfig()
{
    string node = "cs_cnfg.global.be_id";
    fBeId = read_ipbus(fBoard,node);
    fCbcFeIds[0] = 0;
    node = "cs_stat.global.nfe";
    fNFes = read_ipbus(fBoard,node);
    node = "cs_stat.global.ncbc";
    fNCbcs = read_ipbus(fBoard,node);
    char cnode[256];
    fNActiveCbcs = 0;
    for( unsigned cbc_id=1; cbc_id <= fNCbcs; cbc_id++ ){
	sprintf( cnode, "cs_cnfg.cbc%02d.active", cbc_id );
	bool active = read_ipbus(fBoard,cnode);
	sprintf( cnode, "cs_cnfg.cbc%02d.fe_id", cbc_id );
	unsigned fe_id = read_ipbus(fBoard,cnode);
	fCbcActiveFlags[cbc_id] = active;
	fCbcFeIds[cbc_id] = fe_id;
	if(active) fNActiveCbcs++;
    }
    for( unsigned fe_id = 1; fe_id <= fNFes; fe_id++ ){
	sprintf(cnode, "cs_stat.cbc_i2c_bm.bus%1d.n_active_cbcs", fe_id);
	fNFeCbcs[fe_id] = read_ipbus(fBoard, cnode );
//	cout << "I have read the number of cbcs " << fNFeCbcs[fe_id] << " for fe id = " << fe_id << endl;
    }
    fCbcConfigValid = true;
}
bool Cbc3BeFc7::ScanDataClockTiming( unsigned fe_id )
{
    string node;
    char cnode[256];
    bool good(true);

    SendFastSignal( "reset" ); 

    ReadBeCbcConfig();

    cout << "ScanDataClockTiming" << endl;
    cout << fNFeCbcs[fe_id] << endl;
    if( fNFeCbcs.find(fe_id) == fNFeCbcs.end() || fNFeCbcs.find(fe_id)->second == 0 ) return false; 
    cout << "ScanDataClockTiming" << endl;

    unsigned vcth, vcth_zero(0);

    sprintf( cnode, "cs_cnfg.io.cbc_data.timing_tuning.fe%d.cbc_id", fe_id );
    unsigned cbc_id = read_ipbus( fBoard, cnode );
    cout << "CBC " << cbc_id << " is used for data clock timing scan." << endl;
    if( IsCbcActive(cbc_id) == false ){
	cerr << "CBC " << cbc_id << " for the data clock timing tuning for FE" << fe_id << " is not active." << endl; 
	return good;
    }

    rwCbcI2cRegsByName(1,0,"VCTH.vcth", vcth, cbc_id);
    rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth_zero, cbc_id);

    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.stop_trigger";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.global.daq_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_scan_bram_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_scan_bram_rd_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_tuning_reset";
    // Sometimes tuning fails if tune command is sent too early after the reset.  Maybe clock reset in FPGA takes time.--fixed.
    write_ipbus( fBoard, node, 1 );
//    usleep(10);
    sprintf(cnode, "cs_ctrl.io.cbc_data.fe%d.timing_tuning_scan_pulse", fe_id);
    write_ipbus( fBoard, cnode, 1 );

    int dctt_fsm(0);
    usleep(2);
    sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.fsm", fe_id );
    dctt_fsm = read_ipbus( fBoard, cnode );
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    while(dctt_fsm != 0 ){
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 0 ){
	    cerr << "Data clock timing scan failed for FE ID = " << fe_id << endl;
	    good = false;
	    break;
	}
	usleep(1);
	dctt_fsm = read_ipbus( fBoard, cnode );
	//	    cout << dctt_fsm << endl;
    }
    rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth, cbc_id);

    cout << "====================" << endl;
    cout << "cbc data clock timing scan result for FE" << fe_id << endl;
    cout << "--------------------" << endl;
    cout << "Patterns min delay to max delay" << endl;
    cout << "[";
    unsigned dsize(512);
    uhal::ValVector<uint32_t> data = fBoard->getNode("cdctb").readBlock(dsize);
    fBoard->dispatch();
    map<unsigned,unsigned> psn;
    for(unsigned i=0; i < 30; i++ ){
	unsigned addr((fe_id - 1) * 30 + i);
	unsigned p0(0), p1(0);
	bool     s0(true), s1(true), g0(true), g1(true);
	p0 = data.at(addr) & 0x000000FF;
	p1 = (data.at(addr) & 0x00FF0000) >> 16;
	if( data.at(addr) & 0x00000200 ) s0 = false;
	if( data.at(addr) & 0x02000000 ) s1 = false;
	if( data.at(addr) & 0x00000400 ) g0 = false;
	if( data.at(addr) & 0x04000000 ) g1 = false;
	std::cout << setw(8) << setfill('0') << std::hex << data.at(addr) << std::endl;
	if(s0){
	    cout << setw(2) << std::hex << setfill('0') << p0 << " ";
	    if( g0 ){ 
		if(psn.find(p0) == psn.end()) psn[p0] = 1;
		else psn[p0] ++;
	    }
	}
	else cout << "?? ";
	if(s1){
	    cout << setw(2) << std::hex << setfill('0') << p1 << " ";
	    if( g1 ){ 
		if(psn.find(p1) == psn.end()) psn[p1] = 1;
		else psn[p1] ++;
	    }
	}
	else cout << "?? ";
	if( i % 10 == 9 ) cout << "| ";
    }
    cout << "]" << endl;
    map<unsigned,unsigned>::iterator it=psn.begin();
    unsigned gp(0), ngp(0);
    for(; it != psn.end(); it++){
	if( ngp < it->second ){ gp = it->first; ngp = it->second; }
    }
    cout << "Pattern [0x" << setw(2) << std::hex << setfill('0') << gp << "] is stable for " << std::dec << ngp << " idelay taps." << endl;
    unsigned ofst = ceil((40 - ngp)/2);
    //1 tap = 78 ps
    cout << "320MHz clock duration = 40 idelay taps. ceil(40 - " << ngp << ")/2 = " << ofst << " taps to be delayed more to center the sync bit." << endl; 
    cout << "Set cs_cnfg.io.data_clock_timing_tuning.idelay_offset to " <<  ofst << "." << endl; 
    /*
    node = "cs_cnfg.io.data_clock_timing_tuning.idelay_offset";
    write_ipbus( fBoard, node, ofst );
    */

    return good;
}
bool Cbc3BeFc7::TuneDataClockTiming(bool scan)
{
    string node;
    char tmp[256];
    char cnode[256];

    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ) cout << "**** Cbc3BeFc7::TuneDataClockTiming() ****" << endl;

    SendFastSignal( "reset" ); 

    ReadBeCbcConfig();

    map<unsigned,unsigned> fe_cbc_map;

    //setting VCTH to zero temporarily
    map <unsigned,unsigned> vcth, vcth_zero, fci;
    for( unsigned fe_id=1; fe_id <= fNFes; fe_id++ ){ 
	if( fNFeCbcs.find(fe_id) == fNFeCbcs.end() || fNFeCbcs.find(fe_id)->second == 0 ) continue;

	sprintf( cnode, "cs_cnfg.io.cbc_data.timing_tuning.fe%d.cbc_id", fe_id );
	unsigned cbc_id = read_ipbus( fBoard, cnode );
	fe_cbc_map[fe_id] = cbc_id;
	if( IsCbcActive(cbc_id) == false ){
	    sprintf(tmp, "[Cbc3BeException] Cbc3BeFc7::TuneDataClockTiming() cbc id = %d is not active for data clock timing tuning of FE id = %d.", cbc_id, fe_id);
	    Cbc3BeException e(tmp, Cbc3BeException::DAQ_ERR);
	    continue;
	}

	rwCbcI2cRegsByName(1,0, "FciAndError.fci_delay", fci[fe_id], cbc_id);
	rwCbcI2cRegsByName(1,0,"VCTH.vcth", vcth[fe_id], cbc_id);
	vcth_zero[fe_id] = 0;
	rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth_zero[fe_id], cbc_id);
    }

    //disable fast commands, reset daq, and reset cbc timing scanning/tuning modules.
    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.stop_trigger";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.global.daq_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_scan_bram_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_scan_bram_rd_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.io.cbc_data.timing_tuning_reset";
    write_ipbus( fBoard, node, 1 );

    bool good(true);

    if(scan){
	for( unsigned fe_id=1; fe_id <= fNFes; fe_id++ ){ 
	    if( fNFeCbcs.find(fe_id) == fNFeCbcs.end() || fNFeCbcs.find(fe_id)->second == 0 ) continue;

	    unsigned cbc_id = fe_cbc_map[fe_id];

	    sprintf(cnode, "cs_ctrl.io.cbc_data.fe%d.timing_tuning_scan_pulse", fe_id);
	    write_ipbus( fBoard, cnode, 1 );

	    sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.fsm", fe_id );
	    int dctt_fsm = read_ipbus( fBoard, cnode );

	    struct timeval t0, t, dt;
	    gettimeofday(&t0,0);

	    while(dctt_fsm != 0 ){

		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);

		if( dt.tv_sec > 0 ){
		    sprintf(tmp,"[Cbc3BeException] Cbc3BeFc7::TuneDataClockTiming() timing scan failed for FE id = %d", fe_id);
		    throw Cbc3BeException(tmp, Cbc3BeException::DAQ_ERR);
		    good = false;
		    break;
		}
		usleep(1);
		dctt_fsm = read_ipbus( fBoard, cnode );
	    }

	    sprintf( tmp, "---- CBC data clock timing scan result for FE id = %d and CBC id = %d, FCI delay = %d ----\n", fe_id, cbc_id, fci[fe_id]); 
	    cout << tmp;
	    cout << "Patterns min delay to max delay" << endl;
	    cout << "[";
	    unsigned dsize(512);
	    uhal::ValVector<uint32_t> data = fBoard->getNode("cdctb").readBlock(dsize);
	    fBoard->dispatch();
	    map<unsigned,unsigned> psn;
	    for(unsigned i=0; i < 30; i++ ){
		unsigned addr((fe_id - 1) * 30 + i);
		//one word contains two data points
		//format [15 downto 0]
		//[7 downto 0] : pattern
		//[8] : pattern changed flag
		//[9] : pattern unstable flag
		//[10] : pattern bad flag
		//[11] : pattern good flag
		//[15 downto 12] : -  
		unsigned p0(0), p1(0);//patterns
		bool     s0(true), s1(true), g0(true), g1(true);//s for stable flag,
		p0 = data.at(addr) & 0x000000FF;
		p1 = (data.at(addr) & 0x00FF0000) >> 16;
		if( data.at(addr) & 0x00000200 ) s0 = false; //unstable flag check
		if( data.at(addr) & 0x02000000 ) s1 = false;
		if( data.at(addr) & 0x00000400 ) g0 = false; //bad flag check (the pattern will be good if the bitslip is tuned.)
		if( data.at(addr) & 0x04000000 ) g1 = false;
		//std::cout << setw(8) << setfill('0') << std::hex << data.at(addr) << std::endl;
		if(s0){
		    cout << setw(2) << std::hex << setfill('0') << p0 << " ";
		    if( g0 ){ 
			if(psn.find(p0) == psn.end()) psn[p0] = 1;
			else psn[p0] ++;
		    }
		}
		else cout << "?? ";
		if(s1){
		    cout << setw(2) << std::hex << setfill('0') << p1 << " ";
		    if( g1 ){ 
			if(psn.find(p1) == psn.end()) psn[p1] = 1;
			else psn[p1] ++;
		    }
		}
		else cout << "?? ";
		if( i % 10 == 9 ) cout << "| " << endl << " ";
	    }
	    cout << "]" << endl;
	    map<unsigned,unsigned>::iterator it=psn.begin();
	    unsigned gp(0), ngp(0);
	    for(; it != psn.end(); it++){
		if( ngp < it->second ){ gp = it->first; ngp = it->second; }
	    }
	    cout << "Pattern [0x" << setw(2) << std::hex << setfill('0') << gp << "] is stable for " << std::dec << ngp << " idelay taps." << endl;
	    unsigned ofst = ceil((40 - ngp)/2);
	    cout << "320MHz clock duration = 40 idelay taps. ceil(40 - " << ngp << ")/2 = " << ofst << " taps to be delayed more to center the sync bit." << endl; 
	    cout << "Setting cs_cnfg.io.cbc_data.timing_tuning.fe" << fe_id << ".idelay_offset to " <<  ofst << "." << endl; 
	    sprintf(cnode, "cs_cnfg.io.cbc_data.timing_tuning.fe%d.idelay_offset", fe_id); 
	    write_ipbus( fBoard, cnode, ofst ); // for cable test.
	    //ofst = read_ipbus( fBoard, cnode );
	    //cout << "TEST : idelay offset in the configuration file is used : " << ofst << endl;
	}
    }

    //tuning
    node = "cs_ctrl.io.cbc_data.timing_tuning_tune";
    write_ipbus( fBoard, node, 1 );

    for( unsigned fe_id=1; fe_id <= fNFes; fe_id++ ){ 
	if( fNFeCbcs.find(fe_id) == fNFeCbcs.end() || fNFeCbcs.find(fe_id)->second == 0 ) continue;

	unsigned cbc_id = fe_cbc_map[fe_id];

	sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.fsm", fe_id );
	int dctt_fsm = read_ipbus( fBoard, cnode );

	struct timeval t0, t, dt;
	gettimeofday(&t0,0);

	while(dctt_fsm != 9 ){
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > 0 ){
		CBCI2C_ITEM_DATA_MAP item_data;
		CBCI2C_TYPE_DATA type_data( "FciAndError", item_data );
		rwCbcI2cRegsByType(1,0, type_data, cbc_id);
		dump_cbci2c_type_data( type_data, cout );
		sprintf(tmp,"[Cbc3BeException] Cbc3BeFc7::TuneDataClockTiming() timing tuning failed for FE id = %d", fe_id);
		throw Cbc3BeException(tmp, Cbc3BeException::DAQ_ERR);
		good = false;
		break;
	    }
	    usleep(1);
	    dctt_fsm = read_ipbus( fBoard, cnode );
	}
	sprintf( tmp, "---- CBC data clock timing tuning result for FE id = %d and CBC id = %d, FCI delay = %d ----\n", fe_id, cbc_id, fci[fe_id]); 
	cout << tmp;
	sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.mmcme2_drp_saddr", fe_id );
	unsigned saddr = read_ipbus( fBoard, cnode );
	sprintf( cnode, "cs_stat.io.cbc_data.timing_tuning.fe%d.bitslip_counter", fe_id );
	unsigned bitslip = read_ipbus( fBoard, cnode );
	sprintf( cnode, "cs_stat.io.cbc_data.iserdes.cbc%d.slvs5", cbc_id );
	unsigned slvs5 = read_ipbus( fBoard, cnode );
	sprintf( cnode, "cs_stat.io.cbc_data.iserdes.cbc%d.in_delay_tap_out", cbc_id );
	unsigned idelay_tap = read_ipbus( fBoard, cnode );
	sprintf(tmp, "SADDR = %d, bitslip counter = %d, idelay taps = %d, slvs5 pattern = 0x%02x\n", saddr, bitslip, idelay_tap, slvs5);
	cout << tmp;

	rwCbcI2cRegsByName(1,1,"VCTH.vcth", vcth[fe_id], cbc_id);
    }

    return good;
}
void Cbc3BeFc7::InitializeRun()
{

    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFc7::InitializeRun() begins" << endl;

    ReadBeCbcConfig();

    //Set BE firmware trigger latencies from real time CBC I2C REGISTER values 
    unsigned trig_lat(0);
    for( unsigned cbc_id = 1; cbc_id <= fNCbcs; cbc_id++ ){
	if( ! fCbcActiveFlags[cbc_id] ) continue;

	rwCbcI2cRegsByName(1,0,"FcCntrlCompBetaTrigLat.trig_lat", trig_lat, cbc_id );

	char cnode[256];
	sprintf( cnode, "cs_cnfg.cbc_dp.cbc%d.latencies.l1a", cbc_id );
	write_ipbus( fBoard, cnode, trig_lat ); 
	if(fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT ) ) cout << "Cbc3BeFc7::InitializeRun() Trigger Latency = " << std::dec << trig_lat << " for CBC ID = " << cbc_id << endl; 
    }

    SendFastSignal( "reset" ); 

    string node;
    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.stop_trigger";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.global.daq_reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.rdb.reset";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.start_trigger";
    write_ipbus( fBoard, node, 1 );

    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFc7::InitializeRun() ends" << endl;
}

void Cbc3BeFc7::FinalizeRun()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::FinalizeRun() begins" << endl;
    string node;
    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.stop_trigger";
    write_ipbus( fBoard, node, 1 );

    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::FinalizeRun() ends" << endl;
}

void Cbc3BeFc7::StartTrigger()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::StartTrigger()" << endl;
    string node;
    node = "cs_ctrl.fcm.start_trigger";
    write_ipbus( fBoard, node, 1 );
}

void Cbc3BeFc7::StopTrigger()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::StopTrigger()" << endl;
    string node;
    node = "cs_ctrl.fcm.stop_trigger";
    write_ipbus( fBoard, node, 1 );
}
void Cbc3BeFc7::ResetFscBramController()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::ResetFscBramController()" << endl;
    write_ipbus( fBoard, "cs_ctrl.seq.reset", 1 );
}
void Cbc3BeFc7::StartFscBramController()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::StartFscBramController()" << endl;
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int fsm = read_ipbus( fBoard, "cs_stat.seq.fsm" );
    while( fsm != 2 ){  
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    cerr << "sequencer is not ready.  Timeout with fsm = " << fsm << endl;
	    return;
	}
	usleep(1);
	fsm = read_ipbus( fBoard, "cs_stat.seq.fsm" );
    }
    write_ipbus( fBoard, "cs_ctrl.seq.start", 1 );
}
bool Cbc3BeFc7::WaitSequencerReady(int timeout_sec){

    ostringstream oss;
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int fsm = ReadBeBoardRegister( "cs_stat.seq.fsm" );
    while( fsm != 2 ){  
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > timeout_sec ){
	    unsigned kmm_timer = read_ipbus(fBoard, "cs_stat.seq.keithley_cmp_timer");
	    cerr << "Sequencer is stuck with fsm = " << fsm << " KMM timer = " << kmm_timer << endl;
	    for( unsigned fe_id = 1; fe_id <= fNFes; fe_id++ ){
		    check_cbci2c_reply_fifo(fBoard, cerr, fe_id, 1);
	    }
	    throw Cbc3BeException("Cbc3BeFc7::WaitSequencerReady  failed.", Cbc3BeException::SEQ_ERR);
	}
	usleep(1);
	fsm = ReadBeBoardRegister( "cs_stat.seq.fsm" );
//	cout << fsm << endl;
    }
    return true;
}
void Cbc3BeFc7::StartFastSignalGenerator()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::StartFastSignalGenerator()" << endl;
    string node;
    /*
    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
    */
    node = "cs_ctrl.fcm.fcg_load_config";
    write_ipbus( fBoard, node, 1 );
    node = "cs_ctrl.fcm.fcg_start";
    write_ipbus( fBoard, node, 1 );
}
void Cbc3BeFc7::StopFastSignalGenerator()
{
    if( fDebug & ( 1 << DEBUG_CBC3BEFC7_BIT )  ) cout << "Cbc3BeFC7::StopFastSignalGenerator()" << endl;
    string node;
    node = "cs_ctrl.fcm.fcg_stop";
    write_ipbus( fBoard, node, 1 );
}

int Cbc3BeFc7::ReadRawData( uint32_t*& Data, int nword, bool dcheck )
{
    if( dcheck){
	struct timeval t0, t, dt;
	gettimeofday(&t0,0);
	bool read_ready = read_ipbus( fBoard, "cs_stat.rdb.read_ready" );
	while( !read_ready ){  
	    //cout << "raw data is not ready yet!" << endl;
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > 1 ){
		cerr << "raw data buffer is not ready to read. Timeout." << endl;
		return -1;
	    }
	    usleep(1);
	    read_ready = read_ipbus( fBoard, "cs_stat.rdb.read_ready" );
	}
    }
    else{
	usleep(nword/80+1);
    }
    string node = "rdb";
    uhal::ValVector<uint32_t> data = fBoard->getNode(node).readBlock( nword );
    fBoard->dispatch();
    //usleep(10);
    for( unsigned i=0; i < data.size(); i++ ){
	Data[i] = data.at(i); 
    }
    return data.size();
}
