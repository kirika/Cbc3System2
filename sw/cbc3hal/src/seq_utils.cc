#include "seq_utils.h"
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "CbcI2cRegIpbusInterface.h"

using namespace std;

namespace cbc3hal{

    //no readback
    void mk_seqdata_channel_mask( const std::map<CHANNEL_ID, unsigned> &msks, std::vector<uint32_t> &seq_data, int value0 ){ 

	std::map<int,int> regs;
	bool nni_top(false);
	bool nni_bot(false);
	map<CHANNEL_ID,unsigned>::const_iterator it_msks = msks.begin();
	for(; it_msks != msks.end(); it_msks++){ 
	    int ch = it_msks->first;
	    int msk = it_msks->second;
	    int addr(0), bit(0);
	    if(ch < 1){
		if(-1<=ch){
		    addr = 0x55;
		    bit = 1-abs(ch);
		    if(msk)nni_bot = true;
		    /*
		    addr = 0x55;
		    bit = 0; 
		    if(msk)nni_bot = true;
		    */
		    /*
		    addr = 0x55;
		    bit = 1; // address 1 is not produced
		    if(msk)nni_bot = true;
		    */
		    /*
		    addr = 0x51;
		    bit = 2;//--no hit added. 
		    if(msk)nni_top = true;
		    */
		    /*
		    addr = 0x51;
		    bit = 0;//--not hit added.
		    if(msk)nni_top = true;
		    */
		}
		else if(-13<=ch){
		    addr = 0x51;
		    bit = abs(ch) - 10; 
		    if(msk)nni_top = true;
		}
		else if(-32<=ch){
		    int BotIn_B_bit = 11-(abs(ch) - 20);
		    if(msk)nni_bot = true;
		    if(BotIn_B_bit < 6) {
			addr = 0x55;
			bit = BotIn_B_bit + 2;
		    }
		    else{
			addr = 0x56;
			bit = BotIn_B_bit - 6;
		    }
		}
		else if(-51<=ch){
		    int TopIn_B_bit = abs(ch) - 40;
		    if(msk)nni_top = true;
		    if(TopIn_B_bit < 5){
			addr = 0x51;
			bit = TopIn_B_bit + 3;
		    }
		    else{
			addr = 0x52;
			bit = TopIn_B_bit - 5;
		    }
		}
	    }
	    else{
		addr = 0x20 + (ch-1) / 8;
		bit = (ch-1) % 8;
		//cout << ch << " " << bit << " " << endl;
	    }
	    int value;
	    if( regs.find(addr) == regs.end() )
		value = value0; 
	    else
		value = regs[addr];
	    regs[addr] = msk ? (1<<bit) | value : ~(1<<bit) & value;

	}
	int addr(0x53);
	int value;
	if( regs.find(addr) == regs.end() )
	    value = value0; 
	else
	    value = regs[addr];
	regs[addr] = nni_top ? 3 | value : ~(3) & value;

	addr=0x56;
	if( regs.find(addr) == regs.end() )
	    value = value0; 
	else
	    value = regs[addr];
	regs[addr] = nni_bot ? (3<<6) | value : ~(3<<6) & value;

	map<int,int>::iterator it_regs = regs.begin();
	for(;it_regs != regs.end(); it_regs++){
	    int a = it_regs->first;
	    int v = it_regs->second;
	    unsigned value = 0x81100000 | (a << 8) | v;
	    seq_data.push_back(value);
	}
    }
    //no readback
    void mk_seqdata_write_vcth( unsigned vcth, std::vector<uint32_t> &seq_data ){ 

	CBCI2C_TYPE_DATA_MAP type_data_map;
	CBCI2C_TYPE_DATA type_data;
	string name("VCTH.vcth");
	get_cbci2c_type_data( name,vcth, &type_data );
	type_data_map["VCTH"] = type_data.second;
	CBCI2C_TYPE_DATA_MAP::iterator it = type_data_map.begin();

	CBCI2C_ADDR_DATA_MAP reg_values0;
	map<unsigned,unsigned> regs = cbci2creg_formatter::getI2cRegValuesToWrite( *it, reg_values0 ); 
	map<unsigned,unsigned>::iterator it_regs = regs.begin();
	for(;it_regs != regs.end(); it_regs++){
	    unsigned a = it_regs->first;
	    unsigned v = it_regs->second;
	    unsigned value = 0x81100000 | (a << 8) | v;
	    seq_data.push_back(value);
	}
    }

    int mk_seqdata_file( std::string fname, std::vector<uint32_t> &seq_data )
    {
	ifstream cfile(fname.c_str());
	char line[256];

	if( cfile.is_open() ){
	    while( !cfile.eof() ){
		cfile.getline( line, 256 );
		//|     |           cout << line << endl;
		if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		    //|         if( w == 1 && line[0] == '#' ) cout << line << endl;
		    continue;
		}
		istringstream iss( line );
		string com, values_str;
		iss >> com >> values_str;
		//	    cout << com << endl;
		if( com == "UNMASK_CH" ){

		    map<CHANNEL_ID, unsigned> msks;
		    for(int ch=1; ch < 255; ch++) msks[ch] = 0;
		    vector<CHANNEL_ID> um_list;

		    string list = rstr(values_str, ",", " ");
		    stringstream values_ss( list );
		    //cout << list << endl;
		    string item;
		    while( !values_ss.eof() ){
			values_ss >> item;
			um_list.push_back(atoi(item.c_str()));
		    }
		    for(unsigned i=0; i < um_list.size(); i++){
			int c = um_list.at(i); 
			msks[c] = 1;
		    }
		    mk_seqdata_channel_mask( msks, seq_data);
		    seq_data.push_back((unsigned) 0x22000000);
		}
		if( com == "COM" ){
		    unsigned uval = strtoul(values_str.c_str(), 0, 0);  
		    seq_data.push_back(uval);
		}
	    }
	    cfile.close();
	}
	else{
	    cerr << "File " << fname << " could not be opened." << endl;
	    return -1;
	}
	return 0;
    }
    void dump_seq_data( std::vector<uint32_t> &data, std::ostream &os ){
	os << "=================" << endl;
	os << "Sequencer data   " << endl;
	os << "-----------------" << endl;
	for(unsigned i=0; i < data.size(); i++){

	    unsigned com = data.at(i);
	    string comment;

	    unsigned mask(0);
	    string com_str;
	    unsigned com_type_mask = 0xe0000000;

	    mask = 0xa0000000;

	    if( ( com & com_type_mask ) == mask ){

		comment = "| Fast command ";

		mask = 0xa0001000;
		com_str = ": fast reset ";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0xa0000200;
		com_str = ": buffer recording start ";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0xa0000100;
		com_str = ": T1 ";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0xa0000010;
		com_str = ": test pulse ";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0xa0000001;
		com_str = ": orbit reset ";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0xa000FFFF;
		com_str = ": pause";
		if( ( com & mask ) == 0xa0000000 ) comment += com_str;
	    }

	    mask = 0x80000000;
	    if( ( com & com_type_mask ) == mask ){
		comment = "| I2C command  ";
	    }

	    mask = 0x20000000;
	    if( ( com & com_type_mask ) == mask ){

		comment = "| Pause        ";
		mask = 0x24000000;
		com_str = ": until RDB write ready.";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0x22000000;
		com_str = ": until I2C bus becomes idle.";
		if( ( com & mask ) == mask ) comment += com_str;

		mask = 0x21000000;
		com_str = ": until Resume signal is received.";
		if( ( com & mask ) == mask ) comment += com_str;
	    }

	    mask = 0xe0000000;
	    if( ( com & com_type_mask ) == mask ){
		comment = "| End of data";
	    }

	    os << setbase(16) << setfill('0') << setw(8) << data.at(i) << " " << comment << endl; 
	}
	os << "=================" <<endl;
	/*
	for(unsigned i=0; i < data.size(); i++){
	    os << setbase(16) << setfill('0') << setw(8) << data.at(i) << endl; 
	}
	*/
    }
}
