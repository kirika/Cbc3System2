/*
 * Author : Kirika Uchida
 */
#ifndef __CBCI2CREGIPBUSINTERFACE_H__
#define __CBCI2CREGIPBUSINTERFACE_H__

#include <fstream>
#include <uhal/uhal.hpp>

#define CBC_I2C_COMMAND_BUS_ID_BIT_OFFSET      29
#define CBC_I2C_COMMAND_BUS_ID_BIT_MASK        0x00000007 
#define CBC_I2C_COMMAND_CBC_ID_BIT_OFFSET      24
#define CBC_I2C_COMMAND_CBC_ID_BIT_MASK        0x0000001F 
#define CBC_I2C_COMMAND_READ_BIT_OFFSET        21 
#define CBC_I2C_COMMAND_READ_BIT_MASK          0x00000001 
#define CBC_I2C_COMMAND_WRITE_BIT_OFFSET       20 
#define CBC_I2C_COMMAND_WRITE_BIT_MASK         0x00000001 
#define CBC_I2C_COMMAND_PAGE_BIT_OFFSET        16 
#define CBC_I2C_COMMAND_PAGE_BIT_MASK          0x00000001 
#define CBC_I2C_COMMAND_ADDR_BIT_OFFSET         8 
#define CBC_I2C_COMMAND_ADDR_BIT_MASK          0x000000FF 
#define CBC_I2C_COMMAND_VALUE_BIT_OFFSET        0 
#define CBC_I2C_COMMAND_VALUE_BIT_MASK         0x000000FF 

#define CBC_I2C_REPLY_BUS_ID_BIT_OFFSET        29
#define CBC_I2C_REPLY_BUS_ID_BIT_MASK          0xE0000000  
#define CBC_I2C_REPLY_CBC_ID_BIT_OFFSET        24
#define CBC_I2C_REPLY_CBC_ID_BIT_MASK          0x1F000000 
#define CBC_I2C_REPLY_INFO_BIT_OFFSET          20 
#define CBC_I2C_REPLY_INFO_BIT_MASK            0x00100000 
#define CBC_I2C_REPLY_RW_FLAG_BIT_OFFSET       17 
#define CBC_I2C_REPLY_RW_FLAG_BIT_MASK         0x00020000 
#define CBC_I2C_REPLY_PAGE_BIT_OFFSET          16 
#define CBC_I2C_REPLY_PAGE_BIT_MASK            0x00010000 
#define CBC_I2C_REPLY_ADDR_BIT_OFFSET           8 
#define CBC_I2C_REPLY_ADDR_BIT_MASK            0x0000FF00 
#define CBC_I2C_REPLY_VALUE_BIT_OFFSET          0 
#define CBC_I2C_REPLY_VALUE_BIT_MASK           0x000000FF 

#define CBCI2C_COMMAND( r, w, page, addr, value, fe_id, cbc_id ) \
    ( \
      ( (fe_id&CBC_I2C_COMMAND_BUS_ID_BIT_MASK) << CBC_I2C_COMMAND_BUS_ID_BIT_OFFSET) \
      | ( (cbc_id&CBC_I2C_COMMAND_CBC_ID_BIT_MASK) << CBC_I2C_COMMAND_CBC_ID_BIT_OFFSET) \
      | ( (r&CBC_I2C_COMMAND_READ_BIT_MASK) << CBC_I2C_COMMAND_READ_BIT_OFFSET) \
      | ( (w&CBC_I2C_COMMAND_WRITE_BIT_MASK) << CBC_I2C_COMMAND_WRITE_BIT_OFFSET) \
      | ( ((page-1)&CBC_I2C_COMMAND_PAGE_BIT_MASK) << CBC_I2C_COMMAND_PAGE_BIT_OFFSET) \
      | ( (addr&CBC_I2C_COMMAND_ADDR_BIT_MASK) << CBC_I2C_COMMAND_ADDR_BIT_OFFSET) \
      | ( (value&CBC_I2C_COMMAND_VALUE_BIT_MASK) << CBC_I2C_COMMAND_VALUE_BIT_OFFSET) )

#define CBCI2C_REPLY_BUS_ID (r)  ( (r&CBC_I2C_REPLY_BUS_ID_BIT_MASK) >> CBC_I2C_REPLY_BUS_ID_BIT_OFFSET )  
#define CBCI2C_REPLY_CBC_ID (r)  ( (r&CBC_I2C_REPLY_CBC_ID_BIT_MASK) >> CBC_I2C_REPLY_CBC_ID_BIT_OFFSET )
#define CBCI2C_REPLY_INFO   (r)  ( (r&CBC_I2C_REPLY_INFO_BIT_MASK)   >> CBC_I2C_REPLY_INFO_BIT_OFFSET )
#define CBCI2C_REPLY_RW_FLAG(r)  ( (r&CBC_I2C_REPLY_RW_FLAG_BIT_MASK)>> CBC_I2C_REPLY_RW_FLAG_BIT_OFFSET ) 
#define CBCI2C_REPLY_PAGE   (r)  (((r&CBC_I2C_REPLY_PAGE_BIT_MASK) >> CBC_I2C_REPLY_PAGE_BIT_OFFSET) + 1) 
#define CBCI2C_REPLY_ADDR   (r)  ( (r&CBC_I2C_REPLY_ADDR_BIT_MASK)   >> CBC_I2C_REPLY_ADDR_BIT_OFFSET )
#define CBCI2C_REPLY_VALUE  (r)  ( (r&CBC_I2C_REPLY_VALUE_BIT_MASK)  >> CBC_I2C_REPLY_VALUE_BIT_OFFSET )

#define CBCI2C_REPLY_REGID (r) ( \
	r&(CBC_I2C_REPLY_BUS_ID_BIT_MASK|CBC_I2C_REPLY_CBC_ID_BIT_MASK|CBC_I2C_REPLY_PAGE_BIT_MASK|CBC_I2C_REPLY_ADDR_BIT_MASK)

namespace cbc3hal{

	extern int reply_fifo_ndata_sum;

	class CbcI2cRegIpbusException : public std::exception{
	    public:
		CbcI2cRegIpbusException()throw(){}
		CbcI2cRegIpbusException( const std::string &estr )throw(){err_str = estr; }
		CbcI2cRegIpbusException(const CbcI2cRegIpbusException &e)throw(){ err_str = e.what(); }
		CbcI2cRegIpbusException& operator= (const CbcI2cRegIpbusException &e) throw(){ err_str = e.what(); return *this; }
		virtual ~CbcI2cRegIpbusException()throw(){}
		virtual const char * what() const throw(){ return err_str.c_str(); }
	    private:
		std::string err_str;
	};


	typedef uint32_t CBCI2C_REPLY;
	int get_bus_id (CBCI2C_REPLY r);  
	int get_cbc_id (CBCI2C_REPLY r);
	int get_info   (CBCI2C_REPLY r);
	int get_rw_flag(CBCI2C_REPLY r); 
	int get_page   (CBCI2C_REPLY r); 
	int get_addr   (CBCI2C_REPLY r);
	int get_val    (CBCI2C_REPLY r);

	typedef CBCI2C_REPLY CBCI2C_REGID;
	CBCI2C_REGID get_regid(CBCI2C_REPLY r);
	CBCI2C_REGID get_regid( unsigned bus_id, unsigned cbc_id, unsigned page, unsigned addr );

	/* 
	 * CBCI2C_REGS : Map to store I2C transaction replies
	 */
	typedef std::map<CBCI2C_REGID, CBCI2C_REPLY> CBCI2C_REGS;

	typedef std::string CBCI2C_TYPE;
	typedef std::string CBCI2C_ITEM;
	typedef unsigned    CBCI2C_DATA;
	typedef unsigned    CBCI2C_ADDR;

	typedef std::map<CBCI2C_TYPE,  std::set<CBCI2C_ITEM> >         CBCI2C_NAMES;
	typedef std::pair<CBCI2C_ITEM,  CBCI2C_DATA>               CBCI2C_ITEM_DATA;
	typedef std::map<CBCI2C_ITEM,  CBCI2C_DATA>            CBCI2C_ITEM_DATA_MAP;
	typedef std::pair<CBCI2C_TYPE, CBCI2C_ITEM_DATA_MAP>       CBCI2C_TYPE_DATA;
	typedef std::map<CBCI2C_TYPE, CBCI2C_ITEM_DATA_MAP>    CBCI2C_TYPE_DATA_MAP;
	typedef std::map<CBCI2C_ADDR,  CBCI2C_DATA >           CBCI2C_ADDR_DATA_MAP;

	void get_cbci2c_type_data( std::string &name, unsigned value, CBCI2C_TYPE_DATA *type_data );
	void get_cbci2c_type_data( std::string &name, unsigned value, CBCI2C_TYPE_DATA_MAP *map );
	void get_cbci2c_name_value_map( const CBCI2C_TYPE_DATA_MAP &map, std::map<std::string,unsigned> *name_value_map );
	void get_cbci2c_type_data_map( const std::map<std::string,unsigned> &name_value_map, CBCI2C_TYPE_DATA_MAP *map );
	void get_cbci2c_type_data_map( const std::string &filename, CBCI2C_TYPE_DATA_MAP *map );
	std::string get_title_regcnf( const std::string &filename );
	void print_preamble( const std::string &title, std::ostream &os );
	std::map<std::string,unsigned> get_name_regcnf( const std::string &filename, bool CBC3_0 = true );
	void print_name_regcnf( const std::string &filename, std::ostream &os );
	void print_name_regcnf( const std::vector<std::string> &filenames, std::ostream &os );
	void print_regcnf( const std::string &filename, std::ostream &os, bool CBC3_0 = true );
	void print_regcnf( const std::vector<std::string> &filenames, std::ostream &os, bool CBC3_0 = true);
	void dump_cbci2c_type_data( const CBCI2C_TYPE_DATA &type_data, std::ostream &os);  
	void dump_cbci2c_type_data_map( const CBCI2C_TYPE_DATA_MAP &map, std::ostream &os);  

	class cbci2creg_formatter{
	    public:
		static const CBCI2C_NAMES&               getNames(){ return fNames; }
		static std::set<CBCI2C_TYPE>             getTypeNames();
		static const std::set<CBCI2C_ITEM>&      getItemNames(const CBCI2C_TYPE &type);
		static void printNames(std::ostream &os, bool ofst = true );
		static void printTypeNames(std::ostream &os );
		static void printItemNames(const CBCI2C_TYPE &type, std::ostream &os);
		static std::vector<unsigned> getAddrs( const std::string &type );
		static std::vector<unsigned> getReadAddrs( const CBCI2C_TYPE_DATA &type_data );
		static std::vector<unsigned> getReadAddrsToWrite( const CBCI2C_TYPE_DATA &type_data );
		static CBCI2C_ADDR_DATA_MAP   getI2cRegValuesToWrite(const CBCI2C_TYPE_DATA &type_data, const CBCI2C_ADDR_DATA_MAP &reg_values0, bool CBC3_0 = true);
		static void                  getValues( CBCI2C_TYPE_DATA &type_data, const CBCI2C_ADDR_DATA_MAP &reg_addr_values, bool CBC3_0 = true );
		static CBCI2C_NAMES InitNames();

	    private:
		static CBCI2C_NAMES fNames;
		cbci2creg_formatter(){}
	};

	struct cbc_i2c_command{
	    unsigned cbc_id;
	    unsigned w;
	    unsigned r;
	    unsigned page;
	    unsigned addr;
	    unsigned value;
	};
	typedef unsigned FE_ID;
	typedef unsigned CBC_ID;
	typedef unsigned GROUP_ID;
	typedef unsigned CHANNEL_ID;

	/*
	 * IPBUS command generation. 
	 */
	 /* returns a ipbus word for CBC I2C register read/write commands from primitive data. */
	uint32_t get_ipbus_command_i2cregrw( unsigned w, unsigned r, unsigned page, unsigned addr, unsigned value=0, unsigned fe_id=1, unsigned cbc_id=1 );
	 /* returns a list of ipbus words for CBC I2C register read/write commands from a setting file. */ 
	void get_ipbus_commands_i2cregrw( unsigned w, unsigned r, const std::string &filename, std::vector<uint32_t> &commands, unsigned &nreplies, unsigned fe_id=1, unsigned cbc_id=1 ); 


	/*
	 * functions for CBCI2C_REPLY objects
	 */
	/* functions for CBCI2C_REPLY */
	void dump_reply( const CBCI2C_REPLY &reply, std::ostream &os );
	void dump_replies( const std::vector<CBCI2C_REPLY> &reply, std::ostream &os );
	bool good_replies( const std::vector<CBCI2C_REPLY> &replies);
	/* functions for CBCI2C_REGS */
	void dump_replies( const CBCI2C_REGS &regs, std::ostream &os ); 
	/* formatted print out of CBCI2C_REGS for a specific CBC. */
	void print_regcnf( const CBCI2C_REGS &regs, std::ostream &os, int cbc_id ); 
	/* write the CBCI2C_REGS information to database for a specific BE*/
	bool write_postgresql( unsigned be_id, const CBCI2C_REGS &regs, std::string conninfo = "dbname=cbc3");

	bool check_rw_replies(CBCI2C_REPLY w_reply, CBCI2C_REPLY r_reply, std::ostream &os = std::cout );

	//fe_id and cbc_id != 0
	std::map<CHANNEL_ID, unsigned> get_chmasks( const CBCI2C_REGS &regs, FE_ID fe_id=1, CBC_ID cbc_id = 1);
	bool get_comlist_for_chmask( CHANNEL_ID channel_id, unsigned value, std::vector<struct cbc_i2c_command> &comlist, FE_ID fe_id=1, CBC_ID cbc_id = 1);

	/*
	 * Functions for CBC I2C operation through IPBUS
	 */
	/* CBC ID & Frontend ID mapping is read from the backend configuration.*/ 
	std::map<unsigned,unsigned> get_cbcfeids(uhal::HwInterface *hw);
	/* I2C bus manager initialization function. */
	bool init_cbc_i2c_bus_manager( uhal::HwInterface *hw, CBCI2C_REGS *pregs=0, bool debug=false );
	/* to check the bus is not busy */
	bool is_cbci2c_ready( uhal::HwInterface *hw, unsigned fe_id, bool debug = false );
	/* wait until cbci2c bus becomes ready. called before sending IPBUS commands in rw_cbc_i2c_regs() */
	bool wait_for_cbci2c_ready( uhal::HwInterface *hw, unsigned fe_id, int timeout_usec = 1000, bool debug = false );

	/* 
	 * cbc i2c register read write functions.  cbc_id broadcast is implemented with cbc_id = 0.  Those functions which does not support cbc_id = 0 or fe_id =0 are
	 * stated explicitely below.
	 */


	 /* 
	  * I2C communication & write and read consistency are checked. 
	  */
	/* cbc_id != 0 */
	bool rw_cbc_i2c_regs( uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, CBCI2C_REGS &regs, unsigned cbc_id, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true);

	/* cbc_id != 0 
	 * offset vector for 254 channels must be filled in the order of channel 1 to 254 for write.
	 * offset vector can be 0 for read only.
	 */
	bool rw_cbc_i2c_Offsets( uhal::HwInterface *hw, unsigned r, unsigned w, std::vector<unsigned> &offsets, CBCI2C_REGS &regs, unsigned cbc_id=1, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true );
	/* cbc_id != 0
	 * chofst_map is filled with channel, offset pairs.
	 */
	bool rw_cbc_i2c_Offsets( uhal::HwInterface *hw, unsigned r, unsigned w, std::map<CHANNEL_ID,unsigned> &chofst_map, CBCI2C_REGS &regs, unsigned cbc_id=1,
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true );
	/* cbc_id != 0
	 * ch=0 for all channels. offset from the function is not valid for ch=0 and cbc_id=0
	 */
	bool rw_cbc_i2c_Offset( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned ch, unsigned &offset, CBCI2C_REGS &regs, unsigned cbc_id=1, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true ); 

	/* cbc_id != 0 */
	bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, std::string name, unsigned &value, CBCI2C_REGS &regs, unsigned cbc_id=1,
		const std::map<unsigned,unsigned> *cbcfeids=0, bool debug=true, bool CBC3_0 = true );
	bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, std::map<std::string,unsigned> &name_value_map, CBCI2C_REGS &regs, unsigned cbc_id=1,
	    const std::map<unsigned,unsigned> *cbcfeids=0, bool debug=true, bool CBC3_0 = true );
	bool rw_cbc_i2c_regs_by_name(uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, CBCI2C_REGS &regs, unsigned cbc_id=1, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true, bool CBC3_0 = true );

	//bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &type, CBCI2C_ITEM_DATA_MAP &item_data_map, CBCI2C_REGS &regs, unsigned cbc_id=1,  
	bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, CBCI2C_TYPE_DATA &type_data, CBCI2C_REGS &regs, unsigned cbc_id=1,  
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true, bool CBC3_0 = true );

	bool rw_cbc_i2c_regs_by_type(uhal::HwInterface *hw, unsigned r, unsigned w, std::map<CBC_ID, CBCI2C_TYPE_DATA> &cbc_id_type_data, CBCI2C_REGS &regs, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=true, bool CBC3_0 = true );

	/* i2c error flag from the firmare and readback value for r=1 & w=1 are checked */ 
	bool rw_cbc_i2c_reg( uhal::HwInterface *hw, struct cbc_i2c_command &com, CBCI2C_REGS &regs, const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug=false );

	/* writeBlock_ipbus() is called */
	/* i2c error flag from the firmare and readback value for r=1 & w=1 are checked */ 
	bool rw_cbc_i2c_regs( uhal::HwInterface *hw, const std::vector<struct cbc_i2c_command> &commands, CBCI2C_REGS &regs, const std::map<CBC_ID,FE_ID> *cbcfeids, bool debug=false );
	/* write_ipbus() is called */ 
	std::vector<CBCI2C_REPLY> rw_cbc_i2c_reg( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value=0, unsigned cbc_id=1,
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=false ); 
	/* fe_id != 0 writeBlock_ipbus() is called */
	std::vector<CBCI2C_REPLY> rw_cbc_i2c_regs( uhal::HwInterface *hw, const std::vector<uint32_t> &ipbus_commands, int nreplies = -1, unsigned fe_id=1, bool debug=false ); 

	/* cbc_id != 0 */
	std::vector<CBCI2C_REPLY> rw_cbc_i2c_regs( uhal::HwInterface *hw, unsigned r, unsigned w, const std::string &filename, unsigned cbc_id=1, 
		const std::map<CBC_ID,FE_ID> *cbcfeids=0, bool debug=false ); 

	/* write_ipbus() is called */
	void send_cbc_i2c_command( uhal::HwInterface *hw, unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value=0, unsigned cbc_id=1,
		const std::map<CBC_ID,FE_ID> *cbcfeids=0 );
	/* fe_id !=0 */
	/* readBlock_ipbus() is called */
	std::vector<CBCI2C_REPLY> read_cbc_i2c_replies(uhal::HwInterface *hw, unsigned fe_id=1, int nreplies = -1);

	void check_cbci2c_reply_fifo( uhal::HwInterface *hw, std::ostream &os, unsigned bus_id=1, int debug = 0 );

}
#endif

