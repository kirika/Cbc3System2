/*
 * Author : Kirika Uchida
 */

#ifndef __SEQ_UTILS_H__
#define __SEQ_UTILS_H__

#include <stdint.h>
#include <map>
#include <vector>
#include <string>
#include "analysis_utils.h"

namespace cbc3hal{

    //no readback
    void mk_seqdata_channel_mask( const std::map<CHANNEL_ID, unsigned> &msks, std::vector<uint32_t> &seq_data, int value0 = 0 ); 
    void mk_seqdata_write_vcth( unsigned vcth, std::vector<uint32_t> &seq_data ); 

    int mk_seqdata_file( std::string fname, std::vector<uint32_t> &seq_data );
    void dump_seq_data( std::vector<uint32_t> &data, std::ostream &os );
};

#endif

