#include "Cbc3BeIpbusInterface.h"
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include <cstdlib>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

Cbc3BeIpbusInterface::~Cbc3BeIpbusInterface(){ delete fBoard; }
void Cbc3BeIpbusInterface::Initialize( const std::string& connection_file, const std::string& id )
{
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
	cout << "**** Cbc3BeIpbusInterface::Initialize() ****" << endl;
	cout << "============================================" << endl;
	cout << " CONNECTION FILE : " << connection_file << endl;
	char tmp[256];
	sprintf( tmp, "cat %s", connection_file.c_str() );
	system(tmp); 
	cout << "--------------------------------------------" << endl;
	cout << " BOARD ID        : " << id << endl;
	cout << "============================================" << endl;
    }
    ConnectionManager cm( string( "file://" ) + connection_file ); 
    fBoard = new HwInterface( cm.getDevice( id ) );

    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ){
	cout << "********************************************" << endl;
    }
}
void Cbc3BeIpbusInterface::WriteBeBoardConfig( const std::string &node, uint32_t value )
{
	write_ipbus( fBoard, node, value );
	ReadBeCbcConfig();
}
void Cbc3BeIpbusInterface::WriteBeBoardConfig( std::map<std::string, uint32_t>&regmap )
{
	write_ipbus( fBoard, regmap );
	ReadBeCbcConfig();
}
void Cbc3BeIpbusInterface::WriteBeBoardConfig( const std::string &fname )
{
	//cout << "Cbc3BeIpbusInterface::WriteBeBoardConfig() : file = " << fname << endl;
	write_ipbus( fBoard, fname );
	ReadBeCbcConfig();
}
void Cbc3BeIpbusInterface::ReadBeBoardConfig( const std::string &fname, std::map<std::string,uint32_t> &regmap )
{
	read_ipbus( fBoard, fname, regmap );
}
unsigned Cbc3BeIpbusInterface::ReadBeBoardRegister( std::string node )
{
    return read_ipbus( fBoard, node );
}
void Cbc3BeIpbusInterface::ReadBeBoardRegisters( std::vector<std::string> &regnodes, std::vector<uint32_t> &values )
{
    read_ipbus( fBoard, regnodes, values );
}

bool Cbc3BeIpbusInterface::CbcI2cBusInit() 
{
    if( fDebug & ( 1 << DEBUG_CBC3BEINTERFACE_BIT )  ) cout << "**** Cbc3BeIpbusInterface::CbcI2cBusInit ****" << endl;
    
    CBCI2C_REGS regs;
    init_cbc_i2c_bus_manager( fBoard, &regs, fDebug & ( 1 << DEBUG_CBCI2C_BIT ) );

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return true;
}

CBCI2C_TYPE_DATA_MAP Cbc3BeIpbusInterface::readCbcAllI2cRegsByType( unsigned cbc_id )
{
    CBCI2C_REGS regs;
    CBCI2C_TYPE_DATA_MAP cbci2c_type_data_map;
    const std::set<string> types = cbci2creg_formatter::getTypeNames();
    set<string>::const_iterator it_types = types.begin(); 
    for(; it_types != types.end(); it_types++){ 
	string type = *it_types;
	CBCI2C_ITEM_DATA_MAP item_data;
	CBCI2C_TYPE_DATA type_data(*it_types, item_data);
	//no readback check since this is a read only transction.
	rw_cbc_i2c_regs_by_type(fBoard, 1, 0, type_data, regs, cbc_id, GetCbcFeIds(), fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0 );
	cbci2c_type_data_map.insert(type_data);
    }
    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;
    return cbci2c_type_data_map; 
}
bool Cbc3BeIpbusInterface::rwCbcI2cReg(unsigned r, unsigned w, unsigned page, unsigned addr, unsigned &value, unsigned cbc_id )
{
    struct cbc_i2c_command com;
    com.cbc_id = cbc_id;
    com.w = w;
    com.r = r;
    com.page = page;
    com.addr = addr;
    com.value = value;
    CBCI2C_REGS regs;
    bool good = rwCbcI2cReg(com);
    value = com.value;
    return good;
}

bool Cbc3BeIpbusInterface::rwCbcI2cReg(struct cbc_i2c_command &command )
{
    CBCI2C_REGS regs;
    bool good = rw_cbc_i2c_reg(fBoard, command, regs, GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT ) );

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
/* 
 * broadcast is not implemented yet.
 * to do this, I have to implement nactive_cbc register in FPGA 
 * fe_id can be mixed.
 */
bool Cbc3BeIpbusInterface::rwCbcI2cRegs(const std::vector<struct cbc_i2c_command> &commands )
{
    CBCI2C_REGS regs;
    bool good = rw_cbc_i2c_regs(fBoard, commands, regs, GetCbcFeIds(), fDebug & ( 1 << DEBUG_CBCI2C_BIT ));

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
/* 
 * broadcast is not implemented yet.
 */
bool Cbc3BeIpbusInterface::rwCbcI2cRegs(unsigned r, unsigned w, const std::string &filename, unsigned cbc_id )
{

    CBCI2C_REGS regs;
    bool good(true);
    good = rw_cbc_i2c_regs(fBoard,r,w,filename,regs,cbc_id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT )); 
    good = good && rw_cbc_i2c_regs_by_name( fBoard, r,w, filename, regs, cbc_id, GetCbcFeIds(), fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0); 

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2cRegsByName(unsigned r, unsigned w, const string &name, unsigned &value, unsigned cbc_id )
{
    if( fDebug & (1 << DEBUG_CBC3BEIPBUSINTERFACE_BIT ) ){
	cout << "Cbc3BeIpbusInterface::rwCbcI2cRegsByName() : name = " << name << " r = " << r << " w = " << w << " value = " << value << " cbc id = " << cbc_id << endl;
    }
    CBCI2C_REGS regs;
    bool good = rw_cbc_i2c_regs_by_name(fBoard,r,w,name, value, regs,cbc_id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0);

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    if( !good ) cerr << "Cbc3BeIpbusInterface::wrCbcI2cRegsByName() name = " << name << " failed." << endl;
    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2cRegsByType(unsigned r, unsigned w, CBCI2C_TYPE_DATA &type_data, unsigned cbc_id )
{
    CBCI2C_REGS regs;
    bool good = rw_cbc_i2c_regs_by_type(fBoard,r,w,type_data, regs,cbc_id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0);

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2cRegsByType(unsigned r, unsigned w, std::map<unsigned, CBCI2C_TYPE_DATA> &cbc_id_type_data )
{
    CBCI2C_REGS regs;
    bool good = rw_cbc_i2c_regs_by_type(fBoard,r,w,cbc_id_type_data,regs,GetCbcFeIds(), fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0);

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    if( !good ) cerr << "Cbc3BeIpbusInterface::wrCbcI2cRegsByType() failed." << endl;
    return good;
}
bool Cbc3BeIpbusInterface::writeAllCbcI2cRegsByType( const std::string &type, CBCI2C_ITEM_DATA_MAP &item_data )
{
    CBCI2C_REGS regs;

    if( fDebug & ( 1 << DEBUG_CBC3BEIPBUSINTERFACE_BIT) ) cout << "Cbc3BeIpbusInterface::writeAllCbcI2cRegsByType() type = " << type << endl; 
//    cout << "item_data.begin()->first=" << item_data.begin()->first << endl;
    CBCI2C_TYPE_DATA type_data( type, item_data );
    std::map<unsigned, CBCI2C_TYPE_DATA> cbc_id_data;
    cbc_id_data[0] = type_data;
//    cout << "Cbc3BeIpbusInterface() writeAllCbcI2cRegsByType()" << endl;
    bool good = rw_cbc_i2c_regs_by_type(fBoard,1,1,cbc_id_data,regs,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT ), fCBC3_0);

    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }

    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2c_Offset( unsigned r, unsigned w, unsigned ch, unsigned &offset, unsigned cbc_id )
{
    CBCI2C_REGS regs;
    bool good(true);
    unsigned offset_tmp = offset;
    for( unsigned id=1; id <= GetNCbcs(); id++){
	if( cbc_id != 0 && id != cbc_id ) continue;
	if( !IsCbcActive(id) ) continue;
	unsigned offset_tmp = offset;
//	cout << "Cbc3BeIpbusInterface() CBC ID = " << id << endl;
	if( !rw_cbc_i2c_Offset(fBoard,r,w,ch,offset_tmp,regs,id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT )) ) good = false;
    }
    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    offset = offset_tmp;
    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2c_Offsets( unsigned r, unsigned w, std::vector<unsigned> &offsets, unsigned cbc_id )
{
    CBCI2C_REGS regs;
    bool good(true);
    for( unsigned id=1; id <= GetNCbcs(); id++){
	if( cbc_id != 0 && id != cbc_id ) continue;
	if( !IsCbcActive(id) ) continue;
//	cout << "Cbc3BeIpbusInterface() CBC ID = " << id << endl;
	if( !rw_cbc_i2c_Offsets(fBoard,r,w,offsets,regs,id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT )) ) good = false;
    }
    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
bool Cbc3BeIpbusInterface::rwCbcI2c_Offsets( unsigned r, unsigned w, std::map<unsigned,unsigned> &chofst_map, unsigned cbc_id )
{
    CBCI2C_REGS regs;
    bool good(true);
    for( unsigned id=1; id <= GetNCbcs(); id++){
	if( cbc_id != 0 && id != cbc_id ) continue;
	if( !IsCbcActive(id) ) continue;
	if( !rw_cbc_i2c_Offsets(fBoard,r,w,chofst_map,regs,id,GetCbcFeIds(),fDebug & ( 1 << DEBUG_CBCI2C_BIT )) ) good = false;
    }
    CBCI2C_REGS::iterator it = regs.begin();
    for(; it != regs.end(); it++) fCbcI2cRegReplies[it->first] = it->second;

    if( good == true && fWriteCbcI2cRegsToDB ){
	write_postgresql(GetBeId(), regs); 
    }
    return good;
}
std::vector<CBCI2C_REPLY> Cbc3BeIpbusInterface::rwCbcI2c_reg( unsigned r, unsigned w, unsigned page, unsigned addr, unsigned value, unsigned cbc_id ) 
{
    std::vector<CBCI2C_REPLY> replies;
    for( unsigned id=1; id <= GetNCbcs(); id++){
	if( cbc_id != 0 && id != cbc_id ) continue;
	if( !IsCbcActive(id) ) continue;
	std::vector<CBCI2C_REPLY> tmp = rw_cbc_i2c_reg(fBoard,r,w,page,addr,value,id,GetCbcFeIds());
	replies.insert( replies.end(), tmp.begin(), tmp.end() );
    }
    return replies;
}
std::vector<CBCI2C_REPLY> Cbc3BeIpbusInterface::rwCbcI2c_regs( const std::vector<uint32_t> &commands, int nreplies, unsigned fe_id ) 
{
    return rw_cbc_i2c_regs(fBoard,commands,nreplies, fe_id);
}
std::vector<CBCI2C_REPLY> Cbc3BeIpbusInterface::rwCbcI2c_regs( unsigned r, unsigned w, const std::string &filename, unsigned cbc_id ) 
{
    return rw_cbc_i2c_regs(fBoard,r,w,filename, cbc_id,GetCbcFeIds());
}
