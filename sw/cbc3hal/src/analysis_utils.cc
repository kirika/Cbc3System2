#include "analysis_utils.h"
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

namespace cbc3hal{

    unsigned swap_bits( unsigned val, unsigned nbits )
    {
	unsigned sval(0);
	for( unsigned i=0; i<nbits; i++ ) {
	    sval |= ( ( ( val >> i ) & 1 ) << (nbits-1-i) );
	}
	return sval;
    }	
    void cbc_param_tuning_data_update( struct cbc_param_tuning_item &item, int nhits_thr, bool prop ){

	if( item.bit_count >= 0 ){
	    if( ( prop && item.nhits <= nhits_thr ) || ( !prop && item.nhits > nhits_thr) ){
		item.le_thr_value = item.value; 
		item.le_thr_nhits = item.nhits; 
		item.value &= ~( 1 << item.bit_count );
	    }
	    else{
		item.gt_thr_value = item.value; 
		item.gt_thr_nhits = item.nhits; 
	    }
	    if( item.bit_count != 0 ){
		item.value |= ( 1 << (item.bit_count - 1));
	    }
//	    item.nhits = 0;
	}
	else if( item.bit_count == -1 ){
	    float dl =  nhits_thr - item.le_thr_nhits;
	    float dg =  item.le_thr_nhits - nhits_thr;
	    if( dl < dg ){
		item.value = item.le_thr_value;
		item.nhits = item.le_thr_nhits;
	    }
	    else{
		item.value = item.gt_thr_value;
		item.nhits = item.gt_thr_nhits;
	    }
	} 
	else{
	    cerr << "tuning_finished." << endl;
	    return;
	}
	item.bit_count--;
    }

    CbcParamTuningItem::CbcParamTuningItem(unsigned nbits, int nhits_thr, bool prop, bool debug): 
	fNbits(nbits), fNhitsThr(nhits_thr), fProp(prop), fDebug(debug)
    {
	Reset();
    }
    bool CbcParamTuningItem::UpdateParam(){

	if(fItem.bit_count == -2) return true; 

	if(fDebug){
	    cout << "ParamTuningItem::UpdateParam() value before = 0x" << setfill('0') << std::right << std::hex << fItem.value << endl;
	    cout << "bit count = " << std::dec << fItem.bit_count << ", # of hits = " << fItem.nhits << ", thr. = " << fNhitsThr << endl;
	}
	cbc_param_tuning_data_update( fItem, fNhitsThr, fProp ); 
	if(fDebug){
	    cout << "ParamTuningItem::UpdateParam() value after  = 0x" << setfill('0') << std::right << std::hex << fItem.value << " with " << std::dec << fItem.nhits << " hits." << endl;
	}
	if(fItem.bit_count == -2) return true; 
	else return false;
    }	
    void CbcParamTuningItem::Reset(){

	fItem.bit_count = fNbits-1;
	fItem.value = (1 << fItem.bit_count);
	fItem.le_thr_value = pow(2,fItem.bit_count)-1;
	fItem.le_thr_nhits = 0;
	fItem.gt_thr_value = 0;
	fItem.gt_thr_nhits = 0;
    }
    bool CbcParamTuningItem::Tuned()const{
	if(fItem.bit_count == -2) return true; 
	else return false;
    }

    //replace s2 to s3 in s1
    std::string rstr( std::string s1, std::string s2, std::string s3 )
    {
	std::string::size_type  pos( s1.find( s2 ) );

	while( pos != std::string::npos )
	{
	    s1.replace( pos, s2.length(), s3 );
	    pos = s1.find( s2, pos + s3.length() );
	}

	return s1;
    }

}
