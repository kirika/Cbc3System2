#ifndef __ANALYSIS_UTILS_H__
#define __ANALYSIS_UTILS_H__

#include <string>
#include <map>
#include <iostream>

namespace cbc3hal{

    unsigned swap_bits( unsigned val, unsigned nbits );

    typedef std::string HIST_TYPE;
    typedef unsigned BE_ID;
    typedef unsigned CBC_ID;
    typedef unsigned GROUP_ID;
    typedef unsigned CHANNEL_ID;

    struct cbc_param_tuning_item{
	int bit_count;
	unsigned value;
	int nhits;
	unsigned le_thr_value;
	unsigned gt_thr_value;
	int le_thr_nhits;
	int gt_thr_nhits;
    };

    void cbc_param_tuning_data_update( struct param_tuning_item &item, int nhits_thr, bool prop = 1 );

    enum CbcParamNbits      { VcthNbits=10, OffsetNbits=8 };

    class CbcParamTuningItem{

	public:
	    CbcParamTuningItem(){;}
	    CbcParamTuningItem(unsigned nbits, int nhits_thr, bool prop = true, bool debug = false); 
	    ~CbcParamTuningItem(){}
	    void AddHits( int n=1 ){ fItem.nhits += n; }
	    void SetHits( int n ){ fItem.nhits = n; }
	    int GetHits()const{ return fItem.nhits; }
	    bool UpdateParam();
	    void ClearHits(){ fItem.nhits = 0; }
	    void Reset();
	    unsigned GetValue()const{ return fItem.value; }
	    bool Tuned()const;
	    void Debug(bool set=true){ fDebug = true ? set : !set; }
	private:
	    unsigned fNbits;
	    int      fNhitsThr;
	    bool     fProp;
	    struct cbc_param_tuning_item fItem;
	    bool     fDebug;

    };
    template <class T> 
	class CbcHistoAxis{
	    public:
		CbcHistoAxis( unsigned nbins, T min, T max);
		virtual ~CbcHistoAxis(){}
		inline unsigned FindBinIndex(T data);
	    protected:
		unsigned _NBins;
		T _BinWidth;
		T _Min, _Max;
		T * _Axis;
	};

    template <class T> 
	class CbcHistogram{
	public:
	   CbcHistogram();
	   CbcHistogram( const std::string & title, unsigned nbins, T min, T max );
	  virtual ~CbcHistogram(){} 
	protected:
	  T * _Storage;

    };	



    template<class T> 
	class CbcHistList{
	public:
	    CbcHistList():_NHist(0){}
	    virtual ~CbcHistList(){}
	    inline void AddHist( T *hist, const HIST_TYPE &htype, BE_ID be_id=0, CBC_ID cbc_id=0, CHANNEL_ID ch_id=0 );
	    inline T* GetHist( const HIST_TYPE &htype, BE_ID be_id=0, CBC_ID cbc_id=0, CHANNEL_ID ch_id=0 );
	    std::map<unsigned, T *> & GetHistMap(){ return _HistMap; }

	protected:
	    std::map<HIST_TYPE, unsigned>                                                             _GlobalHistIdMap;
	    std::map<HIST_TYPE, std::map<BE_ID, unsigned> >                                           _BeHistIdMap;
	    std::map<HIST_TYPE, std::map<BE_ID, std::map<CBC_ID, unsigned> > >                        _CbcHistIdMap;
	    std::map<HIST_TYPE, std::map<BE_ID, std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > > > _ChannelHistIdMap;
	    std::map<unsigned, T * > _HistMap; 
	    unsigned _NHist;

    };

    template<class T> 
	CbcHistoAxis<T>::CbcHistoAxis( unsigned nbins, T min, T max ): _NBins(nbins), _BinWidth(0), _Min(min), _Max(max), _Axis(0)
	{
	    _BinWidth = (_Max - _Min) / _NBins;
	}

    template<class T> 
	unsigned CbcHistoAxis<T>::FindBinIndex(T data)
	{
	    if (data < _Min) {
		// Underflow
		return 0;
	    }
	    else if (data >= _Max) {
		// Overflow
		return _NBins;
	    }
	    else {
		return (unsigned) ((data - _Min) / _BinWidth) + 1;
	    }
	}
    template<class T>
	CbcHistogram<T>::CbcHistogram( const std::string & title, unsigned nbins, T min, T max )
	{
	    _Storage = new T[nbins+2];
	}

    template<class T>
	void CbcHistList<T>::AddHist( T *hist, const HIST_TYPE &htype, BE_ID be_id, CBC_ID cbc_id, CHANNEL_ID ch_id )
	{

	    if(be_id==0){
		std::map<HIST_TYPE, unsigned>::iterator it_ghidmap = _GlobalHistIdMap.find(htype);
		if( it_ghidmap != _GlobalHistIdMap.end() ){
		    std::cerr << "The histogram already exists." << std::endl;
		}
		else{
		    _HistMap[_NHist]        = hist; 
		    _GlobalHistIdMap[htype] = _NHist++;
		}
	    }
	    else{
		if(cbc_id==0){
		    std::map<BE_ID, unsigned> &bhidmap = _BeHistIdMap[htype];
		    std::map<BE_ID, unsigned>::iterator it_bhidmap = bhidmap.find(be_id);
		    if( it_bhidmap != bhidmap.end() ){
			std::cerr << "The histogram already exists." << std::endl;
		    }
		    else{
			_HistMap[_NHist]   = hist; 
			bhidmap[be_id] = _NHist++;
		    }
		}
		else{
		    if(ch_id == 0){
			std::map<BE_ID, std::map<CBC_ID, unsigned> > &bhidmap = _CbcHistIdMap[htype];
			std::map<CBC_ID,unsigned> &chidmap = bhidmap[be_id];
			std::map<CBC_ID,unsigned>::iterator it_chidmap = chidmap.find(cbc_id);
			if(it_chidmap != chidmap.end()){
			    std::cerr << "The histogram already exists." << std::endl;
			}
			else{
			    _HistMap[_NHist]   = hist; 
			    chidmap[cbc_id] = _NHist++;
			}
		    }
		    else{
			std::map<BE_ID, std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > > &bhidmap = _ChannelHistIdMap[htype];
			std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > &chidmap = bhidmap[be_id];
			std::map<CHANNEL_ID, unsigned> &ch_hidmap = chidmap[cbc_id];
			std::map<CHANNEL_ID, unsigned>::iterator it_ch_hidmap = ch_hidmap.find(ch_id); 
			if(it_ch_hidmap != ch_hidmap.end()){
			    std::cerr << "The histogram already exists." << std::endl;
			}
			else{
			    _HistMap[_NHist]   = hist; 
			    ch_hidmap[ch_id] = _NHist++;
			}
		    }
		}
	    }
	}

    template<class T>
	T * CbcHistList<T>::GetHist( const HIST_TYPE &htype, BE_ID be_id, CBC_ID cbc_id, CHANNEL_ID ch_id )
	{
	    if(be_id==0){
		std::map<HIST_TYPE, unsigned>::iterator it_ghidmap = _GlobalHistIdMap.find(htype);
		if( it_ghidmap == _GlobalHistIdMap.end() ) return 0;
		else return _HistMap[it_ghidmap->second]; 
	    }
	    else{
		if(cbc_id==0){
		    std::map<BE_ID, unsigned> &bhidmap = _BeHistIdMap[htype];
		    std::map<BE_ID, unsigned>::iterator it_bhidmap = bhidmap.find(be_id);
		    if( it_bhidmap == bhidmap.end() ){
		       	return 0;
		    }
		    else{
		       	return _HistMap[it_bhidmap->second];
		    }
		}
		else{
		    if(ch_id == 0){
			std::map<BE_ID, std::map<CBC_ID, unsigned> > &bhidmap = _CbcHistIdMap[htype];
			std::map<CBC_ID,unsigned> &chidmap = bhidmap[be_id];
			std::map<CBC_ID,unsigned>::iterator it_chidmap = chidmap.find(cbc_id);
			if(it_chidmap == chidmap.end()) return 0;
			else return _HistMap[it_chidmap->second];
		    }
		    else{
			std::map<BE_ID, std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > > &bhidmap = _ChannelHistIdMap[htype];
			std::map<CBC_ID, std::map<CHANNEL_ID, unsigned> > &chidmap = bhidmap[be_id];
			std::map<CHANNEL_ID, unsigned> &ch_hidmap = chidmap[cbc_id];
			std::map<CHANNEL_ID, unsigned>::iterator it_ch_hidmap = ch_hidmap.find(ch_id); 
			if(it_ch_hidmap == ch_hidmap.end()) return 0;
			else return _HistMap[it_ch_hidmap->second];
		    }
		}
	    }
	}

    //replace s2 to s3 in s1
    std::string rstr( std::string s1, std::string s2, std::string s3 );
};

    


#endif

