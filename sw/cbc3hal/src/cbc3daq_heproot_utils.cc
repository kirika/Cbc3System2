#include "cbc3daq_heproot_utils.h"
#include "TString.h"
#include "TF1.h"
#include "TList.h"
#include <TFitResult.h>
#include "TFitResultPtr.h"
#include "TMath.h"
#include <iostream>

float msize(0.6);
float lwidth(1.5);
double max_width(100);

using namespace std;

Double_t MyErf( Double_t *x, Double_t *par ){
    Double_t x0 = par[0];
    Double_t width = par[1];
    Double_t fitval(0);
    if( x[0] < x0 ) fitval = 0.5 * TMath::Erfc( ( x0 - x[0] )/(sqrt(2.) * width ) );
    else fitval = 0.5 + 0.5 * TMath::Erf( ( x[0] - x0 )/( sqrt(2.) * width ) );
    return fitval;
}

double analyze_scurve( TH1F *h, double &mean, double &mean_e, double &sigma, double &sigma_e, double &gmin, double &gmax, double noise_lim ){

    h->SetMarkerStyle(20);
    h->SetMarkerSize(msize);
    h->SetLineWidth(lwidth);

    double rchi2(-1);

    double min(0), max(1023);
    for( int i=1; i <= h->GetNbinsX(); i++ ){
	float p = h->GetBinContent(i);
	if( min == 0 && p != 0 ) min = h->GetBinCenter(i);
	if( max == 1023 && p == 1 ) {
	    max = h->GetBinCenter(i);
	    break;
	}
	if( min != 0 && max == 1023 && h->GetBinCenter(i) - min > 30 ) max = h->GetBinCenter(i);
    }
    if(max-min > max_width*2) min = max - max_width*2;

    TString fname = h->GetName();
    fname.Replace( 0, 1, "f" );
    //If there is no degree of freedom, skip fit.
    if( min == max ){
	TF1 *f = new TF1( fname, MyErf, max-2, max+1, 2 ); 
	f->SetParameter(0, max-0.5);
	f->SetParameter(1, 0.5);
	TList *list = h->GetListOfFunctions();
	list->Clear();
	list->Add(f);
	cerr << "analyze_scurve() Histo name = " << h->GetName() << " only 0 or 1." << endl;
	if(gmin > min) gmin = min;
	if(gmax < max) gmax = max;
	return -3;
    }
    //If only one degree of freedom, fit anyway.
    else if( min + 1 == max ){
	if(gmin > min) gmin = min;
	if(gmax < max) gmax = max;
	min -= 1;
    }
//    cout << "Fit region = " << min << " " << max << endl;

    TF1 *f = new TF1( fname, MyErf, min, max, 2 ); 
    f->SetLineStyle(2);

    double x(min);
    double p0_0 = x;
    double diffr_p0_0 = fabs(h->GetBinContent(h->FindBin(p0_0))-0.5);
    while( x < max ){ 
	double diffr = fabs(h->GetBinContent(h->FindBin(x))-0.5);
	if( diffr < diffr_p0_0){
	    p0_0 = x; 
	    diffr_p0_0 = diffr;	
	}
	x = x + 1;
    }
    f->SetParameters( p0_0, (max-min)/4. );
    TFitResultPtr result = h->Fit( fname, "RSQ", "e0p0" ); 

    int status = int( result ); 
    if( status != 0 ) {
	result = h->Fit( fname, "MRSQ", "e0p0" ); 
	status = int( result ); 
	if( status != 0 ){
	    cerr << "analyze_scurve() Hist name = " << h->GetName() << " Fit failed." << endl;
	    result->Print();
	    rchi2 = -1;
	}
    }

    if( result->Ndf() == 0 ){
	cerr << "analyze_scurve() Hist name = " << h->GetName() << " only 1 bin not 0 nor 1." << endl;
	if(gmin > min) gmin = min;
	if(gmax < max) gmax = max;
	rchi2 = -2;
    }
    else{
	rchi2 = result->Chi2()/result->Ndf();
    }

    if(rchi2>0){
	mean = f->GetParameter( 0 );
	sigma = f->GetParameter( 1 );
	mean_e = f->GetParError( 0 );
	sigma_e = f->GetParError( 1 );
    }

    if(gmin > min) gmin = min;
    if(gmax < max) gmax = max;

    if(sigma > noise_lim){ 
	cerr << "analyze_scurve() [Large noise]: " << h->GetName() << " with sigma = " << sigma << endl;  
    }

    return rchi2;
}
