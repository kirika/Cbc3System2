#ifndef __CBC3SYSTEM2ROOT_H__
#define __CBC3SYSTEM2ROOT_H__

#include <TDirectory.h>
#include <TString.h>
#include <TGraph.h>

int plotScurves(TDirectory *dir, int color, TString type="", TString cbctype="", TString opt="pe0", double min=-1, double max=-1, double ytitle_ofst=-1);

int getGraphChannelOffsets(const TString &fname, TGraph *g);
int getGraphChannel(const TString &fname, int chbase, int col, TGraph *g);

//int plotChannelOffsets(const TString &fname, const TString &title="", const TString &name="chofst");

#endif

