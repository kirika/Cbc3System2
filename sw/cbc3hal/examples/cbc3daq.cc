/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
//#include "fc7/MmcPipeInterface.hpp"
#include <TH1F.h>
#include <TPad.h>
#include <TStyle.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

enum RUNCMD { RUNCMD_SUSPEND = 1, RUNCMD_RESUME, RUNCMD_STOP, RUNCMD_INVALID };

string connection_file( "connections.xml" );
string id("board");
vector<string> ids;
HwInterface *hw(0);
Cbc3BeFc7 *cbc3be(0);
int error_flag(0);

// replace s2 to s3 in s1
/*
std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}
*/

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                       "        << ": prints this help.\n" 
	<< setw(indent) << " --loadSD                     "        << ": load user image on SD card.\n" 
	<< setw(indent) << " --conFile  [connection file] "        << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]              "        << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]           "        << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --db_en                      "        << ": database is enabled.\n" 
	<< setw(indent) << " --kinoko-canvas [host:port]  "       << ": drawing to kinoko-canvas is enabled.\n" 
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << " ([command])                  "        << "; to execute commands and exit.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;
}
void printCommands(){

    unsigned indent(75);
    cout << left << setfill( ' ');
    cout << "Command list" << endl
	<< setw(indent) << "ipbus list board (delim)"                                         << ": to list boards in the connections."                       << endl
	<< setw(indent) << "ipbus list nodes [reg.exp]"                                       << ": to list nodes in the connections."                        << endl
	<< setw(indent) << "ipbus board url"                                                  << ": to show the url of the board."                            << endl
	<< setw(indent) << "ipbus read node=[reg. exp] file=[file name]"                      << ": to read ipbus registers."                                 << endl
	<< setw(indent) << "ipbus readBlock node=[node] n=[# of words] wpl=[# of words per line" << ": to read ipbus register in block."                      << endl
	<< setw(indent) << "ipbus write node=[reg. exp] value=[value]"                        << ": to write ipbus the register."                             << endl
	<< setw(indent) << "ipbus write file=[file name]"                                     << ": to write ipbus the register."                             << endl
	<< setw(indent) << "ipbus cbc_fast_signal [reset/test_pulse_req/trigger/orbit_reset]" << ": to send fast signal to cbc through ipbus"                 << endl
	<< setw(indent) << "fc7 read [board_id | rev_id | version]"                           << ": to read FC7 system firmware board_id | rev_id | version." << endl
	<< setw(indent) << "cbc3be read [id | version]"                                       << ": to read CBC3 system firmware id | version."               << endl
	<< setw(indent) << "cbc3be configure [be config file name] [fci_delay]"               << ": Cbc3BeInterface::ConfigureBeBoard is called. fci_delay = 0 by default." << endl
	<< setw(indent) << "cbc3be dctt tune/scan [fe_id]"                                    << ": to tune data clock timing."                               << endl
	<< setw(indent) << "cbc3be dump level=[0/1] type=[all,cnfg,stat]"                     << ": to dump system configuration. level=0 for debug, 1 for noaml output." << endl
	<< setw(indent) << "cbc3be test_out ch=[1/2] value=[value]"                           << ": to set test_out channel to a specific signal defined in the firmware." << endl
	<< setw(indent) << "cbc reset"                                                        << ": to reset CBC."                                            << endl
	<< setw(indent) << "cbci2c bus_init"                                                  << ": to initialize the cbc i2c bus manager."                   << endl
	<< setw(indent) << "cbci2c list types"                                                << ": to list types."                                           << endl
	<< setw(indent) << "cbci2c list names (no_ofst)"                                      << ": to list names. if no_ofst is specified, ofsets are not listed."                   << endl
	<< setw(indent) << "cbci2c reg dumpAll cbc_id=[1 to ncbcs, default is 1]"                                  << ": to dump cbc i2c registers on the CBC."                     << endl
	<< setw(indent) << "cbci2c reg r=[1/0] w=[1/0] page=[1/2] addr=[addr] value=[value] cbc_id=[1 to ncbcs, default is 1]"                   << ": to read or write cbc i2c register."                       << endl
	<< setw(indent) << "cbci2c reg r=[1/0] w=[1/0] file=[config file] cbc_id=[1 to ncbcs, default is 1]"                                     << ": to read or write cbc i2c registers listed in the file."   << endl
	<< setw(indent) << "cbci2c reg r=[1/0] w=[1/0] type=[type] values=[value list] cbc_id=[1 to ncbcs, default is 1]"                                     << ": to read/write cbc i2c registers by type (all to read all types). [value list] format is {item1:value1,item2:value2,...} " << endl
	<< setw(indent) << "cbci2c reg r=[1/0] w=[1/0] name=[name] value=[value] cbc_id=[1 to ncbcs]"                                     << ": to read/write cbc i2c registers by name (all to read all types). [value list] format is {item1:value1,item2:value2,...} " << endl
	<< setw(indent) << "cbci2c offsets r=[1/0] w=[1/0] values=[values] cbc_id=[1 to ncbcs]"                                    << ": to set and read offsets"                                  << endl 
	<< setw(indent) << "cbci2c addrs type=[type]"                                          
	<< ": to get register addresses for the type [FcCntlrCompBetaTrigLat/VPLUS/VCTH/TestPulseDelayGroup/TestPulsePolEnAMux/FciAndError]" << endl
	<< setw(indent) << "cbci2c item_names type=[type]"                                          
	<< ": to get item list for the type [FcCntrlCompBetaTrigLat/VPLUS/VCTH/TestPulseDelayGroup/TestPulsePolEnAMux/FciAndError]" << endl
	<< setw(indent) << "cbci2c values type=[type] data={addr1:value1,addr2:value2,...}"                                          
	<< ": to get values for the type [FcCntrlCompBetaTrigLat/VPLUS/VCTH/TestPulseDelayGroup/TestPulsePolEnAMux]. Give register values in vals list in address increasing order." << endl
	<< setw(indent) << "take_data"                                                        << ": to take data in the buffer." << endl
	<< setw(indent) << "run config [config file]"                                         << ": to configure run specific settings on the backend board." << endl
	<< setw(indent) << "run start fsg_en=[0/1] seq_en=[0/1] min_nword=[min. # of word to read] dump=[1/0] noint=[0/1]  rt=[run time in sec] file=[data file name]" << ": start a run."                                             << endl
	<< setw(indent) << "seq write file=[file name]"                                  << ": to write sequence to the block ram." << endl
	<< setw(indent) << "seq start"                                                    << ": to start fsc controller." << endl
	<< setw(indent) << "fsg start"                                                        << ": to start fast signal generator."                          << endl
	<< setw(indent) << "fsg stop"                                                         << ": to stop fast signal generator."                           << endl
	<< setw(indent) << "trigger start"                                                    << ": to start trigger."                                        << endl
	<< setw(indent) << "trigger stop"                                                     << ": to stop trigger."                                         << endl
	<< setw(indent) << "rdb_ctrl reset"                                                   << ": to reset raw data buffer controller."                     << endl
	<< setw(indent) << "rdb_ctrl showstat"                                                << ": to show the raw data buffer controller status."           << endl
	<< setw(indent) << "dump [data file name] level=[0/1/2] n=[# of events to dump]"                    << ": to dump data."                                            << endl
	<< setw(indent) << "configureCbcs    [CBC config file list file.]"                    << ": Cbc3BeInterface::ConfigureCbcs is called."                << endl
	<< setw(indent) << "q"                                                                << ": to exit this session."                                    << endl;
}

void process_ipbus_command( istringstream & iss )
{
    string item("");
    iss >> item;
    if ( item == "list" ){
	iss >> item;
	if( item == "board" ){
	    iss >> item;
	    string delim("\n");
	    if( item != "board" ) delim = item;
	    if( delim == "\\t" ) delim = "\t"; 
	    for( unsigned i=0; i < ids.size(); i++ ){
		cout << ids.at(i) << delim; 
	    }
	}
	if( item == "nodes" ){
	    string expression;
	    iss >> expression;
	    cout << "Expression : " << expression << endl;
	    vector<string> nodes = get_nodes( hw, expression );
	    cout << "Total " << nodes.size() << " nodes found." << endl;
	    for(unsigned i=0; i < nodes.size(); i++){
		cout << nodes.at(i) << endl;
	    }
	}
    }
    else if( item == "board" ){
	iss >> item;
	if( item == "url" ){
	    cout << hw->uri() << endl;	
	}
    } 
    else if( !item.compare("read")){
	iss >> item;
	//cout << "read item = [" << item << "]" << endl;
	if( item.find("node") != string::npos ) {
	    string expression = item.substr(item.find('=')+1);
	    vector<string> nodes = get_nodes( hw, expression );
	    vector<uint32_t> values;
	    read_ipbus( hw, nodes, values );
	    print_registers( cout, nodes, values );
	}
	else if( item.find("file") != string::npos ){
	    string fname = item.substr(item.find('=')+1 );
	    map<string,uint32_t> regmap;
	    read_ipbus( hw, fname, regmap );
	    print_registers( cout, regmap );
	}
    }
    else if( !item.compare("readBlock")){
	iss >> item;
	if( item.find("node") != string::npos ) {
	    string node =  item.substr(item.find('=')+1);
	    iss >> item;
	    if( item.find( "n" ) != string::npos ){
		uint32_t n = strtol( item.substr(item.find('=')+1 ).c_str(), 0, 0 ); 
		unsigned wpl(4);
		while( !iss.eof() ){
		    iss >> item;
		    if( item.find("wpl") != string::npos ) wpl = atoi( item.substr(item.find('=')+1 ).c_str() );
		}
		uint32_t *buffer(0);
		buffer = new uint32_t[n];
		if( node.find( "cbc_i2c_regs.reply_fifos.fe" ) != string::npos ){
		    readBlock_ipbus( hw, node, n, buffer ); 
		    for(unsigned i=0; i < n; i++){
			dump_reply( buffer[i], cout );
		    }
		}
		else if( node.find("data") != string::npos ){
		    string n_node( "cs_stat.db.nword_all" );
		    uint32_t nd = read_ipbus( hw, n_node );
		    while( nd < n ){
			nd = read_ipbus( hw, node );
			usleep(1);
		    }
		    readBlock_ipbus( hw, node, n, buffer ); 
		    //								cout << "0x";
		    cout << setbase(16);
		    cout << setfill('0');
		    cout << setw(8);
//		    unsigned count(0); 

		    cout << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
		    for(unsigned i=0; i < n; i++){
			cout << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
//			if( i == 0 ) count = buffer[i];
			if( i % wpl == wpl - 1 ){
			    cout << endl;
			    if( i != n-1) 
				cout << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
			}
		    }
		    cout << setw(1) << endl; 
		}
		else{
		    readBlock_ipbus( hw, node, n, buffer ); 
		    //								cout << "0x";
		    cout << setbase(16);
		    cout << setfill('0');
		    cout << setw(8);
//		    unsigned count(0); 
		    cout << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
		    for(unsigned i=0; i < n; i++){
			cout << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
//			if( i == 0 ) count = buffer[i];
			if( i % wpl == wpl - 1 ){
			    cout << endl;
			    if( i != n-1) 
				cout << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
			}
		    }
		    cout << setw(1) << endl; 
		}
		delete buffer;
	    }
	}
    }
    else if( !item.compare("write") ){
	iss >> item;
	if( item.find("node") != string::npos ) {
	    string node =  item.substr(item.find('=')+1);
	    iss >> item;
	    if( item.find( "value" ) != string::npos ){
		uint32_t value = strtol( item.substr(item.find('=')+1 ).c_str(), 0, 0 ); 
		write_ipbus( hw, node, value ); 
		/*
		   try{
		   value = read_ipbus( hw, node );
		   }
		   catch(...){
		   continue;
		   }
		   print_register( cout, node, value );
		   */
	    }
	}
	else if( item.find("file") != string::npos ){
	    string fname = item.substr(item.find('=')+1 );
	    cout << "backend board configuration file : " << fname << " is written to the board." << endl;
	    map<string,uint32_t> regmap = write_ipbus( hw, fname );
	    cout << "=== written register values ===" << endl;
	    print_registers( cout, regmap );
	    /*
	       read_ipbus( hw, fname, regmap );
	       cout << "=== read register values    ===" << endl;
	       print_registers( cout, regmap );
	       */
	}	
    }
    else if( item == "cbc_fast_signal" ){
	iss >> item;
	string node;
	if( item == "reset" ) node = "cs_ctrl.fcm.fc_reset";
	else if( item == "test_pulse_req" ) node = "cs_ctrl.fcm.fc_test_pulse_req";
	else if( item == "trigger" ) node = "cs_ctrl.fcm.fc_trigger";
	else if( item == "orbit_reset" ) node = "cs_ctrl.fcm.fc_orbit_reset";
	else return; 
	write_ipbus( hw, node, 1 );
    }
}
void process_fc7_command( istringstream &iss ){

    string item("");
    iss >> item;
    if( item == "read" ){
	iss >> item;
	if( item == "board_id" ){
	    cout << fc7::read_board_id( hw ) << endl;
	}
	else if( item == "rev_id" ){
	    cout << fc7::read_rev_id( hw ) << endl;
	}
	else if( item == "version" ){
	    cout << fc7::read_version( hw ) << endl;
	}
    }
}
void process_cbc3be_command( istringstream &iss )
{
    string item("");
    iss >> item;
    if( item == "read" ){
	iss >> item;
	if( item == "id" ){
	    cout << cbc3be->ReadId() << endl;
	}
	else if( item == "version" ){
	    cout << cbc3be->ReadVersion() << endl;
	}
    }
    else if( item == "configure" ){

	string fname;
	iss >> fname;
	//cout << "file name = [" << fname << "]" << endl;
	string fci_delay;
	iss >> fci_delay; 
	if( fci_delay == "" ) fci_delay = "0";
	if( fname != "" ) cbc3be->ConfigureBeBoard(fname, atoi(fci_delay.c_str()) );
	else cerr << "Specify the configuration file." << endl;
    }
    else if( item == "dctt" ){
	string comm;
	iss >> comm;
	if(comm == "tune")
	    cbc3be->TuneDataClockTiming(true);
	else if(comm == "scan"){
	    int fe_id;
	    iss >> fe_id;
	    cbc3be->ScanDataClockTiming(fe_id);
	}
    }
    else if( item == "dump" ){
	int level(1);
	string type("all");
	while( !iss.eof() ){
	    iss >> item;
	    if( item.find("level") != string::npos ) level = atoi( item.substr(item.find('=')+1 ).c_str() );
	    if( item.find("type") != string::npos ) type = item.substr(item.find('=')+1 ); 

	}
	if( type == "all" ){ 
	    cbc3be->DumpBeBoardConfig( cout, level );
	    cbc3be->DumpBeBoardStatus( cout, level );
	}
	else if( type == "cnfg" ) cbc3be->DumpBeBoardConfig( cout, level );
	else if( type == "stat" ) cbc3be->DumpBeBoardStatus( cout, level );
    }
    else if( item == "test_out" ){
	string ch("1");
	int value(0);
	while( !iss.eof() ){
	    iss >> item;
	    if( item.find("ch") != string::npos ) ch = item.substr(item.find('=')+1 );
	    if( item.find("value") != string::npos ) value = strtol( item.substr(item.find('=')+1 ).c_str(), 0, 0 );
	}
	write_ipbus( hw, string("cs_cnfg.test_out.") + ch, value ); 
    }
}
void process_cbci2c_command( istringstream &iss, bool CBC3_0 = true )
{
    string item("");
    iss >> item;
    if( !item.compare("bus_init") ){
	bool good = cbc3be->CbcI2cBusInit();
	if( !good ){ cerr << "Error : CBC I2C BUS initialization failed." << endl; error_flag = -1; } 
    }
    else if( !item.compare("list") ){
	iss >> item;
	if( item == "names" ){
	    iss >> item;
	    bool ofst(true);
	    if( item == "no_ofst" ) ofst = false;
	    cbci2creg_formatter::printNames(cout, ofst);
	}
	else if( item == "types" ){
	    iss >> item;
//	    bool ofst(true);
//	    if( item == "no_ofst" ) ofst = false;
	    cbci2creg_formatter::printTypeNames(cout);
	}
    }
    else if( !item.compare("reg") ){

	bool dumpAll(false);
	unsigned r(0), w(0), page(0), addr(0), value(0), cbc_id(1);
	string filename("");
	string type(""), name("");
	string values("");
	while( !iss.eof() ){
	    iss >> item;
	    if( item.find( "dumpAll" ) != string::npos ) dumpAll = true;
	    if( item[0] == 'r' ) r = atoi( item.substr(item.find('=')+1 ).c_str() );
	    if( item[0] == 'w' ) w = atoi( item.substr(item.find('=')+1 ).c_str() );
	    if( item.find( "page" ) != string::npos ) page = atoi( item.substr(item.find('=')+1 ).c_str() ); 
	    if( item.find( "addr" ) != string::npos ) addr = strtol( item.substr(item.find('=')+1 ).c_str(), 0, 0 ); 
	    if( item.find( "value" ) != string::npos ) value = strtol( item.substr(item.find('=')+1 ).c_str(), 0, 0 ); 
	    if( item.find( "file" ) != string::npos ) filename = item.substr(item.find('=')+1);
	    if( item.find( "type" ) != string::npos ) type = item.substr(item.find('=')+1);
	    if( item.find( "name" ) != string::npos ) name = item.substr(item.find('=')+1);
	    if( item.find( "values" ) != string::npos ) values = item.substr(item.find('=')+1); 
	    if( item.find( "cbc_id" ) != string::npos ) cbc_id = atoi(item.substr(item.find('=')+1).c_str() );
	}
	if( r > 1 || w > 1 ) {
	    cerr << "Error : Invalid r,w values" << endl;
	    return;
	}

	if( dumpAll ){
	    const CBCI2C_REGS &regs = cbc3be->GetCbcI2cRegReplies();
	    cbc3be->ReadAllCbcConfig(cbc_id);
	    for(unsigned cbcid=1; cbcid <= cbc3be->GetNCbcs(); cbcid++){
		if( cbc3be->IsCbcActive(cbcid) == 0 ) continue;
		if( cbc_id != 0 && cbc_id != cbcid ) continue;
		print_regcnf( regs, cout, cbcid );
	    }
	}
	else if( filename != "" ){
	    cbc3be->ClearCbcI2cRegReplies();
	    bool good = cbc3be->rwCbcI2cRegs( r, w, filename, cbc_id );
	    const CBCI2C_REGS &regs = cbc3be->GetCbcI2cRegReplies();
	    if( good ){
		if( r ){
		    print_regcnf( regs, cout, cbc_id );
		}
	    }
	    else{
		cout << "CBC I2C communication failed." << endl;	
		for(unsigned i=0; i < regs.size(); i++ ){
		    if( get_info(regs.at(i)) != 0 ) dump_reply( regs.at(i), cout ); 
		}
	    }
	}
	else if( type != ""){
	    if( type == "all" ){
		cbc3be->ClearCbcI2cRegReplies();
		CBCI2C_TYPE_DATA_MAP cbci2c_type_data_map = cbc3be->readCbcAllI2cRegsByType(cbc_id);
		//print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
		CBCI2C_TYPE_DATA_MAP::iterator it = cbci2c_type_data_map.begin();
		for(; it != cbci2c_type_data_map.end(); it++){
		    CBCI2C_ITEM_DATA_MAP &id = it->second;
		    CBCI2C_ITEM_DATA_MAP::iterator item_it = id.begin();
		    string item;
		    for(; item_it != id.end(); item_it++){
			item = it->first + "." + item_it->first;
			cout << std::setfill(' ') << setw(50) << std::left << item << " "; 
			cout << setw(3) << std::right << std::dec << item_it->second << endl;
		    }
		}
	    }
	    else{
		CBCI2C_ITEM_DATA_MAP item_data;
		if(values != ""){
		    int values_begin = values.find('{')+1;
		    int values_end   = values.find('}')-1;
		    values = values.substr(values_begin, values_end-values_begin+1); 
		    values = rstr( values, ",", " " );
		    stringstream values_ss( values ); 
		    string item;
		    while( !values_ss.eof() ){
			values_ss >> item;
			string name  = item.substr(0, item.find(':'));
			int value = strtol( item.substr(item.find(':')+1).c_str(), 0, 0 );
			item_data[name] = value;
			cout << name << endl;
		    }
		}
		CBCI2C_TYPE_DATA type_data( type, item_data );
		std::map<CBC_ID, CBCI2C_TYPE_DATA> cbc_id_data;
		cbc_id_data[cbc_id] = type_data;
		cbc3be->ClearCbcI2cRegReplies();
		cbc3be->rwCbcI2cRegsByType( r, w, cbc_id_data );
		print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
		if(cbc_id == 0){
		    std::map<CBC_ID, CBCI2C_TYPE_DATA>::iterator it = cbc_id_data.begin();
		    for(; it != cbc_id_data.end(); it++){
			cout << "Readback CBCID=" << it->first << " "; 
			CBCI2C_TYPE_DATA &td = it->second;
			if( td.first != type )continue;
			CBCI2C_ITEM_DATA_MAP::iterator item_it = td.second.begin();
			for(; item_it != td.second.end(); item_it++){
			    cout << item_it->first << "=" << std::dec << item_it->second << " ";
			}
			cout << endl;
		    }

		}
		else{
		    cout << "Readback ";
		    CBCI2C_TYPE_DATA &td = cbc_id_data[cbc_id];
		    if( td.first == type ){
			CBCI2C_ITEM_DATA_MAP::iterator item_it = td.second.begin();
			for(; item_it != td.second.end(); item_it++){
			    cout << item_it->first << "=" << std::dec << item_it->second << " ";
			}
			cout << endl;
		    }
		}
	    }
	}
	else if( name != ""){
	    cbc3be->ClearCbcI2cRegReplies();
	    cbc3be->rwCbcI2cRegsByName( r, w, name, value, cbc_id );
	    print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
	    cout << "Readback " << name << "=" << std::dec << value << endl;
	}
	else{
	    if( r==1 || w==1 ){
		struct cbc_i2c_command com = { cbc_id, w, r, page, addr, value }; 
		cbc3be->ClearCbcI2cRegReplies();
		if( cbc3be->rwCbcI2cReg( com ) ){
		    if(cbc_id==0){
			for(unsigned cbcid=1; cbcid <= cbc3be->GetNCbcs(); cbcid++){
			    if( cbc3be->IsCbcActive(cbcid) == 0 ) continue;
			    print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbcid );
			}
		    }
		    else{
			print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
		    }
		}
	    }
	}
    }
    else if( item == "offsets" ){
	unsigned r(0), w(0), cbc_id(1);
	map<unsigned,unsigned> chofst_map;
	vector<unsigned>       ofst_vector;
	while( !iss.eof() ){
	    iss >> item;
	    if( item[0] == 'r' ) r = atoi( item.substr(item.find('=')+1 ).c_str() );
	    if( item[0] == 'w' ) w = atoi( item.substr(item.find('=')+1 ).c_str() );
	    if( item.find( "cbc_id" ) != string::npos ) cbc_id = atoi(item.substr(item.find('=')+1).c_str() );
	    if( item.find( "values" ) != string::npos ){
		//string offsets = item.substr(item.find('=')+1 );
		int ofst_begin = item.find('[')+1;
		int ofst_end   = item.find(']')-1;
		string str_offsets = item.substr(ofst_begin, ofst_end-ofst_begin+1);
		str_offsets = rstr( str_offsets, ",", " " );
		stringstream offsets_ss( str_offsets ); 
		string ofst_item;
		if( str_offsets.find('{') != string::npos ){
		    while( !offsets_ss.eof() ){
			offsets_ss >> ofst_item;
			if( ofst_item.find('{') != string::npos ){
			    ofst_begin = ofst_item.find('{')+1;
			    ofst_end   = ofst_item.find('}')-1;
			    string ofst = ofst_item.substr(ofst_begin, ofst_end-ofst_begin+1); 
			    int ch  = strtol( ofst.substr(0, ofst.find(':')).c_str(), 0, 16 );
			    int off = strtol( ofst.substr(ofst.find(':')+1).c_str(), 0, 16 );
			    if(0<ch&&ch<=254)
				chofst_map[ch] = off;
			    else if (ch==0){
				ofst_vector.resize(254,off);
			    }
			}
		    }
		}
		else{
		    while( !offsets_ss.eof() ){
			offsets_ss >> ofst_item;
			ofst_vector.push_back( strtol( ofst_item.c_str(), 0, 0 ) );  
		    }
		    if( ofst_vector.size() != 254 ){
			ofst_vector.resize(0);
		    }
		}
	    }
	}
	if(chofst_map.size()){
	    cbc3be->ClearCbcI2cRegReplies();
	    cbc3be->rwCbcI2c_Offsets( r, w, chofst_map, cbc_id );
	    print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
	    map<unsigned,unsigned>::iterator chofst_it = chofst_map.begin();
	    cout << "Readback [";
	    unsigned i(0);
	    for(; chofst_it != chofst_map.end(); chofst_it++){
		cout << "{" 
		    << setw(2) << setfill('0') << right << std::hex <<  chofst_it->first << ":"
		    << setw(2) << setfill('0') << right << std::hex <<  chofst_it->second << "}";
		if( i < chofst_map.size()-1 )
		    cout << ",";
		i++;
	    }
	    cout << "]" << endl;
	}
	else{
	    cbc3be->ClearCbcI2cRegReplies();
	    cbc3be->rwCbcI2c_Offsets( r, w, ofst_vector, cbc_id );
	    print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
	    cout << "Readback ["; 
	    for( unsigned i=0; i < ofst_vector.size(); i++ )
		cout << setw(2) << setfill('0') << right << std::hex <<  ofst_vector.at(i) << ",";
	    cout << "]" << endl;
	}
    }
    else if ( item == "addrs" )
    {
	iss >> item;
	string type;
	if( item.find( "type" ) != string::npos ) type = item.substr(item.find('=')+1);
	vector<unsigned> addrs = cbci2creg_formatter::getAddrs(type);
	cout << "addrs = [";
	for( unsigned i=0; i < addrs.size(); i++ ){
	    cout << "0x" << setw(2) << setfill('0') << right << std::hex <<  addrs.at(i);
	    if( i < addrs.size()-1)
		cout << ",";
	}
	cout << "]" << endl;
    }
    else if ( item == "item_names" )
    {
	iss >> item;
	string type;
	if( item.find( "type" ) != string::npos ) type = item.substr(item.find('=')+1);
	set<string> items = cbci2creg_formatter::getItemNames(type);
	cout << "ItemNames = [";
	set<string>::const_iterator it = items.begin();
	unsigned i(0);
	for( ; it != items.end(); it++ ){
	    cout << *it; 
	    if( i++ < items.size()-1)
		cout << ",";
	}
	cout << "]" << endl;
    }
    else if ( item == "values" )
    {
	iss >> item;
	string type;
	if( item.find( "type" ) != string::npos ){
	    type = item.substr(item.find('=')+1);
	    CBCI2C_TYPE_DATA_MAP map;
	    get_cbci2c_type_data(type, 0, &map);
	    iss >> item;
	    vector<unsigned>       val_vector;
	    if( item.find( "data" ) != string::npos ){
		string data = item.substr(item.find('=')+1); 
		if( data.find('{') == string::npos || data.find('}') == string::npos ) {
		    cerr << "wrong format of data." << endl;
		    return;
		}
		int data_begin = data.find('{')+1;
		int data_end   = data.find('}')-1;
		data = data.substr(data_begin, data_end-data_begin+1); 
		data = rstr( data, ",", " " );
		stringstream data_ss( data ); 

		CBCI2C_ADDR_DATA_MAP reg_values;
		string datum;
		while( !data_ss.eof() ){
		    data_ss >> datum; 
		    if( datum.find(':') == string::npos ){
			cerr << "wrong format of data." << endl;
			return;
		    }
		    unsigned addr  = strtol( datum.substr(0, datum.find(':')).c_str(), 0, 0);
		    unsigned value = strtol( datum.substr(datum.find(':')+1).c_str(), 0, 0 );
		    reg_values[addr] = value;
		    cout << "(addr:value)=" << addr << " " << value << endl;
		}
		CBCI2C_TYPE_DATA type_data(type, map[type]);

		cbci2creg_formatter::getValues( type_data, reg_values, CBC3_0 );

		CBCI2C_ITEM_DATA_MAP item_data = type_data.second;
		CBCI2C_ITEM_DATA_MAP::iterator it_item_data = item_data.begin();

		cout << "values = {";
		unsigned i(0);
		for(; it_item_data != item_data.end();it_item_data++){
		    cout << it_item_data->first << ":" << std::dec <<  it_item_data->second;
		    if( i < item_data.size()-1)
			cout << ",";
		    i++;
		}
		cout << "}" << endl;
	    }
	}
    }
}

int check_run_command( char *run_command ){

    fd_set rfds;
    struct timeval tv;
    int retval;

    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    tv.tv_sec = 1;
    tv.tv_usec = 1;

    retval = select( 1, &rfds, 0, 0, &tv );

    if( retval == -1 ){
	perror( "select()");
    }
    else if( retval ){

	fgets( &run_command[strlen(run_command)], sizeof( run_command ) - strlen(run_command), stdin ); 
	string command( run_command );
	if( command.find('\n') != string::npos ){
	    run_command[command.find('\n')] = 0;
	    command.erase( command.find('\n') );
	}
	if( !command.compare("e") ){
	    return RUNCMD_STOP;
	}
	else if( !command.compare("s") ){
	    run_command[0] = 0;
	    cout << "type resume." << endl;
	    cout << ">> " << flush;
	    return RUNCMD_SUSPEND;
	}
	else if( !command.compare("r") ){
	    run_command[0] = 0;
	    cout << "type one of the commands, e(end) / s(suspend) / r(resume)." << endl;
	    cout << ">> " << flush;
	    return RUNCMD_RESUME;
	}
	else{
	    run_command[0] = 0;
	    cout << "type one of the commands, e(end) / s(suspend) / r(resume)." << endl;
	    cout << ">> " << flush;
	    return RUNCMD_INVALID;
	}
    }
    return 0;
}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    bool db_en(false);
    bool kc_en(false);
    bool print_version(false);
    int debug(0);
    bool load_sd(false);
    bool CBC3_0(true);
    string arg_s;

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"    , no_argument, 0, 'a' },
	    { "conFile" , required_argument, 0, 'b' },
	    { "boardId" , required_argument, 0, 'c' },
	    { "version" , no_argument, 0, 'd' },
	    { "debug"   , required_argument, 0, 'e' },
	    { "db_en"   , no_argument, 0, 'f' },
	    { "kinoko-canvas"   , required_argument, 0, 'g' },
	    { "loadSD"  , no_argument, 0, 'h' },
	    { "CBC3_1"   , no_argument, 0, 'i' },
	    {0,0,0,0}
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:de:fg:hi", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		print_version = true;
		break;
	    case 'e':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'f':
		db_en = true;
		break;
	    case 'g':
		kc_en = true;
		arg_s = optarg;
		break;
	    case 'h':
		load_sd = true;
		break;
	    case 'i':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }
    /*******************************************/
    //Argument is processed.
    /*******************************************/
    string arg_command;
    if( optind < argc ){
	arg_command = argv[optind++];
    }	
    if(load_sd){
	cout << "load_sd is disabled." << endl;
/*
	try{
	    std::string lPassword ( "RuleBritannia" );
	    std::string lFilename ( "UserImage.bin" );
	    ConnectionManager cm( string( "file://" ) + connection_file ); 
	    hw = new HwInterface( cm.getDevice( id ) );
	    fc7::MmcPipeInterface lNode = hw->getNode< fc7::MmcPipeInterface > ( "sysreg.buf_test" );
	    vector<string> lFiles = lNode.ListFilesOnSD ();
	    std::cout << std::endl;
	    std::cout << "Resetting FPGA and loading " << lFilename << " on initialisation..." << std::endl;
	    lNode.RebootFPGA ( lFilename , lPassword );
	    std::cout << "Complete." << std::endl;
	}
	catch ( std::exception& aExc )
	{

	    uhal::log ( uhal::Error() , "Exception " , uhal::Quote ( aExc.what() ) , " caught at " , ThisLocation() );
	    return 1;
	}
*/
	return 0;
    }

    int portno;
    int sockfd;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    pid_t pid(0);
    if(kc_en){
	arg_s = rstr( arg_s, ":", " " );
	istringstream iss_arg(arg_s); 
	string hostname;
	iss_arg >> hostname;
	iss_arg >> portno;
	cout << "kinoko-canvas at " << hostname << " : " << portno << endl;

	char command[256];
	sprintf( command, "kinoko-canvas localhost %d", portno);
	pid = fork();
	if( pid == 0 ){
	    setsid();
	    system( command );
	}
	sleep(1);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	    cerr << "ERROR opening socket" << endl;
	server = gethostbyname(hostname.c_str());
	if (server == NULL) {
	    fprintf(stderr,"ERROR, no such host\n");
	    exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
		(char *)&serv_addr.sin_addr.s_addr,
		server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){ 
	    cerr << "ERROR connecting" << endl;
	    exit(0);
	}
    }
//    cout << "Child's PID = " << pid << endl;

    /*******************************************/
    //CBC3 backend board FC7 interface
    //prepare ipbus communication and check the system id
    /*******************************************/
    cbc3be = new Cbc3BeFc7( CBC3_0, debug );
    cbc3be->Initialize( connection_file, id );
    hw = cbc3be->GetHwInterface(); 

    ConnectionManager cm( string( "file://" ) + connection_file ); 
    ids = cm.getDevices();

    /*******************************************/
    //Print version
    /*******************************************/
    if(print_version){ 
	try{
	    //cbc3be->ReadId();
	    cbc3be->PrintVersion(cout);
	}
	catch( ... ){
	    cerr << "Backend board FC7 is not ready." << endl;
	    return -1;
	}
    }
    /*******************************************/
    //Write CBC I2C register values to database
    /*******************************************/
    if(db_en) cbc3be->WriteCbcI2cRegsToDB();

    /*******************************************/
    //for daq
    /*******************************************/
    uint32_t *data_buffer;
    data_buffer = new uint32_t[131072];
    char *data_buffer_char = (char *)data_buffer;

    /*******************************************/
    //for history of the interactive shell 
    /*******************************************/
    string history_fname( "./.cbc3daq_history" );
    string scommand = string("touch ") + history_fname;
    system( scommand.c_str() );
    read_history( history_fname.c_str() );
    char *line(0);

    char comline[256];
    string item;
    while( true ){
	string sline;
	if( arg_command.compare("") != 0 ){
	    sline = arg_command + ";q";
	    add_history(sline.c_str());
	}
	else{
	    if(line) free(line);
	    line = readline("> ");
	    if( line == 0 ){
		sline = "q";
	    }
	    else{
	       	add_history(line);
		sline = line;
	    }
	}
	string::iterator it = sline.begin();
	for(; it != sline.end(); it++) if(*it==';') *it = '\n'; 
	istringstream iss_sline(sline.c_str());
	while( !iss_sline.eof() ){
	    iss_sline.getline(comline, 256 );
	    istringstream iss(comline);
	    try{
		item = "";
		iss >> item;
		if( !item.compare("q") || !item.compare("Q") ){
		    write_history( history_fname.c_str() );
		    history_truncate_file( history_fname.c_str(), 1000 );
		    if(kc_en){
			system( "ps aux | grep kinoko" );
			kill(pid*-1, SIGTERM);
			bool wait_child(true);
			while(wait_child){
			    sleep(1);
			    int status;
			    cout << "PID = " << pid << endl;
			    pid_t rpid = waitpid(pid, &status, WNOHANG);
			    cout << "returned value = " << rpid << endl;
			    system( "ps aux | grep kinoko" );
			    if(rpid == pid) wait_child = false;
			}
			close(sockfd);
		    }
		    return error_flag;
		}	
		else if( !item.compare("h") || !item.compare("H") ){
		    printCommands();
		}
		else if( !item.compare("ipbus")){
		    process_ipbus_command( iss );
		}
		else if( item == "fc7" ){
		    process_fc7_command( iss );
		}
		else if( item == "cbc3be" ){
		    process_cbc3be_command( iss );
		}
		else if( !item.compare("cbc") ){
		    iss >> item;
		    if( item.find( "reset" ) != string::npos ){
			cbc3be->CbcReset();
			//					write_ipbus( hw, "cs_ctrl.global.cbc_hard_reset", 1 );
		    }
		}
		else if( !item.compare("cbci2c" ) ){
		    process_cbci2c_command(iss, CBC3_0);
		}
		else if( !item.compare("take_data") ){
//		    cbc3be->MinNumWordToWait(368);
		    cbc3be->MinNumWordToWait(0);
		    cbc3be->DataReadyFlagPollingInterval_usec(0);
		    cbc3be->InitializeRun();
		    cbc3be->StartFastSignalGenerator();
		    struct timeval T0, T, dT, t0, t, dt, tt0, tt, dtt, trd, trd0, dtrd;
		    vector<uint32_t> vdata;
		    int total_words_read(0);
		    int ndiff(0);
		    int last_N(0);
		    gettimeofday(&tt0,0);
		    gettimeofday(&t0,0);
		    gettimeofday(&T0,0);
		    TH1F *h = new TH1F("hnword", ";# of words to read.", 100, 0, 130000 );
		    TH1F *hnreq = new TH1F("hnreq", "; # of request to check the data size", 10, 0, 10 ); 
		    TH1F *hslowr = new TH1F("hslowr", "; count of ipbus request of slow reply", 1000, 0, 1000 ); 
		    TH1F *htread = new TH1F("htread", "; time took to read data [usec]", 100, 0, 100000 ); 
		    TH1F *htreq = new TH1F("htreq", "; time took to request data size [usec]", 100, 0, 10000 ); 
		    unsigned long last_nread(0);
		    ipbutl::n_read = 0;
		    string nword_node = "cs_stat.db.nword_events";
		    while( true ){
		//	if( cbc3be->WaitData( 1, false ) ){
		/*
			int nword = ipbutl::read_ipbus(hw, nword_node);
			if( nword > 0 ){
			*/
			if( cbc3be->HasData(0) ){
			    /*
			    hnreq->Fill(ipbutl::n_read - last_nread);
			    //cout << "# of ipbus read() call : " << std::dec << ipbutl::n_read << endl;
			    int nword = cbc3be->GetNumNextDataWords();
			    h->Fill(nword);
			    //cout << "Time took to read = " << std::dec << dt.tv_usec << " usec. # of words reported is " << nword << endl;
			    */
			    gettimeofday(&t,0);
			    timersub(&t,&t0,&dt);
			    htreq->Fill(dt.tv_sec * 1000000 + dt.tv_usec);
			    /*
			    if( dt.tv_usec > 15000 ){
				cout << "It took more than 10 ms from the first wait after the last read." << endl; 
				cout << "Time took to request the data size = " << std::dec << dt.tv_usec << " # of words reported is " << nword << endl;
				cout << "Time took to read the last data is " << dtrd.tv_sec << " sec & " << dtrd.tv_usec << " usec" << endl;
				hslowr->Fill(ipbutl::n_read);
			    }	
			    */

			    gettimeofday(&trd0,0);
			    vdata = cbc3be->ReadData();
			    gettimeofday(&trd,0);
			    timersub(&trd,&trd0,&dtrd);
			    htread->Fill( dtrd.tv_sec * 1000000 + dtrd.tv_usec );
			    /*
			    if( nword > 50000 )
			    cout << std::dec << nword << endl;
			    total_words_read += nword; 
			    ndiff += nword;
			    */
			    /*
			    if( ndiff / 7000000 > 0 ){ 
				cout << total_words_read << " words has been read." << endl;
				ndiff = 0;
			    }
			    */
			    gettimeofday(&t0,0);
			    last_nread = ipbutl::n_read;
			}
			/*
			gettimeofday(&T,0);
			timersub(&T,&T0,&dT);
			if( dT.tv_sec > 10 ){
			    int dN = total_words_read - last_N;
			    last_N = total_words_read;
			    cout << "Average data flow rate in the last 10 sec = " << std::dec << dN / dT.tv_sec << " events/sec" << endl; 
			    T0 = T;
			} 
			*/
			gettimeofday(&tt,0);
			timersub(&tt,&tt0,&dtt);
			if( dtt.tv_sec > 5 ){
			    cbc3be->StopTrigger();
			    break;
			}
		    }
		    h->Draw();
		    gPad->Print("nword.png");
		    hnreq->Draw();
		    gPad->Print("nreq.png");
		    hslowr->Draw();
		    gPad->Print("hslowr.png");
		    gPad->SetLogy();
		    htread->Draw();
		    gPad->Print("htread.png");
		    htreq->Draw();
		    gStyle->SetOptStat(1111111);
		    htreq->SetStats(1);
		    gPad->SetLogy();
		    gPad->Update();
		    gPad->Print("htreq.png");
		    delete h;
		    delete hnreq;
		    delete hslowr;
		    delete htread;
		    delete htreq;
		    cout << "Total words read = " << std::dec << total_words_read << endl;
		    cout << "event count      = " << std::dec << total_words_read / ( 3 + 11 * cbc3be->GetNActiveCbcs()) << endl;

			/*
		    if( cbc3be->WaitData( 1, false ) ){
			int nwords = cbc3be->ReadData( data_buffer );
			int left = nwords*4;
			int evt_count(0), evt_dump_count(0);
			int this_left = Cbc3BeInterface::DumpData( data_buffer, evt_count, evt_dump_count, 1, -1, 0, left );
			cout << "# of incomplete words = " << this_left << endl;
		    }
			*/
		}
		else if( !item.compare("run") ){
		    iss >> item;
		    if( !item.compare("config") ){
			iss >> item;
			map<string,uint32_t> regmap = write_ipbus( hw, item );
			cout << "=== written register values ===" << endl;
			print_registers( cout, regmap );
		    }
		    else if( !item.compare("start") ){

			int min_nword(1);
			int run_time(20);
			bool fsg_en(0), seq_en(0), dump(0), noint(0);
			string filename("");
			while( !iss.eof() ){
			    iss >> item;
			    if( item.find("min_nword") != string::npos ) min_nword = strtol( item.substr(item.find('=')+1).c_str(), 0, 0 ); 
			    if( item.find("fsg_en") != string::npos ) fsg_en = atoi( item.substr(item.find('=')+1).c_str() ); 
			    if( item.find("seq_en") != string::npos ) seq_en = atoi( item.substr(item.find('=')+1).c_str() ); 
			    if( item.find("dump") != string::npos ) dump = atoi( item.substr(item.find('=')+1).c_str() ); 
			    if( item.find("noint") != string::npos ) noint = atoi( item.substr(item.find('=')+1).c_str() ); 
			    if( item.find("rt") != string::npos ) run_time = strtol( item.substr(item.find('=')+1).c_str(), 0, 0 ); 
			    if( item.find("file") != string::npos ) filename = item.substr(item.find('=')+1); 
			}
			cout << "run time = " << run_time << endl;

			int output_fd(-1);
			if(filename!= "" ){
			    cout << "data file is " << filename << endl;
			    output_fd = open( filename.c_str(), O_WRONLY | O_CREAT, 0444);
			    if(output_fd == -1){
				perror("open");
				break;
			    }
			}
			int bufsize = 10 * 1024 * 1024;
			char *obuf = new char[bufsize];
			int offset(0);
			vector<uint32_t> vdata;
			int total_words_read(0);
			int total_nread(0);
			//		    ofstream cDataFile( filename.c_str(), ofstream::binary);
			cbc3be->MinNumWordToWait(min_nword);
			cbc3be->DataReadyFlagPollingInterval_usec(0);

			if(!noint){
			    cout << "type one of the commands, e(end) / s(suspend) / r(resume)" << endl;
			    cout << ">> " << flush;
			}
			total_words_read = 0;
			cbc3be->InitializeRun();
			if(fsg_en) cbc3be->StartFastSignalGenerator();
			if(seq_en) cbc3be->StartFscBramController(); 
			BeDataDecoder be_data_decoder;
			char run_command[256];
			run_command[0] = 0;
			struct timeval t0, t, dt;
			gettimeofday(&t0,0);
			while( true ){

			    gettimeofday(&t,0);
			    timersub(&t,&t0,&dt);
			    // check the interactive commands or time to end the run
			    if(!noint){
				int run_cmd = check_run_command( run_command );
				if( run_cmd == RUNCMD_STOP ){
				    close (output_fd);
				    cbc3be->StopTrigger();
				    //	cDataFile.close();
				    break;
				}
				else if( run_cmd == RUNCMD_SUSPEND ){
				    cbc3be->StopTrigger();
				}
				else if( run_cmd == RUNCMD_RESUME ){
				    cbc3be->StartTrigger();
				}
			    }
			    if( dt.tv_sec > run_time ){
				cbc3be->StopTrigger();
				if( filename != "" ){ 
				    if( offset ){
					write (output_fd, obuf, (ssize_t) offset);
				    }	    
				    close (output_fd);
				    //cDataFile.close();
				}
				break;
			    } 
			    //reading data.
			    if( cbc3be->WaitData( 1, false ) ){
				total_nread++;
				//read procedure when writing the data to a file
				if( filename != "" || dump ){ 
				    cout << "ReadData" << endl;
				    //cDataFile.write( (char *)data_buffer, nwords * 4 ); 
				    int nwords = cbc3be->ReadData( data_buffer );
				    cout << "ReadData" << endl;
				    if(kc_en){
					cout << "kinoko-canvas is enabled." << endl;
					be_data_decoder.SetData( data_buffer, nwords * 4 );
					while( be_data_decoder.SetNextEvent() ){ 
					    while( be_data_decoder.SetNextCbc() ){
						const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
						ostringstream oss;
						oss << ".create plot p0 0 0 100 100; p0 frame 0 254 0 2; p0 hist 0.5 1 "; 
						for(int i=1; i<255;i++){
						    oss << cbc_data_packet_decoder->ChannelData(i) << " "; 
						}
						oss << ";";
						cout << "oss : " << oss.str() << endl;
						char buf[1024];
						strcpy( buf, oss.str().c_str() );
						write(sockfd, buf, strlen(buf));
					    }
					}
				    }
				    total_words_read += nwords;
				    int evt_count(0), evt_dump_count(0);
				    cout << "Dumping data" << endl;
				    if(dump){
					    Cbc3BeInterface::DumpData( data_buffer, evt_count, evt_dump_count, 1, 1, 0, nwords * 4 );
					    uint32_t rawdata[500];
						uint32_t *p = &(rawdata[0]);
					    int read_nwords = cbc3be->ReadRawData( p, 500 );
					    raw_data_dump_data(rawdata, 500, cout);
				    }

				    if( filename != "" ){
					if( offset > bufsize - nwords * 4 ){  
					    int size = bufsize - offset;
					    memcpy( &obuf[offset], data_buffer, size ); 
					    int ret_out = write (output_fd, obuf, (ssize_t) bufsize);
					    if( ret_out != bufsize ) cerr << "write failed" << endl;
					    memcpy( obuf, &data_buffer_char[size], nwords * 4 - size ); 
					    offset = nwords * 4 - size; 
					}
					else{
					    int size = nwords * 4;
					    memcpy( &obuf[offset], data_buffer, size ); 
					    offset += size;
					}
				    }
				}
				//read without writing
				else{
				    vdata = cbc3be->ReadData();
				    total_words_read += vdata.size();
				    cout << "read" << endl;
				}
			    }
			}
			cout << "Total # of read  = " << std::dec << total_nread << endl;
			cout << "Total words read = " << std::dec << total_words_read << endl;
			cout << "event count      = " << std::dec << total_words_read / ( 3 + 11 * cbc3be->GetNActiveCbcs()) << endl;
		    }
		}	
		else if( item == "seq" ){
		    iss >> item;
		    if( item == "write" ){
			iss >> item;
			if( item.find("file") != string::npos ){
			    string fname = item.substr(item.find('=')+1 );
			    cbc3be->WriteFscBram(fname);
			}
		    }
		    else if( item == "start" ){
			cbc3be->StartFscBramController();
		    }
		}
		else if( item == "cisc" ){
		    iss >> item;
		    if( item == "write" ){
			iss >> item;
			if( item.find("file") != string::npos ){
			    string fname = item.substr(item.find('=')+1 );
			    cbc3be->WriteCiscBram(fname);
			}
		    }
		    else if( item == "reset" ){
			write_ipbus( hw, "cs_ctrl.cisc.reset", 1 );
			write_ipbus( hw, "cs_ctrl.cbc_i2c_bm.bus1.reset_fifos", 1);
		    }
		    else if( item == "start" ){
			write_ipbus( hw, "cs_ctrl.cisc.start", 1 );
		    }
		    else if( item == "pause" ){
			write_ipbus( hw, "cs_ctrl.cisc.pause", 1 );
			write_ipbus( hw, "cs_ctrl.cbc_i2c_bm.bus1.reset_fifos", 1);
		    }
		    else if( item == "resume" ){
			write_ipbus( hw, "cs_ctrl.cisc.resume", 1 );
		    }
		    else if( item == "stop" ){
			write_ipbus( hw, "cs_ctrl.cisc.stop", 1 );
			write_ipbus( hw, "cs_ctrl.cbc_i2c_bm.bus1.reset_fifos", 1);
		    }
		    else if( item == "reset_ebram_raddr" ){
			write_ipbus( hw, "cs_ctrl.cisc.reset_ebram_raddr", 1 );
		    }
		    if( item == "read" ){
			string n_node( "cs_stat.cisc.nword_ebram");
			uint32_t nd = read_ipbus( hw, n_node );
			uint32_t *buffer(0);
			unsigned wpl(3);
			cout << nd/3 << " SEUs are found." << endl;
			if(nd){
			    buffer = new uint32_t[nd];
			    readBlock_ipbus( hw, "ciscebram", nd, buffer ); 
			    cout << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
			    for(unsigned i=0; i < nd; i++){
				cout << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
				//			if( i == 0 ) count = buffer[i];
				if( i % wpl == wpl - 1 ){
				    cout << endl;
				    if( i != nd-1) 
					cout << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
				}
			    }
			    cout << setw(1) << endl; 
			    delete buffer;
			}
		    }
		}
		else if( item == "fsg" ){
		    iss >> item;
		    if( item == "start" ){
			cbc3be->StartFastSignalGenerator();
		    }
		    if( item == "stop" ){
			cbc3be->StopFastSignalGenerator();
		    }
		}
		else if( item == "trigger" ){
		    iss >> item;
		    if( item == "start" ){
			cbc3be->StartTrigger();
		    }
		    if( item == "stop" ){
			cbc3be->StopTrigger();
		    }
		}
		else if( item == "rdb_ctrl" ){
		    iss >> item;
		    if( item == "reset" ){
			write_ipbus( hw, "cs_ctrl.rdb.reset", 1 );
		    }
		    if( item == "showstat" ){
			vector<string> nodes = get_nodes( hw, "cs_stat.rdb.*" );
			vector<uint32_t> values;
			read_ipbus( hw, nodes, values ); 
			print_registers( cout, nodes, values );
		    }
		}
		else if( item == "dump" ){
		    iss >> item;
		    string fname = item;
		    int level(0), n(0);
		    while( !iss.eof() ){
			iss >> item;
			if( item.find( "level" ) != string::npos ) level = atoi( item.substr(item.find('=')+1 ).c_str() ); 
			if( item[0] == 'n' ) n = atoi( item.substr(item.find('=')+1 ).c_str() );
		    }
		    cbc3be->DumpData( fname, level, n, 0, 56000 );
		}
		else if( item == "configureCbcs" ){
		    string fname;
		    iss >> fname;
		    if( fname != "" ) cbc3be->ConfigureCbcs(fname);
		    else cerr << "Specify the configuration file." << endl;
		}
	    }
	    catch( std::exception &e){
		cerr << e.what() << endl;
		write_history( history_fname.c_str() );
		history_truncate_file( history_fname.c_str(), 1000 );
		return error_flag;
	    }
	}
    }
    write_history( history_fname.c_str() );
    history_truncate_file( history_fname.c_str(), 1000 );
    return error_flag;
}

