/*
 * Author : Kirika Uchida
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fstream>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <sys/time.h>
#include <iomanip>
#include <getopt.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;

struct timeval T0, t0, t, dt;

int data_buffer_size = 65536;
string connection_file( "connections.xml" );
string id("board");

bool run_end = false;
int ievt(0);
ofstream datfile;
int rawdata_fd(-1);
ofstream logfile;

string time_string(){

    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);

    string TimeFormat = "%d %b %Y %H:%M:%S %Z";

    char Buffer[256];
    strftime(Buffer, sizeof(Buffer), TimeFormat.c_str(), timeinfo);

    return Buffer;
}

bool error_flag(false);
const uint32_t *evt_buf(0);
ssize_t   evt_size(0);

void sig_handler(int signo)
{
    if(signo == SIGINT){
	if(logfile.is_open()){
	    logfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    logfile.close();
	}
	if( rawdata_fd != -1 ){
	    if(error_flag & (evt_size != 0) ) write(rawdata_fd, evt_buf, evt_size);
	    close (rawdata_fd);
	}
	if(datfile.is_open()){
	    datfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    datfile.close();
	}
	run_end = true;
    }
    if(signo == SIGSEGV){
	if(logfile.is_open()){
	    logfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    logfile.close();
	}
	if( rawdata_fd != -1 ){
	    if(error_flag & (evt_size != 0) ) write(rawdata_fd, evt_buf, evt_size);
	    close (rawdata_fd);
	}
	if(datfile.is_open()){
	    datfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    datfile.close();
	}
	run_end = true;
    }
}

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                      "            << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file]"            << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]             "            << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]          "            << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --run      [number]         "            << ": run number.\n" 
	<< setw(indent) << " --withHits                  "            << ": all channel hits are expected.\n" 
	<< setw(indent) << " --dataDir                   "            << ": data directory.\n" 
	<< setw(indent) << " --runTime                   "            << ": run time in sec.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    unsigned runtime(0);
    int debug(0);
    int run(0);
    bool withHits(false);
    string ofname("seu_test.dat");
    string rawfname("seu_test.raw");
    string logfname("seu_test.log");
    string dataDir("./");

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"    , no_argument, 0, 'a' },
	    { "conFile" , required_argument, 0, 'b' },
	    { "boardId" , required_argument, 0, 'c' },
	    { "debug"   , required_argument, 0, 'd' },
	    { "run"     , required_argument, 0, 'e' },
	    { "withHits", no_argument, 0, 'f' },
	    { "dataDir" , required_argument, 0, 'g' },
	    { "runTime" , required_argument, 0, 'h' },
	    {0,0,0,0}
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:fg:h:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'e':
		run = strtol(optarg, 0, 0); 
		char tmp[256];
		sprintf( tmp, "seu_test_run%010d.dat", run );
		ofname = tmp;
		sprintf( tmp, "seu_test_run%010d.raw", run );
		rawfname = tmp;
		sprintf( tmp, "seu_test_run%010d.log", run );
		logfname = tmp;
		break;
	    case 'f':
		withHits = true;
		break;
	    case 'g':
		dataDir = optarg;
		break;
	    case 'h':
		runtime = strtoul(optarg, 0, 0);
		break;
	    default:
		abort();
	}
    }

    rawfname = dataDir + "/" + rawfname;
    ofname   = dataDir + "/" + ofname;
    logfname = dataDir + "/" + logfname;

    rawdata_fd = open( rawfname.c_str(), O_WRONLY | O_CREAT, 0444);
    if(rawdata_fd == -1){
	perror("open");
	return -1;
    }
    datfile.open(ofname.c_str());

    logfile.open(logfname.c_str());

//    HwInterface     *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    //prepare ipbus communication and check the system id
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
//	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7( debug );
	cbc3be->Initialize( connection_file, id );
	//cbc3be->WriteCbcI2cRegsToDB();
	cbc3be->ReadId();
	cbc3be->PrintVersion(logfile);
    }
    catch( const std::exception& e ){
	logfile << e.what() << endl;
	logfile << "IPBUS is not ready" << endl;
	return -1;
    }

    if (signal(SIGINT, sig_handler) == SIG_ERR) {
	printf("\ncan't catch SIGINT\n");
    }
    if (signal(SIGSEGV, sig_handler) == SIG_ERR) {
	printf("\ncan't catch SIGSEGV\n");
    }

    int exp_nhits(0);
    int exp_nstubs(0);
    bool exp_sof(false);
    if(withHits){
	exp_nhits  = 254;
    }

    uint32_t *data_buffer;
    data_buffer = new uint32_t[data_buffer_size];
    BeDataDecoder be_data_decoder;

    cbc3be->InitializeRun();
    cbc3be->SendFastSignal("orbit_reset");

    int min_nword(1500);
    cbc3be->MinNumWordToWait(min_nword);
    cbc3be->DataReadyFlagPollingInterval_usec(0);
    cbc3be->StartFastSignalGenerator();

    int leftBufferSize(0);
    bool reset_daq_flag(false);

//    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    gettimeofday(&T0,0);
    /*
    te0.tv_sec = T0.tv_sec;
    te0.tv_usec = T0.tv_usec;
    */
//    unsigned be_l1c(0);
    int CNTR(0);
//    unsigned last_be_l1c(0);

    datfile << "RUNSTART AT " << time_string() << endl;
    logfile << "RUNSTART AT " << time_string() << endl;

    while (!run_end){


	gettimeofday(&t,0);
	timersub(&t,&T0,&dt);
	if(dt.tv_sec > (time_t) runtime ) raise(SIGINT); 

	if(run_end) break;

	if( cbc3be->WaitData( 1 ) ){

	    int next_data_nwords = cbc3be->GetNumNextDataWords();
	    if( next_data_nwords >= cbc3be->GetDataBufferWordSize() ){
		logfile << "E " << time_string() << " DATA BUFFER OVERFLOW. " << std::dec << next_data_nwords << " WORDS IS REPORTED TO BE READ.  RESET IS SENT. " 
		    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
		reset_daq_flag = true;
	    }	

	    int nwords = leftBufferSize/4; 
	    if( nwords ) logfile << "I " << time_string() << " " << std::dec << nwords << " WORDS LEFT IN THE LAST DAQ." << endl; 
	    uint32_t *Data = &data_buffer[nwords];
	    nwords += cbc3be->ReadData( Data, data_buffer_size-nwords );
	    //    logfile << ++nth_read << " th read.  # of read words = " << nwords << endl;
	    be_data_decoder.SetData( data_buffer, nwords * 4 );
	    if( data_buffer[0] != 0x004C402E ){
		logfile << "E " << time_string() << " EVENT HEADER " << data_buffer[0] << " AT THE BEGINNING OF THIS BLOCK OF DATA IS INVALID. THIS DATA BLOCK "
		    << std::dec << next_data_nwords << " WORDS ARE DISCARDED."
		    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
		write (rawdata_fd, data_buffer, nwords * 4 );
		cbc3be->StopTrigger();
		string node = "cs_ctrl.global.daq_reset";
		cbc3be->WriteBeBoardConfig(node,1);
		cbc3be->StartTrigger();
		continue;
	    }	
	    int this_evti(0);
	    while( be_data_decoder.SetNextEvent() ){ 

		string evt_time = time_string();

		this_evti++;

		/*
		be_l1c = be_data_decoder.BeL1ACounter();
		if( ievt != 0 ){
		    if( be_l1c - last_be_l1c != 1 ){
			logfile << "E " << evt_time << " BE L1A COUNTER JUMPED BY " << be_l1c - last_be_l1c << " FROM " << std::dec << last_be_l1c << " TO " << be_l1c << ". "
			    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
//			write (rawdata_fd, data_buffer, nwords * 4 );
		    }
		}
		*/

		evt_buf = be_data_decoder.GetEventPacketPointer();
		evt_size = (ssize_t) be_data_decoder.EventPacketSize() * 4;

		error_flag = false;
		vector<int> pa, cntr;

		while( be_data_decoder.SetNextCbc() ){

		    if( !be_data_decoder.GetCbcEventPacketPointer() ){ 
			logfile << "E " << evt_time << " CbcDataPacketDecoder is invalid. " << std::dec 
			    << std::dec << this_evti << " TH EVENT IN THIS DAQ. RESET IS SENT. "
			    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
			write (rawdata_fd, data_buffer, nwords * 4 );
			reset_daq_flag = true;
			break;
		    }

		    const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
		    

		    //cbc_data_packet_decoder->DumpData(logfile);
		    int cbc_id = cbc_data_packet_decoder->Id(); 
		    if(cbc_id == 4) { CNTR++; CNTR = CNTR % 512; }
		    if( cbc_data_packet_decoder->Error(0) ){
			logfile << "E " << evt_time << " Latency error found at CBC ID = " << cbc_id << endl;
			error_flag = true;
		    }	
		    if( cbc_data_packet_decoder->Error(1) ){
			logfile << "E " << evt_time << " Buffer overflow error found at CBC ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    int nhits = cbc_data_packet_decoder->HitChannels().size();
		    if( nhits != exp_nhits ){
			logfile << "E " << evt_time << " # of hits " << std::dec << nhits << " does not match expected # " << std::dec << exp_nhits << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    vector<unsigned> sps = cbc_data_packet_decoder->StubPositions();
		    vector<unsigned> sbs = cbc_data_packet_decoder->StubBends();
		    int nstubs(0);
		    for(unsigned i=0; i<sps.size(); i++){
			if(sps.at(i) != 0 ) nstubs++;
			if(sbs.at(i) != 0 ) {
			    logfile << "E " << evt_time << " Stub bend is not 0 at CB ID = " << cbc_id << endl;
			    error_flag = true;
			}
		    }
		    if(nstubs != exp_nstubs){
			logfile << "E " << evt_time << " # of stubs " << std::dec << nstubs << " does not match expected # " << std::dec << exp_nstubs << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    bool sof = cbc_data_packet_decoder->TrigSof();
		    if( sof != exp_sof ){
			logfile << "E " << evt_time << " Stub overflow is " << sof << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    bool or254 = cbc_data_packet_decoder->TrigOr254();
		    if( or254 ){
			logfile << "E " << evt_time << " Or254 is reported  at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }	
		    bool trig_err = cbc_data_packet_decoder->TrigErr();
		    if( trig_err ){
			logfile << "E " << evt_time << " Trigger Error is reported  at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    pa.push_back( cbc_data_packet_decoder->PipelineAddress() );
		    cntr.push_back( cbc_data_packet_decoder->L1ACounter() );
		}
		if( reset_daq_flag ) break;
		if( pa[2] != pa[0] || pa[2] != pa[1] || pa[2] != pa[3] ) { 
		    logfile << "E " << evt_time << " Pipeline addresses do not match." << endl;
		    error_flag = true; 
		}
		if( cntr[0] != CNTR || cntr[1] != CNTR || cntr[2] != CNTR || cntr[3] != CNTR ){ 
		    logfile << "E " << evt_time << " L1A counters do not match." << endl;
		    error_flag = true; 
		}
		ievt++;
		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);
		//if( ievt % 100000 == 0 ) logfile << "I " << evt_time << " " << std::dec << ievt << " events has taken." << endl; 
		if( dt.tv_sec > 60 ){
		    logfile << "I " << evt_time << " " << std::dec << ievt << " events has taken." << endl; 
		    t0 = t;
		}
		if(ievt == 1 || ievt == 2){
		    if(ievt == 1)datfile << "==== First event ====" << endl;
		    if(ievt == 2)datfile << "==== Second event ====" << endl;
		    be_data_decoder.ResetEvent();
		    be_data_decoder.DumpEventBeData(datfile);
		    while( be_data_decoder.SetNextCbc() ){
			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			cbc_data_packet_decoder->DumpData(datfile);
		    }
		    if(ievt == 1)datfile << "==== First event ====" << endl;
		    if(ievt == 2)datfile << "==== Second event ====" << endl;
		}
		if( error_flag ){
		    be_data_decoder.ResetEvent();
		    datfile << evt_time << endl; 
		    be_data_decoder.DumpEventBeData(datfile);
		    while( be_data_decoder.SetNextCbc() ){
			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			cbc_data_packet_decoder->DumpData(datfile);
		    }
		    write (rawdata_fd, evt_buf, evt_size);
		}
		if(reset_daq_flag) break;
		//last_be_l1c = be_l1c;
	    }
	    if( reset_daq_flag ){
		cbc3be->StopTrigger();
		usleep(32);
		string node = "cs_ctrl.global.daq_reset";
		cbc3be->WriteBeBoardConfig(node,1);
		cbc3be->StartTrigger();
		reset_daq_flag = false;
	    }
	}
    }
    return 0;
}
