/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <exception>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "Cbc3BeData.h"
#include "seq_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include "tmc.h"
#include "wt.h"
#include "cbc3daq_heproot_utils.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;
using namespace wt;

//---------------------------
// default ipbus settings
string connection_file( "connections.xml" );
string id("board");
string cbc3hal_dir("./");
//---------------------------
// keithley settings and variables
extern "C" {
#include "ut.h"
}
char kmm_sern[256] = "8004246";
#define KMM_BUFLEN 20000 
unsigned char kmm_response_buf[KMM_BUFLEN];
int kmm_reason;
usbtmc_device_data *ut_handle;
//---------------------------
// default file names 
string pedestal_file = "vcthscan-pedestal.root"; 
string offset_file ="offset_tuning-be1cbc1.knt";

//---------------------------
//Emergency ipbus interface outside Cbc3BeInterface.
HwInterface *hw(0);
unsigned cbc_id = 1;

int kmm_read( char *com, usbtmc_device_data *h, unsigned char *buf, int len, int debug = 1){

    struct timeval t0, t, dt, last_sumdt;
    struct timeval sumdt = {0,0};
    usbtmc_printf(h, com);  
    gettimeofday(&t,0);
    timersub(&t,&t0,&dt);
    last_sumdt = sumdt; 
    timeradd(&dt, &last_sumdt, &sumdt);

    //read response
    int reason;
    int nread = usbtmc_read(h, buf, len, &reason);
    if(debug) printf("response size = %d\n", nread);
    buf[nread] = 0;
    return nread;
}
int kmm_read_ndata( usbtmc_device_data *h, unsigned char *buf, int len, int ndata, int debug = 1){

    stringstream oss;
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int points = 0;
    while(1){
	kmm_read(":DATA:POINts?", h, buf, len, debug );
	if(debug) printf("%s\n", buf);
	points = (int)( strtof( (char *)buf, 0 ));

	if(points == ndata) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > (ndata / 100 + 1) ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    return kmm_read(":FETCH?", h, buf, KMM_BUFLEN, debug);
}
std::vector<float> get_kmm_vdata( unsigned char *buf ){
    std::vector<float> meas;
    char *p;
    p = strtok((char *)buf, ",");
    while (p != 0){
	float fv = strtof(p,0);
	meas.push_back(fv);
	p = strtok(0, ",");
    }
    return meas;
}
float kmm_read_VDDA( Cbc3BeFc7 *cbc3be, usbtmc_device_data *ut_handle ){

    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
    //    usbtmc_printf(ut_handle1,":TRIGger:SOURce EXTernal");

    char tmp[256];
    int TRG_COUNT = 10;
    sprintf(tmp, ":TRIGger:COUNt %d", TRG_COUNT);
    usbtmc_printf(ut_handle, tmp);


    usbtmc_printf(ut_handle,":SENSE:FUNC \"VOLT:DC\"");     //dc voltage measurements
    usbtmc_printf(ut_handle,":SENSE:VOLT:NPLC 1");          //time to average the input for each measurement, in power line cycles
    usbtmc_printf(ut_handle,":SENSE:VOLT:RANG 10");         //input voltage range
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO OFF");        //autozero on request only
    usbtmc_printf(ut_handle,":TRIGger:SOURce IMMediate");
    usbtmc_printf(ut_handle,":SAMPle:COUNt 1");
    usbtmc_printf(ut_handle,":TRIGger:COUNt 1");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usbtmc_printf(ut_handle,":FETCH?");  
    //read response
    for(int i=0; i < KMM_BUFLEN; i++) kmm_response_buf[i] = 0; 
    int a=usbtmc_read(ut_handle, kmm_response_buf, KMM_BUFLEN,&kmm_reason);
    return strtof((char *)kmm_response_buf,0);
}
int kmm_check_curr( const std::string &test_name, Cbc3BeFc7 *cbc3be, usbtmc_device_data *ut_handle, std::ostream &os ){

    int result(0);

    usbtmc_printf(ut_handle,":SENSE:FUNC \"VOLT:DC\"");     //dc voltage measurements
    usbtmc_printf(ut_handle,":SENSE:VOLT:NPLC 1");          //time to average the input for each measurement, in power line cycles
    usbtmc_printf(ut_handle,":SENSE:VOLT:RANG 0.1");         //input voltage range
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO OFF");        //autozero on request only
    usbtmc_printf(ut_handle,":SAMPle:COUNt 1");
    usbtmc_printf(ut_handle,":TRIGger:SOURce IMMediate");

    //--------------------
    //VDDD measurement
    //--------------------
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDD_I );
    usleep( 2000 );

    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    //read response
    kmm_read( ":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    //cout << kmm_response_buf << endl;
    float kmm_val = strtof((char *)kmm_response_buf,0);
    float vddd_cur = kmm_val * 10000;//mA
    if(vddd_cur >= 50) result = WT_ERR_CURR; 
    char tmpbuf[256];
    sprintf( tmpbuf, "VDDD current : %3.1f mA", vddd_cur );
    print_test(test_name, tmpbuf, os); 

    //--------------------
    //VLDOI measurement
    //--------------------
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VLDOI_I );
    usleep( 2000 );

    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");
    //read response
    kmm_read( ":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    //cout << kmm_response_buf << endl;
    kmm_val = strtof((char *)kmm_response_buf,0);
    float vldoi_cur = kmm_val * 10000;//mA
    if(vldoi_cur >= 200) result = WT_ERR_CURR; 
    sprintf( tmpbuf, "VLDOI current : %3.1f mA", vldoi_cur );
    print_test(test_name, tmpbuf, os); 
    return result;
}
float kmm_read_bandgap( Cbc3BeFc7 *cbc3be, usbtmc_device_data *ut_handle ){

    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
    usleep( 2000 );
    usbtmc_printf(ut_handle,":SENSE:FUNC \"VOLT:DC\"");     //dc voltage measurements
    usbtmc_printf(ut_handle,":SENSE:VOLT:NPLC 1");          //time to average the input for each measurement, in power line cycles
    usbtmc_printf(ut_handle,":SENSE:VOLT:RANG 10");         //input voltage range
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO OFF");        //autozero on request only
    usbtmc_printf(ut_handle,":SAMPle:COUNt 1");
    usbtmc_printf(ut_handle,":TRIGger:SOURce IMMediate");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usbtmc_printf(ut_handle,":FETCH?");  
    //read response
    for(int i=0; i < KMM_BUFLEN; i++) kmm_response_buf[i] = 0; 
    int a=usbtmc_read(ut_handle, kmm_response_buf, KMM_BUFLEN,&kmm_reason);
    //cout << kmm_response_buf << endl;
    float kmm_val = strtof((char *)kmm_response_buf,0);
    return kmm_val;

}
void read_data_nwords(Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords, ostream &os, int debug = 0){

    int read_nwords(0); 
    cbc3be->MinNumWordToWait(nwords);
    if( cbc3be->WaitData( 1, true ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if(debug >= 2){
	int wpl = 4;
	os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
	for(unsigned ii=0; ii < nwords; ii++){
	    os << setbase(16) << setfill('0') << setw(8) << buffer[ii] << ' ';
	    if( ii % wpl == wpl - 1 ){
		os << endl;
		if( ii != nwords-1) 
		    os << setbase(16) << setfill('0') << setw(8) << ii+1 << ' ';
	    }
	}
	os << endl;
    }
    if( read_nwords != (int)nwords ){
	stringstream oss;
	oss << nwords << " words requested and " << read_nwords << " read.";
	throw WaferTestException( "Data read failed." + oss.str(), WT_ERR_DAQ ); 
    }
}

typedef int wt_func(const std::string &test_name, Cbc3BeFc7 *cbc3be, std::ostream &os, 
	std::map<std::string, std::string> &wt_str_configs, 
	std::map<std::string, int> &wt_int_configs, 
	int debug ); 


int run_test( wt_func func, const std::string &test_name, Cbc3BeFc7 *cbc3be, std::ostream &os, 
	std::map<std::string, std::string> &wt_str_configs, 
	std::map<std::string, int> &wt_int_configs, 
	int debug ){

    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int result = func(test_name, cbc3be, os, wt_str_configs, wt_int_configs, debug);
    gettimeofday(&t,0);
    timersub(&t,&t0,&dt);
    print_delta_t( test_name, dt, os );
    return result;
}

void reset_data( uint32_t *rawdata, int *slvs6, unsigned N ){

    for(unsigned i=0; i<N; i++){
	rawdata[i] = 0;
	if(i%2) slvs6[i/2] = 0;
    }
} 

void init_run(Cbc3BeFc7 *cbc3be){

    unsigned trig_lat(0);
    cbc3be->rwCbcI2cRegsByName(1,0,"FcCntrlCompBetaTrigLat.trig_lat", trig_lat, cbc_id );
    cerr << "Trigger Latency = " << std::dec << trig_lat << endl;

    char cnode[256];
    sprintf( cnode, "cs_cnfg.cbc_dp.cbc%d.latencies.l1a", cbc_id );
    cbc3be->WriteBeBoardConfig( cnode, trig_lat ); 
    //system( "cbc3daq 'ipbus read node=cs_cnfg.*cbc1.*latencies.*'");

    cbc3be->SendFastSignal( "reset" ); 
    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.fcg_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.start_trigger", 1 ); 

}
bool run_fsc_and_take_data( Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords ){

    //    cbc3be->InitializeRun();
    //sleep(1);
    cbc3be->StartFscBramController();
    //cbc3be->ReadBeBoardRegister( "cs_stat.cbc_dp.cbc1.data_frame_counter" );
    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    bool ok(true);
    if( read_nwords != nwords )ok = false; 
    return ok;
}

bool wait_seq_ready(Cbc3BeFc7 *cbc3be, int timeout_sec=1){

    ostringstream oss;
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int fsm = cbc3be->ReadBeBoardRegister( "cs_stat.seq.fsm" );
    while( fsm != 2 ){  
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > timeout_sec ){
	    oss.str("");
	    oss << "Sequencer is stuck with fsm = " << fsm;
	    throw WaferTestException( "Sequencer problem." + oss.str(), WT_ERR_SEQ );
	    return false;
	}
	usleep(1);
	fsm = cbc3be->ReadBeBoardRegister( "cs_stat.seq.fsm" );
    }
    return true;
}
/*
void check_cbci2c_reply_fifo( uhal::HwInterface *hw, std::ostream &os, int debug = 0 ){

    unsigned n_replies = read_ipbus(hw, "cs_stat.cbc_i2c_bm.bus1.reply_fifo_ndata" );
    std::vector<CBCI2C_REPLY> replies = read_cbc_i2c_replies( hw, 1, n_replies );
    if(debug){
	dump_replies(replies, os);
    }
    
    bool good = good_replies(replies);
    if(!good){
	dump_replies(replies, os);
       	throw WaferTestException( "CBC I2C replies from sequencer are not good.", WT_ERR_I2C );
    }
}
*/
bool run_rdb_and_fsc( Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords ){

    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.fcg_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.fcm.start_trigger", 1 ); 

    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    int read_nwords = cbc3be->ReadRawData( buffer, nwords );
    bool ok(true);
    if( read_nwords != nwords )ok = false; 
    return ok;
} 
void dump_raw_data( uint32_t *buffer, int n, std::ostream &os ){

    unsigned wpl(2);
    os << setbase(16);
    os << setfill('0');
    os << setw(8);
    os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
    for(int i=0; i < n; i++){
	os << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
	if( i % wpl == wpl - 1 ){
	    os << endl;
	    if( i != n-1) 
		os << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
	}
    }
}

int find_stubs(const uint32_t *buffer, int n, vector<int> &saddr, vector<int> &sbend, int offset=0){

    for(int i=offset; i < n; i++){

	int slvs1 = (buffer[2*i] & 0x000000FF);
	if(slvs1 != 0 ){
	    int slvs4 = (buffer[2*i] & 0xFF000000) >> 24;
	    saddr.push_back(slvs1);
	    sbend.push_back(slvs4 & 0x0F);

	    int slvs2 = (buffer[2*i] & 0x0000FF00) >> 8;
	    if(slvs2 != 0){
		saddr.push_back(slvs2);
		sbend.push_back((slvs4 & 0xF0) >> 4);

		int slvs3 = (buffer[2*i] & 0x00FF0000) >> 16;
		if(slvs4 != 0){
		    int slvs5 = (buffer[2*i+1] & 0x000000FF);
		    saddr.push_back(slvs3);
		    sbend.push_back(slvs5 & 0x0F);
		}
	    }
	    return i;
	}

    }
    return -1;
}

void get_slvs6(const uint32_t *buffer, int *slvs6, int n ){

    for( int i=0; i < n; i++){
	slvs6[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}
void get_error_bits(int *slvs6, int hp, bool &e1, bool &e2){
    int p(hp);
    e1 = (slvs6[p] & 0x20) ? 1 : 0;
    e2 = (slvs6[p] & 0x10) ? 1 : 0;
}
int get_pipeline_address(int *slvs6, int hp){
    int p(hp);
    int paddr = (slvs6[p] & 0xF) << 5;  
    paddr |= (slvs6[++p] & 0xF8) >> 3; 
    return paddr;
}
int get_l1counter(int *slvs6, int hp){
    int p=hp+1;
    int l1cntr = (slvs6[p] & 0x07) << 6;
    l1cntr |= (slvs6[++p] & 0xFC) >> 2;  
    return l1cntr;
}
int find_data_frame_header_position( int *slvs6, int n, int offset=0 ){

    for(int i=offset; i < n; i++){
	if( (slvs6[i] & 0xC0) == 0xC0 ) return i;
    }
    return -1;
}
bool has_hit(int *slvs6, int ph, int ch ){

    int p_bpos = 7-(ch+5)%8;
    int p_buf = (ch+5)/8 + ph + 2;
    return slvs6[p_buf] & (1 << p_bpos);
}
void dump_chdata( int *slvs6, int ph, std::ostream &os ){

    os << "Channel hit" << endl;
    for(int ch=1; ch < 255; ch++){
	if( ch % 32 == 1 ){
	    int ech(0);
	    if( ch / 32 != 7 ) ech = ch + 31;
	    else ech = ch + 29; 
	    os << endl << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	}
	os << has_hit( slvs6, ph, ch );
    }
}
void dump_i2cregs( Cbc3BeFc7 *cbc3be, std::ostream &os ){

    CBCI2C_TYPE_DATA_MAP cbci2c_type_data_map = cbc3be->readCbcAllI2cRegsByType(cbc_id);
    //print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
    CBCI2C_TYPE_DATA_MAP::iterator it = cbci2c_type_data_map.begin();
    for(; it != cbci2c_type_data_map.end(); it++){
	CBCI2C_ITEM_DATA_MAP &id = it->second;
	CBCI2C_ITEM_DATA_MAP::iterator item_it = id.begin();
	string item;
	for(; item_it != id.end(); item_it++){
	    item = it->first + "." + item_it->first;
	    os << std::setfill(' ') << setw(50) << std::left << item << " "; 
	    os << setw(3) << std::right << std::dec << item_it->second << endl;
	}
    }
}
void print_buff_hit( bool hit[32][254], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned ch=1; ch<255; ch++){
	    if( ch % 32 == 1 ){
		int ech(0);
		if( ch / 32 != 7 ) ech = ch + 31;
		else ech = ch + 29; 
		os << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	    }
	    os << hit[bi][ch-1] << " ";
	    if( ch % 32 == 0 ) os << endl;
	}
	os << endl;
    }

}
void print_buff_pipeline( int paddr_values[32][512], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned i=0; i<512; i++){
	    if( i % 32 == 0 ){
		int ei = i + 31;
		os << "delay[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
	    }
	    os << std::setw(3) << std::setfill('0') << std::hex << paddr_values[bi][i] << " ";
	    if( i%32 == 31 ) os << endl;
	}
    }
}
void print_buff_l1a_count( int l1a_count[32][512], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned i=0; i<512; i++){
	    if( i % 32 == 0 ){
		int ei = i + 31;
		os << "data block[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
	    }
	    os << std::setw(3) << std::setfill('0') << std::hex << l1a_count[bi][i] << " ";
	    if( i%32 == 31 ) os << endl;
	}
    }

}
bool read_after_burn_fuse_values( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os, unsigned chip_id, unsigned band_gap ){

    bool ok(true);
    bool i2c_ok(true);
    string name;
    unsigned value(0);
    name = "ChipIdFuse.sel";
    value = 1;
    //The read back value stays 0
    i2c_ok = cbc3be->rwCbcI2cRegsByName( 0, 1, name, value, 1 ); 
    name = "ChipIdFuse.id";
    i2c_ok &= cbc3be->rwCbcI2cRegsByName( 1, 0, name, value, 1 ); 

    ostringstream oss;
    oss << "after-burn fuse register : ID value      = " << value;
    print_test( test_name, oss.str(), os );
    if( value != chip_id ) ok = false; 

    name = "BandgapFuse.override";
    value = 0;
    i2c_ok &= cbc3be->rwCbcI2cRegsByName( 1, 1, name, value, 1 ); 
    name = "BandgapFuse.sel";
    value = 1;
    //The read back value for the sel stays 1, but the band gap value is afer burn fuse value
    i2c_ok &= cbc3be->rwCbcI2cRegsByName( 0, 1, name, value, 1 ); 
    name = "BandgapFuse.bg_fuse";
    i2c_ok &= cbc3be->rwCbcI2cRegsByName( 1, 0, name, value, 1 ); 

    oss.str("");
    oss << "after-burn fuse register : Bandgap value = " << value;
    print_test( test_name, oss.str(), os );
    if( value != band_gap ) ok = false; 

    if(!i2c_ok) throw WaferTestException( "cbc i2c failed at after fuse check.", WT_ERR_I2C ); 

    return ok;
}
bool offset_tuning( Cbc3BeFc7 *cbc3be, ostream &os, int sample_count, int debug ){

    int group_type(8);
    const GROUP_CHANNELSET_MAP &group_map = Cbc3BeInterface::SetGroupChannelSetMap(group_type);

    /*
    cbc3be->ReadAllCbcConfig(cbc_id);
    print_regcnf( cbc3be->GetCbcI2cRegReplies(), os, cbc_id );
    */

    CBCI2C_ITEM_DATA_MAP item_data;
    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
    cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, cbc_id );

    //Finding pedestal VCTH
    map<CBC_ID, vector<unsigned> > offsets0_map;
    vector<unsigned> offsets0(254,0x80);
    offsets0_map[cbc_id] = offsets0;
    cbc3be->FindVcth( sample_count, 0.5, &offsets0_map );

    unsigned cbc_id = 1;

    if(debug) os << "Tuning Offsets begins" << endl;
    //Tuning Offsets
    map<CBC_ID, map<CHANNEL_ID,unsigned> > all_offsets_map;

    for( unsigned g=0; g<group_map.size(); g++){

	map<CBC_ID, map<CHANNEL_ID,unsigned> > offsets_map;
	cbc3be->TuneOffsets(cbc_id, g,sample_count, offsets_map[cbc_id]);

	//add the tuned values in the global offset map
	all_offsets_map[cbc_id].insert(offsets_map[cbc_id].begin(), offsets_map[cbc_id].end());
    }

    //saving the results and writing those tuned values to CBC.
    char fname[256];
    char fname_root[256];
    sprintf(fname_root, "offset_tuning_be%d.root", cbc3be->GetBeId() );
    TFile *froot = new TFile(fname_root, "recreate");

    //offset histogram is filled.
    TH1F *h = new TH1F( Form("hoffsets_be%dcbc%d",cbc3be->GetBeId(), cbc_id), "; offset; count", 256, -0.5, 255.5 );
    map<CHANNEL_ID,unsigned> the_map =  all_offsets_map[cbc_id];
    map<CHANNEL_ID,unsigned>::iterator it_the_map = the_map.begin();
    for(; it_the_map != the_map.end(); it_the_map++){
	h->Fill(it_the_map->second);
    }
    cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, cbc_id );
    //New configuration file is opened.
    sprintf(fname, "offset_tuning-be%dcbc%d.knt", cbc3be->GetBeId(), cbc_id );
    ofstream ofile(fname);

    //Writing the tuned values to the registers
    if(debug) os << "Write tuned offset values to the registers" << endl;
    cbc3be->rwCbcI2c_Offsets(1,1,all_offsets_map[cbc_id],cbc_id);

    //Writing the tuned configuration to a file.
    if(debug) os << "Read all the registers on CBC " << cbc_id << " and write the tuned configuration to a file." << endl;
    cbc3be->ReadAllCbcConfig(cbc_id);
    CBCI2C_REGS regs = cbc3be->GetCbcI2cRegReplies();
    print_regcnf(regs, ofile, cbc_id);
    ofile.close();

    if(debug) os << "The parameter tunings finished for CBC " << cbc_id << "." << endl;

    froot->Write();
    froot->Close();

    return true;
}

int vcth_scan( Cbc3BeFc7 *cbc3be, 
	double &mean, double &mean_e, double &sigma, double &sigma_e,
	ostream &os, string fname, int sample_count, bool tp_en, int debug ){

    int result(0);
    bool fit_failed(false);
    int n_goodfits(0);
    double gmin(1023), gmax(0);
    double sum_m(0), sum2_m(0), sum_s(0), sum2_s(0), sum2_me(0), sum2_se(0);

    CBCI2C_ITEM_DATA_MAP item_data;
    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
    cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, cbc_id );

    //Get initial offsets
    map<unsigned, vector<unsigned> > offsets0;
    cbc3be->rwCbcI2c_Offsets(1,0,offsets0[cbc_id],cbc_id);
    //Set all offsets to 0xFF
    unsigned offset(0xFF);
    cbc3be->rwCbcI2c_Offset(1,1,0,offset,cbc_id);
    //Opening ROOT file to save histograms.
    ostringstream oss;
    if(debug)oss << "A root file " << fname << " is created.";
    print_test( "vcth_scan", oss.str(), os ); 

    TFile fout( fname.c_str(), "recreate" ); 

    int group_type(8);
    Cbc3BeInterface::SetGroupChannelSetMap(group_type);
    int ng = Cbc3BeInterface::GetGroupChannelSetMap().size(); 
    //VCTH scan
    for( int g=0; g<ng; g++){

	if( tp_en ){
	    CBCI2C_ITEM_DATA_MAP item_data;
	    item_data["group"] = g;
	    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) )  
		throw WaferTestException( "cbc i2c TestPulseDelayGroup write operation failed.", WT_ERR_I2C ); 
	    // cout << "group = " << std::dec << g << " delay = " << tp_delay << endl;
	}

	cbc3be->ActivateGroupOffsets( g, offsets0, 0 );

	CbcHistList<TH1F> hlist;
	//	map<string, TH1F *> hmap;

	bool desc(false);
	int min(-1);
	int max(-1);
	cbc3be->ScanVcth(g, sample_count, hlist, desc, min, max );

//	double mean(0), mean_e(0), sigma(0), sigma_e(0);

	int be_id(0), cbc_id(0), ch(0);
	map<unsigned, TH1F*> &hmap = hlist.GetHistMap();
	map<unsigned,TH1F *>::iterator it_hmap = hmap.begin();
	for( ; it_hmap != hmap.end(); it_hmap++ ){
	    TH1F *hist = it_hmap->second;
	    TString hname = hist->GetName();
	    if(hname.Contains("ch")){
		sscanf( hname, "hbe%dcbc%02dch%03d", &be_id, &cbc_id, &ch );
		double m(0), m_e(0), s(0), s_e(0);
		double rchi2 = analyze_scurve(hist, m, m_e, s, s_e, gmin, gmax );
		cout << "parameters(be,cbc,ch,rch2,m,me,s,se) = " << be_id << ", " << cbc_id << ", " << ch << ", " << rchi2 << ", " << m << ", " << m_e << ", " << s << ", " << s_e << endl;
		if(rchi2){
		    sum_m += m;
		    sum2_m += m*m;
		    sum2_me += m_e*m_e;
		    sum_s += s;
		    sum2_s += s*s;
		    sum2_se += s_e*s_e;
		    n_goodfits++;
		}
		else{
		    fit_failed = true;
		}
	    }
	    it_hmap->second->Write();
	    it_hmap->second->Print();
	}

	cbc3be->DisableGroupOffsets( g, 0 );
    }
    fout.Close();

    cout << "n_goodfits = " << n_goodfits << endl;
    mean = sum_m / n_goodfits;
    mean_e = sqrt( (sum2_m/n_goodfits - mean*mean)) + sqrt(sum2_me/n_goodfits);
    sigma = sum_s / n_goodfits;
    sigma_e = sqrt( (sum2_s/n_goodfits - sigma*sigma)) + sqrt(sum2_se/n_goodfits);

    cbc3be->rwCbcI2c_Offsets(1,1,offsets0[cbc_id],cbc_id);
    cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, cbc_id );

    string command = "root -q -b '";
    command = command + cbc3hal_dir + "/root_scripts/scurve_analyzer_wt.C+(\"";
    command = command + fname + "\", \"be1cbc01\",\"\", 1,0 )' 2>&1 >& rootlog.txt";
    system( command.c_str() );
    return fit_failed;
}


std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}

//valid stubs are in the range of i:[0-127], j:[0-14] for (nss+nsw%2)==0, j:[0-13] for (nss+nsw%2)==1 
int get_seed_address( int i, int nss ){
    int cs = i * 2 + 2;
    if(!(nss%2)) return cs+1;
    else return cs;
}
bool get_seed_channels( int i, int nss, std::vector<int> &chs, int ls ){

    int cs = i * 2 + 1 + ls;
    //if(nss%2) cs += 2;
    chs.push_back(cs);
    if(nss > 1) chs.push_back(cs+2);
    if(nss > 2) chs.push_back(cs-2);
    if(nss > 3) chs.push_back(cs+4);
    if(nss > 4) chs.push_back(cs-4);
    if(nss > 5) chs.push_back(cs+6);

    for(unsigned s_i=0; s_i < chs.size(); s_i++){
	if(chs.at(s_i) < 1 || chs.at(s_i)>254){
	    chs.clear();
	    return false;
	}	
    }
    return true;
}
bool get_window_channels( int saddr, int ibend, int nsw, std::vector<int> &chs, int ls ){

    bool nss_odd = !(saddr % 2);
    int bend_sign = (ibend < 0) ? -1 : 1;
    int half = (nss_odd + nsw)%2;

//    cout << "nss_odd, bend_sign, half = " << nss_odd << " " << bend_sign << " " << half << endl;
    int cw = (saddr/2 + !nss_odd * half +ibend)*2-ls; 

    chs.push_back(cw);
    if( nsw > 1 ) chs.push_back(cw+2); 
    if( nsw > 2 ) chs.push_back(cw-2);
    if( nsw > 3 ) chs.push_back(cw+4);
    if( nsw > 4 ) chs.push_back(cw-4);
    if( nsw > 5 ) chs.push_back(cw+6);

    for(unsigned w_i=0; w_i < chs.size(); w_i++){
	if(chs.at(w_i) < 1 || chs.at(w_i)>254){
	    chs.clear();
	    return false;
	}
    }	
    return true;
}

std::vector<int> get_stub_channels( int i, int ibend, int nss, int nsw, int ls ){

    std::vector<int> chs_seed;
    bool valid_seed = get_seed_channels(i, nss, chs_seed, ls); 
    int saddr = get_seed_address(i, nss);
    std::vector<int> chs_window;
    bool valid_window = get_window_channels(saddr, ibend, nsw, chs_window, ls); 

    if( valid_seed && valid_window ) 
	chs_seed.insert(chs_seed.end(), chs_window.begin(), chs_window.end());
    else chs_seed.clear();

    return chs_seed;
}

int frame_11( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test( test_name, "Mark's frame 4 and 11. Raw data frame check. Not needed practically.", os); 

    string fscbram_file = wt_str_configs["fscbram_file"];

    unsigned N(120);
    unsigned n = N/2;

    uint32_t *rawdata(0);
    int *slvs6(0);

    cbc3be->WriteBeBoardConfig( "cs_cnfg.rdb.cbc_id", 1 );
    cbc3be->WriteBeBoardConfig( "cs_cnfg.rdb.latency", 0 );
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

    cbc3be->WriteFscBram(fscbram_file);

    cbc3be->InitializeRun();
    //init_run(cbc3be);

    rawdata = new uint32_t[N];
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);

    int read_nwords = cbc3be->ReadRawData( rawdata, N );
    if(read_nwords == -1 || read_nwords != N) 
	throw WaferTestException( test_name + "Raw data buffer problem.", WT_ERR_RAW_DATA );


    if(debug) dump_raw_data(rawdata,N, os);

    n = N/2;
    slvs6 = new int[n];
    get_slvs6(rawdata, slvs6, n);

    int hp = find_data_frame_header_position(slvs6, n);

    ostringstream oss;
    oss << "Header position is " << std::dec << hp; 
    print_test( test_name, oss.str(), os );

    if(hp < 0) {
	delete rawdata;
	delete slvs6;
	throw WaferTestException( test_name + " failed.", WT_ERR_RAW_DATA ); 
    }
    int paddr=-1;
    int l1cntr=-1;
    paddr = get_pipeline_address(slvs6, hp);
    oss.str("");
    oss << "Pipeline address is " << std::dec << paddr;
    print_test( test_name, oss.str(), os );
    l1cntr = get_l1counter(slvs6, hp); 
    oss.str("");
    oss << "L1 counter value is " << std::dec << l1cntr;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "Channel hit";
    for(int ch=1; ch < 255; ch++){
	if( ch % 32 == 1 ){
	    print_test( test_name, oss.str(), os );
	    int ech(0);
	    if( ch / 32 != 7 ) ech = ch + 31;
	    else ech = ch + 29; 
	    oss.str("");
	    oss << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	}
	oss << has_hit( slvs6, hp, ch );
    }
    print_test( test_name, oss.str(), os );

    if(debug){
	dump_raw_data(rawdata,N, os);
	dump_chdata(slvs6, hp, os);
    }

    delete rawdata;
    delete slvs6;
    return 0;
}
int frame_14( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os, 
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    ostringstream oss;
    char tmp[256];

    int noFuse(true);
    unsigned chip_id(0);
    float target_bg(0.5);
    if( wt_int_configs.find("no_fuse") != wt_int_configs.end() ){
	noFuse = wt_int_configs["no_fuse"];
    }
    if( wt_int_configs.find("chip_id") != wt_int_configs.end() ){
	chip_id = wt_int_configs["chip_id"];
    }
    if( wt_str_configs.find("target_bg") != wt_str_configs.end() ){
	sscanf( wt_str_configs["target_bg"].c_str(), "%f", &target_bg);
    }

    if( wt_str_configs.find("cbci2c_file_1") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file_1"], cbc_id );
    }
    else{
	throw WaferTestException( "frame_14 : specify frame_14_1 cbc i2c configuration file.", WT_ERR_FUSE_PROC );
    }
    //----------------------------------
    // band gap read for band gap scan
    //----------------------------------
    //band gap read setting for relayA
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
    //usleep( 2000 ); -- not needed for external trigger read.

    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":TRIGger:SOURce EXTernal");
    int sample_count(1);
    int trig_count(64);
    sprintf(tmp, ":SAMPle:COUNt %d", sample_count);
    usbtmc_printf(ut_handle, tmp);
    sprintf(tmp, ":TRIGger:COUNt %d", trig_count);
    usbtmc_printf(ut_handle, tmp);
    usbtmc_printf(ut_handle,":INIT");
    usleep(50000);

    vector<uint32_t> fscbuf_data;
    unsigned value(0);

    unsigned bg_reg_addr = 21; 
    unsigned reg_value(0);
    int ntrig(0);
    for( unsigned bg=0; bg < 64; bg++){
	reg_value = 0x81100000 | (bg_reg_addr << 8) | (0 << 7) | bg;//override(1:override), sel(0:register)
	fscbuf_data.push_back(reg_value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	fscbuf_data.push_back((unsigned) 0x28000000);
	ntrig++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    fscbuf_data.clear();

    //run sequencer 
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be, ntrig/3);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    //keithley read
    int nread = kmm_read_ndata( ut_handle, kmm_response_buf, KMM_BUFLEN, ntrig, debug ); 
    printf("Bandgap voltage values for BC I2C setting 0 - 63 : %s\n", (char *)kmm_response_buf);
    vector<float> bg_meas = get_kmm_vdata(kmm_response_buf);

    //----------------------------------
    // VDDA read for band gap scan
    //----------------------------------
    //VDDA read setting for relayA
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
    //usleep( 2000 ); -- not necessary for external trigger read.
    //change KMM state to trigger waiting
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");
    usleep(50000); //needed for external trigger

    ntrig = 0;
    fscbuf_data.clear();
    for( unsigned bg=0; bg < 64; bg++){
	reg_value = 0x81100000 | (bg_reg_addr << 8) | (0 << 7) | bg;//override(1:override), sel(0:register)
	fscbuf_data.push_back(reg_value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	fscbuf_data.push_back((unsigned) 0x28000000);
	ntrig++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    fscbuf_data.clear();

    //run sequencer 
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be, ntrig/3);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    //wait keithley read
    nread = kmm_read_ndata( ut_handle, kmm_response_buf, KMM_BUFLEN, ntrig, debug ); 
    printf("VDDA values for BC I2C setting 0 - 63 : %s\n", (char *)kmm_response_buf);
    vector<float> vdda_meas = get_kmm_vdata(kmm_response_buf);

    //----------------------------------
    // Find best band gap setting 
    //----------------------------------
    int best_bg_i2c(0);
    float best_bg(0);
    float diff(1);
    for(unsigned i=0; i < 64; i++){
//	cout << i << ", " << bg_meas.at(i) << fabs(bg_meas.at(i) - target_bg) << ", " << diff << endl;
	if( fabs(bg_meas.at(i) - target_bg) < diff ){
//	    cout << "best found." << endl;
	    best_bg_i2c = i+1;
	    best_bg = bg_meas.at(i);
	    diff = fabs(best_bg-target_bg);
	} 
    }
    sprintf(tmp, "The best BG setting is found with I2C value %d at BG %.4f V for target %.4f.", best_bg_i2c, best_bg,target_bg);
    print_test(test_name, tmp, os); 

    //----------------------------------
    //read preburn bg & vdda values with the best bg setting
    //----------------------------------
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    sample_count=1;
    trig_count=1;
    sprintf(tmp, ":SAMPle:COUNt %d", sample_count);
    usbtmc_printf(ut_handle, tmp);
    sprintf(tmp, ":TRIGger:COUNt %d", trig_count);
    usbtmc_printf(ut_handle, tmp);
    usbtmc_printf(ut_handle,":INIT");
    usleep(50000);

    fscbuf_data.clear();
    ntrig=0;
    reg_value = 0x81100000 | (bg_reg_addr << 8) | (0 << 7) | best_bg_i2c;//override(1:override), sel(0:register)
    fscbuf_data.push_back(reg_value);
    fscbuf_data.push_back((unsigned) 0x22000000);
    fscbuf_data.push_back((unsigned) 0x00000001);
    fscbuf_data.push_back((unsigned) 0x28000000);
    ntrig++;
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    fscbuf_data.clear();

    //run sequencer 
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be, ntrig/3);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    //wait keithley read
    nread = kmm_read_ndata( ut_handle, kmm_response_buf, KMM_BUFLEN, ntrig, debug ); 
    printf("VDDA value for the best BG I2C setting %d : %s\n", best_bg_i2c, (char *)kmm_response_buf);
    float preburn_vdda = strtof((char *)kmm_response_buf,0);

    //band gap read setting for relayA
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
    usleep( 2000 );
    usbtmc_printf(ut_handle,":TRIGger:SOURce IMMediate");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("Bandgap value for the best BG I2C setting %d : %s\n", best_bg_i2c, (char *)kmm_response_buf);
    float preburn_bg = strtof((char *)kmm_response_buf,0);

    if( !noFuse ){
	cout << "I am here.  fuse" << endl;

	cbc3be->WriteBeBoardConfig("relay1.relayB", RELAYB_ON );
	sleep(1);
	string name = "ChipIdFuse.id";
	cbc3be->rwCbcI2cRegsByName( 0, 1, name, chip_id, 1 ); 
	unsigned value = (1<<7 | best_bg_i2c);  
	cbc3be->rwCbcI2c_reg( 1, 1, 1, 21, value, 1 ); 
	usleep(500000);
	cbc3be->WriteBeBoardConfig("cs_ctrl.wt.fpp", 1);
	//send fuse program pulse for 0.9ms
	usleep(500000);
	cbc3be->WriteBeBoardConfig("relay1.relayB", RELAYB_OFF );
	cbc3be->CbcReset();
	cbc3be->CbcI2cBusInit();
    }
    if( wt_str_configs.find("cbci2c_file_2") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file_2"], cbc_id );
	cout << "I am here writing the second settings." << endl;
    }
    else{
	throw WaferTestException( "frame_14 : specify frame_14_2 cbc i2c configuration file.", WT_ERR_FUSE_PROC );
    }
    string name = "BandgapFuse.override";
    value = 0;
    cbc3be->rwCbcI2cRegsByName( 0, 1, name, value, 1 ); 
    name = "BandgapFuse.bg_fuse";
    cbc3be->rwCbcI2cRegsByName( 1, 0, name, value, 1 ); 
    sprintf(tmp, "Readback bandgap fuse value = %d.", value);
    print_test(test_name, tmp, os );

    usbtmc_printf(ut_handle,":INIT");	
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("After burn bandgap value for the best BG I2C setting %d : %s\n", best_bg_i2c, (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);
    float afterburn_bg = strtof((char *)kmm_response_buf,0);
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VDDA );
    usleep( 2000 );
    usbtmc_printf(ut_handle,":INIT");
    usleep(50000);
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("After burn VDDA value for the best BG I2C setting %d : %s\n", best_bg_i2c, (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);
    float afterburn_vdda = strtof((char *)kmm_response_buf,0);
    wt_int_configs["bandGap"] = best_bg_i2c;

    return 0;
}
int frame_15( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os, 
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    int result(0);

    unsigned chipId(0), bandGap(0);
    if( wt_int_configs.find("chipId")!= wt_int_configs.end() ){
	chipId = wt_int_configs["chipId"];
    }
    if( wt_int_configs.find("bandGap")!= wt_int_configs.end() ){
	bandGap = wt_int_configs["bandGap"];
    }
    //cout << "chip id = " << chipId << " band gap = " << bandGap << endl;
    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();

    print_test(test_name, "after hard reset, default configuration", os);
    if( !read_after_burn_fuse_values(test_name, cbc3be, os, chipId, bandGap) ) result = WT_ERR_AFTER_FUSE; 

    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();
    //AMUX = 7, IPRE1=100
    cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );

    print_test(test_name, "after hard reset, frame 15 configuration", os);
    if( !read_after_burn_fuse_values(test_name, cbc3be, os, chipId, bandGap) ) result |= WT_ERR_AFTER_FUSE; 

    float bg = kmm_read_bandgap(cbc3be, ut_handle);
    char tmpbuf[256];
    sprintf( tmpbuf, "Bandgap : %.3f V", bg );
    print_test(test_name, tmpbuf, os); 

    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();
    print_test(test_name, "after hard reset, default configuration", os);
    if( !read_after_burn_fuse_values(test_name, cbc3be, os, chipId, bandGap) ) result |= WT_ERR_AFTER_FUSE; 
    float vdda = kmm_read_VDDA(cbc3be, ut_handle);
    sprintf( tmpbuf, "VDDA : %.3f V", vdda );
    print_test(test_name, tmpbuf, os); 

    return result;
}
int frame_16( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    int result(0);

    print_test(test_name, "Pedestal offsets tuning.", os);
    cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);

    //Get sample count
    int sample_count(100);
    std::map<std::string,uint32_t> regmap;
    cbc3be->ReadBeBoardConfig( wt_str_configs["config_file"], regmap );
    std::map<std::string,uint32_t>::iterator it_regmap = regmap.begin();
    it_regmap = regmap.find( "cs_cnfg.fcm.fcg.Ncycle" );
    if( it_regmap != regmap.end() ) sample_count = it_regmap->second; 

    cbc3be->CbcReset();//Mark's proceduer
    //CBC I2C BUS INITIALIZATION AND CONFIGURATION
    cbc3be->CbcI2cBusInit();
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) ){
	result = 1;
    }
    //Offset tuning
    if( !offset_tuning(cbc3be, cout, sample_count, debug ) ) result = 1;
    return result;
}
int vcth_scan_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    unsigned sample_count = cbc3be->ReadBeBoardRegister( "cs_cnfg.fcm.fcg.Ncycle" );

    string ofname;
    if( wt_str_configs.find("output_file") == wt_str_configs.end() ){
	throw WaferTestException( "output file name is not specified.", WT_ERR_VCTH_SCAN); 
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );
    }
    if( wt_int_configs.find("TestPulsePolEnAMux.tp_en")!= wt_int_configs.end() ){
	unsigned value = wt_int_configs["TestPulsePolEnAMux.tp_en"];
	if(debug)cout << "Test pulse enabled? : " << value << endl;
	cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, cbc_id );
	cbc3be->rwCbcI2cRegsByName( 1, 0, "TestPulsePolEnAMux.tp_en", value, cbc_id ); 
	if(debug)cout << "Test pulse enabled read back value? : " << value << endl;

    }
    bool tp_en(false);
    if( wt_int_configs.find("tp_en")!= wt_int_configs.end() && wt_int_configs["tp_en"] == 1 ) tp_en = true;

    double mean(0), mean_e(0), sigma(0), sigma_e(0);
    int result = vcth_scan( cbc3be, 
	    mean, mean_e, sigma, sigma_e,
	    cout, ofname, sample_count, tp_en, debug );

    char tmp[256];
    sprintf( tmp, "scurve statistics (mean, mean err, sigma, sigma err) = (%.3f, %.3f, %.3f, %.3f)\n", mean, mean_e, sigma, sigma_e );
    print_test(test_name, tmp, os);
    wt_int_configs["scurve.mean"] = mean;
    wt_int_configs["scurve.mean_e"] = mean_e;
    wt_int_configs["scurve.sigma"] = sigma;
    wt_int_configs["scurve.sigma_e"] = sigma_e;

    if(debug)dump_i2cregs(cbc3be,os);
    return result;
}

int frame_18_001( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    int result(0);
    print_test(test_name, "Pipeline check - check all pipeline address, l1a counter, and all channel data should be 0 delaying the L1A by 1 clock 511 times.", os);
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( "cbc i2c VCTH.vcth write operation failed.", WT_ERR_I2C); 


    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    fscbuf_data.push_back((unsigned) 0xa0060000);
    //fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    int npipe[512];
    for(unsigned i=0; i < 512; i++) npipe[i]=0;
//    unsigned sum_header_bits(0);
    unsigned sum_err_bits(0);
    bool no_hit = true;


    unsigned nwords = 14 * 512;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;
    int ncbcdata(0);

    init_run(cbc3be);

    for(unsigned i=0; i < 512; i++){
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 

    }
    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords )
	throw WaferTestException( "Data read failed.", WT_ERR_DAQ ); 

    be_data_decoder.SetData( buffer, nwords * 4 );
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){

	    ncbcdata++;
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    sum_err_bits += cbc_dpd->Error(0);
	    sum_err_bits += cbc_dpd->Error(1);
	    int paddr = cbc_dpd->PipelineAddress();
	    if(paddr >=0 && paddr <= 511) npipe[paddr]++; 
	    if(cbc_dpd->HitChannels().size() != 0 ){
		no_hit = false;
		cout << "Some channels have hits!" << endl;
	    }
	}
    }
    delete buffer;
    /*
    unsigned N(80);
    uint32_t *rawdata = new uint32_t[N];
    unsigned n = N/2;
    int *slvs6 = new int[n];

    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

    for(unsigned i=0; i < 512; i++){

	cbc3be->WriteFscBram(fscbuf_data);
	run_rdb_and_fsc( cbc3be, rawdata, N );
	get_slvs6(rawdata, slvs6, n);
	int hp = find_data_frame_header_position(slvs6, n);
	int p = hp;
	sum_header_bits += (slvs6[p] & 0x80) >> 7;
	sum_header_bits += (slvs6[p] & 0x40) >> 6;
	bool e1(false), e2(false);
	get_error_bits(slvs6, hp, e1, e2);
	sum_err_bits += e1; 
	sum_err_bits += e2; 
	int paddr = get_pipeline_address(slvs6, hp);
	npipe[paddr]++;

	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 
	for(int ch=1; ch < 255; ch++){
	    if( has_hit(slvs6, hp, ch) ){
		no_hit = false;
		break;
	    }
	}
    }
    delete rawdata;
    delete slvs6;
    cout << "sum of all header bits is " << sum_header_bits << endl;
    */
    ostringstream oss;
    oss << "# of CBC DATA FRAME is " << ncbcdata << "."; 
    print_test( test_name, oss.str(), os );

    oss.str("");
    oss << "Sum of all error bits is " << sum_err_bits << ".";
    print_test( test_name, oss.str(), os );

    bool pipe_ok(true);
    oss.str("");
    oss << "Pipeline stats." << endl;
    for(unsigned i=0; i < 512; i++){
	if(i%32==0){
	    print_test( test_name, oss.str(), os );
	    int eaddr = i + 31;
	    oss.str("");
	    oss << "addr[" << std::setw(3) << std::setfill('0') << i << "-" << std::setw(3) << std::setfill('0') << eaddr << "]";
	}
	oss << npipe[i] << " ";
	if(npipe[i] != 1) {
	    pipe_ok = false;
	}
    }
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "no hit ? " << no_hit;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "pipeline address seen once only? " << pipe_ok;
    print_test( test_name, oss.str(), os );
    if(!pipe_ok || sum_err_bits || !no_hit) result = 1;
    return result;
}
int frame_18_002( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    int result(0);
    print_test(test_name, "Pipeline check - check all pipeline address, l1a counter, and all channel data should be 1 delaying the L1A by 1 clock 511 times.", os);

    ostringstream oss;

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException("i2c VCTH.vcth write operation failed.", WT_ERR_I2C); 

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
//    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    int npipe[512];
    for(unsigned i=0; i < 512; i++) npipe[i]=0;
//    unsigned sum_header_bits(0);
    unsigned sum_err_bits(0);
    bool all_hit = true;


    unsigned nwords = 14 * 512;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;
    int ncbcdata(0);

    init_run(cbc3be);

    for(unsigned i=0; i < 512; i++){
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 

    }

    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 0, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords ){
	oss.str("");
	oss << " Data read failed. " << read_nwords << " words read.";
	throw WaferTestException( oss.str(), WT_ERR_DAQ ); 
    }

    be_data_decoder.SetData( buffer, nwords * 4 );
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){

	    ncbcdata++;
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    if(debug)cbc_dpd->DumpData(os);
	    sum_err_bits += cbc_dpd->Error(0);
	    sum_err_bits += cbc_dpd->Error(1);
	    int paddr = cbc_dpd->PipelineAddress();
	    if(paddr >=0 && paddr <= 511) npipe[paddr]++; 
	    if(cbc_dpd->HitChannels().size() != 254 ){
		all_hit = false;
		cout << "Some all channels do not have hits!" << endl;
	    }
	}
    }
    if(debug){
	int wpl = 4;
	cout << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
	for(unsigned ii=0; ii < nwords; ii++){
	    cout << setbase(16) << setfill('0') << setw(8) << buffer[ii] << ' ';
	    //			if( i == 0 ) count = buffer[i];
	    if( ii % wpl == wpl - 1 ){
		cout << endl;
		if( ii != nwords-1) 
		    cout << setbase(16) << setfill('0') << setw(8) << ii+1 << ' ';
	    }
	}
    }
    delete buffer;

    oss.str("");
    oss << "# of CBC DATA is " << ncbcdata << "."; 
    print_test( test_name, oss.str(), os );

    oss.str("");
    oss << "Sum of all error bits is " << sum_err_bits << ".";

    bool pipe_ok(true);
    oss.str("");
    oss << "Pipeline stats." << endl;
    for(unsigned i=0; i < 512; i++){
	if(i%32==0){
	    print_test( test_name, oss.str(), os );
	    int eaddr = i + 31;
	    oss.str("");
	    oss << "addr[" << std::setw(3) << std::setfill('0') << i << "-" << std::setw(3) << std::setfill('0') << eaddr << "]";
	}
	oss << npipe[i] << " ";
	if(npipe[i] != 1) {
	    pipe_ok = false;
	}
    }
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "all hit ? " << all_hit;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "pipeline address seen once only? " << pipe_ok;
    print_test( test_name, oss.str(), os );
    if(!pipe_ok || sum_err_bits || !all_hit ) result = 1;
    return result;
}
int frame_19_001( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    ostringstream oss;

    string ofname("frame_19_001.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    unsigned value = wt_int_configs["40MHzClockOr254DLL.dll"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", value, cbc_id ) )
	throw WaferTestException( "cbc i2c 40MHzClockOr254DLL.dll write operation failed.", WT_ERR_I2C); 

    int ne1(0);
    int ne2(0);
    bool e1=false;
    bool e2=false;
    bool no_hit = true;

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0df0000);
    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);

    int N = 2600;
    int n = N/2;
    uint32_t *rawdata = new uint32_t[N];
    int *slvs6 = new int[n];
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

    bool hit[32][254];
    for(unsigned ch=0; ch < 254; ch++){
	for(unsigned bi=0; bi<32; bi++){
	    hit[bi][ch]=false;
	}
    }
    run_rdb_and_fsc( cbc3be, rawdata, N );
    get_slvs6(rawdata, slvs6, n);
    int hp=0;
    int ofst_incr=0;
    for(unsigned bi=0; bi<32; bi++){
	bool this_no_hit(true);
	if(bi!=0) ofst_incr = 35;
	hp = find_data_frame_header_position(slvs6, n, hp + ofst_incr);
	if(hp<0) {
	    dump_raw_data(rawdata,N, os);
	    delete rawdata;
	    delete slvs6;
	    ofile.close();
	    oss.str("");
	    oss << "header_error at Buffer[" << bi << "]";
	    throw WaferTestException( oss.str(), WT_ERR_RAW_DATA );
	    break;
	}
	get_error_bits(slvs6, hp, e1, e2);
	ne1+=e1;
	ne2+=e2;
	for(int ch=1; ch < 255; ch++){
	    if( has_hit(slvs6, hp, ch) ){
		hit[bi][ch-1]=true;
		this_no_hit = false;
		break;
	    }
	}
	if(!this_no_hit) no_hit = false; 
    }
    delete rawdata;
    delete slvs6;
    print_buff_hit(hit,ofile);
    ofile.close();

    if( !(ne1 == 0 && ne2 == 0 && no_hit )){

	oss.str("");
	if( !(ne1==0 && ne2 == 0) ){
	    oss << "error bit stats # of e1 = " << std::dec << ne1 << ", # of e2 = " << ne2;
	}
	if(!no_hit){
	    oss << ", some hits are found.";
	}
	return 1;
    }
    return 0;
}
int frame_19_002( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all error bits are 0 and data bits are 1.", os);

    ostringstream oss;

    string ofname("frame_19_002.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( "cbc i2c VCTH.vcth write operation failed.", WT_ERR_I2C); 

    value = wt_int_configs["40MHzClockOr254DLL.dll"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", value, cbc_id ) )
	throw WaferTestException( "cbc i2c 40MHzClockOr254DLL.dll write operation failed.", WT_ERR_I2C); 


    int ne1(0);
    int ne2(0);
    bool e1=false;
    bool e2=false;
    bool all_hit = true;

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1.  It seems like that the buffer overflow error disappears when the buffer gets free space.
//    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xe0000000);

    cbc3be->WriteFscBram(fscbuf_data);


    int N = 2600;
    int n = N/2;
    uint32_t *rawdata = new uint32_t[N];
    int *slvs6 = new int[n];
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 

    bool hit[32][254];
    for(unsigned ci=0; ci < 254; ci++){
	for(unsigned bi=0; bi<32; bi++){
	    hit[bi][ci]=true;
	}
    }

    init_run(cbc3be);
    cbc3be->StartFscBramController();
    if( wait_seq_ready(cbc3be) == 0 )
	throw WaferTestException( "Sequencer problem.", WT_ERR_SEQ );
    int read_nwords = cbc3be->ReadRawData( rawdata, N );
    if(read_nwords == -1 || read_nwords != N) 
	throw WaferTestException( test_name + "Raw data buffer problem.", WT_ERR_RAW_DATA );
    /*
    if( !run_rdb_and_fsc( cbc3be, rawdata, N ) ){

	delete rawdata;
	throw WaferTestException( test_name + " failed in run_rdb_and_fsc." );
    }
    if(wait_seq_ready(cbc3be)==0) 
	throw WaferTestException( test_name + " Sequencer problem." );
	*/

    if(debug) dump_raw_data(rawdata,N, os);

    get_slvs6(rawdata, slvs6, n);
    int hp=0;
    int ofst=0;
    for(int i=0; i < n; i++){ 
	if(slvs6[i] == 0){
	    ofst = i;
	    break;
	}
    }
    if(debug) os << "Header offset is " << ofst << endl;
    for(unsigned bi=0; bi<32; bi++){
	bool this_all_hit(true);
	if(bi!=0) ofst = 35;
	hp = find_data_frame_header_position(slvs6, n, hp + ofst);
	if(hp<0) {
	    dump_raw_data(rawdata,N, os);
	    delete rawdata;
	    delete slvs6;
	    oss.str("");
	    oss << "header_error at Buffer[" << bi << "]";
	    throw WaferTestException( oss.str(), WT_ERR_RAW_DATA );
	    break;
	}
	get_error_bits(slvs6, hp, e1, e2);
	ne1+=e1;
	ne2+=e2;
	if( e1 || e2 ) os << "Buffer["<< bi << "] errors are not both 0 [e1,e2] = " << e1 << "," << e2 << endl;
	for(int ch=1; ch < 255; ch++){
	    if( !has_hit(slvs6, hp, ch) ){
		hit[bi][ch-1]=false;
		this_all_hit = false;
	//	break;
	    }
	}
	if(!this_all_hit) all_hit = false; 
    }
    print_buff_hit(hit,ofile);
    ofile.close();

    if( !(ne1 == 0 && ne2 == 0 && all_hit )){

	oss.str("");
	if( !(ne1==0 && ne2 == 0) ){
	    oss << "error bit stats # of e1 = " << std::dec << ne1 << ", # of e2 = " << ne2;
	}
	if(!all_hit){
	    oss << endl << " Some channels do not have hits.";
	}
//	system("cbc3daq 'cbci2c reg r=1 file=offset_tuning/offset_tuning-be1cbc1.knt'" );
	return 1;
    }
    delete rawdata;
    delete slvs6;
    return 0;
}

int frame_19_003( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all pipeline address exists in all pipeline address buffer ram region.", os);

    ostringstream oss;

    string ofname("frame_19_003.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    unsigned value = wt_int_configs["FcCntrlCompBetaTrigLat.trig_lat"];
    oss << "Trigger latency = " << value;
    print_test( test_name, oss.str(), os );
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "FcCntrlCompBetaTrigLat.trig_lat", value, cbc_id ) )
	throw WaferTestException( "cbc i2c FcCntrlCompBetaTrigLat.trig_lat write operation failed.", WT_ERR_I2C); 

    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->SendFastSignal( "reset" ); 

    BeDataDecoder be_data_decoder;


    //pipeline
    int paddr_values[32][512];
    for(unsigned bi=0; bi<32; bi++){
	for(unsigned i=0; i<512; i++){
	    paddr_values[bi][i] = -1;
	}
    }
    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xa0100000);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    unsigned nwords = 14*32*256;
    cbc3be->MinNumWordToWait(nwords);

    bool failed(false);
    for(unsigned j=0; j<2;j++){
	for(unsigned i=0; i<256; i++){

	    unsigned pause = 0x16+i+j*256;
	    unsigned pause_com = (pause<<16) | 0xa0000000;
	    fscbuf_data[2] = pause_com;

	    if(debug)dump_seq_data(fscbuf_data,os);

	    cbc3be->WriteFscBram(fscbuf_data);
	    cbc3be->StartFscBramController();

	    if( wait_seq_ready(cbc3be) == 0 )
		throw WaferTestException( "Sequencer problem.", WT_ERR_SEQ );

	    /*
	    dump_seq_data(fscbuf_data,os);
	    unsigned nframes = cbc3be->ReadBeBoardRegister("cs_stat.cbc_dp.cbc1.data_frame_counter");
	    unsigned l1a = cbc3be->ReadBeBoardRegister("cs_stat.cbc_dp.cbc1.l1a_counter");
	    cout << "i = " << std::dec << i << ": # of frames at cbc data processor = " << std::dec << nframes << " # of l1a = " << l1a << endl;
	    */
	}
	uint32_t *buffer = new uint32_t[nwords];
	int read_nwords(0);
	if( cbc3be->WaitData( 1, false ) ){
	    read_nwords = cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << std::dec << nwords << " words expected to read but " << read_nwords << " read.";
	    throw WaferTestException( "Data read failed." + oss.str(), WT_ERR_DAQ ); 
	}

	be_data_decoder.SetData( buffer, nwords * 4 );
	int cd_i(0);
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		if(debug)cbc_dpd->DumpData(os);
		int paddr = cbc_dpd->PipelineAddress();
		int bi = cd_i % 32;//buffer id
		int i = cd_i / 32 + j * 256;//pipeline incrementation at a buffer location.
		int r_paddr = (i + bi)%512; 
		paddr_values[bi][i] = paddr;
		if(paddr != r_paddr) {
		    oss.str("");
		    oss << "Pipeline Address is " << paddr << ", but " << r_paddr << " is expected.";
		    print_test( test_name, oss.str(), os );

		    failed = true; 
		}
		cd_i++;
	    }
	}
	delete buffer;
    }
    print_buff_pipeline(paddr_values, ofile);
    ofile.close();

    if(debug){

	int addr(0);
	for(unsigned j=0; j<2;j++){
	    for(unsigned i=0; i<256; i++){
		addr = i + j * 256;
		for(unsigned bi=0; bi<32; bi++){
		    if(addr == 512) addr = 0;
		    if(paddr_values[bi][i+j*256] != addr ) {
			oss.str("");
			oss << "Buffer[" << std::dec << std::setw(2) << std::setfill('0') << bi << "] iteration " 
			    << i << "+" << j << "*256 should have address " << addr << ", but " << paddr_values[bi][i+j*256] << " was found.";
		    }
		    addr ++;
		}
	    }
	}
    }
    return failed;
}


int frame_19_004( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all Level 1 counter value exists in the all buffer ram region.", os);

    ostringstream oss;

    string ofname("frame_19_004.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    unsigned nwords = 14*32*16;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;

    int l1a_count[32][512];
    for(unsigned bi=0; bi<32; bi++){
	for(unsigned i=0; i<512; i++){
	    l1a_count[bi][i] = -1;
	}
    }
    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xa5000000);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->SendFastSignal( "orbit_reset" ); 

    int cd_i(0);
    for(unsigned j=0; j<32;j++){
	// 32 consecutive triggers are sent for 16 times for a single read out from BE.
	for(unsigned i=0; i<16; i++){

	    cbc3be->WriteFscBram(fscbuf_data);
	    cbc3be->StartFscBramController();
	}
	int read_nwords(0);
	cbc3be->MinNumWordToWait(nwords);
	if( cbc3be->WaitData( 1, false ) ){
	    read_nwords = cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << std::dec << nwords << " words expected to read but " << read_nwords << " read.";
	    throw WaferTestException( " Data read failed." + oss.str(), WT_ERR_DAQ ); 
	}

	be_data_decoder.SetData( buffer, nwords * 4 );
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		int l1ac = cbc_dpd->L1ACounter();
//		cout << "CBC data " << std::dec << cd_i << " counter " << l1ac << endl;
		int bi = cd_i % 32;
		int ci = cd_i / 32;
		l1a_count[bi][ci] = l1ac;
		cd_i++;
	    }
	}
	cbc3be->SendFastSignal( "trigger" ); 
	usleep(1);
	cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
    }
    delete buffer;
    print_buff_l1a_count(l1a_count, ofile);
    ofile.close();

    bool l1a_count_ok(true);
    int count(1);
    for(unsigned j=0; j<32; j++){
	for(unsigned i=0; i<16; i++){
	    for(unsigned bi=0; bi<32; bi++){
		if(count == 512 ) count = 0;
		if(count++ != l1a_count[bi][i+j*16]){
		    oss.str("");
		    oss << "Buffer[" << std::dec << std::setw(2) << std::setfill('0') << bi << "] iteration " 
		    << i << "+" << j << "*16 should have l1a count " << count-1 << ", but " << l1a_count[bi][i+j*16] << " was found.";
		    l1a_count_ok = false;
		    break;
		}
	    }
	}
	++count;
    }
    return !l1a_count_ok;
}
int frame_20( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    int result(0);
    print_test(test_name, "check the channel masking", os);

    ostringstream oss;

    string ofname("frame_20.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( "cbc i2c VCTH.vcth write operation failed.", WT_ERR_I2C); 

    int nwords(14*2);
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;

    /*
    unsigned N(500);
    unsigned n = N/2;
    uint32_t *rawdata(0);
    rawdata = new uint32_t[N];
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    */

    vector<uint32_t> fscbuf_data;
    for(int a=31; a<64;a++){
	unsigned value = 0x81100000 | (a << 8) | 0xff;
	fscbuf_data.push_back(value);
    }
    fscbuf_data.push_back((unsigned) 0x22000000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    //fscbuf_data.push_back((unsigned) 0xa0260000);
    //fscbuf_data.push_back((unsigned) 0xa1000000);
    for(int a=31; a<64;a++){
	unsigned value = 0x81100000 | (a << 8);
	fscbuf_data.push_back(value);
    }
    fscbuf_data.push_back((unsigned) 0x22000000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->WriteBeBoardConfig("cs_ctrl.ds1.frame_counter_reset", 1);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    int read_nwords(0); 
    cbc3be->MinNumWordToWait(nwords);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords ){
	oss.str("");
	oss << nwords << " words requested and " << read_nwords << " read.";
	throw WaferTestException( "Data read failed." + oss.str(), WT_ERR_DAQ ); 
    }
    be_data_decoder.SetData( buffer, nwords * 4 );
    int ei(0);
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    int nhit = cbc_dpd->HitChannels().size();
	    if(( ei==0 && nhit != 254) || ( ei==1 && nhit != 0 )){
	       result = 1; 
	       cbc_dpd->DumpData(os);
	    }
	}
	ei++;
    }
    delete buffer;
    return result;
}

int generate_stubs(int ibend, map<CHANNEL_ID, unsigned> &msks, int max_cw, int pt_width, int nss, int nsw, int ls, 
	vector<int> &addr0, vector<int> &bend0, vector<int> &v_max_cw, vector<int> &v_nss, vector<int> &v_nsw, vector<int> &v_ibend, Cbc3BeFc7 *cbc3be, ostream &os, int debug=0 )
{
    int ni0(43);
    int nm(3);

    char tmp[256];
    sprintf(tmp, "(nss, nsw, ls, max_cw)=(%d, %d, %d, %d)", nss, nsw, ls, max_cw);
    os << tmp << endl;

    vector<uint32_t> fscbuf_data;
    int nevt(0);

    bool bend_half = (nss+nsw)%2;

    //seed loop
    for(int i0 = 0; i0 < ni0; i0++){

	if(debug) cout << "i0, ibend = " << std::dec << i0 << ", " << ibend << endl;

	//single event check 

	//making 3 stubs at most with masking channels.

	//create unmasked channel maps
	//initialization of the channel maps from the previous test.
	//unmasked channels are masked. already masked channels are erased.
	map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	while(it_msks != msks.end()){
	    //	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	    if(it_msks->second != 0) {
		it_msks->second = 0;
		it_msks++;
	    }
	    else {
		msks.erase(it_msks++); 
	    }
	}
	for( int im = 0; im < nm; im++){

	    //get channels for the stub index (i,j)
	    int i = i0 + ni0 * im;

	    unsigned saddr = get_seed_address(i,nss);
	    if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

	    vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls);
	    if(!chs.size()) continue;

	    //update the masks
	    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
		msks[chs.at(ch_i)] = 1;
	    }

	    //calculate addr0 -- the created hits may not pass the cluster width condition.
	    if(nss <= max_cw && nsw <= max_cw){

		if(!(bend_half && (ibend == 7 || ibend == -8) ) ){//2 stubs at the edge of bend limits are not created in CBC3. 
//		if(true){

		    unsigned saddr = get_seed_address(i,nss);
		    unsigned sbend = ibend & 0xf; 
		    if( pt_width != 0 ){
			if(bend_half && (ibend >= pt_width/2 + pt_width%2 || ibend <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
			if(!bend_half && abs(ibend) >= pt_width/2 + 1 ) sbend = 8;
		    }

		    addr0.push_back(saddr);
		    bend0.push_back(sbend);
		    v_max_cw.push_back(max_cw);
		    v_nss.push_back(nss);
		    v_nsw.push_back(nsw);
		    v_ibend.push_back(ibend);

		    if(debug){
			cout << " stub (im,addr,bend)=(" << std::dec << im << ", " << saddr << "," << sbend << ") ch[";
			for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
			    cout << chs.at(ch_i) << ",";
			}
			cout << "] " << endl;
		    }
		}
	    }

	}
	if(debug) cout << "Total # of stubs = " << addr0.size() << endl;

	mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

	//test position
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
	fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	fscbuf_data.push_back((unsigned) 0xa0bb0000);
	fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	fscbuf_data.push_back((unsigned) 0xa0100000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	nevt++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    if(debug) cout << "size of seq data = " << fscbuf_data.size() << endl;

    if(debug>=2) dump_seq_data(fscbuf_data, os);

    cbc3be->WriteFscBram(fscbuf_data);
    //cbc3be->InitializeRun();
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    //cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
    /*
       system( "cbc3daq 'ipbus read node=cs_stat.*cbc1.*'");
       system( "cbc3daq 'ipbus read node=cs_stat.*eb.*'");
       system( "cbc3daq 'ipbus read node=cs_stat.*db.*'");
       */
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug);
    return nevt;
}
int generate_stubs(int nss, int nsw, int ibend, int ls, int max_cw, int pt_width, bool l1_supp, bool stub_supp, map<CHANNEL_ID, unsigned> &msks, 
	vector<vector<int> > &addr0, vector<vector<int> > &bend0, vector<set<int> > &ch0, 
	Cbc3BeFc7 *cbc3be, ostream &os, int debug=0 )
{
    int ni0(43);
    int nm(3);

    char tmp[256];
    if(debug){
	sprintf(tmp, "(nss, nsw, ibend, ls, max_cw, pt_width)=(%d, %d, %d, %d, %d, %d)", nss, nsw, ibend, ls, max_cw, pt_width);
	os << tmp << endl;
    }

    vector<uint32_t> fscbuf_data;
    int nevt(0);

    bool bend_half = (nss+nsw)%2;


    //seed loop
    for(int i0 = 0; i0 < ni0; i0++){

	vector<int> evt_addr0;
	vector<int> evt_bend0;
	set<int> evt_ch0;

	map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	while(it_msks != msks.end()){
	    //	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	    if(it_msks->second != 0) {
		it_msks->second = 0;
		it_msks++;
	    }
	    else {
		msks.erase(it_msks++); 
	    }
	}
	for( int im = 0; im < nm; im++){
	    //get channels for the stub index (i,j)
	    int i = i0 + ni0 * im;

	    vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls);
	    if(!chs.size()) continue;
	    if(!l1_supp) evt_ch0.insert(chs.begin(), chs.end());

	    //update the masks
	    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
		msks[chs.at(ch_i)] = 1;
	    }
	    if(!stub_supp){
		//the created hits may not pass the cluster width condition.
		if(nss <= max_cw && nsw <= max_cw){
		    if(!(bend_half && (ibend == 7 || ibend == -8) ) ){//2 stubs at the edge of bend limits are not created in CBC3. 

			unsigned saddr = get_seed_address(i,nss);
			unsigned sbend = ibend & 0xf; 
			if( pt_width != 0 ){
			    if(bend_half && (ibend >= pt_width/2 + pt_width%2 || ibend <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
			    if(!bend_half && abs(ibend) >= pt_width/2 + 1 ) sbend = 8;
			}
			evt_addr0.push_back(saddr);
			evt_bend0.push_back(sbend);
		    }
		}
	    }
	}
	addr0.push_back(evt_addr0);
	bend0.push_back(evt_bend0);
	ch0.push_back(evt_ch0);

	mk_seqdata_channel_mask(msks, fscbuf_data, 0 );
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
	fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	fscbuf_data.push_back((unsigned) 0xa0bb0000);
	fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	fscbuf_data.push_back((unsigned) 0xa0100000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	nevt++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    if(debug) os << "size of seq data = " << fscbuf_data.size() << endl;

    if(debug>=2) dump_seq_data(fscbuf_data, os);

    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug);
    return nevt;
}
bool read_data_and_check_stubs( string test_name, Cbc3BeFc7 *cbc3be, int nevt, 
	vector<int> &addr0, vector<int> &bend0, vector<int> &v_max_cw, vector<int> &v_nss, vector<int> &v_nsw, vector<int> &v_ibend,
      TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend, ostream &os, int debug = 0 )
{
    bool stub_ok(true);

    char tmp[256];

    if(debug >= 2){
	unsigned N = cbc3be->ReadBeBoardRegister("cs_cnfg.rdb.write_block_size"); 
	uint32_t *rawdata(0);
	rawdata = new uint32_t[N];
	cbc3be->ReadRawData( rawdata, N, true );
	dump_raw_data(rawdata, N, os);
	delete rawdata;
    }

    unsigned nwords = 14 * nevt;
    //if(debug)cout << "nwords = " << nwords << endl;
    cout << "nwords = " << nwords << endl;
    uint32_t *buffer = new uint32_t[nwords];
    read_data_nwords( cbc3be, buffer, nwords, os, debug );

    BeDataDecoder be_data_decoder;
    be_data_decoder.SetData( buffer, nwords * 4 );

    int n_stub(0);
    int stub_i(0);

    while( be_data_decoder.SetNextEvent() ){ 

	if(addr0.size() != 0 && stub_i >= addr0.size()) {
	    stub_ok = false;
	    break;
	}

	while( be_data_decoder.SetNextCbc() ){

	    if(addr0.size() != 0 && stub_i >= addr0.size()) {
		stub_ok = false;
		break;
	    }
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    if(debug)cbc_dpd->DumpData(os);
	    vector<unsigned> sp = cbc_dpd->StubPositions();
	    vector<unsigned> sb = cbc_dpd->StubBends();

	    for(unsigned s_i=0; s_i < 3; s_i++){

		if(sp.at(s_i) == 0) continue;
		n_stub++;
		if(addr0.size() == 0){
		    stub_ok = false;
		    os << "stub found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			<< " for stubs should be discriminated." << endl;
		    break;
		}
		if( stub_i >= addr0.size() ){
		    stub_ok = false;
		    break;
		}

		h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
		h_addr->Fill( addr0.at(stub_i), sp.at(s_i));

		// stub address on CBC3 is not listed as bad.
		bool this_addr_ok(true);

		//if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		//check the stub. fill bend hist if address is o.k.
		if(sp.at(s_i) != addr0.at(stub_i)){
		    stub_ok = false;
		    this_addr_ok = false;
		    sprintf( tmp,  "(nss, nsw, ibend, max_cw)=(%d, %d, %d, %d) ", v_nss.at(stub_i), v_nsw.at(stub_i), v_ibend.at(stub_i), v_max_cw.at(stub_i));  
		    os << tmp << "stub found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			<< " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		}
		if(sb.at(s_i) != bend0.at(stub_i)){
		    stub_ok = false;
		    if( this_addr_ok == false ) cout << "\t";
		    sprintf( tmp,  "(nss, nsw, ibend, max_cw)=(%d, %d, %d, %d) ", v_nss.at(stub_i), v_nsw.at(stub_i), v_ibend.at(stub_i), v_max_cw.at(stub_i));  
		    os << tmp << "stub found with a wrong bend addr=0x" 
			<< std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			<< " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		}
		if(this_addr_ok)h_bend->Fill( bend0.at(stub_i), sb.at(s_i) );
		stub_i++;
	    }
	}
    }
    sprintf( tmp, "Total # of stubs created = %d", n_stub );
    print_test( test_name, tmp, os );

    delete buffer;
    return stub_ok;
}
bool read_data_and_check_stubs_and_hits( string test_name, int err_code, 
	bool stop_at_fail, bool stub_addr_check, bool stub_bend_check, bool hit_check, 
	Cbc3BeFc7 *cbc3be, int nevt, 
	vector<vector<int> > &addr0, vector<vector<int> > &bend0, vector<set<int> > &ch0,
      TH2F *h_addr_bend, TH2F *h_addr, TH2F *h_bend, ostream &os, int debug = 0 )
{
    bool ok(true);
    bool stub_ok(true);
    bool hit_ok(true);

    char tmp[256];
    stringstream oss;

    if(debug >= 2){
	unsigned N = cbc3be->ReadBeBoardRegister("cs_cnfg.rdb.write_block_size"); 
	uint32_t *rawdata(0);
	rawdata = new uint32_t[N];
	cbc3be->ReadRawData( rawdata, N, true );
	dump_raw_data(rawdata, N, os);
	delete rawdata;
    }

    unsigned nwords = 14 * nevt;
    //if(debug)cout << "nwords = " << nwords << endl;
    cout << "nwords = " << nwords << endl;
    uint32_t *buffer = new uint32_t[nwords];
    read_data_nwords( cbc3be, buffer, nwords, os, debug );

    BeDataDecoder be_data_decoder;
    be_data_decoder.SetData( buffer, nwords * 4 );

    int total_hits(0);
    int stub_i(0);
    int evt_i(0);
    while( be_data_decoder.SetNextEvent() ){ 

	if(debug) os << "Event[" << evt_i << "]" << endl;

	vector<int> evt_addr0 = addr0.at(evt_i);
	vector<int> evt_bend0 = bend0.at(evt_i);
	set<int> evt_ch0 = ch0.at(evt_i);
	int evt_hit_i(0);
	while( be_data_decoder.SetNextCbc() ){

	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    if(debug)cbc_dpd->DumpData(os);

	    if(hit_check){
		vector<unsigned> hits = cbc_dpd->HitChannels(); 
		evt_hit_i = hits.size();
		if(hits.size() != evt_ch0.size() ){
		    hit_ok = false;
		    sprintf(tmp, "EVT[%04d] Unexpected # of hits created. %d hits are created where %d hits are expected.", evt_i, hits.size(), evt_ch0.size() );
		    cbc_dpd->DumpData(os);
		    if(stop_at_fail)
			throw WaferTestException( test_name + " " + tmp, err_code ); 
		    else
			print_test( test_name, tmp, os );
		}
		else{
		    for(unsigned h_i=0; h_i < hits.size(); h_i++){
			if(evt_ch0.find(hits.at(h_i))==evt_ch0.end()){
			    hit_ok = false;
			    sprintf(tmp, "EVT[%04d] Unexpected hit is found at channel %d.", hits.at(h_i) );
			    cbc_dpd->DumpData(os);
			    if(stop_at_fail)
				throw WaferTestException( test_name + " " + tmp, err_code ); 
			    else
				print_test( test_name, tmp, os );
			}
		    }
		}
	    }
	    if( !stub_addr_check && !stub_bend_check ) continue;

	    //if(debug) os << "stub check" << endl;

	    vector<unsigned> sp = cbc_dpd->StubPositions();
	    vector<unsigned> sb = cbc_dpd->StubBends();

	    int evt_nstub(0);
	    for(unsigned s_i=0; s_i < 3; s_i++){
		//os << "stub[" << s_i << "]" << endl;
		if(sp.at(s_i) == 0){
		    if(evt_addr0.size() > s_i){
			stub_ok = false;
			sprintf(tmp, "EVT[%04d] Stub at addr=0x%02x bend=0x%02x is missing.", evt_i, evt_addr0.at(s_i), evt_bend0.at(s_i) );
			cbc_dpd->DumpData(os);
			if(stop_at_fail)
			    throw WaferTestException( test_name + " " + tmp, err_code ); 
			else
			    print_test( test_name, tmp, os );
		    }
		}
		else{
		    stub_i++;
		    if( s_i+1 > evt_addr0.size() ){
			stub_ok = false;
			sprintf(tmp, "EVT[%04d] Unexpected stub is found at addr=0x%02x bend=0x%02x is missing.", evt_i, sp.at(s_i), sb.at(s_i) );
			cbc_dpd->DumpData(os);
			if(stop_at_fail)
			    throw WaferTestException( test_name + " " + tmp, err_code ); 
			else
			    print_test( test_name, tmp, os );
		    }
		    if(h_addr_bend) h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
		    if(h_addr) h_addr->Fill( evt_addr0.at(s_i), sp.at(s_i));

		    if(wt::waddrs.find(evt_addr0.at(s_i)) != wt::waddrs.end() ) continue;

		    //check the stub. fill bend hist if address is o.k.
		    bool this_addr_ok(true);
		    if( stub_addr_check && sp.at(s_i) != evt_addr0.at(s_i)){
			stub_ok = false;
			this_addr_ok = false;
			sprintf(tmp, "EVT[%04d] Stub is found at a wrong place addr=0x%02x bend=0x%02x for addr=0x%02x bend=0x%02x.", 
				evt_i, sp.at(s_i), sb.at(s_i), evt_addr0.at(s_i), evt_bend0.at(s_i) );
			cbc_dpd->DumpData(os);
			if(stop_at_fail)
			    throw WaferTestException( test_name + " " + tmp, err_code ); 
			else
			    print_test( test_name, tmp, os );
		    }
		    if( stub_bend_check && sb.at(s_i) != evt_bend0.at(s_i)){
			stub_ok = false;
			oss.str("");
			sprintf(tmp, "EVT[%04d] Stub is found at a wrong bend bend=0x%02x for addr=0x%02x bend=0x%02x.", 
				evt_i, sb.at(s_i), evt_addr0.at(s_i), evt_bend0.at(s_i) );
			cbc_dpd->DumpData(os);
			if(stop_at_fail)
			    throw WaferTestException( test_name + " " + tmp, err_code ); 
			else
			    print_test( test_name, tmp, os );
		    }
		    if(this_addr_ok && h_bend) h_bend->Fill( evt_bend0.at(s_i), sb.at(s_i) );
		}
	    }
	}
	evt_i++;
	total_hits += evt_hit_i;
    }
    delete buffer;

    if(debug && hit_check){
	sprintf( tmp, "Total # of hits created = %d", total_hits );
	print_test( test_name, tmp, os );
    }
    if(debug && (stub_addr_check || stub_bend_check) ){
	sprintf( tmp, "Total # of stubs created = %d", stub_i );
	print_test( test_name, tmp, os );
    }

    ok = hit_ok && stub_ok; 

    return ok;
}
int frame_21( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    if( wt_str_configs.find("config_file") != wt_str_configs.end() ){
	cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 
    }

    bool stub_ok(true);

    TFile *froot = new TFile( "frame21.root", "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    unsigned N(512);
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

    cbc3be->InitializeRun();
    int nevt(0);

    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;

    vector<int> v_max_cw;
    vector<int> v_nss;
    vector<int> v_nsw;
    vector<int> v_ibend;
    vector<int> addr0;
    vector<int> bend0;

    int max_cw(4);
    int pt_width(0);
    int ls(0);

    char tmp[256];

    for( int nss = 1; nss < 5; nss ++){

	for( int nsw = 1; nsw < 5; nsw ++){

	    if(nss != 1 && nsw != 1) continue;

	    bool bend_half = (nss+nsw)%2;

	    for(int ibend = -7; ibend < 8; ibend ++){ 

		if(bend_half && ibend == 7) continue;

		nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 
	    }
	}
    }
    sprintf( tmp, "Total # of stubs to be = %d", addr0.size() );
    print_test( test_name, tmp, os );

    stub_ok &= read_data_and_check_stubs(test_name, cbc3be, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend, os, debug);

    h_addr_bend->Write();
    h_addr->Write();
    h_bend->Write();
    froot->Close();

    //check the result.
    if(!stub_ok){
	print_test( test_name, " stub test failed.", os );
    }
    return !stub_ok;


}
int frame_22( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    if( wt_str_configs.find("config_file") != wt_str_configs.end() ){
	cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 
    }

    bool stub_ok(true);

    TFile *froot = new TFile( "frame22.root", "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    unsigned N(512);
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

    cbc3be->InitializeRun();
    int nevt(0);

    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;

    vector<int> v_max_cw;
    vector<int> v_nss;
    vector<int> v_nsw;
    vector<int> v_ibend;
    vector<int> addr0;
    vector<int> bend0;

    int pt_width(0);
    int ls(0);

    char tmp[256];

    for(int max_cw = 4; max_cw > 0; max_cw--){

	unsigned value = (unsigned)max_cw;
	cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.width", value, 1 );

	int nss = max_cw + 1;
	int nsw = 1;
	bool bend_half = (nss+nsw)%2;
	int ibend = 0;
	if(bend_half) ibend = -1;
	nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 

	nss = 1; 
	nsw = max_cw + 1;
	ibend = 0;
	nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 
    }
    //layer swap test.  I do not do non swap test, since this is done already in frame21
    int max_cw(4), nss(1), nsw(1);
    ls = 1;
    unsigned value = max_cw; 
    cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.width", value, 1 );
    value = ls;
    cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.lswap", value, 1 );
    int ibend = 0;
    nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 

    nss=2; 
    ibend = -1;
    nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 

    sprintf( tmp, "Total # of stubs to be created = %d", addr0.size() );
    print_test( test_name, tmp, os );

    stub_ok &= read_data_and_check_stubs(test_name, cbc3be, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend, os, debug);

    h_addr_bend->Write();
    h_addr->Write();
    h_bend->Write();
    froot->Close();

    value = 0;
    cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.lswap", value, 1 );

    //check the result.
    if(!stub_ok){
	print_test( test_name, " stub test failed.", os );
    }
    return !stub_ok;
}
int frame_23( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    if( wt_str_configs.find("config_file") != wt_str_configs.end() ){
	cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 
    }

    bool stub_ok(true);

    TFile *froot = new TFile( "frame23.root", "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    unsigned N(512);
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

    cbc3be->InitializeRun();
    int nevt(0);

    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;

    vector<int> v_max_cw;
    vector<int> v_nss;
    vector<int> v_nsw;
    vector<int> v_ibend;
    vector<int> addr0;
    vector<int> bend0;

    int ls(0), max_cw(4);

    char tmp[256];

    for(int pt_width=1; pt_width < 14; pt_width++){

	unsigned value = (unsigned) pt_width;
	cbc3be->rwCbcI2cRegsByName( 1, 1, "PlStLogicSelAndPtWidth.pt_width", value, 1 );

	for(int nss = 1; nss < 3; nss++){

	    for(int nsw = 1; nsw < 3; nsw++){

		bool bend_half = (nss+nsw)%2;

		int ibend(0);
		if(bend_half) ibend = pt_width / 2 + pt_width%2;
		else ibend = pt_width / 2 + 1;

		nevt+=generate_stubs(ibend, msks, max_cw, pt_width, nss, nsw, ls, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, cbc3be, os, debug); 
	    }
	}
    }
    sprintf( tmp, "Total # of stubs to be created = %d", addr0.size() );
    print_test( test_name, tmp, os );

    stub_ok &= read_data_and_check_stubs(test_name, cbc3be, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend, os, debug);

    h_addr_bend->Write();
    h_addr->Write();
    h_bend->Write();
    froot->Close();

    //check the result.
    if(!stub_ok){
	print_test( test_name, " stub test failed.", os );
    }
    return !stub_ok;
}
int stub_scan_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    string stub_test_type = wt_str_configs["stub_test_type"];
    print_test(test_name, string("stub address and bend check : ") + stub_test_type, os);
    string rootfile_name = wt_str_configs["rootfile_name"];

    if( wt_str_configs.find("config_file") != wt_str_configs.end() ){
	cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( "cbc i2c configuration failed.", WT_ERR_I2C); 
    }
    if( wt_int_configs.find("LayerSwapAndClusterWidth.width")!= wt_int_configs.end() ){
	unsigned value = wt_int_configs["LayerSwapAndClusterWidth.width"];
	if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.width", value, cbc_id ) )
	    throw WaferTestException( "cbc i2c LayerSwapAndClusterWidth.width write operation failed.", WT_ERR_I2C ); 
    }
    if( wt_int_configs.find("LayerSwapAndClusterWidth.lswap")!= wt_int_configs.end() ){
	unsigned value = wt_int_configs["LayerSwapAndClusterWidth.lswap"];
	if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "LayerSwapAndClusterWidth.lswap", value, cbc_id ) )
	    throw WaferTestException( "cbc i2c LayerSwapAndClusterWidth.lswap write operation failed.", WT_ERR_I2C ); 
    }
    int pt_width(0);
    if( wt_int_configs.find("PlStLogicSelAndPtWidth.pt_width")!= wt_int_configs.end() ){
	unsigned value = wt_int_configs["PlStLogicSelAndPtWidth.pt_width"];
	if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "PlStLogicSelAndPtWidth.pt_width", value, cbc_id ) )
	    throw WaferTestException( "cbc i2c write operation failed.", WT_ERR_I2C ); 
	pt_width = value;
    }

    int nss = atoi(stub_test_type.substr( 0, 1 ).c_str());
    int nsw = atoi(stub_test_type.substr( 4, 1 ).c_str());
    int cw  = atoi(stub_test_type.substr( 8, 1 ).c_str());
    int ls  = atoi(stub_test_type.substr( 12, 1 ).c_str());
    int the_ibend  = atoi(stub_test_type.substr( 16, 4 ).c_str());

    bool bend_half = (nss+nsw)%2;
    if(debug) cout << "the_ibend, pt_width, bend_half = " << the_ibend << ", " << pt_width <<  ", " << bend_half << endl;

    bool noStub(false);
    if( nss > cw || nsw > cw ) noStub = true;

    ostringstream oss;


    TFile *froot = new TFile(rootfile_name.c_str(), "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 


    bool stub_ok(true);
    int nevt(0);
    vector<int> addr0;
    vector<int> bend0;

    vector<uint32_t> fscbuf_data;

    BeDataDecoder be_data_decoder;

    unsigned N(512);
    unsigned n = N/2;
    uint32_t *rawdata(0);
    rawdata = new uint32_t[N];
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", n); 
    //    usleep(1);
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

    cbc3be->InitializeRun();

    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;


    int ni0(43);
    int nm(3);

    int min_ibend(-7), max_ibend(7); 
    if(the_ibend == 511){
	if(bend_half) max_ibend = 6;
    }
    else{
	min_ibend = the_ibend;
	max_ibend = the_ibend;
    }

    //seed loop
    for(int i0 = 0; i0 < ni0; i0++){

	//bend loop 
	for(int ibend=min_ibend; ibend <= max_ibend; ibend++){

	    if(debug) cout << "i0, ibend = " << std::dec << i0 << ", " << ibend << endl;


	    //single event check 

	    //making 3 stubs at most with masking channels.
	    //initialization of stub_id in the event
	    /*
	    int stub_id[nm];
	    for( int im = 0; im < nm; im++)stub_id[im] = -1;
	    */

	    //create unmasked channel maps
	    //initialization of the channel maps from the previous test.
	    //unmasked channels are masked. already masked channels are erased.
	    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
	    while(it_msks != msks.end()){
		//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
		if(it_msks->second != 0) {
		    it_msks->second = 0;
		    it_msks++;
		}
		else {
		    msks.erase(it_msks++); 
		}
	    }
	    /*
	       cout << "msks.size() " << msks.size() << endl;
	       it_msks = msks.begin();
	       for(; it_msks != msks.end(); it_msks++){
	       cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	       }
	       */
	    int nth_stub(0);
	    for( int im = 0; im < nm; im++){

		//get channels for the stub index (i,j)
		int i = i0 + ni0 * im;

		unsigned saddr = get_seed_address(i,nss);
		if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls);
		if(!chs.size()) continue;

		//update the masks
		for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		    if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
		    msks[chs.at(ch_i)] = 1;
		}

		//calculate addr0 -- the created hits may not pass the cluster width condition.
		if(nss <= cw && nsw <= cw){

		    if(!(bend_half && (ibend == 7 || ibend == -8) ) ){ 

			unsigned saddr = get_seed_address(i,nss);
			unsigned sbend = ibend & 0xf; 
			if( pt_width != 0 ){
			    if(bend_half && (ibend >= pt_width/2 + pt_width%2 || ibend <= (pt_width/2 + pt_width%2 + 1) * -1 ) ) sbend = 8;
			    if(!bend_half && abs(ibend) >= pt_width/2 + 1 ) sbend = 8;
			}

			addr0.push_back(saddr);
			bend0.push_back(sbend);

			if(debug){
			    cout << " stub (im,addr,bend)=(" << std::dec << im << ", " << saddr << "," << sbend << ") ch[";
			    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
				cout << chs.at(ch_i) << ",";
			    }
			    cout << "] " << endl;
			}
		    }
		}

	    }
	    if(debug) cout << "Total # of stubs = " << addr0.size() << endl;

	    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

	    //test position
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
	    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	    fscbuf_data.push_back((unsigned) 0xa0bb0000);
	    fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
	    fscbuf_data.push_back((unsigned) 0xa0100000);
	    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	    nevt++;
	}
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    if(debug) cout << "size of seq data = " << fscbuf_data.size() << endl;

    if(debug>=2) dump_seq_data(fscbuf_data, os);

    cbc3be->WriteFscBram(fscbuf_data);
    //cbc3be->InitializeRun();
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );
    /*
       system( "cbc3daq 'ipbus read node=cs_stat.*cbc1.*'");
       system( "cbc3daq 'ipbus read node=cs_stat.*eb.*'");
       system( "cbc3daq 'ipbus read node=cs_stat.*db.*'");
       */

    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    if(debug >= 2){
	cbc3be->ReadRawData( rawdata, N, true );
	dump_raw_data(rawdata, N, os);
    }

    unsigned nwords = 14 * nevt;
    //if(debug)cout << "nwords = " << nwords << endl;
    cout << "nwords = " << nwords << endl;
    uint32_t *buffer = new uint32_t[nwords];
    read_data_nwords( cbc3be, buffer, nwords, os, debug );

    be_data_decoder.SetData( buffer, nwords * 4 );

    int stub_i(0);

    while( be_data_decoder.SetNextEvent() ){ 

	if(addr0.size() != 0 && stub_i >= addr0.size()) {
	    stub_ok = false;
	    break;
	}

	while( be_data_decoder.SetNextCbc() ){

	    if(addr0.size() != 0 && stub_i >= addr0.size()) {
		stub_ok = false;
		break;
	    }
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    if(debug)cbc_dpd->DumpData(os);
	    vector<unsigned> sp = cbc_dpd->StubPositions();
	    vector<unsigned> sb = cbc_dpd->StubBends();

	    for(unsigned s_i=0; s_i < 3; s_i++){

		if(sp.at(s_i) == 0) continue;
		if(addr0.size() == 0 || stub_i >= addr0.size() ){
		    stub_ok = false;
		    break;
		}

		h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
		h_addr->Fill( addr0.at(stub_i), sp.at(s_i));

		// stub address on CBC3 is not listed as bad.
		bool this_addr_ok(true);
//		if(wt::waddrs.find(addr0.at(stub_i)) != wt::waddrs.end() )continue;

		if(sp.at(s_i) != addr0.at(stub_i)){
		    stub_ok = false;
		    this_addr_ok = false;
		    cout << "stub found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			<< " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		}
		if(sb.at(s_i) != bend0.at(stub_i)){
		    stub_ok = false;
		    if( this_addr_ok == false ) cout << "\t";
		    cout << "stub found with a wrong bend addr=0x" 
			<< std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) 
			<< " for addr=0x" << addr0.at(stub_i) << " bend=0x" << bend0.at(stub_i) << endl;
		}
		if(this_addr_ok)h_bend->Fill( bend0.at(stub_i), sb.at(s_i) );

		stub_i++;
	    }
	}
    }
    delete buffer;
    delete rawdata;

    h_addr_bend->Write();
    h_addr->Write();
    h_bend->Write();
    froot->Close();

    char tmp[256];
    sprintf( tmp, "Total # of stubs tested = %d", stub_i);
    print_test( test_name, tmp, os );

    //check the result.
    if(!stub_ok){
	print_test( test_name, " stub test failed.", os );
    }
    return !stub_ok;
}

int stub_cwofst_scan_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 


    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );
    }
    if( wt_str_configs.find("cbci2c_file_ofst") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file_ofst"], cbc_id );
    }

    TFile *froot = new TFile( "frame24.root", "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    bool stub_ok(true);
    stringstream oss;

    unsigned N(512);
    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 

    cbc3be->InitializeRun();
    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;

    int ni0(43);
    int nm(3);
    //seed loop

    unsigned cwo[12] = {1,2,3,4,5,6,15,14,13,12,11,10};//window offset
    unsigned bnd[12] = {15,14,13,12,11,10,1,2,3,4,5,6};
    unsigned ibnd[12] = {-1,-2,-3,-4,-5,-6,1,2,3,4,5,6};

    int max_cw(4); 
    vector<int> v_max_cw;
    vector<int> v_nss;
    vector<int> v_nsw;
    vector<int> v_ibend;
    vector<int> addr0;
    vector<int> bend0;

    int nevt(0);
    for(int cwo_i = 0; cwo_i < 12; cwo_i++){

	//create sequencer data
	vector<uint32_t> fscbuf_data;
	mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

	unsigned cworeg_value = (cwo[cwo_i] & 0xf) | ((cwo[cwo_i] & 0xf ) << 4); 
	unsigned value = 0x81100000 | (0x14 << 8) | cworeg_value;
	fscbuf_data.push_back(value);
	value = 0x81100000 | (0x13 << 8) | cworeg_value;
	fscbuf_data.push_back(value);

	for(int nss = 1; nss < 3; nss++){

	    if(debug)cout << "NSS = " << nss << endl;

	    for(int i0 = 0; i0 < ni0; i0++){

		/*
		int stub_id[nm];
		for( int im = 0; im < nm; im++)stub_id[im] = -1;
		*/

		map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
		while(it_msks != msks.end()){
		    //	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
		    if(it_msks->second != 0) {
			it_msks->second = 0;
			it_msks++;
		    }
		    else {
			msks.erase(it_msks++); 
		    }
		}

		int nth_stub(0);
		for( int im = 0; im < nm; im++){

		    int nss_even = !(nss%2);
		    unsigned saddr = (i0 + im * ni0+1)*2 + nss_even;
		    if(wt::waddrs.find(saddr) != wt::waddrs.end() ) continue;

		    vector<int> chs; 
		    //get channels for the stub
		    for(int sch_i = 0; sch_i < nss; sch_i++){
			int ch = (i0 + ni0 * im + sch_i)*2 + 1;
			if(ch < 1 || ch > 254) break;
			chs.push_back(ch);
		    }
		    if(chs.size()!=nss) continue;

		    for(int wch_i = 0; wch_i < nss; wch_i++){
			int ch = (i0 + ni0 * im + wch_i)*2 + 2;
			if(ch < 1 || ch > 254) break;
			chs.push_back(ch);
		    }
		    if(chs.size()!=(2*nss)) continue;

		    //update the masks
		    for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
			msks[chs.at(ch_i)] = 1;
		    }
		    addr0.push_back(saddr);
		    bend0.push_back(bnd[cwo_i]);
		    v_max_cw.push_back(max_cw);
		    v_nss.push_back(nss);
		    v_nsw.push_back(nss);
		    v_ibend.push_back(ibnd[cwo_i]);
		    //stub_id[nth_stub++] = im;
		}
		if(debug) cout << "Total # of stubs = " << nth_stub << endl;

		mk_seqdata_channel_mask(msks, fscbuf_data, 0 );

		//test position
		fscbuf_data.push_back((unsigned) 0x22000000);
		//fscbuf_data.push_back((unsigned) 0xa0010200);//rdb record trigger
		fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
		fscbuf_data.push_back((unsigned) 0xa0bb0000);
		fscbuf_data.push_back((unsigned) 0xa0010001);//orbit reset
		fscbuf_data.push_back((unsigned) 0xa0100000);
		fscbuf_data.push_back((unsigned) 0xa0010100);//T1
		nevt++;
	    }
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
//	cout << "size of sequencer data = " << fscbuf_data.size() << endl;

	if(debug>=2) dump_seq_data(fscbuf_data, os);

	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
//	cbc3be->WriteBeBoardConfig( "cs_ctrl.global.daq_reset", 1 );

	cbc3be->StartFscBramController();
	wait_seq_ready(cbc3be);
	check_cbci2c_reply_fifo(hw, os, 1, debug);
    }

    char tmp[256];
    sprintf( tmp, "Total # of stubs to be created = %d", addr0.size() );
    print_test( test_name, tmp, os );

    stub_ok &= read_data_and_check_stubs(test_name, cbc3be, nevt, addr0, bend0, v_max_cw, v_nss, v_nsw, v_ibend, h_addr_bend, h_addr, h_bend, os, debug);


    //check the result.
    if(!stub_ok){
	print_test( test_name, " stub test failed.", os );
    }
    return !stub_ok;
}
bool find_hits( std::vector<unsigned> &sp, std::vector<unsigned> &hits, 
	std::vector<int> &addr0, std::vector<int> &bend0,
	bool find_hit=true){

    bool stub_ok(true);
    bool l1_data_ok(true);

    int stub_i(0);
    int hits_i(0);
    int ns(0);
    for(unsigned s_i=0; s_i < 3; s_i++){

	if(!find_hit){
	    if(s_i==0){ 
		if(sp.at(s_i) != 0) stub_ok = false;
		if(hits.size() != 0) l1_data_ok = false;
		break;
	    }
	}
	else{
	    if(sp.at(s_i) == 0) continue;
	    if(stub_i > addr0.size()) {
		stub_ok = false;
		break;
	    }
	    if(sp.at(s_i) != addr0.at(stub_i)){
		stub_ok = false;
	    }
	    if(hits.size() < hits_i+1){
		l1_data_ok = false;
	    }
	    else{
		if(hits.at(hits_i++) != addr0.at(stub_i) - 1) l1_data_ok = false;
		if(hits.at(hits_i++) != addr0.at(stub_i) ) l1_data_ok = false;
	    }
	    ns++;
	    stub_i++;
	}
    }
}
int frame_25( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;

    bool stub_hit_ok(true);

    TFile *froot = new TFile( "frame25.root", "recreate");
    TH2F *h_addr_bend = new TH2F( "h_addr_bend", "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    TH2F *h_addr = new TH2F( "h_addr", "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    TH2F *h_bend = new TH2F( "h_bend", "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    int hip_reg_addr(12);
    int hip_reg_val(0);

    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );
    }
    CBCI2C_TYPE_DATA type_data;
    CBCI2C_ITEM_DATA_MAP items;



    int nevt(0);
    int nevt_prev(0);
    vector<vector<int> > addr0;
    vector<vector<int> > bend0;
    vector<set<int> > ch0;

    vector<uint32_t> fscbuf_data;
    BeDataDecoder be_data_decoder;

    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;

    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );
    cbc3be->InitializeRun();

    //bool stop_at_fail = (debug == 0) ? true : false;
    bool stop_at_fail(false);
    bool stub_addr_check(true);
    bool stub_bend_check(false);
    bool hit_check(true);

    int ibend(0);
    int max_cw(4);
    int pt_width(0);
    int nss(1);
    int nsw(1);
    int ls(0);

    //HIP supp. enabled and source is or, 
    //but pipe and stub logic inputs are sampled (not HIP supp. logic output. HIP suppression logic is not selected).
    items["hip_count"] = 3;
    items["hip_supp"]  = 1;
    items["hip_src"]   = 1;
    type_data.first = "HipAndTestMode"; 
    type_data.second= items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id );
    items.clear();
    items["pl_sel"] = 0;
    items["st_sel"] = 0;
    type_data.first = "PlStLogicSelAndPtWidth";
    type_data.second = items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id);

    cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, 0, cbc_id);
    CBCI2C_REGID reg_id = get_regid(1,cbc_id,1,hip_reg_addr);
    hip_reg_val = get_val(cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
    cout << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;
    bool l1_supp(false);
    bool stub_supp(false);

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'SAMPLED'. Hits and stubs should be created.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;

    items.clear();
    items["pl_sel"] = 3;
    items["st_sel"] = 3;
    type_data.first = "PlStLogicSelAndPtWidth";
    type_data.second = items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id);
    l1_supp = true;
    stub_supp = true;

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'FIXED PULSE WIDTH'. No hit, no stub.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;

    items.clear();
    items["pl_sel"] = 1;
    items["st_sel"] = 1;
    type_data.first = "PlStLogicSelAndPtWidth";
    type_data.second = items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id);
    l1_supp = false;
    stub_supp = false;

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'OR'. Hits and stubs should be created.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;

    items.clear();
    items["pl_sel"] = 2;
    items["st_sel"] = 2;
    type_data.first = "PlStLogicSelAndPtWidth";
    type_data.second = items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id);
    l1_supp = false;
    stub_supp = false;

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='OR', count=3). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. Hits and stubs should be created.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;

    items.clear();
    items["hip_src"] = 0;
    type_data.first = "HipAndTestMode"; 
    type_data.second= items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id );

    cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, 0, cbc_id);
    reg_id = get_regid(1,cbc_id,1,hip_reg_addr);
    hip_reg_val = get_val(cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
    cout << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;

    l1_supp = false;
    stub_supp = false;

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='SAMPLED', count=3). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. Hits and stubs should be created.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;

    items.clear();
    items["hip_count"] = 0;
    type_data.first = "HipAndTestMode"; 
    type_data.second= items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id );

    cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, 0, cbc_id);
    reg_id = get_regid(1,cbc_id,1,hip_reg_addr);
    hip_reg_val = get_val(cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
    cout << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;

    l1_supp = true;
    stub_supp = true;

    nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
    sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic disabled (source='SAMPLED', count=0). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. No hit, no stub.", nevt_prev, nevt-1 );
    print_test( test_name, tmp, os );
    nevt_prev = nevt;


    items.clear();
    items["hip_supp"]  = 0;
    type_data.first = "HipAndTestMode"; 
    type_data.second= items;
    cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id );

    l1_supp = true;
    stub_supp = true;
    for(int hip_count = 0; hip_count < 8; hip_count ++ ){
	items.clear();
	items["hip_count"] = hip_count;
	type_data.first = "HipAndTestMode"; 
	type_data.second= items;
	cbc3be->rwCbcI2cRegsByType( 1, 1, type_data, cbc_id );

	cbc3be->rwCbcI2cReg(1, 0, 1, hip_reg_addr, 0, cbc_id);
	reg_id = get_regid(1,cbc_id,1,hip_reg_addr);
	hip_reg_val = get_val(cbc3be->GetCbcI2cRegReplies().find(reg_id)->second); 
	cout << "hip reg addr val " << hip_reg_addr << " " << hip_reg_val << endl;


	nevt+=generate_stubs(nss, nsw, ibend, ls, max_cw, pt_width, l1_supp, stub_supp, msks, addr0, bend0, ch0, cbc3be, os, debug); 
	sprintf( tmp, "EVT[%04d-%04d] HIP supp. logic enabled (source='SAMPLED', count=%d). Pipe. & Stub logic inputs are both 'HIP supp. logic output'. No hit, no stub.", nevt_prev, nevt-1, hip_count );
	print_test( test_name, tmp, os );
	nevt_prev = nevt;
    }

    stub_hit_ok &= read_data_and_check_stubs_and_hits(test_name, WT_ERR_HDHIP, stop_at_fail, stub_addr_check, stub_bend_check, hit_check,
	    cbc3be, nevt, addr0, bend0, ch0, h_addr_bend, h_addr, h_bend, os, debug);

    h_addr_bend->Write();
    h_addr->Write();
    h_bend->Write();
    froot->Close();

    //check the result.
    if(!stub_hit_ok){
	print_test( test_name, "HIT detect HIP suppression test failed.", os );
    }
    return !stub_hit_ok;
}
int frame_26( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;

    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );
    }

    bool stub_hit_ok(true);

    bool stop_at_fail = (debug == 0 ) ? true : false;
    bool stub_addr_check = true;
    bool stub_bend_check = true;
    bool hit_check = true;
    int nevt = 1;

    vector<uint32_t> fscbuf_data;

    vector<vector<int> > addr0;
    vector<vector<int> > bend0;
    vector<set<int> >    ch0;

    vector<int> evt_addr0;
    vector<int> evt_bend0;
    set<int>    evt_ch0;

    //make three stubs
    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;
    int i0(3);
    int ni0(43);
    int nm(3);
    int nss(1);
    int nsw(1);
    int ibend(0);
    int ls(0);
    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
    while(it_msks != msks.end()){
	//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	if(it_msks->second != 0) {
	    it_msks->second = 0;
	    it_msks++;
	}
	else {
	    msks.erase(it_msks++); 
	}
    }
    for( int im = 0; im < nm; im++){
	//get channels for the stub index (i,j)
	int i = i0 + ni0 * im;

	unsigned saddr = get_seed_address(i,nss);
	unsigned sbend = ibend & 0xf; 

	vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls);
	if(!chs.size()) continue;
	evt_ch0.insert(chs.begin(), chs.end());

	//update the masks
	for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
	    if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
	    msks[chs.at(ch_i)] = 1;
	}
	evt_addr0.push_back(saddr);
	evt_bend0.push_back(sbend);
    }
    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );
    fscbuf_data.push_back((unsigned) 0x22000000);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    addr0.push_back(evt_addr0);
    bend0.push_back(evt_bend0);
    ch0.push_back(evt_ch0);

    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug);

    int bitslip = cbc3be->ReadBeBoardRegister( "cs_stat.io.cbc_data.timing_tuning.fe1.bitslip_counter" );

    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", 512); 

    unsigned fci[3] = { 1, 3, 0 };
    int delay[3] = { 1, 2, 0 };
    int prev_bitslip(0);
    for(int fci_i = 0; fci_i < 3; fci_i++){
	prev_bitslip = bitslip;
	cbc3be->rwCbcI2cRegsByName(1,1,"FciAndError.fci_delay", fci[fci_i]);
	cbc3be->TuneDataClockTiming();
	cbc3be->InitializeRun();

	bitslip = cbc3be->ReadBeBoardRegister( "cs_stat.io.cbc_data.timing_tuning.fe1.bitslip_counter" );
	sprintf(tmp, "FCI %d, BITSLIP COUNTER = %d", fci[fci_i], bitslip); 
	print_test(test_name, tmp, os);

	if(fci_i == 0 && bitslip - prev_bitslip != 1 ){
	    sprintf(tmp, "data timing is not expected. bitslip counter changed from %d to %d where %d 320MHz clock delay is expected.", prev_bitslip, bitslip, delay[fci_i] );
	    throw( test_name + " " + tmp, WT_ERR_FCI ); 
	}
	fscbuf_data.clear();
	fscbuf_data.push_back((unsigned) 0xa0011200);//fast reset & rdb recording
	fscbuf_data.push_back((unsigned) 0xa0300000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	fscbuf_data.push_back((unsigned) 0xe0000000);

	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

	cbc3be->StartFscBramController();
	wait_seq_ready(cbc3be);

	stub_hit_ok &= read_data_and_check_stubs_and_hits(test_name, WT_ERR_FCI, stop_at_fail, stub_addr_check, stub_bend_check, hit_check,
		cbc3be, nevt, addr0, bend0, ch0, 0, 0, 0, os, debug);
    }
    
    return !stub_hit_ok;

}
int frame_27( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;
    unsigned value(0);

    cbc3be->CbcReset();
    usleep(100000);
    cbc3be->CbcI2cBusInit();
    if( wt_str_configs.find("cbci2c_offset_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_offset_file"], cbc_id );
	cout << "Offsets are written" << endl;
    }
    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id );
    }
    usleep(100000);

    if( wt_int_configs.find("TestPulsePotentiometer.pot") != wt_int_configs.end() ){
	unsigned value = wt_int_configs["TestPulsePotentiometer.pot"];
	cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePotentiometer.pot", value, cbc_id );
    }
    /*
    unsigned value = 1;
    cbc3be->rwCbcI2cRegsByName(1,1,"TestPulseDelayGroup.group", value, cbc_id);
    */
    unsigned vcth(0);
    if( wt_int_configs.find("vcth") != wt_int_configs.end() ){
	vcth = wt_int_configs["vcth"];
    }
    unsigned vcth_lsb = vcth & 0xff;
    unsigned vcth_msb = (vcth & 0x200) >> 8;

    vector<uint32_t> fscbuf_data;
    unsigned N(512);
    unsigned n = N/2;
    uint32_t *rawdata(0);
    int *slvs6(0);
    rawdata = new uint32_t[N];
    slvs6 = new int[n];
    int hp(0), last_hp(0);
    int sp(0), last_sp(0);

    cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.write_block_size", N); 
    cbc3be->InitializeRun();
   // cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

    //-----------------------
    //make three stubs
    //-----------------------
    vector<vector<int> > addr0;
    vector<vector<int> > bend0;
    vector<set<int> >    ch0;

    vector<int> evt_addr0;
    vector<int> evt_bend0;
    set<int>    evt_ch0;
    map<CHANNEL_ID, unsigned> msks;
    for(unsigned i=1; i<255; i++) msks[i] = 0xff;
    int i0(0);
    int ni0(16);
    int nm(3);
    int nss(1);
    int nsw(1);
    int ibend(0);
    int ls(0);
    map<CHANNEL_ID,unsigned>::iterator it_msks = msks.begin();
    while(it_msks != msks.end()){
	//	cout << "f,s=" << it_msks->first << "," << it_msks->second << endl;
	if(it_msks->second != 0) {
	    it_msks->second = 0;
	    it_msks++;
	}
	else {
	    msks.erase(it_msks++); 
	}
    }
    for( int im = 0; im < nm; im++){
	//get channels for the stub index (i,j)
	int i = i0 + ni0 * im;

	unsigned saddr = get_seed_address(i,nss);
	unsigned sbend = ibend & 0xf; 

	vector<int> chs = get_stub_channels(i,ibend,nss,nsw,ls);
	if(!chs.size()) continue;
	evt_ch0.insert(chs.begin(), chs.end());

	//update the masks
	for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
	    cout << "ch = " << chs.at(ch_i) << endl;
	    if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
	    msks[chs.at(ch_i)] = 1;
	}
	evt_addr0.push_back(saddr);
	evt_bend0.push_back(sbend);
    }
    mk_seqdata_channel_mask(msks, fscbuf_data, 0 );
    fscbuf_data.push_back((unsigned) 0x22000000);
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    check_cbci2c_reply_fifo(hw, os, 1, debug );


    cout << "dll loop" << endl;
    unsigned dll_reg_val[26] = { 128, 144, 136, 152, 132, 148, 140, 156, 130, 146, 138, 154, 134, 150, 142, 158, 129, 145, 137, 153, 133, 149, 141, 157, 131, 147 }; 
    value = 0;
    for(int i = 0; i < 26; i++){

	/*
	value=0;
	cbc3be->rwCbcI2cRegsByName( 0, 1, "TestPulsePolEnAMux.tp_en", value, 1 ); 
	value=1;
	cbc3be->rwCbcI2cRegsByName( 0, 1, "TestPulsePolEnAMux.tp_en", value, 1 ); 
	*/
	fscbuf_data.clear();
	value = 0x81100000 | (28 << 8) | dll_reg_val[i];
	fscbuf_data.push_back(value);
	/*
	value = 0x81100000 | (0x0f << 8) | 49;
	fscbuf_data.push_back(value);
	value = 0x81100000 | (0x0f << 8) | 113;
	fscbuf_data.push_back(value);
	*/
	//If AMUX is selected, stub lines are noisy.
	value = 0x81100000 | (0x0f << 8) | 32;
	fscbuf_data.push_back(value);
	value = 0x81100000 | (0x0f << 8) | 96;//AMUX is not selected.
	fscbuf_data.push_back(value);
	value = 0x81100000 | (0x4f << 8) | vcth_lsb;
	fscbuf_data.push_back(value);
	value = 0x81100000 | (0x50 << 8) | vcth_msb;
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0xe0000000);

	dump_seq_data(fscbuf_data, os);
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

	cbc3be->StartFscBramController();
	wait_seq_ready(cbc3be);
	check_cbci2c_reply_fifo(hw, os, 1, debug);

	usleep(100000);

	fscbuf_data.clear();
	fscbuf_data.push_back((unsigned) 0xa0011200);//fast reset & rdb recording
	fscbuf_data.push_back((unsigned) 0xa0300000);
	fscbuf_data.push_back((unsigned) 0xa0010010);//test pulse request
	fscbuf_data.push_back((unsigned) 0xa0220000);
	fscbuf_data.push_back((unsigned) 0xa0010100);//T1
	fscbuf_data.push_back((unsigned) 0xe0000000);

	cbc3be->InitializeRun();
	cbc3be->WriteFscBram(fscbuf_data);
//	cbc3be->WriteBeBoardConfig( "cs_ctrl.rdb.reset", 1 );

	cbc3be->StartFscBramController();
	wait_seq_ready(cbc3be);
//	check_cbci2c_reply_fifo(hw, os);

	int read_nwords = cbc3be->ReadRawData( rawdata, N );
	int dll = swap_bits(( dll_reg_val[i] & 0x1f ), 5);
	sprintf(tmp, "--DLL (reg, dll) = 0x%02x, %d)", dll_reg_val[i], dll);
	cout << tmp << endl;
	if(debug){
	    dump_raw_data(rawdata, N, os);
	}
	get_slvs6(rawdata, slvs6, n);
	last_hp = hp;
	hp = find_data_frame_header_position( slvs6, n );
	cout << "HP " << hp << endl;
	if( last_hp != 0 && hp != last_hp){
	    oss.str("");
	    oss << "L1 triggerd data frame header position changed at " << std::dec << "(reg val = " << dll_reg_val[i] << ")" << dll << " from " << last_hp << " to " << hp;
	    print_test(test_name, oss.str(), os);
	} 
	vector<int> saddr;
	vector<int> sbend;
	last_sp = sp;
	sp = find_stubs( rawdata, n, saddr, sbend );
	cout << "SP " << sp << endl;
	if( last_sp != 0 && sp != last_sp ){
	    oss.str("");
	    oss << "Stub position changed at " << std::dec << "(reg val = " << dll_reg_val[i] << ")" << dll << " from " << last_sp << " to " << sp;
	    print_test(test_name, oss.str(), os);
	}
    }
    delete(rawdata);

    int result(0);
    return result;
}
int frame_28( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;

    if( wt_str_configs.find("cbci2c_offset_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_offset_file"], cbc_id );
    }
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
    usleep( 2000 );

    usbtmc_printf(ut_handle,":TRIGger:COUNt 1024");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usleep(60000);

    vector<uint32_t> fscbuf_data;
    unsigned value(0);

    unsigned amux_reg_addr = 15; 
    unsigned reg_value(0);
    //amux setting 5, VCTH
    reg_value = 37;
    value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
    fscbuf_data.push_back(value);
    int ntrig(0);
    for( unsigned vcth_h=0; vcth_h < 2; vcth_h++){
	for( unsigned vcth_l=0; vcth_l < 512; vcth_l++){
	    mk_seqdata_write_vcth((vcth_h<<9) + vcth_l, fscbuf_data);
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	int timeout_sec = 20;
	wait_seq_ready(cbc3be, timeout_sec);
	fscbuf_data.clear();
    }
    //wait keithley read
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("%s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == ntrig) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    int nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("Voltage values for VCTH 0 - 1023 : %s\n", (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);

    //----------------------------------------
    //Voltage measurement for VPLUS scan
    //----------------------------------------
    usbtmc_printf(ut_handle,":TRIGger:COUNt 16");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usleep(50000);

    fscbuf_data.clear();
    //VCTH is set to 0
    mk_seqdata_write_vcth(0, fscbuf_data);
    //amux setting 17, VPLUS 
    reg_value = 49; 
    value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
    fscbuf_data.push_back(value);
    ntrig = 0;
    unsigned vplus_reg_addr = 11;
    for(unsigned vplus=0; vplus < 16; vplus++){
	value = 0x81100000 | (vplus_reg_addr << 8) | ( ( 0x7 << 4) | (reg_value & 0xF) );
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	fscbuf_data.push_back((unsigned) 0x28000000);
	ntrig++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    fscbuf_data.clear();
    //wait keithley read
    gettimeofday(&t0,0);
    points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("# of data in KMM data buffer = %s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == ntrig) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("Voltage values for VPLUS 0 - 15 : %s\n", (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);

    //----------------------------------------
    //Voltage measurement for VPLUS2 scan
    //----------------------------------------
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usleep(50000);

    fscbuf_data.clear();
    //amux setting 16, VPLUS2 
    reg_value = 48; 
    value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
    fscbuf_data.push_back(value);
    ntrig = 0;
    for(unsigned vplus=0; vplus < 16; vplus++){
	value = 0x81100000 | (vplus_reg_addr << 8) | ( ((reg_value & 0xf) << 4) | 0x7);
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	fscbuf_data.push_back((unsigned) 0x28000000);
	ntrig++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be);
    fscbuf_data.clear();
    //wait keithley read
    gettimeofday(&t0,0);
    points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("# of data in KMM data buffer = %s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == ntrig) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("Voltage values for VPLUS2 0 - 15 : %s\n", (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);

    //----------------------------------------
    //Voltage measurement for CAL_VCASC scan
    //----------------------------------------
    usbtmc_printf(ut_handle,":TRIGger:COUNt 256");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usleep(50000);

    fscbuf_data.clear();
    //VPLUS is set to 0x77
    value = 0x81100000 | (vplus_reg_addr << 8) | 0x77;
    fscbuf_data.push_back(value);
    //amux setting 15, CAL_VCASC 
    reg_value = 47; 
    value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
    fscbuf_data.push_back(value);
    ntrig = 0;
    unsigned cal_vcasc_reg_addr = 17;
    for(unsigned cv=0; cv < 256; cv++){
	value = 0x81100000 | (cal_vcasc_reg_addr << 8) | cv;
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	fscbuf_data.push_back((unsigned) 0x28000000);
	ntrig++;
    }
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be, 5);
    fscbuf_data.clear();
    //wait keithley read
    gettimeofday(&t0,0);
    points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("# of data in KMM data buffer = %s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == ntrig) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    printf("Voltage values for CAL_VCASC 0 - 255 : %s\n", (char *)kmm_response_buf);
    if(debug) printf("response size = %d\n", nread);
    return 0;
}
int frame_29( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;

    string ofname("currents.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    if( wt_str_configs.find("cbci2c_offset_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_offset_file"], cbc_id );
    }
    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_VLDOI_I );
    usleep( 2000 );

    vector<uint32_t> fscbuf_data;
    unsigned value(0);

    //amux setting  
    unsigned amux_reg_addr = 15; 
    unsigned reg_value = 32; 
    value = 0x81100000 | (amux_reg_addr << 8) | reg_value;
    fscbuf_data.push_back(value);
    map<string,unsigned> bias_addr_map;
    bias_addr_map["IPRE1"] = 0x03;
    bias_addr_map["IPRE2"] = 0x04;
    bias_addr_map["IPSF"]  = 0x05;
    bias_addr_map["IPA"]   = 0x06;
    bias_addr_map["IPAOS"] = 0x07;
    bias_addr_map["ICOMP"] = 0x09;
    bias_addr_map["CAL_I"] = 0x10;

    usbtmc_printf(ut_handle,":SAMPle:COUNt 1");
    usbtmc_printf(ut_handle,":TRIGger:COUNt 364");
    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");
    usleep(50000);

    int ntrig = 0;
    map<string,unsigned>::iterator it = bias_addr_map.begin();
    for(; it != bias_addr_map.end(); it++){
	for(unsigned v=0; v < 256; v+=5){
	    value = 0x81100000 | (it->second << 8) | v;
	    fscbuf_data.push_back(value);
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0x00000001);
	    fscbuf_data.push_back((unsigned) 0x28000000);
	    ntrig++;
	}
	fscbuf_data.push_back((unsigned) 0xe0000000);
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	wait_seq_ready(cbc3be, 10);
	fscbuf_data.clear();
    }
    cout << ntrig << endl;
    //wait keithley read
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("# of data in KMM data buffer = %s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == ntrig) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
    }
    int nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    vector<float> meas;
    char *p;
    p = strtok((char *)kmm_response_buf, ",");
    while (p != 0){
	float fv = strtof(p,0);
	meas.push_back(fv * 10000);//mA
	p = strtok(0, ",");
    }
    it = bias_addr_map.begin();
    int i=0;
    ofile << "#TYPE I2CVAL CURRmA" << endl;
    for(; it != bias_addr_map.end(); it++){
	for(unsigned v=0; v < 256; v+=5){
	    sprintf(tmp, "%-10s %10d %10.1f", it->first.c_str(), v, meas.at(i++));
	    ofile << tmp << endl;
	}
    }
    return 0;
}
int frame_30( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    char tmp[256];
    stringstream oss;

    string ofname("amux.txt");
    if( wt_str_configs.find("output_file") != wt_str_configs.end() ) ofname = wt_str_configs["output_file"]; 
    ofstream ofile(ofname.c_str());

    if( wt_str_configs.find("cbci2c_offset_file") != wt_str_configs.end() ){
	cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_offset_file"], cbc_id );
    }

    cbc3be->WriteBeBoardConfig("relay1.relayA", RELAYA_AMUX );
    usleep( 2000 );
    usbtmc_printf(ut_handle,":SENSE:VOLT:NPLC 0.001");          //time to average the input for each measurement, in power line cycles
//    usbtmc_printf(ut_handle,":TRIGger:DELay:AUTO 0");  
    //int sample_count(10);
    int sample_count(10);
    int trig_count(17);
    sprintf(tmp, ":SAMPle:COUNt %d", sample_count);
    usbtmc_printf(ut_handle, tmp);
    sprintf(tmp, ":TRIGger:COUNt %d", trig_count);
    usbtmc_printf(ut_handle, tmp);
//    usbtmc_printf(ut_handle,":SENSE:ZERO:AUTO ONCE");        //autozero 
    usbtmc_printf(ut_handle,":INIT");	
    usleep(100000);

    vector<string> bias;
    bias.push_back("IPA");
    bias.push_back("IPRE2");
    bias.push_back("CAL_I");
    bias.push_back("IBIAS");
    bias.push_back("VCTH");
    bias.push_back("VBGBIAS");
    bias.push_back("VBG_LDO");
    bias.push_back("Vpafb");
    bias.push_back("NC50");
    bias.push_back("PRE1");
    bias.push_back("IPSF");
    bias.push_back("IPAOS");
    bias.push_back("ICOMP");
    bias.push_back("IHYST");
    bias.push_back("CAL_VCASC");
    bias.push_back("VPLUS2");
    bias.push_back("VPLUS1");

    vector<uint32_t> fscbuf_data;
    unsigned value(0);
    //vcth setting
    mk_seqdata_write_vcth(400, fscbuf_data);
    //amux setting  
    unsigned amux_reg_addr = 15; 
    int ntrig(0);
    for(int i=1; i < 18; i++){
	value = 0x81100000 | (amux_reg_addr << 8) | (32+i);
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	fscbuf_data.push_back((unsigned) 0x00000001);
	for(int i_s = 0; i_s < sample_count; i_s++){
	    fscbuf_data.push_back((unsigned) 0x28000000);
	}
	ntrig++;
    }
    if(debug) cout << "# of triggers = " << ntrig << endl;
    fscbuf_data.push_back((unsigned) 0xe0000000);
    if(debug>=2) dump_seq_data(fscbuf_data, os);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();
    wait_seq_ready(cbc3be, 5);
    fscbuf_data.clear();

    //wait keithley read
    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int points = 0;
    while(1){
	kmm_read(":DATA:POINts?", ut_handle, kmm_response_buf, KMM_BUFLEN );
	if(debug) printf("# of data in KMM data buffer = %s\n", kmm_response_buf);
	//printf("---------------------\n");
	points = (int)( strtof( (char *)kmm_response_buf, 0 ));

	if(points == trig_count * sample_count) break;

	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    oss.str("");
	    oss << points << ".";
	    throw WaferTestException( "KEITHLEY read does not complete. # of data points = " + oss.str(), WT_ERR_KMM );
	}
	usleep(1000);
    }
    int nread = kmm_read(":FETCH?", ut_handle, kmm_response_buf, KMM_BUFLEN);
    if(debug) cout << "kmm read values : " << kmm_response_buf << endl; 
    vector<float> meas;
    char *p;
    p = strtok((char *)kmm_response_buf, ",");
    while (p != 0){
	float fv = strtof(p,0);
	meas.push_back(fv);
	p = strtok(0, ",");
    }
    oss.str("");
    sprintf(tmp, "%-10s", "#TYPE");
    oss << tmp;
    for(int i_smpl=0; i_smpl < sample_count; i_smpl++){ 
	sprintf(tmp, " %10d", i_smpl); 
	oss << tmp;
    }
    sprintf(tmp, " %10s", "MEAN"); 
    oss << tmp;
    sprintf(tmp, " %10s", "TEST"); 
    oss << tmp;
    oss << endl;

    bool ok(true);
    int start_s(1);
    for(int i=0; i<bias.size(); i++){
	sprintf(tmp, "%-10s", bias.at(i).c_str());
	oss << tmp;
	float sum(0), mean(0), min(1.0),max(0.0);
	bool this_ok(true);
	for(int i_smpl=0; i_smpl < sample_count; i_smpl++){ 
	    float val = meas.at(i*sample_count + i_smpl);
	    sprintf(tmp, " %10.5f", val);
	    oss << tmp;
	    if(i_smpl<start_s) continue;
	    min = (val < min ) ? val : min;
	    max = (val > max ) ? val : max;
	    sum+=val;
	}
	mean = sum / (sample_count-start_s);
	if( min < mean * ( 1 - 0.001 ) || mean * ( 1 + 0.001 ) < max ) this_ok = false; 
	sprintf(tmp, " %10.5f", mean);
	oss << tmp;
	if(this_ok)
	    sprintf(tmp, " %10s", "passed"); 
	else{
	    sprintf(tmp, " %10s", "failed"); 
	    ok = false;
	}
	oss << tmp;

	oss << endl;
    }
    ofile << oss.str();
    return !ok; 
}



int main( int argc, char *argv[] )
{
    cout << wt::NC << wt::time_string() << "CBC3 Wafer testing program begins." <<  endl;

    stringstream oss;

    //setLogLevelTo( uhal::Fatal() );
    setLogLevelTo( uhal::Debug() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    cbc3hal_dir = env_value;

    string wt_config_file("wt_config.txt");
    string cbci2c_file;

//    string test_names("frame03,frame05,frame06,frame07,frame08,frame10,frame11,frame13_a,frame13_b,frame14,frame15,frame16,frame17,frame18,frame19,frame20,frame21,frame22,frame23,frame24,frame25,frame26,frame27,frame28,frame29,frame30");
    string test_names("frame03,frame05,frame07,frame08,frame11,frame13_a,frame13_b,frame16,frame17,frame18,frame19,frame20,frame21,frame22,frame23,frame24,frame25,frame26,frame27,frame28,frame29,frame30");
    bool test_all(true);
    map<string, bool> test_list;
    int dll(1);
    bool tune(false);
    int debug(0);

    string no_fuse("0"); 
    string chip_id("0");
    string target_bg("0.52");
    
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "debug"    , required_argument, 0, 'f' },
	    { "tune"      , no_argument, 0, 'g' },
	    { "noFuse"   , no_argument, 0, 'i' },
	    { "chipId"   , required_argument, 0, 'j' },
	    { "testFrames", required_argument, 0, 'k'},
	    { "wtConfig"   , required_argument, 0, 'm'},
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:ghij:k:l:m:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'f':
		debug = atoi(optarg);
		break;
	    case 'g':
		tune = true;
		break;
	    case 'i':
		no_fuse = "1"; 
		break;
	    case 'j':
		chip_id = optarg;
		break;
	    case 'k':
		test_names = optarg;
		break;
	    case 'm':
		wt_config_file = optarg;
		break;
	    default:
		abort();
	}
    }
    setLogLevelTo( uhal::Error() );

    int result(0);

    reply_fifo_ndata_sum = 0;

    try{
	/*
	   Initialization for CBC3 Backend interface
	   create ipbus connection and read the firmare build info. 
	   */
	Cbc3BeFc7   *cbc3be(0);
	string setting_file_dir( cbc3hal_dir + "/etc/wt/" );

	WaferTest *wt = new WaferTest(wt_config_file, setting_file_dir, cout, tune, debug);
	wt->set_test_setting("frame14", "no_fuse", no_fuse);
	wt->set_test_setting("frame14", "chip_id", chip_id);
	wt->set_test_setting("frame14", "target_bg", target_bg);

	cbc3be = wt->init_be(connection_file, id);
	hw = wt->hw();
	cbc_id = wt->cbc_id(); 
	//Initialization for KEITHLEY multimeter 
	ut_handle = wt->init_kmm(kmm_sern);


	result |= wt->run_test(test_names);


	/* variables for tests */
	/*
	string test_name;
	map<string,string> wt_str_configs;
	map<string,int>    wt_int_configs;
	struct timeval t0, t, dt;
	unsigned value(0);
	char ofname[256];
	char tmpstr[256];
	CBCI2C_ITEM_DATA_MAP item_data;


	double ped_mean(0), ped_mean_e(0), ped_sigma(0), ped_sigma_e(0);
	double tp_mean(0), tp_mean_e(0), tp_sigma(0), tp_sigma_e(0);



	if(test_names.find("frame27") != std::string::npos){
	    test_name = "FRAME[27    ]";

	    wt_int_configs.clear();
	    wt_str_configs.clear();
	    wt_str_configs["cbci2c_offset_file"] = offset_file; 
	    wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME21.txt";
	    wt_int_configs["TestPulsePotentiometer.pot"] = 60;
//	    wt_int_configs["vcth"] = int( 0.5 * (tp_mean + ped_mean) );
	    wt_int_configs["vcth"] = 540;
	    if( run_test(frame_27, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug ) ) result |= WT_ERR_FCI;
	    print_test(test_name, "END-----------------------------", cout);
	}

	*/
	//close links 
	usbtmc_close(ut_handle);
	wt->print_wafer_test();
	char tmp[256];
	sprintf(tmp, "# of I2C transactions = %d", reply_fifo_ndata_sum);
	print_test( "CBC I2C", tmp, cout ); 
	sprintf(tmp, "0x%08x", result );
	print_test( "RESULT", tmp, cout ); 
	return result;
    }
    catch( IpbusUtilsException &e ){
	cout << e.what() << endl;
	result |= WT_ERR_IPBUS;
	cout << wt::RED << setbase(16) << std::setw(8) << std::setfill('0') << result << endl;
	return result;
    }
    catch( CbcI2cRegIpbusException &e ){
	cout << e.what() << endl;
	result |= WT_ERR_I2C;
	cout << wt::RED << setbase(16) << std::setw(8) << std::setfill('0') << result << endl;
	return result;

    }
    catch( Cbc3BeException &e ){
	cout << e.what() << endl;
	result |= WT_ERR_CBC3BE;
	cout << wt::RED << setbase(16) << std::setw(8) << std::setfill('0') << result << endl;
	return result;
    }
    catch( WaferTestException &e ){
	cout << wt::RED << wt::time_string() << e.what() << endl;
	result |= e.ecode();
	cout << wt::RED << setbase(16) << std::setw(8) << std::setfill('0') << result << endl;
	return result;
    }
    cout << wt::NC << setbase(16) << std::setw(8) << std::setfill('0') << result << endl;
    return result;
}


