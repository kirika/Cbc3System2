#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Cbc3BeFc7.h"
#include "tmc.h"
#include "ut.h"
#include "Cbc3BeData.h"
#include "seq_utils.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

#define N_MAX_HIT 16
HwInterface *hw(0);

/*
 * modify here
 */
std::string connection_file("../etc/connections.xml");
std::string id("FC7_SINGLE_CBC3_64_CH");

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                           "        << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file]     "        << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]                  "        << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]               "        << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --readRawData [# of BX to save]  "        << ": to read rawdata after running the sequencer.\n"
	<< setw(indent) << " [sequencer conf file]            "        << ": sequencer configuration file.\n" 
	<< endl;
}

void dump_slvs_data( uint32_t *buffer, int n, std::ostream &os ){
    int slvs[6];
    os << "=================" <<endl;
    os << "SLVS DATA        " << endl;
    os << "-----------------" <<endl;
    os << "S1 S2 S3 S4 S5 S6" << endl;  
    os << "-----------------" <<endl;
    for(int i=0; i < n; i++){
	if(i%2==0){
	    slvs[0] = buffer[i] & 0x000000ff;
	    slvs[1] = (buffer[i] & 0x0000ff00) >> 8;
	    slvs[2] = (buffer[i] & 0x00ff0000) >> 16;
	    slvs[3] = (buffer[i] & 0xff000000) >> 24;
	}
	if(i%2==1){
	    slvs[4] = buffer[i] & 0x000000ff;
	    slvs[5] = (buffer[i] & 0x0000ff00) >> 8;

	    os << setbase(16) << setfill('0') << setw(2) 
		<< slvs[0] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[1] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[2] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[3] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[4] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[5] << ' '
		<< endl;
	}
    }
    os << "=================" <<endl;
}
void get_slvs5(const uint32_t *buffer, int *slvs5, int n ){

    for( int i=0; i < n; i++){
	slvs5[i] = (buffer[2*i+1] & 0x000000FF);
    }
}
void get_slvs6(const uint32_t *buffer, int *slvs6, int n ){

    for( int i=0; i < n; i++){
	slvs6[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}


int main( int argc, char *argv[] )
{

    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    bool CBC3_0(true);
    int debug(0);
    int nbx(1000);
    bool read_rawdata(false);
    string seq_cnfg_file;
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"       , no_argument,       0, 'a' },
	    { "conFile"    , required_argument, 0, 'b' },
	    { "boardId"    , required_argument, 0, 'c' },
	    { "debug"      , required_argument, 0, 'd' },
	    { "readRawData", required_argument, 0, 'e' }, 
	    { "CBC3_1"   , no_argument, 0, 'f' },
	    {0,0,0,0}
	};

	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		debug = strtol(optarg, 0, 0);
	       break;
	    case 'e':
	       read_rawdata = true;
	       nbx = strtol(optarg, 0, 0);
	       break;
	    case 'f':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }

    if( optind < argc ){
	    seq_cnfg_file = argv[optind++];
    }
    else{
	    printHelp(argv);	
	    return 0;
    }

    vector<uint32_t> seq_data;

    ifstream cfile(seq_cnfg_file.c_str());
    char line[256];

    if( cfile.is_open() ){
	cout << "=================" <<endl;
	cout << "Sequencer input file for seq_test" << endl;
	cout << "-----------------" <<endl;
	while( !cfile.eof() ){
	    cfile.getline( line, 256 );
	    cout << line << endl;
	    //|     |           cout << line << endl;
	    if( strlen(line ) == 0 || line[0] == '#' || line[0] == ' ' ){
		//|         if( w == 1 && line[0] == '#' ) cout << line << endl;
		continue;
	    }
	    istringstream iss( line );
	    string com, values_str;
	    iss >> com >> values_str;
//	    cout << com << endl;
	    if( com == "UNMASK_CH" ){


		map<CHANNEL_ID, unsigned> msks;
		for(int ch=1; ch < 255; ch++) msks[ch] = 0;
		vector<CHANNEL_ID> um_list;

		string list = rstr(values_str, ",", " ");
		stringstream values_ss( list );
		//cout << list << endl;
		string item;
		while( !values_ss.eof() ){
		    values_ss >> item;
		    um_list.push_back(atoi(item.c_str()));
		}
		for(unsigned i=0; i < um_list.size(); i++){
		    int c = um_list.at(i); 
		    msks[c] = 1;
		}
		mk_seqdata_channel_mask( msks, seq_data);
		seq_data.push_back((unsigned) 0x22000000);
	    }
	    if( com == "COM" ){
		unsigned uval = strtoul(values_str.c_str(), 0, 0);  
		seq_data.push_back(uval);
	    }
	}
	cfile.close();
	cout << "=================" <<endl;
    }
    else{
	ostringstream oss;
	oss << "File " << cfile << " could not be opened.";
    }
    seq_data.push_back((unsigned) 0xe0000000);
    dump_seq_data(seq_data, cout);

    Cbc3BeFc7 *cbc3be = new Cbc3BeFc7( CBC3_0, debug );
    cbc3be->Initialize( connection_file, id );
    ConnectionManager cm( string( "file://" ) + connection_file ); 
    hw = new HwInterface( cm.getDevice( id ) );

    uint32_t *rawdata(0);
    unsigned N = 2*nbx;
    unsigned n = N/2;
    if(read_rawdata){
	rawdata = new uint32_t[N];
	cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.cbc_id", 1 );
	cbc3be->WriteBeBoardConfig("cs_cnfg.rdb.latency", 0 );
	cbc3be->WriteBeBoardConfig( "cs_cnfg.rdb.write_block_size", N );
    }

    cbc3be->InitializeRun();
    cbc3be->WriteFscBram(seq_data);
    cbc3be->StartFscBramController();
    //cbc3be->WaitSequencerReady(1000);
    usleep(2000);
    check_cbci2c_reply_fifo(hw, cerr, 1, 1);

    if(read_rawdata){
	    int read_nwords = cbc3be->ReadRawData( rawdata, N );
	    if(read_nwords == -1 || read_nwords != (int)N) 
		    cerr << "Raw data buffer problem." << endl;

	    dump_slvs_data(rawdata,N-2, cout);//fix me later.
//	    raw_data_dump_data(rawdata, N, cout);
    }

    return 0;
}
