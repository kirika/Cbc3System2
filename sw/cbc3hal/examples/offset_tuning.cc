/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "Cbc3BeData.h"
#include <TH1F.h>
#include <TFile.h>
#include "analysis_utils.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --configBe  [be board configuration file]         "  << ": be board configuration with the file.\n" 
	<< setw(indent) << " --configCbc [cbc i2c reg. config. file list file] "  << ": cbc i2c register configuration with the file.\n" 
	<< setw(indent) << " --configRun [run configuration file]              "  << ": run configuration on the be board for tuning with the file.\n" 
	<< setw(indent) << " --outDir    [output directory]                    "  << ": output directory.\n"
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << " --debug     [debug flag]                          "  << ": debugging information is printed.\n"
	<< setw(indent) << " --group_type [value]                              "  << ": group type.\n"
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    string be_config_file = "";
    string cbci2creg_config_file = "";
    string config_file = "";
    string output_dir = "";
    int debug(0);
    bool CBC3_0(true);
    int group_type(8);

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "configBe" , required_argument, 0, 'd' },
	    { "configCbc", required_argument, 0, 'e' },
	    { "configRun", required_argument, 0, 'f' },
	    { "outDir"   , required_argument, 0, 'g' },
	    { "debug"    , required_argument, 0, 'h' },
	    { "group_type"    , required_argument, 0, 'i' },
	    { "CBC3_1"   , no_argument, 0, 'j' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h:i:j", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		be_config_file = optarg;
		break;
	    case 'e':
		cbci2creg_config_file = optarg;
		break;
	    case 'f':
		config_file = optarg;
		cout << "run config file : " << config_file << endl;
		break;
	    case 'g':
		output_dir  = optarg;
		break;
	    case 'h':
		debug = atoi(optarg);
		break;
	    case 'i':
		group_type = atoi(optarg);
		break;
	    case 'j':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }

    HwInterface *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    //create ipbus connection and read the node value
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7(CBC3_0, debug);
	cbc3be->Initialize( connection_file, id );
	read_ipbus( hw, "cs_stat.system.id" );
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	cout << "IPBUS is not ready" << endl;
	return -1;
    }
    const GROUP_CHANNELSET_MAP &group_map = Cbc3BeInterface::SetGroupChannelSetMap(group_type);

//    cbc3be->WriteCbcI2cRegsToDB();

    std::map<std::string,uint32_t> regmap;
    // system config
    try{
	if( be_config_file != "" ) cbc3be->ConfigureBeBoard( be_config_file );
	cbc3be->ReadBeBoardConfig( be_config_file, regmap );
    }
    catch( Cbc3BeException &e ){
	cerr << e.what() << endl;
	return -1;
    }

    // run config config
    if( config_file != "" ) cbc3be->WriteBeBoardConfig( config_file );
    cbc3be->ReadBeBoardConfig( config_file, regmap );

    // dump be configurations
    std::map<std::string,uint32_t>::iterator it = regmap.begin();
    for(; it != regmap.end(); it++ ) cout << it->first << " " << it->second << endl;
    int sample_count = regmap["cs_cnfg.fcm.fcg.Ncycle"];
    cout << "SAMPLE COUNT = " << sample_count << endl;


    // cbci2creg config
    if( cbci2creg_config_file != "" ) cbc3be->ConfigureCbcs( cbci2creg_config_file );

    cbc3be->ReadAllCbcConfig();
    for(unsigned cbcid=1; cbcid <= cbc3be->GetNCbcs(); cbcid++){
	if( cbc3be->IsCbcActive(cbcid) == 0 ) continue;
	print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbcid );
    }

    CBCI2C_ITEM_DATA_MAP item_data;
    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA type_data( "VCTH", item_data );
    std::map<unsigned, CBCI2C_TYPE_DATA> cbc_id_data_vcth0;
    cbc_id_data_vcth0[0] = type_data;
    cbc3be->rwCbcI2cRegsByType( 1, 0, cbc_id_data_vcth0 );

    //Finding pedestal VCTH
    map<CBC_ID, vector<unsigned> > offsets0_map;
    for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	vector<unsigned> offsets0(254,0x80);
	offsets0_map[cbc_id] = offsets0;
    }
    if( cbc3be->FindVcth( sample_count, 0.5, &offsets0_map ) < 0 ){
       return -1;
    }

    if(debug) cout << endl << "Tuning Offsets begins" << endl;
    //Tuning Offsets
    map<CBC_ID, map<CHANNEL_ID,unsigned> > all_offsets_map;

    map<CBC_ID, map<unsigned,unsigned> > masks;
    bool only_1cbc = (cbc3be->GetNActiveCbcs() == 1);
    if( !only_1cbc ){
	cout << "Reading CBC channel masks" << endl;
	for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	    if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	    cbc3be->ClearCbcI2cRegReplies();
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 0, 1, 1, addr, 0 };
		commands.push_back( com );
	    }
	    cbc3be->rwCbcI2cRegs(commands);  
	    const CBCI2C_REGS & regs = cbc3be->GetCbcI2cRegReplies(); 
	    CBCI2C_REGS::const_iterator it = regs.begin();
	    for(; it != regs.end(); it++){
		CBCI2C_REPLY reply = it->second;
		unsigned addr = get_addr(reply);
		if( get_rw_flag(reply) == 1 && get_page(reply) == 1 && 0x20 <= addr && addr < 0x40 ){ 
		    masks[cbc_id][addr] = get_val(reply);
		}
	    }
	}
	cout << "Masking all CBC channels" << endl;
	for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	    if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	    unsigned value = 0x00;
	    cbc3be->rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
    }

    for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){

	if( !cbc3be->IsCbcActive(cbc_id) ) continue;

	if( !only_1cbc ){
	    //Unmask this cbc channels
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    cbc3be->rwCbcI2cRegs(commands);  
	}
	//Group by group offset tuning
	GROUP_CHANNELSET_MAP::const_iterator it_group = group_map.begin();
	for(; it_group != group_map.end(); it_group++){

	    //Offset tuning with bitonic search
	    unsigned g = it_group->first;
	    map<CHANNEL_ID,unsigned> offsets_map;
	    if( cbc3be->TuneOffsets(cbc_id, g, sample_count, offsets_map) < 0 ) return -1;

	    //add the tuned values in the global offset map
	    all_offsets_map[cbc_id].insert(offsets_map.begin(), offsets_map.end());
	}
	if( !only_1cbc ){
	    //Mask this cbc channels
	    unsigned value=0x00;
	    cbc3be->rwCbcI2cRegsByName(1,1,"MaskChannels", value, cbc_id);  
	}
    }

    if( !only_1cbc ){
	for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	    if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	    vector<struct cbc_i2c_command> commands;
	    for( unsigned addr=0x20; addr < 0x40; addr++){
		struct cbc_i2c_command com = { cbc_id, 1, 1, 1, addr, masks[cbc_id][addr] };
		commands.push_back( com );
	    }
	    cbc3be->rwCbcI2cRegs(commands);  
	}
    }

    //saving the results and writing those tuned values to CBC.
    string name;
    char fname[256];
    char fname_root[256];
    sprintf(fname_root, "offset_tuning_be%d.root", cbc3be->GetBeId() );
    if( output_dir == "" ) name = fname_root;
    else name = output_dir + "/" + fname_root;
    TFile froot(name.c_str(), "recreate");
    for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){

	if( !cbc3be->IsCbcActive(cbc_id) ) continue;

	//offset histogram is filled.
	TH1F *h = new TH1F( Form("hoffsets_be%dcbc%02d",cbc3be->GetBeId(), cbc_id), "; offset; count", 256, -0.5, 255.5 );
	map<CHANNEL_ID,unsigned> the_map =  all_offsets_map[cbc_id];
	map<CHANNEL_ID,unsigned>::iterator it_the_map = the_map.begin();
	for(; it_the_map != the_map.end(); it_the_map++){
	    h->Fill(it_the_map->second);
	}
	cbc3be->rwCbcI2cRegsByType( 1, 1, cbc_id_data_vcth0[cbc_id] );
	//New configuration file is opened.
	sprintf(fname, "offset_tuning-be%dcbc%02d.knt", cbc3be->GetBeId(), cbc_id );
	if( output_dir == "" ) name = fname;
	else name = output_dir + "/" + fname;
	ofstream ofile(name.c_str());

	//Writing the tuned values to the registers
	if(debug) cout << "Write tuned offset values to the registers" << endl;
	cbc3be->rwCbcI2c_Offsets(1,1,all_offsets_map[cbc_id],cbc_id);

	//Writing the tuned configuration to a file.
	if(debug) cout << "Read all the registers on CBC " << cbc_id << " and write the tuned configuration to a file." << endl;
	cbc3be->ReadAllCbcConfig(cbc_id);
	CBCI2C_REGS regs = cbc3be->GetCbcI2cRegReplies();
	print_regcnf(regs, ofile, cbc_id);
	ofile.close();

	if(debug) cout << "The parameter tunings finished for CBC " << cbc_id << "." << endl;
    }
    froot.Write();
    froot.Close();

    if(debug) cout << "The parameter tunings finished for all CBCs." << endl;

    return 0;
}
