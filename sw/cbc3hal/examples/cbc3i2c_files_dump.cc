#include <getopt.h>
#include <iostream>
#include <iomanip>
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace cbc3hal;

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --to_name                                         "  << ": default output is with address. This option enable output with name.\n" 
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << "[file list separated by spaces ]                   "  << ": file list.\n" 
	<< endl;
}
int main( int argc, char *argv[] )
{
    bool to_name(false);
    bool CBC3_0(true);
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "to_name"   , no_argument, 0, 'b' },
	    { "CBC3_1"   , no_argument, 0, 'c' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}
	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		to_name = true;
		break;
	    case 'c':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }
    std::vector<string> files;
    int i(0);
    while( optind < argc ){
	files.push_back( argv[optind++] );
	//cout << "file " << ++i << " : " << files.back() << endl; 
    }
    if( to_name ){
	if(files.size() == 1) print_name_regcnf(files.at(0), cout);
	else print_name_regcnf( files, cout );
    }
    else{
	print_regcnf( files, cout, CBC3_0 );
    }
}
