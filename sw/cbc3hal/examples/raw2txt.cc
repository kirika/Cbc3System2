#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
//#include "fc7/MmcPipeInterface.hpp"
#include <TH1F.h>
#include <TPad.h>
#include <TStyle.h>
#include <TH2F.h>
#include <TFile.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/types.h>
#include <sys/wait.h>


using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");

// replace s2 to s3 in s1 and the result is returned
/*
std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}
*/

int main( int argc, char *argv[] )
{

    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    string cbc3hal_dir( env_value );
	
    string filename = argv[1];
    int vcth(-1);
    if(argc >= 3){
	vcth = strtol( argv[2], 0, 0);
    }
    bool fix_trig(false);
    if(argc >= 4){
	if( strcmp(argv[3], "fix_trig") == 0 ){
	    fix_trig = true;
	}
    }

    Cbc3BeFc7 *cbc3be(0);

    int debug = 0;
    bool CBC3_0(true);
    cbc3be = new Cbc3BeFc7( CBC3_0, debug );
    cbc3be->Initialize( connection_file, id );

    //	cbc3be->DumpData( filename, 1 );
    //int buf_size(131072); //131Kx4 bytes.  524KB
    int buf_size(1400*4); //131Kx4 bytes.  524KB
    char *cDataBuffer = new char[buf_size];
    int cFreeBufferSize(buf_size);
    int cDataSize(0);

    ifstream cDataFile( filename.c_str(),ifstream::binary);
    if( !cDataFile.is_open() ){
	    cerr << "Data file " << filename << " cannot be opened." << endl;
	    return -1;
    }
    string ofname = rstr( filename, "raw", "txt" );
    ofstream ofile( ofname.c_str() );
    cout << ofname << " is created." << endl;
    ofile << "# Title: CBC TEXT DATA" << endl;
    ofile << "# Fields: \"0:EVENT COUNT\" \"1:BE L1A COUNT\" \"2:TRIG COUNT\" \"3:UA9 COUNT\" \"4:UA9 TRIG\" \"5:OR254 0\" \"6:OR254 1\" \"7:TDC\"";
    ofile << " \"8:CBC ID\" \"9:ERROR\" \"10:PIPELINE ADDRESS\" \"11:L1A COUNT\"";
    ofile << " \"12:NSTUB\"";
    ofile << " \"13:STUB1 ADDR\" \"14:STUB1 BEND\"";
    ofile << " \"15:STUB2 ADDR\" \"16:STUB2 BEND\"";
    ofile << " \"17:STUB3 ADDR\" \"18:STUB3 BEND\"";
    ofile << " \"19:TRIG SOF\" \"20:TRIG OR254\" \"21:TRIG ERR\"  \"22:N HIT\" \"23:HIT INDEX\" \"24:CHANNEL ID\"";
    ofile << " \"25:VCTH\"";
    ofile << endl; 	
    ofile << "# FieldTypes: int int int int int int int int";
    ofile << " int int int int int"; 
    ofile << " int int int int int int"; 
    ofile << " int int int int int int int" << endl; 

    ostringstream condoss;
    condoss << vcth << " ";
    cout << "VCTH = " << vcth << endl;

    int or254_1(0);
    int or254_0(0);

    cDataFile.read( cDataBuffer, cFreeBufferSize );
    cDataSize = cDataFile.gcount();
    int evt_count(0);
	
    while( true ){
	    int cLeftBufferSize(0);
	    BeDataDecoder decoder;
//	    decoder.SetData( cDataBuffer, buf_size );
	    decoder.SetData( cDataBuffer, cDataSize );
	    while( decoder.SetNextEvent() ){
		    int ua9_count(-1), tdc(-1);
		    if(fix_trig){
			ua9_count = decoder.UA9_count_fix();
			tdc = decoder.TDC_fix();
		    }
		    else{
			ua9_count = decoder.UA9_count();
			tdc = decoder.TDC();
		    }
		    ostringstream evtoss;
		    evtoss << evt_count++ << " ";
		    evtoss << decoder.BeL1ACounter() << " ";
		    evtoss << decoder.TriggerCounter() << " ";
		    evtoss << ua9_count << " ";	
		    evtoss << (decoder.Triggers() >> 2 & 1 ) << " ";
		    evtoss << (decoder.Triggers() >> 1 & 1 ) << " ";
		    evtoss << (decoder.Triggers() & 1 ) << " ";
		    evtoss << tdc << " ";
		    while( decoder.SetNextCbc() ){
			    ostringstream cbcoss;
			    const CbcDataPacketDecoder *cdec = decoder.GetCbcDataPacketDecoder();
			    cbcoss << cdec->Id() << " "; 
			    int e1 = cdec->Error(1); 
			    int e0 = cdec->Error(0);
			    cbcoss << ((e1 << 1) & e0) << " ";
			    cbcoss << cdec->PipelineAddress() << " ";
			    cbcoss << cdec->L1ACounter() << " ";
			    std::vector<unsigned> hits = cdec->HitChannels();
			    int nstub(0);
			    std::vector<unsigned> stubs_addr = cdec->StubPositions();
			    std::vector<unsigned> stubs_bend = cdec->StubBends();
			    int saddr[3],sbend[3];
			    for(unsigned i=0; i < stubs_addr.size(); i++){
				    if(stubs_addr.at(i)!=0) nstub++;
				    saddr[i] = stubs_addr.at(i);
				    sbend[i] = stubs_bend.at(i);
			    }
			    cbcoss << nstub << " ";
			    cbcoss << saddr[0] << " " << sbend[0] << " ";
			    cbcoss << saddr[1] << " " << sbend[1] << " ";
			    cbcoss << saddr[2] << " " << sbend[2] << " ";
			    cbcoss << cdec->TrigSof() << " "; 
			    cbcoss << cdec->TrigOr254() << " "; 
			    cbcoss << cdec->TrigErr() << " "; 
			    cbcoss << hits.size() << " ";
			    if(!hits.size()) ofile << evtoss.str() << cbcoss.str() << 0 << " " << 0 << " " << condoss.str() << endl;
			    else{
				    for(unsigned i=0; i < hits.size(); i++){
					    ofile << evtoss.str() << cbcoss.str() << i << " " << hits.at(i) << " " << condoss.str() << endl;
				    }
			    }
		    }
	    }
	    decoder.GetIncompleteEventBuffer( cLeftBufferSize );

	    if( cLeftBufferSize ){
		    cout << "some data is left in the buffer at the event " << evt_count << endl; 
		    memcpy( cDataBuffer, &cDataBuffer[cDataSize - cLeftBufferSize], cLeftBufferSize );
	    }
	    cFreeBufferSize = buf_size - cLeftBufferSize;
	    if( !cDataFile.eof() ){
		    cDataFile.read( &cDataBuffer[cLeftBufferSize], cFreeBufferSize );
		    cDataSize = cLeftBufferSize + cDataFile.gcount();
		    if(cDataSize == 0) break;
	    }
	    else if( cLeftBufferSize ){
		    cerr << "Data is truncated. " << cLeftBufferSize << " left." << endl;
		    break;
	    }
	    else{
		    break;
	    }
    }
    cDataFile.close();
    ofile.close();
    cout << "Total number of events is " << std::dec << evt_count << endl; 
    delete cbc3be;
}

