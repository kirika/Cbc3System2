/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --configBe  [be board configuration file]         "  << ": be board configuration enable with the file.\n" 
	<< setw(indent) << " --configCbc [cbc i2c reg. config. file list file] "  << ": cbc i2c register configuration enable with the file.\n" 
	<< setw(indent) << " --config    [scan configuration file]             "  << ": be board configuration for scan enable with the file.\n" 
	<< setw(indent) << " --group     [test pulse group]                    "  << ": test pulse group. 0 for default. \n" 
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}

int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    string be_config_file = "";
    string cbci2creg_config_file = "";
    string config_file = "";
    string output_dir = "";
    int group(0);
    bool CBC3_0(true);
    int debug(0);

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "configBe" , required_argument, 0, 'd' },
	    { "configCbc", required_argument, 0, 'e' },
	    { "config"   , required_argument, 0, 'f' },
	    { "group"    , required_argument, 0, 'g' },
	    { "debug"    , required_argument, 0, 'h' },
	    { "CBC3_1"   , no_argument, 0, 'i' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h:i", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		be_config_file = optarg;
		break;
	    case 'e':
		cbci2creg_config_file = optarg;
		break;
	    case 'f':
		config_file = optarg;
		break;
	    case 'g':
		group = atoi(optarg); 
		break;
	    case 'h':
		debug = atoi(optarg);	
		break;
	    case 'i':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }

    HwInterface *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    int fci_delay(1);
    //create ipbus connection and read the node value
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7(CBC3_0, debug);
	cbc3be->Initialize( connection_file, id );
	read_ipbus( hw, "cs_stat.system.id" );
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	cout << "IPBUS is not ready" << endl;
	return -1;
    }

    std::map<std::string,uint32_t> regmap;

    // system config

    if( be_config_file != "" ) cbc3be->ConfigureBeBoard( be_config_file, fci_delay );
    cbc3be->ReadBeBoardConfig( config_file, regmap );

    // scan config
    if( config_file != "" ) cbc3be->WriteBeBoardConfig( config_file );
    cbc3be->ReadBeBoardConfig( config_file, regmap );

    std::map<std::string,uint32_t>::iterator it = regmap.begin();
//    for(; it != regmap.end(); it++ ) cout << it->first << " " << it->second << endl;

    // cbci2creg config
    if( cbci2creg_config_file != "" ){
       if( !cbc3be->ConfigureCbcs( cbci2creg_config_file ) ) return -1;
    }

    CBCI2C_ITEM_DATA_MAP item_data;

    item_data["fci_delay"] = fci_delay;
    if( !cbc3be->writeAllCbcI2cRegsByType("FciAndError", item_data) ) return -1;


    //trigger latency setting
    item_data["trig_lat"] = 200;
    if( !cbc3be->writeAllCbcI2cRegsByType("FcCntrlCompBetaTrigLat", item_data) ) return -1;
    cbc3be->SendFastSignal("reset");

    //test pulse settings
    int tp_pot = 72;
    item_data.clear();
    item_data["pot"] = tp_pot;
    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulsePotentiometer",item_data) ) return -1;
    cout << "[lat_trig_data_scan] Test pulse size " << std::dec << tp_pot << endl;

    int tp_delay(15);
    item_data.clear();
    item_data["delay"] = tp_delay;
    item_data["group"] = group;
    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) ) return -1;

    unsigned en(1), gnd(1);
    item_data.clear();
    item_data["tp_en"] = en; 
    item_data["tp_gnd"] = gnd; 
    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulsePolEnAMux",item_data) ) return -1;

    int vcth = 400;
    item_data.clear();
    item_data["vcth"] = vcth;
    if(!cbc3be->writeAllCbcI2cRegsByType("VCTH", item_data) ) return -1;

    int pl_sel = 3;
    int st_sel = 3;
    int pt_width = 3; 
    item_data.clear();
    item_data["pl_sel"] = pl_sel;
    item_data["st_sel"] = st_sel;
    item_data["pt_width"] = pt_width;
    if(!cbc3be->writeAllCbcI2cRegsByType("PlStLogicSelAndPtWidth", item_data) ) return -1;

    int cl_width = 4;
    item_data.clear();
    item_data["width"] = cl_width;
    if(!cbc3be->writeAllCbcI2cRegsByType("LayerSwapAndClusterWidth", item_data) ) return -1;

    int tpg = 1;
    int dll = 4;
    item_data.clear();
    item_data["tpg"] = tpg;
    item_data["dll"] = dll;
    if(!cbc3be->writeAllCbcI2cRegsByType("40MHzClockOr254DLL", item_data) ) return -1;

//    std::vector<CBCI2C_REPLY> replies = cbc3be->rwCbcI2c_reg(1,1,1, 0x12, 0x53, 0 );
//    dump_replies(replies, cout);
    
    uint32_t *data_buffer;
    data_buffer = new uint32_t[66000];

    //daq initialization
    cbc3be->MinNumWordToWait( 3 + 11 * cbc3be->GetNActiveCbcs() );
    cbc3be->DataReadyFlagPollingInterval_usec(1);
    cbc3be->InitializeRun();

    unsigned l1_count(0);
    char node_c[256];

    sprintf( node_c, "cs_stat.cbc_dp.cbc%d.l1a_counter", 1 );
    l1_count = read_ipbus( hw, node_c ); 
    cout << "[lat_trig_data_scan] l1a count = " << l1_count << endl;

    BeDataDecoder be_data_decoder;

    int ncbcs = cbc3be->GetNCbcs();


    sprintf( node_c, "cs_cnfg.cbc_dp.cbc%d.latencies.l1a", 1 );
    unsigned l1_lat = read_ipbus( hw, node_c ); 
    cout << "[lat_trig_data_scan] Starting the scan for l1a latency = " << l1_lat << endl;

    for( int i=0; i < 256; i++){
//	cout << "[" << i << "]" << endl;
	for( int cbc_id = 1; cbc_id <= ncbcs; cbc_id ++ ){
	    sprintf( node_c, "cs_cnfg.cbc_dp.cbc%d.latencies.trig_data", cbc_id );
	    write_ipbus( hw, node_c, i ); 
	}
	sprintf( node_c, "cs_stat.cbc_dp.cbc%d.l1a_counter", 1 );
	l1_count = read_ipbus( hw, node_c ); 
	cout << "[lat_trig_data_scan] just before starting fast signal generator l1a count = " << l1_count << endl;
	cbc3be->StartFastSignalGenerator();
	if( cbc3be->WaitData( 1, true ) ){

		sprintf( node_c, "cs_stat.cbc_dp.cbc%d.l1a_counter", 1 );
		l1_count = read_ipbus( hw, node_c ); 
		cout << "[lat_trig_data_scan] just before reading data l1a count = " << l1_count << endl;
	    int nwords = cbc3be->ReadData( data_buffer );
	    //cout << "# of words read = " << nwords << endl;
	    be_data_decoder.SetData( data_buffer, nwords * 4 );
	    while( be_data_decoder.SetNextEvent() ){ 

		int nstubs(0);
		while( be_data_decoder.SetNextCbc() ){

		    const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
		    //int cbc_id = cbc_data_packet_decoder->Id(); 
		    vector<unsigned> sps = cbc_data_packet_decoder->StubPositions();
		    for(unsigned sp_i=0; sp_i < sps.size();sp_i++){
			if( sps.at(sp_i) != 0 ) nstubs++; 
		    }
		    if(debug){
			    cout << "[lat_trig_data_scan] ==========================" << endl;
			    cout << "[lat_trig_data_scan] stub_data_latency = " << std::dec << i << endl;
			    cbc_data_packet_decoder->DumpData(cout);
		    }
		}

		if(nstubs){
		    cout << "[lat_trig_data_scan] +++++++++++++++++++++++++" << endl;
		    cout << "[lat_trig_data_scan] stubs are found at stub_data_latency = " << std::dec << i << endl;
		    int evt_count(0), evt_dump_count(0);
		    Cbc3BeInterface::DumpData( data_buffer, evt_count, evt_dump_count, 1, 1, 0, nwords * 4 );
//		    be_data_decoder.DumpEventBeData(cout);
		    cout << "[lat_trig_data_scan] +++++++++++++++++++++++++" << endl;
		}
	    }
	}
    }
}
