/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <exception>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "Cbc3BeData.h"
#include "seq_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include "tmc.h"
#include "wt.h"
#include "cbc3daq_heproot_utils.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;
using namespace wt;

//---------------------------
// default ipbus settings
string connection_file( "connections.xml" );
string id("board");
string cbc3hal_dir("./");
//---------------------------
// keithley settings and variables
extern "C" {
#include "ut.h"
}
char kmm_sern[256] = "8004246";
//---------------------------
// default file names 
string pedestal_file = "vcthscan-pedestal.root"; 
string offset_file ="offset_tuning-be1cbc1.knt";

//---------------------------
//Emergency ipbus interface outside Cbc3BeInterface.
HwInterface *hw(0);

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --kmmSerN   [Keithley Multimeter serial number]   "  << ": Keithley multimeter serial number to use.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< setw(indent) << " --tune                                            "  << ": CBC biases are measured with multiple times after changing the settings for the test tuning."  << endl 
	<< setw(indent) << " --noFuse                                          "  << ": Fuse burning operation is skipped."  << endl 
	<< setw(indent) << " --chipId    [chip id]                             "  << ": Chip Id to burn and test."  << endl 
	<< setw(indent) << " --testFrames [frames list to test]                "  << ": example frame05,frame06"   << endl 
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << " --wtSettingPath [wt setting file path]            " << ": default is ${CBC3HAL_ROOT}/etc/wt" << endl 
	<< setw(indent) << "([wafer test config file name])                    "  << ": default is wt_config.txt." << endl
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< setw(indent) << "[kmm serial #]    : WT_KMM_SERN,          " << kmm_sern << "\n"
	<< endl;

}


int main( int argc, char *argv[] )
{

    stringstream oss;

    //setLogLevelTo( uhal::Fatal() );
    setLogLevelTo( uhal::Debug() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3SYSTEM" );
    if( env_value != NULL)
	    cbc3hal_dir = string(env_value) + "/sw/cbc3hal";
    env_value = getenv( "WT_KMM_SERN" );
    if( env_value != NULL)
	    strcpy(kmm_sern, env_value);	

    string wt_config_file("wt_config.txt");

    string test_names("frame03,frame05,frame06,frame07,frame08,frame10,frame11,frame13_a,frame13_b,frame14,frame15,frame16,frame17,frame18,frame19,frame20,frame21,frame22,frame23,frame24,frame25,frame26,frame27,frame28,frame29,frame30");
    //string test_names("frame03,frame05,frame06,frame07,frame08,frame10,frame11,frame13_a,frame13_b,frame14,frame15,frame16,frame17,frame18,frame19,frame20,frame21,frame22,frame23,frame24,frame25,frame27,frame28,frame29,frame30");
    bool CBC3_0(true);
    int debug(0);
    bool tune(false);
    string no_fuse("0"); 
    string chip_id("0");
    string setting_file_dir( cbc3hal_dir + "/etc/wt/" );
    
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "kmmSerN"  , required_argument, 0, 'd' },
	    { "debug"    , required_argument, 0, 'e' },
	    { "tune"     , no_argument, 0, 'f' },
	    { "noFuse"   , no_argument, 0, 'g' },
	    { "chipId"   , required_argument, 0, 'h' },
	    { "testFrames", required_argument, 0, 'i'},
	    { "CBC3_1"   , no_argument, 0, 'j' },
	    { "CBC3_0"   , no_argument, 0, 'k' }, //dummy for johan
	    { "wtSettingPath", required_argument, 0, 'm' },
	    {0,0,0,0}
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:fgh:i:jkm:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}
	cerr << "c = " << c << endl;

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		strcpy(kmm_sern, optarg);
		break;
	    case 'e':
		debug = atoi(optarg);
		break;
	    case 'f':
		tune = true;
		break;
	    case 'g':
		no_fuse = "1"; 
		break;
	    case 'h':
		chip_id = optarg;
		break;
	    case 'i':
		test_names = optarg;
		break;
	    case 'j':
		CBC3_0 = false;
		break;
	    case 'k':
		break;
	    case 'm':
		setting_file_dir = optarg;
		break;
	    case '?':
	    default:
		printf("?? getopt returned character code 0%o ??\n", c);
		return -1;
	}
    }
    if( optind < argc ){
	wt_config_file = argv[optind++];
    }

    setLogLevelTo( uhal::Error() );

    char tmp[256];
    int result(0);
    string color = wt::NC;

    reply_fifo_ndata_sum = 0;

    WaferTest *wt(0);

    try{
	cout << wt::NC << wt::time_string() << "CBC3 Wafer testing program begins." <<  endl;
	cout << "Test list : " << test_names << endl;
	/*
	   Initialization for CBC3 Backend interface
	   create ipbus connection and read the firmare build info. 
	   */
	wt = new WaferTest(wt_config_file, setting_file_dir, cout, tune, CBC3_0, debug);
	wt->set_test_setting("frame14", "no_fuse", no_fuse);
	wt->set_test_setting("frame14", "chip_id", chip_id);
	
	wt->set_test_setting("frame15", "no_fuse", no_fuse);

	//Initialization of CBC3BE
	cout << "CONNECTION_FILE : " << connection_file << endl;
	cout << "BOARD ID        : " << id << endl;
	wt->init_be(connection_file, id);
	//Initialization of KEITHLEY multimeter 
	wt->init_kmm(kmm_sern);

	result |= wt->run_test(test_names);

	wt->print_wafer_test();

	sprintf(tmp, "# of I2C transactions = %d", reply_fifo_ndata_sum);
	print_test( "CBC I2C", tmp, cout ); 

	if(result == 0) color = wt::NC;
	else color = wt::YELLOW;

	delete wt;
    }
    catch( IpbusUtilsException &e ){
	if(wt) {
	    wt->print_wafer_test();
	    result = wt->result();
	    delete wt;
	}
	cout << wt::RED << e.what() << endl;
	result |= WT_ERR_IPBUS;
	color = wt::RED;
    }
    catch( CbcI2cRegIpbusException &e ){
	if(wt) {
	    wt->print_wafer_test();
	    result = wt->result();
	    delete wt;
	}
	cout << wt::RED << e.what() << endl;
	result |= WT_ERR_I2C;
	color = wt::RED;
    }
    catch( Cbc3BeException &e ){
	if(wt) {
	    wt->print_wafer_test();
	    result = wt->result();
	    delete wt;
	}
	wt->print_wafer_test();
	cout << wt::RED << e.what() << endl;
	std::string current_test = wt->current_test();
	result |= WT_ERR_BE;

	if( e.type() == Cbc3BeException::DAQ_ERR ) result |= WT_ERR_DAQ; 
	else if( e.type() == Cbc3BeException::SEQ_ERR ) result |= WT_ERR_SEQ; 
	else result |= WT_ERR_OTHER; 
	color = wt::RED;
    }
    catch( WaferTestException &e ){
	if(wt) {
	    wt->print_wafer_test();
	    result = wt->result();
	    delete wt;
	}
	cout << wt::RED << wt::time_string() << e.what() << endl;
	result |= e.ecode();
	color = wt::RED;
    }
    catch(std::exception &e){
	if(wt) {
	    wt->print_wafer_test();
	    result = wt->result();
	    delete wt;
	}
	cout << wt::RED << wt::time_string() << e.what() << endl;
	result |= WT_ERR_OTHER;
	color = wt::RED;
    }
//    if(((result & 0x0000007f) == 0 ) && ((result & 0xffffff00) != 0)) result |= 0x00000080; 
    if(((result & 0x0000007f) == 0 ) && ((result & 0xffffff00) != 0)) result |= WT_ERR_TEST; 

    sprintf(tmp, "0x%08x", result);
    print_test("WAFER TEST RESULT", tmp, cout, color);
    color = wt::NC;
    cout << color;

    return result;
}


