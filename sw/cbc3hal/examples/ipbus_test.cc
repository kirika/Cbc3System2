/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <TH1F.h>
#include <TPad.h>
#include <TStyle.h>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --configBe  [be board configuration file]         "  << ": be board configuration enable with the file.\n" 
	<< setw(indent) << " --configCbc [cbc i2c reg. config. file list file] "  << ": cbc i2c register configuration enable with the file.\n" 
	<< setw(indent) << " --config    [scan configuration file]             "  << ": be board configuration for scan enable with the file.\n" 
	<< setw(indent) << " --group     [test pulse group]                    "  << ": test pulse group. 0 for default. \n" 
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}

int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    string be_config_file = "";
    string cbci2creg_config_file = "";
    string config_file = "";
    string output_dir = "";
    int group(0);
    bool CBC3_0(true);
    int debug(0);

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "configBe" , required_argument, 0, 'd' },
	    { "configCbc", required_argument, 0, 'e' },
	    { "config"   , required_argument, 0, 'f' },
	    { "group"    , required_argument, 0, 'g' },
	    { "debug"    , required_argument, 0, 'h' },
	    { "CBC3_1"   , no_argument, 0, 'i' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h:i", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		be_config_file = optarg;
		break;
	    case 'e':
		cbci2creg_config_file = optarg;
		break;
	    case 'f':
		config_file = optarg;
		break;
	    case 'g':
		group = atoi(optarg); 
		break;
	    case 'h':
		debug = atoi(optarg);	
		break;
	    case 'i':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }

    HwInterface *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    int fci_delay(1);
    //create ipbus connection and read the node value
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7(CBC3_0, debug);
	cbc3be->Initialize( connection_file, id );
	read_ipbus( hw, "cs_stat.system.id" );
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	cout << "IPBUS is not ready" << endl;
	return -1;
    }

    std::map<std::string,uint32_t> regmap;

    // system config

    if( be_config_file != "" ) cbc3be->ConfigureBeBoard( be_config_file, fci_delay );

    // run config
    if( config_file != "" ){
	cbc3be->WriteBeBoardConfig( config_file );
	std::map<std::string,uint32_t> regmap;
	cbc3be->ReadBeBoardConfig( config_file, regmap );
	std::map<std::string,uint32_t>::iterator it = regmap.begin();
	for(; it != regmap.end(); it++ ) cout << it->first << " " << it->second << endl;
    }

    // cbci2creg config
    if( cbci2creg_config_file != "" ){
       if( !cbc3be->ConfigureCbcs( cbci2creg_config_file ) ) return -1;
    }

    TH1F *htreq = new TH1F("htreq", "; time took to request data size [usec]", 100, 0, 10000 ); 
    TH1F *htread = new TH1F("htread", "; time took to request data size [usec]", 100, 0, 100000 ); 
    unsigned long total_words_read(0);

    string node = "cs_stat.db.nword_events";
    struct timeval T0, t0, tr0, t, tr, dt, dT;

    cbc3be->InitializeRun();
    cbc3be->StartFastSignalGenerator();

    int nsleep(0);
    gettimeofday(&T0,0);
    while(true){
	gettimeofday(&t0,0);
	ValWord<uint32_t> nword = hw->getNode( node ).read();
	hw->dispatch();
	gettimeofday(&t,0);
	//cout << nword << endl;
	timersub(&t,&t0,&dt);
	htreq->Fill(dt.tv_sec * 1000000 + dt.tv_usec);
	if(nword){
	    gettimeofday(&tr0,0);
	    uhal::ValVector<uint32_t> data = hw->getNode("data").readBlock( nword );
	    hw->dispatch();
	    gettimeofday(&tr,0);
	    timersub(&tr,&tr0,&dt);
	    htread->Fill(dt.tv_sec * 1000000 + dt.tv_usec);
	    total_words_read += nword;
	    if(nsleep) cout << "slept " << nsleep << endl;
	    nsleep = 0;
	}
	else{
	   usleep(2); 
	   nsleep++;
	}
	/*
	ipbutl::read_ipbus(hw, node);
	*/
	timersub(&t,&T0,&dT);
	if( dT.tv_sec > 20 ) break;
    }
    cout << "Total words read = " << std::dec << total_words_read << endl;
    cout << "event count      = " << std::dec << total_words_read / ( 3 + 11 * cbc3be->GetNActiveCbcs()) << endl;
    gStyle->SetOptStat(1111111);
    htreq->SetStats(1);
    htreq->Draw();
    gPad->SetLogy();
    gPad->Update();
    gPad->Print("htreq.png");
    htread->SetStats(1);
    htread->Draw();
    gPad->Update();
    gPad->Print("htread.png");
}
