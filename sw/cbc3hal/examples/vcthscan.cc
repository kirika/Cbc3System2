/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <set>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "Cbc3BeData.h"

#include <TH1F.h>
#include <TFile.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");
/*
std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}
*/

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --configBe  [be board configuration file]         "  << ": be board configuration file.\n" 
	<< setw(indent) << " --configCbc [cbc i2c reg. config. file list files separated by ','] "  << ": cbc i2c register configuration files.\n" 
	<< setw(indent) << " --config    [vcthscan configuration file]         "  << ": be board configuration for vcthscan enable with the file.\n" 
	<< setw(indent) << " --outDir    [output directory]                    "  << ": output directory.\n"
	<< setw(indent) << " --tp_pot    [value]                               "  << ": test pulse potentiometer(255-regvalue) value to set. test pulse is disabled by default.\n" 
	<< setw(indent) << " --tp_delay  [value]                               "  << ": test pulse delay value to set.\n" 
	<< setw(indent) << " --trig_lat  [value]                               "  << ": trigger latency. default: 200\n" 
	<< setw(indent) << " --dll       [value]                               "  << ": DLL value.\n"
	<< setw(indent) << " --group_type [value]                              "  << ": group.\n"
	<< setw(indent) << " --test_group [value]                              "  << ": test group.\n"
	<< setw(indent) << " --desc                                            "  << ": vcth scan descending order.\n"
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< setw(indent) << " --optionCbc [cbc i2c reg optional config files]   "  << ": cbc i2c settings to overwrite the config settings. \n" 
	<< setw(indent) << " --min        [value]                              "  << ": vcth minimum value to scan.\n"
	<< setw(indent) << " --max        [value]                              "  << ": vcth maximum value to scan.\n"
	<< setw(indent) << " --CBC3_1                                         "  << ": default is CBC3_0.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    string be_config_file = "";
    string cbci2creg_config_file = "";
    string config_file = "";
    string output_dir = "";

    int trig_lat(200);
    int tp_pot(-1);
    int tp_delay(0);
    int dll(-1);
    int group_type(8);
    int test_group(-1);
    int min(-1);
    int max(-1);

    bool desc(false);
    bool CBC3_0(true);
    int debug(0);
    string cbci2creg_option_files("");
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "configBe" , required_argument, 0, 'd' },
	    { "configCbc", required_argument, 0, 'e' },
	    { "config"   , required_argument, 0, 'f' },
	    { "outDir"   , required_argument, 0, 'g' },
	    { "tp_pot"   , required_argument, 0, 'h' },
	    { "tp_delay" , required_argument, 0, 'i' },
	    { "trig_lat" , required_argument, 0, 'j' },
	    { "dll"      , required_argument, 0, 'k' },
	    { "group_type", required_argument, 0, 'l' },
	    { "test_group", required_argument, 0, 'r' },
	    { "desc"      , no_argument, 0, 'm' },
	    { "debug"     , required_argument, 0, 'n' },
	    { "optionCbc" , required_argument, 0, 'o' },
	    { "min"       , required_argument, 0, 'p' },
	    { "max"       , required_argument, 0, 'q' },
	    { "CBC3_1"   , no_argument, 0, 's' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h:i:j:k:l:mn:o:r:s", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		be_config_file = optarg;
		break;
	    case 'e':
		cbci2creg_config_file = optarg;
		break;
	    case 'f':
		config_file = optarg;
		break;
	    case 'g':
		output_dir  = optarg;
		break;
	    case 'h':
		tp_pot = atoi(optarg);
		break;
	    case 'i':
		tp_delay = atoi(optarg);
		break;
	    case 'j':
		trig_lat = atoi(optarg);
		break;
	    case 'k':
		dll     = atoi(optarg);
		break;
	    case 'l':
		group_type = atoi(optarg);
		break;
	    case 'm':
		desc   = true;
		break;
	    case 'n':
		debug = atoi(optarg);
		break;
	    case 'o':
		cbci2creg_option_files = optarg;
		break;
	    case 'p':
		min = strtol(optarg, 0, 0 );
		break;
	    case 'q':
		max = strtol(optarg, 0, 0 );
		break;
	    case 'r':
		test_group = atoi(optarg);
		break;
	    case 's':
		CBC3_0 = false;
		break;
	    default:
		abort();
	}
    }

    HwInterface *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    //create ipbus connection and read the node value
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7(CBC3_0, debug);
	cbc3be->Initialize( connection_file, id );
	read_ipbus( hw, "cs_stat.system.id" );
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	cout << "IPBUS is not ready" << endl;
	return -1;
    }
//    cbc3be->WriteCbcI2cRegsToDB();

    /*
    uint32_t *data_buffer;
    data_buffer = new uint32_t[66000];
    BeDataDecoder be_data_decoder;
    */

    int sample_count(100);

    std::map<std::string,uint32_t> regmap;
    // system config
    if( be_config_file != "" ) {
	cbc3be->ConfigureBeBoard( be_config_file );
	cbc3be->ReadBeBoardConfig( be_config_file, regmap );
    }
    // vcthscan config
    if( config_file != "" ) {
	cbc3be->WriteBeBoardConfig( config_file );
	cbc3be->ReadBeBoardConfig( config_file, regmap );
    }
    std::map<std::string,uint32_t>::iterator it = regmap.begin();
    it = regmap.find( "cs_cnfg.fcm.fcg.Ncycle" );
    if( it != regmap.end() ) sample_count = it->second; 
//    for(; it != regmap.end(); it++ ) cout << it->first << " " << it->second << endl;

    // cbci2creg config
    if( cbci2creg_config_file != "" ){
	cbc3be->ConfigureCbcs( cbci2creg_config_file );
    }
    if( cbci2creg_option_files != "" ){
	string tmp = rstr( cbci2creg_option_files, ",", " ");
	istringstream iss(tmp);
	string opt_file;
	while( !iss.eof() ){
	    iss >> opt_file; 
	    cbc3be->ConfigureCbcs( opt_file );
	}
    }

    CBCI2C_ITEM_DATA_MAP item_data;

    //trigger latency setting
    item_data.clear();
    item_data["trig_lat"] = trig_lat;
    if( !cbc3be->writeAllCbcI2cRegsByType("FcCntrlCompBetaTrigLat", item_data) ) return -1;

    if( dll != -1 ){ 
	item_data["dll"] = dll;
	if(!cbc3be->writeAllCbcI2cRegsByType("40MHzClockOr254DLL", item_data) ) return -1;
	dll = item_data["dll"];
	usleep(10);
    }

    cbc3be->SendFastSignal("reset");

    //test pulse settings
    unsigned en(1), gnd(1);
    if( tp_pot == -1 ){ 
	en=0;
	item_data.clear();
	item_data["tp_en"] = en; 
	if( !cbc3be->writeAllCbcI2cRegsByType("TestPulsePolEnAMux", item_data) ) return -1;
	/*
	cout << "CBCID=" << cbc_id << endl;
	cout << "Test pulse pol " << item_data["tp_pol"] << ", amux " << item_data["amux"] << endl;
	cout << "Test pulse enable " << item_data["tp_en"] << ", gnd " << item_data["tp_gnd"] << endl;
	*/
    }
    else{
	item_data.clear();
	item_data["pot"] = tp_pot;
	if( !cbc3be->writeAllCbcI2cRegsByType("TestPulsePotentiometer",item_data) ) return -1;
	cout << "Test pulse size " << std::dec << tp_pot << endl;

	item_data.clear();
	item_data["delay"] = tp_delay;
	if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) ) return -1;

	item_data.clear();
	item_data["tp_en"] = en; 
	item_data["tp_gnd"] = gnd; 
	if( !cbc3be->writeAllCbcI2cRegsByType("TestPulsePolEnAMux",item_data) ) return -1;

	int tpg = 1;
	item_data.clear();
	item_data["tpg"] = tpg;
	if(!cbc3be->writeAllCbcI2cRegsByType("40MHzClockOr254DLL", item_data) ) return -1;
	//cbc3be->SendFastSignal("reset");
    }
    /*
    int st_sel = 2;
    int pt_width = 3; 
    item_data.clear();
    item_data["st_sel"] = st_sel;
    item_data["pt_width"] = pt_width;
    if(!cbc3be->writeAllCbcI2cRegsByType("PlStLogicSelAndPtWidth", item_data) ) return -1;
    */

    /*
    item_data.clear();
    item_data["hip_count"] = 0;
    item_data["hip_supp"] = false;
    item_data["hip_src"] = 1;  
    if(!cbc3be->writeAllCbcI2cRegsByType("HipAndTestMode", item_data) ) return -1;
    struct cbc_i2c_command com;
    print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, 1 );
    */

    for( unsigned cbc_id=1; cbc_id <= cbc3be->GetNCbcs(); cbc_id ++){
	if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	CBCI2C_ITEM_DATA_MAP item_data;
	CBCI2C_TYPE_DATA type_data("FciAndError", item_data);
	cbc3be->rwCbcI2cRegsByType(1,0,type_data,cbc_id);
	item_data = type_data.second;
	if( item_data["err_bad_code"] | item_data["err_sync_lost"] | item_data["err_sync_stat"] | item_data["err_lat_err"] | item_data["err_ram_ofl"] ){
	    cerr << "Readback BadCode=" << item_data["err_bad_code"] 
		<< " SyncStat="         << item_data["err_sync_lost"]
		<< " SyncLost="         << item_data["err_sync_stat"]
		<< " LatencyError="     << item_data["err_lat_err"]
		<< " BufRamOfl="        << item_data["err_ram_ofl"] 
		<< endl; 
	}
    }
    cbc3be->ReadAllCbcConfig();
    for( unsigned cbc_id=1; cbc_id <= cbc3be->GetNCbcs(); cbc_id ++){
	if( !cbc3be->IsCbcActive(cbc_id) ) continue;

	char fname[256];
	sprintf(fname, "vcthscan-be%dcbc%d.knt", cbc3be->GetBeId(), cbc_id );

	string name;
	if( output_dir == "" ) name = fname;
	else name = output_dir + "/" + fname;
	ofstream ofile(name.c_str());
	print_regcnf( cbc3be->GetCbcI2cRegReplies(), ofile, cbc_id ); 
    }

    if( (debug >> 3) & 1 ){ 
	for( unsigned cbc_id=1; cbc_id <= cbc3be->GetNCbcs(); cbc_id ++){
	    if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	    CBCI2C_TYPE_DATA_MAP cbci2c_type_data_map = cbc3be->readCbcAllI2cRegsByType(cbc_id);
	    dump_cbci2c_type_data_map(cbci2c_type_data_map, cout);
	}
    }

    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA type_data( "VCTH", item_data );
    std::map<unsigned, CBCI2C_TYPE_DATA> cbc_id_data_vcth0;
    cbc_id_data_vcth0[0] = type_data;
    cbc3be->rwCbcI2cRegsByType( 1, 0, cbc_id_data_vcth0 );

    //Get initial offsets
    map<unsigned, vector<unsigned> > offsets0;
    for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	cbc3be->rwCbcI2c_Offsets(1,0,offsets0[cbc_id],cbc_id);
	cout << "read offsets " << endl;
    }
    //Set all offsets to 0xFF
    unsigned offset(0xFF);
    cbc3be->rwCbcI2c_Offset(1,1,0,offset,0);

    //Opening ROOT file to save histograms.
    char fname[256];
    if( tp_pot == -1 ){
	sprintf(fname, "vcthscan-pedestal.root" );
    }
    else{
	sprintf(fname, "vcthscan-tppot%03d_tpdelay%02d_triglat%03d_dll%02d.root", tp_pot, tp_delay, trig_lat, dll );
    }
    string name;
    if( output_dir == "" ) name = fname;
    else name = output_dir + "/" + fname;
    TFile fout( name.c_str(), "recreate" ); 

    /*
    if(group_type == 10){
	GROUP_CHANNELSET_MAP gmap0;
	for(unsigned i=0; i<1;i++){
	    set<unsigned> channel_set;
	    channel_set.insert(1 + i*2);
	    channel_set.insert(2 + i*2);
	    gmap0[i] = channel_set;
	}
	Cbc3BeInterface::SetGroupChannelSetMap(gmap0);
    }
    */
    Cbc3BeInterface::SetGroupChannelSetMap(Cbc3BeInterface::InitGroupChannelSetMap(group_type) );
    int ng = Cbc3BeInterface::GetGroupChannelSetMap().size(); 
    cout << "Number of Groups " << ng << endl;
    const GROUP_CHANNELSET_MAP & gmap = Cbc3BeInterface::GetGroupChannelSetMap();  
    GROUP_CHANNELSET_MAP::const_iterator it_gmap = gmap.begin();
    for(; it_gmap != gmap.end(); it_gmap++){
	cout << "GROUP ID = [" << it_gmap->first << "] CHANNEL ID = [ ";
	const std::set<CHANNEL_ID> &chset = it_gmap->second;
	set<CHANNEL_ID>::const_iterator it_chset = chset.begin();
	for(; it_chset != chset.end(); it_chset++) cout << *it_chset << " ";
	cout << "]" << endl;
	cout << "The number of channels is " << std::dec << chset.size() << endl;
    }
    
    //VCTH scan
//    for( int g=0; g<ng; g++){
    it_gmap = gmap.begin();
    for(; it_gmap != gmap.end(); it_gmap++){
	int g = it_gmap->first;
	if( tp_pot != -1 ){
	    CBCI2C_ITEM_DATA_MAP item_data;
	    item_data["delay"] = tp_delay;
//	    item_data["group"] = g;
	    if (test_group == -1){
		item_data["group"] = g;
	    }
	    else{
		item_data["group"] = test_group;
	    }
	    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) ) return -1;
	    if(debug) cout << "[vcthscan] group = " << std::dec << g << " delay = " << tp_delay << endl;
	}

	cbc3be->ActivateGroupOffsets( g, offsets0, 0 );

	CbcHistList<TH1F> hlist;
//	map<string, TH1F *> hmap;

	cbc3be->ScanVcth(g, sample_count, hlist, desc, min, max );

	map<unsigned, TH1F*> &hmap = hlist.GetHistMap();
	cout << "Total number of histograms is " << hmap.size() << endl;
	map<unsigned,TH1F *>::iterator it_hmap = hmap.begin();
	for( ; it_hmap != hmap.end(); it_hmap++ ){
	    it_hmap->second->Write();
	}

	cbc3be->DisableGroupOffsets( g, 0 );
    }
    fout.ls();
    fout.Close();

    for( unsigned cbc_id = 1; cbc_id <= cbc3be->GetNCbcs(); cbc_id++ ){
	if( !cbc3be->IsCbcActive(cbc_id) ) continue;
	cbc3be->rwCbcI2c_Offsets(1,1,offsets0[cbc_id],cbc_id);
//	cout << "set offsets " << endl;
    }
    cbc3be->rwCbcI2cRegsByType( 1, 1, cbc_id_data_vcth0 );

    return 0;
}
